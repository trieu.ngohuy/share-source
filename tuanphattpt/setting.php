<?php
return array (
  'WebsiteName' => 'CÔNG TY TNHH KỸ THUẬT CÔNG NGHỆ TUẤN PHÁT TPT',
  'Email' => 'tuanphattpt@tuanphattpt.com.vn',
  'Logo' => 'media/userfiles/user/logo.png',
  'FooterLogo' => 'media/userfiles/user/logo.png',
  'Add' => '289/10 Đường số 10, Phường 8, Quận Gò Vấp, TP Hồ Chí Minh',
  'Office' => '129/15 Đường số 2, Phường 16, Quận Gò Vấp, TP Hồ Chí Minh',
  'MST' => '0314849757',
  'Website' => 'tuanphattpt.com.vn',
  'status_footer' => 'Copyrights © 2017 Tuấn Phát. All Rights Reserved.',
  'hotline' => '0909 87 49 16',
  'phone_call' => '(84)909 87 49 16',
  'facebook' => 'tuanphattpt',
  'google' => '#',
  'skype' => '#',
  'youtube' => '#',
  'yahoo' => '#',
  'status_home' => '',
  'google_map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.530808271649!2d106.65937051420457!3d10.84717306085142!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529bae01e6791%3A0x721d679343301009!2zMTI5LCAxNSDEkMaw4budbmcgU-G7kSAyLCBwaMaw4budbmcgMTYsIEfDsiBW4bqlcCwgSOG7kyBDaMOtIE1pbmgsIFZpZXRuYW0!5e0!3m2!1sen!2s!4v1535044330983',
  'contact_detail' => '<h2>
	<span style="font-size:16px;">C&Ocirc;NG TY TNHH KỸ THUẬT C&Ocirc;NG NGHỆ TUẤN PH&Aacute;T TPT</span></h2>
<p>
	<span style="font-size:16px;">289/10 Đường số 10, Phường 8, Quận G&ograve; Vấp, TP Hồ Ch&iacute; Minh</span></p>
<p class="footer_col_2_tel">
	<span style="font-size:16px;">Tel: +84-28-39961882 - 39961883</span></p>
<p class="footer_col_2_fax">
	<span style="font-size:16px;">Fax:&nbsp;+84-28-39961882</span></p>
<p class="footer_col_2_website">
	<span style="font-size:16px;">Website: www.tuanphattpt.com.vn&nbsp;</span></p>
',
  'contact_detail_en' => '<h2>
	<span style="font-size: 16px;">C&Ocirc;NG TY TNHH KỸ THUẬT C&Ocirc;NG NGHỆ TUẤN PH&Aacute;T TPT a</span></h2>
<p>
	<span style="font-size: 16px;">289/10 Đường số 10, Phường 8, Quận G&ograve; Vấp, TP Hồ Ch&iacute; Minh</span></p>
<p class="footer_col_2_tel">
	<span style="font-size: 16px;">Tel: +84-28-39961882 - 39961883</span></p>
<p class="footer_col_2_fax">
	<span style="font-size: 16px;">Fax:&nbsp;+84-28-39961882</span></p>
<p class="footer_col_2_website">
	<span style="font-size: 16px;">Website: www.tuanphattpt.com.vn&nbsp;</span></p>
',
  'Contacts' => '<div class="contactDetail col-md-6">
	<div class="contactWrap">
		<div class="contactHeadline">
			Mua h&agrave;ng</div>
		<div class="contactContent">
			<a class="ctaButton" href="tel:+84912122016">0902538429<span>(Ms. Liễu)</span></a></div>
	</div>
</div>
<hr />
<div class="contactDetail col-md-6">
	<div class="contactHeadline">
		Tư vấn kỹ thuật</div>
	<div class="contactContent">
		<a class="ctaButton" href="tel:+84937659657">0938252474<span>(Mr. Quang)</span></a></div>
</div>
<hr />
<div class="contactDetail col-md-6">
	<div class="contactWrap">
		<div class="contactHeadline">
			Điện thoại cố định</div>
		<div class="contactContent">
			<a class="ctaButton" href="tel:+842837269399">028.39961882<span>(Ms. Liễu)</span></a></div>
	</div>
</div>
<hr />
<div class="contactDetail col-md-6">
	<div class="contactHeadline">
		Email</div>
	<div class="contactContent">
		<a class="ctaButton" href="mailto:potechled@gmail.com"><span style="font-weight: bold;font-size: 17px;line-height: 38px;">lieula@tuanphattpt.com.vn</span><span>(Ms. Liễu)</span></a></div>
</div>
<p>
	&nbsp;</p>
',
  'Description' => '<div class="col-md-6">
	<p>
		<strong style="color: #d71818;">TUẤN PH&Aacute;T TPT&nbsp; l&agrave; C&ocirc;ng ty chuy&ecirc;n cung cấp sản phẩm v&agrave; giải ph&aacute;p chiếu s&aacute;ng đ&egrave;n LED tiết kiệm năng lượng.</strong></p>
	<p>
		Ch&uacute;ng t&ocirc;i l&agrave; nh&agrave; sản xuất đ&egrave;n LED chiếu s&aacute;ng hiệu quả chỉ sử dụng duy nhất chip LED Philips v&agrave; Mạch điều khiển LED Philips đứng h&agrave;ng đầu thế giới về chất lượng. C&ocirc;ng ty TUẤN PH&Aacute;T TPT ho&agrave;n to&agrave;n đ&aacute;p ứng đầy đủ nhu cầu chiếu s&aacute;ng như đ&egrave;n LED nh&agrave; xưởng, đ&egrave;n LED s&acirc;n b&oacute;ng đ&aacute;, đ&egrave;n LED đa năng, đ&egrave;n pha LED, đ&egrave;n đường LED, đ&egrave;n LED tube, đ&egrave;n LED &acirc;m trần, đ&egrave;n LED bulb, đ&egrave;n LED downlight,...</p>
	<p>
		<strong style="color: #d71818;">C&aacute;c hạng mục chiếu s&aacute;ng chuy&ecirc;n nghiệp của c&ocirc;ng ty gồm:</strong></p>
	<ul>
		<li>
			Chiếu s&aacute;ng nh&agrave; xưởng, nh&agrave; m&aacute;y, chế biến, nh&agrave; kho.</li>
		<li>
			Chiếu s&aacute;ng c&aacute;c c&ocirc;ng tr&igrave;nh c&ocirc;ng cộng, cảnh quan, s&acirc;n b&atilde;i, bến b&atilde;i.</li>
		<li>
			Chiếu s&aacute;ng đường giao th&ocirc;ng, đường phố, đường ng&otilde;, đường nội bộ nh&agrave; m&aacute;y, khu c&ocirc;ng nghiệp.</li>
		<li>
			Chiếu s&aacute;ng s&acirc;n b&oacute;ng đ&aacute;, s&acirc;n vận động, nh&agrave; thi đấu đa năng, s&acirc;n quần vợt, cầu l&ocirc;ng, b&oacute;ng b&agrave;n, bể bơi...</li>
		<li>
			Chiếu s&aacute;ng dẫn dụ c&aacute; cho t&agrave;u thuyền đ&aacute;nh bắt c&aacute; xa bờ.</li>
	</ul>
	<p>
		Đặc biệt, C&ocirc;ng ty TUẤN PH&Aacute;T đ&atilde; sản xuất th&agrave;nh c&ocirc;ng đ&egrave;n LED t&agrave;u c&aacute; d&agrave;nh cho c&aacute;c t&agrave;u đ&aacute;nh bắt xa bờ đ&aacute;p ứng nhu cầu tiết kiệm điện d&agrave;nh cho chiếu s&aacute;ng của h&agrave;ng triệu ngư d&acirc;n Việt Nam.</p>
</div>
<div class="col-md-6">
	5
	<p>
		&nbsp;</p>
</div>
<p>
	&nbsp;</p>
',
  'HeadQuaters' => '<p>
	<strong>MIỀN NAM: TRỤ SỞ CH&Iacute;NH</strong></p>
<p style="color: #d71818;">
	<strong>C&Ocirc;NG TY TNHH KỸ THUẬT C&Ocirc;NG NGHỆ TUẤN PH&Aacute;T TPT</strong></p>
<p>
	<strong>Địa chỉ:</strong> 289/10 Đường số 10, Phường 8, Quận G&ograve; Vấp, TP Hồ Ch&iacute; Minh</p>
<p>
	<strong>Hotline:</strong> 0908112706 (MR. Tuấn)</p>
<p>
	<strong>Tư vấn kĩ thuật:</strong>0938252474 (Mr. Quang)</p>
<p>
	<strong>Điện thoại:</strong> 028.39961882-39961883</p>
<p>
	<strong>Fax:</strong></p>
<p>
	<strong>Email:</strong> <a href="mailto:hieu.tuanphat@gmail.com">tuantran@tuanphattpt.com.vn</a></p>
<hr />
<h3>
	ĐỊA CHỈ NH&Agrave; PH&Acirc;N PHỐI</h3>
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>
',
 'HeadQuaters_en' => '<p>
	<strong>SOUTH: HEADEQUATERS</strong></p>
<p style="color: #d71818;">
	<strong>C&Ocirc;NG TY TNHH KỸ THUẬT C&Ocirc;NG NGHỆ TUẤN PH&Aacute;T TPT</strong></p>
<p>
	<strong>Địa chỉ:</strong> 289/10 Đường số 10, Phường 8, Quận G&ograve; Vấp, TP Hồ Ch&iacute; Minh</p>
<p>
	<strong>Hotline:</strong> 0908112706 (MR. Tuấn)</p>
<p>
	<strong>Tư vấn kĩ thuật:</strong>0938252474 (Mr. Quang)</p>
<p>
	<strong>Điện thoại:</strong> 028.39961882-39961883</p>
<p>
	<strong>Fax:</strong></p>
<p>
	<strong>Email:</strong> <a href="mailto:hieu.tuanphat@gmail.com">tuantran@tuanphattpt.com.vn</a></p>
<hr />
<h3>
	ĐỊA CHỈ NH&Agrave; PH&Acirc;N PHỐI</h3>
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>
',
);
