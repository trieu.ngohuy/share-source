<?php
class Route_Cart {
	/**
	 * Build friendly URL
	 *
	 * @param array $linkArr
	 *        	0 => <module>, 1 => controller, 2 => action, 3...n param/value
	 * @param array $params
	 *        	param => value
	 *        	
	 * @return string friendly URL
	 */
	public function build($linkArr, $params = array()) {
		$result = implode ( '/', $linkArr );
		
		/*INDEX CART*/
// 		if ('index' == @$linkArr [2]) {
// 			/**
// 			 * Structure: pages/<id>/<alias>.html
// 			 */
			
// 			if (isset ( $linkArr [6] ))
// 				$result = "cart/{$linkArr[4]}/{$linkArr[6]}/";
// 				else
// 					$result = "cart/{$linkArr[4]}/";
// 					if (null != $params ['alias']) {
// 						$result .= urlencode ( $params ['alias'] );
// 					}
// 		}
		/**
		 * Link is detail
		 */
		if ('detail' == @$linkArr [2]) {
			/**
			 * Structure: pages/<id>/<alias>.html
			 */
			
			if (isset ( $linkArr [6] ))
				$result = "cart/pages/{$linkArr[4]}/{$linkArr[6]}/";
			else
				$result = "cart/pages/{$linkArr[4]}/";
			if (null != $params ['alias']) {
				$result .= urlencode ( $params ['alias'] );
			}
		}
		
		if ('add' == @$linkArr [2]) {
			/**
			 * Structure: pages/<id>/<alias>.html
			 */
			$result = "new/{$linkArr[4]}/";
			if (null != $params ['alias']) {
				$result .= urlencode ( $params ['alias'] );
			}
		}
		
		return $result;
	}
	/**
	 * Parse friendly URL
	 */
	public function parse() {
		$router = Nine_Controller_Front::getInstance ()->getRouter ();
		/*INDEX CART*/
		$routeIndexCart = new Zend_Controller_Router_Route_Regex ( 'cart/(.*).html', array (
				'module' => 'cart',
				'controller' => 'index',
				'action' => 'index'
		), array (
				1 => 'alias'
		) );
		$router->addRoute ( 'routeIndexCart', $routeIndexCart);
		/*
                 * Cart detail
                 */
                $routeCartDetail = new Zend_Controller_Router_Route_Regex ( 'cart/review.html', array (
				'module' => 'cart',
				'controller' => 'index',
				'action' => 'review'
		));
		$router->addRoute ( 'routeCartDetail', $routeCartDetail);
                
		$routeIndexCart2 = new Zend_Controller_Router_Route_Regex ( 'cart', array (
				'module' => 'cart',
				'controller' => 'index',
				'action' => 'index'
		) );
		$router->addRoute ( 'routeIndexCart2', $routeIndexCart2);
		
		
		$routeCart = new Zend_Controller_Router_Route_Regex ( 'cart/pages/([0-9]+)/([0-9]+)/(.*).html', array (
				'module' => 'feedback',
				'controller' => 'index',
				'action' => 'detail' 
		), array (
				1 => 'id',
				1 => 'user_id' 
		) );
		$router->addRoute ( 'routeCart', $routeCart);
		
		$route1 = new Zend_Controller_Router_Route_Regex ( 'cart/([0-9]+)/remove.html', array (
				'module' => 'cart',
				'controller' => 'index',
				'action' => 'remove' 
		), array (
				1 => 'id' 
		) );
		$router->addRoute ( 'cart1', $route1 );
		
		$route4 = new Zend_Controller_Router_Route_Regex ( 'estore/manage-cart/(.*).html', array (
				'module' => 'cart',
				'controller' => 'estore',
				'action' => 'manage-category' 
		), array (
				1 => 'alias' 
		) );
		$router->addRoute ( 'cart4', $route4 );
		
		$route6 = new Zend_Controller_Router_Route_Regex ( 'estore/edit-cart/(.*)/([0-9]+)', array (
				'module' => 'cart',
				'controller' => 'estore',
				'action' => 'detail' 
		), array (
				1 => 'alias',
				2 => 'gid' 
		) );
		$router->addRoute ( 'cart6', $route6 );
		
		$route7 = new Zend_Controller_Router_Route_Regex ( 'estore/edit-cart/(.*)/([0-9]+)/([0-9]+)', array (
				'module' => 'cart',
				'controller' => 'estore',
				'action' => 'edit-cart' 
		), array (
				1 => 'alias',
				2 => 'gid',
				3 => 'lid' 
		) );
		$router->addRoute ( 'cart7', $route7 );
	}
}