<?php
return array (
    'permissionRules' => array(
                        'see_feedback' => 'See all feedbacks',
                        'delete_feedback' => 'Delete existing feedback',
                        'edit_feedback' => 'Edit existing feedback',
                    ),
    'defaultFeedbackNumberRowPerPage'	=>	2
);
