<?php
require_once 'modules/cart/models/Cart.php';
require_once 'modules/mail/models/Mail.php';
require_once 'modules/user/models/User.php';

class cart_AddController extends Nine_Controller_Action
{
	public function indexAction()
	{
	  
	   $id = $this->_getParam('id',false);
      
		if (false == $id) {
			$this->_redirectToNotFoundPage();
		}
        
        
         if($id != "") //  Neu co id truy cap vao
            {
                if(isset($_SESSION['giohang'][$id]))
                {
                    // Tăng số lượng nó lên nếu đã có id sản phẩm đó trong giỏ hàng
                    $_SESSION['giohang'][$id]++;
                }
                else // Nếu chưa có thì thêm mới vào
                {
                    $_SESSION['giohang'][$id] = 1; 
                }
                
            }
            header('Location: /cart');
	}
	
    
	private function _redirectToNotFoundPage()
	{
	    $this->_redirect("");
	}
}