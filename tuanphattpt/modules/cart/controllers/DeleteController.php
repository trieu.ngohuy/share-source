<?php
require_once 'modules/cart/models/Cart.php';
require_once 'modules/mail/models/Mail.php';
require_once 'modules/user/models/User.php';

class cart_DeleteController extends Nine_Controller_Action
{
	public function indexAction()
	{
	   //echo "<pre />";print_r($_POST);die;
	   $id = $this->_getParam('id',false);
      
		if (false == $id) {
			$this->_redirectToNotFoundPage();
		}
		unset ($_SESSION['giohang'][$id]);
		if($_SESSION['giohang'] == null){
			unset ($_SESSION['giohang']);
		}		
		header('Location: /cart');
       
	}
	
    
	private function _redirectToNotFoundPage()
	{
	    $this->_redirect("");
	}
}