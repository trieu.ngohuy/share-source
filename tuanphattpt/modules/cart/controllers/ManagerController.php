<?php
require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/content/models/Content.php';

include_once 'modules/mail/models/phpmailer.php';
include_once 'modules/mail/models/smtp.php';

require_once 'modules/cart/models/Cart.php';
require_once 'modules/cart/models/CartDetail.php';

class cart_ManagerController extends Nine_Controller_Action
{
	public function indexAction()
	{
	  	
		$objCart     = new Models_Cart();
	  	$objCartDetail     = new Models_CartDetail();
	  	$objProductDetail = new Models_Product();
	  	$loggedUser= $_SESSION['user_login'];
	  	if($loggedUser != null){
	  		
	  	}else{
	  		$this->_redirect("");
	  	}
	  	$config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
		if($currentPage == false){
        	$currentPage = 1;
        	$this->session->contentDisplayNum = null;
        	$this->session->contentCondition = null;
        }
        
		if (false === $displayNum) {
            $displayNum = $this->session->contentDisplayNum;
        } else {
            $this->session->contentDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->contentCondition;
        } else {
            $this->session->contentCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        if(@$condition['keyword'] != ''){
        	$condition['keyword'] = $objContent->convert_vi_to_en($condition['keyword']);
        	$condition['keyword'] = preg_replace("/( )/", '-', $condition['keyword']);
        }
        $condition['user_id'] = $loggedUser['user_id'];
        $allCart = $objCart->getAllCarts($condition, array('created_date DESC'),
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage
                                                  );
        
		$count = count($objCart->getAllCarts($condition));
		foreach ($allCart as &$content){
			$content['total_price'] = 0;
			$content['detail'] = $objCartDetail->getByColumns(array('cart_id=?' => $content['cart_id']))->toArray();
			$content['created_date'] = date('d-m-Y',$content['created_date']);
			foreach ($content['detail'] as &$item){
				$item['product'] = $objProductDetail->getCurrentProductByCondition(array('product_gid' => $item['product_gid'] ));
				if($item['product'] != ''){
					$content['total_price'] += ($item['product']['price'] * $item['mount']);
					$item ['product']['url'] = Nine_Route::_ ( "product/index/detail/gid/{$item['product']['product_gid']}", array ('alias' => $item ['product']['alias'] ) );
					$tmp = explode ( '||', $item['product'] ['images'] );
					$item['product']['thumb_image']  =  Nine_Function::getThumbImage ( @$tmp[0], 130, 154 , false , false );
					$item['product']['total_price'] = $objProductDetail->makeUpPrice($item['product']['price'] * $item['mount']);
					$item['product']['tprice_prase'] = $objProductDetail->makeUpPrice($item['product']['price']);
					
					
				}
				
			}
			$content['total_price'] = $objProductDetail->makeUpPrice($content['total_price']);
			
			$content['total_ship'] = $objProductDetail->makeUpPrice($content['total_ship']);
			unset($item);
			$content['total_number'] = $objProductDetail->makeUpPrice($content['total_number']);
		}
		unset($content);
		$this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCart = $allCart;
        $this->view->displayNum = $numRowPerPage;
				
		$this->view->messageCart = $this->session->messageCart;
		$this->session->messageCart = array();
		
		$objProduct = new Models_Product ();
		$objProductCategory = new Models_ProductCategory();
		$objContent = new Models_Content ();
		$allFlasher = $objProduct->getAllProductTinNhanh();
		foreach ( $allFlasher as &$content ) {
			$content ['title'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $content ['title'] ) ), 40 );
			$content ['intro_text'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $content ['full_text'] ) ), 120 );
			if($content ['alias'] == ""){
				$content ['alias'] = $objContent->convert_vi_to_en($content ['alias']);
				$content ['alias'] = str_replace(" ", "-", str_replace("&*#39;","",trim($content ['alias'])));
			}
			$content ['url'] = Nine_Route::_ ( "product/index/detail/gid/{$content['product_gid']}", array ('alias' => $content ['alias'] ) );
			$tmp = explode ( '||', $content ['images'] );
			$content ['main_image'] = Nine_Function::getThumbImage ( @$tmp [0], 182, 109 ,false,false);
		}
		$this->view->allFlasher = $allFlasher;
		
		
	}
	public function detailAction()
	{
	  	
		$objCart     = new Models_Cart();
	  	$objCartDetail     = new Models_CartDetail();
	  	$objProductDetail = new Models_Product();
	  	
	  	$loggedUser= $_SESSION['user_login'];
	  	$id = $this->_getParam('id',false);
	  	if($loggedUser != null){
	  		
	  	}else{
	  		$this->_redirect("");
	  	}
	  	if($id == false){
	  		$this->_redirect("");
	  	}
	  	
		$total_price = 0;
		$cart = current($objCart->getByColumns(array('cart_id=?' => $id))->toArray());
		$detail = $objCartDetail->getByColumns(array('cart_id=?' => $id))->toArray();
		foreach ($detail as &$item){
			$item['product'] = $objProductDetail->getCurrentProductByCondition(array('product_gid' => $item['product_gid']));
			$total_price += ($item['product']['price'] * $item['mount']);
			$item ['product']['url'] = Nine_Route::_ ( "product/index/detail/gid/{$item['product']['product_gid']}", array ('alias' => $item ['product']['alias'] ) );
			$tmp = explode ( '||', $item['product'] ['images'] );
			$item['product']['thumb_image']  =  Nine_Function::getThumbImage ( @$tmp[0], 50, 50 , false , false );
			$item['product']['total_price'] = $objProductDetail->makeUpPrice($item['product']['price'] * $item['mount']);
			$item['product']['tprice_prase'] = $objProductDetail->makeUpPrice($item['product']['price']);
		}
		$total_price = $objProductDetail->makeUpPrice($total_price+$cart['total_ship']);
		$cart['total_number'] = $objProductDetail->makeUpPrice($cart['total_number']);
		$cart['total_ship'] = $objProductDetail->makeUpPrice($cart['total_ship']);
		
		
		
		$this->view->total_price = $total_price;
        $this->view->allCard = $detail;
        $this->view->cart = $cart;
        
        $objProduct = new Models_Product ();
		$objProductCategory = new Models_ProductCategory();
		$objContent = new Models_Content ();
		$allFlasher = $objProduct->getAllProductTinNhanh();
		foreach ( $allFlasher as &$content ) {
			$content ['title'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $content ['title'] ) ), 40 );
			$content ['intro_text'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $content ['full_text'] ) ), 120 );
			if($content ['alias'] == ""){
				$content ['alias'] = $objContent->convert_vi_to_en($content ['alias']);
				$content ['alias'] = str_replace(" ", "-", str_replace("&*#39;","",trim($content ['alias'])));
			}
			$content ['url'] = Nine_Route::_ ( "product/index/detail/gid/{$content['product_gid']}", array ('alias' => $content ['alias'] ) );
			$tmp = explode ( '||', $content ['images'] );
			$content ['main_image'] = Nine_Function::getThumbImage ( @$tmp [0], 182, 109 ,false,false);
		}
		$this->view->allFlasher = $allFlasher;
		
		
	}
	public function deleteAction()
	{
		$objCart     = new Models_Cart();
	  	$objCartDetail     = new Models_CartDetail();
	  	$id = $this->_getParam('id',false);
		if($id == false){
	  		$this->_redirect("");
	  	}
	  	$detail = current($objCart->getByColumns(array('cart_id=?' => $id))->toArray());
	  	if(!isset($detail) || $detail['enabled'] != 1){
	  		$this->_redirect("");
	  	}
	  	$objCart->delete(array('cart_id=?' => $id));
	  	$objCartDetail->delete(array('cart_id=?' => $id));
	  	$this->_redirect("cart/manager");
	}
	
    

	private function _redirectToNotFoundPage()
	{
	    $this->_redirect("");
	}
}