<?php
require_once 'modules/cart/models/Cart.php';
require_once 'modules/mail/models/Mail.php';
require_once 'modules/user/models/User.php';
require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/ProductCategory.php';

class cart_SuccessController extends Nine_Controller_Action
{
	public function indexAction()
	{
            /*
             * GEt config
             */
            $this->view->config = Nine_Query::getConfig();
            /*
             * Set layout
             */
            Nine_Common::setLayoutAndView($this, 'Success', 'cart', "success");            
	}
	
    

	private function _redirectToNotFoundPage()
	{
	    $this->_redirect("");
	}
}