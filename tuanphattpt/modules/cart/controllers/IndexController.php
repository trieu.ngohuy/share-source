<?php

require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/content/models/Content.php';

include_once 'modules/mail/models/phpmailer.php';
include_once 'modules/mail/models/smtp.php';

require_once 'modules/cart/models/Cart.php';
require_once 'modules/provinces/models/Provinces.php';
require_once 'modules/cart/models/Payment.php';
require_once 'modules/cart/models/CartDetail.php';
include_once 'modules/mail/models/Mail.php';
require_once 'modules/country/models/simple_html_dom.php';

class cart_IndexController extends Nine_Controller_Action {
    /*
     * Select delivery add
     *
     * @return json()
     */

    public function selectDeliveryAddAction() {
        $objUser = new Models_User();

        //Get post data
        $data = $this->_request->getPost();
        try {
            //Update user
            $temp = array(
                "delivery_add" => $data['delivery_add']
            );
            $objUser->update($temp, array('user_id=?' => $data['user_id']));

            echo json_encode(array(
                "status" => true
            ));
        } catch (Exception $e) {
            echo json_encode(array(
                "status" => false,
                "error" => $e->getMessage()
            ));
        }
        exit;
    }

    /*
     * Update and add new add and add type
     *
     * @return json()
     */

    public function updateDeliveryAddAction() {
        $objUser = new Models_User();

        //Get post data
        $data = $this->_request->getPost();

        //Get user info
        $userInfo = @reset($objUser->getByColumns(array(
                            "user_id" => $data['user_id']
                        ))->toArray());

        try {
            //check if update or new add
            if ($data['type'] == 1) {
                //Get original add and convert to array
                $add = explode('||', $userInfo['add']);
                //Update add
                foreach ($add as $key => &$value) {
                    if ($key == $userInfo['delivery_add']) {
                        $value = $data['add'];
                    }
                }
                unset($value);
                $add = implode("||", $add);
                //Get original add type and convert to array
                $add_type = explode('||', $userInfo['add_type']);
                //Upadte add type
                foreach ($add_type as $key => &$value) {
                    if ($key == $userInfo['delivery_add']) {
                        $value = $data['add_type'];
                    }
                }
                unset($value);
                $add_type = implode("||", $add_type);
            } else {
                //Get original add and add new add to list add
                if (trim($userInfo['add']) != "") {
                    $add = $userInfo['add'] . "||" . $data['add'];
                } else {
                    $add = $data['add'];
                }
                if (trim($userInfo['add_type']) != "") {
                    $add_type = $userInfo['add_type'] . "||" . $data['add_type'];
                } else {
                    $add_type = $data['add_type'];
                }
            }


            //Update user
            $temp = array(
                "full_name" => $data['full_name'],
                "phone" => $data['phone'],
                "add" => $add,
                "add_type" => $add_type
            );
            $objUser->update($temp, array('user_id=?' => $data['user_id']));

            echo json_encode(array(
                "status" => true
            ));
        } catch (Exception $e) {
            echo json_encode(array(
                "status" => false,
                "error" => $e->getMessage()
            ));
        }
        exit;
    }

    public function returnPaymentAction() {
        //get order return
        if (isset($_GET['payment_id'])) {
            $cart_detail = array();
            $cart_detail['payment_id'] = $_GET['payment_id'];
            $cart_detail['transaction_info'] = $_GET['transaction_info'];
            $cart_detail['order_code'] = $_GET['order_code'];
            $cart_detail['price'] = $_GET['price'];
            $cart_detail['payment_type'] = $_GET['payment_type'];
            $cart_detail['error_text'] = $_GET['error_text'];
            $cart_detail['secure_code'] = $_GET['secure_code'];
            $cart_detail['created_date'] = date("Y-m-d h:i:sa");

            $this->view->cart_detail = $cart_detail;

            $cartId = $this->_getParam('cart_id', false);
            $cart_detail['cart_id'] = $cartId;
            //set status to cart
            //0 - waiting
            //1 - in progress
            //2 - delivering
            //3 - payed
            $objCart = new Models_Cart();
            $cart['status'] = 3;
            $cart['payed_date'] = date("Y-m-d");
            $objCart->update($cart, array('cart_id=?' => $cartId));
            //insert to payment table
            $objPayment = new Models_Payment();
            $objPayment->insert($cart_detail);

            //send mail
            $config = Nine_Query::getConfig();

            $this->session->messageCart = array(
                'success' => true,
                'message' => Nine_Language::translate('Payment success. Thanks you for ordering our products. We sent mail to you about cart order. Please check mail.')
            );
        } else {
            $this->session->messageCart = array(
                'success' => 'erro',
                'message' => Nine_Language::translate('Payment failed')
            );
        }
    }

    public function GetDrivingDistance($lat1, $lat2, $long1, $long2) {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving&language=pl-PL";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
        $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

        return array('distance' => $dist, 'time' => $time);
    }

    public function successAction() {
        
    }

    /*
     * Remove product from cart
     */

    public function removeProductAction() {
        try {
            /*
             * Get product gid
             */
            $index = $this->_getParam('index', false);
            /*
             * Get logged user
             */
            $loggedUser = Nine_Common::getLoggedUser($this, true);
            /*
             * If not loggin then go to login page
             */
            if ($loggedUser == '') {
                $this->_redirect('access/login');
            }

            /*
             * Get cart
             */
            $allCart = Nine_Common::getCart($this->session, $this->view);
            /*
             * Remove product from cart
             */
            unset($allCart['cartDetail'][$index]);

            //Calcualting cart
            Nine_Query::CalCart($allCart, $this->session);

            /*
             * Get to review page
             */
            //$this->_redirect('cart/review.html');
            echo $index;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /*
     * Remove cart
     */

    public function removeCartAction() {
        /*
         * Get cart id
         */
        $cartId = $this->_getParam('cartId', false);
        /*
         * Remove cart
         */
        $objCart = new Models_Cart();
        $objCart->delete(array(
            'cart_id=?' => $cartId
        ));
        $objCartDetail = new Models_CartDetail();
        $objCartDetail->delete(array(
            'cart_id=?' => $cartId
        ));
        /*
         * return to cart manager
         */
        //$this->_redirect("user/cart-manager");
    }

    /*
     * Update amount of products in cart
     */

    public function updateCartAction() {
        /*
         * Get list update data
         */
        $data = $this->_getParam('data', false);
        /*
         * Get logged user
         */
        $loggedUser = Nine_Common::getLoggedUser($this, true);
        /*
         * If not loggin then go to login page
         */
        if ($loggedUser == '') {
            $this->_redirect('access/login');
        }

        /*
         * Get cart
         */
        $allCart = Nine_Common::getCart($this->session, $this->view);
        /*
         * Update cart
         */
        foreach ($data as $key => $item) {
            $allCart['cartDetail'][$key]['mount'] = $item['mount'];
        }

        //Calcualting cart
        Nine_Query::CalCart($allCart, $this->session);

        /*
         * Get to review page
         */
        $this->_redirect('cart/review.html');
    }

    /*
     * Cart detail
     */

    public function reviewAction() {
        /*
         * Get logged user
         */
        $loggedUser = Nine_Common::getLoggedUser($this, true);
        /*
         * If not loggin then go to login page
         */
        if ($loggedUser == '') {
            $this->_redirect('access/login');
        }
        /*
         * Get breadcrumbs
         */
        $url = Nine_Route::url();
        $this->view->breadCrumbs = Nine_Common::getBreadCumbs(0, 'cart', array(
                    'name' => Nine_Language::translate("Chi tiết giỏ hàng"),
                    'url' => $url['path'] . 'cart/review.html'
        ));
        /*
         * Get config
         */
        $config = Nine_Common::getConfig($this);
        $this->view->config = $config;
        /*
         * Get cart
         */
        $cart = Nine_Common::getCart($this->session, $this->view);
        /*
         * If cart is empty then goto homepage
         */
        if(count($cart['cartDetail']) <= 0){
            $this->_redirect("/");
        }
        /*
         * Get cart
         */
        /*
         * Set layout and view
         */
        Nine_Common::setLayoutAndView($this, 'Cart', 'cart', 'detail');
    }

    /*
     * Index functions
     */

    public function indexAction() {
        try {
            /*
             * Get breadcrumbs
             */
            $url = Nine_Route::url();
            $this->view->breadCrumbs = Nine_Common::getBreadCumbs(0, 'cart', array(
                        'name' => Nine_Language::translate("Giỏ hàng"),
                        'url' => $url['path'] . 'cart'
            ));

            //GET INPUT DATA
            $data = $this->_getParam('data', false);
            $cart = $this->_getParam('cart', false);
            $branch = $this->_getParam('branch', false);
            $payment = $this->_getParam('payment', false);

            /*
             * Get config
             */
            $config = Nine_Common::getConfig($this);
            $this->view->config = $config;

            /*
             * Get logged user
             */
            $loggedUser = Nine_Common::getLoggedUser($this, true);
            if ($loggedUser == '') {
                $this->_redirect('access/login');
            }
            //GET LOGGED INFO
            $objUser = new Models_User();
            $tempLogUser = @reset($objUser->getByColumns(array(
                                "user_id" => $loggedUser['user_id']
                            ))->toArray());
            if (trim($tempLogUser['add']) != "") {
                $tempLogUser['add'] = explode('||', $tempLogUser['add']);
            } else {
                $tempLogUser['add'] = array();
            }

            $delivery_add = '';
            foreach ($tempLogUser['add'] as $key => $value) {
                if ($key == $tempLogUser['delivery_add']) {
                    $delivery_add = $value;
                }
            }
            $tempLogUser['add_type'] = explode('||', $tempLogUser['add_type']);
            $this->view->tempLogUser = $tempLogUser;

            /*
             * Get cart
             */
            $allCard = Nine_Query::getCart($this->session);

            //$this->session->cart = array();
            //CHECK OUT CART
            if ($cart != false) {
                $message = array();

                //Check if cart is empty
                if (count($allCard) > 0) {
                    //Save to cart
                    $objCart = new Models_Cart();
                    $cartDetail = array(
                        'customer' => $tempLogUser['full_name'],
                        'email' => $tempLogUser['email'],
                        'address' => $delivery_add,
                        'phone' => $tempLogUser['phone'],
                        'company' => $tempLogUser['name_company'],
                        'created_date' => date("Y/m/d"),
                        'enabled' => 1,
                        'user_id' => $loggedUser['user_id'],
                        'total_number' => $allCard['total_price'],
                        'fullname' => $tempLogUser['full_name'],
                        'status' => 1
                    );
                    $cart_id = $objCart->insert($cartDetail);
                    $cartDetail['cart_id'] = $cart_id;

                    //Save cart detail   
                    $objCartDetail = new Models_CartDetail();
                    foreach ($allCard['cartDetail'] as $item) {
                        $temp = $item['product'];
                        $objCartDetail->insert(array(
                            'cart_id' => $cart_id,
                            'product_gid' => $temp['product_gid'],
                            'price' => $temp['tprice_prase'],
                            'mount' => $item['mount']
                        ));
                    }
                    //Send mail
                    $content = $this->MailContent($loggedUser, $allCard, $cartDetail, $cart, $branch, $payment, $delivery_add, $tempLogUser);
                    //echo $content;
                    $data_send = array(
                        'name_to' => $tempLogUser['full_name'],
                        'email_to' => array($config['Email'], $tempLogUser['email']),
                        'title_email' => 'Order- MAXXIMART',
                        'name_from' => $tempLogUser['full_name'],
                        'content' => $content
                    );
                    $this->sendmail_smtp($data_send);
                    //Empty cart
                    $this->session->cart = array();
                    //Go to home page                
                    $this->_redirect("/cart/success");
                } else {
                    $message = array(
                        'success' => false,
                        'message' => Nine_Language::translate('Đơn hàng chưa có sản phẩm.')
                    );
                    $this->view->message = $message;
                }
            }

            //UPDATE CART AMOUNT
            if ($data != false) {
                foreach ($data as $key => $item) {
                    $allCard['cartDetail'][$key]['mount'] = $item['mount'];
                }

                //Calcualting cart
                Nine_Query::CalCart($allCard, $this->session);

                //get all carts
                $allCard = $this->session->cart;
            }


            //GET LIST PROVINCES
            $objProvinces = new Models_Provinces();
            $listProvinces = $objProvinces->getByColumns(array())->toArray();
            $this->view->listProvinces = $listProvinces;

            //GET LIST BRANCHES
            $objUser = new Models_User();
            $listSeller = $objUser->getByColumns(array(
                        "group_id" => 5
                    ))->toArray();
            $this->view->listSeller = $listSeller;
            if (count($allCard) > 0) {
                $this->view->allCard = $allCard;
            } else {
                $this->view->allCard = array(
                    'cartDetail' => array(),
                    'total_price' => 0
                );
            }
            /*
             * Set layout
             */
            Nine_Common::setLayoutAndView($this, 'Cart', 'cart');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString() . '<br />';
            echo $exc->getMessage();
        }
    }

    //Create mail content
    public function MailContent($loggedUser, $allCard, $cartDetail, $cart, $branch, $payment, $delivery_add, $tempLogUser) {
        $content = '<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				    <title>HOMARTTPHCM ORDER MAIL</title><style>
					  .hom-tablet td{
					  	text-align: center;
					  	border: 1px solid black;
					  	width: 200px;
					  }
					</style></head><body>';
        $content .= '<div><h2 style="text-align: center">Chi tiết đơn hàng</h2>';
        $content .= '<br>
							<p><b>Xin cảm ơn ' . $tempLogUser['full_name'] . ', đã mua hàng tại maxximart.com Đây là chi tiết đơn hàng của bạn:</b></p><br>
							<p><b>Thông tin người mua hàng:</b></p><ul style="margin-left: 20px">
							  <li><b>Họ tên:</b> ' . $tempLogUser['full_name'] . '</li><li><b>Address:</b> ' . $delivery_add . '</li>
							  <li><b>Company:</b> ' . $tempLogUser['name_company'] . '</li>
							  
							  <li><b>Email:</b> ' . $tempLogUser['email'] . '</li>
							  <li><b>Phone:</b> ' . $tempLogUser['phone'] . '</li>
							</ul>';
        // $content .='<br><p><b>Chi nhánh nhận hàng:</b></p>
        // 			<ul style="margin-left: 20px">
        // 			  <li><b>Tên chi nhánh:</b> '.$branch['full_name'].'</li>
        // 			  <li><b>Điện thoại:</b> '.$branch['phone'].'</li>
        // 			</ul>';
        $content .= '<br><p><b>Phương thức thanh toán:</b> ' . $payment['method'] . '</p>';
        if ($payment['method'] == 'Visa card , Master card, JCB') {
            $content .= '<ul style="margin-left: 20px;">
					<li><b>Số tài khoản:</b> ' . $payment['cart_number'] . '</li>
					<li><b>Tên chủ khoản:</b> ' . $payment['cart_holder'] . '</li>
					<li><b>Ngày hết hạn:</b> ' . $payment['month'] . '/' . $payment['year'] . '</li>
					</ul>';
        }
        $content .= '<br><p><b>Thông tin đơn hàng:</b></p><ul style="margin-left: 20px">
					  <li><b>Mã số đơn hàng:</b> ' . $cartDetail['cart_id'] . '</li>
					  <li><b>Ngày đặt hàng:</b>  ' . $cartDetail['created_date'] . '</li>
					  <li><b>Tổng giá:</b> ' . $allCard['total_price'] . '</li>
					</ul>';
        $content .= '<br><p><b>Danh sách sản phẩm:</b></p>
					<table class="hom-tablet">
					  <tbody>
					    <tr style="font-weight: bold; background: blue; color: #fff;">
					      <td>#</td>
					      <td>Tên sản phẩm</td>
					      <td>Hình ảnh</td>
					      <td>Giá</td>
					      <td>Số lượng</td>
					      <td>Tổng giá</td>
					    </tr>
					<tr>';
        $url = Nine_Route::url();
        foreach ($allCard['cartDetail'] as $item) {
            $temp = $item['product'];
            $content .= '<td>' . $temp['product_gid'] . '</td>
					  <td><a href="' . $url['path'] . $temp['url'] . '"><b>' . $temp['title'] . '</b></a></td>
					      <td><img src="' . $temp['main_image'] . '" style="width: 100px;"></td>
					      <td>' . $temp['tprice_prase'] . ' đ</td>
					      <td>' . $item['mount'] . '</td>
					      <td>' . $temp['total_price'] . ' đ</td>
					    </tr>';
        }
        $content .= '</tbody>
					</table>
					';

        $content .= '</div>';
        $content .= '</body></html>';
        return $content;
    }

    public function removeAction() {
        $id = $this->_getParam('id', false);
        $allCard = $this->session->cart;

        //Remove product from cart
        unset($allCard['cartDetail'][$id]);

        //Recalculating cart
        Nine_Query::CalCart($allCard, $this->session);

        // if(count($allCard['cartDetail']) == 0){
        //     $allCard['total_price'] = 0;
        // }
        //$this->session->cart = $allCard;
        $this->_redirect('cart');
    }

    public function checkoutAction() {

        $data = $this->_getParam('data', false);

        if ($data != false) {
            $allCard = $this->session->cart;
        }
    }

    public function sendmail_smtp($data) {
        $oldSettings = include 'setting.php';
        $config = $oldSettings;
        $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
        $mail->IsSMTP(); // telling the class to use SMTP
        try {
            //echo $config['Email_Username'] . '/' . $config['Email_Password'];
            //$mail->Host       = "mail.gmail.com"; // SMTP server
            //$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
            $mail->SMTPAuth = true;                  // enable SMTP authentication
            $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
            $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
            $mail->Port = 465;   // set the SMTP port for the GMAIL server
            $mail->SMTPKeepAlive = true;
            $mail->Mailer = "smtp";
            $mail->Username = $config['Email_Username'];  // GMAIL username
            $mail->Password = $config['Email_Password'];            // GMAIL password
            $to = $data['email_to'];
            $name = $data['name_to'];
            if (is_array($to)) {
                foreach ($to as $key => $sto) {
                    $mail->AddAddress($sto, '');
                }
            } else {
                $mail->AddAddress($to, $name);
            }
            $mail->CharSet = "UTF-8";
            $mail->WordWrap = 50;
            //$mail->AddAddress('trieu.ngohuy@gmail.com', 'abc');

            $mail->SetFrom($config['Email_Username'], $data['name_from']);
            $mail->Subject = $data['title_email'];
            $mail->AltBody = $data['content']; // optional - MsgHTML will create an alternate automatically
            $mail->MsgHTML($data['content']);
            $mail->Send();
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }

    private function _redirectToNotFoundPage() {
        $this->_redirect("");
    }

}
