<?php

require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/content/models/Content.php';

include_once 'modules/mail/models/phpmailer.php';
include_once 'modules/mail/models/smtp.php';

require_once 'modules/cart/models/Cart.php';
require_once 'modules/cart/models/CartDetail.php';

class cart_EstoreController extends Nine_Controller_Action
{
public function manageCategoryAction()
    {
    	$url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = @reset($objUser->getByColumns(array('alias=?' => $url['alias']))->toArray());
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $this->setLayout('estore');
        $this->view->alias = $user['alias'] . '.html';
       	$objCart     = new Models_Cart();
	  	$objCartDetail     = new Models_CartDetail();
	  	$objProductDetail = new Models_Product();
	  	
    	$data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $objCart->update(array('enabled' => $value), array('cart_id=?' => $id));
        }
        
        
	  	$loggedUser= $_SESSION['user_login'];
	  	if($loggedUser != null){
	  		
	  	}else{
	  		$this->_redirect("");
	  	}
	  	$config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
		if($currentPage == false){
        	$currentPage = 1;
        	$this->session->contentDisplayNum = null;
        	$this->session->contentCondition = null;
        }
        
		if (false === $displayNum) {
            $displayNum = $this->session->contentDisplayNum;
        } else {
            $this->session->contentDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->contentCondition;
        } else {
            $this->session->contentCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        if(@$condition['keyword'] != ''){
        	$condition['keyword'] = $objContent->convert_vi_to_en($condition['keyword']);
        	$condition['keyword'] = preg_replace("/( )/", '-', $condition['keyword']);
        }
        $condition['vendor_id'] = $user['user_id'];
        $allCart = $objCart->getAllCarts($condition, array('created_date DESC'),
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage
                                                  );
        
		$count = count($objCart->getAllCarts($condition));
		foreach ($allCart as &$content){
			$content['total_price'] = 0;
			$content['detail'] = $objCartDetail->getByColumns(array('cart_id=?' => $content['cart_id']))->toArray();
			$content['created_date'] = date('d-m-Y',$content['created_date']);
			foreach ($content['detail'] as &$item){
				$item['product'] = $objProductDetail->getCurrentProductByCondition(array('product_gid' => $item['product_gid'] ));
				if($item['product'] != ''){
					$content['total_price'] += ($item['product']['price'] * $item['mount']);
					$item ['product']['url'] = Nine_Route::_ ( "product/index/detail/gid/{$item['product']['product_gid']}", array ('alias' => $item ['product']['alias'] ) );
					$tmp = explode ( '||', $item['product'] ['images'] );
					$item['product']['thumb_image']  =  Nine_Function::getThumbImage ( @$tmp[0], 130, 154 , false , false );
					$item['product']['total_price'] = $objProductDetail->makeUpPrice($item['product']['price'] * $item['mount']);
					$item['product']['tprice_prase'] = $objProductDetail->makeUpPrice($item['product']['price']);
				}
			}
			$content['total_price'] = $objProductDetail->makeUpPrice($content['total_price']);
			$content['total_ship'] = $objProductDetail->makeUpPrice($content['total_ship']);
			unset($item);
			$content['total_number'] = $objProductDetail->makeUpPrice($content['total_number']);
		}
		unset($content);
		$this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCart = $allCart;
        $this->view->displayNum = $numRowPerPage;
				
		$this->view->messageCart = $this->session->messageCart;
		$this->session->messageCart = array();
		$this->view->menu = array(
        	0=>'cart',
        );
    }
    
	public function detailAction()
	{
		$url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = @reset($objUser->getByColumns(array('alias=?' => $url['alias']))->toArray());
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
       
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        $this->setLayout('estore');
        $this->view->alias = $user['alias']. '.html';
	  	
		$objCart     = new Models_Cart();
	  	$objCartDetail     = new Models_CartDetail();
	  	$objProductDetail = new Models_Product();
	  	$loggedUser= $_SESSION['user_login'];
	  	$id = $this->_getParam('gid',false);
		$data = $this->_getParam('data', false);
    	
    	if($data != false){
    		$cart = @reset($objCart->getByColumns(array('cart_id=?' => $id))->toArray());
    		if($data['enabled'] == 3 && $cart['enabled']!= 3){
    		}else if($data['enabled'] != 3 && $cart['enabled']== 3){
    		}
    		
    		$objCart->update(array('enabled' => $data['enabled']), array('cart_id=?' => $id));
    	}
	  	
	  	if($loggedUser != null){
	  		
	  	}else{
	  		$this->_redirect("");
	  	}
	  	if($id == false){
	  		$this->_redirect("");
	  	}
	  	
		$total_price = 0;
		$cart = @reset($objCart->getByColumns(array('cart_id=?' => $id))->toArray());
		$detail = $objCartDetail->getByColumns(array('cart_id=?' => $id))->toArray();
		foreach ($detail as &$item){
			$item['product'] = $objProductDetail->getCurrentProductByCondition(array('product_gid' => $item['product_gid']));
			$total_price += ($item['product']['price'] * $item['mount']);
			$item ['product']['url'] = Nine_Registry::getBaseUrl()."detail/{$item['product']['product_gid']}/{$item ['product']['alias']}.html";
			$tmp = explode ( '||', $item['product'] ['images'] );
			$item['product']['thumb_image']  =  Nine_Function::getThumbImage ( @$tmp[0], 50, 50 , false , false );
			$item['product']['total_price'] = $objProductDetail->makeUpPrice($item['product']['price'] * $item['mount']);
			$item['product']['tprice_prase'] = $objProductDetail->makeUpPrice($item['product']['price']);
		}
		$total_price = $objProductDetail->makeUpPrice($total_price+$cart['total_ship']);
		$cart['total_ship'] = $objProductDetail->makeUpPrice($cart['total_ship']);
		unset($item);
		$cart['total_number'] = $objProductDetail->makeUpPrice($cart['total_number']);
	
		$this->view->cart = $cart;
		$this->view->total_price = $total_price;
        $this->view->allCard = $detail;
				$this->view->menu = array(
        		0=>'cart',
        );
        
		
		
	}

    

    
    public function changeStringAction()
    {
    	$objContent = new Models_Content();
    	$str = $this->_getParam("string","");
    	$str = $objContent->convert_vi_to_en($str);
    	$str = str_replace(" ", "-", trim($str));
    	echo $str;die;
    }
}