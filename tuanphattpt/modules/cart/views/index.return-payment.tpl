<div class="container">
    <div class=" content">
        <div class="col-md-12 col-sm-12 col-xs-12 p0">
            <div class="checkout col-md-12 col-sm-12 col-xs-12 p0">
                <h3>{{l}}Thông tin thanh toán{{/l}}</h3>
                {{if $message|@count > 0}}
                <div class="alert {{if $message.success == false}}alert-danger{{else}}alert-success {{/if}} col-sm-12">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$message.message}}.
                </div>
                {{/if}}
            </div>
            <div class="checkout col-md-12 col-sm-12 col-xs-12 p0">
                <div class="product-cart cart col-md-6 col-sm-12 col-xs-12">
                    <p class="checkout-title">
                        1. {{l}}Sản phẩm trong giỏ{{/l}}
                    </p>
                    <form method="post" action="">
                        {{foreach from=$allCard item=item key=key}}
                        <div class="checkout-product col-md-12 col-sm-6 col-xs-12">
                            <div class="checkout-product-img col-md-3 col-sm-4 col-xs-4">
                                <img src="{{$BASE_URL}}{{$item.product.thumb_image}}"/>
                            </div>
                            <div class="checkout-product-detail col-md-9 col-sm-8 col-xs-8">
                                <p class="checkout-product-name">
                                    {{$item.product.title}}
                                </p>
                                <p class="checkout-product-price">
                                    {{l}}Giá bán :{{/l}} {{$item.product.tprice_prase}}	x
                                    <input type="text" value="{{$item.mount}}" name="data[{{$key}}][mount]" >
                                    <a href="{{$BASE_URL}}cart/{{$key}}/remove.html" class="checkout-remove"><img src="{{$LAYOUT_HELPER_URL}}front/images/checkout-remove.png" /></a>
                                </p>
                                <p class="checkout-product-subtotal">
                                    {{l}}Thành tiền :{{/l}} {{$item.product.total_price}}
                                </p>
                            </div>
                        </div>
                        {{/foreach}}

                        <div class="grandtotal col-md-12 col-sm-12 col-xs-12">
                            {{l}}TỔNG THÀNH TIỀN :{{/l}} {{$total_price}} VNĐ
                        </div>
                    </form>
                </div>

                <div class="info-cart cart col-md-6 col-sm-12 col-xs-12">
                    <p class="checkout-title">
                        2. Thông tin thanh toán
                    </p>
                    <div class="checkout-info col-md-12 col-sm-12 col-xs-12">
                        <label>Vui lòng kiểm tra thông tin thanh toán</label>


                        <div class="form-group" style="padding: 0 10px;">
                            <label class="control-label col-sm-4">{{l}}Tên đơn hàng{{/l}}</label>
                            <div class="col-sm-8">
                                Cart_Order_20170421
                            </div>
                            <br class="cb">
                        </div>
                        <div class="form-group" style="padding: 0 10px;">
                            <label class="control-label col-sm-4">{{l}}Mã đơn hàng{{/l}}</label>
                            <div class="col-sm-8">
                                43534HRJRTJR56656YHRtrbyrty46%$b4
                            </div>
                            <br class="cb">
                        </div>
                        <div class="form-group" style="padding: 0 10px;">
                            <label class="control-label col-sm-4">{{l}}ID đơn hàng{{/l}}</label>
                            <div class="col-sm-8">
                                21335151512512421421412412
                            </div>
                            <br class="cb">
                        </div>
                        <div class="form-group" style="padding: 0 10px;">
                            <label class="control-label col-sm-4">{{l}}Tổng thanh toán{{/l}}</label>
                            <div class="col-sm-8">
                                1,200,000 VNĐ
                            </div>
                            <br class="cb">
                        </div>
                        <div class="form-group" style="padding: 0 10px;">
                            <label class="control-label col-sm-4">{{l}}Ghi chú{{/l}}</label>
                            <div class="col-sm-8">
                                Payment success!
                            </div>
                            <br class="cb">
                        </div>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>	
    </div>
</div>
<!--		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwSO_eqXfBtw1xN2arwYbbU2n-CuN3Gi8" type="text/javascript"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBLozyHdzhBfYMVFJKvYlPOMok4P15pLr8"></script>
<style>
    #pac-input{
        left: 0px !important;
    }
    .gmnoprint{
        display: none !important;
    }
</style>
<script type="text/javascript">
    jQuery(document).ready(function () {
        initAutocomplete();
    });

    var gmarkers = [];

    function initAutocomplete() {

        //			    	var myLatLng = {lat:parseFloat($("#Event_lattitude").val()), lng: parseFloat($("#Event_longtitude").val())};

        if ($("#lat").val() != '' && $("#long").val() != '') {
            var myLatLng = {
                lat: parseFloat($("#lat").val()),
                lng: parseFloat($("#long").val())
            };
        } else {
            var myLatLng = {
                lat: 10.797840,
                lng: 106.692098
            };
        }

        var map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        for (i = 0; i < gmarkers.length; i++) {
            gmarkers[i].setMap(null);
        }
        if (navigator.geolocation) {
            browserSupportFlag = true;
            navigator.geolocation.getCurrentPosition(function (position) {

                initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(initialLocation);

                for (i = 0; i < gmarkers.length; i++) {
                    gmarkers[i].setMap(null);
                }
                $("#lat").val(position.coords.latitude);
                $("#long").val(position.coords.longitude);

                var myLatLng = {
                    lat: parseFloat($("#lat").val()),
                    lng: parseFloat($("#long").val())
                };
                var marker = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.DROP,
                    position: myLatLng
                });
                gmarkers.push(marker);


                $.ajax({
                    type: 'post',
                    url: 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + ',' + position.coords.longitude + '&sensor=true',
                    success: function (data) {

                        $('#pac-input').val(data['results']['1']['formatted_address']);

                    }
                });


            }, function () {
                handleNoGeolocation(browserSupportFlag);
            });
        }


        var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: myLatLng
        });
        gmarkers.push(marker);

        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // [START region_getplaces]
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {


                $("#lat").val(place.geometry.location.lat());
                $("#long").val(place.geometry.location.lng());




            });
            map.fitBounds(bounds);

            var myLatLng = {
                lat: parseFloat($("#lat").val()),
                lng: parseFloat($("#long").val())
            };



            map.setZoom(14);
            map.setCenter(new google.maps.LatLng(parseFloat($("#lat").val()), parseFloat($("#long").val())));

            for (i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);
            }

            var marker = new google.maps.Marker({
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: myLatLng
            });
            gmarkers.push(marker);


        });




    }
</script>