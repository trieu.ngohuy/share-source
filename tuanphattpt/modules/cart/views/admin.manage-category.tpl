


<div class="page-header">
	<h1>
		{{l}}List Cart{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}List All Cart{{/l}}
		</small>
	</h1>
</div>

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">
				
                
				<table id="simple-table" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<form id="top-search" name="search" method="post" action="">
								<th>{{l}}No.{{/l}}</th>
                                <th>{{l}}Fullname{{/l}}</th>
                                <th>{{l}}Phone{{/l}}</th>
                                <th>{{l}}Created{{/l}}</th>
                                <th class="center">
                                	{{l}}Status{{/l}}
<!--                                     	<a href="javascript:document.sortForm.submit();"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/save_16.png" alt="Save sorting"></a>-->
                                </th>
                                <th>{{l}}Vendor{{/l}}</th>
                                <th>{{l}}Giao Hàng{{/l}}</th>
                                <th>{{l}}Thanh Toán{{/l}}</th>
                                <th>{{l}}Grandtotal{{/l}}</th>
                                <th>{{l}}Ship Total{{/l}}</th>
	                            <th>{{l}}Time Ship{{/l}}</th>
                                <th>{{l}}Action{{/l}}</th>
                        	</form>
						</tr>
					</thead>

					<tbody>
							<form action="" method="post" name="sortForm">
								{{foreach from=$allCart item=item name=category key=key}}
									<tr>
		            					<td class="text-center">
											{{$key+1}}
										</td>
		            					<td class="text-center">
											{{$item.fullname}}
										</td>
		            					<td class="text-center">
		                                	{{$item.phone}}
		                                </td>
		                        		<td class="qty text-center">
		                               		{{$item.created_date}}
		                            	</td>
		                        		<td class="center">
		                        			{{if $item.enabled == 1}}
												{{l}}Chờ Duyệt Đơn{{/l}}
											{{elseif $item.enabled == 2}}
												{{l}}Đang Duyệt Đơn{{/l}}
											{{elseif $item.enabled == 3}}
												{{l}}Đã Duyệt Đơn{{/l}}
											{{/if}}
						            	</td>
						            	<td class="grandtotal text-center">{{$item.vfull_name}}</td>
						            	<td class="grandtotal text-center">
											{{if $item.htgh == 1}}
												Giao tận nhà
											{{else}}
												Đến Goods Shop để nhận hàng
											{{/if}}
										</td>
										<td class="grandtotal text-center">
											{{if $item.httt == 1}}
												Thanh toán tiền mặt khi nhận hàng
											{{elseif $item.httt == 2}}
												Trừ tiền vào tài khoản goods
											{{else}}
												Chuyển khoản ngân hàng
											{{/if}}
										</td>
		                				<td class="grandtotal text-center">{{$item.total_number}} VNĐ</td>
		                				<td class="grandtotal text-center">{{$item.total_ship}} VNĐ</td>
			                			<td class="grandtotal text-center">{{$item.time_ship}} {{l}}MINUTE{{/l}}</td>
		                				<td class="text-center">
		                                	<a href="{{$BASE_URL}}admin/cart/admin/detail/id/{{$item.cart_id}}" class="btn btn-danger btn-md" ><i class="fa fa-eye"></i></a>
		                                </td>
		        					</tr>
								{{/foreach}}
							</form>
					</tbody>
				</table>
				
				
			</div><!-- /.span -->
		</div><!-- /.row -->
		
		
		<div class="col-lg-12">
							<div class="form-group col-lg-4">
								<label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
								<div class="col-lg-6">
									<select id="action" class="form-control" >
		                                <option value=";">{{l}}Choose an action...{{/l}}</option>
		                                {{p name=delete_category module=content}}
		                                <option value="deleteCategory();">{{l}}Delete{{/l}}</option>
		                                {{/p}}
		                                <option value="enableCategory();">{{l}}Enable{{/l}}</option>
		                                <option value="disableCategory();">{{l}}Disable{{/l}}</option>
		                            </select>
		                        </div>
		                        <div class="col-lg-6">
	                            	<a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
	                            </div>
							</div>
							<div class="form-group col-lg-2">
								<label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
								<div class="col-lg-12">
									<form class="search" name="search" method="post" action="">
		                                <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
		                                    <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
		                                    <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
		                                    <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
		                                    <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
		                                    <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
		                                </select>
		                            </form>
		                        </div>
							</div>
							{{if $countAllPages > 1}}
							<div class="col-lg-6 pagination">
								{{if $first}}
	                            <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
	                            {{/if}}
	                            {{if $prevPage}}
	                            <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
	                            {{/if}}
	                            
	                            {{foreach from=$prevPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>
	                            
	                            {{foreach from=$nextPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            {{if $nextPage}}
	                            <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
	                            {{/if}}
	                            {{if $last}}
	                            <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
	                            {{/if}}
                            
							</div>
							{{/if}}
						</div>
					</div>
		
		
	</div>
</div>

<script language="javascript" type="text/javascript">
$(document).ready(function(){
    $('.close').click(function(){
		$(this).parent().hide( "slow");
    });
    $('.check-all').click(function(){
    	if(this.checked) { // check select status
            $('.allCategories').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.allCategories').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
});

function applySelected()
{
	var task = document.getElementById('action').value;
	eval(task);
}
function enableCategory()
{
    var all = document.getElementsByName('allCategories');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('Please choose an category');
    }
    window.location.href = '{{$APP_BASE_URL}}content/admin/enable-category/gid/' + tmp;
}

function disableCategory()
{
    var all = document.getElementsByName('allCategories');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('Please choose an category');
    }
    window.location.href = '{{$APP_BASE_URL}}content/admin/disable-category/gid/' + tmp;
}

function deleteCategory()
{
    var all = document.getElementsByName('allCategories');
    var tmp = '';
    var count = 0;
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
             count++;
        }
    }
    if ('' == tmp) {
        alert('Please choose an category');
        return;
    } else {
    	result = confirm('Are you sure you want to delete ' + count + ' category(s)?');
        if (false == result) {
            return;
        }
    }
    window.location.href = '{{$APP_BASE_URL}}content/admin/delete-category/gid/' + tmp;
}


function deleteACategory(id)
{
    result = confirm('Are you sure you want to delete this category?');
    if (false == result) {
        return;
    }
    window.location.href = '{{$APP_BASE_URL}}content/admin/delete-category/gid/' + id;
}
</script>