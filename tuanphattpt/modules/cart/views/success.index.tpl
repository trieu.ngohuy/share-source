<br /><br />
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Mua hàng thành công!!!</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="http://icons.iconarchive.com/icons/graphicloads/100-flat/256/cart-add-icon.png" align="" width="100" />
                        </div>
                        <div class="col-md-9">
                            <br />
                            <p>Chúc mừng bạn đã mua hàng thành công {{$config.Website}}</p>
                            <p>Chúng tôi sẽ gửi đơn hàng về mail mà bạn cung cấp. Cảm ơn!!!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>		
        <div class="col-md-3"></div>
    </div>
</div>