							
<div class="container">
    <div class="row">
        <div class="category-label col-md-12 col-sm-12 col-xs-12">
            {{$name_cate}}
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 category-col-right col-md-push-9">
            <div class="recommend-box col-md-12 col-sm-12">
                <div class="recommend-label col-md-12 col-sm-12">
                    {{l}}GỢI Ý CHO BẠN{{/l}}
                </div>
                <div class="recommend-content col-md-12 col-sm-12">

                    {{foreach from=$allFlasher item=item key=key}}
                    <div class="hot-news-item col-md-12 col-sm-6">
                        <img src="{{$BASE_URL}}{{$item.main_image}}">
                        <br class="cb">
                        <a href="{{$item.url}}">{{$item.intro_text}}</a>
                    </div>
                    {{/foreach}}

                </div>
            </div>
        </div>
        <div class="category-col-left product-list col-md-9 col-sm-12 col-xs-12 col-md-pull-3">
            <div class="page-title text-center">
                <h1>{{l}}Quản Lý Đơn Hàng{{/l}}</h1>
            </div>

            {{if $messageCart|@count > 0}}
            <div class="alert {{if $messageCart.success == false}}alert-danger{{else}}alert-success {{/if}} col-sm-12">
                <button class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>
                {{$messageCart.message}}.
            </div>
            {{/if}}		
            <table class="table custom-table table-striped">

                <thead>
                    <tr class="first last">
                        <th>{{l}}No.{{/l}}</th>
                        <th>{{l}}Vendor{{/l}}</th>
                        <th>{{l}}Created{{/l}}</th>
                        <th>{{l}}Status{{/l}}</th>
                        <th>{{l}}Grandtotal{{/l}}</th>
                        <th>{{l}}Ship Total{{/l}}</th>
                        <th>{{l}}Time Ship{{/l}}</th>
                        <th>{{l}}Action{{/l}}</th>
                    </tr>
                </thead>

                <tbody>

                    {{foreach from=$allCart item=item key=key}}
                    <tr>

                        <td class="text-center">
                            <a href="{{$BASE_URL}}cart/manager/detail/id/{{$item.cart_id}}" >{{$key+1}}</a>
                        </td>
                        <td class="text-center">
                            <a href="{{$BASE_URL}}cart/manager/detail/id/{{$item.cart_id}}" >{{$item.vfull_name}}</a>
                        </td>
                        <td class="qty text-center">
                            <a href="{{$BASE_URL}}cart/manager/detail/id/{{$item.cart_id}}" >{{$item.created_date}}</a>
                        </td>
                        <td class="subtotal text-center">
                            {{if $item.enabled == 1}}
                            {{l}}Chờ Duyệt Đơn{{/l}}
                            {{elseif $item.enabled == 2}}
                            {{l}}Đang Duyệt Đơn{{/l}}
                            {{elseif $item.enabled == 3}}
                            {{l}}Đã Duyệt Đơn{{/l}}
                            {{/if}}
                        </td>
                        <td class="grandtotal text-center"><a href="{{$BASE_URL}}cart/manager/detail/id/{{$item.cart_id}}" >{{$item.total_number}} VNĐ</a></td>
                        <td class="grandtotal text-center"><a href="{{$BASE_URL}}cart/manager/detail/id/{{$item.cart_id}}" >{{$item.total_ship}} VNĐ</a></td>
                        <td class="grandtotal text-center"><a href="{{$BASE_URL}}cart/manager/detail/id/{{$item.cart_id}}" >{{$item.time_ship}} {{l}}MINUTE{{/l}}</a></td>

                        <td class="text-center">
                            {{if $item.enabled == 1}}
                            <a href="javascript:deleteACart({{$item.cart_id}});" class="btn btn-danger btn-md" ><i class="fa fa-trash"></i></a>
                                {{/if}}
                        </td>
                    </tr>
                    {{/foreach}}

                </tbody>

            </table>
            <div class="text-right">
                {{if $countAllPages > 1}}	
                {{if $first}}
                <a href="?page=1" class="btn btn-default btn-md" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
                {{/if}}

                {{if $prevPage}}
                <a href="?page={{$prevPage}}" class="btn btn-default btn-md" title="{{l}}Previous Page{{/l}}">&laquo;</a>
                {{/if}}

                {{foreach from=$prevPages item=item key=key}}
                <a href="?page={{$item}}" class="btn btn-default btn-md" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                <a href="#" class="btn btn-danger btn-md"  title="{{$currentPage}}">{{$currentPage}}</a>

                {{foreach from=$nextPages item=item key=key}}
                <a href="?page={{$item}}" class="btn btn-default btn-md" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                {{if $nextPage}}
                <a href="?page={{$nextPage}}" class="btn btn-default btn-md" title="{{l}}Next Page{{/l}}">&raquo;</a>
                {{/if}}
                {{if $last}}
                <a href="?page={{$countAllPages}}" class="btn btn-default btn-md" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
                {{/if}}	

                {{/if}}	
            </div>

        </div>

    </div>
</div>

<script type="text/javascript">
    function deleteACart(id)
    {
        result = confirm('{{l}}Are you sure you want to delete this cart?{{/l}}');
                if (false == result) {
                    return;
                }
                window.location.href = '{{$BASE_URL}}cart/manager/delete/id/' + id;
            }
</script>

