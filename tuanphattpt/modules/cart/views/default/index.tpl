<div class="container">
    <div class=" content">
        <br />
        <!-- breadcrumb -->
        {{sticker name=common_breadcrumbs}}
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading no-line">
            <span class="page-heading-title2">{{l}}Giỏ hàng{{/l}}</span>
        </h2>
        <hr class="mg-t-5 mg-b-15">
        <!-- ../page heading-->
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>{{l}}1. Sản phẩm{{/l}}</b></div>
                    <div class="panel-body">
                        <form method="post" action="" >
                            {{foreach from=$allCard.cartDetail item=item key=key}}    
                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <img src="{{$item.product.main_image}}" />
                                </div>
                                <div class="col-xs-8 col-sm-4 col-md-8">
                                    <p class="pg-b-5"><b>{{$item.product.title}}</b> (<a href="{{$BASE_URL}}cart/{{$key}}/remove.html">{{l}}Xóa{{/l}}</a>)</p>                                    
                                    <div class="row pd-b-5">
                                        <div class="col-md-7 mg-t-5 pd-r-0">
                                            <p class="pg-b-5"><b>{{l}}Gía:{{/l}}</b> {{$item.product.tprice_prase}}{{$config.currency}}</p>
                                        </div>
                                        <div class="col-md-5 pd-l-0">
                                            <div class="row">
                                                <div class="col-md-6 mg-t-5">
                                                    <p><b>SL:</b></p>
                                                </div>
                                                <div class="col-md-6 pd-l-0">
                                                    <input style="width: 43px;" class="form-control" type="text" value="{{$item.mount}}" name="data[{{$key}}][mount]">    
                                                </div>     
                                            </div>
                                        </div>
                                    </div>
                                    <p><b>{{l}}Tổng:{{/l}}</b> {{$item.product.total_price}}{{$config.currency}}</p>                                    
                                </div>
                            </div>
                            <hr>
                            {{/foreach}}
                            <p style="text-align: right; font-size: 20px"><b>Tổng tiền:</b> {{$allCard.total_price}} {{$config.currency}}</p>
                            <br />
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-8" style="text-align: right;">
                                    <button type="submit" class="btn btn-danger">{{l}}Cập nhật{{/l}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <form method="post" action="" id="cart-form">
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading"><b>{{l}}2. Địa chỉ nhận hàng{{/l}}</b></div>
                        <div class="panel-body">

                            <p id="pFullname"><b>{{$tempLogUser.full_name}}</b></p>
                            <p id="pAdd">
                                {{foreach from=$tempLogUser.add item=item key=key}}
                                {{if $key == $tempLogUser.delivery_add}}
                                {{$item}}
                                {{/if}}
                                {{/foreach}}	
                            </p>
                            <hr class="mg-t-5 mg-b-10">
                            <div id="divlistAdd">
                                <select class="form-control" name="cart['delivery_add']" id="comlistAdd">
                                    {{foreach from=$tempLogUser.add item=item key=key}}
                                    a
                                    <option value="{{$key}}" {{if $key == $tempLogUser.delivery_add}} selected="" {{/if}}>{{$item}}</option>    
                                    {{/foreach}}
                                </select>
                            </div>
                            <br />
                            <div class="cart-btn">
                                <a href="javascript:void()" id="linChooseAdd" class="btn btn-info">{{l}}Chọn lại{{/l}}</a>                                                                
                                <a href="javascript:void()" id="linNewAdd" class="btn btn-success">{{l}}Tạo mới{{/l}}</a>
                                <a href="javascript:void()" id="linChange" class="btn btn-warning">{{l}}Thay đổi{{/l}}</a>                                
                                <!--a href="javascript:void()" id="linDelete" class="btn btn-danger">{{l}}Xóa{{/l}}</a-->
                            </div>
                            <br />
                            <div class="">
                                <p><b>{{l}}Nhận hàng tại C-mart hoặc Chi nhánh{{/l}}</b></p><br />
                                <select class="form-control" id="cart_branch" name="branch">
                                    <option value="">{{l}}--Chọn chi nhánh{{/l}}</option>
                                    {{foreach from=$listSeller item=item}}
                                    <option value="{{$item.user_id}}" data-phone="{{$item.phone}}" data-fullname="{{$item.full_name}}">{{$item.full_name}}</option>
                                    {{/foreach}} 
                                </select>
                                <div id="branch-info">
                                    <br class="cb"> 
                                    <input id="branch_fullname" type="text" name="branch[full_name]" value="" style="display: none;"> 
                                    <span id="branch-fullname">
                                        ---
                                    </span>
                                    <br class="cb"> 
                                    <input id="branch_phone" type="text" name="branch[phone]" value="" style="display: none;"> 
                                    <span id="branch-phone">
                                        ---
                                    </span>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading"><b>{{l}}3. Chọn hình thức thanh toán{{/l}}</b></div>
                        <div class="panel-body">
                            <label>
                                <input type="radio" name="payment[method]"
                                       id="checkout-payment" value="Pay on received" checked> <img src="http://www.iconninja.com/files/610/982/521/money-dollar-cash-price-bills-payment-exchange-icon.svg"
                                       width="30px" /> {{l}}Pay on received{{/l}}
                            </label>                        
                            <hr>
                            <label>
                                <input class="visa-card" type="radio" name="payment[method]"
                                       id="checkout-payment" value="Visa card , Master card, JCB" > <img src="https://cdn2.iconfinder.com/data/icons/financial-circle/512/credit_card-512.png"
                                       width="30px" /> Visa card , Master card, JCB 
                            </label>
                            <div class="visa-form" style="display: none;">
                                <br/>
                                <div class="row" >
                                    <label class="col-sm-4">{{l}}Card number{{/l}}</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control col-sm-8"
                                               placeholder="{{l}}Type your card seri number{{/l}}"
                                               name="payment[cart_number]"
                                               value="{{$payment[cart_number]}}"> </div>                                
                                </div>
                                <br />
                                <div class="row" >
                                    <label class="col-sm-4">{{l}}Card holder{{/l}}</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control col-sm-8"
                                               placeholder="{{l}}Type your card holder full name{{/l}}"
                                               name="payment[cart_holder]"
                                               value="{{$payment[cart_holder]}}"> </div>                                
                                </div>
                                <br />
                                <div class="row">
                                    <label class="col-sm-4">{{l}}Due date{{/l}}</label>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control col-sm-8"
                                                       name="payment[month]"
                                                       value="{{$payment[month]}}">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control col-sm-8"
                                                       name="payment[year]"
                                                       value="{{$payment[year]}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                            </div>
                            <hr>
                            <label>
                                <input type="radio" name="payment[method]"
                                       id="checkout-payment" value="ATM card with internet banking" > <img src="https://cdn2.iconfinder.com/data/icons/financial-circle/512/banking_cards-512.png"
                                       width="30px" /> {{l}}ATM card with
                                internet banking{{/l}} </label>
                            <hr>
                            <label>
                                <input type="radio" name="payment[method]"
                                       id="checkout-payment" value="123Pay" > <img src="http://uploads.webflow.com/5330afa79cfe20d6750002ec/53287fd6edf098b121000049_graphicMobilePaymentsLargeRet.png"
                                       width="30px" /> {{l}}123Pay{{/l}}
                            </label>
                        </div>
                    </div>
                </div>
        </div>
        <br />
        <div class="row cart-button">
            <div class="col-sm-offset-2 col-sm-10 text-right">
                <button type="submit" class="btn btn-primary" >{{l}}Continue{{/l}}</button> <a type="submit"
                                                                                               class="btn btn-default" href="#">{{l}}Remove Card{{/l}}</a>				</div>
        </div>        
        </form>
    </div>
</div>
</div>
<!--		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwSO_eqXfBtw1xN2arwYbbU2n-CuN3Gi8" type="text/javascript"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBLozyHdzhBfYMVFJKvYlPOMok4P15pLr8"></script>
<style>
    #pac-input {
        left: 0px !important;
    }
    .gmnoprint {
        display: none !important;
    }
    .form-group label {
        font-size: 12px;
    }
</style>

<!--Modal: Edit delivery add-->
<div id="modalEditAdd" class="modal fade" role="dialog" data-backdrop="static" >
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Thay đổi địa chỉ giao hàng !!! </b></h4>
            </div>
            <form method="post" action="" id="formEditAdd">
                <div class="modal-body edit-add-body">      	
                    <div class="row">		  
                        <div class="col-sm-3 col-md-3 " style="margin-top: 6px;"><label for="example-text-input" class="col-form-label"><b>Họ và tên: </b></label></div>
                        <div class="col-sm-9 col-md-9">
                            <input class="form-control" type="text" id="modalFullname" required="" value="">
                        </div>
                    </div>
                    <br />
                    <div class="row">		  
                        <div class="col-sm-3 col-md-3 " style="margin-top: 6px;"><label for="example-text-input" class="col-form-label"><b>Số điện thoại: </b></label></div>
                        <div class="col-sm-9 col-md-9">
                            <input class="form-control" type="text" id="modalPhone" required="" value="">
                        </div>
                    </div>
                    <br />
                    <div class="row">		  
                        <div class="col-sm-3 col-md-3 " style="margin-top: 6px;"><label for="example-text-input" class="col-form-label"><b>Số nhà: </b></label></div>
                        <div class="col-sm-9 col-md-9">
                            <input class="form-control" type="text" value="" id="modalHouseNumber" required="">
                        </div>
                    </div>
                    <br />
                    <div class="row">		  
                        <div class="col-sm-3 col-md-3 " style="margin-top: 6px;"><label for="example-text-input" class="col-form-label"><b>Đường: </b></label></div>
                        <div class="col-sm-9 col-md-9">
                            <input class="form-control" type="text" value="" id="modalStreet" required="">
                        </div>
                    </div>
                    <br />
                    <div class="row">		  
                        <div class="col-sm-3 col-md-3 " style="margin-top: 6px;"><label for="example-text-input" class="col-form-label"><b>Tỉnh / TP: </b></label></div>
                        <div class="col-sm-9 col-md-9">
                            <select class="form-control" id="modalProvince">

                            </select>
                        </div>
                    </div>
                    <br />
                    <div class="row">		  
                        <div class="col-sm-3 col-md-3 " style="margin-top: 6px;"><label for="example-text-input" class="col-form-label"><b>Quận / Huyện: </b></label></div>
                        <div class="col-sm-9 col-md-9">
                            <select class="form-control" id="modalDistrict">

                            </select>
                        </div>
                    </div>
                    <br />
                    <div class="row">		  
                        <div class="col-sm-3 col-md-3 " style="margin-top: 6px;"><label for="example-text-input" class="col-form-label"><b>Phường / xã: </b></label></div>
                        <div class="col-sm-9 col-md-9">
                            <select class="form-control" id="modalWard">

                            </select>
                        </div>
                    </div>
                    <br />
                    <div class="row">		  
                        <div class="col-sm-3 col-md-3 " style="margin-top: 6px;"><label for="example-text-input" class="col-form-label"><b>Loại địa chỉ: </b></label></div>
                        <div class="col-sm-9 col-md-9">
                            <label class="radio-inline"><input type="radio" name="modalWorkingPlace" value="0" checked="" id="radHome">Nhà riêng</label>
                            <label class="radio-inline"><input type="radio" name="modalWorkingPlace" value="1" id="radWork">Nơi làm việc</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Lưu thay đổi</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </form>

        </div>

    </div>
</div>
<!--Modal: Progressing-->
<!-- Modal -->
<div id="modalProgressing" class="modal fade" role="dialog" data-backdrop="static" >
    <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Thông báo!!!</b></h4>
            </div>
            <div class="modal-body progressing-body">
                <p>Đang lưu.....</p>
            </div>
        </div>

    </div>
</div>
<!--Modal: Message-->
<div id="cartModal" class="modal fade" role="dialog" >
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Thông báo!!!</b></h4>
            </div>
            <div class="modal-body cart-body">
                <p>Chọn chi nhánh trước khi tiếp tục!!!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>

    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
    var listProvinces = [];
    var editMode = 0;
    var listAddType = [];
    var userInfo = {};
    jQuery(document).ready(function () {
        //CONVERT SMART ARRAY TO JAVASCRIPT ARRAY
    {{foreach from=$listProvinces item=item key=key}}
        listProvinces['{{$key}}'] = {
            province_id: '{{$item.province_id}}',
            name: '{{$item.name}}',
            type: '{{$item.type}}',
            parent_id: '{{$item.parent_id}}'
        };
    {{/foreach}}

        //ARRAY ADD TYPES
    {{if $tempLogUser.add_type|@count >0}}
    {{foreach from=$tempLogUser.add_type item=item key=key}}
        listAddType['{{$key}}'] = '{{$item}}';
    {{/foreach}}
    {{/if}}

        //USER INFO
        userInfo['full_name'] = '{{$tempLogUser.full_name}}';
        userInfo['phone'] = '{{$tempLogUser.phone}}';

        //HIDE BRANCH INFO		
        $("#branch-info").hide();

        //CHECK OUT CART
        $("#cart-form").submit(function (e) {
            if ({{$allCard.cartDetail|@count}} <= 0) {
                $(".cart-body").html("Không có sản phẩm trong giỏ hàng!!!");
                $('#cartModal').modal('toggle');
                return false;
            } else if ($("#cart_branch").find('option:selected').val() == "") {
                $(".cart-body").html("Chọn chi nhánh trước khi tiếp tục!!!");
                $('#cartModal').modal('toggle');
                return false;
            } else if ($('#comlistAdd option').size() <= 0) {
                $(".cart-body").html("Tạo địa chỉ nhận hàng trước khi tiếp tục!!!");
                $('#cartModal').modal('toggle');
                return false;
            }

        });
        //Chose another delivery address
        $("#linChooseAdd").click(function () {            
            
            $("#divlistAdd").css("display", "block");
        });
        //Change delivery address
        $("#linChange").click(function () {
            $(".modal-title").html("<b>{{l}}Thay đổi địa chỉ nhận hàng{{/l}}</b>");
            //Update mode
            editMode = 1;

            $("#divlistAdd").css("display", "none");
            $("#modalEditAdd").modal("show");

            //Get list add selected text
            var text = $("#comlistAdd").find('option:selected').text();
            //Convert add to array
            text = text.split(",");

            //Set full name and phone
            $("#modalFullname").val(userInfo['full_name']);
            $("#modalPhone").val(userInfo['phone']);

            //Set address type
            for (var i = 0; i < listAddType.length; i++) {
                var temp = $("#comlistAdd").find('option:selected').val();
                if (temp == i) {
                    if (listAddType[i] == 1) {
                        $("#radWork").prop("checked", true);
                        $("#radHome").prop("checked", false);
                    } else {
                        $("#radWork").prop("checked", false);
                        $("#radHome").prop("checked", true);
                    }

                }
            }

            //Set to form	  
            if (text.length >= 5) {
                DisplayCombobox("modalProvince", text[4], 1);
            }
            if (text.length >= 4) {
                DisplayCombobox("modalDistrict", text[3], 2);
            }
            if (text.length >= 3) {
                DisplayCombobox("modalWard", text[2], 3);
            }
            if (text.length >= 2) {
                $("#modalStreet").val(text[1]);
            }
            if (text.length >= 1) {
                $("#modalHouseNumber").val(text[0]);
            }
        });
        //New delivery add
        $("#linNewAdd").click(function () {
            $(".modal-title").html("<b>{{l}}Thêm mới địa chỉ nhận hàng{{/l}}</b>");
            //Update mode
            editMode = 2;

            //Show modal
            $("#modalEditAdd").modal("show");

            //Set full name and phone
            $("#modalFullname").val(userInfo['full_name']);
            $("#modalPhone").val(userInfo['phone']);

            //Reset data in modal
            $("#modalHouseNumber").val("");
            $("#modalStreet").val("");
            DisplayCombobox("modalProvince", "", 1);
            DisplayCombobox("modalDistrict", "", 2);
            DisplayCombobox("modalWard", "", 3);
        });

        //Form update delivery address submit
        $("#formEditAdd").submit(function (event) {
            event.preventDefault();
            UpdateDeliveryAdd();
        });

        //Select list add
        $('#comlistAdd').on('change', function () {
            //Update select delivery add
            SelectDeliveryAdd();
        });

        //Select combobox provinces
        $('#modalProvince').on('change', function () {
            DisplayCombobox("modalDistrict", "", 2);
            DisplayCombobox("modalWard", "", 3);
        });

        //Select combobox distric
        $('#modalDistrict').on('change', function () {
            DisplayCombobox("modalWard", "", 3);
        });
    });

    //FUNCTION: DISPLAY COMBOBOX
    function DisplayCombobox(comName, value, type) {
        var isExist = false;
        //Fill data to combobox provinces
        $('#' + comName + ' option').remove();
        $.each(listProvinces, function (i, item) {
            if (item.type == type) {
                if (item.name == value)
                {
                    isExist = true;
                }
                if (type == 1) {
                    $('#' + comName).append($('<option>', {
                        value: item.province_id,
                        text: item.name
                    }));
                } else if (type == 2 && item.parent_id == $("#modalProvince").find('option:selected').val()) {
                    $('#' + comName).append($('<option>', {
                        value: item.province_id,
                        text: item.name
                    }));
                } else if (type == 3 && item.parent_id == $("#modalDistrict").find('option:selected').val()) {
                    $('#' + comName).append($('<option>', {
                        value: item.province_id,
                        text: item.name
                    }));
                }

            }

        });
        //If exist
        if (isExist) {
            //Set selected provinces
            $("#" + comName + " option:contains(" + value + ")").attr("selected", "selected");
        }
    }
    /**
     * Update select delivery add
     */
    function SelectDeliveryAdd() {
        //Open modal progressing
        $("#modalProgressing").modal("show");
        var data = {
            user_id: '{{$tempLogUser.user_id}}',
            delivery_add: $("#comlistAdd").find('option:selected').val()
        };
        $.ajax({
            type: 'post',
            url: '{{$APP_BASE_URL}}cart/index/select-delivery-add',
            data: (data),
            success: function (response) {
                response = JSON.parse(response);
                //Close modal progress
                $("#modalProgressing").modal("hide");
                //Close modal new add
                $("#modalEditAdd").modal("hide");
                if (response.status == false) {
                    //Show error message
                    $(".cart-body").html("Có lỗi xảy ra!!! <br />");
                    $('#cartModal').modal('show');
                } else {
                    //Update delivery add
                    $("#pAdd").html($("#comlistAdd").find('option:selected').text());
                }
            },
            error: function (xhr, status, error) {
                //Close modal progress
                $("#modalProgressing").modal("hide");
                //Close modal new add
                $("#modalEditAdd").modal("hide");
                //Show error message
                $(".cart-body").html("Có lỗi xảy ra!!! <br />");
                $('#cartModal').modal('show');
            }
        });
    }
    /**
     * Create new or update delivery address and update user info
     */
    function UpdateDeliveryAdd() {
        //Open modal progressing
        $("#modalProgressing").modal("show");
        var data = {
            type: editMode,
            user_id: '{{$tempLogUser.user_id}}',
            full_name: $("#modalFullname").val(),
            phone: $("#modalPhone").val(),
            add: $("#modalHouseNumber").val() + "," + $("#modalStreet").val() + ","
                    + $("#modalWard").find('option:selected').text() + "," + $("#modalDistrict").find('option:selected').text() + ","
                    + $("#modalProvince").find('option:selected').text(),
            add_type: $('input[name=modalWorkingPlace]:checked').val(),
        };
        $.ajax({
            type: 'post',
            url: '{{$APP_BASE_URL}}cart/index/update-delivery-add',
            data: (data),
            success: function (response) {
                response = JSON.parse(response);
                //Close modal progress
                $("#modalProgressing").modal("hide");
                //Close modal new add
                $("#modalEditAdd").modal("hide");
                if (response.status == false) {
                    //Show error message
                    $(".cart-body").html("Có lỗi xảy ra!!! <br />");
                    $('#cartModal').modal('show');
                } else {
                    if (editMode == 1) {
                        //Update combobox
                        $("#comlistAdd").find('option:selected').text(data['add']);
                        //Update lable add
                        $("#pAdd").html($("#comlistAdd").find('option:selected').text());

                        //Update list add type
                        for (var i = 0; i < listAddType.length; i++) {
                            var temp = $("#comlistAdd").find('option:selected').val();
                            if (temp == i) {
                                listAddType[i] = data['add_type'];
                            }
                        }
                    } else {
                        //Add new add to com list add
                        var index = $('#comlistAdd option').size();
                        $('#comlistAdd').append($('<option>', {
                            value: index,
                            text: data['add']
                        }));
                        //Add new add type
                        listAddType.push(parseInt(data['add_type']));

                        //Check if empty list add then add new item as selected add
                        if ({{$tempLogUser.add|@count}} <= 0) {
                            $("#pAdd").html($("#comlistAdd").find('option:selected').text());
                        }
                    }

                    //Update full name lable
                    $("#pFullname").html("<b>" + data['full_name'] + "</b>");
                    //Update user info
                    userInfo['full_name'] = data['full_name'];
                    userInfo['phone'] = data['phone'];

                }
            },
            error: function (xhr, status, error) {
                //Close modal progress
                $("#modalProgressing").modal("hide");
                //Close modal new add
                $("#modalEditAdd").modal("hide");
                //Show error message
                $(".cart-body").html("Có lỗi xảy ra!!! <br />");
                $('#cartModal').modal('show');
            }
        });
    }

    /*JQUERY: Click visa card*/
    $('#cart_branch').on('change', function () {
        if ($('option:selected', this).val() == "")
        {
            $("#branch-info").hide();
        } else {
            $("#branch-info").show();
            $("#branch-phone").html("Phone: " + $('option:selected', this).attr('data-phone'));
            $("#branch_phone").val("Dolly Duck");
            $("#branch-fullname").html("Full name: " + $('option:selected', this).attr('data-fullname'));
            $("#branch_fullname").val("ff");
        }


    });
    $('input:radio[name="payment[method]"]').change(function () {
        var value = $(this).val();
        if (value != "Visa card , Master card, JCB") {
            $(".visa-form").css("display", "none");
        } else {
            $(".visa-form").css("display", "block");
        }
    });
</script>