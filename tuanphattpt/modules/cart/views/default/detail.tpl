<div class="container">
    <div class=" content">
        <br />
        <!-- breadcrumb -->
        {{sticker name=common_breadcrumbs}}
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading no-line">
            <span class="page-heading-title2">{{l}}Giỏ hàng{{/l}}</span>
        </h2>
        <hr>
        <!-- ../page heading-->
        <form method="post" action="{{$BASE_URL}}cart/index/update-cart" id="frmCartDetail">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-5">Sản phẩm</div>
                        <div class="col-md-2 alg-c">Đơn giá</div>
                        <div class="col-md-2 alg-c">
                            Số lượng 
                            <i class="fa fa-save achNumberSave" ></i>
                        </div>
                        <div class="col-md-2 alg-c">Thành tiền</div>
                        <div class="col-md-1 alg-c">Action</div>
                    </div>
                </div>
                <div class="panel-body">
                    {{assign var='count' value='1'}}
                    {{foreach from=$allCard.cartDetail item=item key=key}}    
                    <div class="row">
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="{{$item.product.main_image}}" style="width: 100%;"/>
                                </div>
                                <div class="col-md-10">
                                    <h4><a href="{{$item.product.url}}"><b>{{$item.product.title}}</b></a></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 alg-c">
                            {{$item.product.tprice_prase}}{{$config.currency}}
                        </div>
                        <div class="col-md-2 alg-c">
                            <input type="number" class="form-control" value="{{$item.mount}}" name="data[{{$key}}][mount]">
                        </div>
                        <div class="col-md-2 alg-c">{{$item.product.total_price}}{{$config.currency}}</div>
                        <div class="col-md-1 alg-c">
                            <i class="fa fa-remove achRemove" data-index="{{$key}}" style="font-size:24px"></i>
                        </div>
                    </div>

                    <hr>

                    {{assign var='count' value=$count+1}}
                    {{/foreach}}
                </div>

            </div>
        </form>
        <br />
        <div class="row cart-button">
            <div class="col-sm-offset-2 col-sm-10 text-right">
                <a type="submit" class="btn btn-default" href="{{$BASE_URL}}">[Tiếp tục mua hàng]</a>				
                <a href="{{$BASE_URL}}cart/index" class="btn btn-primary">[Thanh toán]</a> 

            </div>
        </div>    
    </div>
</div>
</div>
<!--Boostrap modal-->
<!-- Modal -->
<div id="digMessage" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{l}}Thông báo!!!{{/l}}</h4>
            </div>
            <div class="modal-body alg-c">
                <p>{{l}}Đang lưu dữ liệu...{{/l}}</p>
                <img src="{{$LAYOUT_HELPER_URL}}front/assets/images/loading.gif" alt="Loading"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!--Style-->      
<!--Javascript-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
    /*
     * Update amount
     */
    $(".achNumberSave").click(function () {
        /*
         * Show loading modal
         */
        $("#digMessage").modal("show");
        //submit form
        setTimeout(function () {
            $('#frmCartDetail').submit();
        }, 3000);

    });
    /*
     * Remove product from cart
     */
    $(".achRemove").click(function () {
        /*
         * Show loading modal
         */
        $("#digMessage").modal("show");
        $(".modal-body p").html("Đang xóa sản phẩm khỏi giỏ hàng!!!");
        /*
         * Get product index
         */
        var index = $(this).data("index");
        //submit form
        setTimeout(function () {
            /*
             * Ajax remove product from cart
             */
            $.ajax({
                url: "{{$BASE_URL}}/cart/index/remove-product",
                type: "post",
                data: {index: index},
                success: function (response) {
                    //Reload page
                    location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                    $(".cmodal-body").html("Có lỗi xảy ra!!!");
                }


            });
        }, 3000);

    });

</script>