		<div class="container">
			<div class="row">
				<div class="category-label col-md-12 col-sm-12 col-xs-12">
					{{$name_cate}}
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12 category-col-right col-md-push-9">
					<div class="recommend-box col-md-12 col-sm-12">
						<div class="recommend-label col-md-12 col-sm-12">
							{{l}}GỢI Ý CHO BẠN{{/l}}
						</div>
						<div class="recommend-content col-md-12 col-sm-12">
						
							{{foreach from=$allFlasher item=item key=key}}
								<div class="hot-news-item col-md-12 col-sm-6">
									<img src="{{$BASE_URL}}{{$item.main_image}}">
									<br class="cb">
									<a href="{{$item.url}}">{{$item.intro_text}}</a>
								</div>
							{{/foreach}}
							
						</div>
					</div>
				</div>
				<div class="category-col-left product-list col-md-9 col-sm-12 col-xs-12 col-md-pull-3">
						<div class="page-title text-center">
                        	<h1>{{l}}Chi Tiết Đơn Hàng{{/l}}</h1>
                        </div>
		                        
		               	{{if $messageCart|@count > 0}}
	                        <div class="alert {{if $messageCart.success == false}}alert-danger{{else}}alert-success {{/if}} col-sm-12">
	                        	<button class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
									{{$messageCart.message}}.
							</div>
                        {{/if}}		
						<table class="table custom-table table-striped">
			                	
		                        <thead>
		                            <tr class="first last">
		                                <th>{{l}}Image{{/l}}</th>
		                                <th>{{l}}Product Name{{/l}}</th>
		                                <th>{{l}}Quantity{{/l}}</th>
		                                <th>{{l}}Subtotal{{/l}}</th>
		                                <th>{{l}}Grandtotal{{/l}}</th>
		                            </tr>
		                        </thead>
		                        
		                        <tbody>
		                        	
		                        	{{foreach from=$allCard item=item key=key}}
			                        	<tr>
			            					<td><a href="{{$item.product.url}}" title="{{$item.product.title}} - {{$item.product.ttype}}" class="product-image">
			                                	<img src="{{$BASE_URL}}{{$item.product.thumb_image}}" alt="{{$item.product.title}} - {{$item.product.ttype}}">
			                                </a></td>
			            					<td>
			                                	<a href="{{$item.product.url}}">{{$item.product.title}}</a>
			                                </td>
			                        		<td class="qty">
			                               		<div class="input-group">
			                                        <div class="text-muted">{{$item.mount}}</div>
			                                    </div><!-- /input-group -->
	                            	</td>
	                        		<td class="subtotal">{{$item.product.tprice_prase}} VNĐ</td>
	                				<td class="grandtotal">{{$item.product.total_price}} VNĐ</td>
	        					</tr>
	        				{{/foreach}}
        					
                    	</tbody>
			                    	
	                    	
	                    </table>
	                    <div class="text-right col-sm-8"></div>
		                    <div class="text-right col-sm-7"></div>
		                    <div class="text-right col-sm-5">
		                    		<table class="table table-cart-total" style="border: 0px ;" >
		                        		<tbody>
			                            	<tr>
			                                	<td class="col-sm-6 text-left" style="font-weight: bold;border: 0px;">{{l}}Price Total Product{{/l}}:</td>
			                                    <td class="text-left" style="border: 0px;">{{$cart.total_number}} VNĐ</td>
			                                </tr>
			                                <tr>
			                                	<td class="col-sm-6 text-left" style="font-weight: bold;border: 0px;">{{l}}Ship Price{{/l}}:</td>
			                                    <td class="text-left" style="border: 0px;">{{$cart.total_ship}} VNĐ</td>
			                                </tr>
			                            </tbody>
		                           	</table>
		                        	<table class="table table-cart-total" style="border-bottom: 0 none;" >
		                        		<tbody>
			                            	
			                                <tr>
			                                	<td class="col-sm-6 text-left" style="font-weight: bold;border-top: 1px solid #000;">{{l}}Total{{/l}}:</td>
			                                    <td class="text-left" style="border-top: 1px solid #000;">{{$total_price}} VNĐ</td>
			                                </tr>
			                            </tbody>
		                           	</table>
									
		                    </div>
				</div>
				
			</div>
		</div>

<script type="text/javascript">
	function deleteACart(id)
	{
	    result = confirm('{{l}}Are you sure you want to delete this cart?{{/l}}');
	    if (false == result) {
	        return;
	    }
	    window.location.href = '{{$BASE_URL}}cart/manager/delete/id/' + id;
	}
</script>
                        
                        