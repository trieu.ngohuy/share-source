


<div class="page-header">
	<h1>
		{{l}}Detail Cart{{/l}}
	</h1>
</div>

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">
				
				<div class="col-xs-4">
					<form action="" method="post" class="form-horizontal">
						<div class="form-group">
								<label class="control-label col-md-4">{{l}}Người Mua Hàng{{/l}}</label>
								<div class="col-md-8">
									{{$cart.fullname}}
								</div>
						</div>
						<div class="form-group">
								<label class="control-label col-md-4">{{l}}Điện Thoại Liên Hệ{{/l}}</label>
								<div class="col-md-8">
									{{$cart.phone}}
								</div>
						</div>
						<div class="form-group">
								<label class="control-label col-md-4">{{l}}Địa Chỉ Giao Hàng{{/l}}</label>
								<div class="col-md-8">
									{{$cart.address}}
								</div>
						</div>
						<div class="form-group">
								<label class="control-label col-md-4">{{l}}Yêu Cầu Khách Hàng{{/l}}</label>
								<div class="col-md-8">
									{{$cart.note}}
								</div>
						</div>
						<div class="form-group">
								<label class="control-label col-md-4">{{l}}Hình Thức Vận Chuyển{{/l}}</label>
								<div class="col-md-8">
									{{if $cart.htgh == 1}}
										Giao tận nhà
									{{else}}
										Đến Goods Shop để nhận hàng
									{{/if}}
								</div>
						</div>
						<div class="form-group">
								<label class="control-label col-md-4">{{l}}Phương Thức Thanh Toán{{/l}}</label>
								<div class="col-md-8">
									{{if $cart.httt == 1}}
										Thanh toán tiền mặt khi nhận hàng
									{{elseif $cart.httt == 2}}
										Trừ tiền vào tài khoản goods
									{{else}}
										Chuyển khoản ngân hàng
									{{/if}}
								</div>
						</div>
						
						<div class="form-group">
								<label class="control-label col-md-4">{{l}}Tổng Tiền Sản Phẩm{{/l}}</label>
								<div class="col-md-8">
									{{$cart.total_number}} VNĐ
								</div>
						</div>
						<div class="form-group">
								<label class="control-label col-md-4">{{l}}Tổng Tiền Ship{{/l}}</label>
								<div class="col-md-8">
									{{$cart.total_ship}} VNĐ
								</div>
						</div>
						<div class="form-group">
								<label class="control-label col-md-4">{{l}}Tổng Đơn Hàng{{/l}}</label>
								<div class="col-md-8">
									{{$total_price}} VNĐ
								</div>
						</div>
						<div class="form-group">
								<label class="control-label col-md-4">{{l}}Category{{/l}}</label>
								<div class="col-md-8">
										<select name="data[enabled]" >
	                        				<option value="1" {{if $cart.enabled == 1 }} selected="selected"{{/if}} >Chờ Duyệt Đơn</option>
	                        				<option value="2" {{if $cart.enabled == 2 }} selected="selected"{{/if}}>Đang Duyệt Đơn</option>
	                        				<option value="3" {{if $cart.enabled == 3 }} selected="selected"{{/if}}>Đã Duyệt Đơn</option>
	                        			</select>
								</div>
						</div>
						<div class="clearfix form-actions">
							<div class="col-md-12">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									{{l}}Save{{/l}}
								</button>
							</div>
						</div>
					</form>
                </div>
                <div class="col-xs-8">
					<table id="simple-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<form id="top-search" name="search" method="post" action="">
									<th>{{l}}Image{{/l}}</th>
	                                <th>{{l}}Product Name{{/l}}</th>
	                                <th>{{l}}Quantity{{/l}}</th>
	                                <th>{{l}}Subtotal{{/l}}</th>
	                                <th>{{l}}Grandtotal{{/l}}</th>
	                        	</form>
							</tr>
						</thead>
	
						<tbody>
								{{foreach from=$allCard item=item key=key}}
		                        	<tr>
		            					<td><a href="{{$item.product.url}}" target="_blank" title="{{$item.product.title}} - {{$item.product.ttype}}" class="product-image">
		                                	<img src="{{$BASE_URL}}{{$item.product.thumb_image}}" alt="{{$item.product.title}} - {{$item.product.ttype}}">
		                                </a></td>
		            					<td>
		                                	<a href="{{$item.product.url}}" target="_blank">{{$item.product.title}}</a>
		                                    <div class="text-muted">{{l}}Size{{/l}}: {{$item.product.ttype}}<br></div>
		                                </td>
		                        		<td class="qty">
		                               		<div class="input-group">
		                                        <div class="text-muted">{{$item.mount}}</div>
		                                    </div><!-- /input-group -->
		                            	</td>
		                        		<td class="subtotal">{{$item.product.tprice_prase}} VNĐ</td>
		                				<td class="grandtotal">{{$item.product.total_price}} VNĐ</td>
		        					</tr>
		        				{{/foreach}}
						</tbody>
					</table>
					
				</div>
				
				
			</div><!-- /.span -->
		</div><!-- /.row -->
		
		
					</div>
		
		
	</div>
</div>

<script language="javascript" type="text/javascript">
$(document).ready(function(){
    $('.close').click(function(){
		$(this).parent().hide( "slow");
    });
    $('.check-all').click(function(){
    	if(this.checked) { // check select status
            $('.allCategories').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.allCategories').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
});

function applySelected()
{
	var task = document.getElementById('action').value;
	eval(task);
}
function enableCategory()
{
    var all = document.getElementsByName('allCategories');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('Please choose an category');
    }
    window.location.href = '{{$APP_BASE_URL}}content/admin/enable-category/gid/' + tmp;
}

function disableCategory()
{
    var all = document.getElementsByName('allCategories');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('Please choose an category');
    }
    window.location.href = '{{$APP_BASE_URL}}content/admin/disable-category/gid/' + tmp;
}

function deleteCategory()
{
    var all = document.getElementsByName('allCategories');
    var tmp = '';
    var count = 0;
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
             count++;
        }
    }
    if ('' == tmp) {
        alert('Please choose an category');
        return;
    } else {
    	result = confirm('Are you sure you want to delete ' + count + ' category(s)?');
        if (false == result) {
            return;
        }
    }
    window.location.href = '{{$APP_BASE_URL}}content/admin/delete-category/gid/' + tmp;
}


function deleteACategory(id)
{
    result = confirm('Are you sure you want to delete this category?');
    if (false == result) {
        return;
    }
    window.location.href = '{{$APP_BASE_URL}}content/admin/delete-category/gid/' + id;
}
</script>