<div class="col-md-12 col-sm-12 col-xs-12 p0">
            <div class="checkout col-md-12 col-sm-12 col-xs-12 p0">
                {{if $message|@count > 0}}
                <div class="alert {{if $message.success == false}}alert-danger{{else}}alert-success {{/if}} col-sm-12">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$message.message}}.
                </div>
                {{/if}}
                <div class="product-cart cart col-md-4 col-sm-12 col-xs-12">
                    <p class="checkout-title">
                        1. {{l}}Sản phẩm trong giỏ{{/l}}
                    </p>
                    <form method="post" action="">
                        {{foreach from=$allCard item=item key=key}}
                        <div class="checkout-product col-md-12 col-sm-6 col-xs-12">
                            <div class="checkout-product-img col-md-3 col-sm-4 col-xs-4">
                                <img src="{{$item.product.main_image}}"/>
                            </div>
                            <div class="checkout-product-detail col-md-9 col-sm-8 col-xs-8">
                                <p class="checkout-product-name">
                                    {{$item.product.title}}
                                </p>
                                <p class="checkout-product-price">
                                    {{l}}Giá bán :{{/l}} {{$item.product.tprice_prase}}	x
                                    <input type="text" value="{{$item.mount}}" name="data[{{$key}}][mount]" >
                                    <a href="{{$BASE_URL}}cart/{{$key}}/remove.html" class="checkout-remove"><img src="{{$LAYOUT_HELPER_URL}}front/images/checkout-remove.png" /></a>
                                </p>
                                <p class="checkout-product-subtotal">
                                    {{l}}Thành tiền :{{/l}} {{$item.product.total_price}}
                                </p>
                            </div>
                        </div>
                        {{/foreach}}
                        <div class="col-sm-offset-2 col-sm-12 text-right" style="margin: 5px 0px;">
                            <button type="submit" class="btn btn-default btn-warning"style="float: right;background: #000;border: #000;border-radius:0px;">CẬP NHẬP</button>
                        </div>

                        <div class="grandtotal col-md-12 col-sm-12 col-xs-12">
                            {{l}}TỔNG THÀNH TIỀN :{{/l}} {{$total_price}} VNĐ
                        </div>
                    </form>
                </div>
                {{if $loggedUser != null}}
                <form class="form-horizontal" data-toggle="validator" role="form" action="" method="post" id="cart_submit" >
                    {{else}}

                    {{/if}}
                    <div class="info-cart cart col-md-4 col-sm-12 col-xs-12">
                    
                                        
                        <p class="checkout-title">
                            2. Thông tin người mua
                        </p>
                        <div class="row">
                            <label>
                                <input type="radio" name="cart[payment_method]" id="checkout-payment" value="1" checked>
                                <p>{{l}}Giao hàng tận nơi{{/l}}</p>
                            </label>
                            <div class="row">
                            </div>
                        </div>
                        <div class="row">
                            <label>
                                <input type="radio" name="cart[payment_method]" id="checkout-payment" value="1" checked>
                                <p>{{l}}Nhận hàng tại C-mart hoặc Chi nhánh{{/l}}</p>
                            </label>
                        </div>
                        {{if $loggedUser == ''}}
                        <form class="form-horizontal" data-toggle="validator" role="form"  method="post" action="{{$BASE_URL}}login.html?page=cart">
                            <div class="checkout-login col-md-12 col-sm-12 col-xs-12">
                                <label>Vui lòng đăng nhập</label>
                                <form role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="input-user" class="col-sm-4 control-label">Tên đăng nhập</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="input-user" value="" placeholder="{{l}}Nhập tên đăng nhập{{/l}}" name="username" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="input-password" class="col-sm-4 control-label">Mật khẩu</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control" id="input-password" type="password" value="" placeholder="{{l}}Nhập mật khẩu"{{/l}} name="password" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10 text-right">
                                            <!--													<a href="#">Quên mật khẩu?</a>-->
                                            <button type="submit" class="btn btn-default" >ĐĂNG NHẬP</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </form>
                        {{else}}
                        <div class="checkout-info col-md-12 col-sm-12 col-xs-12">
                            <label>Vui lòng kiểm tra thông tin tài khoản của bạn để xác nhận việc giao hàng?</label>


                            <div class="form-group" style="padding: 0 10px;">
                                <label class="control-label col-sm-4">{{l}}Full Name{{/l}}</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" placeholder="{{l}}Nhập họ tên{{/l}}" name="cart[fullname]" value="{{$loggedUser.full_name}}" >
                                </div>
                                <br class="cb">
                            </div>
                            <div class="form-group" style="padding: 0 10px;">
                                <label class="control-label col-sm-4">{{l}}Email{{/l}}</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control col-sm-8" placeholder="{{l}}Nhập email{{/l}}" name="cart[email]" value="{{$loggedUser.email}}" >
                                </div>
                                <br class="cb">
                            </div>
                            <div class="form-group" style="padding: 0 10px;">
                                <label class="control-label col-sm-4">{{l}}Address{{/l}}</label>
                                <div class="col-sm-8">
                                    <input id="pac-input"  type="text" class="form-control col-sm-8" placeholder="{{l}}Nhập địa chỉ{{/l}}" name="cart[address]"  value="" >
                                </div>
                                <br class="cb">
                            </div>
                            <div class="form-group" style="padding: 0 10px;">
                                <label class="control-label col-sm-4">{{l}}Phone{{/l}}</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control col-sm-8" id="phone_cart" placeholder="{{l}}Nhập điện thoại{{/l}}" name="cart[phone]"  value="{{$loggedUser.phone}}">
                                </div>
                                <br class="cb">
                            </div>
                            <div class="form-group" style="padding: 0 10px;">
                                <label class="control-label col-sm-4">{{l}}Note{{/l}}</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control col-sm-8" placeholder="{{l}}Nhập yêu cầu{{/l}}" name="cart[note]">{{$cart.note}}</textarea>
                                </div>
                                <br class="cb">
                            </div>
                            <!-- </form> -->
                        </div>
                        {{/if}}
                    </div>

                    <div class="method-cart cart col-md-4 col-sm-12 col-xs-12">
                        <p class="checkout-title">
                            3. Thanh toán và vận chuyển
                        </p>
                        <!-- <form role="form" class="form-horizontal"> -->
                        <div class="shipping-method col-md-12 col-sm-12 col-xs-12">
                            <label>Chọn phương thức giao hàng</label>
                            <!--div class="form-group">
                                <div class="col-sm-12">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="cart[htgh]" id="checkout-shipping" value="1" checked>
                                            <p>Giao tận nhà</p>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="cart[htgh]" id="checkout-shipping" value="2">
                                            <p>{{l}}Đến Goods Shop để nhận hàng{{/l}}</p>
                                        </label>
                                    </div>
                                </div>
                            </div-->
                            <div class="account-info">
                                <label>Thông tin chủ tài khoản</label>
                                <p><span>Họ tên: </span><b>{{$config.owner_fullname}}</b></p>
                                <p><span>Email: </span><b>{{$config.owner_email}}</b></p>
                                <p><span>Điện thoại: </span><b>{{$config.owner_phone}}</b></p>
                                <p><span>Địa chỉ: </span><b>{{$config.owner_address}}</b></p>
                            </div>
                        </div>
                        <div class="payment-method col-md-12 col-sm-12 col-xs-12">
                            <label>Chọn phương thức thanh toán</label>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="cart[httt]" id="checkout-payment" value="1" checked>
                                            <p>Thanh toán tiền mặt khi nhận hàng</p>
                                        </label>
                                    </div>
                                    <!--											<div class="radio">-->
                                    <!--												<label>-->
                                    <!--													<input type="radio" name="cart[httt]" id="checkout-payment" value="2">-->
                                    <!--													<p>Trừ tiền vào tài khoản goods</p>-->
                                    <!--												</label>-->
                                    <!--											</div>-->
                                    <!--											<div class="radio">-->
                                    <!--												<label>-->
                                    <!--													<input type="radio" name="cart[httt]" id="checkout-payment" value="3">-->
                                    <!--													<p>Chuyển khoản ngân hàng</p>-->
                                    <!--												</label>-->
                                    <!--											</div>-->
                                </div>
                            </div>
                            <div class="account-info">
                                <label>Thông tin chủ tài khoản</label>
                                <p><span>Họ tên: </span><b>{{$config.owner_fullname}}</b></p>
                                <p><span>Email: </span><b>{{$config.owner_email}}</b></p>
                                <p><span>Điện thoại: </span><b>{{$config.owner_phone}}</b></p>
                                <p><span>Địa chỉ: </span><b>{{$config.owner_address}}</b></p>
                            </div>
                            <div style="margin-bottom: 10px;"></div>
                            <div class="account-info">
                                <label>Tài khoản ngân hàng</label>
                                <p><span>Tài khoản: </span><b>{{$config.owner_bankfullname}}</b></p>
                                <p><span>Số tài khoản: </span><b>{{$config.owner_bankseri}}</b></p>
                                <p><span>Ngân hàng: </span><b>{{$config.owner_bankname}}</b></p>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="cart[httt]" id="checkout-payment" value="1" checked>
                                            <p>{{l}}Online Payment{{/l}}</p>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <br>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-11">
                                        <select style="margin-bottom: 20px;" class="form-control" id="payment_branch">
                                            <option value="">{{l}}--Chọn chi nhánh{{/l}}</option>
                                            {{foreach from=$listSeller item=item}}
                                            <option value="{{$item.user_id}}">{{$item.full_name}}</option>
                                            {{/foreach}}
                                        </select>
                                        <a id="nganluong" target="_blank" href="https://www.nganluong.vn/button_payment.php?receiver=trieu.ngohuy@gmail.com&product_name=Pay_Cart_201720421&price=1000&return_url=localhost:8080/webfrobussiness/cart/index/returnPayment/cart_id/20170421&comments=(Haha)"><img src="https://www.nganluong.vn/css/newhome/img/button/pay-lg.png"border="0" /></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    {{if $loggedUser != null}}
                    <div class="place-order text-center col-md-12 col-sm-12 col-xs-12">
                        <button type="submit" class="btn btn-default">GỬI ĐƠN HÀNG</button>
                        <!--button type="button" onclick="submitForm();" class="btn btn-default">GỬI ĐƠN HÀNG</button-->
                    </div>
                </form>
                {{else}}

                {{/if}}
                <script src="http://code.jquery.com/jquery-latest.min.js"
                type="text/javascript"></script>
                <script>
                    var payment_method = "";
                    $("#payment_branch").change(function () {
                        if ($("#checkout-payment").is(':checked')) {
                            alert("Check Online Payment before continue!");
                            return false;
                        }
                    });
                    $("#nganluong").click(function () {
                        if ($("#checkout-payment").is(':checked')) {
                            alert("Check Online Payment before continue!");
                            return false;
                        } 


                    });
                    function submitForm() {
                        var phone = $('#phone_cart').val();
                        var lat = $('#lat').val();
                        var long = $('#long').val();
                        if ($.trim(phone) == '') {
                            alert('{{l}}Vui Lòng Nhập Số Điện Thoại Để Chúng Tôi Liên Hệ{{/l}}');
                                    } else if ($.trim(lat) == '' || $.trim(long) == '') {
                                        alert('{{l}}Bạn Phải Chọn Địa Chỉ Từ Google Map Để Chúng Tôi Giao Hàng Và Tính Ship Tự Động Từ Hệ Thống Tránh Trường Hợp Mất Mát Dành Cho Quý Khách{{/l}}');
                                                } else {
                                                    $('#cart_submit').submit();
                                                }
                                            }
                </script>
            </div>
        </div>