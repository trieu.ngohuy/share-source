<?php
/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2009 visualidea.org
 * @license    http://license.visualidea.org
 * @version    v 1.0 2009-04-15
 */
require_once 'Nine/Model.php';
class Models_FeedbackComment extends Nine_Model
{ 
    protected $_primary = 'feedback_comment_id';
    /**
     * Let system know this is miltilingual table or not.
     * If this table has multilingual fields, Zend_Db_Table_Select object
     * will be inserted language condition automatically.
     * 
     * @var array
     */
    protected $_multilingualFields = array();
    
    public function __construct($config = array())
    {
        $this->_name = $this->_prefix . 'feedback_comment';
        return parent::__construct($config); 
    }
	
    public function getAllCommentsByFeedBackId( $feedbackId) {
		$select = $this->select()
                ->order(array('feedback_comment_id ASC'))
                ->where('feedback_id=?',$feedbackId);
        return $this->fetchAll($select)->toArray();
    }
    
}