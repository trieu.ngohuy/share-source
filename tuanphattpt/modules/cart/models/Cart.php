<?php
/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2009 visualidea.org
 * @license    http://license.visualidea.org
 * @version    v 1.0 2009-04-15
 */
require_once 'Nine/Model.php';
class Models_Cart extends Nine_Model
{ 
    protected $_primary = 'cart_id';
    /**
     * Let system know this is miltilingual table or not.
     * If this table has multilingual fields, Zend_Db_Table_Select object
     * will be inserted language condition automatically.
     * 
     * @var array
     */
    protected $_multilingualFields = array();
    
    public function __construct($config = array())
    {
        $this->_name = $this->_prefix . 'cart';
        return parent::__construct($config); 
    }    
	public function getAllCarts($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username'))
                //->join(array('v' => $this->_prefix . 'user'), 'c.vendor_id = v.user_id', array('vfull_name' => 'full_name'))
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
//        if (null != @$condition['user_id']) {
//            $select->where('c.user_id=?', $condition['user_id']);
//        }
//    	if (null != @$condition['vendor_id']) {
//            $select->where('c.vendor_id=?', $condition['vendor_id']);
//        }
        
        return $this->fetchAll($select)->toArray();
    }
	
	public function getAllCartsByIdUser($idUser)
    {
        $select = $this->select();
                
        if (null != @$idUser) {
        	$select->where('user_id=?', $idUser);
        }
        return $this->fetchAll($select)->toArray();
    }
	
    public function getFeedback( $feedbackId) {
    	$select = $this->select()
    			->where('feedback_id=?',$feedbackId);
    			
    	return @reset($this->fetchAll($select)->toArray());
    }
    
    
}