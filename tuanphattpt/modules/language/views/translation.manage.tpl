<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.treeview.js" type="text/javascript"></script>
<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/css/jquery.treeview.css" />
<div class="page-header">
	<h1>
		{{l}}Language{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}List All Translate Language{{/l}}
		</small>
	</h1>
</div>
                        
                        
                        {{if $translationMessage|@count > 0 && $translationMessage.success == true}}
		                        <div class="alert alert-success">
		                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
									{{$translationMessage.message}}.
								</div>
		                        {{/if}}
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">
			
				<div class="col-sm-3">
					<div class="widget-box">
						<div class="widget-header widget-header-flat">
							<h4 class="widget-title">{{l}}Access{{/l}}</h4>
						</div>
	
						<div class="widget-body">
							<div class="panel-body" style="display: block;">
									<ul id="module" class="filetree">
				                        <li><span class="module">{{l}}Layouts{{/l}}</span>
				                            <ul>
				                                {{foreach from=$allLayouts item=item}}
				                                <li><span class="file"><a href="javascript:chooseAccess('{{$item}}', 'Layout');">{{$item}}</a></span></li>
				                                {{/foreach}}
				                            </ul>
				                        </li>
				                        <li><span class="module">{{l}}Modules{{/l}}</span>
				                            <ul>
				                                {{foreach from=$allModules item=item}}
				                                <li><span class="file"><a href="javascript:chooseAccess('{{$item}}', 'Module');">{{$item}}</a></span></li>
				                                {{/foreach}}
				                            </ul>
				                        </li>
				                        <li><span class="module">{{l}}Stickers{{/l}}</span>
				                            <ul>
				                                {{foreach from=$allStickers item=item}}
				                                <li><span class="file"><a href="javascript:chooseAccess('{{$item}}', 'Sticker');">{{$item}}</a></span></li>
				                                {{/foreach}}
				                            </ul>
				                        </li>
				                    </ul>
							</div>
						</div>
					</div>
					
					
				</div>
				
				
				<form action="#" method="post"  name="translation" id="translation" onsubmit="submitForm();return false;">
				<div class="col-sm-9">
					<div class="widget-box">
						<div class="widget-header widget-header-flat">
							<h4 class="widget-title"><strong id="access_type">???</strong> >>> <strong id="access_name">???</strong></h4>
						</div>
	
						<div class="widget-body">
							<div class="panel-body content-box-content" style="display: block; " id="translation_box">
								<p>Please select access.</p>
							</div>
						</div>
					</div>
					
					
				</div>
				</form>
				
				
			</div>
			
		</div>
		
	</div>
	
</div>
           
<script language="javascript" type="text/javascript">
$(document).ready(function(){
    // second example
    $("#module").treeview();
});
var access = '';
var atype = '';
function chooseAccess(name, type) {
	access = name;
	atype = type;
    $("#access_name").html(name);
    $("#access_type").html(atype);
    loadTranslation();
}
function loadTranslation() {
	if ('' == atype || '' == access) {
		return;
	}
	$("#translation_box").html('Loading...');
	
	$.ajax({
	       type: "POST",
	       cache: false,
	       url: "{{$APP_BASE_URL}}language/translation/translate?r=" + Math.random(),
	       data: {
    		   'access': access,
    		   'atype': atype
		   },
	       success: function(msg){
			   $("#translation_box").html(msg);
			   $( 'html, body' ).animate( { scrollTop: 0 }, 0 );
//			   $('tbody > tr:odd').addClass("alt-row"); // Add class "alt-row" to even table rows // Add class "alt-row" to even table rows
	       }
	     });
	
}

function submitForm() {
	
	$.ajax({
	       type: "POST",
	       cache: false,
	       url: "{{$APP_BASE_URL}}language/translation/translate?r=" + Math.random(),
	       data: {
    		   'access': access,
    		   'atype': atype,
    		   'data' : jQuery("#translation").serialize()
		   },
		   beforeSend: function (a, o) {
			   $("#translation_box").html('Saving...');
			   $( 'html, body' ).animate( { scrollTop: 0 }, 'fast' );
		   },
	       success: function(msg){
			   $("#translation_box").html(msg);
//			   $('tbody > tr:odd').addClass("alt-row"); // Add class "alt-row" to even table rows // Add class "alt-row" to even table rows
	       }
	     });
	
}

</script>