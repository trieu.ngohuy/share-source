<?php

class Route_User
{
    /**
     * Build friendly URL
     * @param array $linkArr 0 => <module>, 1 => controller, 2 => action, 3...n param/value
     * @param array $params param => value
     *
     * @return string friendly URL
     */
    public function build($linkArr, $params = array())
    {

        $result = implode('/', $linkArr);

        /**
         * Link is detail
         */
        if ('profile' == @$linkArr[2]) {
            /**
             * Structure: pages/<id>/<alias>.html
             */
            $result = "member/{$linkArr[4]}/";
            if (null != $params['alias']) {
                $result .= urlencode($params['alias']);
            }

        }

        return $result;

    }

    /**
     * Parse friendly URL
     */
    public function parse()
    {
        $router = Nine_Controller_Front::getInstance()->getRouter();
        $route = new Zend_Controller_Router_Route_Regex(
            'member/([0-9]+)/(.*).html',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'profile'
            ),
            array(1 => 'id')
        );
        $router->addRoute('user', $route);


        $route1 = new Zend_Controller_Router_Route_Regex(
            'estore/edit-info/(.*).html',
            array(
                'module' => 'user',
                'controller' => 'estore',
                'action' => 'edit-info'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('user1', $route1);

        $route2 = new Zend_Controller_Router_Route_Regex(
            'estore/change-password/(.*).html',
            array(
                'module' => 'user',
                'controller' => 'estore',
                'action' => 'change-password'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('user2', $route2);

        $route3 = new Zend_Controller_Router_Route_Regex(
            'user/profile',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'profile'
            )
        );
        $router->addRoute('user3', $route3);

        $route4 = new Zend_Controller_Router_Route_Regex(
            'user/edit-info',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'edit-info'
            )
        );
        $router->addRoute('user4', $route4);

        $routecart = new Zend_Controller_Router_Route_Regex(
            'user/cart-manager',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'cart-manager'
            )
        );
        $router->addRoute('routecart', $routecart);

        $route5 = new Zend_Controller_Router_Route_Regex(
            'user/change-password',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'change-password'
            )
        );
        $router->addRoute('user5', $route5);

        $route6 = new Zend_Controller_Router_Route_Regex(
            'seller',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'seller'
            )
        );
        $router->addRoute('user6', $route6);

        $route7 = new Zend_Controller_Router_Route_Regex(
            'seller/search',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'seller'
            )
        );
        $router->addRoute('user7', $route7);

        $route8 = new Zend_Controller_Router_Route_Regex(
            'user/register',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'register'
            )
        );
        $router->addRoute('user8', $route8);

        $route9 = new Zend_Controller_Router_Route_Regex(
            'user/buying-request',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'buying-request'
            )
        );
        $router->addRoute('user9', $route9);

        $route10 = new Zend_Controller_Router_Route_Regex(
            'user/buying-request/(.*)',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'buying-request-detail'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('user10', $route10);

        $route11 = new Zend_Controller_Router_Route_Regex(
            'user/agree-quote/(.*)/(.*)',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'agree-quote'
            ),
            array(
                1 => 'gid',
                2 => 'category_gid'
            )
        );
        $router->addRoute('user11', $route11);

        $route12 = new Zend_Controller_Router_Route_Regex(
            'user/contact',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'contact'
            )
        );
        $router->addRoute('user12', $route12);

        $route13 = new Zend_Controller_Router_Route_Regex(
            'user/contact/([0-9]+)',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'contact-detail'
            ),
            array(
                1 => 'gid'
            )
        );
        $router->addRoute('user13', $route13);
        $route14 = new Zend_Controller_Router_Route_Regex(
            'user/upgrade',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'upgrade'
            ),
            array(
                1 => 'gid'
            )
        );
        $router->addRoute('user14', $route14);
        $route15 = new Zend_Controller_Router_Route_Regex(
            'user/manager-upgrade',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'manager-upgrade'
            ),
            array(
                1 => 'gid'
            )
        );
        $router->addRoute('user15', $route15);
        
        $route16 = new Zend_Controller_Router_Route_Regex(
            'user/upgrade-step1/([0-9]+)',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'upgrade-step1'
            ),
            array(
                1 => 'gid'
            )
        );
        $router->addRoute('user16', $route16);
        
        $route17 = new Zend_Controller_Router_Route_Regex(
            'user/upgrade-step2',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'upgrade-step2'
            ),
            array(
                1 => 'gid'
            )
        );
        $router->addRoute('user17', $route17);
        
        $route18 = new Zend_Controller_Router_Route_Regex(
            'user/upgrade-success',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'upgrade-success'
            ),
            array(
                1 => 'gid'
            )
        );
        $router->addRoute('user18', $route18);
        
        $route19 = new Zend_Controller_Router_Route_Regex(
            'user/payprocessqt',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'payprocessqt'
            ),
            array(
                1 => 'gid'
            )
        );
        $router->addRoute('user19', $route19);
        
        $route20 = new Zend_Controller_Router_Route_Regex(
            'user/payprocess',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'payprocess'
            ),
            array(
                1 => 'gid'
            )
        );
        $router->addRoute('user20', $route20);
        
        $route21 = new Zend_Controller_Router_Route_Regex(
            'user/contact-estore',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'contact-estore'
            )
        );
        $router->addRoute('user21', $route21);

        $route22 = new Zend_Controller_Router_Route_Regex(
            'user/contact-detail/([0-9]+)',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'contact-detail-estore'
            ),
            array(
                1 => 'gid'
            )
        );
        $router->addRoute('user22', $route22);
        
        $route23 = new Zend_Controller_Router_Route_Regex(
            'user/buying-request-reply',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'buying-request-reply'
            )
        );
        $router->addRoute('user23', $route23);

        $route24 = new Zend_Controller_Router_Route_Regex(
            'user/buying-request-reply/(.*)',
            array(
                'module' => 'user',
                'controller' => 'index',
                'action' => 'buying-request-reply-detail'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('user24', $route24);
    }
}