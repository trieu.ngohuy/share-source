<?php

/**
 * LICENSE
 *
 * [license information]
 *
 * @category   Nine
 * @copyright  Copyright (c) 2009 visualidea.org
 * @license    http://license.visualidea.org
 * @version    v 1.0 2009-04-15
 */
require_once 'Nine/Model.php';
require_once 'modules/content/models/Content.php';
require_once 'modules/banner/models/Banner.php';
require_once 'modules/product/models/ProductCategory.php';

class Models_User extends Nine_Model {

    /**
     * The primary key column or columns.
     * A compound key should be declared as an array.
     * You may declare a single-column primary key
     * as a string.
     *
     * @var mixed
     */
    protected $_primary = 'user_id';

    /**
     * Let system know this is miltilingual table or not.
     * If this table has multilingual fields, Zend_Db_Table_Select object
     * will be inserted language condition automatically.
     *
     * @var array
     */
    protected $_multilingualFields = array();

    /**
     * Constructor.
     *
     * Supported params for $config are:
     * - db              = user-supplied instance of database connector,
     *                     or key name of registry instance.
     * - name            = table name.
     * - primary         = string or array of primary key(s).
     * - rowClass        = row class name.
     * - rowsetClass     = rowset class name.
     * - referenceMap    = array structure to declare relationship
     *                     to parent tables.
     * - dependentTables = array of child tables.
     * - metadataCache   = cache for information from adapter describeTable().
     *
     * @param  mixed $config Array of user-specified config options, or just the Db Adapter.
     * @return void
     */
    public function __construct($config = array()) {
        $this->_name = $this->_prefix . 'user';
        return parent::__construct($config);
    }

    /**
     * Delete many users
     *
     * @param $userIds Array of userId want to delete
     * @return int|string Number of users have been deleted if success.
     *                    String of error if can not delete one of users
     */
    public function deleteUsers($userIds = array()) {
        $where = null;
        if (!empty($userIds)) {
            $where = "user_id = -1";
            foreach ($userIds as $userId) {
                if (is_int($userId)) {
                    $where .= " OR user_id = $userId";
                }
            }
        }
        try {
            return $this->delete($where);
        } catch (Exception $e) {
            return Nine_Language::translate('Can not delete users now');
        }
    }

    /**
     * Get all user's datas with group's information
     *
     * Honors the Zend_Db_Adapter fetch mode.
     *
     * @param string|array $order OPTIONAL An SQL ORDER clause.
     * @param int $count OPTIONAL An SQL LIMIT count.
     * @param int $offset OPTIONAL An SQL LIMIT offset.
     * @return array The row results per the Zend_Db_Adapter fetch mode.
     *
     * @example $this->getAllUsersWithGroup(array(),'user_id ASC', 10, 0);
     */
    public function getUserByCode($code) {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('u' => $this->_name))
                ->where('u.active_code=?', $code);

        return $this->fetchAll($select)->current()->toArray();
    }

    public function updateActive($userName) {
        return $this->update(array('enabled' => '1'), array('username = ?' => $userName));
    }

    public function getAllUsersWithGroupFrontend($condition = array(), $order = null, $count = null, $offset = null) {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('u' => $this->_name))
                ->join(array('g' => $this->_prefix . 'group'), 'u.group_id = g.group_id', array('gname' => 'name', 'gcolor' => 'color', 'genabled' => 'enabled'))
                ->joinLeft(array('pro' => $this->_prefix . 'product'), 'u.user_id = pro.user_id AND pro.lang_id = ' . Nine_Language::getCurrentLangId(), array('ptitle' => 'title'))
                ->joinLeft(array('p' => $this->_prefix . 'user_profile'), 'u.user_id = p.user_id AND p.lang_id = ' . Nine_Language::getCurrentLangId(), array('padd' => 'add', 'pcompany_name' => 'company_name', 'pnote' => 'note', 'pabout' => 'about', 'pproduct_chinh' => 'product_chinh', 'pproduct_khac' => 'product_khac'))
                ->where('pro.title IS NOT NULL')
                ->order($order)
                ->group('u.user_id')
                ->limit($count, $offset);
        /**
         * Conditions
         */
        if ("" != @$condition['ids']) {
            $select->where('u.user_id IN (' . trim($condition['ids'], ',') . ')');
        }
        if ("" != @$condition['name_company']) {
            $select->where($this->getAdapter()->quoteInto('p.company_name LIKE ?', "%{$condition['name_company']}%"));
        }

        if ("" != @$condition['username']) {
            $select->where($this->getAdapter()->quoteInto('u.username LIKE ?', "%{$condition['username']}%"));
        }
        if (null != @$condition['group_id']) {
            $select->where("u.group_id = ?", $condition['group_id']);
        }
        if (null != @$condition['product_category_gid'] && null == @$condition['get_user_buying']) {

            $select->where($this->getAdapter()->quoteInto('u.product_category_gid LIKE ?', "%||{$condition['product_category_gid']}||%"));
        }
        if (null != @$condition['product_category_gid'] && null != @$condition['get_user_buying']) {
            $sqlcate = "SELECT gid_string FROM 9_product_category Where product_category_gid = '" . $condition['product_category_gid'] . "' AND lang_id = '" . Nine_Language::getCurrentLangId() . "'";
            $idCatAll = '';
            $idcates = $this->_db->fetchAll($sqlcate);
            foreach ($idcates as $rows) {
                $idCatAll .= $rows['gid_string'] . ',';
            }
            if ($idCatAll == "") {
                return array();
            }
            $idCatAll = explode(",", trim($idCatAll, ','));
            $ids = array();

            foreach ($idCatAll as $rows) {
                if ($rows != '') {
                    $sqlcate = "SELECT user_id FROM 9_user Where product_category_gid LIKE '%||" . $rows . "||%' AND group_id = 3";
                    $id = $this->_db->fetchAll($sqlcate);
                    foreach ($id as $item) {
                        if (!in_array($item['user_id'], $ids)) {
                            $ids[] = $item['user_id'];
                        }
                    }
                }
            }

            $ids = implode(',', $ids);
            if ($ids != '') {
                $select->where('u.user_id IN (' . trim($ids, ',') . ')');
            } else {
                return array();
            }
        }
        if (null != @$condition['package_id']) {
            $select->where('u.package_id = ?', $condition['package_id']);
        }
        return $this->fetchAll($select)->toArray();
    }

    public function getAllUsersWithGroup($condition = array(), $order = null, $count = null, $offset = null) {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('u' => $this->_name))
                ->join(array('g' => $this->_prefix . 'group'), 'u.group_id = g.group_id', array('gname' => 'name', 'gcolor' => 'color', 'genabled' => 'enabled'))
                ->joinLeft(array('p' => $this->_prefix . 'user_profile'), 'u.user_id = p.user_id AND p.lang_id = ' . Nine_Language::getCurrentLangId(), array('padd' => 'add', 'pcompany_name' => 'company_name', 'pnote' => 'note', 'pabout' => 'about', 'pproduct_chinh' => 'product_chinh', 'pproduct_khac' => 'product_khac'))
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
        if ("" != @$condition['ids']) {
            $select->where('u.user_id IN (' . trim($condition['ids'], ',') . ')');
        }
        if ("" != @$condition['name_company']) {
            $select->where($this->getAdapter()->quoteInto('p.company_name LIKE ?', "%{$condition['name_company']}%"));
        }

        if ("" != @$condition['username']) {
            $select->where($this->getAdapter()->quoteInto('u.username LIKE ?', "%{$condition['username']}%"));
        }
        if (null != @$condition['group_id']) {
            $select->where("u.group_id = ?", $condition['group_id']);
        }
        if (null != @$condition['product_category_gid'] && null == @$condition['get_user_buying']) {

            $select->where($this->getAdapter()->quoteInto('u.product_category_gid LIKE ?', "%||{$condition['product_category_gid']}||%"));
        }
        if (null != @$condition['product_category_gid'] && null != @$condition['get_user_buying']) {
            $sqlcate = "SELECT gid_string FROM 9_product_category Where product_category_gid = '" . $condition['product_category_gid'] . "' AND lang_id = '" . Nine_Language::getCurrentLangId() . "'";
            $idCatAll = '';
            $idcates = $this->_db->fetchAll($sqlcate);
            foreach ($idcates as $rows) {
                $idCatAll .= $rows['gid_string'] . ',';
            }
            if ($idCatAll == "") {
                return array();
            }
            $idCatAll = explode(",", trim($idCatAll, ','));
            $ids = array();

            foreach ($idCatAll as $rows) {
                if ($rows != '') {
                    $sqlcate = "SELECT user_id FROM 9_user Where product_category_gid LIKE '%||" . $rows . "||%' AND group_id = 3";
                    $id = $this->_db->fetchAll($sqlcate);
                    foreach ($id as $item) {
                        if (!in_array($item['user_id'], $ids)) {
                            $ids[] = $item['user_id'];
                        }
                    }
                }
            }

            $ids = implode(',', $ids);
            if ($ids != '') {
                $select->where('u.user_id IN (' . trim($ids, ',') . ')');
            } else {
                return array();
            }
        }
        if (null != @$condition['package_id']) {
            $select->where('u.package_id = ?', $condition['package_id']);
        }
        return $this->fetchAll($select)->toArray();
    }

    public function getAllUsersWithGroupNotUser($condition = array(), $order = null, $count = null, $offset = null) {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('u' => $this->_name))
                ->join(array('g' => $this->_prefix . 'group'), 'u.group_id = g.group_id', array('gname' => 'name', 'gcolor' => 'color', 'genabled' => 'enabled'))
                ->joinLeft(array('p' => $this->_prefix . 'user_profile'), 'u.user_id = p.user_id AND p.lang_id = ' . Nine_Language::getCurrentLangId(), array('padd' => 'add', 'pcompany_name' => 'company_name', 'pnote' => 'note', 'pabout' => 'about', 'pproduct_chinh' => 'product_chinh', 'pproduct_khac' => 'product_khac'))
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
        if (null != @$condition['group_id']) {
            $select->where("u.group_id = ?", $condition['group_id']);
        }
        $sql = "SELECT user_id FROM {$this->_prefix}topsuplier_category WHERE lang_id = " . Nine_Language::getCurrentLangId();
        $ids = $this->_db->fetchAll($sql);
        if (empty($ids)) {
            return array();
        }
        $idString = '';
        foreach ($ids as $row) {
            $idString .= $row['user_id'] . ',';
        }
        /**
         * Add to select object
         */
        $select->where('u.user_id NOT IN (' . trim($idString, ',') . ')');
        return $this->fetchAll($select)->toArray();
    }

    public function getAllUsersWithGroupContent($condition) {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('u' => $this->_name))
                ->join(array('g' => $this->_prefix . 'group'), 'u.group_id = g.group_id', array('gname' => 'name', 'gcolor' => 'color', 'genabled' => 'enabled'));
        $select->where("u.group_id = ?", $condition);

        return $this->fetchAll($select)->toArray();
    }

    public function getUser($userId) {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('u' => $this->_name))
                ->joinLeft(array('p' => $this->_prefix . 'user_profile'), 'u.user_id = p.user_id AND p.lang_id = ' . Nine_Language::getCurrentLangId(), array('padd' => 'add', 'pcompany_name' => 'company_name', 'pnote' => 'note', 'pabout' => 'about', 'pproduct_chinh' => 'product_chinh', 'pproduct_khac' => 'product_khac'))
                ->where('u.user_id=?', $userId);

        return current($this->fetchAll($select)->toArray());
    }

    public function getUserWithGroup($userId) {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('u' => $this->_name))
                ->join(array('g' => $this->_prefix . 'group'), 'u.group_id = g.group_id', array('gname' => 'name', 'gcolor' => 'color', 'genabled' => 'enabled'))
                ->where('u.user_id=?', $userId);

        return $this->fetchAll($select)->current()->toArray();
    }

    public function countAllUsers($condition) {
        $countCondition = array();
        if (null != @$condition['group_id']) {
            $countCondition['group_id'] = $condition['group_id'];
        }
        if (null != @$condition['username']) {
            $countCondition['username LIKE ?'] = "%{$condition['username']}%";
        }

        return parent::count($countCondition);
    }

    /**
     * Update lastest login time of user
     *
     * @param  string $username The $userName
     * @return int              The number of rows updated
     */
    public function updateLastLogin($userName) {
        return $this->update(array('last_login_date' => time()), array('username = ?' => $userName));
    }

    /**
     * Get user by username
     *
     * @param  string $userName The username
     * @return Zend_Db_Table_Row|false  Return array of all information of user if success
     */
    public function getByUserName($userName) {
        return $this->getByColumns(array('username' => $userName))->current();
    }

    public function getByEmail($userName) {
        return $this->getByColumns(array('email' => $userName))->current();
    }

    public function getByUserId($userId) {

        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('u' => $this->_name))
                //->join(array('ct' => $this->_prefix . 'country_category'), 'u.country = ct.country_category_gid', array('ctname' => 'name', 'ctimage' => 'images'))
                ->joinLeft(array('p' => $this->_prefix . 'user_profile'), 'u.user_id = p.user_id AND p.lang_id = ' . Nine_Language::getCurrentLangId(), array('padd' => 'add', 'pcompany_name' => 'company_name', 'pnote' => 'note', 'pabout' => 'about', 'pproduct_chinh' => 'product_chinh', 'pproduct_khac' => 'product_khac'))                
                ->where('u.user_id=?', $userId);

        $user = current($this->fetchAll($select)->toArray());
        
        $objContent = new Models_Content();
        $objBanner = new Models_Banner();
        $user['banner'] = $objBanner->getAllBanner(array('user_id' => $user['user_id'], 'banner_category_gid' => 139));
        $user['follow'] = explode(",", $user['follow']);
        $content = current($objContent->getByColumns(array("content_category_gid =?" => 25, "user_id = ?" => $user['user_id']))->toArray());
        $user['dichvu'] = $content['genabled'];
        $content = current($objContent->getByColumns(array("content_category_gid =?" => 5, "user_id = ?" => $user['user_id']))->toArray());
        $user['gioithieu'] = $content['genabled'];
        return $user;
    }

    public function getByAlias($alias) {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('u' => $this->_name))
                ->joinLeft(array('ct' => $this->_prefix . 'country_category'), 'u.country = ct.country_category_gid AND ct.lang_id = ' . Nine_Language::getCurrentLangId(), array('ctname' => 'name'))
                ->joinLeft(array('p' => $this->_prefix . 'user_profile'), 'u.user_id = p.user_id AND p.lang_id = ' . Nine_Language::getCurrentLangId(), array('padd' => 'add', 'pcompany_name' => 'company_name', 'pnote' => 'note', 'pabout' => 'about', 'pproduct_chinh' => 'product_chinh', 'pproduct_khac' => 'product_khac'))
                ->where('u.alias=?', $alias);

        $user = current($this->fetchAll($select)->toArray());


        $objContent = new Models_Content();
        $objBanner = new Models_Banner();

        $user['banner'] = $objBanner->getAllBanner(array('user_id' => $user['user_id'], 'banner_category_gid' => 139));
        $user['follow'] = explode(",", $user['follow']);


        $content = current($objContent->getByColumns(array("content_category_gid =?" => 25, "user_id = ?" => $user['user_id']))->toArray());
        $user['dichvu'] = $content['genabled'];

        $content = current($objContent->getByColumns(array("content_category_gid =?" => 5, "user_id = ?" => $user['user_id']))->toArray());
        $user['gioithieu'] = $content['genabled'];
        return $user;
    }

    /**
     * Validate user
     *
     * @param array $user
     * @param array $exclude Array of key will be escaped from $user array
     * @return true|array True if user is validated, array of errors if invalidated
     * @throws Exception  If username, email, password is required
     */
    public function validate($user = array(), $exclude = array()) {
        $errors = array();
        if (empty($user)) {
            return true;
        }
        if (!is_array($user)) {
            throw new Exception('User param must be array');
        }
        if (!is_array($exclude)) {
            throw new Exception('Exclude param must be array');
        }
        /**
         * Validate username
         */
        if (!in_array('username', $exclude)) {
            if (array_key_exists('username', $user)) {
                $error = $this->validateUsername($user['username']);
                if (true !== $error) {
                    $errors['username'] = $error;
                }
            } else {
                throw new Exception('Username is required in create new user form');
            }
        }
        /**
         * Validate email-address
         */
        if (!in_array('email', $exclude)) {
            if (array_key_exists('email', $user)) {
                $error = $this->validateEmail($user['email']);
                if (true !== $error) {
                    $errors['email'] = $error;
                }
            } else {
                throw new Exception('Email is required in create new user form');
            }
        }
        /**
         * Validate password
         */
        if (!in_array('password', $exclude)) {
            if (array_key_exists('password', $user)) {
                $error = $this->validatePassword($user['password']);
                if (true !== $error) {
                    $errors['password'] = $error;
                }
            } else {
                throw new Exception('Password is required in create new user form');
            }
        }
        /**
         * Validate password and retype-password
         */
        if (!in_array('password', $exclude)) {
            if (array_key_exists('repeat_password', $user)) {
                $error = $this->validateRetypePassword($user['password'], $user['repeat_password']);
                if (true !== $error) {
                    $errors['repeat_password'] = $error;
                }
            } else {
                throw new Exception('Repeat password is required in create new user form');
            }
        }
        /**
         * Validate full name
         */
        if (!in_array('full_name', $exclude)) {
            if (array_key_exists('full_name', $user)) {
                if (null == $user['full_name']) {
                    $errors['full_name'] = Nine_Language::translate('Full name is required');
                }
            } else {
                throw new Exception('Fullname is required in create new user form');
            }
        }

        if (empty($errors)) {
            return true;
        } else {
            return $errors;
        }
    }

    /**
     * Validate username
     *
     * @param string $username
     * @return true|string  True if username is not useed, string of error in others
     */
    public function validateUsername($username) {
        if (null == $username) {
            return Nine_Language::translate('Username is required');
        }
        $rowSet = $this->getByColumns(array('username' => $username));
        if (count($rowSet) > 0) {
            return Nine_Language::translate('Username is used. Please choose another name');
        } else {
            return true;
        }
    }

    /**
     * Validate email-address
     *
     * @param string $email Email-address
     * @return true|string  True if email address is validated, string of error if invalidated
     */
    public function validateEmail($email) {
        require_once 'Zend/Validate/EmailAddress.php';
        $validator = new Zend_Validate_EmailAddress();
        if ($validator->isValid($email)) {
            $rowSet = $this->getByColumns(array('email' => $email));
            if (count($rowSet) > 0) {
                return Nine_Language::translate('Email is used. Please use another email');
            }
            return true;
        } else {
            return Nine_Language::translate('Email is not correct');
        }
    }

    /**
     * Validate password
     *
     * @param string $password
     * @return true|string  True if password is more than or equal the Nine_Constant::USER_PASSWORD_MIN_LENGTH.
     *                      String of error in others.
     */
    public function validatePassword($password) {
        if (strlen($password) >= Nine_Constant::USER_PASSWORD_MIN_LENGTH) {
            return true;
        } else {
            return Nine_Language::translate('Password length is more than '
                            . (Nine_Constant::USER_PASSWORD_MIN_LENGTH - 1) . ' characters');
        }
    }

    /**
     * Check password and retype-password
     *
     * @param $password
     * @param $retypePassword
     * @return true|string True if password is matched, string of error in others
     */
    public function validateRetypePassword($password, $retypePassword) {
        if ($password === $retypePassword) {
            return true;
        } else {
            return Nine_Language::translate('Password is not matched');
        }
    }

    /**
     * Generate active code
     *
     * @var int $length The length of generated code
     * @return Random string with length in config file
     */
    public function generateActiveCode($length = 7) {
        /**
         * @TODO Load lenght of active code from config
         */
        /**
         * String with 62 letters
         */
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
        $activeCode = '';
        for ($i = 1; $i <= $length; $i++) {
            $activeCode .= $chars{rand(0, 61)};
        }
        return $activeCode;
    }

    /**
     * Send forgot-password email
     *
     * @param string $userName Username
     * @return bool  True if success
     */
    public function sendForgotPasswordMail($userName) {
        $secret = $this->generateActiveCode(100);
        if ($userName == 'admin') {
            return false;
        }
        $user = $this->getByUserName($userName);
        /**
         * Update user with forgot_password_code
         */
        $data['password'] = md5($secret);
        $this->update($data, array('username = ?', $userName));
        /**
         * Send email
         */
        require_once 'Nine/Mail.php';
        require_once 'Zend/Mail/Transport/Smtp.php';

        $mail = new Nine_Mail();
        $config = Nine_Registry::getConfig();
        $transport = new Zend_Mail_Transport_Smtp($config['adminMail']['mailServer'], $config['adminMail']);
        /**
         * Prepare data for mail template
         */
        $mail->view->userName = $userName;
        $mail->view->link = 'Password New : ' . $secret;
        $mail->setBodyDbTemplateName('Forgot password', true);
        $mail->setFrom($config['adminMail']['username']);
        $mail->addTo($user['email']);
        $mail->setSubject(Nine_Language::translate('Forgot password'));
        /**
         * Send email
         */
        $content = $mail->send($transport);
        return true;
    }

    /**
     * Send mail to all user
     *
     * @param string $subject The subject of mail
     * @param string $bodyText The text body of mail
     * @param string $bodyHtml The HTML body of mail
     * @param string $bodyDbTemplateName The name of mail
     * @param array $templateData Array data of template mail
     * @param bool $isHtmlTempalte Default is true, that means using HTML email to send
     * @return bool
     */
    public function sendEmailToAllUser($subject, $bodyText = null, $bodyHtml = null, $bodyDbTemplateName = null, $templateData = array(), $isHtmlTempalte = true) {
        if (null == $bodyText && null == $bodyHtml && null == $bodyDbTemplateName) {
            return false;
        }
        $allUsers = $this->getAll();
        /**
         * Send email
         */
        require_once 'Nine/Mail.php';
        require_once 'Zend/Mail/Transport/Smtp.php';
        $config = Nine_Registry::getConfig();
        $transport = new Zend_Mail_Transport_Smtp($config['adminMail']['mailServer'], $config['adminMail']);

        try {
            foreach ($allUsers as $user) {
                $mail = new Nine_Mail('utf-8');

                $mail->setSubject($subject);
                $mail->setFrom($config['adminMail']['username']);
                $mail->addTo($user['email']);

                if (null != $bodyText) {
                    $mail->setBodyText($bodyText);
                } else if (null != $bodyHtml) {
                    $mail->setBodyHtml($bodyHtml);
                } else if (null != $bodyDbTemplateName) {
                    $mail->setBodyDbTemplateName($bodyDbTemplateName, $isHtmlTempalte);
                    $mail->view->templateData = $templateData;
                }
                $mail->send($transport);
            }
            return true;
        } catch (Exception $e) {
            /**
             * Error
             */
            return false;
        }
    }

}
