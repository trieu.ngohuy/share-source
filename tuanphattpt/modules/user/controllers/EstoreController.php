<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/user/models/User.php';
require_once 'modules/user/models/UserProfile.php';
require_once 'modules/properties/models/PropertiesCategory.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/country/models/CountryCategory.php';
require_once 'modules/language/models/Lang.php';
class user_EstoreController extends Nine_Controller_Action
{
    public function editInfoAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $this->setLayout('estore');
        $errors = true;
        $oldUser = $objUser->getByAlias($url['alias']);
        $data = $this->_getParam('data', false);
        
        $objLang = new Models_Lang();
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $this->view->allLangs = $allLangs;
        
        $objProfile = new Models_UserProfile();
        
        $profile = $this->_getParam('profile', false);

        if (empty($oldUser)) {
            /**
             * User doesn't exsit
             */

            $this->session->userMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('User does NOT exist')
            );
            $this->_redirect('/' );
        }

        if (false !== $data) {
            /**
             * Update new user
             */
            if(isset($data['product_category_gid'])){
                $data['product_category_gid'] = '||'.implode("||", $data['product_category_gid']).'||' ;
            }

            if(isset($data['category_id'])){
                $data['category_id'] = '||'.implode("||", $data['category_id']).'||';
            }else{
                $data['category_id'] = '';
            }

            unset($data['time_start']);
            unset($data['time_end']);
            
            
            $newUser = $data;
            $newUser['full_name'] = trim($data['first_name']." ".$data['last_name']);
            /**
             * Check permission
             */

            if($newUser['email'] != $oldUser['email']) {
                $errors = $objUser->validateEmail($newUser['email']);
            }  else {
                unset($newUser['email']);
            }

            if (true === $errors) {
                /**
                 * TODO Read date format from language table
                 */

                $target_dir = 'media/userfiles/user/';
                if($_FILES["image"]["name"] != ''){
                    $target_dir = $target_dir.time().'-'  . basename( $_FILES["image"]["name"]);
                    $uploadOk=1;


                    // Check file size
                    if ($_FILES["image"]["size"] > 20000000) {
                        $errors['erro'] = 1;
                        $errors['mess'] = Nine_Language::translate('File L?n H�n 20Mb.');
                        $uploadOk=0;
                    }

                    // Only GIF files allowed
                    if ($_FILES["image"]["type"] != "image/gif" && $_FILES["image"]["type"] != "image/jpg" && $_FILES["image"]["type"] != "image/png"&& $_FILES["image"]["type"] != "image/jpeg") {
                        $errors['erro'] = 1;
                        $errors['mess'] = Nine_Language::translate('Ch? nh?n file png / jpg / gif / jpeg .');
                        $uploadOk = 0;
                    }

                    // Check if $uploadOk is set to 0 by an error
                    if ($uploadOk == 0) {

                    } else {
                        if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir)) {
                            $newUser['image'] = $target_dir;
                            $data['image'] = $target_dir;
                        } else {die;
                            $erro['mess'] = Nine_Language::translate('Sever Not Upload File . ');
                        }
                    }
                }
                try {

                    $objUser->update ( $newUser, array ('alias=?' => $oldUser['alias'] ) );
               		$profile_search = $objProfile->getByColumns("user_id = ".$user['user_id'])->toArray();
                    if(count($profile_search) > 0){
                    	$objProfile->update($profile, array('user_id=?' => $user['user_id']));
                    }else{
                    	$profile['user_id'] = $user['user_id'];
                    
                    	$objProfile->insert($profile);
                    }
                    /**
                     * Reload current login user
                     */
                    $loggedUser = $this->session->backendUser;
                    if ($oldUser ['user_id'] == @$loggedUser ['user_id']) {
                        $this->session->backendUser = $objUser->getByUserId ( $oldUser ['user_id'] )->toArray ();
                    }

                    $userMessage = array(
                        'success' => true,
                        'message' => Nine_Language::translate('User is updated successfully')
                    );

                } catch (Exception $e) {
                    $userMessage = array(
                        'success' => false,
                        'message' => Nine_Language::translate('Can not update user now')
                    );
                }
            }else{
                echo '<pre>';
                echo print_r($errors);
                echo '<pre>';
                die;
            }
        } else {
            /**
             * Get current user
             */
            $data = $oldUser;
            $profile = $objProfile->setAllLanguages(true)->getByColumns("user_id = ".$user['user_id'])->toArray();
            $tmp = array();
            foreach ($profile as $item){
            	$tmp[$item['lang_id']] = $item;
            }
            $profile = $tmp;
        }
        
        $data['product_category_gid'] = explode('||', trim(@$data['product_category_gid'],'||'));
        $data['time_start'] = date('m/d/Y',@$oldUser['time_start']);
        $data['time_end'] = date('m/d/Y',@$oldUser['time_end']);

        if($data['category_id']!= ''){
            $data['category_id'] = explode("||", trim($data['category_id'],'||'));
        }else{
            $data['category_id'] = array();
        }
        /**
         * Prepare for template
         */
        $this->view->errors = $errors;
        $this->view->data = $data;
        
        $this->view->profile = $profile;
        $this->view->headTitle(Nine_Language::translate('Edit user'));

        $this->view->menu = array(
            0=>'profile',
            1=>'edit-profile'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-user',
                'url'	=>	Nine_Registry::getBaseUrl().'estore/edit-info/' . $user['alias'] . '.html',
                'name'	=>	Nine_Language::translate('Edit Infomation')
            ),
        );

        $objCategoryProperties = new Models_PropertiesCategory;
        $allCatsProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(29);
        $this->view->allCatsProperties = $allCatsProperties;

        $objCatProduct     = new Models_ProductCategory();
        $allCatsProduct = $objCatProduct->getAll(array('sorting ASC'))->toArray();
        $this->view->allCatsProduct = $allCatsProduct;

        $objCountry = new Models_CountryCategory();
        $allCountry = $objCountry->getAllCategoriesParentNull();
        $this->view->allCountry = $allCountry;

        $this->view->userMessage = $userMessage;
        $this->view->alias = $oldUser['alias'] . '.html';
    }

    public function changePasswordAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        $this->setLayout('estore');
        $data = $this->_getParam('data', false);

        if (false !== $data) {

            if (md5($data['current-password']) != $user['password']) {
                $userMessage['messagePasswordNotCorrect'] = Nine_Language::translate('Password is not correct');
            }

            $errorLenghtPassword = $objUser->validatePassword($data['new-password']);
            if($errorLenghtPassword !== true) {
                $userMessage['messageLenghtPassword'] = $errorLenghtPassword;
            }

            $errorRepeatPassword = $objUser->validateRetypePassword($data['new-password'], $data['repeat-password']);
            if($errorRepeatPassword !== true) {
                $userMessage['messageRepeatPassword'] = $errorRepeatPassword;
            }

            if($userMessage != null) {
                $userMessage['success'] = false;
            } else {
                $userMessage['success'] = true;

                $data['password'] = md5($data['new-password']);
                unset($data['current-password']);
                unset($data['new-password']);
                unset($data['repeat-password']);

                $objUser->update ( $data, array ('alias=?' => $user['alias'] ) );
                $userMessage['changeSuccess'] = Nine_Language::translate('Change Password Successful!');
                unset($data['password']);
            }
        }

        $this->view->menu = array(
            0=>'profile',
            1=>'change-password'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-pencil',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('Change Password')
            )
        );

        $this->view->userMessage = $userMessage;
        $this->view->alias = $user['alias'] . '.html';
    }
}