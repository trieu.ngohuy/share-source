<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/user/models/User.php';
require_once 'modules/cart/models/Cart.php';
require_once 'modules/cart/models/CartDetail.php';
require_once 'modules/user/models/Payment.php';
require_once 'modules/buying/models/Buying.php';
require_once 'modules/buying/models/BuyingCategory.php';
require_once 'modules/properties/models/PropertiesCategory.php';
require_once 'modules/country/models/CountryCategory.php';
require_once 'modules/contact/models/Contact.php';
include_once 'modules/mail/models/Mail.php';
include_once 'modules/mail/models/phpmailer.php';
include_once 'modules/mail/models/smtp.php';
require_once 'modules/message/models/MessageCategory.php';
require_once 'modules/bank/models/ConnectBank.php';

class user_IndexController extends Nine_Controller_Action {

    public function profileAction() {
        if ($this->auth->isLogin()) {
            $this->setLayout('access');
            $userLogin = Nine_Registry::getLoggedInUser();
            $objCountry = new Models_CountryCategory();

            $userLogin['created_date'] = date('d-m-Y', $userLogin['created_date']);
            $userLogin['time_end'] = date('d-m-Y', $userLogin['time_end']);
            $this->view->user = $userLogin;
            $this->view->menu = 'profile';
            /*
             * Get breadcrumbs
             */
            $url = Nine_Route::url();
            $this->view->breadCrumbs = Nine_Common::getBreadCumbs(0, 'edit-info', array(
                        'name' => Nine_Language::translate("Trang cá nhân"),
                        'url' => $url['path'] . 'user/profile'
            ));
        } else {
            $this->_redirect('/');
        }
    }

    public function changePasswordAction() {
        if ($this->auth->isLogin()) {
            $objUser = new Models_User();

            $this->setLayout('access');
            $userLogin = Nine_Registry::getLoggedInUser();

            $data = $this->_getParam('data', false);

            if (false !== $data) {
                $userMessage = null;
                if (md5($data['current-password']) != $userLogin['password']) {
                    $userMessage['messagePasswordNotCorrect'] = Nine_Language::translate('Password is not correct');
                }

                $errorLenghtPassword = $objUser->validatePassword($data['new-password']);
                if ($errorLenghtPassword !== true) {
                    $userMessage['messageLenghtPassword'] = $errorLenghtPassword;
                }

                $errorRepeatPassword = $objUser->validateRetypePassword($data['new-password'], $data['repeat-password']);
                if ($errorRepeatPassword !== true) {
                    $userMessage['messageRepeatPassword'] = $errorRepeatPassword;
                }

                if ($userMessage != null) {
                    $userMessage['success'] = false;
                } else {
                    $userMessage['success'] = true;

                    $data['password'] = md5($data['new-password']);
                    unset($data['current-password']);
                    unset($data['new-password']);
                    unset($data['repeat-password']);

                    $objUser->update($data, array('alias=?' => $userLogin['alias']));
                    $userMessage['changeSuccess'] = Nine_Language::translate('Change Password Successful!');
                    unset($data);
                }
                $this->view->userMessage = $userMessage;
            }

            $this->view->user = $userLogin;
            $this->view->menu = 'change-password';
            /*
             * Get breadcrumbs
             */
            $url = Nine_Route::url();
            $this->view->breadCrumbs = Nine_Common::getBreadCumbs(0, 'edit-info', array(
                        'name' => Nine_Language::translate("Thay đổi mật khẩu"),
                        'url' => $url['path'] . 'user/change-password'
            ));
        } else {
            $this->_redirect('/');
        }
    }

    public function upgradeAction() {
        if ($this->auth->isLogin()) {

            $objUser = new Models_User();
            $this->setLayout('access');
            $userLogin = Nine_Registry::getLoggedInUser();


            $this->view->menu = 'upgrade';
            /*
             * Get breadcrumbs
             */
            $url = Nine_Route::url();
            $this->view->breadCrumbs = Nine_Common::getBreadCumbs(0, 'edit-info', array(
                        'name' => Nine_Language::translate("Nâng cấp tài khoản"),
                        'url' => $url['path'] . 'user/upgrade'
            ));
        } else {
            $this->_redirect('/');
        }
    }

    public function upgradeStep1Action() {
        if ($this->auth->isLogin()) {
            $objUser = new Models_User();
            $this->setLayout('access');
            $userLogin = Nine_Registry::getLoggedInUser();
            $gid = $this->_getParam('gid', false);
            if ($gid == false) {
                $this->_redirect('/');
            }
            $this->view->menu = 'upgrade';
            $this->view->gid = $gid;


            $userLogin['created_date'] = date('d-m-Y', $userLogin['created_date']);

            $objCountry = new Models_CountryCategory();
            $allCountry = $objCountry->buildTree($objCountry->getAllCategoriesParentNull());
            $this->view->allCountry = $allCountry;
            $oldUser = $objUser->getUser($userLogin['user_id']);

            $this->view->oldUser = $oldUser;
            /*
             * Get breadcrumbs
             */
            $url = Nine_Route::url();
            $this->view->breadCrumbs = Nine_Common::getBreadCumbs(0, 'edit-info', array(
                        'name' => Nine_Language::translate("Nâng cấp tài khoản"),
                        'url' => $url['path'] . 'user/upgrade-step-1'
            ));
        } else {
            $this->_redirect('/');
        }
    }

    public function upgradeStep2Action() {
        if ($this->auth->isLogin()) {
            $objUser = new Models_User();
            $this->setLayout('access');
            $userLogin = Nine_Registry::getLoggedInUser();
            if (!$_POST) {
                $this->session->upgrate = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Có Lỗi Xãy Ra Khi Nâng Cấp Tài Khoản')
                );
                $this->_redirect('user/manager-upgrade');
            }


            $oldUser = $objUser->getUser($userLogin['user_id']);

            $this->view->oldUser = $oldUser;
            $payment_method = $_POST['method_id'];
            $_POST['package_price'] = $_POST['package_price_vnd'];
            $package_id = $_POST['package_id'];
            $package_days = $_POST['package_days'];

            $data = array();

            $data['user_id'] = $oldUser['user_id'];
            $data['payment_date'] = time();
            $data['payment_price'] = $_POST['package_price_vnd'];
            $data['country_id'] = $_POST['country_id'];
            $data['payment_status'] = '0';
            $data['package_id'] = $package_id;
            $data['payment_type'] = $_POST['method_id'];
            $data['currency_id'] = 4;


            $this->view->data = $data;
            $this->session->data = $data;
            /*
             * Get breadcrumbs
             */
            $url = Nine_Route::url();
            $this->view->breadCrumbs = Nine_Common::getBreadCumbs(0, 'edit-info', array(
                        'name' => Nine_Language::translate("Nâng cấp tài khoản"),
                        'url' => $url['path'] . 'user/upgrade-step-2'
            ));
        } else {
            $this->_redirect('/');
        }
    }

    public function upgradeSuccessAction() {
        if (!$_POST) {
            $this->session->upgrate = array(
                'success' => false,
                'message' => Nine_Language::translate('Có Lỗi Xãy Ra Khi Nâng Cấp Tài Khoản')
            );
            $this->_redirect('user/manager-upgrade');
        }

        /*
          $POST = array();
          $POST['vpc_Version'] = '1';
          $POST['vpc_Command'] = 'pay';
          $POST['vpc_AccessCode'] = '72AD46B6';
          $POST['vpc_MerchTxnRef'] = 1;
          $POST['vpc_Merchant'] = 'test03051980';
          $POST['vpc_OrderInfo'] = 'VPC PHP Merchant Example';
          $POST['vpc_Amount'] = '100000';
          $POST['vpc_Locale'] = 'en_AU';
          $POST['vpc_Currency'] = 'VND';
          $POST['vpc_ReturnURL'] = $this->_dirApp['user_url'] . 'payment/payprocessqt';
          //$POST['vpc_BackURL'] = $this->_dirApp['user_url'] . 'payment/payback';
          $POST["virtualPaymentClientURL"] = 'https://migs.mastercard.com.au/vpcpay';
          $POST["SubButL"] = 'Pay';
          $POST["Title"] = 'PHP Merchant';
          $this->Pay( $POST );
         */

        include( "libs/Smartlink/Payment.php");
        $payment = new Payment();

        echo $method_payment = $_POST['method_payment'];


        if ($method_payment == 'VisaCreditCard') { //cong thanh toan quoc te
            $arr = array(
                'smartlink' => array(
                    'vpc_Secret' => "37D9749620F6611A21000E96E3B026AB",
                    'vpc_Version' => "1",
                    'vpc_Command' => "pay",
                    'vpc_BackURL' => "https://www.C&D.com",
                    'vpc_AccessCode' => "C31E9A3C",
                    'vpc_Merchant' => "C&D",
                    'vpc_OrderInfo' => "Payment Estore World",
                    'virtualPaymentClientURL' => "https://migs.mastercard.com.au/vpcpay"
                )
            );
            $smartlink = $arr['smartlink'];
            $payment->setVirtualPaymentUrl($smartlink["virtualPaymentClientURL"]);
            $payment->setSecureSecret($smartlink["vpc_Secret"]);

            unset($smartlink["virtualPaymentClientURL"]);
            unset($smartlink["vpc_Secret"]);

            $_params = $smartlink;
            $_params['vpc_Locale'] = 'en';
            $_params['vpc_Currency'] = 'VND';

            /*
              if ( $_SESSION['lang'] == 'EN' ) { $_params['vpc_Currency'] = 'USD'; }
             */

            $_params['vpc_ReturnURL'] = 'https://www.C&D.com/user/payprocessqt';

            $package_price = (int) 100000;

            //print_r( $_params );die;
        } else {
            $arr = array(
                'smartlink' => array('vpc_Secret' => "11B71F733A833B4A7FE22B4693B77A59",
                    'vpc_Version' => "1.1",
                    'vpc_Command' => "pay",
                    'vpc_BackURL' => "https://www.C&D.com",
                    'vpc_AccessCode' => "N1O2C3N4D5E6A7L8",
                    'vpc_Merchant' => "C&D",
                    'vpc_OrderInfo' => "Payment Estore",
                    'vpc_Api' => "https://payment.smartlink.com.vn/vpcpay.do")
            );
            $smartlink = $arr['smartlink'];

            $payment->setVirtualPaymentUrl($smartlink["vpc_Api"]);
            $payment->setSecureSecret($smartlink["vpc_Secret"]);
            unset($smartlink["vpc_Api"]);
            unset($smartlink["vpc_Secret"]);

            $_params = $smartlink;
            $_params['vpc_Locale'] = 'vn';
            $_params['vpc_Currency'] = 'VND';
            $_params['vpc_ReturnURL'] = 'https://www.C&D.com/user/payprocess';

            $package_price = (int) $_POST['package_price'] * 100;
        }


        $_params['vpc_Amount'] = $package_price;
        $_params['vpc_MerchTxnRef'] = time();
        $payment->redirect($_params);
    }

    public function payprocessAction() {

        $arr = array(
            'smartlink' => array('vpc_Secret' => "11B71F733A833B4A7FE22B4693B77A59",
                'vpc_Version' => "1.1",
                'vpc_Command' => "pay",
                'vpc_BackURL' => "https://C&D.com",
                'vpc_AccessCode' => "N1O2C3N4D5E6A7L8",
                'vpc_Merchant' => "C&D",
                'vpc_OrderInfo' => "Payment Estore",
                'vpc_Api' => "https://payment.smartlink.com.vn/vpcpay.do")
        );
        $smartlink = $arr['smartlink'];

        include("libs/Smartlink/Payment.php");
        $payment = new Payment();

        $payment->setSecureSecret($smartlink["vpc_Secret"]);
        $payment->checkSum($_GET);

        $txnResponseCode = $payment->getParameter("vpc_ResponseCode");
        $vpc_TransactionNo = $payment->getParameter("vpc_TransactionNo");

        if ($txnResponseCode == 0) {
            $data = $this->session->data;
            $objUser = new Models_User();
            $userLogin = Nine_Registry::getLoggedInUser();
            $oldUser = $objUser->getUser($userLogin['user_id']);
//					$updateUser = array(
//						'time_end' => $oldUser['time_end'] + ($data['package_id']*365/2 *86400)
//					);
//					$objUser->update($updateUser, array('user_id = ?'=>$userLogin['user_id']));

            $objPayment = new Models_Payment();
            $newpayment = array(
                'user_id' => $userLogin['user_id'],
                'method' => $data['payment_type'],
                'packed' => $data['package_id'],
                'status' => 1,
                'time' => time()
            );
            $objPayment->insert($newpayment);


            $content = '<p style="margin-bottom:15px; font-size: 16px; line-height: 25px;">';
            $content .= Nine_Language::translate('Cảm ơn bạn đã nâng cấp tài khoản tại website chúng tôi : C&D.com');
            $content .= '</p>';
            $content .= '<p><b>' . Nine_Language::translate('Tài khoản của quý khách đã được nâng cấp thành công . Xin chân thành cảm ơn quý khách') . '</b></p>';


            $objMail = new Models_Mail();
            //$objMail->sendHtmlMail('contact', $data, "phamducphuc.it@gmail.com");

            /**
             * Get admin
             */
            $objUser = new Models_User();
            //$admin = $objUser->getByUserName('admin');
            //$adminemail = $admin['email'];

            $data_send = array(
                'name_to' => "User",
                'email_to' => array($oldUser['email']),
                'title_email' => Nine_Language::translate('Xác nhận nâng cấp tài khoản - C&D'),
                'name_from' => Nine_Language::translate("Admin C&D"),
                'content' => $content
            );
            $this->sendmail_smtp($data_send);

            $oldUser = $objUser->getUser(1);
            $content = '<p style="margin-bottom:15px; font-size: 16px; line-height: 25px;">';
            $content .= Nine_Language::translate('Upgrade Estore');
            $content .= '</p>';
            $content .= '<p><b>' . Nine_Language::translate('Thành viên vừa upgrade thành công . Vui lòng vào cms check duyệt tài khoản') . '</b></p>';
            $data_send = array(
                'name_to' => "User",
                'email_to' => array($oldUser['email']),
                'title_email' => Nine_Language::translate('Xác nhận nâng cấp tài khoản - C&D'),
                'name_from' => Nine_Language::translate("Admin C&D"),
                'content' => $content
            );
            $this->sendmail_smtp($data_send);


            $this->session->data = null;
            $this->session->upgrate = array(
                'success' => true,
                'message' => Nine_Language::translate('Nâng cấp tài khoản thành công . Cảm ơn quý khách đã tin tưởng khi sử dụng website của chúng tôi')
            );
            $this->_redirect('user/manager-upgrade');
        } else {
            $this->session->data = null;
            $this->session->upgrate = array(
                'success' => false,
                'message' => Nine_Language::translate('Có Lỗi Xãy Ra Khi Nâng Cấp Tài Khoản')
            );
            $this->_redirect('user/manager-upgrade');
        }
    }

    public function payprocessqtAction() {

        $arr = array(
            'smartlink' => array('vpc_Secret' => "37D9749620F6611A21000E96E3B026AB",
                'vpc_Version' => "1",
                'vpc_Command' => "pay",
                'vpc_BackURL' => "https://C&D.com",
                'vpc_AccessCode' => "C31E9A3C",
                'vpc_Merchant' => "C&D",
                'vpc_OrderInfo' => "Payment Estore World",
                'virtualPaymentClientURL' => "https://migs.mastercard.com.au/vpcpay")
        );
        $smartlink = $arr['smartlink'];

        include( "libs/Smartlink/Payment.php");
        $payment = new Payment();

        $payment->setSecureSecret($smartlink["vpc_Secret"]);
        $payment->checkSum($_GET);

        $txnResponseCode = $payment->getParameter("vpc_TxnResponseCode");
        $vpc_TransactionNo = $payment->getParameter("vpc_TransactionNo");

        if ($txnResponseCode == '0') {
            $data = $this->session->data;
            $objUser = new Models_User();
            $userLogin = Nine_Registry::getLoggedInUser();
            $oldUser = $objUser->getUser($userLogin['user_id']);
//					$updateUser = array(
//						'time_end' => $oldUser['time_end'] + ($data['package_id']*365/2 *86400)
//					);
//					$objUser->update($updateUser, array('user_id = ?'=>$userLogin['user_id']));

            $objPayment = new Models_Payment();
            $newpayment = array(
                'user_id' => $userLogin['user_id'],
                'method' => $data['payment_type'],
                'packed' => $data['package_id'],
                'status' => 1,
                'time' => time()
            );
            $objPayment->insert($newpayment);
            $content = '<p style="margin-bottom:15px; font-size: 16px; line-height: 25px;">';
            $content .= Nine_Language::translate('Cảm ơn bạn đã nâng cấp tài khoản tại website chúng tôi : C&D.com');
            $content .= '</p>';
            $content .= '<p><b>' . Nine_Language::translate('Tài khoản của quý khách đã được nâng cấp thành công . Xin chân thành cảm ơn quý khách') . '</b></p>';


            $objMail = new Models_Mail();
            //$objMail->sendHtmlMail('contact', $data, "phamducphuc.it@gmail.com");

            /**
             * Get admin
             */
            $objUser = new Models_User();
            //$admin = $objUser->getByUserName('admin');
            //$adminemail = $admin['email'];

            $data_send = array(
                'name_to' => "User",
                'email_to' => array($oldUser['email']),
                'title_email' => Nine_Language::translate('Xác nhận nâng cấp tài khoản - C&D'),
                'name_from' => Nine_Language::translate("Admin C&D"),
                'content' => $content
            );
            $this->sendmail_smtp($data_send);

            $oldUser = $objUser->getUser(1);
            $content = '<p style="margin-bottom:15px; font-size: 16px; line-height: 25px;">';
            $content .= Nine_Language::translate('Upgrade Estore');
            $content .= '</p>';
            $content .= '<p><b>' . Nine_Language::translate('Thành viên vừa upgrade thành công . Vui lòng vào cms check duyệt tài khoản') . '</b></p>';
            $data_send = array(
                'name_to' => "User",
                'email_to' => array($oldUser['email']),
                'title_email' => Nine_Language::translate('Xác nhận nâng cấp tài khoản - C&D'),
                'name_from' => Nine_Language::translate("Admin C&D"),
                'content' => $content
            );
            $this->sendmail_smtp($data_send);


            $this->session->data = null;
            $this->session->upgrate = array(
                'success' => true,
                'message' => Nine_Language::translate('Nâng cấp tài khoản thành công . Cảm ơn quý khách đã tin tưởng khi sử dụng website của chúng tôi')
            );
            $this->_redirect('user/manager-upgrade');
        } else {
            $this->session->data = null;
            $this->session->upgrate = array(
                'success' => false,
                'message' => Nine_Language::translate('Có Lỗi Xãy Ra Khi Nâng Cấp Tài Khoản')
            );
            $this->_redirect('user/manager-upgrade');
        }
    }

    function getResponseDescription($responseCode) {

        switch ($responseCode) {
            case "0" : $result = "Transaction Successful";
                break;
            case "?" : $result = "Transaction status is unknown";
                break;
            case "1" : $result = "Unknown Error";
                break;
            case "2" : $result = "Bank Declined Transaction";
                break;
            case "3" : $result = "No Reply from Bank";
                break;
            case "4" : $result = "Expired Card";
                break;
            case "5" : $result = "Insufficient funds";
                break;
            case "6" : $result = "Error Communicating with Bank";
                break;
            case "7" : $result = "Payment Server System Error";
                break;
            case "8" : $result = "Transaction Type Not Supported";
                break;
            case "9" : $result = "Bank declined transaction (Do not contact Bank)";
                break;
            case "A" : $result = "Transaction Aborted";
                break;
            case "C" : $result = "Transaction Cancelled";
                break;
            case "D" : $result = "Deferred transaction has been received and is awaiting processing";
                break;
            case "F" : $result = "3D Secure Authentication failed";
                break;
            case "I" : $result = "Card Security Code verification failed";
                break;
            case "L" : $result = "Shopping Transaction Locked (Please try the transaction again later)";
                break;
            case "N" : $result = "Cardholder is not enrolled in Authentication scheme";
                break;
            case "P" : $result = "Transaction has been received by the Payment Adaptor and is being processed";
                break;
            case "R" : $result = "Transaction was not processed - Reached limit of retry attempts allowed";
                break;
            case "S" : $result = "Duplicate SessionID (OrderInfo)";
                break;
            case "T" : $result = "Address Verification Failed";
                break;
            case "U" : $result = "Card Security Code Failed";
                break;
            case "V" : $result = "Address Verification and Card Security Code Failed";
                break;
            default : $result = "Unable to be determined";
        }
        return $result;
    }

    public function Pay($POST) {

        $SECURE_SECRET = "0ECDD6D43A5BF997EC20C18600441E56";
        $vpcURL = $POST["virtualPaymentClientURL"] . "?";

        unset($POST["virtualPaymentClientURL"]);
        unset($POST["SubButL"]);

        $md5HashData = $SECURE_SECRET;
        ksort($POST);
        $appendAmp = 0;

        foreach ($POST as $key => $value) {
            if (strlen($value) > 0) {
                if ($appendAmp == 0) {
                    $vpcURL .= urlencode($key) . '=' . urlencode($value);
                    $appendAmp = 1;
                } else {
                    $vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
                }
                $md5HashData .= $value;
            }
        }
        if (strlen($SECURE_SECRET) > 0) {
            $vpcURL .= "&vpc_SecureHash=" . strtoupper(md5($md5HashData));
        }
        //echo $vpcURL;die;
        header("Location: " . $vpcURL);
    }

    public function managerUpgradeAction() {

        if ($this->auth->isLogin()) {
            $objUser = new Models_User();
            $this->setLayout('access');
            $userLogin = Nine_Registry::getLoggedInUser();
            $objPayment = new Models_Payment();

            $config = Nine_Registry::getConfig();

            $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");

            $currentPage = $this->_getParam("page", false);
            $displayNum = $this->_getParam('displayNum', false);
            $objUser = new Models_User();

            $data = $this->_getParam('data', array());
            foreach ($data as $id => $value) {
                $value = intval($value);
                $objUser->update(array('group_id' => $value), array('user_id=?' => $id));
                $this->session->userMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate("User Group is edited successfully")
                );
            }

            if ($currentPage == false) {
                $currentPage = 1;
                $this->session->userDisplayNum = null;
                $this->session->userCondition = null;
            }

            /**
             * Get number of users per page
             */
            if (false === $displayNum) {
                $displayNum = $this->session->userDisplayNum;
            } else {
                $this->session->userDisplayNum = $displayNum;
            }
            if (null != $displayNum) {
                $numRowPerPage = $displayNum;
            }
            /**
             * Get condition
             */
            $condition = $this->_getParam('condition', false);
            if (false === $condition) {
                $condition = $this->session->userCondition;
            } else {
                $this->session->userCondition = $condition;
                $currentPage = 1;
            }
            if (false == $condition) {
                $condition = array();
            }

            /**
             * Load all users
             */
            $condition['user_id'] = $userLogin['user_id'];
            $allPayments = $objPayment->getAllUsersWithUser($condition, 'payment_id DESC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage
            );
            /**
             * Count all users
             */
            $count = count($objPayment->getAllUsersWithUser($condition));
            $this->view->condition = $condition;
            /**
             * Modify all users
             */
            foreach ($allPayments as &$user) {
                if (null != $user['time']) {
                    $user['time'] = date($config['dateFormat'], $user['time']);
                }
            }
            unset($user);
            $this->view->allPayments = $allPayments;
            $this->view->displayNum = $numRowPerPage;

            $this->view->upgrate = $this->session->upgrate;
            $this->session->upgrate = null;
            $this->view->menu = 'manager-upgrade';
            /*
             * Get breadcrumbs
             */
            $url = Nine_Route::url();
            $this->view->breadCrumbs = Nine_Common::getBreadCumbs(0, 'edit-info', array(
                        'name' => Nine_Language::translate("Nâng cấp tài khoản"),
                        'url' => $url['path'] . 'user/upgrade'
            ));
        } else {
            $this->_redirect('/');
        }
    }

    public function editInfoAction() {
        if ($this->auth->isLogin()) {
            $this->setLayout('access');
            $objUser = new Models_User();
            //$this->setLayout('access');
            $userLogin = Nine_Registry::getLoggedInUser();

            $data = $this->_getParam('data', false);
            $errors = true;
            if (false !== $data) {
                /**
                 * Update new user
                 */
                $newUser = $data;
                $newUser['full_name'] = trim($data['first_name'] . " " . $data['last_name']);

                if (true === $errors) {
                    /**
                     * TODO Read date format from language table
                     */
                    $target_dir = 'media/userfiles/user/';
                    if ($_FILES["image"]["name"] != '') {
                        $target_dir = $target_dir . basename($_FILES["image"]["name"]);
                        $uploadOk = 1;

                        // Check if file already exists
                        if (file_exists($target_dir . $_FILES["image"]["name"])) {
                            $errors['erro'] = 1;
                            $errors['mess'] = Nine_Language::translate('Tên File Đã Tồn Tại');
                            $uploadOk = 0;
                        }

                        // Check file size
                        if ($_FILES["image"]["size"] > 20000000) {
                            $errors['erro'] = 1;
                            $errors['mess'] = Nine_Language::translate('File Lớn Hơn 20Mb.');
                            $uploadOk = 0;
                        }

                        // Only GIF files allowed
                        if ($_FILES["image"]["type"] != "image/gif" && $_FILES["image"]["type"] != "image/jpg" && $_FILES["image"]["type"] != "image/png" && $_FILES["image"]["type"] != "image/jpeg") {
                            $errors['erro'] = 1;
                            $errors['mess'] = Nine_Language::translate('Ch? nh?n file png / jpg / gif / jpeg .');
                            $uploadOk = 0;
                        }

                        // Check if $uploadOk is set to 0 by an error
                        if ($uploadOk == 0) {
                            
                        } else {
                            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir)) {
                                $newUser['image'] = $target_dir;
                                $data['image'] = $target_dir;
                            } else {
                                die;
                                $erro['mess'] = Nine_Language::translate('Sever Not Upload File . ');
                            }
                        }
                    } else {
                        $newUser['image'] = $userLogin['image'];
                        $data['image'] = $userLogin['image'];
                    }
                    try {

                        $objUser->update($newUser, array('alias=?' => $userLogin['alias']));
                        /**
                         * Reload current login user
                         */
                        $loggedUser = $this->session->backendUser;
                        if ($userLogin ['user_id'] == @$loggedUser ['user_id']) {
                            $this->session->backendUser = $objUser->getByUserId($userLogin ['user_id'])->toArray();
                        }

                        $userMessage = array(
                            'success' => true,
                            'message' => Nine_Language::translate('Information updated successfully')
                        );
                    } catch (Exception $e) {
                        $userMessage = array(
                            'success' => false,
                            'message' => Nine_Language::translate('Can not update user now')
                        );
                    }
                    $this->view->userMessage = $userMessage;
                } else {
                    echo '<pre>';
                    echo print_r($errors);
                    echo '<pre>';
                    die;
                }
            } else {
                /**
                 * Get current user
                 */
                $data = $userLogin;
            }

            $this->view->user = $data;
            $this->view->menu = 'edit-info';
        } else {
            $this->_redirect('/');
        }
        $objCountry = new Models_CountryCategory();
        $allCountry = $objCountry->buildTree($objCountry->getAllCategoriesParentNull());
        $this->view->allCountry = $allCountry;
        /*
         * Get breadcrumbs
         */
        $url = Nine_Route::url();
        $this->view->breadCrumbs = Nine_Common::getBreadCumbs(0, 'edit-info', array(
                    'name' => Nine_Language::translate("Chỉnh sửa thông tin"),
                    'url' => $url['path'] . 'user/user/edit-info'
        ));
        $this->view->headTitle("Chỉnh sửa thông tin user");
    }

    /*
     * Manager cart
     */

    public function cartManagerAction() {
        if ($this->auth->isLogin()) {

            $objCart = new Models_Cart();
            $this->setLayout('access');
            //Get logged user
            $userLogin = Nine_Registry::getLoggedInUser();

            //Paging
            $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
            $currentPage = $this->_getParam("page", false);
            $displayNum = $this->_getParam('displayNum', false);
            if ($currentPage == false) {
                $currentPage = 1;
                $this->session->productDisplayNum = null;
                $this->session->productCondition = null;
            }
            /**
             * Get number of items per page
             */
            if (false === $displayNum) {
                $displayNum = $this->session->productDisplayNum;
            } else {
                $this->session->productDisplayNum = $displayNum;
            }
            if (null != $displayNum) {
                $numRowPerPage = $displayNum;
            }

            //Get list cat on user id
            $listCart = $objCart->getByColumns(array(
                        'user_id' => $userLogin['user_id']
                    ))->toArray();
            foreach ($listCart as &$value) {
                //Get list products on each cart
                $objCartDetail = new Models_CartDetail();
                $detail = $objCartDetail->getByColumns(array(
                            'cart_id' => $value['cart_id']
                        ))->toArray();
                //Get list products
                foreach ($detail as &$content) {
                    $objProduct = new Models_Product();
                    $product = @reset($objProduct->getByColumns(array(
                                        'product_gid' => $content['product_gid']
                                    ))->toArray());

                    //Get main image
                    $tmp = explode('||', $product ['images']);
                    $product ['main_image'] = $tmp [0];
                    //Cal sub total price

                    $temp_price = 0;
                    if ($product['price_sale'] != '')
                        $temp_price = $product['price_sale'];
                    else
                        $temp_price = $product['price'];

                    $temp_price = str_replace(",", "", $temp_price);
                    $temp_price = str_replace(".", "", $temp_price);

                    $product ['total_price'] = str_replace(".", ",", Nine_Query::makeUpPrice($temp_price * $content ['mount']));
                    //Assign to list products on each item
                    $content['products'] = $product;
                }
                unset($content);
                $value['detail'] = $detail;
            }
            unset($value);

            //Set variables to views
            $this->view->listCart = $listCart;
            $this->setPagination($numRowPerPage, $currentPage, count($listCart));
            $this->view->menu = 'cart-manager';
            /*
             * Get breadcrumbs
             */
            $url = Nine_Route::url();
            $this->view->url = $url;
            Nine_Common::getConfig($this->view);
            $this->view->breadCrumbs = Nine_Common::getBreadCumbs(0, 'edit-info', array(
                        'name' => Nine_Language::translate("Quản lý đơn hàng"),
                        'url' => $url['path'] . 'user/cart-manager'
            ));
        } else {
            $this->_redirect('access/login');
        }
    }

    public function sellerAction() {
        $this->setLayout('access');
        $objUser = new Models_User();

        $key = $this->_getParam('key', false);


        $numRowPerPage = 10;
        $currentPage = $this->_getParam("page", false);
        if ($currentPage == false) {
            $currentPage = 1;
        }

        $condition['group_id'] = 3;
        if (false != $key) {
            $condition['name_company'] = $key;
        }
        $objBanner = new Models_Banner();

        $this->view->adv = $objBanner->getAllBanner(array('user_id' => 1, 'banner_category_gid' => 141, 'page' => 1, 'location' => 1));

        $users = $objUser->getAllUsersWithGroupFrontend($condition, 'product_category_gid ASC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
        foreach ($users as &$item) {
            
        }
        unset($item);
        $count = count($objUser->getAllUsersWithGroupFrontend($condition));

        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->users = $users;
        $this->view->count = $count;
        $this->view->firstItemInPage = ($currentPage - 1) * $numRowPerPage + 1;
        $this->view->lastItemInPage = ($currentPage - 1) * $numRowPerPage + count($objUser->getAllUsersWithGroup(array('group_id' => 3), 'product_category_gid ASC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage));
    }

    public function sendmail_smtp($data) {
        // Mail
        define('MAIL_PROTOCOL', 'smtp');
        define('MAIL_HOST', 'ssl://smtp.googlemail.com');
        define('MAIL_PORT', '465');
        define('MAIL_USER', 'luutn.it@gmail.com');
        define('MAIL_PASS', 'Linh2204');
        define('MAIL_SEC', 'ssl');
        define('MAIL_FROM', 'no.reply.C&D@gmail.com');


        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPAuth = TRUE;
        $mail->SMTPSecure = MAIL_SEC;
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASS;
        $mail->CharSet = "UTF-8";
        $mail->WordWrap = 50;

        $mail->IsHTML(TRUE);
        $mail->From = MAIL_USER;
        $mail->FromName = $data['name_from'];
        $to = $data['email_to'];
        $name = $data['name_to'];
        if (is_array($to)) {
            foreach ($to as $key => $sto) {
                $mail->AddAddress($sto, '');
            }
        } else {
            $mail->AddAddress($to, $name);
        }

        $mail->AddReplyTo(MAIL_USER, "C&D");
        $mail->Subject = $data['title_email'];
        $mail->Body = $data['content'];
        $mail->AltBody = $data['content'];
        if (!$mail->Send()) {
            return false;
        } else {
            return true;
        }
    }

    public function registerAction() {
        $objUser = new Models_User();

        $this->setLayout('access');

        $data = $this->_getParam('data', false);
        $captcha = $this->_getParam('captcha', false);
        if (false != $data) {
            if (@$captcha == $_SESSION['captcha']) {
                $newUser = $data;
                $email = $newUser['email'];
                $newUser['full_name'] = trim($newUser['first_name'] . " " . $newUser['last_name']);
                $newUser['alias'] = $newUser['username'];
                $newUser['user_active'] = 1;
                $newUser['enabled'] = 0;
                $newUser['created_date'] = time();
                $newUser['group_id'] = 4;
                $generatedKey = sha1(mt_rand(10000, 99999) . $email);
                $newUser['active_code'] = $generatedKey;
                $pass = $newUser['password'];
                $validate = $objUser->validate($newUser);
                if ($validate === true) {
                    unset($newUser['repeat_password']);
                    $newUser['password'] = md5($newUser['password']);
                    $objUser->insert($newUser);


                    $content = '
						Hi ' . $newUser['username'] . ', <br/>
						Welcome to C&D.com - The No. 1 "Quality" B2B E-Marketplace in Vietnam!<br/>
						Congratulations! You have now become a C&D member<br/><br/>
						
						<b>Your Member Login Information:</b><br/>
						User: ' . $newUser['username'] . '<br/>
						Password: ' . $pass . '<br/><br/>
						
						Now you can:
						•	Learning Center: <a href="http://www.C&D.com/support/learning-center/" target="_blank">[Link]</a> <br/>
						• 	Add Your Company Information: <a href="https://www.C&D.com/estore/edit-info/' . $newUser['username'] . '.html" target="_blank">[Link]</a><br/>
						•	Post Buying Request: <a href="https://www.C&D.com/post-buying-request" target="_blank">[Link]</a><br/>
						•	Registration Confirmation:  <a href="https://www.C&D.com/access/active/?code=' . $generatedKey . '" style="color:blue">' . Nine_Language::translate('Link') . '</a><br/>
						
						For any assistance, you can contact support@C&D.com. We look forward to meeting all your business needs.<br/>
						<br/>
						We wish you a successful business<br/>
						C&D Service Team.<br/>
						               
						This email was sent from C&D.COM<br/>
						Read our Privacy Policy and Terms of Use<br/>
						<img alt="C&D" src="https://www.nocndeal.com/layouts/default/helpers/front/assets/images/logo_nocndeal.png"><br/>
						[Address]: 2nd floor, 132-134 Dien Bien Phu, Dakao Ward, Ho Chi Minh City, Vietnam<br/>
						[Phone]: (+84) 8 3820 1386<br/>
						[Website]: http://www.C&D.com  -  http://www.miphabros.com<br/>
						[Email]: support@C&D.com <br/>
						               
					
					';

                    //                    $data_send = array(
//                        'name_to' => "C&D Service",
//                        'email_to' => array($newUser['email']),
//                        'title_email' => Nine_Language::translate('Your Membership Application Has Been Submitted'),
//                        'name_from' => Nine_Language::translate("C&D Service"),
//                        'content' => $content
//                    );
//                    $this->sendmail_smtp($data_send);
                    $objMail = new Models_Mail();
                    $objMail->sendHtmlMail('register', array('data' => $content), $newUser['email']);

                    $this->view->success = Nine_Language::translate('Congratulations, your registration successfully! Please check the email to confirm registration.');
                    unset($validate);
                } else {
                    $this->view->validate = $validate;
                }
            } else {
                $this->view->erro = Nine_Language::translate('Code is not match. Please type again');
            }
        }

        unset($_SESSION['captcha']);
        $this->view->randomNumber = rand(0, 1000000);
        $this->view->data = $data;

        $objCountry = new Models_CountryCategory();
        $allCountry = $objCountry->buildTree($objCountry->getAllCategoriesParentNull());
        $this->view->allCountry = $allCountry;
    }

    public function buyingRequestReplyAction() {
        if ($this->auth->isLogin()) {

            $userLogin = Nine_Registry::getLoggedInUser();

            $objBuyingCategory = new Models_BuyingCategory();
            $objCountry = new Models_CountryCategory();
            $this->setLayout('access');

            $numRowPerPage = 10;
            $currentPage = $this->_getParam("page", false);
            if ($currentPage == false) {
                $currentPage = 1;
            }

            $condition = array(
                'user_id_reply' => $userLogin['user_id']
            );
            $buyingRequest = $objBuyingCategory->getAllEnabledCategory($condition, array('view', 'buying_category_gid DESC'), $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
            foreach ($buyingRequest as &$item) {
                $country = $objCountry->getCategoryByGid($item['country_category_gid']);
                $item['country'] = $country['name'];
                $item['country_image'] = $country['images'];
                $item['description'] = Nine_Function::subStringAtBlank(trim(strip_tags($item['description'])), 30);
                unset($item);
            }

            $count = count($objBuyingCategory->getAllEnabledCategory($condition, 'country_category_gid DESC'));

            $this->setPagination($numRowPerPage, $currentPage, $count);

            $this->view->buyingRequest = $buyingRequest;
            $this->view->menu = 'buying-request-reply';
        } else {
            $this->_redirect('/');
        }
    }

    public function buyingRequestAction() {
        if ($this->auth->isLogin()) {

            $userLogin = Nine_Registry::getLoggedInUser();

            $objBuyingCategory = new Models_BuyingCategory();
            $objCountry = new Models_CountryCategory();
            $this->setLayout('access');

            $numRowPerPage = 10;
            $currentPage = $this->_getParam("page", false);
            if ($currentPage == false) {
                $currentPage = 1;
            }

            $condition = array(
                'user_id' => $userLogin['user_id']
            );
            $buyingRequest = $objBuyingCategory->getAllEnabledCategory($condition, array('view', 'buying_category_gid DESC'), $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
            foreach ($buyingRequest as &$item) {
                $country = $objCountry->getCategoryByGid($item['country_category_gid']);
                $item['country'] = $country['name'];
                $item['country_image'] = $country['images'];
                $item['description'] = Nine_Function::subStringAtBlank(trim(strip_tags($item['description'])), 30);
                unset($item);
            }

            $count = count($objBuyingCategory->getAllEnabledCategory($condition, 'country_category_gid DESC'));

            $this->setPagination($numRowPerPage, $currentPage, $count);

            $this->view->buyingRequest = $buyingRequest;
            $this->view->menu = 'buying-request';
        } else {
            $this->_redirect('/');
        }
    }

    public function buyingRequestReplyDetailAction() {

        if ($this->auth->isLogin()) {
            $objBuyingCategory = new Models_BuyingCategory();
            $objCountry = new Models_CountryCategory();
            $objBuying = new Models_Buying();
            $objBankConnect = new Models_ConnectBank();

            $this->setLayout('access');

            $alias = $this->_getParam('alias', false);

            if (false == $alias) {
                $this->_redirect('/');
            } else {
                $buyingRequest = $objBuyingCategory->getCategoryByAlias($alias);
                $objBuyingCategory->update(array('view' => 2), array('buying_category_gid = ? ' => $buyingRequest['buying_category_gid']));

                $country = $objCountry->getCategoryByGid($buyingRequest['country_category_gid']);
                $buyingRequest['price'] = number_format(floatval($buyingRequest['price']), 0, '.', ',');
                $buyingRequest['end_date'] = date('d-m-Y', $buyingRequest['end_date']);
                $buyingRequest['created_date'] = date('d-m-Y', $buyingRequest['created_date']);

                $conditionBuying = array('buying_category_gid' => $buyingRequest['buying_category_gid'], 'user_id' => Nine_Registry::getLoggedInUserId());
                $allBuying = $objBuying->getAllBuying($conditionBuying, array('sorting ASC', 'buying_gid DESC', 'buying_id ASC'));
                $selectedQuote = false;
                foreach ($allBuying as $index => $buying) {
                    if ($buying['enabled'] == 2) {
                        $selectedQuote = true;
                        $quote = $buying['buying_gid'];
                        break;
                    }
                }

                $connect_bank = current($objBankConnect->getByColumns(array('buying_category_gid=?' => $buyingRequest['buying_category_gid']))->toArray());

                $this->view->connect_bank = $connect_bank;

                $this->view->buyingRequest = $buyingRequest;
                $this->view->allBuying = $allBuying;
                $this->view->selectedQuote = $selectedQuote;
                $this->view->pageTitle = $buyingRequest['name'];
                $this->view->country = $country;
                $this->view->menu = 'buying-request-reply';
            }
        } else {
            $this->_redirect('/');
        }
    }

    public function buyingRequestDetailAction() {

        if ($this->auth->isLogin()) {
            $objBuyingCategory = new Models_BuyingCategory();
            $objCountry = new Models_CountryCategory();
            $objBuying = new Models_Buying();
            $objBankConnect = new Models_ConnectBank();

            $this->setLayout('access');

            $alias = $this->_getParam('alias', false);

            if (false == $alias) {
                $this->_redirect('/');
            } else {
                $buyingRequest = $objBuyingCategory->getCategoryByAlias($alias);
                $objBuyingCategory->update(array('view' => 2), array('buying_category_gid = ? ' => $buyingRequest['buying_category_gid']));

                $country = $objCountry->getCategoryByGid($buyingRequest['country_category_gid']);
                $buyingRequest['price'] = number_format(floatval($buyingRequest['price']), 0, '.', ',');
                $buyingRequest['end_date'] = date('d-m-Y', $buyingRequest['end_date']);
                $buyingRequest['created_date'] = date('d-m-Y', $buyingRequest['created_date']);

                $conditionBuying = array('buying_category_gid' => $buyingRequest['buying_category_gid']);
                $allBuying = $objBuying->getAllBuying($conditionBuying, array('sorting ASC', 'buying_gid DESC', 'buying_id ASC'));
                $selectedQuote = false;
                foreach ($allBuying as $index => $buying) {
                    if ($buying['enabled'] == 2) {
                        $selectedQuote = true;
                        $quote = $buying['buying_gid'];
                        break;
                    }
                }

                $connect_bank = current($objBankConnect->getByColumns(array('buying_category_gid=?' => $buyingRequest['buying_category_gid']))->toArray());

                $this->view->connect_bank = $connect_bank;

                $this->view->buyingRequest = $buyingRequest;
                $this->view->allBuying = $allBuying;
                $this->view->selectedQuote = $selectedQuote;
                $this->view->pageTitle = $buyingRequest['name'];
                $this->view->country = $country;
                $this->view->menu = 'buying-request';
            }
        } else {
            $this->_redirect('/');
        }
    }

    public function agreeQuoteAction() {
        if ($this->auth->isLogin()) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $objBuying = new Models_Buying();
            $objBuyingCategory = new Models_BuyingCategory();

            $gid = $this->_request->getParam('gid', false);
            $category_gid = $this->_request->getParam('category_gid', false);
            if (false == $gid) {
                $this->_redirect('/');
            }
            if ($gid != null && $gid != '') {
                $objBuying->update(array('enabled' => 2), array('buying_gid=?' => $gid));
                $objBuyingCategory->update(array('status' => 4), array('buying_category_gid=?' => $category_gid));

                $buyingCategory = $objBuyingCategory->getCategoryByGid($category_gid);

                $this->_redirect('user/buying-request/' . $buyingCategory['alias']);
            } else {
                $this->_redirect('/');
            }
        } else {
            $this->_redirect('/');
        }
    }

    public function contactAction() {
        $objContact = new Models_Contact();

        if ($this->auth->isLogin()) {

            $this->setLayout('access');
            $condition = array();
            $condition['no_parent'] = true;
            $condition['user_id'] = Nine_Registry::getLoggedInUserId();

            $type = $this->_getParam('type', null);
            $allContact = $objContact->getAllContactByUser($condition, array('contact_gid DESC', 'contact_id ASC'));

            $this->view->allContact = $allContact;
            $this->view->menu = 'contact';
            $this->view->condition = $condition;
            /*
             * Get breadcrumbs
             */
            $url = Nine_Route::url();
            $this->view->breadCrumbs = Nine_Common::getBreadCumbs(0, 'edit-info', array(
                        'name' => Nine_Language::translate("Liên hệ"),
                        'url' => $url['path'] . 'user/contact'
            ));
        } else {
            $this->_redirect('/');
        }
    }

    public function contactEstoreAction() {
        $objCategory = new Models_MessageCategory();

        if ($this->auth->isLogin()) {
            $this->setLayout('access');
            $condition = array();
            $condition['no_parent'] = true;
            $condition['user_from_id'] = Nine_Registry::getLoggedInUserId();
            $condition['parent_id'] = 0;

            $type = $this->_getParam('type', null);
            $allContact = $objCategory->setAllLanguages(true)->getAllCategories($condition, array('message_category_gid DESC', 'message_category_id ASC'));

            $this->view->allContact = $allContact;
            $this->view->menu = 'contact-estore';
            $this->view->condition = $condition;
            $this->view->categoryMessage = $this->session->categoryMessage;
            $this->session->categoryMessage = null;
        } else {
            $this->_redirect('/');
        }
    }

    public function contactDetailAction() {
        $objContact = new Models_Contact();
        $objCat = new Models_ContactCategory();
        $objLang = new Models_Lang();

        if ($this->auth->isLogin()) {
            $this->setLayout('access');

            $gid = $this->_getParam('gid', false);
            $lid = $this->_getParam('lid', false);
            if (false == $gid) {
                $this->_redirect('contact/admin/manage-contact');
            }

            $data = $this->_getParam('data', false);

            /**
             * Get all contact languages
             */
            $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
            $allContactLangs = $objContact->setAllLanguages(true)->getByColumns(array('contact_gid=?' => $gid))->toArray();

            $errors = array();
            if (false !== $data) {
                /**
                 * Insert new contact
                 */
                $description = $this->_getParam('description', false);

                /**
                 * Insert new contact
                 */
                $newContact = $data;
                $newContact['created_date'] = time();
                /**
                 * Change format date
                 */
                try {
                    /**
                     * Increase all current sortings
                     */
                    if ($description != null) {
                        $userSession = Nine_Registry::getLoggedInUser();
                        $newContact['user_id'] = Nine_Registry::getLoggedInUserId();
                        $newContact['tennguoigui'] = $userSession['full_name'];
                        $newContact['full_text'] = $description;
                        $newContact['parent_id'] = $gid;
                        $newContact['type'] = 1;
                        $newContact[2]['full_text'] = $description;
                        $newContact[1]['full_text'] = $description;

                        if (isset($_FILES['images']) && $_FILES['images']['tmp_name'] != '') {
                            $fileName = basename($_FILES['images']['name']);
                            $fileTmp = $_FILES['images']['tmp_name'];
                            $uploadPath = 'media/userfiles/user/' . $userSession['user_id'] . '/';
                            $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                            $uploadFile = $uploadDir . $fileName;

                            $ext_allow = array(
                                'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX'
                            );
                            $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
                            if (in_array($ext, $ext_allow)) {
                                move_uploaded_file($fileTmp, $uploadFile);
                                $newContact['images'] = $uploadPath . $fileName;
                            }
                        }
                        $objContact->insert($newContact);
                        $objContact->update(array('enabled' => 0), array('contact_gid=?' => $gid));

                        $objContact->update(array('admin_view' => 0), array('contact_gid=?' => $gid));

                        /* Start send mail */
//                        try{
//
//                            $contact = $objContact->getContactByGid($gid);
//                            $email = $contact['email'];
//                            $data['username'] = $contact['tennguoigui'];
//                            $data['message'] = $description;
//                            $data['websitename'] = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost();
//                            $objMail = new Models_Mail();
//                            $objMail->sendHtmlMail('contact_admin', $data, $email);
//
//                        }catch (Exception $e) {
//
//                        }
                        /* End */


                        /**
                         * Message
                         */
                        $this->session->contactMessage = array(
                            'success' => true,
                            'message' => Nine_Language::translate('Contact is created successfully.')
                        );
                    } else {
                        /**
                         * Message
                         */
                        $this->session->contactMessage = array(
                            'success' => true,
                            'message' => Nine_Language::translate('Not yet reply.')
                        );
                    }

                    $contact_category_gid = $newContact['contact_category_gid'];
                    $updated_date = time();
                    $objContact->update(array('updated_date' => $updated_date, 'contact_category_gid' => $contact_category_gid, 'enabled' => $data['enabled']), array('contact_gid=?' => $gid));

                    $this->_redirect('user/contact');
                } catch (Exception $e) {
                    echo '<pre>';
                    echo print_r($newContact);
                    echo '<pre>';
                    die;
                    $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
                }
            } else {
                /**
                 * Get old data
                 */
                $data = @reset($allContactLangs);
                $objCountry = new Models_CountryCategory();
                $country = current($objCountry->getCategoryByGid($data['quocgia']));
                $this->view->country = $country;
                if (false == $data) {
                    $this->session->contactMessage = array(
                        'success' => false,
                        'message' => Nine_Language::translate("Contact doesn't exist.")
                    );
                    $this->_redirect('user/contact');
                }
                /**
                 * Change date
                 */
                if (0 != $data['publish_up_date']) {
                    $data['publish_up_date'] = date('d/m/Y', $data['publish_up_date']);
                } else {
                    $data['publish_up_date'] = '';
                }
                if (0 != $data['publish_down_date']) {
                    $data['publish_down_date'] = date('d/m/Y', $data['publish_down_date']);
                } else {
                    $data['publish_down_date'] = '';
                }

                /**
                 * Get all lang contacts
                 */
                foreach ($allContactLangs as $contact) {
                    /**
                     * Rebuild readmore DIV
                     */
                    $contact['full_text'] = Nine_Function::combineTextWithReadmore($contact['intro_text'], $contact['full_text']);
                    $data[$contact['lang_id']] = $contact;
                }

                /**
                 * Add deleteable field
                 */
                if (null != @$data['contact_category_gid']) {
                    $cat = @reset($objCat->getByColumns(array('contact_category_gid' => $data['contact_category_gid']))->toArray());
                    $data['contact_deleteable'] = @$cat['contact_deleteable'];
                }
                $objContact->update(array('user_view' => 1), array('contact_gid=?' => $gid));
            }
            /**
             * Prepare for template
             */
            $this->view->errors = $errors;
            $this->view->data = $data;
            $this->view->headTitle(Nine_Language::translate('Edit Contact'));
            $this->view->menu = 'contact';


            $condition['parent_id'] = $gid;
            $allContact = $objContact->getAllContact($condition, array('created_date'));

            $this->view->allContact = $allContact;
            $this->view->titleContact = $allContact[0]['title'];
            $this->view->gid = $allContact[0]['contact_gid'];
            $this->view->support = $allContact[0]['cname'];
        } else {
            $this->_redirect('/');
        }
    }

    public function contactDetailEstoreAction() {
        $objCategory = new Models_MessageCategory();
        $objLang = new Models_Lang();
        $objContent = new Models_Content();
        $objUser = new Models_User();

        if ($this->auth->isLogin()) {
            $this->setLayout('access');

            $gid = $this->_getParam('gid', false);
            if (false == $gid) {
                $this->_redirect('user/contact-estore');
            }

            $data = $this->_getParam('data', false);


            /**
             * Get all category languages
             */
            $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
            $allCategoryLangs = $objCategory->setAllLanguages(true)->getByColumns(array('message_category_gid=?' => $gid))->toArray();

            $errors = array();
            $description = $this->_getParam('description', false);
            if (false !== $description) {


                $newCategory = $data;
                try {

                    if (null != $description) {
                        $userSession = Nine_Registry::getLoggedInUser();
                        $newCategory['parent_id'] = $gid;
                        $newCategory['type_contact'] = 1;
                        $newCategory['description'] = $description;
                        $newCategory['created_date'] = time();
                        $newCategory['user_from_id'] = Nine_Registry::getLoggedInUserId();
                        $newCategory[1]['description'] = $description;
                        $newCategory[2]['description'] = $description;

                        if (isset($_FILES['images']) && $_FILES['images']['tmp_name'] != '') {
                            $fileName = basename($_FILES['images']['name']);
                            $fileTmp = $_FILES['images']['tmp_name'];
                            $uploadPath = 'media/userfiles/user/' . $userSession['user_id'] . '/';
                            $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                            $uploadFile = $uploadDir . $fileName;

                            $ext_allow = array(
                                'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX'
                            );
                            $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
                            if (in_array($ext, $ext_allow)) {
                                move_uploaded_file($fileTmp, $uploadFile);
                                $newCategory['images'] = $uploadPath . $fileName;
                            }
                        }

                        try {
                            $objCategory->insert($newCategory);
                        } catch (Exception $e) {
                            echo '<pre>';
                            echo print_r($e);
                            echo '</pre>';
                            die;
                        }
                        $this->session->contactMessage = array(
                            'success' => true,
                            'message' => Nine_Language::translate('Contact is created successfully.')
                        );
                    } else {
                        /**
                         * Message
                         */
                        $this->session->contactMessage = array(
                            'success' => true,
                            'message' => Nine_Language::translate('Not yet reply.')
                        );
                    }
                    $updated_date = time();
                    $objCategory->update(array('updated_date' => $updated_date, 'admin_view' => 0), array('message_category_gid =?' => $gid));

                    $this->_redirect('user/contact-estore');
                } catch (Exception $e) {
                    $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
                }
            } else {
                /**
                 * Get old data
                 */
                $data = @reset($allCategoryLangs);
                if (false == $data) {
                    $this->session->categoryMessage = array(
                        'success' => false,
                        'message' => Nine_Language::translate("Category doesn't exist.")
                    );
                    $this->_redirect('user/contact-estore');
                }
                /**
                 * Get all lang categorys
                 */
                foreach ($allCategoryLangs as $category) {
                    $data[$category['lang_id']] = $category;
                }

                /**
                 * Get all child category
                 */
                $updated_date = time();
                $objCategory->update(array('updated_date' => $updated_date, 'user_view' => 1), array('message_category_gid =?' => $gid));
            }

            $user_to = $objUser->getByUserId($data['user_to_id']);
            $user_form = $objUser->getByUserId($data['user_from_id']);
            $data['user_to'] = $user_to;
            $this->view->menu = 'contact-estore';

            /**
             * Remove it and its childs from category list
             */
            $oldData = @reset($allCategoryLangs);
            $condition['parent_id'] = $gid;
            $allMessage = $objCategory->getAllMessage($condition);
            /**
             * Prepare for template
             */
            $this->view->allCats = $allMessage;
            $this->view->allLangs = $allLangs;
            $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
            $this->view->user_form = $user_form;
            $this->view->errors = $errors;
            $this->view->data = $data;
            $this->view->headTitle(Nine_Language::translate('Edit Category'));
        } else {
            $this->_redirect('/');
        }
    }

}
