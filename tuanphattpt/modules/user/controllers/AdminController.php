<?php

require_once 'modules/user/models/Group.php';
require_once 'modules/user/models/User.php';
require_once 'modules/user/models/UserProfile.php';
require_once 'modules/package/models/PackageCategory.php';
require_once 'modules/properties/models/PropertiesCategory.php';
require_once 'modules/content/models/Content.php';
require_once 'modules/content/models/ContentCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/country/models/CountryCategory.php';
require_once 'modules/user/models/Payment.php';

class user_AdminController extends Nine_Controller_Action_Admin {

    public function getInfoAction() {
        $this->setLayout('default');
        $objUser = new Models_User();

        $id = $this->_getParam('id', false);

        $oldUser = $objUser->getUser($id);

        $this->view->user = $oldUser;
    }

    public function newUserAction() {
        
        $objContent = new Models_Content();
        $objCategory = new Models_PackageCategory;
        
        /**
         * Check permission
         */        
        
        if (false == $this->checkPermission('new_user')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $allLangs = Nine_Language::getAllLanguages();
        $this->view->allLangs = $allLangs;


        $data = $this->_getParam('data', false);
        $profile = $this->_getParam('profile', false);
        $errors = array();
        if (false !== $data) {

            /**
             * Insert new user
             */
            if (isset($data['product_category_gid'])) {
                $data['product_category_gid'] = '||' . implode("||", $data['product_category_gid']) . '||';
            }
            $objUser = new Models_User();
            if ($data['time_start'] != '') {
                $tmp = explode("/", $data['time_start']);
                $time = mktime(0, 0, 0, $tmp[0], $tmp[1], $tmp[2]);
                $data['time_start'] = $time;
            }
            if ($data['time_end'] != '') {
                $tmp = explode("/", $data['time_end']);
                $time = mktime(0, 0, 0, $tmp[0], $tmp[1], $tmp[2]);
                $data['time_end'] = $time;
            }

            if (isset($data['category_id'])) {
                $data['category_id'] = '||' . implode("||", $data['category_id']) . '||';
            } else {
                $data['category_id'] = '';
            }

            $newUser = $data;



            $newUser['full_name'] = trim($data['first_name'] . " " . $data['last_name']);
            $newUser['created_date'] = time();

            //$errors = $objUser->validate($newUser);
            //if (true === $errors) {
            if (true) {
                $newUser['password'] = md5($newUser['password']);
                /**
                 * TODO Read date format from language table
                 */
                $target_dir = 'media/userfiles/user/';
                if ($_FILES["image"]["name"] != '') {
                    $target_dir = $target_dir . time() . '-' . basename($_FILES["image"]["name"]);
                    $uploadOk = 1;


                    // Check file size
                    if ($_FILES["image"]["size"] > 20000000) {
                        $errors['erro'] = 1;
                        $errors['mess'] = Nine_Language::translate('File Lớn Hơn 20Mb.');
                        $uploadOk = 0;
                    }

                    // Only GIF files allowed
                    if ($_FILES["image"]["type"] != "image/gif" && $_FILES["image"]["type"] != "image/jpg" && $_FILES["image"]["type"] != "image/png" && $_FILES["image"]["type"] != "image/jpeg") {
                        $errors['erro'] = 1;
                        $errors['mess'] = Nine_Language::translate('Chỉ nhận file png / jpg / gif / jpeg .');
                        $uploadOk = 0;
                    }

                    // Check if $uploadOk is set to 0 by an error
                    if ($uploadOk == 0) {
                        
                    } else {
                        if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir)) {
                            $newUser['image'] = $target_dir;
                            $data['image'] = $target_dir;
                        } else {
                            die;
                            $erro['mess'] = Nine_Language::translate('Sever Not Upload File . ');
                        }
                    }
                }

                unset($newUser['repeat_password']);
                try {
                    $newUser['created_date'] = time();
                    $id = $objUser->insert($newUser);

                    $objProfile = new Models_UserProfile();
                    $profile['user_id'] = $id;

                    $objProfile->insert($profile);
                    $this->createPath('media/userfiles/user/' . $id);

                    $this->session->userMessage = array(
                        'success' => true,
                        'message' => Nine_Language::translate('User is created successfully.')
                    );
                    $objLang = new Models_Lang();
                    $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
                    $data_news = array(
                        //'content_category_gid' => 89,
                        'sorting' => 1,
                        'video' => '',
                        'link' => '',
                        'genabled' => 1,
                        'tinnhanh' => 0,
                        'seo_title' => '',
                        'seo_keywords' => '',
                        'seo_description' => '',
                        2 => Array
                            (
                            'enabled' => 1,
                            'title' => 'Giới Thiệu',
                            'alias' => 'about-us',
                            'tag' => '',
                            'full_text' => '',
                        ),
                        1 => Array
                            (
                            'enabled' => 1,
                            'title' => 'About Us',
                            'alias' => 'about-us',
                            'tag' => '',
                            'full_text' => '',
                        )
                    );
                    $newContent = $data_news;
                    $newContent['created_date'] = time();
                    $newContent['publish_up_date'] = null;
                    $newContent['publish_down_date'] = null;

                    foreach ($allLangs as $index => $lang) {
                        list($newContent[$lang['lang_id']]['intro_text'], $newContent[$lang['lang_id']]['full_text']) = Nine_Function::splitTextWithReadmore($newContent[$lang['lang_id']]['full_text']);
                        if ($newContent[$lang['lang_id']]['alias'] == "") {
                            $newContent[$lang['lang_id']]['alias'] = $objContent->convert_vi_to_en($newContent[$lang['lang_id']]['title']);
                            $newContent[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", $newContent[$lang['lang_id']]['alias']));
                        }
                    }
                    if (1 > $newContent['sorting']) {
                        $newContent['sorting'] = 1;
                    }
                    $objContent->increaseSorting($newContent['sorting'], 1);
                    $newContent['user_id'] = $id;
                    if ($newContent['publish_up_date'] == '') {
                        $newContent['publish_up_date'] = date("D, d M Y H:i:s");
                    }
                    $objContent->insert($newContent);

                    $data_news = array(
                        //'content_category_gid' => 89,
                        'sorting' => 1,
                        'video' => '',
                        'link' => '',
                        'genabled' => 1,
                        'tinnhanh' => 0,
                        'seo_title' => '',
                        'seo_keywords' => '',
                        'seo_description' => '',
                        2 => Array
                            (
                            'enabled' => 1,
                            'title' => 'Dịch Vụ',
                            'alias' => 'service',
                            'tag' => '',
                            'full_text' => '',
                        ),
                        1 => Array
                            (
                            'enabled' => 1,
                            'title' => 'Service',
                            'alias' => 'service',
                            'tag' => '',
                            'full_text' => '',
                        )
                    );
                    $newContent = $data_news;
                    $newContent['created_date'] = time();
                    $newContent['publish_up_date'] = null;
                    $newContent['publish_down_date'] = null;

                    foreach ($allLangs as $index => $lang) {
                        list($newContent[$lang['lang_id']]['intro_text'], $newContent[$lang['lang_id']]['full_text']) = Nine_Function::splitTextWithReadmore($newContent[$lang['lang_id']]['full_text']);
                        if ($newContent[$lang['lang_id']]['alias'] == "") {
                            $newContent[$lang['lang_id']]['alias'] = $objContent->convert_vi_to_en($newContent[$lang['lang_id']]['title']);
                            $newContent[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", $newContent[$lang['lang_id']]['alias']));
                        }
                    }
                    if (1 > $newContent['sorting']) {
                        $newContent['sorting'] = 1;
                    }
                    $objContent->increaseSorting($newContent['sorting'], 1);
                    $newContent['user_id'] = $id;
                    if ($newContent['publish_up_date'] == '') {
                        $newContent['publish_up_date'] = date();
                    }
                    $objContent->insert($newContent);


                    $this->_redirect('user/admin/manage-user');
                } catch (Exception $e) {
                    $errors = array('main' => Nine_Language::translate('Can not insert into database now: ') . $e);
                }
            }
        }
        if ($data['time_start'] != '') {
            $data['time_start'] = date('m/d/Y', @$data['time_start']);
        }
        if ($data['time_end'] != '') {
            $data['time_end'] = date('m/d/Y', @$data['time_end']);
        }
        if ($data['category_id'] != '') {
            $data['category_id'] = explode("||", trim($data['category_id'], '||'));
        } else {
            $data['category_id'] = array();
        }
        $data['product_category_gid'] = explode('||', trim(@$data['product_category_gid'], '||'));
        /**
         * Prepare for template
         */
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->profile = $profile;
        $this->view->headTitle(Nine_Language::translate('New user'));
        /**
         * Get all groups
         */
        $objGroup = new Models_Group();
        $this->view->allGroups = $objGroup->getAll(array('sorting ASC', 'group_id ASC'))->toArray();

        $this->view->menu = array(
            0 => 'user',
            1 => 'new-user'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-user',
                'url' => Nine_Registry::getBaseUrl() . 'admin/user/admin/manage-user',
                'name' => Nine_Language::translate('Manager User')
            ),
            1 => array(
                'icon' => 'fa-user-plus',
                'url' => '',
                'name' => Nine_Language::translate('New User')
            )
        );

        $allCats = $objCategory->getAll(array('sorting ASC'))->toArray();
        $this->view->allCats = $allCats;

        $objCategoryProperties = new Models_PropertiesCategory;
        $allCatsProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(29));

        $this->view->allCatsProperties = $allCatsProperties;


        $objCatProduct = new Models_ProductCategory();
        $allCatsProduct = $objCatProduct->buildTree($objCatProduct->getAll(array('sorting ASC'))->toArray());
        $this->view->allCatsProduct = $allCatsProduct;

        $objCountry = new Models_CountryCategory();
        $allCountry = $objCountry->buildTree($objCountry->getAllCategoriesParentNull());
        $this->view->allCountry = $allCountry;
    }

    private function createPath($path) {
        if (is_dir($path)) {
            
        } else {
            $ret = mkdir($path); // use @mkdir if you want to suppress warnings/errors
        }

        return true;
    }

    public function editUserAction() {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_user')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $id = $this->_getParam('id', false);
        $data = $this->_getParam('data', false);

        $objLang = new Models_Lang();
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $this->view->allLangs = $allLangs;

        $objProfile = new Models_UserProfile();

        $profile = $this->_getParam('profile', false);

        if (false == $id) {
            $this->_redirect('user/admin/manage-user');
        }

        $objUser = new Models_User();
        $errors = array();

        /**
         * Get old user
         */
        $oldUser = $objUser->getUser($id);

        if (empty($oldUser)) {
            /**
             * User doesn't exsit
             */
            $this->session->userMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('User does NOT exist')
            );
            $this->_redirect('user/admin/manage-user#listofuser');
        }

        if (false !== $data) {
            /**
             * Update new user
             */
            if (isset($data['product_category_gid'])) {
                $data['product_category_gid'] = '||' . implode("||", $data['product_category_gid']) . '||';
            }
            if ($data['time_start'] != '') {
                $tmp = explode("/", $data['time_start']);
                $time = mktime(0, 0, 0, $tmp[0], $tmp[1], $tmp[2]);
                $data['time_start'] = $time;
            }
            if ($data['time_end'] != '') {
                $tmp = explode("/", $data['time_end']);
                $time = mktime(0, 0, 0, $tmp[0], $tmp[1], $tmp[2]);
                $data['time_end'] = $time;
            }
            if (isset($data['category_id'])) {
                $data['category_id'] = '||' . implode("||", $data['category_id']) . '||';
            } else {
                $data['category_id'] = '';
            }

            $newUser = $data;
            $newUser['full_name'] = trim($data['first_name'] . " " . $data['last_name']);
            $newUser['created_date'] = time();
            /**
             * Check permission
             */
            if ($oldUser['group_id'] != $newUser['group_id'] && false == $this->checkPermission('change_group')) {
                $newUser['group_id'] = $oldUser['group_id'];
            }
            $exclude = array();

            if (null == @$newUser['password'] && null == @$newUser['repeat_password']) {
                $exclude[] = 'password';
                unset($newUser['password']);
                unset($newUser['repeat_password']);
            }
            if ($newUser['email'] == $oldUser['email']) {
                $exclude[] = 'email';
                unset($newUser['email']);
            }
            if ($newUser['username'] == $oldUser['username']) {
                $exclude[] = 'username';
                unset($newUser['username']);
            }

            $errors = $objUser->validate($newUser, $exclude);

            if (true === $errors) {
                if (null != @$newUser['password']) {
                    $newUser['password'] = md5($newUser['password']);
                    unset($newUser['repeat_password']);
                }
                /**
                 * TODO Read date format from language table
                 */
                $target_dir = 'media/userfiles/user/';
                if ($_FILES["image"]["name"] != '') {
                    $target_dir = $target_dir . time() . '-' . basename($_FILES["image"]["name"]);
                    $uploadOk = 1;


                    // Check file size
                    if ($_FILES["image"]["size"] > 20000000) {
                        $errors['erro'] = 1;
                        $errors['mess'] = Nine_Language::translate('File Lớn Hơn 20Mb.');
                        $uploadOk = 0;
                    }

                    // Only GIF files allowed
                    if ($_FILES["image"]["type"] != "image/gif" && $_FILES["image"]["type"] != "image/jpg" && $_FILES["image"]["type"] != "image/png" && $_FILES["image"]["type"] != "image/jpeg") {
                        $errors['erro'] = 1;
                        $errors['mess'] = Nine_Language::translate('Chỉ nhận file png / jpg / gif / jpeg .');
                        $uploadOk = 0;
                    }

                    // Check if $uploadOk is set to 0 by an error
                    if ($uploadOk == 0) {
                        
                    } else {
                        if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir)) {
                            $newUser['image'] = $target_dir;
                            $data['image'] = $target_dir;
                        } else {
                            die;
                            $erro['mess'] = Nine_Language::translate('Sever Not Upload File . ');
                        }
                    }
                }
                try {

                    $objUser->update($newUser, array('user_id=?' => $id));

                    $profile_search = $objProfile->getByColumns("user_id = " . $id)->toArray();
                    if (count($profile_search) > 0) {
                        $objProfile->update($profile, array('user_id=?' => $id));
                    } else {
                        $profile['user_id'] = $id;

                        $objProfile->insert($profile);
                    }


                    /**
                     * Reload current login user
                     */
                    $loggedUser = $this->session->backendUser;
                    if ($oldUser ['user_id'] == @$loggedUser ['user_id']) {
                        $this->session->backendUser = $objUser->getByUserId($oldUser ['user_id'])->toArray();
                    }

                    $this->session->userMessage = array(
                        'success' => true,
                        'message' => Nine_Language::translate('User is updated successfully')
                    );
                    $this->_redirect('user/admin/manage-user');
                } catch (Exception $e) {
                    echo '<pre>';
                    echo print_r($e);
                    echo '<pre>';
                    die;
                    $errors = array('main' => Nine_Language::translate('Can not update user now'));

                    $this->session->userMessage = array(
                        'success' => false,
                        'message' => Nine_Language::translate('Can not update user now')
                    );
                }
            } else {
                echo '<pre>';
                echo print_r($errors);
                echo '<pre>';
                die;
            }
        } else {
            /**
             * Get current user
             */
            $data = $oldUser;
            $profile = $objProfile->setAllLanguages(true)->getByColumns("user_id = " . $id)->toArray();
            $tmp = array();
            foreach ($profile as $item) {
                $tmp[$item['lang_id']] = $item;
            }
            $profile = $tmp;
        }
        $data['product_category_gid'] = explode('||', trim(@$data['product_category_gid'], '||'));
        if ($data['time_start'] != '') {
            $data['time_start'] = date('m/d/Y', @$data['time_start']);
        }
        if ($data['time_end'] != '') {
            $data['time_end'] = date('m/d/Y', @$data['time_end']);
        }

        if ($data['category_id'] != '') {
            $data['category_id'] = explode("||", trim($data['category_id'], '||'));
        } else {
            $data['category_id'] = array();
        }
        /**
         * Prepare for template
         */
        $this->view->errors = $errors;
        $this->view->data = $data;

        $this->view->profile = $profile;
        $this->view->headTitle(Nine_Language::translate('Edit user'));

        /**
         * Get all groups
         */
        $objGroup = new Models_Group();
        $this->view->allGroups = $objGroup->getAll(array('sorting ASC', 'group_id ASC'))->toArray();

        $this->view->menu = array(
            0 => 'user',
            1 => 'manager-user'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-user',
                'url' => Nine_Registry::getBaseUrl() . 'admin/user/admin/manage-user',
                'name' => Nine_Language::translate('Manager User')
            ),
            1 => array(
                'icon' => 'fa-pencil',
                'url' => '',
                'name' => Nine_Language::translate('Edit User')
            )
        );
        $objCategory = new Models_PackageCategory;
        $allCats = $objCategory->getAll(array('sorting ASC'))->toArray();
        $this->view->allCats = $allCats;

        $objCategoryProperties = new Models_PropertiesCategory;
        $allCatsProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(29));
        $this->view->allCatsProperties = $allCatsProperties;

        $objCatProduct = new Models_ProductCategory();
        $allCatsProduct = $objCatProduct->buildTree($objCatProduct->getAll(array('sorting ASC'))->toArray());
        $this->view->allCatsProduct = $allCatsProduct;

        $objCountry = new Models_CountryCategory();
        $allCountry = $objCountry->buildTree($objCountry->getAllCategoriesParentNull());
        $this->view->allCountry = $allCountry;
    }

    public function manageUserAction() {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_user')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $this->view->headTitle(Nine_Language::translate('Manage Users'));

        $config = Nine_Registry::getConfig();

        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");

        $currentPage = $this->_getParam("page", false);
        $displayNum = $this->_getParam('displayNum', false);
        $objUser = new Models_User();

        $data = $this->_getParam('data', array());
        foreach ($data as $id => $value) {
            $value = intval($value);
            $objUser->update(array('group_id' => $value), array('user_id=?' => $id));
            $this->session->userMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("User Group is edited successfully")
            );
        }

        if ($currentPage == false) {
            $currentPage = 1;
            $this->session->userDisplayNum = null;
            $this->session->userCondition = null;
        }

        /**
         * Get number of users per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->userDisplayNum;
        } else {
            $this->session->userDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->userCondition;
        } else {
            $this->session->userCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }

        /**
         * Load all users
         */
        $objPackage = new Models_PackageCategory();
        $allUsers = $objUser->getAllUsersWithGroup($condition, 'user_id DESC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage
        );
        /**
         * Count all users
         */
        $count = count($objUser->getAllUsersWithGroup($condition));
        $this->view->condition = $condition;
        /**
         * Modify all users
         */
        foreach ($allUsers as &$user) {
            if (null != $user['created_date']) {
                $user['created_date'] = date($config['dateFormat'], $user['created_date']);
            }
            if (null != $user['last_login_date']) {
                $user['last_login_date'] = date($config['dateFormat'], $user['last_login_date']);
            }
            $user['package'] = current(@$objPackage->getByColumns("package_category_gid = " . $user['package_id'] . " and lang_id=" . Nine_Language::getCurrentLangId())->toArray());
        }
        unset($user);

        $export = $this->_getParam('export', false);
        if ($export != false) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $style = "style = 'border: 1px solid'";

            if (Nine_Language::getCurrentLangId() == 1) {

                $textHeader = array(
                    "<b>No</b>",
                    "<b>Username</b>",
                    "<b>Email</b>",
                    "<b>Full Name</b>",
                    "<b>Group</b>",
                    "<b>Create Date</b>",
                    "<b>Last Login</b>"
                );
            } else {

                $textHeader = array(
                    mb_convert_encoding("<b>Số Thứ Tự</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Tên Đăng Nhập</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Email</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Tên Đầy Đủ</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Nhóm</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Ngày Tạo</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Đăng Nhập Lần Trước</b>", 'HTML-ENTITIES', 'UTF-8')
                );
            }

            $header = "<tr>";
            foreach ($textHeader as $text) {
                $header .= "<td $style>" . utf8_encode($text) . "</td>";
            }
            $header .= "</tr>";

            $content = '';
            $no = 1;

            foreach ($allUsers as $item) {
                $content .= "<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['username'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['email'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['full_name'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['gname'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['created_date'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['last_login_date'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "</tr>";
                $no++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-product-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();
        }
        /**
         * Load all groups
         */
        $objGroup = new Models_Group();
        /**
         * Set values for tempalte
         */
        $this->view->allGroups = $objGroup->getAll(array('sorting ASC', 'group_id ASC'))->toArray();
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allUsers = $allUsers;
        $this->view->allPackages = $objPackage->getAll(array('sorting ASC', 'package_category_id'))->toArray();
        $this->view->userMessage = $this->session->userMessage;
        $this->session->userMessage = null;
        $this->view->displayNum = $numRowPerPage;

        $this->view->menu = array(
            0 => 'user',
            1 => 'manager-user'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-user',
                'url' => Nine_Registry::getBaseUrl() . 'admin/user/admin/manage-user',
                'name' => Nine_Language::translate('Manager User')
            )
        );
    }

    public function enableUserAction() {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_user')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('user/admin/manage-user');
        }

        $ids = explode('_', trim($id, '_'));

        $objUser = new Models_User();
        try {
            foreach ($ids as $id) {
                $objUser->update(array('enabled' => 1), array('user_id=?' => $id));
            }
            $this->session->userMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('User is activated successfully')
            );
        } catch (Exception $e) {
            $this->session->userMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT enable this user. Please try again')
            );
        }
        $this->_redirect('user/admin/manage-user#listofuser');
    }

    public function disableUserAction() {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_user')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('user/admin/manage-user');
        }

        $ids = explode('_', trim($id, '_'));

        $objUser = new Models_User();
        try {
            foreach ($ids as $id) {
                $objUser->update(array('enabled' => 0), array('user_id=?' => $id, 'username !=?' => 'admin'));
            }
            $this->session->userMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('User is deactived successfully')
            );
        } catch (Exception $e) {
            $this->session->userMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT disable this user. Please try again')
            );
        }
        $this->_redirect('user/admin/manage-user#listofuser');
    }

    public function deleteUserAction() {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_user')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('user/admin/manage-user');
        }

        $ids = explode('_', trim($id, '_'));

        $objUser = new Models_User();
        try {
            foreach ($ids as $id) {
                $objUser->delete(array('user_id=?' => $id, 'username !=?' => 'admin'));
            }
            $this->session->userMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('User is deleted successfully')
            );
        } catch (Exception $e) {
            $this->session->userMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT delete this user. Please try again')
            );
        }
        $this->_redirect('user/admin/manage-user#listofuser');
    }

    /**
     * ######################################  GROUP FUNCTIONS ##############################################
     */
    public function manageGroupAction() {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_group')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $this->view->headTitle(Nine_Language::translate('Manage Groups'));

        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page", false);
        $displayNum = $this->_getParam('displayNum', false);

        if ($currentPage == false) {
            $currentPage = 1;
            $this->session->groupDisplayNum = null;
        }

        /**
         * Get number of groups per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->groupDisplayNum;
        } else {
            $this->session->groupDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Load all groups
         */
        $objGroup = new Models_Group();
        $allGroups = $objGroup->getAll('sorting ASC')->toArray();
        /**
         * Count all groups
         */
        $count = count($allGroups);
        /**
         * Load all groups
         */
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allGroups = $allGroups;
        $this->view->groupMessage = $this->session->groupMessage;
        $this->session->groupMessage = null;
        $this->view->displayNum = $numRowPerPage;

        $this->view->menu = array(
            0 => 'user',
            1 => 'manager-group'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-users',
                'url' => Nine_Registry::getBaseUrl() . 'admin/user/admin/manage-group',
                'name' => Nine_Language::translate('Manager Group')
            )
        );
    }

    public function enableGroupAction() {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_group')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('user/admin/manage-group');
        }

        $ids = explode('_', trim($id, '_'));
        $objGroup = new Models_Group();
        try {
            foreach ($ids as $id) {
                $objGroup->update(array('enabled' => 1), array('group_id=?' => $id));
            }
            $this->session->groupMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Enable group successfully')
            );
        } catch (Exception $e) {
            $this->session->groupMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT enable this group. Please try again')
            );
        }
        $this->_redirect('user/admin/manage-group#listofgroup');
    }

    public function disableGroupAction() {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_group')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('user/admin/manage-group');
        }

        $ids = explode('_', trim($id, '_'));
        $objGroup = new Models_Group();
        try {
            foreach ($ids as $id) {
                $objGroup->update(array('enabled' => 0), array('group_id=?' => $id));
            }
            $this->session->groupMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Disable group successfully')
            );
        } catch (Exception $e) {
            $this->session->groupMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT disable this group. Please try again')
            );
        }
        $this->_redirect('user/admin/manage-group#listofgroup');
    }

    public function editGroupAction() {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_group')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $id = $this->_getParam('id', false);
        $data = $this->_getParam('data', false);

        if (false == $id) {
            $this->_redirect('user/admin/manage-group');
        }

        $objGroup = new Models_Group();
        $errors = array();
        /**
         * Get old group
         */
        $oldGroup = $objGroup->find($id)->current()->toArray();

        if (empty($oldGroup)) {
            /**
             * Group doesn't exsit
             */
            $this->session->groupMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Group does NOT exist')
            );
            $this->_redirect('user/admin/manage-group#listofgroup');
        }

        if (false !== $data) {
            /**
             * Update new group
             */
            $newGroup = array(
                'name' => $data['name'],
                'default' => $data['default'],
                'description' => $data['description'],
                'color' => $data['color'],
                'enabled' => $data['enabled'],
                'sorting' => $data['sorting']
            );

            if (null == $newGroup['name']) {
                $errors['name'] = Nine_Language::translate('Name is required');
            }
            if (null == $newGroup['color']) {
                $errors['color'] = Nine_Language::translate('Color is required');
            }
            if (null == $newGroup['sorting']) {
                $errors['sorting'] = Nine_Language::translate('Sorting is required');
            }

            if (empty($errors)) {
                try {
                    $objGroup->update($newGroup, array('group_id=?' => $id));
                    /**
                     * Reload current login group
                     */
                    $this->_redirect('user/admin/manage-group');
                } catch (Exception $e) {
                    $errors = array('main' => Nine_Language::translate('Can not update group now'));
                }
            }
        } else {
            /**
             * Get current group
             */
            $data = $oldGroup;
        }
        /**
         * Prepare for template
         */
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit group'));
        $this->view->menu = array(
            0 => 'user',
            1 => 'manager-group'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-users',
                'url' => Nine_Registry::getBaseUrl() . 'admin/user/admin/manage-group',
                'name' => Nine_Language::translate('Manager Group')
            ),
            1 => array(
                'icon' => 'fa-pencil',
                'url' => '',
                'name' => Nine_Language::translate('Edit Group')
            )
        );
    }

    public function managePaymentAction() {
        $objPayment = new Models_Payment();


        $this->view->headTitle(Nine_Language::translate('manage Product '));



        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page", false);
        $displayNum = $this->_getParam('displayNum', false);
        if ($currentPage == false) {
            $currentPage = 1;
            $this->session->productDisplayNum = null;
            $this->session->productCondition = null;
        }
        if (false === $displayNum) {
            $displayNum = $this->session->productDisplayNum;
        } else {
            $this->session->productDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->productCondition;
        } else {
            $this->session->productCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }

        $numRowPerPage = $numRowPerPage;
        $allPayments = $objPayment->getAllUsersWithUser($condition, 'payment_id DESC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage
        );
        /**
         * Count all users
         */
        $count = count($objPayment->getAllUsersWithUser($condition));
        foreach ($allPayments as &$user) {
            if (null != $user['time']) {
                $user['time'] = date($config['dateFormat'], $user['time']);
            }
        }
        unset($user);

        /**
         * Set values for tempalte
         */
//        echo print_r($condition); die;
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allPayments = $allPayments;
        $this->view->productMessage = $this->session->productMessage;
        $this->session->productMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;

        $this->view->menu = array(
            0 => 'payment',
            1 => 'manager-payment'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-product-hunt',
                'url' => Nine_Registry::getBaseUrl() . 'admin/user/admin/manage-payment',
                'name' => Nine_Language::translate('Manager Payment')
            )
        );
    }

    public function enablePaymentAction() {
        $objPayment = new Models_Payment();
        $objUser = new Models_User();
        $gid = $this->_getParam('gid', false);
        if (false == $gid) {
            $this->_redirect('user/admin/manage-payment');
        }
        try {

            $payment = current($objPayment->getByColumns(array('payment_id=?' => $gid))->toArray());

            $oldUser = $objUser->getUser($payment['user_id']);
            $updateUser = array(
                'time_end' => $oldUser['time_end'] + ($payment['package_id'] * 365 / 2 * 86400),
                'group_id' => 3
            );
            $objUser->update($updateUser, array('user_id = ?' => $oldUser['user_id']));
            $objPayment->update(array('status' => 2), array('payment_id = ?' => $gid));

            $this->session->productMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Payment is enable successfully')
            );


            $this->_redirect('user/admin/manage-payment');
        } catch (Exception $e) {
            echo '<pre>';
            echo print_r($e);
            echo '</pre>';
            die;
        }
    }

}
