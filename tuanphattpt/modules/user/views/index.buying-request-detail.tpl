<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{$buyingRequest.name}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row buydetail">
            {{sticker name=front_user_menu}}
            <div class="col-md-4">
                <div class="col-md-12">
                    <h2>{{$buyingRequest.name}}</h2>
                    <dl class="dl-horizontal">
                        <dt>{{l}}Estimate order quantity{{/l}}</dt>
                        <dd>{{$buyingRequest.so_luong}} {{$buyingRequest.pname}}</dd>
                        <dt>{{l}}Target price{{/l}}</dt>
                        <dd>{{$buyingRequest.price}} {{$buyingRequest.pname2}}</dd>
                        <dt>{{l}}Time left{{/l}}</dt>
                        <dd>{{$buyingRequest.end_date}}</dd>
                        <dt>{{l}}Date posted{{/l}}</dt>
                        <dd>{{$buyingRequest.created_date}}</dd>
                        <dt>{{l}}Location Delivery{{/l}}</dt>
                        <dd><img style="width: 20px" src="{{$BASE_URL}}{{$country.images}}"/> {{$country.name}}</dd>
                    </dl>
                </div>
                <div class="col-md-12">
                    <h2>{{l}}Request Detail{{/l}}</h2>
                    <p>
                        {{$buyingRequest.description}}
                    </p>
                </div>
            </div>
            <div class="col-md-8">
                <div class="col-md-12">
                    {{if $allBuying|@count > 0 && $selectedQuote == true}}
                    <h2 style="color: red">{{l}}Estore Selected{{/l}}</h2>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{l}}Estore{{/l}}</th>
                            <th>{{l}}Quote Date{{/l}}</th>
                            <th>{{l}}Action{{/l}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{foreach from=$allBuying item=item}}
                        {{if $item.enabled == 2}}
                        <tr>
                            <td>{{$item.name_company}}</td>
                            <td>{{$item.created_date|date_format:"%d-%m-%Y"}}</td>
                            <td>
                                {{if $selectedQuote == false}}
                                <button class="btn btn-sm btn-info" onclick="window.location.href='{{$BASE_URL}}user/agree-quote/{{$item.buying_gid}}/{{$item.buying_category_gid}}'">
                                    {{l}}Agree quote{{/l}}
                                </button>
                                {{/if}}
                                <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#buying_{{$item.buying_gid}}">
                                    {{l}}Quotation Detail{{/l}}
                                </button>
                                {{if $connect_bank != ''}}
                                	<a class="btn btn-sm btn-info" href="#">
	                                    {{l}}Đã Kết Nối Tài Chính{{/l}}
	                                </a>
                                {{else}}
	                                <a class="btn btn-sm btn-info" href="{{$BASE_URL}}connect/{{$item.buying_category_gid}}/bank.html">
	                                    {{l}}Kết Nối Tài Chính{{/l}}
	                                </a>
                                {{/if}}
                                <div class="modal fade" id="buying_{{$item.buying_gid}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Quotation Detail</h4>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table table-striped">
                                                    <tr>
                                                        <td>{{l}}Price{{/l}}</td>
                                                        <td>
                                                            {{if $item.price_sale != null}}
                                                            {{$item.price_sale|number_format:0:".":","}} {{$item.pname}}
                                                            {{else}}
                                                            {{l}}No Price{{/l}}
                                                            {{/if}}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{l}}Quantity{{/l}}</td>
                                                        <td>{{$item.quantity}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{l}}Unit{{/l}}</td>
                                                        <td>{{$item.pname2}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Quotation Content</td>
                                                        <td>{{$item.full_text}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Files Attached</td>
                                                        {{if $item.file_attached != ''}}
                                                        <td><a href="{{$BASE_URL}}{{$item.file_attached}}">Download Files</a></td>
                                                        {{else}}
                                                        {{l}}No attach file{{/l}}
                                                        {{/if}}
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        {{/if}}
                        {{/foreach}}
                        </tbody>
                    </table>
                    {{/if}}
                </div>

                <div class="col-md-12">
                    {{if $allBuying|@count > 0}}
                        <h2>{{l}}Buying Request Quote{{/l}}</h2>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>{{l}}Estore{{/l}}</th>
                                <th>{{l}}Quote Date{{/l}}</th>
                                <th>{{l}}Action{{/l}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{foreach from=$allBuying item=item}}
                                {{if $item.enabled == 1}}
                                    <tr>
                                        <td>{{$item.name_company}}</td>
                                        <td>{{$item.created_date|date_format:"%d-%m-%Y"}}</td>
                                        <td>
                                            {{if $selectedQuote == false}}
                                            <button class="btn btn-sm btn-info" onclick="window.location.href='{{$BASE_URL}}user/agree-quote/{{$item.buying_gid}}/{{$item.buying_category_gid}}'">
                                                {{l}}Agree quote{{/l}}
                                            </button>
                                            {{/if}}
                                            <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#buying_{{$item.buying_gid}}">
                                                {{l}}Quotation Detail{{/l}}
                                            </button>
                                            <div class="modal fade" id="buying_{{$item.buying_gid}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Quotation Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table class="table table-striped">
                                                                <tr>
                                                                    <td>{{l}}Price{{/l}}</td>
                                                                    <td>
                                                                        {{if $item.price_sale != null}}
                                                                        {{$item.price_sale|number_format:0:".":","}} {{$item.tiente}}
                                                                        {{else}}
                                                                        {{l}}No Price{{/l}}
                                                                        {{/if}}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>{{l}}Quantity{{/l}}</td>
                                                                    <td>{{$item.quantity}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>{{l}}Unit{{/l}}</td>
                                                                    <td>{{$item.cachtinh}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Quotation Content</td>
                                                                    <td>{{$item.full_text}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Files Attached</td>
                                                                    {{if $item.file_attached != ''}}
                                                                    <td><a href="{{$BASE_URL}}{{$item.file_attached}}">Download Files</a></td>
                                                                    {{else}}
                                                                    {{l}}No attach file{{/l}}
                                                                    {{/if}}
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                {{/if}}
                            {{/foreach}}
                            </tbody>
                        </table>
                    {{/if}}
                </div>
            </div>

        </div>
        <!-- ./row-->
    </div>
</div>
