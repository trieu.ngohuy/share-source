


<div class="page-header">
	<h1>
		{{l}}User Group{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}Edit User Group{{/l}}
		</small>
	</h1>
</div>

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
			<div class="row">
				<div class="col-xs-12">
						{{if $errors|@count > 0}}
	                        <div class="alert alert-danger">
	                        	<button class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
									{{if $errors.main}}
	                                   </strong> {{$errors.main}}
	                                {{else}}
	                                   {{l}}Please check following information again{{/l}}
	                                {{/if}} 
							</div>
	                	{{/if}}
	                     
	                        
	                	<div class="col-sm-12 widget-container-col ui-sortable">
	                	
										<div class="tabbable tabs-left">
										
											<ul class="nav nav-tabs" id="myTab2">
												<li class="active">
													<a href="#tab2" data-toggle="tab" aria-expanded="true">
														<i class="pink ace-icon fa fa-info bigger-110"></i>
														{{l}}Detail{{/l}}
													</a>
												</li>
											</ul>
													
											<form action="" method="post" class="form-horizontal">
												<div class="tab-content">
												
													<div id="tab2" class="tab-pane active">
													
														
														<div class="form-group has-info">
															<label class="control-label col-md-2">{{l}}Name{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[name]" value="{{$data.name}}" required data-fv-notempty-message="{{l}}The name is empty{{/l}}" >
															</div>
														</div>
														<div class="form-group has-info">
															<label class="control-label col-md-2">{{l}}Color{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[color]"  value="{{$data.color}}" required data-fv-notempty-message="{{l}}The color is empty{{/l}}" >
															</div>
														</div>
														<div class="form-group has-info">
															<label class="control-label col-md-2">{{l}}Sorting{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[sorting]"  value="{{$data.sorting}}" required data-fv-notempty-message="{{l}}The sorting is empty{{/l}}" >
															</div>
														</div>
														<div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Enable group{{/l}}</label>
																<div class="col-md-10">
																		<label class="radio-inline">
																				<input type="radio" name="data[enabled]" value="1" {{if $data.enabled != '0'}}checked="checked"{{/if}}/> Yes
																		</label>
																		<label class="radio-inline">
																			<input type="radio" name="data[enabled]" value="0" {{if $data.enabled == '0'}}checked="checked"{{/if}}/> No
																		</label>
																</div>
														</div>
														<div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Default{{/l}}</label>
																<div class="col-md-10">
																		<label class="radio-inline">
																				<input type="radio" name="data[default]" value="1" {{if $data.default != '0'}}checked="checked"{{/if}}/> Yes
																		</label>
																		<label class="radio-inline">
																			<input type="radio" name="data[default]" value="0" {{if $data.default == '0'}}checked="checked"{{/if}}/> No
																		</label>
																</div>
														</div>
						                                <div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Description{{/l}}</label>
																<div class="col-md-10">
																		<textarea class="form-control" rows="5" id="textarea" name="data[description]">{{$data.description}}</textarea>
																</div>
														</div>
														
														
														<div class="clearfix form-actions">
															<div class="col-md-offset-3 col-md-9">
																<button class="btn btn-info" type="submit">
																	<i class="ace-icon fa fa-check bigger-110"></i>
																	{{l}}Save{{/l}}
																</button>
															</div>
														</div>
														
														
													</div>
													
													
													
												</div>
											
											</form>
											
										</div>
										
									</div>
					
					
					
				</div><!-- /.span -->
				
			</div><!-- /.row -->
		
		
		
		</div>
		
		
	</div>
			