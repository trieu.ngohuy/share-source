


<div class="page-header">
	<h1>
		{{l}}User Group{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}List All User Group{{/l}}
		</small>
	</h1>
</div>

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">
                        {{if $allGroups|@count <= 0}}
                        <div class="alert alert-info">
                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
								{{l}}No group in system now.{{/l}}
						</div>
                        {{/if}}
                        
                        {{if $groupMessage|@count > 0 && $groupMessage.success == true}}
                        <div class="alert alert-success">
                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
								{{$groupMessage.message}}.
						</div>
                        {{/if}}
                        
                        {{if $groupMessage|@count > 0 && $groupMessage.success == false}}
                        <div class="alert alert-danger">
                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
								{{$groupMessage.message}}.
						</div>
                        {{/if}}
                        
                
				<table id="simple-table" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<form id="top-search" name="search" method="post" action="">
									<th class="center">{{l}}No.{{/l}}</th>
									<th class="center"><input class="check-all" type="checkbox" /></th>
	                            	<th>ID</th>
                                   	<th>{{l}}Group{{/l}}</th>
                                   	<th>{{l}}Description{{/l}}</th>
                                   	<th class="center">{{l}}Sorting{{/l}}</th>
                                   	<th class="center">{{l}}Enabled{{/l}}</th>
                                   	<th class="center">{{l}}Action{{/l}}</th>
                        	</form>
						</tr>
					</thead>

					<tbody>
						{{if $allGroups|@count > 0}}
												
												
							{{foreach from=$allGroups item=item key=key}}
								<tr>
									<td class="center">{{$key+1}}</td>
                                   	<td class="center"><input type="checkbox" value="{{$item.group_id}}" name="allGroups" class="allGroups"/></td>
                                    <td>{{$item.group_id}}</td>
                                    <td><span style="
                                                        {{if $item.color}} color: {{$item.color}}; {{/if}}
                                                ">{{$item.name}}</span></td>
					                                    <td>{{$item.description}}</td>
					                                    <td class="center">{{$item.sorting}}</td>
					                                    <td class="center">
					                                        {{if $item.enabled == '1'}}
					                                            <a href="{{$APP_BASE_URL}}user/admin/disable-group/id/{{$item.group_id}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png"></a>
					                                        {{else}}
					                                            <a href="{{$APP_BASE_URL}}user/admin/enable-group/id/{{$item.group_id}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png"></a>
					                                        {{/if}}
					                                    </td>
					                                    <td class="center">
					                                        <span class="tooltip-area">
																<a href="{{$APP_BASE_URL}}user/admin/edit-group/id/{{$item.group_id}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
															</span>
					                                    </td>
                                    
													</tr>
												{{/foreach}}
						{{/if}}
					</tbody>
				</table>
				
				
			</div><!-- /.span -->
		</div><!-- /.row -->
		
		
		<div class="col-lg-12">
							<div class="form-group col-lg-4">
								<label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
								<div class="col-lg-6">
									<select id="action" class="form-control" >
		                                <<option value=";">{{l}}Choose an action...{{/l}}</option>
		                            	<option value="enableGroup();">{{l}}Enable Group{{/l}}</option>
		                                <option value="disableGroup();">{{l}}Disable Group{{/l}}</option>
		                            </select>
		                        </div>
		                        <div class="col-lg-6">
	                            	<a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
	                            </div>
							</div>
							<div class="form-group col-lg-2">
								<label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
								<div class="col-lg-12">
									<form class="search" name="search" method="post" action="">
		                                <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
		                                    <option value="5" {{if $displayNum == 5}} selected="selected" {{/if}}>5</option>
		                                    <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
		                                    <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
		                                    <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
		                                    <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
		                                    <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
		                                </select>
		                            </form>
		                        </div>
							</div>
							{{if $countAllPages > 1}}
							<div class="col-lg-6 pagination">
								{{if $first}}
	                            <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
	                            {{/if}}
	                            {{if $prevPage}}
	                            <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
	                            {{/if}}
	                            
	                            {{foreach from=$prevPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>
	                            
	                            {{foreach from=$nextPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            {{if $nextPage}}
	                            <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
	                            {{/if}}
	                            {{if $last}}
	                            <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
	                            {{/if}}
                            
							</div>
							{{/if}}
						</div>
					</div>
		
		
	</div>
</div>
	
<script language="javascript" type="text/javascript">
$(document).ready(function(){
    $('.close').click(function(){
		$(this).parent().hide( "slow");
    });
    $('.check-all').click(function(){
    	if(this.checked) { // check select status
            $('.allGroups').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.allGroups').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
});
function applySelected()
{
	var task = document.getElementById('action').value;
	eval(task);
}
function enableGroup()
{
    var all = document.getElementsByName('allGroups');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('Please choose an group');
    }
    window.location.href = '{{$APP_BASE_URL}}user/admin/enable-group/id/' + tmp;
}

function disableGroup()
{
    var all = document.getElementsByName('allGroups');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('Please choose an group');
    }
    window.location.href = '{{$APP_BASE_URL}}user/admin/disable-group/id/' + tmp;
}

</script>
							