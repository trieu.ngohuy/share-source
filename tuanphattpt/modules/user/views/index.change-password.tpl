<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        {{sticker name=common_breadcrumbs}}
        <!-- breadcrumb -->
        <div class="row">
            {{sticker name=front_user_menu}}
            <div class="col-md-12 col-xs-12 " id="contact">
                <div class="col-md-12 col-xs-12" style="margin-bottom: 15px">
                    <h4>{{l}}Change Password{{/l}}</h4>
                </div>
                <div class="col-md-4">
                    <p>{{l}}You can change or reset the password by providing some information.{{/l}}</p>
                </div>
                <div class="col-md-4">
                    {{if $userMessage.success == true && isset($userMessage.changeSuccess)}}
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>
                        {{$userMessage.changeSuccess}}.
                    </div>
                    {{/if}}
                    <form role="form" data-toggle="validator" method="post">
                        <div class="contact-form-box col-md-12">
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Current Password{{/l}}</label>
                                <input type="password" class="form-control input-sm" name="data[current-password]" id="title" required />

                                {{if $userMessage.success == false && isset($userMessage.messagePasswordNotCorrect)}}
                                <p class="alert alert-danger">
                                    <button class="close" data-dismiss="alert">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    {{$userMessage.messagePasswordNotCorrect}}
                                </p>
                                {{/if}}
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}New Password{{/l}}</label>
                                <input type="password" class="form-control input-sm" name="data[new-password]" id="title" required />

                                {{if $userMessage.success == false && isset($userMessage.messageLenghtPassword)}}
                                <p class="alert alert-danger">
                                    <button class="close" data-dismiss="alert">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    {{$userMessage.messageLenghtPassword}}
                                </p>
                                {{/if}}
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Confirm New Password{{/l}}</label>
                                <input type="password" class="form-control input-sm" name="data[repeat-password]" id="title" required />

                                {{if $userMessage.success == false && isset($userMessage.messageRepeatPassword)}}
                                <p class="alert alert-danger">
                                    <button class="close" data-dismiss="alert">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    {{$userMessage.messageRepeatPassword}}
                                </p>
                                {{/if}}
                            </div>
                            <div class="form-selector">
                                <button type="submit" id="btn-contact" class="btn btn-primary">{{l}}Save{{/l}}</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>