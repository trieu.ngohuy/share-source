<div class="container hs-code">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        {{sticker name=common_breadcrumbs}}
        <!-- breadcrumb -->
        <div class="row">
            {{sticker name=front_user_menu}}
            <div class="col-sm-12">
                <h3>{{l}}Payment{{/l}}</h3>
            </div>
            <div class="col-sm-12">
                {{if $upgrate.success == true}}
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$upgrate.message}}.
                </div>
                {{/if}}
                {{if $upgrate.success === false}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$upgrate.message}}.
                </div>
                {{/if}}
                <table class="table table-striped table-bordered">
                    <form id="top-search" name="search" method="post" action="">
                        <thead>
                            <tr>
                                <th>
                                    {{l}}NO.{{/l}}
                                </th>
                                <th>
                                    {{l}}Ngày Nâng Cấp tài Khoản{{/l}}
                                </th>
                                <th>
                                    {{l}}Kiểu Nâng Cấp{{/l}}
                                <th>
                                    {{l}}Gói Nâng Cấp{{/l}}
                                </th>
                                <th>
                                    {{l}}Status{{/l}}
                                </th>
                            </tr>
                        </thead>
                    </form>

                    <tbody>
                        {{foreach from=$allPayments item=item key=key}}
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>
                                {{$item.time}}
                            </td>
                            <td>
                                {{if $item.method == '1'}}
                                {{l}}Visa, Master, JCB, Amex{{/l}}
                                {{elseif $item.method == '2'}}
                                {{l}}ATM{{/l}}
                                {{/if}}
                            </td>
                            <td>
                                {{if $item.packed == '1'}}
                                {{l}}Gói Nâng Cấp 6 Tháng{{/l}}
                                {{elseif $item.packed == '2'}}
                                {{l}}Gói Nâng Cấp 1 Năm{{/l}}
                                {{/if}}
                            </td>
                            <td>
                                {{if $item.status == '1'}}
                                {{l}}Pendding{{/l}}
                                {{elseif $item.status == '2'}}
                                {{l}}Approve{{/l}}
                                {{elseif $item.status == '3'}}
                                {{l}}Not Approve{{/l}}
                                {{/if}}
                            </td>
                        </tr>
                        {{/foreach}}
                    </tbody>
                </table>
            </div>
            <div class="col-md-12 sortPagiBar">
                {{if $countAllPages > 1}}
                <div class="bottom-pagination">
                    <nav>
                        <ul class="pagination">
                            {{if $prevPage}}
                            <li>
                                <a href="?page={{$prevPage}}" aria-label="Prev">
                                    <span aria-hidden="true">&laquo; Prev</span>
                                </a>
                            </li>
                            {{/if}}

                            {{foreach from=$prevPages item=item}}
                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                {{/foreach}}

                            <li class="active"><a>{{$currentPage}}</a></li>

                            {{foreach from=$nextPages item=item}}
                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                {{/foreach}}

                            {{if $nextPage}}
                            <li>
                                <a href="?page={{$nextPage}}" aria-label="Next">
                                    <span aria-hidden="true">Next &raquo;</span>
                                </a>
                            </li>
                            {{/if}}
                        </ul>
                    </nav>
                </div>
                {{/if}}
            </div>
        </div>
    </div>
</div>
