<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>  
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<script type="text/javascript">
    //<![CDATA[

    jQuery(document).ready(function (){
        CKFinder.setupCKEditor( null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/' );
        jQuery( "#images" ).sortable();
        jQuery( "#images" ).disableSelection();
        //Make slug
        {{foreach from=$allLangs item=item name=langDiv}}
        jQuery('#title{{$smarty.foreach.langDiv.iteration}}').makeSlug({
            slug: jQuery('#alias{{$smarty.foreach.langDiv.iteration}}')
        });
        {{/foreach}}
        	//Display images
            jQuery(".input_image[value!='']").parent().find('div').each( function (index, element){
                jQuery(this).toggle();
            });
    });
    var imgId;
    function chooseImage(id)
    {
        imgId = id;
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();
        finder.basePath = '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/'; // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.selectActionFunction = setFileField;
        finder.popup();
    } 
    // This is a sample function which is called when a file is selected in CKFinder.
    function setFileField( fileUrl )
    {
        document.getElementById( 'chooseImage_img' + imgId ).src = fileUrl;
        document.getElementById( 'chooseImage_input' + imgId).value = fileUrl;
        document.getElementById( 'chooseImage_div' + imgId).style.display = '';
        document.getElementById( 'chooseImage_noImage_div' + imgId ).style.display = 'none';
    }
    function clearImage(imgId)
    {
        document.getElementById( 'chooseImage_img' + imgId ).src = '';
        document.getElementById( 'chooseImage_input' + imgId ).value = '';
        document.getElementById( 'chooseImage_div' + imgId).style.display = 'none';
        document.getElementById( 'chooseImage_noImage_div' + imgId).style.display = '';
    }

    function addMoreImg()
    {
        jQuery("ul#images > li.hidden").filter(":first").removeClass('hidden');
    }

//]]>
</script>
<style type="text/css">
    #images { list-style-type: none; margin: 0; padding: 0;}
    #images li { margin: 10px; float: left; text-align: center;  height: 180px;}
</style>

<div class="page-header">
	<h1>
		{{l}}User{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}Edit User{{/l}}
		</small>
	</h1>
</div>

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
			<div class="row">
				<div class="col-xs-12">
						{{if $errors|@count > 0}}
	                        <div class="alert alert-danger">
	                        	<button class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
									{{if $errors.main}}
	                                   </strong> {{$errors.main}}
	                                {{else}}
	                                   {{l}}Please check following information again{{/l}}
	                                {{/if}} 
							</div>
	                	{{/if}}
	                        
	                        
	                	<div class="col-sm-12 widget-container-col ui-sortable">
	                	
										<div class="tabbable tabs-left">
										
											<ul class="nav nav-tabs" id="myTab2">
												<li class="active">
													<a href="#tab1" data-toggle="tab" aria-expanded="true">
														<i class="pink ace-icon fa fa-home bigger-110"></i>
														{{l}}Basic{{/l}}
													</a>
												</li>
												<li>
													<a href="#tab3" data-toggle="tab" aria-expanded="true">
														<i class="pink ace-icon fa fa-info bigger-110"></i>
														{{l}}Estore{{/l}}
													</a>
												</li>
												{{foreach from=$allLangs item=item index=index name=langTab}}
													<li>
														<a href="#tab_{{$item.lang_id}}" data-toggle="tab" aria-expanded="true">
															<image style="vertical-align:middle;" src="{{$BASE_URL}}{{$item.lang_image}}"> {{$item.name}}
														</a>
													</li>
												{{/foreach}}
											</ul>
													
											<form action="" method="post" class="form-horizontal"  enctype="multipart/form-data">
												<div class="tab-content">
												
													<div id="tab1" class="tab-pane active ">
														<div class="form-group col-md-9">
															<div class="form-group has-info col-md-12">
																	<label class="control-label col-md-2">{{l}}Group{{/l}}</label>
																	<div class="col-md-10">
																			<select name="data[group_id]" class="form-control">
																					{{foreach from=$allGroups item=item}}
												                                        <option value="{{$item.group_id}}" {{if $data.group_id == $item.group_id}}selected="selected"{{/if}}>{{$item.name}}</option>
												                                    {{/foreach}}
																			</select>
																	</div>
															</div>
															<div class="form-group has-info col-md-12">
																<label class="control-label col-md-2">{{l}}Username{{/l}}</label>
																<div class="col-md-10">
																		<input type="text" class="form-control" name="data[username]" value="{{$data.username}}" required data-fv-notempty-message="{{l}}The username is empty{{/l}}">
																</div>
															</div>
														</div>
														
														<input style="display: none;" type="file" name="image" id="ip_avatar">
														<input type="hidden" value="{{$data.image}}" name="data[image]">
														<div class="form-group col-md-3 " id="select_avatar">
															<div id="avatar" style="{{if $data.image == ''}}display: none;{{/if}}">
						                          				<img src="{{$BASE_URL}}{{$data.image}}" id="display_avatar" style="max-width: 100%; max-height:80px; border:dashed blue thin;"></img>
						                                     </div>
						                                  	<div id="no_avatar" style="{{if $data.image != ''}}display: none;{{/if}}width: 100%; border: thin blue dashed; text-align: center; padding:38px 0px;">
						                                		Logo
						                               		</div>
														</div>
															
														<br style="clear: both;">
														<div class="form-group has-info" style="display: none;">
															<label class="control-label col-md-2">{{l}}Type User{{/l}}</label>
															<div class="col-md-10">
																<select name="data[user_active]" class="chosen-select form-control" id="form-field-select-3" data-placeholder="{{l}}Choose a Type User...{{/l}}">
																	<option value="1" {{if $data.user_active == 1}}selected="selected"{{/if}}>{{l}}User Free{{/l}}</option>
																	<option value="2" {{if $data.user_active == 2}}selected="selected"{{/if}}>{{l}}Eser Fee{{/l}}</option>
																</select>
															</div>
														</div>
														<div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Enable user{{/l}}</label>
																<div class="col-md-10">
																		<label class="radio-inline">
																				<input type="radio" name="data[enabled]" value="1" {{if $data.enabled != '0'}}checked="checked"{{/if}}/> Yes
																		</label>
																		<label class="radio-inline">
																			<input type="radio" name="data[enabled]" value="0" {{if $data.enabled == '0'}}checked="checked"{{/if}}/> No
																		</label>
																</div>
														</div>
									                	<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Password{{/l}}</label>
															<div class="col-md-10">
																	<input type="password" class="form-control" name="data[password]" value=""   data-fv-notempty-message="{{l}}The password is empty{{/l}}">
															</div>
														</div>
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Repeat Password{{/l}}</label>
															<div class="col-md-10">
																	<input type="password" class="form-control" name="data[repeat_password]" value=""   data-fv-notempty-message="{{l}}The repeat password is empty{{/l}}">
															</div>
														</div>
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Email Manager{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[email]"  value="{{$data.email}}"  required data-fv-notempty-message="{{l}}The email is empty{{/l}}">
															</div>
														</div>
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Email Company{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[email_company]"  value="{{$data.email_company}}"  required data-fv-notempty-message="{{l}}The email is empty{{/l}}">
															</div>
														</div>
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}First Name{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[first_name]"  value="{{$data.first_name}}"  required data-fv-notempty-message="{{l}}The first name is empty{{/l}}">
															</div>
														</div>
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Last Name{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[last_name]"  value="{{$data.last_name}}"  required data-fv-notempty-message="{{l}}The last name is empty{{/l}}">
															</div>
														</div>
														
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Phone{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[phone]"  value="{{$data.phone}}"   >
															</div>
														</div>
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Mobile{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[mobile]"  value="{{$data.mobile}}"   >
															</div>
														</div>
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Fax{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[fax]"  value="{{$data.fax}}"   >
															</div>
														</div>
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Quốc Gia{{/l}}</label>
															<div class="col-md-10">
																<select name="data[country]" class="chosen-select form-control" id="form-field-select-3" data-placeholder="{{l}}Choose a Country...{{/l}}">
																	{{foreach from=$allCountry item=item}}
																	<option value="{{$item.country_category_gid}}" {{if $data.country==$item.country_category_gid}} selected="selected" {{/if}}>{{$item.name}}</option>
																	{{/foreach}}
																</select>
															</div>
														</div>
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Tỉnh Thành{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[city]"  value="{{$data.city}}"   >
															</div>
														</div>
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Website{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[website]"  value="{{$data.website}}"   >
															</div>
														</div>
														
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Mã Bưu Chính{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[mbc]"  value="{{$data.mbc}}"   >
															</div>
														</div>
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Mã Số Doanh Nghiệp{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[mst]"  value="{{$data.mst}}"   >
															</div>
														</div>
														
														
<!--						                                <footer class="panel-footer">-->
<!--															<div class="form-group">-->
<!--																<label class="control-label col-md-3">{{l}}Banner:{{/l}}</label>-->
<!--																<div class="col-md-9">-->
<!--																	<p><a href="javascript:addMoreImg()">{{l}}+ Add more images{{/l}}</a></p> -->
<!--									                                <ul id="images">-->
<!--									                                    {{foreach from=$data.banner item=item key=i name=images }} -->
<!--									                                     <li {{if null == $item}}class="hidden"{{/if}}>-->
<!--									                                            <input class="input_image" type="hidden" id="chooseImage_input{{$i}}" name="data[banner][]" value="{{if $item}}{{$BASE_URL}}{{$item}}{{/if}}">-->
<!--									                                            <div id="chooseImage_div{{$i}}" style="display: none;">-->
<!--									                                                <img src="{{if $item}}{{$BASE_URL}}{{$item}}{{/if}}" id="chooseImage_img{{$i}}" style="max-width: 150px; max-height:150px; border:dashed thin;"></img>-->
<!--									                                            </div>-->
<!--									                                            <div id="chooseImage_noImage_div{{$i}}" style="width: 150px; border: thin dashed; text-align: center; padding:70px 0px;">-->
<!--									                                                No image-->
<!--									                                            </div>-->
<!--									                                            <br/>-->
<!--									                                            <a href="javascript:chooseImage({{$i}});">{{l}}Choose image{{/l}}</a>-->
<!--									                                            | -->
<!--									                                            <a href="javascript:clearImage({{$i}});">{{l}}Delete{{/l}}</a>-->
<!--									                                    </li>-->
<!--									                                    {{/foreach}}-->
<!--									                                </ul>-->
<!--																</div>-->
<!--															</div>-->
<!--						                                -->
<!--							                                -->
<!--														</footer>   -->
						                                
														<div class="clearfix form-actions">
															<div class="col-md-offset-3 col-md-9">
																<button class="btn btn-info" type="submit">
																	<i class="ace-icon fa fa-check bigger-110"></i>
																	{{l}}Save{{/l}}
																</button>
															</div>
														</div>
													</div>
													
													<div id="tab3" class="tab-pane">
														
														<div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Category Product{{/l}}</label>
																<div class="col-md-10">
																		<select multiple="multiple" size="20" name="data[product_category_gid][]" id="duallist_cate">
																			{{foreach from=$allCatsProduct item=item key=key}}
																				<option value="{{$item.product_category_gid}}" {{if $item.product_category_gid|in_array:$data.product_category_gid}} selected="selected"{{/if}}>{{$item.name}}</option>
																			{{/foreach}}
																		</select>
							
																		<!-- /section:plugins/input.duallist -->
																		<div class="hr hr-16 hr-dotted"></div>
																</div>
														</div>
														<div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Hình Thức Kinh Doanh{{/l}}</label>
																<div class="col-md-10">
																		<select multiple="multiple" size="10" name="data[category_id][]" id="duallist">
																			{{foreach from=$allCatsProperties item=item key=key}}
																				{{if $item.parent_id != ''}}
																				<option value="{{$item.properties_category_gid}}" {{if $item.properties_category_gid|in_array:$data.category_id }} selected="selected"{{/if}}>{{$item.name}}</option>
																				{{/if}}
																			{{/foreach}}
																		</select>
							
																		<!-- /section:plugins/input.duallist -->
																		<div class="hr hr-16 hr-dotted"></div>
																</div>
														</div>
														
														
														<div class="form-group has-info">
															<label class="control-label col-md-2">{{l}}Năm Đăng Ký{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[namdangky]"  value="{{$data.namdangky}}"   >
															</div>
														</div>
														<div class="form-group has-info">
															<label class="control-label col-md-2">{{l}}Số Nhân Viên{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[sonhanvien]"  value="{{$data.sonhanvien}}"   >
															</div>
														</div>
														<div class="form-group has-info">
															<label class="control-label col-md-2">{{l}}Quy Mô Công Ty{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[quymo]"  value="{{$data.quymo}}"   >
															</div>
														</div>
														
														<div class="form-group has-info">
															<label class="control-label col-md-2">{{l}}Hạn Sử Dụng{{/l}}</label>
															<div class="col-md-10">
																	<div class="row">
																		<div class="col-xs-8 col-sm-11">
																			<div class="input-daterange input-group">
																				<input type="text"  name="data[time_start]"  value="{{$data.time_start}}"  class="input-sm form-control">
																				<span class="input-group-addon">
																					<i class="fa fa-exchange"></i>
																				</span>
			
																				<input type="text"  name="data[time_end]"  value="{{$data.time_end}}"   class="input-sm form-control">
																			</div>
			
																			<!-- /section:plugins/date-time.datepicker -->
																		</div>
																	</div>
															</div>
														</div>
														
														<div class="form-group has-info" style="display: none;">
																<label class="control-label col-md-2">{{l}}Package{{/l}}</label>
																<div class="col-md-10">
																		<select name="data[package_id]" class="chosen-select form-control" id="form-field-select-3" data-placeholder="Choose a State...">
																			<option value="">{{l}}No Package{{/l}}</option>
										                                    {{foreach from=$allCats item=item}}
										                                        <option value="{{$item.package_category_gid}}" {{if $data.package_id == $item.package_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
										                                    {{/foreach}}
																		</select>
																</div>
														</div>

														<div class="form-group has-info">
															<label class="control-label col-md-2">{{l}}Template{{/l}}</label>
															<div class="col-md-10">
																<select name="data[template]" class="chosen-select form-control" id="form-field-select-3" data-placeholder="Choose a Template...">
																	<option value="1" {{if $data.template == 1}}selected="selected"{{/if}}>{{l}}Template 1{{/l}}</option>
																	<option value="2" {{if $data.template == 2}}selected="selected"{{/if}}>{{l}}Template 2{{/l}}</option>
																	<option value="3" {{if $data.template == 3}}selected="selected"{{/if}}>{{l}}Template 3{{/l}}</option>
																</select>
															</div>
														</div>
														<div class="form-group has-info"">
															<label class="control-label col-md-2">{{l}}Alias{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[alias]"  value="{{$data.alias}}"  required="" >
															</div>
														</div>
														
														<div class="clearfix form-actions">
															<div class="col-md-offset-3 col-md-9">
																<button class="btn btn-info" type="submit">
																	<i class="ace-icon fa fa-check bigger-110"></i>
																	{{l}}Save{{/l}}
																</button>
															</div>
														</div>
													</div>
													{{foreach from=$allLangs item=item name=langDiv}}
														<div class="tab-pane" id="tab_{{$item.lang_id}}">
																<div class="form-group has-info"">
																	<label class="control-label col-md-2">{{l}}Tên Công Ty{{/l}}</label>
																	<div class="col-md-10">
																			<input type="text" class="form-control" name="profile[{{$item.lang_id}}][company_name]"  value="{{$profile[$item.lang_id].company_name}}"   >
																	</div>
																</div>
																<div class="form-group has-info"">
																	<label class="control-label col-md-2">{{l}}Địa Chỉ{{/l}}</label>
																	<div class="col-md-10">
																			<input type="text" class="form-control" name="profile[{{$item.lang_id}}][add]"  value="{{$profile[$item.lang_id].add}}"   >
																	</div>
																</div>
																<div class="form-group has-info"">
																	<label class="control-label col-md-2">{{l}}Sản Phẩm Chính{{/l}}</label>
																	<div class="col-md-10">
																			<input type="text" class="form-control" name="profile[{{$item.lang_id}}][product_chinh]"  value="{{$profile[$item.lang_id].product_chinh}}"   >
																	</div>
																</div>
																<div class="form-group has-info"">
																	<label class="control-label col-md-2">{{l}}Sản Phẩm Khác{{/l}}</label>
																	<div class="col-md-10">
																			<input type="text" class="form-control" name="profile[{{$item.lang_id}}][product_khac]"  value="{{$profile[$item.lang_id].product_khac}}"   >
																	</div>
																</div>
								                                <div class="form-group has-info"">
																		<label class="control-label col-md-2">{{l}}Điểm Nổi Bật{{/l}}</label>
																		<div class="col-md-10">
																				<textarea class="form-control" rows="5" id="textarea" name="profile[{{$item.lang_id}}][note]">{{$profile[$item.lang_id].note}}</textarea>
																		</div>
																</div>
																<div class="form-group has-info">
																	<label class="control-label col-md-2">{{l}}Mô Tả Chi Tiết{{/l}}</label>
																	<div class="col-md-10">
																			<textarea style="float:left;" class="text-input textarea ckeditor"  name="profile[{{$item.lang_id}}][about]" rows="20" cols="90">{{$profile[$item.lang_id].about}}</textarea>
																	</div>
																</div>
																
																
								                                
								                                <div class="clearfix form-actions">
																	<div class="col-md-offset-3 col-md-9">
																		<button class="btn btn-info" type="submit">
																			<i class="ace-icon fa fa-check bigger-110"></i>
																			{{l}}Save{{/l}}
																		</button>
																	</div>
																</div> 
														</div>
													
													{{/foreach}}
													
													
												</div>
											
											</form>
											
										</div>
										
									</div>
					
					
					
				</div><!-- /.span -->
				
			</div><!-- /.row -->
		
		
		
		</div>
		
		
	</div>
			
			
			<script type="text/javascript">
	jQuery(document).ready(function (){
			$('#select_avatar').click(function(event) {
			  $('#ip_avatar').click();
			});

			$('#ip_avatar').change(function(event) {
				var tmppath = URL.createObjectURL(event.target.files[0]);
			  	$('#display_avatar').attr('src',URL.createObjectURL(event.target.files[0]));
			  	$("#no_avatar").css("display", "none");
			  	$("#avatar").css("display", "block");
			});
	
	});
	
	
</script>