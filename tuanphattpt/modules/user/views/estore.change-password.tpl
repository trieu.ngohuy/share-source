<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<style type="text/css">
    #images { list-style-type: none; margin: 0; padding: 0;}
    #images li { margin: 10px; float: left; text-align: center;  height: 180px;}
</style>

<div class="page-header">
    <h1>
        {{l}}Infomation{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}Change Password{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">
                {{if $userMessage.success == true && isset($userMessage.changeSuccess)}}
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>
                        {{$userMessage.changeSuccess}}.
                    </div>
                {{/if}}

                <div class="col-sm-12 widget-container-col ui-sortable">

                    <div class="tabbable tabs-left">

                        <ul class="nav nav-tabs" id="myTab2">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab" aria-expanded="true">
                                    <i class="pink ace-icon fa fa-home bigger-110"></i>
                                    {{l}}Basic{{/l}}
                                </a>
                            </li>
                        </ul>

                        <form action="" method="post" class="form-horizontal"  enctype="multipart/form-data">
                            <div class="tab-content">
                                <div id="tab1" class="tab-pane active ">
                                    <div class="form-group has-info">
                                        <label class="control-label col-md-2">{{l}}Current Password{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control" id="current-password" name="data[current-password]"/>
                                            <p class="alert alert-danger empty-current-password" style="display: none">
                                                <button class="close" data-dismiss="alert">
                                                    <i class="ace-icon fa fa-times"></i>
                                                </button>
                                                {{l}}Current Password is not empty.{{/l}}
                                            </p>
                                            {{if $userMessage.success == false && isset($userMessage.messagePasswordNotCorrect)}}
                                                <p class="alert alert-danger">
                                                    <button class="close" data-dismiss="alert">
                                                        <i class="ace-icon fa fa-times"></i>
                                                    </button>
                                                    {{$userMessage.messagePasswordNotCorrect}}
                                                </p>
                                            {{/if}}

                                        </div>
                                    </div>
                                    <div class="form-group has-info">
                                        <label class="control-label col-md-2">{{l}}New Password{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control" id="new-password" name="data[new-password]"/>
                                            <p class="alert alert-danger empty-new-password" style="display: none">
                                                <button class="close" data-dismiss="alert">
                                                    <i class="ace-icon fa fa-times"></i>
                                                </button>
                                                {{l}}New Password is not empty.{{/l}}
                                            </p>
                                            {{if $userMessage.success == false && isset($userMessage.messageLenghtPassword)}}
                                                <p class="alert alert-danger">
                                                    <button class="close" data-dismiss="alert">
                                                        <i class="ace-icon fa fa-times"></i>
                                                    </button>
                                                    {{$userMessage.messageLenghtPassword}}
                                                </p>
                                            {{/if}}
                                        </div>
                                    </div>
                                    <div class="form-group has-info">
                                        <label class="control-label col-md-2">{{l}}Repeat Password{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control" id="repeat-password" name="data[repeat-password]"/>
                                            <p class="alert alert-danger empty-repeat-password" style="display: none">
                                                <button class="close" data-dismiss="alert">
                                                    <i class="ace-icon fa fa-times"></i>
                                                </button>
                                                {{l}}Repeat Password is not empty.{{/l}}
                                            </p>
                                            {{if $userMessage.success == false && isset($userMessage.messageRepeatPassword)}}
                                                <p class="alert alert-danger">
                                                    <button class="close" data-dismiss="alert">
                                                        <i class="ace-icon fa fa-times"></i>
                                                    </button>
                                                    {{$userMessage.messageRepeatPassword}}
                                                </p>
                                            {{/if}}
                                        </div>
                                    </div>
                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Save{{/l}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var check = true;
    var currentPassword = $(':input[name="data[current-password]"]');
    var newPassword = $(':input[name="data[new-password]"]');
    var repeatPassword = $(':input[name="data[repeat-password]"]');
    
    $(':button').click(function() {
        if(currentPassword.val() == '') {
            $('.empty-current-password').css({display: "block"});
            check = false;
        } else {
            $('.empty-current-password').css({display: "none"});
        }

        if(newPassword.val() == '') {
            $('.empty-new-password').css({display: "block"});
            check = false;
        } else {
            $('.empty-new-password').css({display: "none"});
        }

        if(repeatPassword.val() == '') {
            $('.empty-repeat-password').css({display: "block"});
            check = false;
        } else {
            $('.empty-repeat-password').css({display: "none"});
        }

        if(currentPassword.val() != '' && newPassword.val() != '' && repeatPassword.val() != '') {
            check = true;
        }

        return check;
    });

</script>
