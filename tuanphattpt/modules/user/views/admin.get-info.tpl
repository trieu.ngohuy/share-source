<div class="col-sm-12">
	<ul>
		
		<li>	{{l}}Tên Đầy Đủ{{/l}}
			<ul class="list-unstyled">
				<li>
					<i class="ace-icon fa fa-caret-right blue"></i>
					{{$user.full_name}}
				</li>
			</ul>
		</li>
		<li>	{{l}}User Name{{/l}}
			<ul class="list-unstyled">
				<li>
					<i class="ace-icon fa fa-caret-right blue"></i>
					{{$user.username}}
				</li>
			</ul>
		</li>
		<li>	{{l}}Email{{/l}}
			<ul class="list-unstyled">
				<li>
					<i class="ace-icon fa fa-caret-right blue"></i>
					{{$user.email}}
				</li>
			</ul>
		</li>
		<li>	{{l}}Địa Chỉ{{/l}}
			<ul class="list-unstyled">
				<li>
					<i class="ace-icon fa fa-caret-right blue"></i>
					{{$user.add}}
				</li>
			</ul>
		</li>
		<li>	{{l}}Phone{{/l}}
			<ul class="list-unstyled">
				<li>
					<i class="ace-icon fa fa-caret-right blue"></i>
					{{$user.phone}}
				</li>
			</ul>
		</li>
		
	</ul>
</div>
<br class="cb">