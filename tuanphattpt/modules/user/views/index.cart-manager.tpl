<div class="container">	
    <br />
    <!-- breadcrumb -->
    {{sticker name=common_breadcrumbs}}
    <!-- breadcrumb -->
    <!--Front Menu-->
    {{sticker name=front_user_menu}}		
    <br />
    <h2>{{l}}Cart Manager{{/l}}</h2>
    <br />
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-1">#</div>
                <div class="col-md-3">{{l}}Chi tiết đơn hàng{{/l}}</div>
                <div class="col-md-2">{{l}}Ngày đặt{{/l}}</div>
                <div class="col-md-2">{{l}}Trạng thái{{/l}}</div>
                <div class="col-md-2">{{l}}Tổng tiền{{/l}}</div>
                <div class="col-md-2">{{l}}Action{{/l}}</div>
            </div>
        </div>
        <div class="panel-body">
            {{foreach from=$listCart item=item}}
            <div class="row">
                <div class="col-md-1">{{$item.cart_id}}</div>
                <div class="col-md-3">
                    {{assign var='count' value=1}}
                    {{foreach from=$item.detail item=item2 key=key}}
                    <ul>
                        <li><a href="{{$item2.products.url}}">{{$count}}. {{$item2.products.title}}</a></li>
                    </ul>
                    {{assign var='count' value=$count+1}}
                    {{/foreach}}
                </div>
                <div class="col-md-2">{{$item.created_date}}</div>
                <div class="col-md-2">
                    {{if $item.enabled == 1}}
                    {{l}}Chờ Duyệt Đơn{{/l}}
                    {{elseif $item.enabled == 2}}
                    {{l}}Đang Duyệt Đơn{{/l}}
                    {{elseif $item.enabled == 3}}
                    {{l}}Đã Duyệt Đơn{{/l}}
                    {{/if}}
                </div>
                <div class="col-md-2">{{$item.total_number}}{{$config.currency}}</div>
                <div class="col-md-2">
                    <a href="javascript:void()" class="cartDetail" data-cart-id="{{$item.cart_id}}">{{l}}Detail{{/l}}</a>
                    |
                    <a href="javascript:void()" class="cartRemove" data-cart-id="{{$item.cart_id}}">{{l}}Remove{{/l}}</a>
                </div>
            </div>
            <hr>
            {{/foreach}}
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{l}}Cart Detail{{/l}}</h4>
                </div>
                <div class="modal-body alg-c">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>			
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //Convert list cart to javascript

        //Show cart detail
        $(".cartDetail").click(function () {
            $("#myModal").modal("show");
            //Get cart id
            var cart_id = $(this).attr("data-cart-id");
            //Estable list products
            var content = '<table class="table profile-cart-table">'
                    + '<tbody>'
                    + '<tr class="profile-cart-header">'
                    + '<td>{{l}}ID{{/l}}</td>'
                    + '<td>{{l}}Name{{/l}}</td>'
                    + '<td>{{l}}Image{{/l}}</td>'
                    + '<td>{{l}}Price{{/l}}</td>'
                    + '<td>{{l}}Mount{{/l}}</td>'
                    + '<td>{{l}}Total{{/l}}</td>'
                    + '</tr>	';
    {{foreach from=$listCart item=item}}
            if ('{{$item.cart_id}}' == cart_id) {
                content += '<tr>'
    {{foreach from=$item.detail item=item2}}
                + '<td>{{$item2.products.product_gid}}</th>'
                        + '<td>{{$item2.products.title}}</td>'
                        + '<td><img src="{{$url.path}}{{$item2.products.main_image}}" style="width: 50px;"/></td>'
                        + '<td>{{if $item2.products.price == ""}}{{$item2.products.price}}{{else}}{{$item2.products.price_sale}}{{/if}}</td>'
                        + '<td>{{$item2.mount}}</td>'
                        + '<td>{{$item2.products.total_price}}VNĐ</td>'
                        + '</tr>';
    {{/foreach}}
            }
    {{/foreach}}
            content += '  </tbody></table>';
            $(".modal-body").html(content);
        });
        $(".cartRemove").click(function () {
            /*
             * Show modal
             */
            $("#myModal").modal("show");
            /*
             * Set content to modal
             */
            $(".modal-body").html(
                    '<p>{{l}}Đang xóa dữ liệu...{{/l}}</p>' +
                    '<img src="{{$LAYOUT_HELPER_URL}}front/assets/images/loading.gif" alt="Loading"/>');

            //Get cart id
            var cart_id = $(this).attr("data-cart-id");
            /*
             * Ajax remove cart
             */
            $.ajax({
                type: 'post',
                url: '{{$APP_BASE_URL}}cart/index/remove-cart/cartId/' + cart_id,
                //data: (data),
                success: function (response) {
                    if (response.status == false) {
                        $(".modal-body").html("<p>Có lỗi xảy ra!!!</p>");
                    } else {
                        $(".modal-body").html("<p>Xóa cart thành công!!!</p>");
                        /*
                         * Reload lại trang
                         */
                        location.reload();
                    }
                },
                error: function (xhr, status, error) {
                    $(".modal-body").html("<p>Có lỗi xảy ra!!!</p>");
                }
            });
        });
    });
</script>