<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{l}}Seller{{/l}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block filter -->
                {{sticker name=front_default_category}}
                <!-- ./block filter  -->

                 {{foreach from=$adv item=item}}
				    <div class="col-xs-12 col-sm-12 col-md-12 m10t qc" style="padding: 0px">
				        <a href="{{$item.link}}"><img alt="Banner right 1" src="{{$BASE_URL}}{{$item.images}}"></a>
				    </div>
				{{/foreach}}
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <div class="product-tab">
                    <ul class="nav-tab">
                        <li>
                            <a aria-expanded="true" data-toggle="tab">{{l}}Seller{{/l}}</a>
                        </li>
                    </ul>
                    <div class="tab-container">
                        <div id="product-detail" class="tab-panel active">
                            <div class="sortPagiBar clearfix">
                                <span class="page-noite">Showing {{$firstItemInPage}} to {{$lastItemInPage}} of {{$count}} ({{$countAllPages}} Page)</span>
                                {{if $countAllPages > 1}}
                                <div class="bottom-pagination">
                                    <nav>
                                        <ul class="pagination">
                                            {{if $prevPage}}
                                            <li>
                                                <a href="?page={{$prevPage}}" aria-label="Prev">
                                                    <span aria-hidden="true">&laquo; Prev</span>
                                                </a>
                                            </li>
                                            {{/if}}

                                            {{foreach from=$prevPages item=item}}
                                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                            {{/foreach}}

                                            <li class="active"><a>{{$currentPage}}</a></li>

                                            {{foreach from=$nextPages item=item}}
                                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                            {{/foreach}}

                                            {{if $nextPage}}
                                            <li>
                                                <a href="?page={{$nextPage}}" aria-label="Next">
                                                    <span aria-hidden="true">Next &raquo;</span>
                                                </a>
                                            </li>
                                            {{/if}}
                                        </ul>
                                    </nav>
                                </div>
                                {{/if}}
                            </div>
                            <ul class="blog-posts">
                                {{foreach from=$users item=item}}
                                <li class="post-item">
                                    <article class="entry">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="entry-thumb image-hover2">
                                                    <a href="{{$BASE_URL}}{{$item.alias}}.htm">
                                                        <img src="{{$BASE_URL}}{{$item.image}}" alt="Blog">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="entry-ci">
                                                    <h3 class="entry-title"><a href="{{$BASE_URL}}{{$item.alias}}.htm"><b>{{$item.pcompany_name}}</b></a></h3>

                                                    <div class="entry-excerpt">
                                                        <p><b>Main products:</b> {{$item.pproduct_chinh}}</p>
                                                        <p><b>Year Company Registered:</b> {{$item.namdangky}}</p>
                                                    </div>
                                                    <div class="entry-more">
                                                        <a href="{{$BASE_URL}}{{$item.alias}}/contact-us" class="contact_now" >{{l}}Contact Now{{/l}}</a>
                                                        <a href="#">Chat</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                {{/foreach}}
                            </ul>
                            <div class="sortPagiBar clearfix">
                                {{if $countAllPages > 1}}
                                <div class="bottom-pagination">
                                    <nav>
                                        <ul class="pagination">
                                            {{if $prevPage}}
                                            <li>
                                                <a href="?page={{$prevPage}}" aria-label="Prev">
                                                    <span aria-hidden="true">&laquo; Prev</span>
                                                </a>
                                            </li>
                                            {{/if}}

                                            {{foreach from=$prevPages item=item}}
                                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                            {{/foreach}}

                                            <li class="active"><a>{{$currentPage}}</a></li>

                                            {{foreach from=$nextPages item=item}}
                                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                            {{/foreach}}

                                            {{if $nextPage}}
                                            <li>
                                                <a href="?page={{$nextPage}}" aria-label="Next">
                                                    <span aria-hidden="true">Next &raquo;</span>
                                                </a>
                                            </li>
                                            {{/if}}
                                        </ul>
                                    </nav>
                                </div>
                                {{/if}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
