<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{l}}Register{{/l}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">{{l}}Register{{/l}}</span>
        </h2>
        <!-- ../page heading-->
        <div id="contact" class="page-content page-contact">
            <div id="message-box-conact"></div>
            <div class="row">
                <div class="col-sm-8">
                    <form role="form" data-toggle="validator" method="post">
                        {{if $success != ''}}
                        <div class="alert alert-success" >
                            <button data-dismiss="alert" class="close" type="button">
                                <i class="ace-icon fa fa-times"></i>
                            </button>
                            <p class="erro">
                                {{$success}}
                            </p>
                        </div>
                        {{/if}}
                        <div class="contact-form-box col-sm-6">
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Username{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[username]" id="username" value="{{$data.username}}" required />
                                {{if $validate.username != ''}}
                                <div class="alert alert-danger" >
                                    <button data-dismiss="alert" class="close" type="button">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    <p class="erro">
                                        {{$validate.username}}
                                    </p>
                                </div>
                                {{/if}}
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Password{{/l}}</label>
                                <input type="password" class="form-control input-sm" name="data[password]" id="password" required />
                                {{if $validate.password != ''}}
                                <div class="alert alert-danger" >
                                    <button data-dismiss="alert" class="close" type="button">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    <p class="erro">
                                        {{$validate.password}}
                                    </p>
                                </div>
                                {{/if}}
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Repeat Password{{/l}}</label>
                                <input type="password" class="form-control input-sm" name="data[repeat_password]" id="repeat_password" required />
                                {{if $validate.repeat_password != ''}}
                                <div class="alert alert-danger" >
                                    <button data-dismiss="alert" class="close" type="button">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    <p class="erro">
                                        {{$validate.repeat_password}}
                                    </p>
                                </div>
                                {{/if}}
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Firstname{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[first_name]" id="first_name" value="{{$data.first_name}}" required />
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Lastname{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[last_name]" id="last_name" value="{{$data.last_name}}" required />
                            </div>
                            <div class="form-selector"">
								<label class="">{{l}}Quốc Gia{{/l}}</label>
								<select name="data[country]" class="form-control input-sm" id="form-field-select-3" data-placeholder="{{l}}Choose a Country...{{/l}}">
									{{foreach from=$allCountry item=item}}
									<option value="{{$item.country_category_gid}}" {{if $data.country == $item.country_category_gid}} selected="selected" {{/if}}>{{$item.name}}</option>
									{{/foreach}}
								</select>
							</div>
                        </div>
                        <div class="contact-form-box col-sm-6">
                            <div class="form-selector">
                                <label>{{l}}Telephone{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[phone]" id="phone" value="{{$data.phone}}"/>
                            </div>
                            <div class="form-selector">
                                <label>{{l}}Mobile{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[mobile]" id="mobile" value="{{$data.mobile}}"/>
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Email{{/l}}</label>
                                <input type="email" class="form-control input-sm" name="data[email]"  id="email" value="{{$data.email}}" required />
                                {{if $validate.email != ''}}
                                <div class="alert alert-danger" >
                                    <button data-dismiss="alert" class="close" type="button">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    <p class="erro">
                                        {{$validate.email}}
                                    </p>
                                </div>
                                {{/if}}
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Company name{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[name_company]" id="name_company" value="{{$data.name_company}}" required />
                            </div>
                            <div class="form-selector">
                                <label>{{l}}Company address{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="dat[add]" id="add" value="{{$data.add}}" />
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Security Code{{/l}}</label>
                                <input type="text" class="form-control" name="captcha" required data-fv-notempty-message="{{l}}The captcha{{/l}}">
                                <div class="box-code"><img src="{{$LAYOUT_HELPER_URL}}front/captcha/create_image.php?r={{$randomNumber}}" style="padding-top:4px;" /></div>
                                {{if $erro != ''}}
                                <div class="alert alert-danger" >
                                    <button data-dismiss="alert" class="close" type="button">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    <p class="erro">
                                        {{$erro}}
                                    </p>
                                </div>
                                {{/if}}
                            </div>
                            <div class="form-selector">
                                <button type="submit" id="btn-contact" class="btn">{{l}}Submit{{/l}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->