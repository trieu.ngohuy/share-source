<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        {{sticker name=common_breadcrumbs}}
        <!-- breadcrumb -->
        <div class="">            
            {{sticker name=front_user_menu}}
            <div class="col-md-12 col-xs-12" id="contact">
                <div class="col-md-12 col-xs-12" style="margin-bottom: 15px">
                    <h4>{{l}}Edit Info{{/l}}</h4>
                </div>
                <div class="col-md-4">
                    <p>{{l}}To view and edit your account information. Change your name and email address, or update your contact information.{{/l}}</p>
                </div>
                <form role="form" data-toggle="validator" method="post" enctype="multipart/form-data">
                    <div class="col-md-4">
                        {{if $userMessage.success == true}}
                        <div class="alert alert-success">
                            <button class="close" data-dismiss="alert">
                                <i class="ace-icon fa fa-times"></i>
                            </button>
                            {{$userMessage.message}}.
                        </div>
                        {{/if}}
                        <div class="contact-form-box col-md-12">
                            <div class="form-selector">
                                <label><span>*</span> {{l}}First Name{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[first_name]" value="{{$user.first_name}}" id="title" required />
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Last Name{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[last_name]"  value="{{$user.last_name}}" id="title" required />
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Email{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[email]" value="{{$user.email}}" id="title" required />
                            </div>
                            <div class="form-selector">
                                <label>{{l}}Telephone{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[phone]" value="{{$user.phone}}" id="title"/>
                            </div>
                            <div class="form-selector">
                                <label>{{l}}Mobile{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[mobile]" value="{{$user.mobile}}" id="title"/>
                            </div>
                            <div class="form-selector"">
								<label class="">{{l}}Quốc Gia{{/l}}</label>
								<select name="data[country]" class="form-control input-sm" id="form-field-select-3" data-placeholder="{{l}}Choose a Country...{{/l}}">
									{{foreach from=$allCountry item=item}}
									<option value="{{$item.country_category_gid}}" {{if $user.country == $item.country_category_gid}} selected="selected" {{/if}}>{{$item.name}}</option>
									{{/foreach}}
								</select>
							</div>
                            <div class="form-selector">
                                <button type="submit" id="btn-contact" class="btn btn-primary">{{l}}Save{{/l}}</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="contact-form-box col-md-12">
                            <div class="form-selector">
                                {{if $user.image != ''}}
                                    <img style="width: 50%" src="{{$BASE_URL}}{{$user.image}}">
                                {{else}}
                                    {{l}}No Avatar{{/l}}
                                {{/if}}
                            </div>
                            <div class="form-selector">
                                <label>{{l}}Tệp đính kèm{{/l}}</label>
                                <input type="file" name="image"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>