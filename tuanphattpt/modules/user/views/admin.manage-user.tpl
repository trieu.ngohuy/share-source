


<div class="page-header">
	<h1>
		{{l}}User{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}List All User{{/l}}
		</small>
	</h1>
</div>
<a class="btn btn-lg btn-success" href="?export=true&page=1">
	<i class="ace-icon fa fa-file-excel-o"></i>
	Export 
</a>
<br class="cb">
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">
			
				{{if $allUsers|@count <= 0}}
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">
								<i class="ace-icon fa fa-times"></i>
							</button>
						{{l}}No user with above conditions.{{/l}}
				</div>
				{{/if}}
                        
				{{if $userMessage|@count > 0 && $userMessage.success == true}}
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">
								<i class="ace-icon fa fa-times"></i>
							</button>
						{{$userMessage.message}}.
				</div>
				{{/if}}
                        
				{{if $userMessage|@count > 0 && $userMessage.success == false}}
				<div class="alert alert-danger">
					<button class="close" data-dismiss="alert">
								<i class="ace-icon fa fa-times"></i>
							</button>
						{{$userMessage.message}}.
				</div>
				{{/if}}

				<table id="simple-table" class="table table-striped table-bordered table-hover">
					
					<thead>
						<tr>
							<form id="top-search" name="search" method="post" action="">
								<th class="center">
									<button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
								</th>
								<th class="center">
									
									<input class="check-all" type="checkbox" />
								</th>
                            	<th>ID</th>
                                <th class="center"> 
                                	{{l}}Username{{/l}}
                                	<input  type="text" name="condition[username]" id="username" value="{{$condition.username}}"" placeholder="{{l}}Find Username{{/l}}" class="form-control" />
                                </th>
                                <th>{{l}}Full Name{{/l}}</th>
                                <th>{{l}}Email{{/l}}</th>
                                <th class="center">
                                	{{l}}Group{{/l}}
                                	<select class=" form-control" name="condition[group_id]" onchange="this.form.submit();"  >
										<option value="">{{l}}All groups{{/l}}</option>
			                            {{foreach from=$allGroups item=item}}
			                                <option value="{{$item.group_id}}" {{if $condition.group_id == $item.group_id}}selected="selected"{{/if}}>{{$item.name}}</option>
			                            {{/foreach}}
									</select>
									<a href="javascript:document.sortForm.submit();"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/save_16.png" alt="Save sorting"></a>
                                </th>
                                <th>{{l}}Created Date{{/l}}</th>
                                <th>{{l}}Lastest Login{{/l}}</th>
                                <th>{{l}}Enabled{{/l}}</th>
                                <th>{{l}}Module{{/l}}</th>
                                <th style="display: none;">
									{{l}}Package{{/l}}
									<select class="form-control" name="condition[package_id]" onchange="this.form.submit();" >
										<option value="">{{l}}All Packages{{/l}}</option>
										{{foreach from=$allPackages item=item}}
											<option value="{{$item.package_category_gid}}" {{if $condition.package_id == $item.package_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
										{{/foreach}}
									</select>
								</th>
                                <th>{{l}}Action{{/l}}</th>
                        	</form>
						</tr>
					</thead>
					<form name="sortForm"  method="post" action="">
					<tbody>
						{{if $allUsers|@count > 0}}
							{{foreach from=$allUsers item=item key=key}}
								<tr>
									<td class="center">{{$key+1}}</td>
									<td class="center">
										<input type="checkbox" value="{{$item.user_id}}" name="allUsers" class="allUsers"/>
									</td>
                                    <td>{{$item.user_id}}</td>
                                    <td>{{$item.username}}</td>
                                    <td>{{$item.last_name}} {{$item.first_name}}</td>
                                    <td>{{$item.email}}</td>
                                    <td>
                                   		<select class=" form-control" name="data[{{$item.user_id}}]" >
											<option value="1" {{if $item.group_id == 1}}selected="selected"{{/if}}>{{l}}Admin{{/l}}</option>
											<option value="2" {{if $item.group_id == 2}}selected="selected"{{/if}}>{{l}}Bank{{/l}}</option>
											<option value="3" {{if $item.group_id == 3}}selected="selected"{{/if}}>{{l}}Estore{{/l}}</option>
											<option value="4" {{if $item.group_id == 4}}selected="selected"{{/if}}>{{l}}Member{{/l}}</option>
											<option value="4" {{if $item.group_id == 5}}selected="selected"{{/if}}>{{l}}Branch{{/l}}</option>
										</select>
                                   	</td>
                                                
					                                    <td>{{$item.created_date}}</td>
					                                    <td>{{$item.last_login_date}}</td>
					                                    
					                                    <td class="center">
					                                       {{if $item.enabled == '1'}}
					                                            <a href="{{$APP_BASE_URL}}user/admin/disable-user/id/{{$item.user_id}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png"></a>
					                                        {{else}}
					                                            <a href="{{$APP_BASE_URL}}user/admin/enable-user/id/{{$item.user_id}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png"></a>
					                                        {{/if}}
					                                    </td>
					                                    <td class="center">
					                                    	{{if $item.group_id == 3}} 
						                                    	<select onchange="this.options[this.selectedIndex].value && (window.open(this.options[this.selectedIndex].value,'_blank'))" class="form-control" data-search="true">
									                            	<option value="">{{l}}Choose an action...{{/l}}</option>
									                            	<option value="{{$APP_BASE_URL}}banner/admin/new-banner/id/{{$item.user_id}}">{{l}}New Banner{{/l}}</option>
									                            	<option value="{{$APP_BASE_URL}}content/admin/new-content/id/29_{{$item.user_id}}">{{l}}New News{{/l}}</option>
									                            	<option value="{{$APP_BASE_URL}}content/admin/new-content/id/27_{{$item.user_id}}">{{l}}New News Sales{{/l}}</option>
									                            	<option value="{{$APP_BASE_URL}}product/admin/new-product/id/{{$item.user_id}}">{{l}}New Product{{/l}}</option>
									                            </select>
								                            {{/if}}
					                                    </td>
					                                     <td style="display: none;">{{$item.package.name}}</td>
					                                    <td class="center">
					                                        <span class="tooltip-area">
																<a href="{{$APP_BASE_URL}}user/admin/edit-user/id/{{$item.user_id}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
																<a href="javascript:deleteAUser({{$item.user_id}});"  class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash-o"></i></a>
																
															</span>
					                                    </td>
                                    
													</tr>
												{{/foreach}}
						{{/if}}
					</tbody>
					</form>
				</table>
				
				
			</div><!-- /.span -->
		</div><!-- /.row -->
		
		
		<div class="col-lg-12">
							<div class="form-group col-lg-4">
								<label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
								<div class="col-lg-6">
									<select id="action" class="form-control" >
		                                <option value=";">{{l}}Choose an action...{{/l}}</option>
		                                <option value="deleteUser();">{{l}}Delete User{{/l}}</option>
		                                <option value="enableUser();">{{l}}Enable User{{/l}}</option>
		                                <option value="disableUser();">{{l}}Disable User{{/l}}</option>
		                            </select>
		                        </div>
		                        <div class="col-lg-6">
	                            	<a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
	                            </div>
							</div>
							<div class="form-group col-lg-2">
								<label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
								<div class="col-lg-12">
									<form class="search" name="search" method="post" action="">
		                                <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
		                                    <option value="5" {{if $displayNum == 5}} selected="selected" {{/if}}>5</option>
		                                    <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
		                                    <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
		                                    <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
		                                    <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
		                                    <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
		                                </select>
		                            </form>
		                        </div>
							</div>
							{{if $countAllPages > 1}}
							<div class="col-lg-6 pagination">
								{{if $first}}
	                            <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
	                            {{/if}}
	                            {{if $prevPage}}
	                            <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
	                            {{/if}}
	                            
	                            {{foreach from=$prevPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>
	                            
	                            {{foreach from=$nextPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            {{if $nextPage}}
	                            <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
	                            {{/if}}
	                            {{if $last}}
	                            <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
	                            {{/if}}
                            
							</div>
							{{/if}}
						</div>
					</div>
		
		
	</div>
</div>
							

<script language="javascript" type="text/javascript">
$(document).ready(function(){
    document.getElementById('username').select();
    document.getElementById('username').focus();
    $('.close').click(function(){
		$(this).parent().hide( "slow");
    });
    $('.check-all').click(function(){
    	if(this.checked) { // check select status
            $('.allUsers').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.allUsers').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
});

function applySelected()
{
	var task = document.getElementById('action').value;
	eval(task);
}
function enableUser()
{
    var all = document.getElementsByName('allUsers');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('{{l}}Please choose an user{{/l}}');
    }
    window.location.href = '{{$APP_BASE_URL}}user/admin/enable-user/id/' + tmp;
}

function disableUser()
{
    var all = document.getElementsByName('allUsers');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('{{l}}Please choose an user{{/l}}');
    }
    window.location.href = '{{$APP_BASE_URL}}user/admin/disable-user/id/' + tmp;
}

function deleteUser()
{
    var all = document.getElementsByName('allUsers');
    var tmp = '';
    var count = 0;
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
             count++;
        }
    }
    if ('' == tmp) {
        alert('{{l}}Please choose an user{{/l}}');
        return;
    } else {
    	result = confirm('{{l}}Are you sure you want to delete{{/l}} ' + count + ' {{l}}user(s){{/l}}?');
        if (false == result) {
            return;
        }
    }
    window.location.href = '{{$APP_BASE_URL}}user/admin/delete-user/id/' + tmp;
}


function deleteAUser(id)
{
    result = confirm('{{l}}Are you sure you want to delete this user{{/l}}?');
    if (false == result) {
        return;
    }
    window.location.href = '{{$APP_BASE_URL}}user/admin/delete-user/id/' + id;
}
</script>
							