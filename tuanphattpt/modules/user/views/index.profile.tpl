<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        {{sticker name=common_breadcrumbs}}
        <!-- breadcrumb -->
        <div class="row">
            {{sticker name=front_user_menu}}
            <div class="col-md-12 col-xs-12">
                <div class="col-md-7">
                    <div class="col-md-4">
                        <img src="{{$BASE_URL}}{{$user.image}}">
                    </div>
                    <div class="col-md-8 short-info">
                        <p>{{l}}Name{{/l}} : {{$user.full_name}}</p>
                        <p>{{l}}Email{{/l}} : {{$user.email}}</p>
                        <p>{{l}}Address{{/l}} : {{$user.add}}</p>
                        <p>{{l}}Username{{/l}} : {{$user.username}}</p>
                        <p>{{l}}Register Date{{/l}} : {{$user.created_date}}</p>
                        {{if $user.group_id == 3 }}
                        <p>{{l}}End Date{{/l}} : {{$user.time_end}}</p>
                        {{/if}}
                    </div>
                </div>
                {{if $user.group_id == 4}}
                <div class="col-md-5">
                    <div class="col-md-12 short-info">
                        <h2>{{l}}Thành Viên Miễn Phí{{/l}}</h2>
                        <p>
                            {{l}}Nâng cấp tài khoản của bạn để có thêm những tính năng hữu dụng từ dịch vụ của chúng tôi{{/l}}
                        </p>
                        <p>
                            <a class="btn btn-info" type="button" href="{{$BASE_URL}}user/upgrade">
                                {{l}}Nâng Cấp{{/l}}
                            </a>
                        </p>
                    </div>
                </div>
                {{elseif $user.group_id == 3}}
                <div class="col-md-5">
                    <div class="col-md-12 short-info">
                        <h2>{{l}}Bạn Là Thành Viên Chính Thức{{/l}}</h2>
                        <p>
                            {{l}}Gia hạn tài khoản để tiếp tục sử dụng dịch vụ của chúng tôi{{/l}}
                        </p>
                        <p>
                            <a class="btn btn-info" type="button" href="{{$BASE_URL}}user/upgrade">
                                {{l}}Gia Hạn{{/l}}
                            </a>
                        </p>
                    </div>
                </div>
                {{/if}}
            </div>
        </div>
    </div>
</div>