<div class="container hs-code">
    <div class="row">
        {{sticker name=front_user_menu}}
        <div class="col-sm-12">
            <h3>{{l}}CONTACT{{/l}}</h3>
        </div>
        <div class="col-sm-12">

                {{if $categoryMessage|@count > 0 && $categoryMessage.success == true}}
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$categoryMessage.message}}.
                </div>
                {{/if}}

                {{if $categoryMessage|@count > 0 && $categoryMessage.success == false}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$categoryMessage.message}}.
                </div>
                {{/if}}
            <table class="table table-striped table-bordered">
            	<form id="top-search" name="search" method="post" action="">
                <thead>
                <tr>
                    <th>
                        {{l}}NO.{{/l}}
                    </th>
                    <th>
                        {{l}}TITLE{{/l}}
                    </th>
                    <th>
                        {{l}}CREATED DATE{{/l}}
                    </th>
                    <th>
                        {{l}}STATUS{{/l}}
                    </th>
                </tr>
                </thead>
                </form>

                <tbody>
                {{foreach from=$allContact item=item key=key}}
                <tr {{if $item.user_view == "0"}} style="font-weight: bold;"{{/if}}>
                    <td>{{$key+1}}</td>
                    <td>
                        <a href="{{$BASE_URL}}user/contact-detail/{{$item.message_category_gid}}">{{$item.name}}</a>
                    </td>
                    <td>{{$item.created_date|date_format:"%d-%m-%Y"}}</td>
                    <td>
                        {{if $item.enabled == '0'}}
                            {{l}}Chờ xử lý{{/l}}
                        {{elseif $item.enabled == '1'}}
                            {{l}}Đang xử lý{{/l}}
                        {{elseif $item.enabled == '2'}}
                            {{l}}Đã xử lý{{/l}}
                        {{elseif $item.enabled == '3'}}
                            {{l}}Đã đóng{{/l}}
                        {{/if}}
                    </td>
                </tr>
                {{/foreach}}
                </tbody>
            </table>
        </div>
        <div class="col-md-12 sortPagiBar">
            {{if $countAllPages > 1}}
            <div class="bottom-pagination">
                <nav>
                    <ul class="pagination">
                        {{if $prevPage}}
                        <li>
                            <a href="?page={{$prevPage}}" aria-label="Prev">
                                <span aria-hidden="true">&laquo; Prev</span>
                            </a>
                        </li>
                        {{/if}}

                        {{foreach from=$prevPages item=item}}
                        <li><a href="?page={{$item}}">{{$item}}</a></li>
                        {{/foreach}}

                        <li class="active"><a>{{$currentPage}}</a></li>

                        {{foreach from=$nextPages item=item}}
                        <li><a href="?page={{$item}}">{{$item}}</a></li>
                        {{/foreach}}

                        {{if $nextPage}}
                        <li>
                            <a href="?page={{$nextPage}}" aria-label="Next">
                                <span aria-hidden="true">Next &raquo;</span>
                            </a>
                        </li>
                        {{/if}}
                    </ul>
                </nav>
            </div>
            {{/if}}
        </div>
    </div>
</div>
