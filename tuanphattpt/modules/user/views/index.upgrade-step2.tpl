<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        {{sticker name=common_breadcrumbs}}
        <!-- breadcrumb -->
        <div class="row">
            {{sticker name=front_user_menu}}
            <div class="col-md-12 col-xs-12">
            
					<div class="row">
				      <div class="col-xs-12 sd">
				      	<h2>{{l}}Payment Setup{{/l}}</h2>
				      </div>
				      <div class="col-xs-12 col-ms-4 col-md-4">
				        <p>{{l}}Please ensure your company name and business address are correct and complete, and that your company is legally registered.{{/l}}</p>         
				        <p>{{l}}Please complete the payment information for your membership.{{/l}}</p>
				        <a class="btn btnv" href="{{$BASE_URL}}user/upgrade">{{l}}Or change your membership{{/l}}</a>
				      </div>
				     
				      <div class="col-xs-12 col-ms-8 col-md-8">
							<form action="{{$BASE_URL}}user/upgrade-success" method="post" name="frm_data">
							<div class="row">
						        <div class="col-xs-12 col-ms-6 col-md-6">
						          <input type="hidden" readonly="readonly" value="{{$oldUser.first_name}}" name="firstname">
						            
								  <input type="hidden" readonly="readonly" value="{{$oldUser.email}}" name="email" id="email">
								  
								  <input type="hidden" readonly="readonly" value="{{$oldUser.phone}}" name="phone" id="phone">
						          <input type="hidden" readonly="readonly" value="{{$oldUser.pcompany_name}}" name="company" id="company">
						          
						          <input type="hidden" readonly="readonly" id="address" value="{{$oldUser.padd}}" name="address">
									             
						            <input type="hidden" readonly="readonly" id="country_ids" value="{{$data.country_id}}" name="country_id">
						            			            			            			            			            			            			            			            			            			            			            			            			            <input type="hidden" readonly="readonly" value="1" name="method_id" id="method_id">
						            
						            <input type="hidden" readonly="readonly" class="form-control" value="{{$data.payment_price}}" name="package_price" id="package_price">
						            
						            
						            <input type="hidden" readonly="readonly" value="VN" name="vpc_Locale" id="vpc_Locale">
						            {{if $data.payment_type == 1}}
						        		 <input type="hidden" class="form-control" value="VisaCreditCard" name="method_payment" id="method_payment">
						        	{{elseif $data.payment_type == 2 }}
						        		 <input type="hidden" class="form-control" value="" name="method_payment" id="method_payment">
						        	{{else}}
						        	{{/if}}
						           
						            
									<input type="hidden" readonly="readonly" value="" id="vpc_BackURL" name="vpc_BackURL">
									
									
									
									
						            
						      </div>
						       
						        <div class="col-xs-12 infopaymen">
						        <strong>
						        	{{if $data.payment_type == 1}}
						        		Visa, Master, JCB, Amex
						        	{{elseif $data.payment_type == 2 }}
						        		ATM
						        	{{else}}
						        		Direct pay
						        	{{/if}}
						        </strong>
						        </div>
						      <div class="col-xs-12 infopaymen">
						        <p>
						        <strong>{{l}}Upgrade to {{/l}}</strong>: 
						        	{{if $data.package_id == 1}}
							        <span class="red">
					                	VND 5.500.000 
					                	<span style="font-size:15px;">≈ $246</span>
							                
							        </span>
							        {{else}}
							        	<span class="red">
						                	VND 10.500.000  
						                	<span style="font-size:15px;">≈ $468</span>
								                
								        </span>
							        {{/if}}
						        </p>
						        
						        {{if $data.payment_type == 1}}
						        	<span>
								        <p style="padding-left:20px;color:#666; font-size:12px;">Pay via Smartlink payment</p>
								        <p style="padding-left:20px;color:#666; font-size:12px;"></p>
							        </span>
						        		<div align="center">
							        		<button class="btn btnred" id="btnPayment" type="submit">Payment</button>
							        	</div>
					        	{{elseif $data.payment_type == 2 }}
					        		<span>
								        <p style="padding-left:20px;color:#666; font-size:12px;">Pay via Smartlink payment</p>
								        <p style="padding-left:20px;color:#666; font-size:12px;"></p>
							        </span>
					        		<div align="center">
						        		<button class="btn btnred" id="btnPayment" type="submit">Payment</button>
						        	</div>
					        	{{else}}
					        		<span>
							        <p style="padding-left:20px;color:#666; font-size:12px;">{{l}}Pay at C&D Office{{/l}}</p>
							        <p style="padding-left:20px;color:#666; font-size:12px;"></p><p>{{l}}The fee can be paid by Cash in VND or USD at the Fee counter in C&D Office.{{/l}}</p>
							        <p><br>{{l}}Address : 2nd Fl, 132-134 Dien Bien Phu, Da Kao ward, Distrist 1, HCMC&nbsp;{{/l}}</p><p></p>
							        </span>
					        	{{/if}}
						       		
						     </div>
						      
						      
						     
						     
						    </div>
							</form>
					  
				        
					  
					  
					  
				     </div>
				     
				    </div>
            </div>
        </div>
    </div>
</div>