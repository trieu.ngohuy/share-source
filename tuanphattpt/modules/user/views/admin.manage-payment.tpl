


<div class="page-header">
	<h1>
		{{l}}Product{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}List All Product{{/l}}
		</small>
	</h1>
</div>
<a class="btn btn-lg btn-success" href="?export=true&page=1">
	<i class="ace-icon fa fa-file-excel-o"></i>
	Export 
</a>
<br class="cb">
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">
			
								{{if $allPayments|@count <= 0}}
		                        <div class="alert alert-info">
		                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
										{{l}}No payment with above conditions.{{/l}}
								</div>
		                        {{/if}}
		                        
		                        {{if $productMessage|@count > 0 && $productMessage.success == true}}
		                        <div class="alert alert-success">
		                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
										{{$productMessage.message}}.
								</div>
		                        {{/if}}
		                        
		                        {{if $productMessage|@count > 0 && $productMessage.success == false}}
		                        <div class="alert alert-danger">
		                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
										{{$productMessage.message}}.
								</div>
		                        {{/if}}
		                        
                
				<table id="simple-table" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<form id="top-search" name="search" method="post" action="">
								<th class="center">
									<button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
								</th>
                                   	<th>{{l}}User Upgrade{{/l}}
                                   	</th>
                                   	<th class="center">
                                        {{l}}Infomation{{/l}}
                                   	</th>
                                   	<th>{{l}}Created{{/l}}</th>
                                   	<th class="center">
                                        {{l}}Gói Nâng Cấp{{/l}}
                                   	</th>
                                   	<th class="center">
                                        {{l}}Kiểu Thanh Toán{{/l}}
                                   	</th>
                                   	
                                   	<th class="center">
                                        {{l}}Tình Trạng{{/l}}
                                   	</th>
                        	</form>
						</tr>
					</thead>

					<tbody>
						{{if $allPayments|@count > 0}}
							<form action="" method="post" name="sortForm">
							{{foreach from=$allPayments item=item name=product key=key}}
													<tr>
														<td class="center">{{$key+1}}</td>
					                                    <td  width="10%">
					                                    	{{$item.full_name}}
					                                    </td>
					                                    <td  width="10%">
					                                    	<b>{{l}}Tên Công Ty {{/l}}</b>{{$item.pcompany_name}}<br>
					                                    	<b>{{l}}Địa Chỉ {{/l}}</b>{{$item.padd}}<br>
					                                    	<b>{{l}}Số Điện Thoại {{/l}}</b>{{$item.phone}}<br>
					                                    	<b>{{l}}Ngành Nghề {{/l}}</b>{{$item.pproduct_chinh}}<br>
					                                    </td>
					                                    
					                                    <td>{{$item.time}}</td>
					                                    <td>
					                                    	{{if $item.packed == '1'}}
									                            {{l}}Gói Nâng Cấp 6 Tháng{{/l}}
									                        {{elseif $item.packed == '2'}}
									                            {{l}}Gói Nâng Cấp 1 Năm{{/l}}
									                        {{/if}}
					                                    </td>
					                                    <td>
									                        {{if $item.status == '1'}}
									                            {{l}}Pendding{{/l}}
									                        {{elseif $item.status == '2'}}
									                            {{l}}Approve{{/l}}
									                        {{elseif $item.status == '3'}}
									                            {{l}}Not Approve{{/l}}
									                        {{/if}}
									                    </td>
					                                    <td class="center">
					                                        {{if $item.status == '2' }}
					                                        	<img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png">
					                                        {{else}}
					                                        	<a href="{{$APP_BASE_URL}}user/admin/enable-payment/gid/{{$item.payment_id}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png"></a>
					                                  		{{/if}}
					                                    </td>
					                                    
                                    
													</tr>
												{{/foreach}}
												</form>
						{{/if}}
					</tbody>
				</table>
				
				
			</div><!-- /.span -->
		</div><!-- /.row -->
		
		
		<div class="col-lg-12">
							<div class="form-group col-lg-4">
								<label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
								<div class="col-lg-6">
									<select id="action" class="form-control" >
		                               <option value=";">{{l}}Choose an action...{{/l}}</option>
		                            </select>
		                        </div>
		                        <div class="col-lg-6">
	                            	<a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
	                            </div>
							</div>
							<div class="form-group col-lg-2">
								<label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
								<div class="col-lg-12">
									<form class="search" name="search" method="post" action="">
		                                <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
		                                    <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
		                                    <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
		                                    <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
		                                    <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
		                                    <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
		                                </select>
		                            </form>
		                        </div>
							</div>
							{{if $countAllPages > 1}}
							<div class="col-lg-6 pagination">
								{{if $first}}
	                            <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
	                            {{/if}}
	                            {{if $prevPage}}
	                            <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
	                            {{/if}}
	                            
	                            {{foreach from=$prevPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>
	                            
	                            {{foreach from=$nextPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            {{if $nextPage}}
	                            <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
	                            {{/if}}
	                            {{if $last}}
	                            <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
	                            {{/if}}
                            
							</div>
							{{/if}}
						</div>
					</div>
		
		
	</div>
</div>

<script language="javascript" type="text/javascript">
$(document).ready(function(){
    $('.close').click(function(){
		$(this).parent().hide( "slow");
    });
    $('.check-all').click(function(){
    	if(this.checked) { // check select status
            $('.allProduct').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.allProduct').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
});

function applySelected()
{
	var task = document.getElementById('action').value;
	eval(task);
}
function enableProduct()
{
    var all = document.getElementsByName('allProduct');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('Please choose an product');
    }
    window.location.href = '{{$APP_BASE_URL}}product/admin/enable-product/gid/' + tmp;
}

function disableProduct()
{
    var all = document.getElementsByName('allProduct');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('Please choose an product');
    }
    window.location.href = '{{$APP_BASE_URL}}product/admin/disable-product/gid/' + tmp;
}

function deleteProduct()
{
    var all = document.getElementsByName('allProduct');
    var tmp = '';
    var count = 0;
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
             count++;
        }
    }
    if ('' == tmp) {
        alert('Please choose an product');
        return;
    } else {
    	result = confirm('Are you sure you want to delete ' + count + ' product(s)?');
        if (false == result) {
            return;
        }
    }
    window.location.href = '{{$APP_BASE_URL}}product/admin/delete-product/gid/' + tmp;
}


function deleteAProduct(id)
{
    result = confirm('Are you sure you want to delete this product?');
    if (false == result) {
        return;
    }
    window.location.href = '{{$APP_BASE_URL}}product/admin/delete-product/gid/' + id;
}
</script>