<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{l}}Buying Request{{/l}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            {{sticker name=front_user_menu}}
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <div class="product-tab">
                    <ul class="nav-tab">

                        <li>
                            <a aria-expanded="true" data-toggle="tab">{{l}}Buying Request{{/l}}</a>
                        </li>
                    </ul>
                    <div class="tab-container">
                        <div id="product-detail" class="tab-panel active">
                            <div class="sortPagiBar clearfix">
                                <span class="page-noite">Showing {{$firstItemInPage}} to {{$lastItemInPage}} of {{$count}} ({{$countAllPages}} Page)</span>
                                {{if $countAllPages > 1}}
                                <div class="bottom-pagination">
                                    <nav>
                                        <ul class="pagination">
                                            {{if $prevPage}}
                                            <li>
                                                <a href="?page={{$prevPage}}" aria-label="Prev">
                                                    <span aria-hidden="true">&laquo; Prev</span>
                                                </a>
                                            </li>
                                            {{/if}}

                                            {{foreach from=$prevPages item=item}}
                                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                            {{/foreach}}

                                            <li class="active"><a>{{$currentPage}}</a></li>

                                            {{foreach from=$nextPages item=item}}
                                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                            {{/foreach}}

                                            {{if $nextPage}}
                                            <li>
                                                <a href="?page={{$nextPage}}" aria-label="Next">
                                                    <span aria-hidden="true">Next &raquo;</span>
                                                </a>
                                            </li>
                                            {{/if}}
                                        </ul>
                                    </nav>
                                </div>
                                {{/if}}
                            </div>
                            	<table class="table table-striped table-bordered">
				                <thead>
				                <tr>
				                    <th>
				                        {{l}}NO.{{/l}}
				                    </th>
				                    <th>
				                        {{l}}TITLE{{/l}}
				                    </th>
				                    <th>
				                        {{l}}Nội Dung{{/l}}
				                    </th>
				                    <th>
				                        {{l}}Số Lượng{{/l}}
				                    </th>
				                    <th>
				                        {{l}}Giá Tiền{{/l}}
				                    </th>
				                    <th>
				                        {{l}}Time left{{/l}}
				                    </th>
				                </tr>
				                </thead>
				
				                <tbody>
				               {{foreach from=$buyingRequest item=item key=key}}
				               	
					                <tr>
					                	
					                    <td>{{$key+1}}</td>
					                    <td><a href="{{$BASE_URL}}user/buying-request-reply/{{$item.alias}}">{{$item.name}}</a></td>
					                    <td>{{if $item.description != ''}}
                                                            <a href="{{$BASE_URL}}user/buying-request-reply/{{$item.alias}}">{{$item.description}}</a>
                                                        {{else}}
                                                            <a href="{{$BASE_URL}}user/buying-request-reply/{{$item.alias}}">{{l}}No description{{/l}}</a>
                                                        {{/if}}</td>
					                    <td>{{$item.soluong}}</td>
					                    <td>{{$item.price}}</td>
					                    <td>{{$item.end_date|date_format:"%d-%m-%Y"}}</td>
					                    
					            		
					                </tr>
				                {{/foreach}}
				                </tbody>
				                </table>
                                <div class="sortPagiBar clearfix">
                                    {{if $countAllPages > 1}}
                                    <div class="bottom-pagination">
                                        <nav>
                                            <ul class="pagination">
                                                {{if $prevPage}}
                                                <li>
                                                    <a href="?page={{$prevPage}}" aria-label="Prev">
                                                        <span aria-hidden="true">&laquo; Prev</span>
                                                    </a>
                                                </li>
                                                {{/if}}

                                                {{foreach from=$prevPages item=item}}
                                                <li><a href="?page={{$item}}">{{$item}}</a></li>
                                                {{/foreach}}

                                                <li class="active"><a>{{$currentPage}}</a></li>

                                                {{foreach from=$nextPages item=item}}
                                                <li><a href="?page={{$item}}">{{$item}}</a></li>
                                                {{/foreach}}

                                                {{if $nextPage}}
                                                <li>
                                                    <a href="?page={{$nextPage}}" aria-label="Next">
                                                        <span aria-hidden="true">Next &raquo;</span>
                                                    </a>
                                                </li>
                                                {{/if}}
                                            </ul>
                                        </nav>
                                    </div>
                                    {{/if}}
                                </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
