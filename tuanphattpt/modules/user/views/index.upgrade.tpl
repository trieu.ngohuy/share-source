<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        {{sticker name=common_breadcrumbs}}
        <!-- breadcrumb -->
        <div class="row">
            {{sticker name=front_user_menu}}
            <div class="col-md-12 col-xs-12">
                <div class="col-xs-12 p0">
        <div class="bords">
          <div align="center" class="free cold-xs-12 col-ms-6 col-md-4 nopadding  p0">
            <div class="top1">
              <strong>{{l}}Free member{{/l}}</strong>
            </div>
            <div style="height:171px;" class="top2">
			  <strong>{{l}}00{{/l}}</strong>
              <span>{{l}}/ Month{{/l}}</span>
            </div>
            <div class="top3">
              	<p><strong>{{l}}Can't post Products{{/l}}</strong> <strong>{{l}}Can post 3 Buying Request{{/l}}</strong> <strong>{{l}}Can't see Quality Standard{{/l}}</strong></p>

				<p style="line-height: 1.5em; padding: 10px 0px;">{{l}}Create Account{{/l}}</p>
			
				<p style="line-height: 1.5em; padding: 10px 0px;">{{l}}Provide company information{{/l}}</p>
			
				<p style="line-height: 1.5em; padding: 10px 0px;">{{l}}View Products basic information{{/l}}</p>
			
				<p style="line-height: 1.5em; padding: 10px 0px;">{{l}}See Buying Requests basic information{{/l}}</p>

            </div>
            <div class="top4">
              <a href="#">{{l}}Your're free member{{/l}}</a>
            </div>
          </div>
          <div align="center" class="month cold-xs-12 col-ms-6 col-md-4 nopadding  p0">
              <div class="top1">
                <strong>{{l}}Qualify 6 months{{/l}}</strong>
              </div>
              <div class="top2">
                <strong>
											{{l}}VND 5.500.000{{/l}}<br>
						 <span style="font-size:25px">{{l}}≈ $246{{/l}}</span>
									</strong>
                <span>{{l}}/ 6 Months{{/l}}</span>
              </div>
              <div class="top3">
                	<p><strong>{{l}}Post/ Management Products{{/l}}</strong> <strong>{{l}}Post/ Management Buying Request{{/l}}</strong> <strong>{{l}}Premium Company Website{{/l}}</strong> <strong>{{l}}Can see Quality Standard{{/l}}</strong> </p>

				<p style="line-height: 1.5em; padding: 8px 0px;">{{l}}View full Products information{{/l}}</p>
				
				<p style="line-height: 1.5em; padding: 8px 0px;">{{l}}See full Buying Requests information{{/l}}</p>
				
				<p style="line-height: 1.5em; padding: 8px 0px;">{{l}}Can Chat, Send and Receive messages{{/l}}</p>
				
				<p style="line-height: 1.5em; padding: 8px 0px;">{{l}}Let Buyers and Sellers know you are online{{/l}}</p>
				
				<p style="line-height: 1.5em; padding: 8px 0px;">{{l}}Can post promotional information{{/l}}</p>

              </div>
              <div class="top4">
                <a href="{{$BASE_URL}}user/upgrade-step1/1" class="active">{{l}}Upgrade now{{/l}}</a>
              </div>
          </div>
          <div align="center" class="year cold-xs-12 col-ms-6 col-md-4 nopadding  p0">
              <div class="top1">
                <strong>{{l}}Qualify 1 year{{/l}}</strong>
              </div>
              <div class="top2">
                <strong>
											{{l}}VND 10.500.000 {{/l}}<br>
						<span style="font-size:25px">{{l}}≈ $468{{/l}}</span>
									</strong>
                <span>{{l}}/ 1 Year {{/l}}</span>
              </div>
              <div class="top3">
			  		<p><strong>{{l}}Post/ Management Products{{/l}}</strong> <strong>{{l}}Post/ Management Buying Request{{/l}}</strong> <strong>{{l}}Premium Company Website{{/l}}</strong> <strong>{{l}}Can see Quality Standard{{/l}}</strong> </p>

					<p style="line-height: 1.5em; padding: 8px 0px;">{{l}}View full Products information{{/l}}</p>
					
					<p style="line-height: 1.5em; padding: 8px 0px;">{{l}}See full Buying Requests information{{/l}}</p>
					
					<p style="line-height: 1.5em; padding: 8px 0px;">{{l}}Can Chat, Send and Receive messages{{/l}}</p>
					
					<p style="line-height: 1.5em; padding: 8px 0px;">{{l}}Let Buyers and Sellers know you are online{{/l}}</p>
					
					<p style="line-height: 1.5em; padding: 8px 0px;">{{l}}Can post promotional information{{/l}}</p>

              </div>
              <div class="top4">
                <a href="{{$BASE_URL}}user/upgrade-step1/2">{{l}}Upgrade now{{/l}}</a>
              </div>
            </div>
          <br clear="all">
        </div>
      </div>
            </div>
        </div>
    </div>
</div>