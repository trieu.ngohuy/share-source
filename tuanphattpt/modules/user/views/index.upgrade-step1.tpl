<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        {{sticker name=common_breadcrumbs}}
        <!-- breadcrumb -->
        <div class="row">
            {{sticker name=front_user_menu}}
            <div class="col-md-12 col-xs-12">
            
            	<form class="changepass editinfo" action="{{$BASE_URL}}user/upgrade-step2" method="post" name="frm_data">
					<div class="row">
				      <div class="col-xs-12 sd">
				      	<h2>{{l}}Payment Setup{{/l}}</h2>
				      </div>
				      <div class="col-xs-12 col-ms-4 col-md-4">
				        <p>{{l}}Please ensure your company name and business address are correct and complete, and that your company is legally registered.{{/l}}</p>         
				        <p>{{l}}Please complete the payment information for your membership.{{/l}}</p>
				        <a class="btn btnv" href="{{$BASE_URL}}user/upgrade">{{l}}Or change your membership{{/l}}</a>
				      </div>
				     
				      <div class="col-xs-12 col-ms-8 col-md-8">
				        <div class="col-xs-12 col-ms-6 col-md-6">
				          <p><strong>Personal Infomation</strong></p>
				            <div class="form-group">
				            	
				            	{{if $gid == 1}}
							        <input type="hidden" value="1" name="package_id">
							        <input type="hidden" value="246.00" name="package_price">
									<input type="hidden" value="5500000.00" name="package_price_vnd">
									<input type="hidden" value="182" name="package_days">
								{{else}}
									<input type="hidden" value="2" name="package_id">
							        <input type="hidden" value="468.00" name="package_price">
									<input type="hidden" value="10500000.00" name="package_price_vnd">
									<input type="hidden" value="365" name="package_days">
								{{/if}}
				                
								
				                <label for="exampleInputPassword1">{{l}}Name{{/l}}</label>
				                <div class="">
								<div style="padding:0px;" class="col-md-6">
								<input type="text"  class="form-control col-md-6" value="{{$oldUser.first_name}}" name="firstname" required>
								</div>
								<div style="padding:0px;" class="col-md-6">
				                <input type="text" class="form-control col-md-6" value="{{$oldUser.last_name}}" name="lastname" required>
								</div>
				                <br clear="all">
				              </div>
				            </div>
				            <div class="form-group">
				              <label for="email">{{l}}Email{{/l}}</label>
				              <input type="email" class="form-control" value="{{$oldUser.email}}" name="email" id="email" required>
				            </div>
				            <div class="form-group">
				              <label for="phone">{{l}}Telephone{{/l}}</label>
				              <input type="phone"  class="form-control" value="{{$oldUser.phone}}" name="phone" id="phone" required>
				            </div>
				        </div>
				
				      <div class="col-xs-12 col-ms-6 col-md-6 colform2">
				        <div style="margin-top:15px;" class="form-group">
				          <label for="Company">{{l}}Company name{{/l}}</label>
				          <input type="text"  class="form-control" value="{{$oldUser.pcompany_name}}" name="company" id="company"  required>
				        </div>
						<div class="form-group">
				          <label for="Address">{{l}}Address{{/l}}</label>
				          <input type="text"  class="form-control" id="address" value="{{$oldUser.padd}}" name="address"  required >
				        </div>
				        <div class="form-group">
				          <label for="Address">{{l}}Country{{/l}}</label>
				          <select class="form-control" id="country_id" name="country_id">
								{{foreach from=$allCountry item=item}}
								<option value="{{$item.country_category_gid}}" {{if $oldUser.country == $item.country_category_gid}} selected="selected" {{/if}}>{{$item.name}}</option>
								{{/foreach}}
							</select>
				        </div>
				      </div>
					  
				        
				      <div class="col-xs-12 col-ms-6 col-md-6 infopaymen">
				      	{{if $gid == 1}}
					        <p>
								<strong>Upgrade to Qualify 6 months</strong>: 
								<span class="red"><br>VND 5.500.000 
									<span style="font-size:15px;">≈ $246</span>
								</span>
							</p>
						{{else}}
							<p>
								<strong>Upgrade to Qualify 1 year</strong>: 
								<span class="red"><br>VND 10.500.000 
									<span style="font-size:15px;">≈ $468</span>
								</span>
							</p>
						{{/if}}
						
						        <div class="radio">
				          <label>
				            <input type="radio" checked="checked" value="1" id="payment_1" name="method_id">
				            <strong>Visa, Master, JCB, Amex</strong>
				          </label>
						  <span>
						   <p style="padding-left:20px;color:#666; font-size:12px;">Pay via Smartlink payment</p>
						   </span>
				        </div>	
						        <div class="radio">
				          <label>
				            <input type="radio" value="2" id="payment_3" name="method_id">
				            <strong>ATM</strong>
				          </label>
						  <span>
						   <p style="padding-left:20px;color:#666; font-size:12px;">Pay via Smartlink payment</p>
						   </span>
				        </div>	
						        <div class="radio">
				          <label>
				            <input type="radio" value="3" id="payment_4" name="method_id">
				            <strong>Direct pay</strong>
				          </label>
						  <span>
						   <p style="padding-left:20px;color:#666; font-size:12px;">Pay at C&D Office</p>
						   </span>
				        </div>	
						       
						
				      </div>
					  
					  <div class="col-xs-12 col-ms-6 col-md-6 colform2 text-justify">
					  	<br>
					  	{{l}}C&D do not save any payment information of your card. All information of your payments are Smartlink encrypted SSL modern technology{{/l}}
					  </div>
					  
					  <div class="col-xs-12">
					  	<div align="left"><button class="btn btnred" id="btnPayment" type="submit">{{l}}Continue{{/l}}</button></div>
					  </div>
					  
				     </div>
				     
				    </div>
					</form>
            </div>
        </div>
    </div>
</div>