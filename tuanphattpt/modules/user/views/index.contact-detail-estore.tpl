<<style>
<!--
dd, dt {
    line-height: 20px   !important;
}
-->
</style>
<div class="columns-container">
    <div class="container" id="columns">
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{l}}Contact Detail{{/l}}</span>
        </div>

        <div class="row">
            {{sticker name=front_user_menu}}
            <div class="col-md-4 contact-info">
                <h4 class="widget-title">{{l}}Ticket{{/l}}</h4>
                <dl class="dl-horizontal">
                    <dt>{{l}}Status{{/l}}</dt>
                    <dd>
                       	{{if $data.enabled == "0"}}
                                                    {{l}}Chờ xử lý{{/l}}
                                                    {{elseif $data.enabled == "1"}}
                                                    {{l}}Đang xử lý {{/l}}
                                                    {{elseif $data.enabled == "2"}}
                                                    {{l}}Đã xử lý {{/l}}
                                                    {{elseif $data.enabled == "3"}}
                                                    {{l}}Đã đóng {{/l}}
                                                    {{/if}}
                    </dd>
<!--                    <dt>{{l}}Bộ phận{{/l}}</dt>-->
<!--                    <dd>{{$support}}</dd>-->
                    <dt>{{l}}ID Ticket{{/l}}</dt>
                    <dd>CT-{{$data.message_category_gid}}</dd>
                    <dt>{{l}}Created At{{/l}}</dt>
                    <dd>{{$data.created_date|date_format:"%d-%m-%Y %H:%M:%S"}}</dd>
                    <dt>{{l}}Updated At{{/l}}</dt>
                    <dd>{{$data.updated_date|date_format:"%d-%m-%Y %H:%M:%S"}}</dd>
                </dl>

                <h4 class="widget-title">{{l}}User Details{{/l}}</h4>
                <dl class="dl-horizontal">
                    <dt>{{l}}Người Gửi{{/l}}</dt>
                    <dd>{{$data.name_sender}}</dd>
                    <dt>{{l}}Email{{/l}}</dt>
                    <dd>{{$data.email}}</dd>
                    <dt>{{l}}Phone{{/l}}</dt>
                    <dd>{{$data.phone}}</dd>
                    <dt>{{l}}Company Name{{/l}}</dt>
                    <dd>{{$data.name_company}}</dd>
                    <dt>{{l}}Company Address{{/l}}</dt>
                    <dd>{{$data.add_company}}</dd>
<!--                    <dt>{{l}}Country{{/l}}</dt>-->
<!--                    <dd>{{$country.name}}</dd>-->
                    <dt>{{l}}Type Business{{/l}}</dt>
                    <dd>{{$user_form.pproduct_chinh}}</dd>
                </dl>
            </div>

            <div class="col-md-8">
                <div class="col-md-12 col-sm-6 widget-container-col">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h5 class="widget-title">{{$data.name}}</h5>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="timeline-container">
                                    <div class="timeline-items">
                                        {{foreach from=$allCats item=item key=key}}
                                        {{if $item.type_contact == 1}}
                                        <div class="timeline-item clearfix">
                                            <div class="timeline-info">
                                                <i class="timeline-indicator ace-icon fa fa-comment btn btn-grey no-hover"></i>
                                            </div>
                                            <div class="widget-box widget-color-grey">
                                                <div class="widget-header widget-header-small">
                                                    <h5 class="widget-title smaller">{{l}}Người gửi{{/l}} : {{$data.name_sender}}</h5>

																				<span class="widget-toolbar no-border">
																					<i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                                    {{$item.created_date|date_format:"%d-%m-%Y %H:%M"}}
																				</span>
                                                </div>

                                                <div class="widget-body">
                                                    <div class="widget-main">
                                                        {{$item.description}}
                                                        {{if $item.images != null}}
                                                                    <a href="{{$BASE_URL}}{{$item.images}}">{{l}}Download Attached File{{/l}}</a>
                                                        {{/if}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{else}}
                                        <div class="timeline-item clearfix">
                                            <div class="timeline-info">
                                                <i class="timeline-indicator ace-icon fa fa-comment btn btn-primary no-hover"></i>
                                            </div>
                                            <div class="widget-box widget-color-blue2">
                                                <div class="widget-header widget-header-small">
                                                    <h5 class="widget-title smaller">{{l}}Reply{{/l}} :{{$data.user_to.full_name}}</h5>
																				<span class="widget-toolbar no-border">
																					<i class="ace-icon fa fa-clock-o bigger-110"></i>
	                                                                                {{$item.created_date|date_format:"%d-%m-%Y %H:%M"}}
																				</span>
                                                </div>

                                                <div class="widget-body">
                                                    <div class="widget-main">
                                                        {{$item.description}}
                                                                    {{if $item.images != null}}
                                                                    <a href="{{$BASE_URL}}{{$item.images}}">{{l}}Download Attached File{{/l}}</a>
                                                                    {{/if}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{/if}}
                                        {{/foreach}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-6 widget-container-col">
                    <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="col-md-12">
                        <div class="form-group has-info">
                            <label class="control-label col-md-2">{{l}}Message{{/l}}</label>
                            <div class="col-md-10">
                                <textarea class="form-control input-sm" rows="6" name="description" id="message" required></textarea>
                            </div>
                        </div>
                        <div class="form-group has-info">
                            <label class="control-label col-md-2">{{l}}Attach File{{/l}}</label>
                            <div class="col-md-10">
                                <input type="file" class="form-control" name="images" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-offset-8 col-md-4">
                            <button class="btn btn-info" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                {{l}}Send{{/l}}
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>