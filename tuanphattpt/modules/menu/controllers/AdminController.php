<?php

class menu_AdminController extends Nine_Controller_Action_Admin {
    /*
     * Manager menu
     */

    public function manageMenuAction() {
        //Set title
        $this->view->headTitle(Nine_Language::translate('Manage Menu'));

        /*
         * Paging
         */
        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page", false);
        //$displayNum = $this->_getParam('displayNum', false);

        if ($currentPage == false) {
            $currentPage = 1;
        }

        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        $objMenu = new Models_Menu();
        foreach ($data as $id => $value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }

            $objMenu->update(array('sorting' => $value), array('menu_gid=?' => $id));
            $this->session->menuMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("Menu Order is edited successfully")
            );
        }

        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        $tmpCondition = $condition;
        if (false !== $condition) {
            $currentPage = 1;
        }
        if (false == $condition || $condition['name'] === '') {
            $condition = array();
            $tmpCondition = array();
        } else {
            //Adjust condition
            $tmpCondition = array(
                'name LIKE ?' => '%' . $condition['name'] . '%'
            );
        }
        /**
         * Get all menus
         */
        $arrMenu = Nine_Read::GetListData('Models_Menu', $tmpCondition, array('sorting ASC'), 'index', $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
        //$arrMenu = Nine_Common::FomatArrayAsTree($arrMenu, 0, '', 'menu_gid', false);
        //Add parent column
        foreach ($arrMenu as &$value) {
            if ($value['parent_id'] === '0') {
                $value['parent_name'] = '---';
            } else {
                $tmp = @reset(Nine_Read::GetListData('Models_Menu', array(
                                    'menu_gid' => $value['parent_id']
                                        ), array(), 'detail'));
                $value['parent_name'] = $tmp['name'];
            }
        }
        unset($value);
        /**
         * Count all menus
         */
        $count = count(Nine_Read::GetListData('Models_Menu', $tmpCondition));
        /**
         * Format
         */
        $tmp = array();
        $tmp2 = false;
        $tmpGid = $arrMenu[0]['menu_gid'];
        foreach ($arrMenu as $index => $menu) {

            if ($tmpGid != $menu['menu_gid']) {
                $tmp[] = $tmp2;
                $tmp2 = false;
                $tmpGid = $menu['menu_gid'];
            }
            if (false === $tmp2) {
                $tmp2 = $menu;
            }
            $tmp2['langs'][] = $menu;
            /**
             * Final element
             */
            if ($index == count($arrMenu) - 1) {
                $tmp[] = $tmp2;
            }
        }
        $arrMenu = $tmp;


        /**
         * Set values out to views
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);

        $this->view->arrMenu = $arrMenu;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;

        $this->view->menu = array(
            0 => 'menu',
            1 => 'manager-menu'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-menu-hunt',
                'url' => Nine_Registry::getBaseUrl() . 'admin/menu/admin/manage-menu',
                'name' => Nine_Language::translate('Manager Menu')
            )
        );
    }

    /*
     * Create new
     */

    public function newMenuAction() {
        $objLang = new Models_Lang();
        $objMenu = new Models_Menu();
        $id = $this->_getParam('id', false);

        if ($id != false) {
            $this->_redirect('menu/admin/manage-menu');
        }
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_menu', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data = $this->_getParam('data', false);
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permission for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_menu', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        //Get parent menu
        $arrMenus = Nine_Read::GetListData('Models_Menu', array(), array('sorting ASC'));
        $arrMenus = Nine_Common::FomatTree($arrMenus, 0, '', 'menu_gid');
        $this->view->arrMenus = $arrMenus;

        //Get all modules
        $this->GetAllModules();

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new menu
             */
            $newMenu = $data;
            $newMenu['created_date'] = time();

            /**
             * Sorting
             */
            if (null == $newMenu['sorting']) {
                unset($newMenu['sorting']);
            }
            try {
                /**
                 * Increase all current sorting
                 */
                if (1 > $newMenu['sorting']) {
                    $newMenu['sorting'] = 1;
                }

                $objMenu->insert($newMenu);
                /**
                 * Message
                 */
                $this->session->menuMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Menu is created successfully.')
                );
                $this->_redirect('menu/admin/manage-menu');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        }
        /**
         * Prepare for template
         */
        $this->view->allLangs = $allLangs;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Menu'));
        $this->view->menu = array(
            0 => 'menu',
            1 => 'new-menu'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-newspaper-o',
                'url' => Nine_Registry::getBaseUrl() . 'admin/menu/admin/manage-menu',
                'name' => Nine_Language::translate('Manager Menu')
            ),
            1 => array(
                'icon' => 'fa-plus',
                'url' => '',
                'name' => Nine_Language::translate('New Menu')
            )
        );
    }

    /*
     * Edit
     */

    public function editMenuAction() {
        $objMenu = new Models_Menu();
        $objLang = new Models_Lang();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_menu', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        if (false == $gid) {
            $this->_redirect('menu/admin/manage-menu');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_menu', null, '*')) || (false != $lid && false == $this->checkPermission('edit_menu', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data = $this->_getParam('data', false);

        /**
         * Get all menu languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allMenuLangs = $objMenu->setAllLanguages(true)->getByColumns(array('menu_gid=?' => $gid))->toArray();


        //Get parent menu
        $arrMenus = Nine_Read::GetListData('Models_Menu', array('parent_id' => 0), array('sorting ASC'));
        $this->view->arrMenus = $arrMenus;

        //Get all modules
        $this->GetAllModules();

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new menu
             */
            $newMenu = $data;

            /**
             * Sorting
             */
            if (null == $newMenu['sorting']) {
                unset($newMenu['sorting']);
            }

            try {
                /**
                 * Update
                 */
                $objMenu->update($newMenu, array('menu_gid=?' => $gid));
                /**
                 * Message
                 */
                $this->session->menuMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Menu is updated successfully.')
                );

                $this->_redirect('menu/admin/manage-menu');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allMenuLangs);

            if (false == $data) {
                $this->session->menuMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("Menu doesn't exist.")
                );
                $this->_redirect('menu/admin/manage-menu');
            }

            /**
             * Get all lang menus
             */
            foreach ($allMenuLangs as $menu) {

                $data[$menu['lang_id']] = $menu;
            }
        }

        /**
         * Prepare for template
         */
        $this->view->allLangs = $allLangs;
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;

        $this->view->headTitle(Nine_Language::translate('Edit Menu'));
        $this->view->fullPermisison = $this->checkPermission('edit_menu', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_menu');
        $this->view->menu = array(
            0 => 'menu',
            1 => 'manager-menu'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-newspaper-o',
                'url' => Nine_Registry::getBaseUrl() . 'admin/menu/admin/manage-menu',
                'name' => Nine_Language::translate('Manager Menu')
            ),
            1 => array(
                'icon' => 'fa-pencil',
                'url' => '',
                'name' => Nine_Language::translate('Edit Menu')
            )
        );
    }

    /*
     * Delete
     */

    public function deleteMenuAction() {
        $obj = new Models_Menu();

        $id = $this->_getParam('gid', false);

        if (false == $id) {
            $this->_redirect('menu/admin/manage-menu');
        }

        $gids = explode('_', trim($id, '_'));

        try {
            foreach ($gids as $gid) {

                $obj->delete(array('menu_gid=?' => $gid));
            }
            $this->session->contactMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Menu is deleted successfully')
            );
        } catch (Exception $e) {
            $this->session->contactMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT delete this contact. Please try again')
            );
        }
        $this->_redirect('menu/admin/manage-menu');
    }

    /*
     * Get all modules
     */

    function GetAllModules() {
        $arrProductCategory = Nine_Read::GetListData('Models_ProductCategory');
        $arrProductCategory = Nine_Common::FomatTree($arrProductCategory, 0, '', 'product_category_gid');
        $this->view->arrProductCategory = $arrProductCategory;

        $arrProducts = Nine_Read::GetListData('Models_Product');
        $this->view->arrProducts = $arrProducts;

        $arrContentCategory = Nine_Read::GetListData('Models_ContentCategory');
        $arrContentCategory = Nine_Common::FomatTree($arrContentCategory, 0, '', 'content_category_gid');
        $this->view->arrContentCategory = $arrContentCategory;

        $arrContents = Nine_Read::GetListData('Models_Content');
        $this->view->arrContents = $arrContents;

        $arrProjects = Nine_Read::GetListData('Models_Project');
        $this->view->arrProjects = $arrProjects;
    }

}
