<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>
<script src="{{$LAYOUT_HELPER_URL}}libs/common/js/admin.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        //Init params
        arrAttr = {
            source: 'name',
            destination: 'alias',
            mode: 0
        };
        //Admin ready
        SetupSlug({{$allLangs|@json_encode}});

        //Combobox modules select
        $("#comSelectLink").change(function () {
            // Get attr
            var modules = $('#comSelectLink').find(":selected").attr('data-modules');
            var controller = $('#comSelectLink').find(":selected").attr('data-controller');
            var functions = $('#comSelectLink').find(":selected").attr('data-function');
            var params = $('#comSelectLink').find(":selected").attr('data-params');

            // Set value to inputs
            $('#inpModules').val(modules);
            $('#inpController').val(controller);
            $('#inpFunction').val(functions);
            $('#inpParams').val(params);
        });
    });
</script>

<div class="page-header">
    <h1>
        {{l}}Menu{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}New Menu{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">
                {{if $errors|@count > 0}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{if $errors.main}}
                    </strong> {{$errors.main}}
                    {{else}}
                    {{l}}Please check following information again{{/l}}
                    {{/if}}
                </div>
                {{/if}}


                <div class="col-sm-12 widget-container-col ui-sortable">

                    <div class="tabbable tabs-left">

                        <ul class="nav nav-tabs" id="myTab2">
                            <li class="active">
                                <a href="#tab0" data-toggle="tab" aria-expanded="true">
                                    <i class="pink ace-icon fa fa-home bigger-110"></i>
                                    {{l}}Basic{{/l}}
                                </a>
                            </li>

                            {{foreach from=$allLangs item=item index=index name=langTab}}
                            <li>
                                <a href="#tab{{$item.lang_id}}" data-toggle="tab" aria-expanded="true">
                                    <image style="vertical-align:middle;"
                                           src="{{$BASE_URL}}{{$item.lang_image}}"> {{$item.name}}
                                </a>
                            </li>
                            {{/foreach}}
                        </ul>

                        <form action="" method="post" class="form-horizontal">
                            <div class="tab-content">

                                <div id="tab0" class="tab-pane active">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Hiển thị{{/l}}</label>
                                        <div class="col-md-10">
                                            <label class="radio-inline">
                                                <input type="radio" name="data[genabled]" value="1"
                                                       {{if $data.genabled != '0'}}checked="checked"{{/if}}/> Yes
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="data[genabled]" value="0"
                                                       {{if $data.genabled == '0'}}checked="checked"{{/if}}/> No
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Menu cha{{/l}}</label>
                                        <div class="col-md-10">
                                            <select name="data[parent_id]" class="chosen-select form-control"
                                                    id="form-field-select-3">
                                                <option value="0" {{if $data.parent_id === ''}}selected="selected"{{/if}}>---</option>
                                                {{foreach from=$arrMenus item=item}}
                                                <option value="{{$item.menu_gid}}"
                                                        {{if $data.parent_id == $item.menu_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
                                                {{/foreach}}
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Sorting{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="data[sorting]"
                                                   value="{{$data.sorting}}">
                                        </div>
                                    </div>

                                    <div class="form-group" style="display: none;">
                                        <label class="control-label col-md-2">{{l}}Loại menu{{/l}}</label>
                                        <div class="col-md-10">
                                            <label class="radio-inline">
                                                <input type="radio" name="data[display_position]" value="0" checked="checked"/> Header
                                            </label>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Thiết lập địa chỉ url{{/l}}</label>
                                        <div class="col-md-10">
                                            <p><b>Lưu ý: </b> <i>Phần này sẽ thiết lập đường dẫn url khi click vào menu. Ví dụ menu <b>Trang chủ</b> có đường dẫn đi tới trang chủ, thì lúc click vào menu <b>Trang chủ</b> sẽ đi tới trang chủ</i></p>
                                            <label class="radio-inline" style="width: 100%">
                                                <input type="radio" name="data[type]" value="0"
                                                       {{if $data.link != ''}}checked="checked"{{/if}}/> {{l}}Tự định nghĩa{{/l}}
                                                <p style="font-style: italic; font-size: 11px"><i>Tự gõ đường link vào, ví dụ: google.com. Nếu muốn click vào menu 
                                                        vẫn ở trang hiện tại thì gõ #.</i></p>
                                                <input type="text" class="form-control" name="data[link]" value="{{$data.link}}">
                                            </label>
                                            <label class="radio-inline" style="width: 100%; margin-left: 0px;">
                                                <input type="radio" name="data[type]" value="1"
                                                       {{if $data.link == ''}}checked="checked"{{/if}}/> {{l}}Chọn từ hệ thống{{/l}}
                                                <p style="font-style: italic; font-size: 11px"><i>Chọn từ hệ thống, như sản phẩm, dự án, bài viết....</i></p>

                                                <select id="comSelectLink" class="chosen-select form-control" >
                                                    <option value="0" 
                                                            {{if $data.modules === '' && $data.controller === ''
                                                            && $data.function === '' && $data.params === ''}}
                                                            selected="selected"{{/if}}>---</option>
                                                    <!--Cơ bản-->
                                                    <optgroup label="{{l}}Cơ bản{{/l}}">
                                                        <option data-modules="default" 
                                                                data-controller="index" 
                                                                data-function="index" 
                                                                data-params="" 
                                                                {{if $data.modules === 'default'
                                                                && $data.controller=== 'index'
                                                                && $data.function === 'index'}}
                                                                selected="selected"
                                                                {{/if}}
                                                                >{{l}}Trang chủ{{/l}}</option>
                                                        <option data-modules="contact" 
                                                                data-controller="index" 
                                                                data-function="index" 
                                                                data-params="" 
                                                                {{if $data.modules === 'contact'
                                                                && $data.controller=== 'index'
                                                                && $data.function === 'index'}}
                                                                selected="selected"
                                                                {{/if}}>{{l}}Liên hệ{{/l}}</option>
                                                        <option data-modules="design" 
                                                                data-controller="index" 
                                                                data-function="index" 
                                                                data-params="" 
                                                                {{if $data.modules === 'design'
                                                                && $data.controller=== 'index'
                                                                && $data.function === 'index'}}
                                                                selected="selected"
                                                                {{/if}}>{{l}}Thiết kế chiếu sáng miễn phí{{/l}}</option>
                                                        <option data-modules="design" 
                                                                data-controller="index" 
                                                                data-function="calculation" 
                                                                data-params="" 
                                                                {{if $data.modules === 'design'
                                                                && $data.controller=== 'index'
                                                                && $data.function === 'calculation'}}
                                                                selected="selected"
                                                                {{/if}}>{{l}}Tính toán & chọn đèn nhà xưởng{{/l}}</option>
                                                        <option data-modules="project" 
                                                                data-controller="index" 
                                                                data-function="index" 
                                                                data-params="" 
                                                                {{if $data.modules === 'project'
                                                                && $data.controller=== 'index'
                                                                && $data.function === 'index'}}
                                                                selected="selected"
                                                                {{/if}}>{{l}}Danh sách dự án{{/l}}</option>
                                                    </optgroup>
                                                    <!--Product category-->
                                                    <optgroup label="{{l}}Danh mục sản phẩm{{/l}}">
                                                        {{foreach from=$arrProductCategory item=item}}
                                                        <option data-modules="product" 
                                                                data-controller="index" 
                                                                data-function="index" 
                                                                data-params="{{$item.alias}}" 
                                                                {{if $data.modules === 'product'
                                                                && $data.controller=== 'index'
                                                                && $data.function === 'index'
                                                                && $data.params === $item.alias}}
                                                                selected="selected"
                                                                {{/if}}
                                                                >{{$item.name}}</option>
                                                        {{/foreach}}
                                                    </optgroup>
                                                    <!--Products-->
                                                    <optgroup label="{{l}}Sản phẩm{{/l}}">
                                                        {{foreach from=$arrProducts item=item}}
                                                        <option data-modules="product" 
                                                                data-controller="index" 
                                                                data-function="detail" 
                                                                data-params="{{$item.alias}}" 
                                                                {{if $data.modules === 'product'
                                                                && $data.controller=== 'index'
                                                                && $data.function === 'detail'
                                                                && $data.params === $item.alias}}
                                                                selected="selected"
                                                                {{/if}}
                                                                >{{$item.title}}</option>
                                                        {{/foreach}}
                                                    </optgroup>
                                                    <!--Content category-->
                                                    <optgroup label="{{l}}Danh mục tin tức{{/l}}">
                                                        {{foreach from=$arrContentCategory item=item}}
                                                        <option data-modules="content" 
                                                                data-controller="index" 
                                                                data-function="index" 
                                                                data-params="{{$item.alias}}" 
                                                                {{if $data.modules === 'content'
                                                                && $data.controller=== 'index'
                                                                && $data.function === 'index'
                                                                && $data.params === $item.alias}}
                                                                selected="selected"
                                                                {{/if}}
                                                                >{{$item.name}}</option>
                                                        {{/foreach}}
                                                    </optgroup>
                                                    <!--Contents-->
                                                    <optgroup label="{{l}}Tin tức{{/l}}">
                                                        {{foreach from=$arrContents item=item}}
                                                        <option data-modules="content" 
                                                                data-controller="index" 
                                                                data-function="detail" 
                                                                data-params="{{$item.alias}}" 
                                                                {{if $data.modules === 'content'
                                                                && $data.controller=== 'index'
                                                                && $data.function === 'detail'
                                                                && $data.params === $item.alias}}
                                                                selected="selected"
                                                                {{/if}}
                                                                >{{$item.title}}</option>
                                                        {{/foreach}}
                                                    </optgroup>
                                                    <!--Projects-->
                                                    <optgroup label="{{l}}Dự án{{/l}}">
                                                        {{foreach from=$arrProjects item=item}}
                                                        <option data-modules="project" 
                                                                data-controller="index" 
                                                                data-function="detail" 
                                                                data-params="{{$item.alias}}" 
                                                                {{if $data.modules === 'project'
                                                                && $data.controller=== 'index'
                                                                && $data.function === 'detail'
                                                                && $data.params === $item.alias}}
                                                                selected="selected"
                                                                {{/if}}
                                                                >{{$item.title}}</option>
                                                        {{/foreach}}
                                                    </optgroup>
                                                </select>

                                                <input id="inpModules" type="hidden" class="form-control" name="data[modules]" value="{{$data.modules}}">
                                                <input id="inpController" type="hidden" class="form-control" name="data[controller]" value="{{$data.controller}}">
                                                <input id="inpFunction" type="hidden" class="form-control" name="data[function]" value="{{$data.function}}">
                                                <input id="inpParams" type="hidden" class="form-control" name="data[params]" value="{{$data.params}}">
                                            </label>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Save{{/l}}
                                            </button>
                                        </div>
                                    </div>

                                </div>

                                {{foreach from=$allLangs item=item name=langDiv}}
                                <div class="tab-pane" id="tab{{$item.lang_id}}">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Hiển thị{{/l}}</label>
                                        <div class="col-md-10">
                                            <label class="radio-inline">
                                                <input type="radio" name="data[{{$item.lang_id}}][enabled]" value="1"
                                                       {{if $data[$item.lang_id].enabled == '1'}}checked="checked"{{/if}}/>
                                                Yes
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="data[{{$item.lang_id}}][enabled]" value="0"
                                                       {{if $data[$item.lang_id].enabled != '1'}}checked="checked"{{/if}}/>
                                                No
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Name{{/l}}</label>
                                        <div class="col-md-10">
                                            <input id="name{{$smarty.foreach.langDiv.iteration}}" type="text"
                                                   class="form-control" name="data[{{$item.lang_id}}][name]"
                                                   value="{{$data[$item.lang_id].name}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Alias{{/l}}</label>
                                        <div class="col-md-10">
                                            <input id="alias{{$smarty.foreach.langDiv.iteration}}" type="text"
                                                   class="form-control" name="data[{{$item.lang_id}}][alias]"
                                                   value="{{$data[$item.lang_id].alias}}">.html
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Mô tả{{/l}}</label>
                                        <div class="col-md-10">
                                            <textarea style="float:left;" class="text-input textarea ckeditor"
                                                      name="data[{{$item.lang_id}}][intro_text]" rows="20"
                                                      cols="90">{{$data[$item.lang_id].intro_text}}</textarea>
                                        </div>
                                    </div>

                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Save{{/l}}
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                {{/foreach}}

                            </div>

                        </form>

                    </div>

                </div>


            </div><!-- /.span -->

        </div><!-- /.row -->


    </div>


</div>