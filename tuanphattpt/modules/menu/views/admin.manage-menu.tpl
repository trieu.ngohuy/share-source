<div class="page-header">
    <h1>
        {{l}}Menu{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}List All Menu{{/l}}
        </small>
    </h1>
</div>
<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">

                {{if $arrMenu|@count <= 0}}
                <div class="alert alert-info">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{l}}No new menu.{{/l}}
                </div>
                {{/if}}

                {{if $menuMessage|@count > 0 && $menuMessage.success == true}}
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$categoryMessage.message}}.
                </div>
                {{/if}}

                {{if $menuMessage|@count > 0 && $menuMessage.success == false}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$menuMessage.message}}.
                </div>
                {{/if}}


                <table id="simple-table" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                    <form id="top-search" name="search" method="post" action="">
                        <th class="center">
                            #
                        </th>
                        <th class="center">
                            <input class="check-all" type="checkbox" />
                        </th>
                        <th>{{l}}Modules{{/l}}</th>
                        <th>{{l}}Controller{{/l}}</th>
                        <th>{{l}}Function{{/l}}</th>
                        <th>{{l}}Created{{/l}}</th>
                        <th class="center">
                            {{l}}Sorting{{/l}}
                            <a href="javascript:document.sortForm.submit();"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/save_16.png" alt="Save sorting"></a>
                        </th>
                        <th>{{l}}Menu cha{{/l}}</th>
                        <th colspan="5">
                            <div class="col-md-6">
                                {{l}}Name{{/l}}
                                <input  type="text" name="condition[name]" id="name" value="{{$condition.name}}" placeholder="{{l}}Find Name{{/l}}" class="form-control" />
                            </div>
                            <div class="col-md-6">
                                {{l}}Description{{/l}}
                            </div>
                        </th>
                    </form>
                    </tr>
                    </thead>

                    <tbody>
                        {{if $arrMenu|@count > 0}}
                    <form action="" method="post" name="sortForm">
                        {{assign var=count value=1}}
                        {{foreach from=$arrMenu item=item name=menu key=key}}
                        <tr>
                            <td class="center">{{$count}}</td>
                            <td class="center">
                                <input type="checkbox" value="{{$item.menu_gid}}" name="allItems" class="allItems"/>
                            </td>
                            <td>{{$item.modules}}</td>
                            <td>{{$item.controller}}</td>
                            <td>{{$item.function}}</td>
                            <td>{{$item.created_date}}</td>
                            <td class="center">
                                <input name="data[{{$item.menu_gid}}]" value="{{$item.sorting}}" size="3" style="text-align: center;"></input>
                            </td>
                            <td>{{$item.parent_name}}</td>
                            <td colspan="5" >                                
                                <!-- All languages -->
                                <table style="margin-bottom: 0px" class="table table-bordered table-striped data-table">
                                    <tbody id="table{{$item.menu_id}}">

                                        {{foreach from=$item.langs item=item2}}
                                        <tr>
                                            <td>{{$item2.name}}</td>
                                            <td width='30%'>{{$item2.intro_text}}</td>

                                            <td class="center" width='10%'>
                                                {{p name=edit_menu module=menu expandId=$langId}}
                                                <span class="tooltip-area">
                                                    <a href="{{$APP_BASE_URL}}menu/admin/edit-menu/gid/{{$item.menu_gid}}" class="btn btn-default btn-sm" title="Edit">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </span>
                                                {{/p}}
                                                {{np name=edit_menu module=menu expandId=$langId}}
                                                --
                                                {{/np}}
                                            </td>

                                        </tr>
                                        {{/foreach}}

                                    </tbody>
                                </table>
                            </td>

                        </tr>
                        {{assign var=count value=$count+1}}
                        {{/foreach}}
                    </form>
                    {{/if}}
                    </tbody>
                </table>


            </div><!-- /.span -->
        </div><!-- /.row -->


        <div class="col-lg-12">
            <div class="form-group col-lg-4">
                <label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
                <div class="col-lg-6">
                    <select id="action" class="form-control" >
                        <option value=";">{{l}}Choose an action...{{/l}}</option>
                        <option value="DeleteData('{{$APP_BASE_URL}}menu/admin/delete-menu/gid/');">{{l}}Delete{{/l}}</option>
                    </select>
                </div>
                <div class="col-lg-6">
                    <a href="javascript:RunAction();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
                </div>
            </div>
            <div class="form-group col-lg-2">
                <label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
                <div class="col-lg-12">
                    <form class="search" name="search" method="post" action="">
                        <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
                            <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
                            <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
                            <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
                            <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
                            <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
                        </select>
                    </form>
                </div>
            </div>
            {{if $countAllPages > 1}}
            <div class="col-lg-6 pagination">
                {{if $first}}
                <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
                {{/if}}
                {{if $prevPage}}
                <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
                {{/if}}

                {{foreach from=$prevPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>

                {{foreach from=$nextPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                {{if $nextPage}}
                <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
                {{/if}}
                {{if $last}}
                <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
                {{/if}}

            </div>
            {{/if}}
        </div>
    </div>


</div>
<script type="text/javascript" defer src="{{$LAYOUT_HELPER_URL}}libs/common/js/admin.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //Admin ready
        AdminReady();
    });
</script>