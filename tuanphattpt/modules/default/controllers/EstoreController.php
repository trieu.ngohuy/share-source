<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/user/models/User.php';

class default_EstoreController extends Nine_Controller_Action
{
    public function indexAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url[1]);
        $userSession = Nine_Registry::getLoggedInUser();

        if ($user == null || $user['alias'] != $userSession['alias']) {
            return false;
        }
        $this->setLayout('estore');
        $this->view->alias = $user['alias'] . '.html';
    }
}