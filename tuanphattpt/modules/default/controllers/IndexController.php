﻿<?php
require_once 'Zend/Config.php';
require_once 'Zend/Config/Writer/Array.php';

class default_IndexController extends Nine_Controller_Action {
    /*
     * Index action
     */

    public function indexAction() {

        try {
            //Set layout and title
            $this->setLayout('front');
            $this->view->headTitle("TUẤN PHÁT");

            //Get list banner
            $cat = @reset(Nine_Query::getListContentCategory(array(
                                'displayMode' => 3
            )));
            $this->view->banners = Nine_Query::getListContents(array(
                        'content_category_gid' => $cat['content_category_gid']
            ));
            
            //Get list bottom banner
            $cat = @reset(Nine_Query::getListContentCategory(array(
                                'displayMode' => 5
            )));
            $this->view->objBottomBannerCat = $cat;
            $this->view->arrBottomBanner = Nine_Query::getListContents(array(
                        'content_category_gid' => $cat['content_category_gid']
            ));

            //Get list product categories
            $this->view->categories = Nine_Query::getListProductCategory(array(
                        'displayMode' => 1
            ));

            //Get list project
            $this->view->projects = Nine_Read::GetListData('Models_Project', array(
                        'displayMode' => 1
            ));

            //Get list top supliers
            Nine_Query::getListSuplier($this->view);

            //Get config
            $this->view->config = Nine_Common::getConfig($this->view);
            //Set variable to hide popup
            $this->view->hidePopup = 1;
            //Set view output
            Nine_Common::SetViews($this, '/templates/default', 'index');
        } catch (Exception $exc) {
            echo $exc->getMessage();
            echo $exc->getTraceAsString();
        }
    }

    /*
     * Export pdf
     */

    public function exportPdfAction() {
        try {
            //Set layout
            $this->setLayout('default');
            //Get data
            $data = $this->_getParam("data", false);
            //Export file
            Nine_ExportFile::ExportPdfFile($data);
            echo '1';
        } catch (Exception $exc) {
            echo '0';
        }
    }

    /*
     * View pdf file
     */

    public function viewPdfAction() {
        $config = Nine_Common::getConfig();
        //echo $config['file_url'];
        // Let the browser know that a PDF file is coming.
        header("Content-type: application/pdf");
        header('Content-Disposition: inline; filename="' . $config['file_name'] . '"');
        header("Content-Length: " . filesize($config['file_url']));

        // Send the file to the browser.
        readfile($config['file_url']);
        exit;
    }

}
