<?php

require_once 'Zend/Config.php';
require_once 'Zend/Config/Writer/Array.php';
require_once 'modules/product/models/Product.php';
require_once 'modules/user/models/User.php';
require_once 'modules/buying/models/BuyingCategory.php';

class default_AdminController extends Nine_Controller_Action_Admin {

    public function indexAction() {
        $objProduct = new Models_Product();
        $objUser = new Models_User();
        $objBuyingCategory = new Models_BuyingCategory();

        $group = Nine_Registry::getLoggedInGroupId();
        if ($group == 3) {
            $user = Nine_Registry::getLoggedInUser();
            $this->_redirect('../estore/' . $user['alias'] . '.html');
        } else {
            $allProduct = $objProduct->getAllProduct();
            $freeUser = $objUser->getAllUsersWithGroup(array('group_id' => 4));
            $officialUser = $objUser->getAllUsersWithGroup(array('group_id' => 3));
            $buyingRequest = $objBuyingCategory->getAllCategories();
            $allUser = $objUser->getAll();

            $this->view->totalProduct = count($allProduct);
            $this->view->freeUser = count($freeUser);
            $this->view->officialUser = count($officialUser);
            $this->view->buyingRequest = count($buyingRequest);
            $this->view->allUser = count($allUser);
        }

        $this->view->headTitle(Nine_Language::translate('Welcome to Administrator'));
        $this->view->menu = array(
            0 => 'home',
            1 => ''
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-tachometer',
                'url' => Nine_Registry::getBaseUrl() . 'admin',
                'name' => Nine_Language::translate('Dashboard')
            )
        );
    }

    public function settingAction() {

        $this->view->headTitle(Nine_Language::translate('Setting CMS'));
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-tachometer',
                'url' => Nine_Registry::getBaseUrl() . 'admin',
                'name' => Nine_Language::translate('Dashboard')
            ),
            1 => array(
                'icon' => 'fa-building-o',
                'url' => '',
                'name' => Nine_Language::translate('Info Company')
            )
        );
        $this->view->menu = array(
            0 => 'setting',
            1 => 'info'
        );

        $data = $this->_getParam('data', false);
        $password = $this->_getParam('password', false);
        $errors = array();

        //Change password
        if (false != @$password && $password['password'] != '') {
            //Encrpty password
            $password['password'] = md5($password['password']);
            //Get loggin user
            $tmpUser = $_SESSION['user_login'];
            //check if save old password
            if ($tmpUser['password'] == $password['password']) {
                $this->view->passwordMessage = array(
                        'status' => 0,
                        'message' => "Mật khẩu trùng với mk hiện tại!"
                    );
            } else {
                //Update user password
                $objUser = new Models_User();
                $update = $objUser->update($password, array('user_id=?' => $tmpUser['user_id']));
                if ($update) {
                    $this->view->passwordMessage = array(
                        'status' => 1,
                        'message' => "Thay đổi mật khẩu thành công!"
                    );
                } else {
                    $this->view->passwordMessage = array(
                        'status' => 0,
                        'message' => "Thay đổi mật khẩu thất bại!"
                    );
                }
            }
        }
        //Save setting
        if (false != @$data) {


            $newSettings = $data;

            $target_dir = 'media/userfiles/user/';
            /*
             * Logo
             */
            if ($_FILES["image"]["name"] != '') {
                $target_dir = $target_dir . basename($_FILES["image"]["name"]);
                $uploadOk = 1;

                // Check if file already exists
                if (file_exists($target_dir . $_FILES["image"]["name"])) {
                    $errors['erro'] = 1;
                    $errors['mess'] = Nine_Language::translate('Tên File Đã Tồn Tại');
                    $uploadOk = 0;
                }

                // Check file size
                if ($_FILES["image"]["size"] > 20000000) {
                    $errors['erro'] = 1;
                    $errors['mess'] = Nine_Language::translate('File Lớn Hơn 20Mb.');
                    $uploadOk = 0;
                }

                // Only GIF files allowed
                if ($_FILES["image"]["type"] != "image/gif" && $_FILES["image"]["type"] != "image/jpg" && $_FILES["image"]["type"] != "image/png" && $_FILES["image"]["type"] != "image/jpeg") {
                    $errors['erro'] = 1;
                    $errors['mess'] = Nine_Language::translate('Chỉ nhận file png / jpg / gif / jpeg .');
                    $uploadOk = 0;
                }

                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                    
                } else {
                    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir)) {
                        $errors['erro'] = 0;
                        $newSettings['Logo'] = $target_dir;
                        $data['Logo'] = $target_dir;
                    } else {
                        die;
                        $errors['erro'] = 1;
                        $erro['mess'] = Nine_Language::translate('Sever Not Upload File . ');
                    }
                }
            }
            /*
             * Footer Logo
             */
            if ($_FILES["footer_logo_image"]["name"] != '') {
                $target_dir = $target_dir . basename($_FILES["footer_logo_image"]["name"]);
                $uploadOk = 1;

                // Check if file already exists
                if (file_exists($target_dir . $_FILES["footer_logo_image"]["name"])) {
                    $errors['erro'] = 1;
                    $errors['mess'] = Nine_Language::translate('Tên File Đã Tồn Tại');
                    $uploadOk = 0;
                }

                // Check file size
                if ($_FILES["footer_logo_image"]["size"] > 20000000) {
                    $errors['erro'] = 1;
                    $errors['mess'] = Nine_Language::translate('File Lớn Hơn 20Mb.');
                    $uploadOk = 0;
                }

                // Only GIF files allowed
                if ($_FILES["footer_logo_image"]["type"] != "image/gif" && $_FILES["footer_logo_image"]["type"] != "image/jpg" && $_FILES["image"]["type"] != "image/png" && $_FILES["image"]["type"] != "image/jpeg") {
                    $errors['erro'] = 1;
                    $errors['mess'] = Nine_Language::translate('Chỉ nhận file png / jpg / gif / jpeg .');
                    $uploadOk = 0;
                }

                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                    
                } else {
                    if (move_uploaded_file($_FILES["footer_logo_image"]["tmp_name"], $target_dir)) {
                        $errors['erro'] = 0;
                        $newSettings['FooterLogo'] = $target_dir;
                        $data['FooterLogo'] = $target_dir;
                    } else {
                        die;
                        $errors['erro'] = 1;
                        $erro['mess'] = Nine_Language::translate('Sever Not Upload File . ');
                    }
                }
            }
            /**
             * Save config
             */
            try {
                $writer = new Zend_Config_Writer_Array();
                $writer->setExclusiveLock(true);
                $writer->write('setting.php', new Zend_Config($newSettings));
                $writer->setExclusiveLock(false);

                $this->session->settingMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Lưu Cài Đặt Thành Công.')
                );
            } catch (Exception $e) {
                $this->session->settingMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Không Thể Thay Đổi Cài Đặt Vào Lúc Này.')
                );
            }
        } else {
            $oldSettings = include 'setting.php';
            $data = $oldSettings;
        }
        /**
         * Assign to view
         */
        $this->view->data = $data;
        $this->view->errors = $errors;
        $this->view->settingMessage = $this->session->settingMessage;
        $this->session->settingMessage = null;
    }

}
