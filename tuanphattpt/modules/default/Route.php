<?php

class Route_Default
{
    /**
     * Build friendly URL
     * @param array $linkArr 0 => <module>, 1 => controller, 2 => action, 3...n param/value
     * @param array $params param => value
     *
     * @return string friendly URL
     */
    public function build($linkArr, $params = array())
    {
        $result = implode('/', $linkArr);

        /**
         * Link is detail
         */

        if ('index' == @$linkArr[2]) {
            /**
             * Structure: pages/<id>/<alias>.html
             */
            $result = "/";
        }
        return $result;

    }

    /**
     * Parse friendly URL
     */
    public function parse()
    {
        $router = Nine_Controller_Front::getInstance()->getRouter();

        $route1 = new Zend_Controller_Router_Route_Regex(
            'estore/(.*).html',
            array(
                'module' => 'default',
                'controller' => 'estore',
                'action' => 'index'
            )
        );
        $router->addRoute('default1', $route1);

        /*Route Frontend*/
        $route2 = new Zend_Controller_Router_Route_Regex(
            '/',
            array(
                'module' => 'default',
                'controller' => 'index',
                'action' => 'index'
            )
        );
        $router->addRoute('default2', $route2);

        $route4 = new Zend_Controller_Router_Route_Regex(
            '(.*).htm',
            array(
                'module' => 'default',
                'controller' => 'index',
                'action' => 'index'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('default4', $route4);

        $route5 = new Zend_Controller_Router_Route_Regex(
            'search',
            array(
                'module' => 'default',
                'controller' => 'index',
                'action' => 'search'
            )
        );
        $router->addRoute('default5', $route5);
        $route6 = new Zend_Controller_Router_Route_Regex(
            'estore/manage-newslate/(.*).html',
            array(
                'module' => 'newlater',
                'controller' => 'estore',
                'action' => 'manage-category'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('default6', $route6);
        $route7 = new Zend_Controller_Router_Route_Regex(
            'estore/send-newsletter/(.*).html',
            array(
                'module' => 'newlater',
                'controller' => 'estore',
                'action' => 'send-newsletter'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('default7', $route7);
        $route8 = new Zend_Controller_Router_Route_Regex(
            'sitemap',
            array(
                'module' => 'default',
                'controller' => 'index',
                'action' => 'sitemap'
            )
        );
        $router->addRoute('default8', $route8);
    }
}