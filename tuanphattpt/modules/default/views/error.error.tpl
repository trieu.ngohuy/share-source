<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Thienchi.vn - Page Not found</title>
<link href="{{$LAYOUT_HELPER_URL}}front/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="{{$LAYOUT_HELPER_URL}}front/images/favicon.ico" />
</head>

<body style="background:none;">
<div id="">
  <div class="box_error">
  <p class="p15b"><a href="{{$BASE_URL}}"><img style="vertical-align: middle;" src="{{$LAYOUT_HELPER_URL}}front/images/logo.png"/></a> <span class="font40" style="padding-left: 100px;">404</span><span class="font30"> - Page Not Found</span></p>
   
   
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="80%">
      <br />
      <br />
      <p class="text_error font18">The requested URL <span class="color_bule">{{$url}}</span> was not found on this server.<br />
      <br />
      <br />
      <p class="p15t"><a href="{{$BASE_URL}}"><img src="{{$LAYOUT_HELPER_URL}}front/images/bt_homepage.jpg" /></a></p>
      </td>
    <td width="20%"><img src="{{$LAYOUT_HELPER_URL}}front/images/img_error.png" width="146" height="141" /></td>
  </tr>
  </table>
</div>
</div>
</body>
</html>
