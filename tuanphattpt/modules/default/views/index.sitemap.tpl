<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span>{{l}} Site Map{{/l}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- Blog category -->
                {{sticker name=front_default_category}}
                <!-- ./blog category  -->

                <!-- Banner -->
                <div class="col-xs-12 col-sm-12 col-md-12 m10t qc">
                    <img alt="Banner right 1" src="http://www.C&D.com/data/uploads/banner/20151202_Moments-Early-Menopause-Check2.jpg">
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 m10t  qc">
                    <img alt="Banner right 1" src="http://www.C&D.com/data/uploads/banner/20151202_Moments-Early-Menopause-Check2.jpg">
                </div>
                <!-- ./Banner -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <h1 class="page-heading">
                    <span class="page-heading-title2">{{l}}Site Map{{/l}}</span>
                </h1>
                <article class="entry-detail">
                    <div class="content-text clearfix">
							
							
							
							<div class="text-justify" style="padding: 10px 0px;">
								<table border="1" style="width: 100%">
									<tbody>
										
										{{foreach from=$allCategory item=item key=key}}
											<tr>
												<td colspan="4">
													<a href="{{$BASE_URL}}category/{{$item.alias}}"  style="color:#ff8c00;">
														<strong>&nbsp; {{$item.name}}</strong>
													</a>
												</td>
											</tr>
											
											{{if $item.cate|@count > 0}}
												
			                                    {{foreach from=$item.cate item=item1 key=key1}}
			                                    {{if $key1%4 == 0}}
			                                    <tr>
			                                    {{/if}}
			                                    	<td style="padding:10px;">
														<p>
															<a href="{{$BASE_URL}}category/{{$item1.alias}}"><strong>{{$item1.name}}</strong></a>
														</p>
														{{if $item1.cate|@count > 0}}
			                                            	{{foreach from=$item1.cate item=item2 key=key2}}
			                                                <p><a href="{{$BASE_URL}}category/{{$item2.alias}}">{{$item2.name}}</a></p>
			                                                {{/foreach}}
			                                            {{/if}}
													</td>
												{{if $key1%4 == 3 || ($key1 == $item1|@count - 1 && $key1%4 != 0) }}
												</tr>
												{{/if}} 
			                                    {{/foreach}}
			                                {{/if}}
					                             
										{{/foreach}}
										
										<tr>
											<td colspan="4"></td>
										</tr>
										<tr>
											<td style="padding:10px;">
												<p>
													<strong>COMPANY</strong></p>
							                    <p>
														<a href="{{$BASE_URL}}page/vision-mission">Vision & Mission</a></li>
												</p>
							                    <p>
														<a href="{{$BASE_URL}}page/messages-from-ceo">Messages from CEO</a></li>
												</p>
							                    <p>
														<a href="{{$BASE_URL}}page/introduction">Introduction</a></li>
												</p>
							                    <p>
														<a href="{{$BASE_URL}}sitemap">Sitemap</a></li>
												</p>
							                    <p>
														<a href="{{$BASE_URL}}page/advertise-with-us">Advertise with Us</a>
												</p>
											</td>
											<td style="padding:10px;">
												<p>
													<strong>INFORMATION</strong></p>
							                    <p><a href="{{$BASE_URL}}page/term-of-use">Term of Use</a></p>
					                            <p><a href="{{$BASE_URL}}page/privacy-policy">Privacy Policy</a></p>
					                            <p><a href="{{$BASE_URL}}page/quality-standards">Quality Standards</a></p>
					                            <p><a href="{{$BASE_URL}}page/my-addresses">My Addresses</a></p>
					                            <p><a href="{{$BASE_URL}}page/quality-standards-vietnam">Quality Standards Vietnam</a></p>
											</td>
											<td style="padding:10px;">
												<p>
													<strong>HELP CENTER</strong></p>
												<p><a href="{{$BASE_URL}}page/faqs">FAQs</a></p>
					                            <p><a href="{{$BASE_URL}}page/learning-center">Learning Center</a></p>
					                            <p><a href="{{$BASE_URL}}contact-us">Contact Us</a></p>
					                            <p><a href="{{$BASE_URL}}page/payment-instructions">Payment Instructions</a></p>
					                           <p><a href="{{$BASE_URL}}page/post-buying-request-rule">Post Buying Request Rule</a></p>
											</td>
										</tr>
										
										<tr>
											<td colspan="4"></td>
										</tr>
										<tr>
											<td style="padding:10px;">
												<p>
													<strong>For Buyers</strong></p>
							                    <p><a href="{{$BASE_URL}}category/hot-deals">{{l}}Hot Deals{{/l}}</a></p>
													{{if $isLogin == true && $userLogin.group_id != 3}}
													<p><a href="{{$BASE_URL}}post-buying-request">{{l}}Post Buying Request{{/l}}</a></p>
													{{/if}}
													<p><a href="{{$BASE_URL}}seller">{{l}}Sellers{{/l}}</a></p>
											</td>
											<td style="padding:10px;">
												<p>
													<strong>For Sellers</strong></p>
							                    <p><a>{{l}}Upgrade Qualify Member{{/l}}</a></p>
												<p><a href="{{$BASE_URL}}page/learning-center">{{l}}Learning Center{{/l}}</a></p>
												<p><a href="{{$BASE_URL}}page/member-benefits">{{l}}Member Benefit{{/l}}</a></p>
											</td>
											<td style="padding:10px;">
												<p>
													<strong>Quality Standards</strong></p>
												<p><a href="{{$BASE_URL}}hs-code">{{l}}HS Code{{/l}}</a></p>
												<p><a href="{{$BASE_URL}}hs-code-vn">{{l}}HS Code Vietnam{{/l}}</a></p>
											</td>
											<td style="padding:10px;">
												<p>
													<strong>Page</strong></p>
												<p><a href="{{$BASE_URL}}news">{{l}}News{{/l}}</a></p>
												<p><a href="{{$BASE_URL}}trade-show">{{l}}Trade Show{{/l}}</a></p>
											</td>
										</tr>
			
			
									</tbody>
								</table>
								<p>
									&nbsp; &nbsp;</p>
							</div>



                    </div>

                </article>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>