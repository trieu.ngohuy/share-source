<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>  
<style type="text/css">
    #images { list-style-type: none; margin: 0; padding: 0;}
    #images li { margin: 10px; float: left; text-align: center;  height: 180px;}
</style>
<div>
    <ul class="breadcrumb">
        <li>
            <a href="{{$BASE_URL}}">Trang Chủ</a>
        </li>
        <li>
            <a href="{{$BASE_URL}}default/site/index">Thông Tin Website</a>
        </li>
    </ul>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            CKFinder.setupCKEditor(null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/');
        });
        $().ready(function () {
            // validate signup form on keyup and submit
            $("input[type=password]").val('');
        });
    </script>


    <div class="page-header">
        <h1>
            {{l}}Setting{{/l}}
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                {{l}}Edit Config Website{{/l}}
            </small>
        </h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-xs-12">
                    {{if $errors.erro == 1}}
                    <div class="alert alert-danger">
                        <button class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>
                        {{if $errors.main}}
                        </strong> {{$errors.main}}
                        {{else}}
                        {{l}}Please check following information again{{/l}}
                        {{/if}} 
                    </div>
                    {{/if}}

                    {{if $settingMessage !=  array()}}
                    <div class="alert {{if $settingMessage.success == false}}alert-danger{{else}}alert-success{{/if}}">
                        <button class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>
                        </strong> {{$settingMessage.message}}
                    </div>
                    {{/if}}

                    <div class="col-sm-12 widget-container-col ui-sortable">

                        <div class="tabbable tabs-left">

                            <ul class="nav nav-tabs" id="myTab2">

                                <li class="active">
                                    <a href="#tab-setting" data-toggle="tab" aria-expanded="true">
                                        <i class="pink ace-icon fa fa-optin-monster bigger-110"></i>
                                        {{l}}Setting{{/l}}
                                    </a>
                                </li>
                            </ul>

                            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="tab-content">

                                    <div id="tab-setting" class="tab-pane active">

                                        {{foreach from=$theme_config item=item}}
                                        {{$item.html}}
                                        {{/foreach}}

                                        <div class="clearfix form-actions">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button class="btn btn-info" type="submit">
                                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                                    {{l}}Save{{/l}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </form>

                        </div>

                    </div>



                </div><!-- /.span -->

            </div><!-- /.row -->



        </div>


    </div>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            $('#select_avatar').click(function (event) {
                $('#ip_avatar').click();
            });

            $('#ip_avatar').change(function (event) {
                var tmppath = URL.createObjectURL(event.target.files[0]);
                $('#display_avatar').attr('src', URL.createObjectURL(event.target.files[0]));
                $("#no_avatar").css("display", "none");
                $("#avatar").css("display", "block");
            });

        });
        var flag = {{$data.BankAcount|@count}};

        function addProduct() {
            $.ajax({
                url: '{{$BASE_URL}}default/site/add-bank/id/' + flag,
                type: "POST",
                success: function (data) {
                    $('#ct_product').append(data);
                }
            });
            flag++;
        }
        function removeProduct(id) {
            $('#' + id).html('');
        }

    </script>
