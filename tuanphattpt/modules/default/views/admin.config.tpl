<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.js"></script>
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>  
<link href="{{$LAYOUT_HELPER_URL}}admin/js/jquery-ui/css/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery-ui/js/jquery-ui.js"></script>
<style type="text/css">
    #images { list-style-type: none; margin: 0; padding: 0;}
    #images li { margin: 10px; float: left; text-align: center;  height: 180px;}
</style>
<div>
    <ul class="breadcrumb">
    	<li>
            <a href="{{$BASE_URL}}">Trang Chủ</a>
        </li>
        <li>
            <a href="{{$BASE_URL}}default/site/index">Thông Tin Website</a>
        </li>
    </ul>
    
    <script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function (){
	        CKFinder.setupCKEditor( null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/' );
	    });
		$().ready(function() {
		    // validate signup form on keyup and submit
		    $("input[type=password]").val('');
		});
	</script>
	<div id="main">
		<form action="" method="post" enctype="multipart/form-data" >
			<div id="content">
				<div>
					<div class="tabbable">
						<h2><strong>Thay Đổi Thông Tin</strong> Website </h2>
						<hr>
						<ul class="nav nav-tabs" data-provide="tabdrop">
							<li class="active">
								<a href="#tab1" data-toggle="tab">Cơ Bản</a>
							</li>
						</ul>
						<div class="tab-content">
							
							<div class="form-horizontal tab-pane fade in active" id="tab1">
								{{if $settingMessage|@count > 0 && $settingMessage.success != true}}
			                        <div class="alert alert-danger">
			                        	<a href="#" class="close"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
											<strong>Thông Báo!</strong> {{$settingMessage.message}}.
									</div>
			                	{{/if}}
			                	{{if $settingMessage|@count > 0 && $settingMessage.success == true}}
			                        <div class="alert alert-success">
			                        	<a href="#" class="close"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
											<strong>Thông Báo!</strong> {{$settingMessage.message}}.
									</div>
			                	{{/if}}
			                	<div class="form-group col-md-9">
				                	<div class="col-md-12" style="margin-bottom: 30px;">
										<label class="control-label col-md-4">Tên Website</label>
										<div class="col-md-7">
												<input type="text" class="form-control" name="data[WebsiteName]" value="{{$data.WebsiteName}}">
										</div>
									</div>
									<div class="col-md-12">
										<label class="control-label col-md-4">Email</label>
										<div class="col-md-7">
												<input type="text" class="form-control" name="data[Email]"  value="{{$data.Email}}" >
										</div>
									</div>
								</div>
								<input style="display: none;" type="file" name="image" id="ip_avatar">
								<input type="hidden" value="{{$data.Logo}}" name="data[Logo]">
								<div class="form-group col-md-3" id="select_avatar">
									<div id="avatar" style="{{if $data.Logo == ''}}display: none;{{/if}}">
                          				<img src="{{$BASE_URL}}{{$data.Logo}}" id="display_avatar" style="max-width: 150px; max-height:150px; border:dashed blue thin;"></img>
                                     </div>
                                  	<div id="no_avatar" style="{{if $data.Logo != ''}}display: none;{{/if}}width: 150px; border: thin blue dashed; text-align: center; padding:70px 0px;">
                                		Logo
                               		</div>
								</div>
								
								<br style="clear: both;">
								<div class="form-group">
									<label class="control-label col-md-3">Địa Chỉ</label>
									<div class="col-md-9">
											<input type="text" class="form-control" name="data[Add]"  value="{{$data.Add}}" >
											<input type="text" class="form-control" name="data[Add_en]"  value="{{$data.Add_en}}" >
									</div>
								</div>
                                <div class="form-group">
									<label class="control-label col-md-3">Bp Order</label>
									<div class="col-md-9">
											<input type="text" class="form-control" name="data[BpOrder]"  value="{{$data.BpOrder}}" >
											<input type="text" class="form-control" name="data[BpOrder_en]"  value="{{$data.BpOrder_en}}" >
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Bp Kế Toán</label>
									<div class="col-md-9">
											<input type="text" class="form-control" name="data[BpAccountant]"  value="{{$data.BpAccountant}}" >
											<input type="text" class="form-control" name="data[BpAccountant_en]"  value="{{$data.BpAccountant_en}}" >
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Website</label>
									<div class="col-md-9">
											<input type="text" class="form-control" name="data[Website]"  value="{{$data.Website}}" >
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Status Footer</label>
									<div class="col-md-9">
											<input type="text" class="form-control" name="data[status_footer]"  value="{{$data.status_footer}}" >
											<input type="text" class="form-control" name="data[status_footer_en]"  value="{{$data.status_footer_en}}" >
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Hotline</label>
									<div class="col-md-9">
											<input type="text" class="form-control" name="data[hotline]"  value="{{$data.hotline}}" >
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Facebook</label>
									<div class="col-md-9">
											<input type="text" class="form-control" name="data[facebook]"  value="{{$data.facebook}}" >
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-3">Google</label>
									<div class="col-md-9">
											<input type="text" class="form-control" name="data[google]"  value="{{$data.google}}" >
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Skype</label>
									<div class="col-md-9">
											<input type="text" class="form-control" name="data[skype]"  value="{{$data.skype}}" >
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Youtobe</label>
									<div class="col-md-9">
											<input type="text" class="form-control" name="data[youtube]"  value="{{$data.youtube}}" >
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Yahoo</label>
									<div class="col-md-9">
											<input type="text" class="form-control" name="data[yahoo]"  value="{{$data.yahoo}}" >
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Status Home</label>
									<div class="col-md-9">
											<input type="text" class="form-control" name="data[status_home]"  value="{{$data.status_home}}" >
											<input type="text" class="form-control" name="data[status_home_en]"  value="{{$data.status_home_en}}" >
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Slogan</label>
									<div class="col-md-9">
											<textarea class="form-control textarea ckeditor" name="data[slogan]" >{{$data.slogan}}</textarea>
											<textarea class="form-control textarea ckeditor" name="data[slogan_en]" >{{$data.slogan_en}}</textarea>
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-3">Description</label>
									<div class="col-md-9">
											<textarea class="form-control textarea ckeditor" name="data[Description]" >{{$data.Description}}</textarea>
											<textarea class="form-control textarea ckeditor" name="data[Description_en]" >{{$data.Description_en}}</textarea>
									</div>
								</div>
								
								<div class="box-content" id="ct_product">
		            				{{foreach from=$data.BankAcount item=item key=key}}
				                	<div id="product_{{$key}}">
					                	<div class="col-md-3">
						                    <div class="form-group">
						                        <label for="exampleInputEmail1">Tên Ngân Hàng</label>
						                        <input type="text" class="form-control" name="data[BankAcount][{{$key}}][0]" value="{{$item.0}}" >
						                    </div>
					                    </div>
					                    <div class="col-md-3">
						                    <div class="form-group">
						                        <label for="exampleInputEmail1">Số Tài Khoản</label>
						                        <input type="text" class="form-control" name="data[BankAcount][{{$key}}][1]" value="{{$item.1}}" >
						                    </div>
					                    </div>
					                    <div class="col-md-3">
						                    <div class="form-group">
						                        <label for="exampleInputEmail1">Chi Nhánh</label>
						                        <input type="text" class="form-control" name="data[BankAcount][{{$key}}][2]" value="{{$item.2}}" >
						                    </div>
					                    </div>
					                    <div class="col-md-2">
						                    <div class="form-group">
						                        <label for="exampleInputEmail1">Chủ Tài Khoản</label>
						                        <input type="text" class="form-control" name="data[BankAcount][{{$key}}][3]" value="{{$item.3}}">
						                    </div>
					                    </div>
					                    <div class="col-md-1">
					                    	<div class="form-group">
					                    		<label for="exampleInputEmail1"> </label><br>
					                    		
					                    		<button type="button" class="btn btn-default btn-info" onclick="removeProduct('product_{{$key}}');"><i class="glyphicon glyphicon-trash"></i></button>
					                    	</div>
					                    </div>
										<div class="clearfix"></div>
									</div>
							
									{{/foreach}}
		            			</div>
		            
		            
								<button type="button" onclick="addProduct();" class="btn btn-default btn-info" style="position: fixed;top: 80px;right: 0px;"><i class="glyphicon glyphicon-plus"></i></button>
		            			<button type="submit" class="btn btn-default btn-info" style="position: fixed;top: 120px;right: 0px;"><i class="glyphicon glyphicon-floppy-disk"></i></button>                            
			                	
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function (){
			$('#select_avatar').click(function(event) {
			  $('#ip_avatar').click();
			});

			$('#ip_avatar').change(function(event) {
				var tmppath = URL.createObjectURL(event.target.files[0]);
			  	$('#display_avatar').attr('src',URL.createObjectURL(event.target.files[0]));
			  	$("#no_avatar").css("display", "none");
			  	$("#avatar").css("display", "block");
			});
	
	});
	var flag = {{$data.BankAcount|@count}};
	
	function addProduct(){
		$.ajax({
	        url: '{{$BASE_URL}}default/site/add-bank/id/'+flag,
	        type: "POST",
	        success: function(data){
	        	$('#ct_product').append(data);
	        }
	     });
		flag++;
	}
	function removeProduct(id){
		$('#'+id).html('');
	}
	
</script>