<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>  
<style type="text/css">
    #images { list-style-type: none; margin: 0; padding: 0;}
    #images li { margin: 10px; float: left; text-align: center;  height: 180px;}
</style>
<div>
    <ul class="breadcrumb">
        <li>
            <a href="{{$BASE_URL}}">Trang Chủ</a>
        </li>
        <li>
            <a href="{{$BASE_URL}}default/site/index">Thông Tin Website</a>
        </li>
    </ul>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            CKFinder.setupCKEditor(null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/');
        });
        $().ready(function () {
            // validate signup form on keyup and submit
            $("input[type=password]").val('');
        });
    </script>


    <div class="page-header">
        <h1>
            {{l}}Setting{{/l}}
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                {{l}}Edit Config Website{{/l}}
            </small>
        </h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <div class="col-xs-12">
                    {{if $errors.erro == 1}}
                    <div class="alert alert-danger">
                        <button class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>
                        {{if $errors.main}}
                        </strong> {{$errors.main}}
                        {{else}}
                        {{l}}Please check following information again{{/l}}
                        {{/if}} 
                    </div>
                    {{/if}}

                    {{if $settingMessage !=  array()}}
                    <div class="alert {{if $settingMessage.success == false}}alert-danger{{else}}alert-success{{/if}}">
                        <button class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>
                        </strong> {{$settingMessage.message}}
                    </div>
                    {{/if}}

                    <div class="col-sm-12 widget-container-col ui-sortable">

                        <div class="tabbable tabs-left">

                            <ul class="nav nav-tabs" id="myTab2">
                                <li class="active">
                                    <a href="#tab-basic" data-toggle="tab" aria-expanded="true">
                                        <i class="pink ace-icon fa fa-home bigger-110"></i>
                                        {{l}}Basic{{/l}}
                                    </a>
                                </li>
                            </ul>
                            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="tab-content">
                                    <!--TAB: Basic-->
                                    <div id="tab-basic" class="tab-pane active">

                                        <div class="form-group col-md-9">
                                            <div class="col-md-12" style="margin-bottom: 30px;">
                                                <label class="control-label col-md-4">Tên Website</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control" name="data[WebsiteName]" value="{{$data.WebsiteName}}">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="control-label col-md-4">Email</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control" name="data[Email]"  value="{{$data.Email}}" >
                                                </div>
                                            </div>
                                        </div>
                                        <!--Logo-->
                                        <input style="display: none;" type="file" name="image" id="ip_avatar">
                                        <input type="hidden" value="{{$data.Logo}}" name="data[Logo]">
                                        <div class="form-group col-md-3" id="select_avatar">
                                            <div id="avatar" style="{{if $data.Logo == ''}}display: none;{{/if}}">
                                                <img src="{{$BASE_URL}}{{$data.Logo}}" id="display_avatar" style="max-width: 100%; max-height:80px; border:dashed blue thin;"></img>
                                            </div>
                                            <div id="no_avatar" style="{{if $data.Logo != ''}}display: none;{{/if}}width: 100%; border: thin blue dashed; text-align: center; padding:38px 0px;">
                                                Logo
                                            </div>
                                        </div>
                                        <!--Logo Footer-->
                                        <input style="display: none;" type="file" name="footer_logo_image" id="ip_footer_logo">
                                        <input type="hidden" value="{{$data.FooterLogo}}" name="data[FooterLogo]">
                                        <div class="form-group col-md-3" id="select_footer_logo">
                                            <div id="footer_logo" style="{{if $data.FooterLogo == ''}}display: none;{{/if}}">
                                                <img src="{{$BASE_URL}}{{$data.FooterLogo}}" id="display_footer_logo" style="max-width: 100%; max-height:80px; border:dashed blue thin;"></img>
                                            </div>
                                            <div id="no_footer_logo" style="{{if $data.FooterLogo != ''}}display: none;{{/if}}width: 100%; border: thin blue dashed; text-align: center; padding:38px 0px;">
                                                FooterLogo
                                            </div>
                                        </div>
                                        <br style="clear: both;">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Địa Chỉ</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[Add]"  value="{{$data.Add}}" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Văn phòng</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[Office]"  value="{{$data.Office}}" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Mã số thuế</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[MST]"  value="{{$data.MST}}" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Website</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[Website]"  value="{{$data.Website}}" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status Footer</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[status_footer]"  value="{{$data.status_footer}}" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Hotline</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[hotline]"  value="{{$data.hotline}}" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{l}}Số điện thoại liên hệ{{/l}}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[phone_call]"  value="{{$data.phone_call}}" >
                                                <p>Hãy nhập số điện thoại theo format sau: (84)xxxxxxxxxx</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Facebook</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[facebook]"  value="{{$data.facebook}}" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Google</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[google]"  value="{{$data.google}}" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Skype</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[skype]"  value="{{$data.skype}}" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Youtobe</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[youtube]"  value="{{$data.youtube}}" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Yahoo</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[yahoo]"  value="{{$data.yahoo}}" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status Home</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[status_home]"  value="{{$data.status_home}}" >
                                            </div>
                                        </div>

                                        <div class="clearfix form-actions">
                                            <label class="control-label col-md-3">Google Embeded url</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[google_map]"  value="{{$data.google_map}}" >
                                            </div>
                                        </div>

                                        <div class="clearfix form-actions">
                                            <label class="control-label col-md-3">{{l}}Thông tin liên hệ{{/l}}</label>
                                            <div class="col-md-9">
                                                <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[contact_detail]" rows="20" cols="90">{{$data.contact_detail}}</textarea>
                                            </div>
                                        </div>
                                        <div class="clearfix form-actions">
                                            <label class="control-label col-md-3">{{l}}Thông tin liên hệ EN{{/l}}</label>
                                            <div class="col-md-9">
                                                <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[contact_detail_en]" rows="20" cols="90">{{$data.contact_detail_en}}</textarea>
                                            </div>
                                        </div>

                                        <div class="clearfix form-actions">
                                            <label class="control-label col-md-3">Contacts</label>
                                            <div class="col-md-9">
                                                <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[Contacts]" rows="20" cols="90">{{$data.Contacts}}</textarea>
                                            </div>
                                        </div>

                                        <div class="clearfix form-actions">
                                            <label class="control-label col-md-3">Policy</label>
                                            <div class="col-md-9">
                                                <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[Description]" rows="20" cols="90">{{$data.Description}}</textarea>
                                            </div>
                                        </div>


                                        <div class="clearfix form-actions">
                                            <label class="control-label col-md-3">Chi nhánh</label>
                                            <div class="col-md-9">
                                                <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[HeadQuaters]" rows="20" cols="90">{{$data.HeadQuaters}}</textarea>
                                            </div>
                                        </div>
                                        <div class="clearfix form-actions">
                                            <label class="control-label col-md-3">Headquaters</label>
                                            <div class="col-md-9">
                                                <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[HeadQuaters_en]" rows="20" cols="90">{{$data.HeadQuaters_en}}</textarea>
                                            </div>
                                        </div>

                                        <div class="clearfix form-actions">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button class="btn btn-info" type="submit">
                                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                                    {{l}}Save{{/l}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </form>

                        </div>

                    </div>



                </div><!-- /.span -->

            </div><!-- /.row -->



        </div>


    </div>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            $('#select_avatar').click(function (event) {
                $('#ip_avatar').click();
            });

            $('#ip_avatar').change(function (event) {
                var tmppath = URL.createObjectURL(event.target.files[0]);
                $('#display_avatar').attr('src', URL.createObjectURL(event.target.files[0]));
                $("#no_avatar").css("display", "none");
                $("#avatar").css("display", "block");
            });

            $('#select_footer_logo').click(function (event) {
                $('#ip_footer_logo').click();
            });

            $('#ip_footer_logo').change(function (event) {
                var tmppath = URL.createObjectURL(event.target.files[0]);
                $('#display_footer_logo').attr('src', URL.createObjectURL(event.target.files[0]));
                $("#no_footer_logo").css("display", "none");
                $("#footer_logo").css("display", "block");
            });

            //Open change password dialog
            $('#divHook img').click(function () {
                if ($('#divHookDetail').is(":visible")) {
                    $('#divHookDetail').fadeOut('fast');
                } else {
                    $('#divHookDetail').fadeIn('fast');
                }
            });
            //If update password success or failed then show dialog
        {{if isset($passwordMessage)}}
            $('#divHookDetail').fadeIn('fast');
            //If success then wait 3 seconds then logout
        {{if $passwordMessage.status == 1}}
            setTimeout(
                    function ()
                    {
                        window.location.href = "{{$BASE_URL}}admin/access/admin/logout";
                    }, 2000);
        {{/if}}

        {{/if}}

        });
        var flag = {{$data.BankAcount|@count}};

        function addProduct() {
            $.ajax({
                url: '{{$BASE_URL}}default/site/add-bank/id/' + flag,
                type: "POST",
                success: function (data) {
                    $('#ct_product').append(data);
                }
            });
            flag++;
        }
        function removeProduct(id) {
            $('#' + id).html('');
        }

    </script>

    <div id="divHook">
        <img src="https://www.webdevelopersnotes.com/wp-content/uploads/how-do-i-change-hotmail-password.png" alt="tuanphat" width="80"/>
        <div id="divHookDetail">
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                <h3>Thay đổi mật khẩu</h3>
                <hr>
                {{if isset($passwordMessage)}}
                <div class="alert alert-{{if $passwordMessage.status == 1}}success{{else}}danger{{/if}}" role="alert">
                    {{$passwordMessage.message}}
                    {{if $passwordMessage.status == 1}}
                    <br>
                    <i>Logout sau 3 giây....</i>
                    {{/if}}
                </div>
                {{/if}}
                <div class="form-group">
                    <label class="control-label col-md-5">Nhập mật khẩu</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" name="password[password]"  value="" >
                    </div>
                </div>
                <div class="clearfix form-actions">
                    <div class="col-md-offset-5 col-md-7">
                        <button class="btn btn-info" type="submit">
                            <i class="ace-icon fa fa-check bigger-110"></i>
                            Lưu
                        </button>
                    </div>
                </div>
            </form>  
        </div>
    </div>
    <style>
        div#divHook {
            position: fixed;
            z-index: 99999999;
            right: 0;
            top: 200;
        }
        div#divHook img {
            cursor: pointer;
        }
        div#divHook img:hover {
            border-radius: 5px;
            border: 5px solid #074809;
        }
        div#divHookDetail {
            display: none;
            position: absolute;
            right: 83px;
            width: 300px;
            background: #ffffff;
            border: 1px solid gray;
            padding: 10px;
            text-align: CENTER;
            top: -10;
            box-shadow: 0px 0px 5px 5px #888888;
        }
    </style>