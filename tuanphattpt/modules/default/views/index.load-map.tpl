
						<div class="home-map col-md-12 col-xs-12">
							<!--MAP-->
							
							
							<script type="text/javascript">
							    //Array of JSON objects.
								
									{{if $allVendor|@count > 0}}
										var markers = [
												    {{foreach from=$allVendor item=item key=key}}
												    {
												        "title": '{{$item.full_name}}',
												        "lat": '{{$item.lattitude}}',
												        "lng": '{{$item.longtitude}}',
												        "description": '<img  style="width:300px;height:100px;float:left; " src="{{$BASE_URL}}{{$item.main_image}}"><div style="width:300px;float:left;font-size:16px;font-weight:bold;margin:10px 0px;">{{$item.full_name}}</div><div style="width:300px;float:left;font-size:14px;margin:0px 0px 10px 0px;">{{$item.admin_note}}</div>'
												    },
												    {{/foreach}}
										];
								    	var mapOptions = {
									            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
									            zoom: 8,
									            mapTypeId: google.maps.MapTypeId.ROADMAP
									        };
								    {{else}}
								    	var mapOptions = {
									            center: new google.maps.LatLng(10.819587,106.622916),
									            zoom: 5,
									            mapTypeId: google.maps.MapTypeId.ROADMAP
									        };
								    {{/if}}
							        
							        var infoWindow = new google.maps.InfoWindow();
							        var latlngbounds = new google.maps.LatLngBounds();
							        var map = new google.maps.Map(document.getElementById("gmap_canvas"), mapOptions);
							 
							        {{foreach from=$allVendor item=item key=key}}
							            var data = {
										        "title": '{{$item.full_name}}',
										        "lat": '{{$item.lattitude}}',
										        "lng": '{{$item.longtitude}}',
										        "description": '<a href="{{$item.url}}"><img  style="width:300px;height:100px;float:left; " src="{{$BASE_URL}}{{$item.main_image}}"><div style="width:300px;float:left;font-size:16px;font-weight:bold;margin:10px 0px;">{{$item.full_name}}</div><div style="width:300px;float:left;font-size:14px;margin:0px 0px 10px 0px;">{{$item.admin_note}}</div></a>'
										    };
							            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
							            var marker = new google.maps.Marker({
							                position: myLatlng,
							                map: map,
							                title: data.title,
							                icon: '{{$BASE_URL}}{{$item.pimage}}'
							            });
							            (function (marker, data) {
							                google.maps.event.addListener(marker, "mouseover", function (e) {
							                    infoWindow.setContent("<div style = 'width:300px;min-height:40px'>" + data.description + "</div>");
							                    infoWindow.open(map, marker);
							                });
							            })(marker, data);
							            latlngbounds.extend(marker.position);
							        {{/foreach}}
								    {{if $allVendor|@count == 0}}
									       	var myLatlng = new google.maps.LatLng(10.819587,106.622916);
								            
								    {{else}}
									    var bounds = new google.maps.LatLngBounds();
								        map.setCenter(latlngbounds.getCenter());
								        map.fitBounds(latlngbounds);
								    {{/if}}
							        
							        
							</script>


							<div style='overflow:hidden;height:380px;width:100%;'>
								<div id='gmap_canvas' style='height:380px;width:100%;'>
									
								</div>
							</div>
							<div class="home-vendor col-md-12 col-xs-12 text-center">
								<div class="col-md-12 col-xs-12">{{l}}VENDOR{{/l}}</div>
								<table  class="col-md-12 col-xs-12">
									<tr>
										<td>
											<div style="background: #ffd828;height: 15px; width: 10px;float: left;">
												
											</div>
											<p style="font-size: 9px;line-height: 15px; color: #fff;float: left;">{{l}}QUẦN ÁO{{/l}}</p>
										</td>
										<td>
											<div style="background: #9018e6;height: 15px; width: 10px;float: left;">
												
											</div>
											<p style="font-size: 9px;line-height: 15px; color: #fff;float: left;">{{l}}PHỤ KIỆN{{/l}}</p>
										</td>
										<td>
											<div style="background: #1890e6;height: 15px; width: 10px;float: left;">
												
											</div>
											<p style="font-size: 9px;line-height: 15px; color: #fff;float: left;">{{l}}DU LỊCH{{/l}}</p>
										</td>
										
										<td>
											<div style="background: #ff0027;height: 15px; width: 10px;float: left;">
												
											</div>
											<p style="font-size: 9px;line-height: 15px; color: #fff;float: left;">{{l}}CÔNG NGHỆ{{/l}}</p>
										</td>
										<td>
											<div style="background: #874500;height: 15px; width: 10px;float: left;">
												
											</div>
											<p style="font-size: 9px;line-height: 15px; color: #fff;float: left;">{{l}}BẤT ĐỘNG SẢN{{/l}}</p>
										</td>
										<td>
											<div style="background: #47b74d;height: 15px; width: 10px;float: left;">
												
											</div>
											<p style="font-size: 9px;line-height: 15px; color: #fff;float: left;">{{l}}ĂN UỐNG{{/l}}</p>
										</td>
										<td>
											<div style="background: #ff7200;height: 15px; width: 10px;float: left;">
												
											</div>
											<p style="font-size: 9px;line-height: 15px; color: #fff;float: left;">{{l}}DỊCH VỤ{{/l}}</p>
										</td>
									</tr>
								</table>
							</div>
							<div class="home-search-vendor col-md-12 col-xs-12 text-center">
								  <div class="form-group">
								    <input type="text" class="form-control" id="searchVendor" value="{{$name_search}}" placeholder="{{l}}Search vendor name here{{/l}}">
								  </div>
							</div>
						</div>
						
						<script>
						$(document).ready(function(){
							 $('#searchVendor').keyup(function( event ) {
								 if(event.keyCode == 13){
									 htmlMap($('#searchVendor').val());
								 }
							 });
						});
						</script>
				