<link rel="stylesheet" href="{{php}} echo SOCKET_IO; {{/php}}chat_css/normalize.css" />
<link rel="stylesheet" href="{{php}} echo SOCKET_IO; {{/php}}chat_css/style.css" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.css"/>


						<div class="page-header">
							<h1>
								{{l}}Dashboard{{/l}}
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									{{l}}overview &amp; stats{{/l}}
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="alert alert-block alert-success">
									<button type="button" class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>

									<i class="ace-icon fa fa-check green"></i>

									Welcome to
									<strong class="green">
										C&D MANAGER APPLICATION
										<small>(v1.3.3)</small>
									</strong>
								</div>

								<div class="row">
									<div class="space-6"></div>

									<div class="col-sm-7 infobox-container">
										<!-- #section:pages/dashboard.infobox -->
										<div class="infobox infobox-green">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-shopping-cart"></i>
											</div>
											<div class="infobox-data">
												<span class="infobox-data-number">{{$totalProduct}}</span>
												<div class="infobox-content">{{l}}Tổng Số Sản Phẩm{{/l}}</div>
											</div>
										</div>


										<div class="infobox infobox-blue">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-user"></i>
											</div>
											<div class="infobox-data">
												<span class="infobox-data-number">{{$freeUser}}</span>
												<div class="infobox-content">{{l}}Tổng Thành Viên Miễn Phí{{/l}}</div>
											</div>
										</div>

										<div class="infobox infobox-pink">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-product-hunt"></i>
											</div>
											<div class="infobox-data">
												<span class="infobox-data-number">{{$officialUser}}</span>
												<div class="infobox-content">{{l}}Tổng Thành Viên Thu Phí{{/l}}</div>
											</div>
										</div>

										<div class="infobox infobox-orange">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-fire"></i>
											</div>
											<div class="infobox-data">
												<span class="infobox-data-number">{{$buyingRequest}}</span>
												<div class="infobox-content">{{l}}Tổng Số Lượng Yêu Cầu Mua{{/l}}</div>
											</div>
										</div>
									</div>

									<div class="vspace-12-sm"></div>

									<div class="col-sm-5">
										<div class="widget-box">
											<div class="widget-header widget-header-flat widget-header-small">
												<h5 class="widget-title">
													<i class="ace-icon fa fa-signal"></i>
													{{l}}Đồ Thị{{/l}}
												</h5>

											</div>

											<div class="widget-body">
												<div class="widget-main">
													<!-- #section:plugins/charts.flotchart -->
													<div id="piechart-placeholder"></div>

													<!-- /section:plugins/charts.flotchart -->
													<div class="hr hr8 hr-double"></div>

													<div class="clearfix">
														<!-- #section:custom/extra.grid -->
														<div class="grid3">
															<span class="grey">
																<i class="ace-icon fa fa-product-hunt fa-2x blue"></i>
																&nbsp; Product
															</span>
															<h4 class="bigger pull-right">{{$totalProduct}}</h4>
														</div>

														<div class="grid3">
															<span class="grey">
																<i class="ace-icon fa fa-shopping-cart fa-2x purple"></i>
																&nbsp; Buying
															</span>
															<h4 class="bigger pull-right">{{$buyingRequest}}</h4>
														</div>

														<div class="grid3">
															<span class="grey">
																<i class="ace-icon fa fa-user fa-2x red"></i>
																&nbsp; User
															</span>
															<h4 class="bigger pull-right">{{$allUser}}</h4>
														</div>

														<!-- /section:custom/extra.grid -->
													</div>
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
									</div><!-- /.col -->
								</div><!-- /.row -->

								<!-- #section:custom/extra.hr -->
								<div class="hr hr32 hr-dotted"></div>
								<script src="https://cdn.jsdelivr.net/scrollreveal.js/3.1.4/scrollreveal.min.js"></script>
								<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.min.js"></script>
								<script src="{{php}} echo SOCKET_IO; {{/php}}js/moment.min.js"></script>
								<script src="{{php}} echo SOCKET_IO; {{/php}}socket.io/socket.io.js"></script>
								<script src="{{php}} echo SOCKET_IO; {{/php}}js/admin.manager.js"></script>
								{{php}}
								$user_id = isset($_SESSION['user_login']) ? $_SESSION['user_login']['user_id'] : 0;
								$user_name = isset($_SESSION['user_login']) ? $_SESSION['user_login']['username'] : null;
								$img = BASEURL.$_SESSION['user_login']['image'];
								{{/php}}
								<script type="text/javascript">
									$(document).ready(function () {
										var us = '{{php}} echo $user_id ;{{/php}}';
										var un = '{{php}} echo $user_name ;{{/php}}';
										var img = '{{php}} echo $img ;{{/php}}';
										ChatManagement.init({
											user_id:us,
											name:un,
											img:img
										});
									});
								</script>
<script type="text/javascript">
jQuery(function($) {
		//flot chart resize plugin, somehow manipulates default browser resize event to optimize it!
		//but sometimes it brings up errors with normal resize event handlers
		$.resize.throttleWindow = false;
		
		var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
		var data = [
			{ label: "{{l}}Tổng Số Lượng Sản Phẩm{{/l}}",  data: 28.7, color: "#68BC31"},
			{ label: "{{l}}Sản Phẩm Được Quan Tâm Nhiều{{/l}}",  data: 24.5, color: "#2091CF"},
			{ label: "{{l}}Tổng Thành Viên Miễn Phí{{/l}}",  data: 8.2, color: "#AF4E96"},
			{ label: "{{l}}Tổng Thành Viên Thu Phí{{/l}}",  data: 18.6, color: "#DA5430"},
			{ label: "{{l}}Tổng Số Lượng Yêu Cầu Mua{{/l}}",  data: 10, color: "#FEE074"},
			{ label: "{{l}}Tổng Số Lượng Yêu Cầu Mua{{/l}}",  data: 10, color: "#FE0000"}
		]
		function drawPieChart(placeholder, data, position) {
			  $.plot(placeholder, data, {
				series: {
					pie: {
						show: true,
						tilt:0.8,
						highlight: {
							opacity: 0.25
						},
						stroke: {
							color: '#fff',
							width: 2
						},
						startAngle: 2
					}
				},
				legend: {
					show: true,
					position: position || "ne", 
					labelBoxBorderColor: null,
					margin:[-30,15]
				}
				,
				grid: {
					hoverable: true,
					clickable: true
				}
			 })
		}
		drawPieChart(placeholder, data);
		
		/**
		we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
		so that's not needed actually.
		*/
		placeholder.data('chart', data);
		placeholder.data('draw', drawPieChart);
		
		
		//pie chart tooltip example
		
});
</script>