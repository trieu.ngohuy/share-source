<style>
    body{
        margin-top: -22px;
    }
</style>
<!--Slide-->
<div class="header-carousel" style="padding: 0px">
    <!-- #region Jssor Slider Begin -->
    <!-- Generator: Jssor Slider Maker -->
    <!-- Source: https://www.jssor.com -->

    <script type="text/javascript">
        jssor_1_slider_init = function () {

            var jssor_1_options = {
                $AutoPlay: 1,
                $SlideDuration: 800,
                $SlideEasing: $Jease$.$OutQuint,
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                } else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /*jssor slider loading skin spin css*/
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }

        /*jssor slider bullet skin 032 css*/
        .jssorb032 {position:absolute;} .jssorb032 .i {position:absolute;cursor:pointer;} .jssorb032 .i .b {fill:#fff;fill-opacity:0.7;stroke:#000;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.25;} .jssorb032 .i:hover .b {fill:#000;fill-opacity:.6;stroke:#fff;stroke-opacity:.35;} .jssorb032 .iav .b {fill:#000;fill-opacity:1;stroke:#fff;stroke-opacity:.35;} .jssorb032 .i.idn {opacity:.3;}

        /*jssor slider arrow skin 051 css*/
        .jssora051 {display:block;position:absolute;cursor:pointer;} .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;} .jssora051:hover {opacity:.8;} .jssora051.jssora051dn {opacity:.5;} .jssora051.jssora051ds

        {opacity:.3;pointer-events:none;}
    </style>
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:295px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin"
             style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;"
                 src="{{$LAYOUT_HELPER_URL}}front/images/loading.svg"/>
        </div>
        <div data-u="slides"
             style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:295px;overflow:hidden;">
            {{foreach from=$banners item=item}}
            <div>
                <img data-u="image"
                     src="{{$item.main_image}}"/>
                <div style="position:absolute;top:225px;left:80px;z-index:0;background-color:rgba(255,188,5,0.8);font-size:30px;color:#000000;line-height:38px;padding:5px;box-sizing:border-box;">
                    {{$item.title}}
                </div>
            </div>
            {{/foreach}}
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;"
             data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;"
             data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;"
             data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->
</div>
<!--End-->
<section class="product-solutions">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="headline">{{l}}Sản phẩm đèn LED theo ứng dụng{{/l}}</h3>
            </div>
            {{foreach from=$categories item=item}}
            <div class="col-sm-4 padding-lr-10">
                <div class="single-solution"><a href="{{$item.url}}"> <img
                            alt="{{$item.name}}"
                            src="{{$item.main_image}}">
                        <div class="title">{{$item.name}}</div>
                    </a></div>
            </div>
            {{/foreach}}
        </div>
    </div>
</section>
<section class="featured-project">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="headline">{{l}}Dự án tiêu biểu{{/l}}</h3>
            </div>
            <div class="col-md-12">
                <div class="slider center">
                    {{foreach from=$projects item=item}}
                    <div>
                        <a href="{{$item.url}}" class="single-project-wrap">
                            <div class="single-project"><img alt="{{$item.title}}"
                                                             src="{{$item.main_image}}">
                                <div class="title">{{$item.title}}</div>
                            </div>
                        </a>
                    </div>
                    {{/foreach}}

                </div>
            </div>
        </div>
    </div>
</section>
<section class="intro">
    <div class="container">
    <div class="row">
    <div class="col-md-12 text-center">
    <h3 class="headline">{{$objBottomBannerCat.name}}</h3>
    </div>
    </div>
    </div>
    <!-- #region Jssor Slider Begin -->
    <!-- Generator: Jssor Slider Maker -->
    <!-- Source: https://www.jssor.com -->

    <script type="text/javascript">
        jssor_2_slider_init = function () {

            var jssor_2_options = {
                $AutoPlay: 1,
                $SlideDuration: 800,
                $SlideEasing: $Jease$.$OutQuint,
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                }
            };

            var jssor_2_slider = new $JssorSlider$("jssor_2", jssor_2_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider2() {
                var containerElement = jssor_2_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_2_slider.$ScaleWidth(expectedWidth);
                } else {
                    window.setTimeout(ScaleSlider2, 30);
                }
            }

            ScaleSlider2();

            $Jssor$.$AddEvent(window, "load", ScaleSlider2);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider2);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider2);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /*jssor slider loading skin spin css*/
        .jssor2-009-spin img {
            animation-name: jssor2-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssor2-009-spin {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }

        /*jssor slider bullet skin 032 css*/
        .jssorb032 {position:absolute;} .jssorb032 .i {position:absolute;cursor:pointer;} .jssorb032 .i .b {fill:#fff;fill-opacity:0.7;stroke:#000;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.25;} .jssorb032 .i:hover .b {fill:#000;fill-opacity:.6;stroke:#fff;stroke-opacity:.35;} .jssorb032 .iav .b {fill:#000;fill-opacity:1;stroke:#fff;stroke-opacity:.35;} .jssorb032 .i.idn {opacity:.3;}

        /*jssor slider arrow skin 051 css*/
        .jssora051 {display:block;position:absolute;cursor:pointer;} .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;} .jssora051:hover {opacity:.8;} .jssora051.jssora051dn {opacity:.5;} .jssora051.jssora051ds

        {opacity:.3;pointer-events:none;}
    </style>
    <div id="jssor_2" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:295px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssor2-009-spin"
             style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;"
                 src="{{$LAYOUT_HELPER_URL}}front/images/loading.svg"/>
        </div>
        <div data-u="slides"
             style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:295px;overflow:hidden;">
            {{foreach from=$arrBottomBanner item=item}}
            <div>
                <img data-u="image"
                     src="{{$item.main_image}}"/>
                <div style="position:absolute;top:225px;left:80px;z-index:0;background-color:rgba(255,188,5,0.8);font-size:30px;color:#000000;line-height:38px;padding:5px;box-sizing:border-box;">
                    {{$item.title}}
                </div>
            </div>
            {{/foreach}}
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;"
             data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;"
             data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;"
             data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>
    <script type="text/javascript">jssor_2_slider_init();</script>
    <!-- #endregion Jssor Slider End -->
</section>
<section class="partner">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="headline" style="margin-bottom: 0px">{{l}}Đối tác{{/l}}</h3>
            </div>
            {{foreach from=$listTopSuplier item=item}}
            <div class="col-sm-3"><a href="{{$item.url}}"><img
                        src="{{$item.main_image}}"
                        alt="{{$item.name}}"></a></div>
                    {{/foreach}}

            <div class="col-md-12 text-center"><a href="#" data-toggle="modal" data-target="#modallienhe" class="cta">{{l}}Liên hệ hợp tác{{/l}}</a></div>

        </div>
    </div>
</section>