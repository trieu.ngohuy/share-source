<div class="content-page">
    <div class="container">
        {{foreach from=$allCats item=allCatItem key=allCatKey}}
        <!-- featured category fashion -->
        <div class="category-featured category-featured-{{$allCatItem.product_category_gid}}">
            <nav class="navbar nav-menu show-brand">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-brand" style="background: {{$allCatItem.color}}">
                        <a href="{{$allCatItem.url}}">
                            <i class="fa {{if $allCatItem.faicon == ''}}fa-cart-plus{{else}}{{$allCatItem.faicon}}{{/if}}" style="margin-top: 18px;"></i>
                            {{$allCatItem.name}}
                        </a>
                    </div>
                    <span class="toggle-menu"></span>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a data-toggle="tab" href="#tab-hot-{{$allCatItem.product_category_gid}}">{{l}}Bán Chạy{{/l}}</a></li>
                            <li><a data-toggle="tab" href="#tab-sale-{{$allCatItem.product_category_gid}}">{{l}}Giảm giá{{/l}}</a></li>
                            <li><a data-toggle="tab" href="#tab-special-{{$allCatItem.product_category_gid}}">{{l}}Sản phẩm đặc biệt{{/l}}</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
                <div id="{{$allCatKey}}" class="floor-elevator">
                    <a href="#{{$allCatKey-1}}" class="btn-elevator up disabled fa fa-angle-up"></a>
                    <a href="#{{$allCatKey+1}}" class="btn-elevator down fa fa-angle-down"></a>
                </div>
            </nav>
            <div class="product-featured clearfix">
                <div class="row">
                    <div class="col-sm-3 sub-category-wapper home-cat-left">
                        <ul class="sub-category-list">
                            {{foreach from=$allCatItem.subCat item=subCatItem}}
                            <li><a href="{{$subCatItem.url}}">{{$subCatItem.name}}</a></li>
                                {{/foreach}}
                        </ul>
                        <!--Category images-->
                        {{if $allCatItem.images|@Count > 0}}
                        {{assign var='CatSlide' value=$allCatItem.images}}
                        {{sticker name=category_slide}}
                        {{/if}}
                    </div>
                    <div class="col-sm-9 col-right-tab home-cat-right">
                        <div class="product-featured-tab-content">
                            <div class="tab-container">
                                <!--Hot product-->
                                <div class="tab-panel active" id="tab-hot-{{$allCatItem.product_category_gid}}">

                                    <div class="box-right">
                                        <ul class="product-list row">
                                            {{assign var='count' value='1'}}
                                            {{foreach from=$allCatItem.products item=productItem}}
                                            
                                            {{if $allCatItem.numProduct == -1}}
                                            
                                                {{if $productItem.tinnhanh == 1}}
                                                <li class="col-sm-3">
                                                    {{sticker name=hproduct_item}}
                                                </li>
                                                {{/if}}
                                            
                                            {{else}}
                                            
                                                
                                                {{if $productItem.tinnhanh == 1 && $count <= $allCatItem.numProduct}}
                                                <li class="col-sm-3">
                                                    {{sticker name=hproduct_item}}
                                                </li>
                                                {{assign var='count' value=$count+1}}
                                                {{/if}}
                                            
                                            {{/if}}
                                            
                                            {{/foreach}}
                                        </ul>
                                    </div>
                                </div>
                                <!--Sale product-->
                                <div class="tab-panel" id="tab-sale-{{$allCatItem.product_category_gid}}">
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            {{assign var='count' value='1'}}
                                            {{foreach from=$allCatItem.products item=productItem}}
                                            {{if $allCatItem.numProduct == -1}}
                                            
                                                {{if $productItem.sale == 1}}
                                                <li class="col-sm-3">
                                                    {{sticker name=hproduct_item}}
                                                </li>
                                                {{/if}}
                                            
                                            {{else}}
                                            
                                                
                                                {{if $productItem.sale == 1 && $count <= $allCatItem.numProduct}}
                                                <li class="col-sm-3">
                                                    {{sticker name=hproduct_item}}
                                                </li>
                                                {{assign var='count' value=$count+1}}
                                                {{/if}}
                                            
                                            {{/if}}
                                            {{/foreach}}
                                        </ul>
                                    </div>
                                </div>
                                <!--Special product-->
                                <div class="tab-panel" id="tab-special-{{$allCatItem.product_category_gid}}">
                                    <div class="box-right">
                                        <ul class="product-list row">
                                            {{assign var='count' value='1'}}
                                            {{foreach from=$allCatItem.products item=productItem}}
                                            {{if $allCatItem.numProduct == -1}}
                                            
                                                {{if $productItem.dacbiet == 1}}
                                                <li class="col-sm-3">
                                                    {{sticker name=hproduct_item}}
                                                </li>
                                                {{/if}}
                                            
                                            {{else}}
                                            
                                                
                                                {{if $productItem.dacbiet == 1 && $count <= $allCatItem.numProduct}}
                                                <li class="col-sm-3">
                                                    {{sticker name=hproduct_item}}
                                                </li>
                                                {{assign var='count' value=$count+1}}
                                                {{/if}}
                                            
                                            {{/if}}
                                            {{/foreach}}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{sticker name=home_slide_style}}
        </div>
        <!-- end featured category fashion -->
        {{/foreach}}
    </div>
</div>

<div id="content-wrap">
    <div class="container">
        <div class="band-logo owl-carousel"  data-dots="false" data-loop="true" data-nav = "true" data-margin = "1.2" data-autoplay="true" data-responsive='{"0":{"items":3},"600":{"items":5},"1000":{"items":8}}'>
            {{foreach from=$listTopSuplier item=suplierItem}}
            <a href="#"><img src="{{$suplierItem.main_image}}" alt="{{$suplierItem.name}}"></a>            
                {{/foreach}}
        </div>
        <!-- blog list -->
        <div class="blog-list">
            <h2 class="page-heading">
                <span class="page-heading-title">{{l}}Top nhà cung cấp{{/l}}</span>
            </h2>
            <div class="blog-list-wapper">
                <ul class="owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" 
                    data-responsive='{"0":{"items":1},"600":{"items":4},"1000":{"items":5},"1500":{"items":6},"2000":{"items":7}}'>
                    {{foreach from=$listSuplier item=listSuperlierItem}}
                    <li>
                        <div class="post-thumb image-hover2">
                            <a href="{{$listSuperlierItem.url}}">
                                <img src="{{$listSuperlierItem.main_image}}" alt="{{$listSuperlierItem.name}}"></a>
                        </div>
                    </li>
                    {{/foreach}}
                </ul>
            </div>
        </div>
        <!-- ./blog list -->
    </div> <!-- /.container -->
</div>