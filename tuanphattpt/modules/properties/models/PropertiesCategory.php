<?php

require_once 'Nine/Model.php';
class Models_PropertiesCategory extends Nine_Model
{ 
    /**
     * The primary key column or columns.
     * A compound key should be declared as an array.
     * You may declare a single-column primary key
     * as a string.
     *
     * @var mixed
     */
    protected $_primary = 'properties_category_id';
     /**
     * The field name what we use to group all language rows together
     * 
     * @var string
     */
    protected $_groupField = 'properties_category_gid';
    /**
     * Let system know this is miltilingual table or not.
     * If this table has multilingual fields, Zend_Db_Table_Select object
     * will be inserted language condition automatically.
     * 
     * @var array
     */
    protected $_multilingualFields = array('name', 'enabled', 'alias', 'description');
    
    /**
     * Constructor.
     *
     * Supported params for $config are:
     * - db              = user-supplied instance of database connector,
     *                     or key name of registry instance.
     * - name            = table name.
     * - primary         = string or array of primary key(s).
     * - rowClass        = row class name.
     * - rowsetClass     = rowset class name.
     * - referenceMap    = array structure to declare relationship
     *                     to parent tables.
     * - dependentTables = array of child tables.
     * - metadataCache   = cache for information from adapter describeTable().
     *
     * @param  mixed $config Array of user-specified config options, or just the Db Adapter.
     * @return void
     */
    public function __construct($config = array())
    {
        $this->_name = $this->_prefix . 'properties_category';
        return parent::__construct($config); 
    }

    /**
     * Get all categories with conditions
     * 
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
    public function getAllCategories($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_name))
                ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.properties_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
        
        if (null != @$condition['keyword']) {
            $sql = "SELECT properties_category_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('name LIKE ?', "%{$condition['keyword']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['properties_category_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('cc.properties_category_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['parent_id']) {
        	
        	$sql = "SELECT gid_string FROM {$this->_prefix}" . 'properties_category' . " WHERE " . $this->getAdapter()->quoteInto('properties_category_gid = ? ', $condition['parent_id']);
        	$ids = @reset($this->_db->fetchAll($sql));
        	if (null == $ids) {
        	    $ids = '0';
        	}
            /**
             * Add to select object
             */
            $select->where('cc.parent_id IN (' . trim($ids['gid_string'], ',') .')');
        }

        if (null != @$condition['properties_category_gid']) {
            $select->where('cc.properties_category_gid=?', $condition['properties_category_gid']);
        }
        
        return $this->fetchAll($select)->toArray();
    }
    public function getAllCategoriesParentNull()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_name))
                ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.properties_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
				->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("sorting");
        /**
         * Conditions
         */
        
    	 $select->where('cc.parent_id IS NULL');
        
        return $this->fetchAll($select)->toArray();
    }
    public function getAllCategoriesByGid($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_name))
                ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.properties_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
				->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("sorting");
        /**
         * Conditions
         */
         	$sql = "SELECT * FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('properties_category_gid = ?', $gid);
            
         	$ids = $this->_db->fetchAll($sql);
         	
            $select->where('cc.properties_category_gid IN (' . trim($ids['gid_string'] , ',') .')');
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllCategoriesOnparentkey($key)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_name))
                ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.properties_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ;
        /**
         * Conditions
         */
            $select->where('cc.parent_id=?', $key);
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllCategoriesOnparentkeyUser($key)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_name))
                ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.properties_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId());
        /**
         * Conditions
         */
                
            $sql = "SELECT * FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('properties_category_gid = ?', $key);
            
         	$ids = current($this->_db->fetchAll($sql));
         	
            $select->where('cc.properties_category_gid IN (' . trim($ids['gid_string'] , ',') .')');
            
        
        return $this->fetchAll($select)->toArray();
    }
    public function getAllEnabledCategory()
    {
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1 AND parent_genabled = 1');
                    
        
		return $this->fetchAll($select)->toArray();       
    }
    
    
    
	public function increaseSorting($startPos = 1, $num = 1)
    {
        $sql = "UPDATE {$this->_name} SET sorting = sorting + " . intval($num) . " WHERE sorting >= " . intval($startPos);
        
        $this->_db->query($sql);
    }
    
    
    public function updateGidString($gid, $gidString) 
    {
    	/**
    	 * Get current update node
    	 */
    	if (null == $gid) {
    		return;
    	}
    	else {
    		$category = @reset($this->getByColumns(array('properties_category_gid = ?'	=>	$gid))->toArray());
    		$this->update(array('gid_string'	=> $this->concatGidString($category['gid_string'],$gidString)),array('properties_category_gid = ?' => $gid));
    		return $this->updateGidString($category['parent_id'],$gidString);
    	}
    }
    
    public function deleteGidString($gid, $gidString)
    {
    	/**
    	 * Get current update node
    	 */
    	if (null == $gid){
    		return;
    	}
    	else {
    		$category = @reset($this->getByColumns(array('properties_category_gid = ?'	=>	$gid))->toArray());
    		$this->update(array('gid_string' => $this->removeGidString($category['gid_string'],$gidString)), array('properties_category_gid = ?' => $gid));
    		return $this->deleteGidString($category['parent_id'], $gidString);
    	}
    }
    
    
    public function concatGidString($oldGidStr, $concatGidStr) 
    {
    	if(null == $oldGidStr){
    		return $concatGidStr;
    	}
    	
    	$oldGidString = explode(',', trim($oldGidStr,','));
    	$concatGidString = explode(',', trim($concatGidStr,','));
    	
    	foreach($concatGidString as $item){
    		if (false == in_array($item, $oldGidString)){
    			$oldGidString[] = $item;
    		}
    	}
    	return implode(',', $oldGidString);
    }
    
    private function removeGidString($oldGidStr, $removedGidStr)
    {
    	if(null == $oldGidStr){
    		return null;
    	}
    	
    	$oldGidString = explode(',', trim($oldGidStr,','));
    	$removedGidString = explode(',', trim($removedGidStr,','));
    	
//    	echo "<pre>";print_r($removedGidString);die; 
    	foreach($oldGidString as $key => $item){
    		if (false != in_array($item, $removedGidString)){
    			unset($oldGidString[$key]);
    		}
    	}
//    	echo "<pre>";print_r($oldGidString);die; 
    	return implode(',', $oldGidString);
    }

}