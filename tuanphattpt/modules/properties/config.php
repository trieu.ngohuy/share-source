<?php
return array (
    'permissionRules' => array(
                        'see_properties' => array(//External permission
                                            'See all articles',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission interface
                        'new_properties' => array(//External permission
                                            'Create new article',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
                        'edit_properties' => array(//External permission
                                            'Edit existed articles',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
						
						'genabled_properties' => 'Genabled existed articles',
                        'delete_properties' => 'Delete existed articles',
  
));
