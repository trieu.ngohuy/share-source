<?php

class Route_Product {

    /**
     * Build friendly URL
     * @param array $linkArr 0 => <module>, 1 => controller, 2 => action, 3...n param/value
     * @param array $params param => value
     *
     * @return string friendly URL
     */
    public function build($linkArr, $params = array()) {

        $result = implode('/', $linkArr);
        if ('index' == @$linkArr[2]) {
            /*
             * Check current language
             */
            if (Nine_Language::getCurrentLangCode() == 'vi') {
                $result = "danh-muc/";
            } else {
                $result = "category/";
            }

            if (null != @$params['alias']) {
                $result .= urlencode($params['alias']);
            } else {
                require_once 'modules/product/models/ProductCategory.php';
                $objProduct = new Models_ProductCategory();

                $pro = @reset($objProduct->getByColumns(array('product_category_gid' => $linkArr[4]))->toArray()); //getProductCategoryByGid($linkArr[4]);
                $result .= urlencode($pro['alias']);
            }
        } else if ('detail' == @$linkArr[2]) {
            /**
             * Category
             */
            //$result = "detail/{$linkArr[4]}/";
            if (Nine_Language::getCurrentLangCode() == 'vi') {
                $result = "san-pham/";
            }
            else{
                $result = "detail/";
            }
            
            if (null != @$params['alias']) {
                $result .= urlencode($params['alias']);
            }
        }
        return $result;
    }

    /**
     * Parse friendly URL
     */
    public function parse() {
        $router = Nine_Controller_Front::getInstance()->getRouter();
        /*
         * Category index rout (en)
         */
        $categoryDetail = new Zend_Controller_Router_Route_Regex(
                'category/(.*)', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'category-detail'
                ), array(
            1 => 'alias',
                )
        );
        $router->addRoute('categoryDetail', $categoryDetail);
        
        /*
         * Category index rout (vi)
         */
        $categoryDetailVi = new Zend_Controller_Router_Route_Regex(
                'danh-muc/(.*)', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'category-detail'
                ), array(
            1 => 'alias',
                )
        );
        $router->addRoute('categoryDetailVi', $categoryDetailVi);


        $route1 = new Zend_Controller_Router_Route_Regex(
                'search-product/(.*)', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'search'
                ), array(1 => 'key')
        );
        $router->addRoute('product1', $route1);
        
        /*
         * Product detail route (en)
         */
        $productDetailEn = new Zend_Controller_Router_Route_Regex(
                'detail/(.*)', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'product-detail'
                ), array(1 => 'alias')
        );
        $router->addRoute('productDetailEn', $productDetailEn);
        /*
         * Product detail route (vi)
         */
        $productDetailVi = new Zend_Controller_Router_Route_Regex(
                'san-pham/(.*)', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'product-detail'
                ), array(1 => 'alias')
        );
        $router->addRoute('productDetailVi', $productDetailVi);
        
        
        $route3 = new Zend_Controller_Router_Route_Regex(
                'search.html', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'search'
                )
        );
        $router->addRoute('product3', $route3);

        $route4 = new Zend_Controller_Router_Route_Regex(
                'estore/manage-product/(.*).html', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'manage-product'
                ), array(
            1 => 'alias'
                )
        );
        $router->addRoute('product4', $route4);

        $route5 = new Zend_Controller_Router_Route_Regex(
                'estore/new-product/(.*).html', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'new-product'
                ), array(
            1 => 'alias'
                )
        );
        $router->addRoute('product5', $route5);

        $route6 = new Zend_Controller_Router_Route_Regex(
                'estore/edit-product/(.*)/([0-9]+)', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'edit-product'
                ), array(
            1 => 'alias',
            2 => 'gid'
                )
        );
        $router->addRoute('product6', $route6);

        $route7 = new Zend_Controller_Router_Route_Regex(
                'estore/edit-product/(.*)/([0-9]+)/([0-9]+)', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'edit-product'
                ), array(
            1 => 'alias',
            2 => 'gid',
            3 => 'lid'
                )
        );
        $router->addRoute('product7', $route7);

        $route8 = new Zend_Controller_Router_Route_Regex(
                'estore/delete-product/(.*)/(.*)', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'delete-product'
                ), array(
            1 => 'alias',
            2 => 'gid',
            3 => 'lid'
                )
        );
        $router->addRoute('product8', $route8);

        $route9 = new Zend_Controller_Router_Route_Regex(
                'estore/enable-hot-product/([0-9]+)/(.*).html', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'tinnhanh-product'
                ), array(
            1 => 'gid',
            2 => 'alias',
                )
        );
        $router->addRoute('product9', $route9);

        $route10 = new Zend_Controller_Router_Route_Regex(
                'estore/disable-hot-product/([0-9]+)/(.*).html', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'distinnhanh-product'
                ), array(
            1 => 'gid',
            2 => 'alias',
                )
        );
        $router->addRoute('product10', $route10);

        /* Category */

        $route11 = new Zend_Controller_Router_Route_Regex(
                'estore/manage-category/(.*).html', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'manage-category'
                ), array(
            1 => 'alias'
                )
        );
        $router->addRoute('product11', $route11);

        $route12 = new Zend_Controller_Router_Route_Regex(
                'estore/edit-category/(.*)/([0-9]+)', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'edit-category'
                ), array(
            1 => 'alias',
            2 => 'gid'
                )
        );
        $router->addRoute('product12', $route12);

        $route13 = new Zend_Controller_Router_Route_Regex(
                'estore/edit-category/(.*)/([0-9]+)/([0-9]+)', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'edit-category'
                ), array(
            1 => 'alias',
            2 => 'gid',
            3 => 'lid'
                )
        );
        $router->addRoute('product13', $route13);

        $route14 = new Zend_Controller_Router_Route_Regex(
                'estore/new-category/(.*).html', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'new-category'
                ), array(
            1 => 'alias',
                )
        );
        $router->addRoute('product14', $route14);

        $route15 = new Zend_Controller_Router_Route_Regex(
                'estore/disable-category/(.*)/(.*)', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'disable-category'
                ), array(
            1 => 'alias',
            2 => 'gid'
                )
        );
        $router->addRoute('product15', $route15);
        $route16 = new Zend_Controller_Router_Route_Regex(
                'estore/disable-category/(.*)/([0-9]+)/([0-9]+)', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'disable-category'
                ), array(
            1 => 'alias',
            2 => 'gid',
            3 => 'lid'
                )
        );
        $router->addRoute('product16', $route16);

        $route17 = new Zend_Controller_Router_Route_Regex(
                'estore/enable-category/(.*)/(.*)', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'enable-category'
                ), array(
            1 => 'alias',
            2 => 'gid'
                )
        );
        $router->addRoute('product17', $route17);

        $route18 = new Zend_Controller_Router_Route_Regex(
                'estore/enable-category/(.*)/([0-9]+)/([0-9]+)', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'enable-category'
                ), array(
            1 => 'alias',
            2 => 'gid',
            3 => 'lid'
                )
        );
        $router->addRoute('product18', $route18);

        $route19 = new Zend_Controller_Router_Route_Regex(
                'estore/delete-category/(.*)/(.*)', array(
            'module' => 'product',
            'controller' => 'estore',
            'action' => 'delete-category'
                ), array(
            1 => 'alias',
            2 => 'gid',
                )
        );
        $router->addRoute('product19', $route19);


        $route21 = new Zend_Controller_Router_Route_Regex(
                'category/load-seller/(.*)', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'load-seller'
                ), array(
            1 => 'product_category_gid'
                )
        );
        $router->addRoute('product21', $route21);

        $route22 = new Zend_Controller_Router_Route_Regex(
                'category/load-product-category/(.*)', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'load-product-category'
                ), array(
            1 => 'product_category_gid'
                )
        );
        $router->addRoute('product22', $route22);


        $route23 = new Zend_Controller_Router_Route_Regex(
                'product/(.*)', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'product-detail'
                ), array(
            1 => 'alias',
                )
        );
        $router->addRoute('product23', $route23);

        $route24 = new Zend_Controller_Router_Route_Regex(
                '(.*)/product/(.*)', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'product-detail'
                ), array(
            1 => 'alias-estore',
            2 => 'alias-product'
                )
        );
        $router->addRoute('product24', $route24);

        $route25 = new Zend_Controller_Router_Route_Regex(
                '(.*)/category/(.*)', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'category-detail'
                ), array(
            1 => 'alias-estore',
            2 => 'alias-category'
                )
        );
        $router->addRoute('product25', $route25);

        $route26 = new Zend_Controller_Router_Route_Regex(
                'product/search', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'search'
                )
        );
        $router->addRoute('product26', $route26);
        $routesearch = new Zend_Controller_Router_Route_Regex(
                'product/search-product', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'search'
                )
        );
        $router->addRoute('routesearch', $routesearch);
        $route27 = new Zend_Controller_Router_Route_Regex(
                '(.*)/product/search', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'search'
                ), array(
            1 => 'alias-estore',
                )
        );
        $router->addRoute('product27', $route27);

        $route28 = new Zend_Controller_Router_Route_Regex(
                'tpp/(.*)', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'product-tpp-detail'
                ), array(
            1 => 'alias',
                )
        );
        $router->addRoute('product28', $route28);
        $route29 = new Zend_Controller_Router_Route_Regex(
                'category/load-product-category-getids/(.*)', array(
            'module' => 'product',
            'controller' => 'index',
            'action' => 'load-product-category-getids'
                ), array(
            1 => 'product_category_gid'
                )
        );
        $router->addRoute('product29', $route29);
    }

}
