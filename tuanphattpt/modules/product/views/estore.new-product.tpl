<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<script type="text/javascript">
    //<![CDATA[

    jQuery(document).ready(function (){
        CKFinder.setupCKEditor( null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/' );
        jQuery( "#images" ).sortable();
        jQuery( "#images" ).disableSelection();
        //Make slug
        {{foreach from=$allLangs item=item name=langDiv}}
        jQuery('#title{{$smarty.foreach.langDiv.iteration}}').makeSlug({
            slug: jQuery('#alias{{$smarty.foreach.langDiv.iteration}}')
        });
        {{/foreach}}

    });
    var imgId;
    function chooseImage(id)
    {
        imgId = id;
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();
        finder.basePath = '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/'; // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.selectActionFunction = setFileField;
        finder.popup();
    }
    // This is a sample function which is called when a file is selected in CKFinder.
    function setFileField( fileUrl )
    {
        document.getElementById( 'chooseImage_img' + imgId ).src = fileUrl;
        document.getElementById( 'chooseImage_input' + imgId).value = fileUrl;
        document.getElementById( 'chooseImage_div' + imgId).style.display = '';
        document.getElementById( 'chooseImage_noImage_div' + imgId ).style.display = 'none';
    }
    function clearImage(imgId)
    {
        document.getElementById( 'chooseImage_img' + imgId ).src = '';
        document.getElementById( 'chooseImage_input' + imgId ).value = '';
        document.getElementById( 'chooseImage_div' + imgId).style.display = 'none';
        document.getElementById( 'chooseImage_noImage_div' + imgId).style.display = '';
    }

    function addMoreImg()
    {
        jQuery("ul#images > li.hidden").filter(":first").removeClass('hidden');
    }

    //]]>
</script>
<style type="text/css">
    #images { list-style-type: none; margin: 0; padding: 0;}
    #images li { margin: 10px; float: left; text-align: center;  height: 180px;}
</style>




<div class="page-header">
    <h1>
        {{l}}Product{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}New Product{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">
                {{if $errors|@count > 0}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{if $errors.main}}
                    </strong> {{$errors.main}}
                    {{else}}
                    {{l}}Please check following information again{{/l}}
                    {{/if}}
                </div>
                {{/if}}


                <div class="col-sm-12 widget-container-col ui-sortable">

                    <div class="tabbable tabs-left">

                        <ul class="nav nav-tabs" id="myTab2">
                            <li class="active">
                                <a href="#tab0" data-toggle="tab" aria-expanded="true">
                                    <i class="pink ace-icon fa fa-home bigger-110"></i>
                                    {{l}}Basic{{/l}}
                                </a>
                            </li>

                            {{foreach from=$allLangs item=item index=index name=langTab}}
                            <li>
                                <a href="#tab{{$item.lang_id}}" data-toggle="tab" aria-expanded="true">
                                    <image style="vertical-align:middle;" src="{{$BASE_URL}}{{$item.lang_image}}">{{$item.name}}
                                </a>
                            </li>
                            {{/foreach}}
                        </ul>

                        <form action="" method="post" class="form-horizontal">
                            <div class="tab-content">

                                <div id="tab0" class="tab-pane active">

                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Estore Category{{/l}}</label>
                                        <div class="col-md-10">
                                            <select name="data[estore_category_gid]" class="chosen-select form-control" id="form-field-select-3">
                                                {{foreach from=$allCatEstores item=item}}
                                                <option value="{{$item.estore_category_gid}}" {{if $data.estore_category_gid == $item.estore_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
                                                {{/foreach}}
                                            </select>
                                        </div>
                                    </div>
														<div class="form-group">
															<label class="control-label col-md-2">{{l}}Type Product{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[type]"  value="{{$data.type}}" >
															</div>
														</div>
														<br class="cb" />
														<div class="form-group">
															<label class="control-label col-md-2">{{l}}Sale Persent / Price sale{{/l}}</label>
															<div class="col-md-10">
																	<div class="col-md-6">
																		<input type="text" class="form-control" name="data[pt_sale]"  value="{{$data.pt_sale}}" >
																	</div>
																	<div class="col-md-6">
																		<input type="text" class="form-control" name="data[price_sale]"  value="{{$data.price_sale}}" >
																	</div>
															</div>
														</div>
														<br class="cb" />
														<div class="form-group">
															<label class="control-label col-md-2">{{l}}Price{{/l}}</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="data[price_sale]"  value="{{$data.price_sale}}" >
															</div>
															<div class="col-md-3">
																<select name="data[unit]" class="chosen-select form-control">
																	{{foreach from=$unitProperties item=item}}
																		{{if $item.parent_id != ''}}
																			<option value="{{$item.properties_category_gid}}" {{if $data.unit == $item.properties_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
																		{{/if}}
																	{{/foreach}}
																</select>
															</div>
															
															<div class="col-md-3">
																	<select name="data[so_luong_gid]" class="chosen-select form-control"  onchange="getDVT();" id="dvt">
																		{{foreach from=$dvtProperties item=item}}
																			{{if $item.parent_id != ''}}
									                                        	<option value="{{$item.properties_category_gid}}" {{if $item.properties_category_gid == $data.so_luong }} selected="selected"{{/if}}>{{$item.name}}</option>
									                                    	{{/if}}
									                                    {{/foreach}}
																	</select>
															</div>
														</div>
														<br class="cb" />
														<div class="form-group has-info">
																<label class="control-label col-md-2" for="duallist">{{l}}Màu Sắc{{/l}}</label>
																
																<div class="col-md-10">
																		<select multiple="multiple" size="10" name="data[color][]" id="duallist">
																		
																			{{foreach from=$colorProperties item=item key=key}}
																				{{if $item.parent_id != ''}}
																					<option value="{{$item.properties_category_gid}}" {{if $item.properties_category_gid|in_array:$data.color }} selected="selected"{{/if}}>{{$item.name}}</option>
																				{{/if}}
																			{{/foreach}}
																		</select>
							
																		<!-- /section:plugins/input.duallist -->
																		<div class="hr hr-16 hr-dotted"></div>
																</div>
														</div>
														<br class="cb" />
														
														<div class="form-group has-info">
																<label class="control-label col-md-2" for="duallist">{{l}}Điều Khoản Thanh Toán{{/l}}</label>
																
																<div class="col-md-10">
																		<select multiple="multiple" size="10" name="data[thanhtoan][]" id="duallist">
																		
																			{{foreach from=$thanhtoanProperties item=item key=key}}
																				{{if $item.parent_id != ''}}
																					<option value="{{$item.properties_category_gid}}" {{if $item.properties_category_gid|in_array:$data.thanhtoan }} selected="selected"{{/if}}>{{$item.name}}</option>
																				{{/if}}
																			{{/foreach}}
																		</select>
							
																		<!-- /section:plugins/input.duallist -->
																		<div class="hr hr-16 hr-dotted"></div>
																</div>
														</div>
														
														
														<div class="form-group">
															<label class="control-label col-md-2">{{l}}Hãng Sản Xuất{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[hsx]"  value="{{$data.hsx}}" >
															</div>
														</div>

														<div class="form-group has-info"  style="display: none;">
															<label class="control-label col-md-2">{{l}}Special{{/l}}</label>
															<div class="col-md-10">
																<label class="radio-inline">
																	<input type="radio" name="data[dacbiet]" value="1" /> Yes
																</label>
																<label class="radio-inline">
																	<input type="radio" name="data[dacbiet]" value="0" checked="checked"/> No
																</label>
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-md-2">{{l}}Model{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[model]"  value="{{$data.model}}" >
															</div>
														</div>
														<div class="form-group">
																<label class="control-label col-md-2">{{l}}Xuất Xứ{{/l}}</label>
																<div class="col-md-10">
																		<select name="data[xuat_xu]" class="chosen-select form-control" id="form-field-select-3">
																			{{foreach from=$allCountry item=item}}
										                                        <option value="{{$item.country_category_gid}}" {{if $data.xuat_xu == $item.country_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
										                                    {{/foreach}}
																		</select>
																</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-2">{{l}}Trọng Lượng{{/l}}</label>
															<div class="col-md-5">
																	<input type="text" class="form-control" name="data[trong_luong]"  value="{{$data.trong_luong}}" >
															</div>
															<div class="col-md-5">
																	<select name="data[trong_luong_gid]" class="chosen-select form-control">
																		{{foreach from=$trongluongProperties item=item}}
																			{{if $item.parent_id != ''}}
									                                        	<option value="{{$item.properties_category_gid}}" {{if $item.properties_category_gid == $data.trong_luong_gid }} selected="selected"{{/if}}>{{$item.name}}</option>
									                                    	{{/if}}
									                                    {{/foreach}}
																	</select>
															</div>
														</div>
														
														<div class="form-group">
															<label class="control-label col-md-2">{{l}}Số Lượng Tối Thiểu{{/l}}</label>
															<div class="col-md-5">
																	<input type="text" class="form-control" name="data[so_luong]"  value="{{$data.so_luong}}" >
															</div>
															<div class="col-md-5">
																	<select class="chosen-select form-control" id="dvt_load" disabled="disabled">
																		{{foreach from=$dvtProperties item=item}}
																			{{if $item.parent_id != ''}}
									                                        	<option value="{{$item.properties_category_gid}}" {{if $item.properties_category_gid == $data.so_luong }} selected="selected"{{/if}}>{{$item.name}}</option>
									                                    	{{/if}}
									                                    {{/foreach}}
																	</select>
															</div>
															
														</div>
														<script type="text/javascript">
															function getDVT(){
																var a = $('#dvt').val();
																$('#dvt_load').val(a).trigger('chosen:updated');
															}
														</script>
														
														<div class="form-group">
															<label class="control-label col-md-2">{{l}}HS Quốc Tế{{/l}}</label>
															<div class="col-md-10">
																	<select name="data[hs_qt]" class="chosen-select form-control">
																		{{foreach from=$allHsQt item=item}}
																			{{if $item.parent_id != ''}}
									                                        	<option value="{{$item.hs_category_gid}}" {{if $data.hs_qt == $item.hs_category_gid}}selected="selected"{{/if}}>{{$item.name}} -- {{$item.description}}</option>
									                                    	{{/if}}
									                                    {{/foreach}}
																	</select>
															</div>
														</div>
						                                
						                                <div class="form-group">
															<label class="control-label col-md-2">{{l}}HS Việt Nam{{/l}}</label>
															<div class="col-md-10">
																	<select name="data[hs_vn]" class="chosen-select form-control">
																		{{foreach from=$allHsVn item=item}}
																			{{if $item.parent_id != ''}}
									                                        	<option value="{{$item.hs_category_gid}}" {{if $data.hs_vn == $item.hs_category_gid}}selected="selected"{{/if}}>{{$item.name}} -- {{$item.description}}</option>
									                                    	{{/if}}
									                                    {{/foreach}}
																	</select>
															</div>
														</div>
														
														
														 <div class="form-group">
															<label class="control-label col-md-2">{{l}}Tiêu Chuẩn VN{{/l}}</label>
															<div class="col-md-10">
																	<textarea style="float:left;width: 100%" class="text-input textarea"  name="data[tc_vn]" rows="10">{{$data.tc_vn}}</textarea>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-2">{{l}}Tiêu Chuẩn QT{{/l}}</label>
															<div class="col-md-10">
																	<textarea style="float:left;width: 100%" class="text-input textarea"  name="data[tc_qt]" rows="10" >{{$data.tc_qt}}</textarea>
															</div>
														</div>
														
														<div class="form-group">
															<label class="control-label col-md-2">{{l}}Mã Hải Cảng{{/l}}</label>
															<div class="col-md-10">
																	<input type="text" class="form-control" name="data[mhc]"  value="{{$data.mhc}}" >
															</div>
														</div>
														
														
														<div class="form-group">
															<label class="control-label col-md-2">{{l}}Supply Ability{{/l}}</label>
															<div class="col-md-3">
																<input type="text" class="form-control" name="data[supply_ability]"  value="{{$data.supply_ability}}" >
															</div>
															<div class="col-md-4">
																<select name="data[supply_case]" class="chosen-select form-control">
																	{{foreach from=$caseProperties item=item}}
																		{{if $item.parent_id != ''}}
																			<option value="{{$item.properties_category_gid}}" {{if $data.supply_case == $item.properties_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
																		{{/if}}
																	{{/foreach}}
																</select>
															</div>
															<div class="col-md-3">
																<select name="data[supply_tine]" class="chosen-select form-control">
																	{{foreach from=$timeProperties item=item}}
																		{{if $item.parent_id != ''}}
																			<option value="{{$item.properties_category_gid}}" {{if $data.supply_tine == $item.properties_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
																		{{/if}}
																	{{/foreach}}
																</select>
															</div>
															
														</div>
														
														<div class="form-group" style="display: none;">
															<label class="control-label col-md-2">{{l}}Hình Thức Đóng Gói{{/l}}</label>
															<div class="col-md-10">
																	<select name="data[donggoi]" class="chosen-select form-control">
																		{{foreach from=$donggoiProperties item=item}}
																			{{if $item.parent_id != ''}}
									                                        	<option value="{{$item.properties_category_gid}}" {{if $item.properties_category_gid == $data.donggoi }} selected="selected"{{/if}}>{{$item.name}}</option>
									                                    	{{/if}}
									                                    {{/foreach}}
																	</select>
															</div>
														</div>
														
														<!--div class="form-group">
					                                        <label class="control-label col-md-2">{{l}}Icon:{{/l}}</label>
					                                        <div class="col-md-10">
					                                            <ul id="images">
					                                                <li>
					                                                    <input type="hidden" id="chooseImage_input0icon" name="data[icon]">
					                                                    <div id="chooseImage_div0icon" style="display: none;">
					                                                        <img src="" id="chooseImage_img0icon" style="max-width: 150px; max-height:150px; border:dashed thin;">
					                                                    </div>
					                                                    <div id="chooseImage_noImage_div0icon" style="width: 150px; border: thin dashed; text-align: center; padding:70px 0px;">
					                                                        No image
					                                                    </div>
					                                                    <br/>
					                                                    <a href="javascript:chooseImage('0icon');">{{l}}Choose image{{/l}}</a>
					                                                    |
					                                                    <a href="javascript:clearImage('0icon');">{{l}}Delete{{/l}}</a>
					                                                </li>
					                                            </ul>
					                                        </div>
					                                    </div-->


                                    <footer class="panel-footer">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">{{l}}Images:{{/l}}</label>
                                            <div class="col-md-10">
                                                <p><a href="javascript:addMoreImg()">{{l}}+ Add more images{{/l}}</a></p>
                                                <ul id="images">
                                                    {{section name=images loop=50}}
                                                    {{assign var="i" value=$smarty.section.images.iteration}}
                                                    <li {{if $i >2 && $i > $data.images|@count}}class="hidden"{{/if}}>
                                                        <input type="hidden" id="chooseImage_input{{$i}}" name="data[images][]">
                                                        <div id="chooseImage_div{{$i}}" style="display: none;">
                                                            <img src="" id="chooseImage_img{{$i}}" style="max-width: 150px; max-height:150px; border:dashed thin;"></img>
                                                        </div>
                                                        <div id="chooseImage_noImage_div{{$i}}" style="width: 150px; border: thin dashed; text-align: center; padding:70px 0px;">
                                                            No image
                                                        </div>
                                                        <br/>
                                                        <a href="javascript:chooseImage({{$i}});">{{l}}Choose image{{/l}}</a>
                                                        |
                                                        <a href="javascript:clearImage({{$i}});">{{l}}Delete{{/l}}</a>
                                                    </li>
                                                    {{/section}}
                                                </ul>
                                            </div>
                                        </div>


                                    </footer>

														<div class="clearfix form-actions">
															<div class="col-md-offset-3 col-md-9">
																<button class="btn btn-info" type="submit">
																	<i class="ace-icon fa fa-check bigger-110"></i>
																	{{l}}Save{{/l}}
																</button>
															</div>
														</div>

                                </div>


                                {{foreach from=$allLangs item=item name=langDiv}}
                                <div class="tab-pane" id="tab{{$item.lang_id}}">

                                    <div class="form-group">
																<label class="control-label col-md-2">{{l}}Title{{/l}}</label>
																<div class="col-md-10">
																		<input id="title{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][title]"  value="{{$data[$item.lang_id].title}}" >
																</div>
															</div>
															
															<div class="form-group">
																<label class="control-label col-md-2">{{l}}Alias{{/l}}</label>
																<div class="col-md-10">
																		<input id="alias{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][alias]"  value="{{$data[$item.lang_id].alias}}" >.html
																</div>
															</div>
															
															<div class="form-group">
																<label class="control-label col-md-2">{{l}}Tag{{/l}}</label>
																<div class="col-md-10">
																		<input type="text" class="form-control" name="data[{{$item.lang_id}}][tag]"  value="{{$data[$item.lang_id].tag}}" >
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-md-2">{{l}}Thời Gian Giao Hàng{{/l}}</label>
																<div class="col-md-10">
																		<input type="text" class="form-control" name="data[{{$item.lang_id}}][time_gh]"  value="{{$data[$item.lang_id].time_gh}}" >
																</div>
															</div>
															<div class="form-group">
																	<label class="control-label col-md-2">{{l}}Cách Thức Đóng Gói{{/l}}</label>
																	<div class="col-md-10">
																			<textarea style="float:left;width: 100%" class="text-input textarea"  name="data[{{$item.lang_id}}][type_package]" rows="10">{{$data[$item.lang_id].type_package}}</textarea>
																	</div>
															</div>
							                                <div class="form-group">
																<label class="control-label col-md-2">{{l}}Infomation Product{{/l}}</label>
																<div class="col-md-10">
																		<textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][full_text]" rows="20" cols="90">{{$data[$item.lang_id].full_text}}</textarea>
																</div>
															</div>
															<div class="form-group">
																<label class="control-label col-md-2">{{l}}Dịch Vụ{{/l}}</label>
																<div class="col-md-10">
																		<textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][dichvu]" rows="20" cols="90">{{$data[$item.lang_id].dichvu}}</textarea>
																</div>
															</div>
															
<!--															<div class="form-group">-->
<!--																	<label class="control-label col-md-2">{{l}}Thông Tin Công Ty{{/l}}</label>-->
<!--																	<div class="col-md-10">-->
<!--																			<textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][company_profile]" rows="20" cols="90">{{$data[$item.lang_id].company_profile}}</textarea>-->
<!--																	</div>-->
<!--															</div>-->
															
															<div class="form-group">
																<label class="control-label col-md-2">{{l}}Seo Title{{/l}}</label>
																<div class="col-md-10">
																		<input type="text" class="form-control" name="data[{{$item.lang_id}}][seo_title]"  value="{{$data[$item.lang_id].seo_title}}" >
																</div>
															</div>
															
															<div class="form-group">
																<label class="control-label col-md-2">{{l}}Seo Keywords{{/l}}</label>
																<div class="col-md-10">
																		<input type="text" class="form-control" name="data[{{$item.lang_id}}][seo_keywords]"  value="{{$data[$item.lang_id].seo_keywords}}" >
																</div>
															</div>
															
															
															<div class="form-group">
																<label class="control-label col-md-2">{{l}}Seo Description{{/l}}</label>
																<div class="col-md-10">
																		<input type="text" class="form-control" name="data[{{$item.lang_id}}][seo_description]"  value="{{$data[$item.lang_id].seo_description}}" >
																</div>
															</div>
															
								                                
								                                <div class="clearfix form-actions">
																	<div class="col-md-offset-3 col-md-9">
																		<button class="btn btn-info" type="submit">
																			<i class="ace-icon fa fa-check bigger-110"></i>
																			{{l}}Save{{/l}}
																		</button>
																	</div>
																</div> 
                                </div>

                                {{/foreach}}

                            </div>

                        </form>

                    </div>

                </div>



            </div><!-- /.span -->

        </div><!-- /.row -->



    </div>


</div>
