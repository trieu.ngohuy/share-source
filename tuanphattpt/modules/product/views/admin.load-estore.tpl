                                        <label class="control-label col-md-2">{{l}}Estore Category{{/l}}</label>
                                        <div class="col-md-10">
                                            <select name="data[estore_category_gid]" class="chosen-select form-control" id="form-field-select-3">
                                                <option>{{l}}Choose category{{/l}}</option>
                                                {{foreach from=$allCatEstores item=item}}
                                                <option value="{{$item.estore_category_gid}}">{{$item.name}}</option>
                                                {{/foreach}}
                                            </select>
                                        </div>
