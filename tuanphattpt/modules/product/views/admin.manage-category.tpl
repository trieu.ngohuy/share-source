


<div class="page-header">
	<h1>
		{{l}}Category Product{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}List All Category Product{{/l}}
		</small>
	</h1>
</div>

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">
				{{if $allCategories|@count <= 0}}
                        <div class="alert alert-info">
                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
								{{l}}No category with above conditions.{{/l}}
						</div>
                        {{/if}}
                        
                        {{if $categoryMessage|@count > 0 && $categoryMessage.success == true}}
                        <div class="alert alert-success">
                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
								{{$categoryMessage.message}}.
						</div>
                        {{/if}}
                        
                        {{if $categoryMessage|@count > 0 && $categoryMessage.success == false}}
                        <div class="alert alert-danger">
                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
								{{$categoryMessage.message}}.
						</div>
                {{/if}}
                
                
				<table id="simple-table" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<form id="top-search" name="search" method="post" action="">
								<th class="center">
									<button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
								</th>
								<th class="center">
									<input class="check-all" type="checkbox" />
								</th>
                                <th>{{l}}Parent{{/l}}</th>
                                <th>{{l}}Created{{/l}}</th>
                                <th class="center">
                                	{{l}}Sort{{/l}}
                                    {{if $fullPermisison}}
                                     	<a href="javascript:document.sortForm.submit();"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/save_16.png" alt="Save sorting"></a>
                               		{{/if}}
                                </th>
                                <th colspan="4">{{l}}Detail{{/l}}
                                	<input  type="text" name="condition[keyword]" id="username" value="{{$condition.keyword}}"" placeholder="{{l}}Find Name{{/l}}" class="form-control" />
                                </th>
                        	</form>
						</tr>
					</thead>

					<tbody>
						{{if $allCategories|@count > 0}}
							<form action="" method="post" name="sortForm">
							{{foreach from=$allCategories item=item name=category key=key}}
													<tr>
														<td class="center">{{$key+1}}</td>
														<td class="center">{{if 1 == $item.product_deleteable}}<input type="checkbox" value="{{$item.product_category_gid}}" name="allCategories" class="allCategories"/>{{else}}<img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/unselect.png" alt="Do not delete, enable, disable" />{{/if}}</td>
					                                    <td>{{$item.parent}}</td>
					                                    <td>{{$item.created_date}}</td>
					                                    <td class="center">
					                                        {{if $fullPermisison}}
					                                        <input name="data[{{$item.product_category_gid}}]" value="{{$item.sorting}}" size="3" style="text-align: center;"></input>
					                                        {{else}}
					                                        {{$item.sorting}}
					                                        {{/if}}
					                                    </td>
					                                    
					                                    <td colspan="5" style="padding:0px;">
					                                    <!-- All languages -->
					                                        <table style="border-collapse: separate;margin-bottom: 0px;" class="table table-bordered table-striped">
					                                            <tbody id="table{{$item.product_category_id}}">
					                                                <tr>
					                                                    <td width="75%">--</td>
					                                                    <td width="7%">
					                                                        {{if $item.genabled == '1'}}
					                                                            {{if $fullPermisison && 1 == $item.product_deleteable}}
					                                                            <a href="{{$APP_BASE_URL}}product/admin/disable-category/gid/{{$item.product_category_gid}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png"></a>
					                                                            {{else}}
					                                                            <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png">
					                                                            {{/if}}
					                                                        {{else}}
					                                                            {{if $fullPermisison && 1 == $item.product_deleteable}}
					                                                            <a href="{{$APP_BASE_URL}}product/admin/enable-category/gid/{{$item.product_category_gid}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png"></a>
					                                                            {{else}}
					                                                            <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png">
					                                                            {{/if}}
					                                                        {{/if}}
					                                                    </td>
					                                                    <td width="10%"  class="center">
					                                                        {{if $fullPermisison}}
					                                                        	<span class="tooltip-area">
																					<a href="{{$APP_BASE_URL}}product/admin/edit-category/gid/{{$item.product_category_gid}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
																				</span>
					                                                        {{else}}
					                                                        --
					                                                        {{/if}}
					                                                    </td>
					                                                    <td width=8%">
					                                                        {{$item.product_category_gid}}
					                                                    </td>
					                                                </tr>
					                                                
					                                                {{foreach from=$item.langs item=item2}}
					                                                <tr>
					                                                    <td><image style="vertical-align:middle;" src="{{$BASE_URL}}{{$item2.lang_image}}"> {{$item2.name}}</td>
					                                                    <td>
					                                                    
					                                                    {{assign var='langId' value=$item2.lang_id}}
					                                                    
					                                                    {{if $item.genabled == '1'}}
					                                                    
					                                                        <!-- FULL PERMISSION -->
					                                                        {{p name=edit_category module=product expandId=$langId}}
					                                                            {{if $item2.enabled == '1' }}
					                                                            	{{if 1 == $item.product_deleteable}}
					                                                                	<a href="{{$APP_BASE_URL}}product/admin/disable-category/gid/{{$item.product_category_gid}}/lid/{{$item2.lang_id}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png"></a>
					                                                            	{{else}}
					                                                            		<img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png">
					                                                            	{{/if}}
					                                                            {{else}}
					                                                            	{{if 1 == $item.product_deleteable}}
					                                                                	<a href="{{$APP_BASE_URL}}product/admin/enable-category/gid/{{$item.product_category_gid}}/lid/{{$item2.lang_id}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png"></a>
					                                                            	{{else}}
					                                                            		<img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png">
					                                                            	{{/if}}
					                                                            {{/if}}
					                                                        {{/p}}
					                                                        <!-- DON'T HAVE PERMISSION -->
					                                                        {{np name=edit_category module=product expandId=$langId}}
					                                                            {{if $item2.enabled == '1'}}
					                                                                <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png">
					                                                            {{else}}
					                                                                <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png">
					                                                            {{/if}}
					                                                        {{/np}}
					                                                    {{else}}
					                                                        <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png">
					                                                    {{/if}}
					                                                    </td>
					                                                    
									                                    <td class="center">
									                                        
																			{{p name=edit_category module=product expandId=$langId}}
					                                                        	<span class="tooltip-area">
																					<a href="{{$APP_BASE_URL}}product/admin/edit-category/gid/{{$item.product_category_gid}}/lid/{{$item2.lang_id}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
																				</span>
					                                                        {{/p}}
					                                                        {{np name=edit_category module=product expandId=$langId}}
					                                                        --
					                                                        {{/np}}
									                                    </td>
					                                                    <td>
					                                                        {{$item2.product_category_id}}
					                                                    </td>
					                                                </tr>
					                                                {{/foreach}}
					                                                
					                                            </tbody>
					                                        </table>
					                                    </td>
                                    
													</tr>
												{{/foreach}}
												</form>
						{{/if}}
					</tbody>
				</table>
				
				
			</div><!-- /.span -->
		</div><!-- /.row -->
		
		
		<div class="col-lg-12">
							<div class="form-group col-lg-4">
								<label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
								<div class="col-lg-6">
									<select id="action" class="form-control" >
		                                <option value=";">{{l}}Choose an action...{{/l}}</option>
		                                {{p name=delete_category module=product}}
		                                <option value="deleteCategory();">{{l}}Delete{{/l}}</option>
		                                {{/p}}
		                                <option value="enableCategory();">{{l}}Enable{{/l}}</option>
		                                <option value="disableCategory();">{{l}}Disable{{/l}}</option>
		                            </select>
		                        </div>
		                        <div class="col-lg-6">
	                            	<a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
	                            </div>
							</div>
							<div class="form-group col-lg-2">
								<label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
								<div class="col-lg-12">
									<form class="search" name="search" method="post" action="">
		                                <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
		                                    <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
		                                    <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
		                                    <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
		                                    <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
		                                    <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
		                                </select>
		                            </form>
		                        </div>
							</div>
							{{if $countAllPages > 1}}
							<div class="col-lg-6 pagination">
								{{if $first}}
	                            <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
	                            {{/if}}
	                            {{if $prevPage}}
	                            <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
	                            {{/if}}
	                            
	                            {{foreach from=$prevPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>
	                            
	                            {{foreach from=$nextPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            {{if $nextPage}}
	                            <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
	                            {{/if}}
	                            {{if $last}}
	                            <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
	                            {{/if}}
                            
							</div>
							{{/if}}
						</div>
					</div>
		
		
	</div>
</div>

<script language="javascript" type="text/javascript">
$(document).ready(function(){
    $('.close').click(function(){
		$(this).parent().hide( "slow");
    });
    $('.check-all').click(function(){
    	if(this.checked) { // check select status
            $('.allCategories').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.allCategories').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
});

function applySelected()
{
	var task = document.getElementById('action').value;
	eval(task);
}
function enableCategory()
{
    var all = document.getElementsByName('allCategories');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('Please choose an category');
    }
    window.location.href = '{{$APP_BASE_URL}}product/admin/enable-category/gid/' + tmp;
}

function disableCategory()
{
    var all = document.getElementsByName('allCategories');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('Please choose an category');
    }
    window.location.href = '{{$APP_BASE_URL}}product/admin/disable-category/gid/' + tmp;
}

function deleteCategory()
{
    var all = document.getElementsByName('allCategories');
    var tmp = '';
    var count = 0;
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
             count++;
        }
    }
    if ('' == tmp) {
        alert('Please choose an category');
        return;
    } else {
    	result = confirm('Are you sure you want to delete ' + count + ' category(s)?');
        if (false == result) {
            return;
        }
    }
    window.location.href = '{{$APP_BASE_URL}}product/admin/delete-category/gid/' + tmp;
}


function deleteACategory(id)
{
    result = confirm('Are you sure you want to delete this category?');
    if (false == result) {
        return;
    }
    window.location.href = '{{$APP_BASE_URL}}product/admin/delete-category/gid/' + id;
}
</script>