﻿<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>  
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<script type="text/javascript">
    //<![CDATA[

    jQuery(document).ready(function () {

        CKFinder.setupCKEditor(null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/');
        jQuery("#images").sortable();
        jQuery("#images").disableSelection();

        //Make slug
        {{foreach from=$allLangs item=item name=langDiv}}
            jQuery('#title{{$smarty.foreach.langDiv.iteration}}').makeSlug({
                slug: jQuery('#alias{{$smarty.foreach.langDiv.iteration}}')
            });
        {{/foreach}}

        //Init ckfinder
        CkfinerImagesInit();
    });

    //Ckfinder images init
    function CkfinerImagesInit(){
        //Init ckfinder
        CKFinder.setupCKEditor(null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/');

        jQuery("#ulImages").sortable();
        jQuery("#ulImages").disableSelection();
        jQuery("#ulImages2").sortable();
        jQuery("#ulImages2").disableSelection();
    }

    var imgId;
    var wrapId;
    function chooseImage(id, _id)
    {
        imgId = id;
        wrapId = _id;
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();
        finder.basePath = '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/'; // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.selectActionFunction = setFileField;
        finder.popup();
    }
    // This is a sample function which is called when a file is selected in CKFinder.
    function setFileField(fileUrl)
    {
        document.getElementById('imgDisplay_'+wrapId+'_' + imgId).src = fileUrl;
        document.getElementById('inp_'+wrapId+'_' + imgId).value = fileUrl;
        document.getElementById('divDisplay_'+wrapId+'_' + imgId).style.display = '';
        document.getElementById('divNoImage_'+wrapId+'_' + imgId).style.display = 'none';
    }
    function clearImage(imgId, _id)
    {
        wrapId = _id;
        document.getElementById('imgDisplay_'+wrapId+'_' + imgId).src = '';
        document.getElementById('inp_'+wrapId+'_' + imgId).value = '';
        document.getElementById('divDisplay_'+wrapId+'_' + imgId).style.display = 'none';
        document.getElementById('divNoImage_'+wrapId+'_' + imgId).style.display = '';
    }

    function addMoreImg(_id)
    {
        jQuery(_id + " > li.hidden").filter(":first").removeClass('hidden');
    }

//]]>
</script>
<style type="text/css">
    .ul-images { list-style-type: none; margin: 0; padding: 0;}
    .ul-images li { margin: 10px; float: left; text-align: center;  height: 180px;}
</style>




<div class="page-header">
    <h1>
        {{l}}Product{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}New Product{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">
                {{if $errors|@count > 0}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{if $errors.main}}
                    </strong> {{$errors.main}}
                    {{else}}
                    {{l}}Please check following information again{{/l}}
                    {{/if}} 
                </div>
                {{/if}}


                <div class="col-sm-12 widget-container-col ui-sortable">

                    <div class="tabbable tabs-left">

                        <ul class="nav nav-tabs" id="myTab2">
                            <li class="active">
                                <a href="#tab0" data-toggle="tab" aria-expanded="true">
                                    <i class="pink ace-icon fa fa-home bigger-110"></i>
                                    {{l}}Basic{{/l}}
                                </a>
                            </li>

                            {{foreach from=$allLangs item=item index=index name=langTab}}
                            <li>
                                <a href="#tab{{$item.lang_id}}" data-toggle="tab" aria-expanded="true">
                                    <image style="vertical-align:middle;" src="{{$BASE_URL}}{{$item.lang_image}}"> {{$item.name}}
                                </a>
                            </li>
                            {{/foreach}}
                        </ul>

                        <form action="" method="post" class="form-horizontal">
                            <div class="tab-content">

                                <div id="tab0" class="tab-pane active">

                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Category{{/l}}</label>
                                        <div class="col-md-10">
                                            <select name="data[product_category_gid]" class="chosen-select form-control" id="form-field-select-3">
                                                {{foreach from=$allCats item=item}}
                                                <option value="{{$item.product_category_gid}}" {{if $data.product_category_gid == $item.product_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
                                                {{/foreach}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Sorting{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="data[sorting]"
                                                   value="{{$data.sorting}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Hiển thị dự án{{/l}}</label>
                                        <div class="col-md-10">
                                            <label class="radio-inline">
                                                <input type="radio" name="data[show_project]" value="0" {{if $data.show_project != '1'}}checked="checked"{{/if}}/> No
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="data[show_project]" value="1" {{if $data.show_project == '1'}}checked="checked"{{/if}}/> Yes
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Product Code{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="data[code]"  value="{{$data.code}}" >
                                        </div>
                                    </div>

                                    <footer class="panel-footer">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">{{l}}Ứng dụng sản phẩm:{{/l}}</label>
                                            <div class="col-md-10">
                                                <p><a href="javascript:addMoreImg('#ulImages2')">{{l}}+ Add more images{{/l}}</a></p>
                                                <ul id="ulImages2" class="ul-images">
                                                    {{section name=images loop=50}}
                                                    {{assign var="i" value=$smarty.section.images.iteration}}
                                                    <li {{if $i >2 && $i > $data.images|@count}}class="hidden"{{/if}}>
                                                        <input type="hidden" id="inp_image2_{{$i}}" name="data[images2][]">
                                                        <div id="divDisplay_image2_{{$i}}" style="display: none;">
                                                            <img src="" id="imgDisplay_image2_{{$i}}" style="max-width: 150px; max-height:150px; border:dashed thin;"></img>
                                                        </div>
                                                        <div id="divNoImage_image2_{{$i}}" style="width: 150px; border: thin dashed; text-align: center; padding:70px 0px;">
                                                            No image
                                                        </div>
                                                        <br/>
                                                        <a href="javascript:chooseImage({{$i}}, 'image2');">{{l}}Choose image{{/l}}</a>
                                                        |
                                                        <a href="javascript:clearImage({{$i}}, 'image2');">{{l}}Delete{{/l}}</a>
                                                    </li>
                                                    {{/section}}
                                                </ul>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">{{l}}Images:{{/l}}</label>
                                            <div class="col-md-10">
                                                <p><a href="javascript:addMoreImg('#ulImages')">{{l}}+ Add more images{{/l}}</a></p>
                                                <ul id="ulImages" class="ul-images">
                                                    {{section name=images loop=50}}
                                                    {{assign var="i" value=$smarty.section.images.iteration}}
                                                    <li {{if $i >2 && $i > $data.images|@count}}class="hidden"{{/if}}>
                                                        <input type="hidden" id="inp_image_{{$i}}" name="data[images][]">
                                                        <div id="divDisplay_image_{{$i}}" style="display: none;">
                                                            <img src="" id="imgDisplay_image_{{$i}}" style="max-width: 150px; max-height:150px; border:dashed thin;"></img>
                                                        </div>
                                                        <div id="divNoImage_image_{{$i}}" style="width: 150px; border: thin dashed; text-align: center; padding:70px 0px;">
                                                            No image
                                                        </div>
                                                        <br/>
                                                        <a href="javascript:chooseImage({{$i}}, 'image');">{{l}}Choose image{{/l}}</a>
                                                        |
                                                        <a href="javascript:clearImage({{$i}}, 'image');">{{l}}Delete{{/l}}</a>
                                                    </li>
                                                    {{/section}}
                                                </ul>
                                            </div>
                                        </div>
                                    </footer>   

                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Save{{/l}}
                                            </button>
                                        </div>
                                    </div>

                                </div>

                                {{foreach from=$allLangs item=item name=langDiv}}
                                <div class="tab-pane" id="tab{{$item.lang_id}}">

                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Title{{/l}}</label>
                                        <div class="col-md-10">
                                            <input id="title{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][title]"  value="{{$data[$item.lang_id].title}}" maxlength="19">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Alias{{/l}}</label>
                                        <div class="col-md-10">
                                            <input id="alias{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][alias]"  value="{{$data[$item.lang_id].alias}}" >.html
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Giới thiệu{{/l}}</label>
                                        <div class="col-md-10">
                                            <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][intro_text]" rows="20" cols="90">{{$data[$item.lang_id].intro_text}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Mô tả{{/l}}</label>
                                        <div class="col-md-10">
                                            <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][full_text]" rows="20" cols="90">{{$data[$item.lang_id].full_text}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Cấu hình rút gọn{{/l}}</label>
                                        <div class="col-md-10">
                                            <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][spec_intro]" rows="20" cols="90">{{$data[$item.lang_id].spec_intro}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Cấu hình{{/l}}</label>
                                        <div class="col-md-10">
                                            <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][spec]" rows="20" cols="90">{{$data[$item.lang_id].spec}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Chọn đèn{{/l}}</label>
                                        <div class="col-md-10">
                                            <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][pickup]" rows="20" cols="90">{{$data[$item.lang_id].pickup}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Cài đặt{{/l}}</label>
                                        <div class="col-md-10">
                                            <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][setup]" rows="20" cols="90">{{$data[$item.lang_id].setup}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Cấu tạo{{/l}}</label>
                                        <div class="col-md-10">
                                            <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][madeup]" rows="20" cols="90">{{$data[$item.lang_id].madeup}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Bảo trì{{/l}}</label>
                                        <div class="col-md-10">
                                            <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][maintaince]" rows="20" cols="90">{{$data[$item.lang_id].maintaince}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Tài liệu{{/l}}</label>
                                        <div class="col-md-10">
                                            <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][doc]" rows="20" cols="90">{{$data[$item.lang_id].doc}}</textarea>
                                        </div>
                                    </div>

                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Save{{/l}}
                                            </button>
                                        </div>
                                    </div> 
                                </div>

                                {{/foreach}}

                            </div>

                        </form>

                    </div>

                </div>



            </div><!-- /.span -->

        </div><!-- /.row -->



    </div>


</div>
