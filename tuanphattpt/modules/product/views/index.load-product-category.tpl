<!-- category-slider -->
<div class="category-slider">
    <ul class="owl-carousel owl-style2" data-dots="false" data-loop="true" data-nav = "true" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-items="1">
        <li>
            <img src="assets/data/category-slide.jpg" alt="category-slider">
        </li>
        <li>
            <img src="assets/data/slide-cart2.jpg" alt="category-slider">
        </li>
    </ul>
</div>
<!-- ./category-slider -->
<!-- view-product-list-->
<div id="view-product-list" class="view-product-list">
    <h2 class="page-heading">
        <span class="page-heading-title">{{$category.name}}</span>
    </h2>
    <ul class="display-product-option">
        <li class="view-as-grid selected">
            <span>grid</span>
        </li>
        <li class="view-as-list">
            <span>list</span>
        </li>
    </ul>
    <!-- PRODUCT LIST -->
    <ul class="row product-list grid">
        {{foreach from=$allProduct item=item}}
        <li class="col-sx-12 col-sm-3">
            <div class="product-container">
                <div class="left-block">
                    <a href="#">
                        <img class="img-responsive" alt="product" src="{{$BASE_URL}}{{$item.images}}" />
                    </a>
                    <div class="quick-view">
                        <a title="Add to my wishlist" class="heart" href="#"></a>
                        <a title="Add to compare" class="compare" href="#"></a>
                        <a title="Quick view" class="search" href="#"></a>
                    </div>
                </div>
                <div class="right-block">
                    <h5 class="product-name"><a href="#">{{$item.title}}</a></h5>

                    <div class="content_price">
                        <span class="price product-price">Call / (Min. Order)</span>
                    </div>
                    <div class="info-orther1">
                        <p><b>{{$item.name_company}}</b></p>
                        <p class="availability"><span>Viet Nam </span><a>Contact Details</a></p>

                    </div>
                    <div class="info-orther contact_show_list">

                        <div class="col-sx-12 col-sm-6 p0">
                            <p><b>Type:</b> Cereals</p>
                            <p><b>Model Number:</b> 9001</p>
                            <p><b>Weight:</b> 9001</p>
                        </div>
                        <div class="col-sx-12 col-sm-6 p0">
                            <p><b>Brand Name:</b> Ngoc Thuy Tien</p>
                            <p><b>Color:</b>  Black White </p>
                            <p><b>Place of origin:</b> Vietnam</p>
                        </div>
                        <div class="entry-more">
                            <a href="#" class="contact_now">Contact Now</a>
                            <a href="#">Chat</a>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        {{/foreach}}
    </ul>
    <!-- ./PRODUCT LIST -->
</div>
<!-- ./view-product-list-->
<div class="sortPagiBar">
    {{if $countAllPages > 1}}
    <div class="bottom-pagination">
        <nav>
            <ul class="pagination">
                {{if $prevPage}}
                <li>
                    <a href="javascript:loadProduct({{$prevPage}})" aria-label="Prev">
                        <span aria-hidden="true">&laquo; Prev</span>
                    </a>
                </li>
                {{/if}}

                {{foreach from=$prevPages item=item}}
                <li><a href="javascript:loadProduct({{$item}})">{{$item}}</a></li>
                {{/foreach}}

                <li class="active"><a>{{$currentPage}}</a></li>

                {{foreach from=$nextPages item=item}}
                <li><a href="javascript:loadProduct({{$item}})">{{$item}}</a></li>
                {{/foreach}}

                {{if $nextPage}}
                <li>
                    <a href="javascript:loadProduct({{$nextPage}})" aria-label="Next">
                        <span aria-hidden="true">Next &raquo;</span>
                    </a>
                </li>
                {{/if}}
            </ul>
        </nav>
    </div>
    {{/if}}
    <div class="show-product-item">
        <select name="displayNum" id="displayNum" onchange="loadProduct(1)">
            <option value="8" {{if $displayNum == 8}} selected="selected" {{/if}}>Hiển thị 8</option>
            <option value="16" {{if $displayNum == 16}} selected="selected" {{/if}}>Hiển thị 16</option>
            <option value="40" {{if $displayNum == 40}} selected="selected" {{/if}}>Hiển thị 40</option>
            <option value="80" {{if $displayNum == 100}} selected="selected" {{/if}}>Hiển thị 100</option>
        </select>
    </div>
    <div class="sort-product">
        <select name="displaySort" id="displaySort" onchange="loadProduct(1)">
            <option value="title" {{if $displaySort == 'title'}} selected="selected" {{/if}}>Product Name</option>
            <option value="price_sale" {{if $displaySort == 'price_sale'}} selected="selected" {{/if}}>Price</option>
        </select>
        <div class="sort-product-icon">
            <i class="fa fa-sort-alpha-asc"></i>
        </div>
    </div>
</div>
<br class="cb">