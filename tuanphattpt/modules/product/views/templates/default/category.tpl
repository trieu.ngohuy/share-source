{{if $gid == 0}}
<!-- Content -->
			<div class="row content">
				<!-- Main Content -->
				<section class="main-content col-lg-12 col-md-12 col-sm-12">
					
					{{foreach from=$allCats item=item_cate}}
						<div class="products-row row">
						
							<!-- Carousel Heading -->
							<div class="col-lg-12 col-md-12 col-sm-12">
								
								<div class="carousel-heading">
									<h4>{{$item_cate.name}}</h4>
									<div class="carousel-arrows">
										<i class="icons icon-left-dir"></i>
										<i class="icons icon-right-dir"></i>
									</div>
								</div>
								
							</div>
							<!-- /Carousel Heading -->
							
							<!-- Carousel -->
							<div class="carousel owl-carousel-wrap col-lg-12 col-md-12 col-sm-12">
								
								<div class="owl-carousel" data-max-items="3">
										
										
									{{foreach from=$item_cate.product item=item}}
										<!-- Slide -->
										<div>
											<!-- Carousel Item -->
											<div class="product">
												
												<div class="product-image">
													{{if $item.sale == 1}}<span class="product-tag">{{l}}Sale{{/l}} {{$item.pt_sale}}</span>{{/if}}
													<img src="{{$BASE_URL}}{{$item.main_image}}" alt="Product1">
												</div>
												
												<div class="product-info">
													<h5><a href="products_page_v1.html">{{$item.title}}</a></h5>
													<span class="price">
														{{if $item.sale == 1}}
															{{$item.gia_sale}} {{$item.tiente}}
														{{else}}
															{{$item.price}} {{$item.tiente}}
														{{/if}}
													</span>
												</div>
												<div class="product-actions">
													<span class="add-to-cart">
														<span class="action-wrapper">
															<i class="icons icon-basket-2"></i>
															<a href="{{$item.url}}"><span class="action-name">{{l}}Xem Thêm{{/l}}</span></a>
														</span>
													</span>
												</div>
												
											</div>
											<!-- /Carousel Item -->
										</div>
										<!-- /Slide -->
										
									{{/foreach}}
										
								</div>
							</div>
							<!-- /Carousel -->
							
						</div>
					{{/foreach}}
						
				</section>
				<!-- /Main Content -->
			</div>
			<!-- /Content -->
{{else}}
<!-- Content -->
			<div class="row content">
				<!-- Main Content -->
				<section class="main-content col-lg-12 col-md-12 col-sm-12">
					
					{{foreach from=$allCats item=item_cate}}
						<div class="products-row row">
						
							<!-- Carousel Heading -->
							<div class="col-lg-12 col-md-12 col-sm-12">
								
								<div class="carousel-heading">
									<h4>{{$item_cate.name}}</h4>
									<div class="carousel-arrows">
										<i class="icons icon-left-dir"></i>
										<i class="icons icon-right-dir"></i>
									</div>
								</div>
								
							</div>
							<!-- /Carousel Heading -->
							
							<!-- Carousel -->
							<div class="carousel owl-carousel-wrap col-lg-12 col-md-12 col-sm-12">
								
								<div class="owl-carousel" data-max-items="3">
										
										
									{{foreach from=$item_cate.product item=item}}
										<!-- Slide -->
										<div>
											<!-- Carousel Item -->
											<div class="product">
												
												<div class="product-image">
													{{if $item.sale == 1}}<span class="product-tag">{{l}}Sale{{/l}} {{$item.pt_sale}}</span>{{/if}}
													<img src="{{$BASE_URL}}{{$item.main_image}}" alt="Product1">
												</div>
												
												<div class="product-info">
													<h5><a href="products_page_v1.html">{{$item.title}}</a></h5>
													<span class="price">
														{{if $item.sale == 1}}
															{{$item.gia_sale}} {{$item.tiente}}
														{{else}}
															{{$item.price}} {{$item.tiente}}
														{{/if}}
													</span>
												</div>
												<div class="product-actions">
													<span class="add-to-cart">
														<span class="action-wrapper">
															<i class="icons icon-basket-2"></i>
															<a href="{{$item.url}}"><span class="action-name">{{l}}Xem Thêm{{/l}}</span></a>
														</span>
													</span>
												</div>
												
											</div>
											<!-- /Carousel Item -->
										</div>
										<!-- /Slide -->
										
									{{/foreach}}
										
								</div>
							</div>
							<!-- /Carousel -->
							
						</div>
					{{/foreach}}
						
				</section>
				<!-- /Main Content -->
			</div>
			<!-- /Content -->
{{/if}}