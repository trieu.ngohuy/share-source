<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Fashion</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">

                <div class="block left-module">
                    <p class="title_block">{{l}}GỢI Ý CHO BẠN{{/l}}</p>
                    <div class="block_content">
                        <ul class="products-block">
                            {{foreach from=$allFlasher item=item key=key}}
                            <li>
                                <div class="products-block-left">
                                    <a href="{{$item.url}}">
                                        <img src="{{$BASE_URL}}{{$item.main_image}}" alt="{{$item.title}}">
                                    </a>
                                </div>
                                <div class="products-block-right">
                                    <p class="product-name">
                                        <a href="{{$item.url}}">{{$item.title}}</a>
                                    </p>
                                    <p class="product-price">{{$item.price}}</p>
                                </div>
                            </li>
                            {{/foreach}}

                        </ul>
                    </div>
                </div>

                <div class="col-left-slide left-module">
                    <ul class="owl-carousel owl-style2" data-loop="true" data-nav = "false" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-items="1" data-autoplay="true">
                        <li><a href="#"><img src="{{$LAYOUT_HELPER_URL}}front/assets/data/slide-left.jpg" alt="slide-left"></a></li>
                        <li><a href="#"><img src="{{$LAYOUT_HELPER_URL}}front/assets/data/slide-left2.jpg" alt="slide-left"></a></li>
                        <li><a href="#"><img src="{{$LAYOUT_HELPER_URL}}front/assets/data/slide-left3.png" alt="slide-left"></a></li>
                    </ul>

                </div>


            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- view-product-list-->
                {{if $gid == 0}}
                <img src="{{$LAYOUT_HELPER_URL}}front/images/freeship.png" class="col-md-12 col-sm-12 col-xs-12" />
                <br style="clear: both;">
                {{/if}}
                <div id="view-product-list" class="view-product-list">
                    <h2 class="page-heading">
                        <span class="page-heading-title">{{$name_cate}}</span>
                    </h2>
                    <ul class="display-product-option">
                        <li class="view-as-grid selected">
                            <span>grid</span>
                        </li>
                        <li class="view-as-list">
                            <span>list</span>
                        </li>
                    </ul>
                    <!-- PRODUCT LIST -->
                    <ul class="row product-list grid">

                        {{foreach from=$allProduct item=item }}
                        {{if $gid == 0}}
                        <li class="col-sx-12 col-sm-4">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="{{$item.url}}">
                                        <img class="img-responsive" alt="{{$item.title}}" src="{{$BASE_URL}}{{$item.main_image}}" />
                                    </a>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="{{$item.url}}">{{$item.title}}</a></h5>
                                    <div class="content_price">
                                        <span class="price product-price">{{$item.price_s}} VNĐ</span>
                                        <span class="price old-price">{{$item.price}} VNĐ</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>{{l}}Item Code:{{/l}} #{{$item.product_gid}}</p>
                                        <div class="product-desc">
                                            {{$item.intro_text}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        {{else}}
                        <li class="col-sx-12 col-sm-4">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="{{$item.url}}">
                                        <img class="img-responsive" alt="{{$item.title}}" src="{{$BASE_URL}}{{$item.main_image}}" />
                                    </a>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="{{$item.url}}">{{$item.title}}</a></h5>
                                    <div class="content_price">
                                        <span class="price product-price">{{$item.price}} VNĐ</span>
                                    </div>
                                    <div class="info-orther">
                                        <p>{{l}}Item Code:{{/l}} #{{$item.product_gid}}</p>
                                        <div class="product-desc">
                                            {{$item.intro_text}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        {{/if}}
                        {{/foreach}}
                    </ul>
                    <!-- ./PRODUCT LIST -->
                </div>
                <!-- ./view-product-list-->
                {{if $countAllPages > 1}}					
                <div class="sortPagiBar">
                    <div class="bottom-pagination">
                        <nav>
                            <ul class="pagination">

                                {{if $first}}
                                <li><a href="?page=1" title="{{l}}First Page{{/l}}"><span aria-hidden="true">&laquo;</span></a></li>
                                    {{/if}}
                                    {{if $prevPage}}
                                <li><a href="?page={{$prevPage}}" title="{{l}}Previous Page{{/l}}"><span aria-hidden="true">&lsaquo;</span></a></li>
                                    {{/if}}

                                {{foreach from=$prevPages item=item}}
                                <li><a href="?page={{$item}}" title="{{$item}}">{{$item}}</a></li>
                                    {{/foreach}}

                                <li class="active"><a href="#"   title="{{$currentPage}}">{{$currentPage}}</a></li>

                                {{foreach from=$nextPages item=item}}
                                <li><a href="?page={{$item}}"  title="{{$item}}">{{$item}}</a></li>
                                    {{/foreach}}

                                {{if $nextPage}}
                                <li><a href="?page={{$nextPage}}"  title="{{l}}Next Page{{/l}}"><span aria-hidden="true">&rsaquo;</span></a></li>
                                    {{/if}}
                                    {{if $last}}
                                <li><a href="?page={{$countAllPages}}"  title="{{l}}Last Page{{/l}}"><span aria-hidden="true">&raquo;</span></a></li>
                                    {{/if}}	
                            </ul>
                        </nav>
                    </div>
                </div>

                {{/if}}	



            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>