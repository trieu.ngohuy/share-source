<!-- Content -->
			<div class="row content">
				<!-- Main Content -->
				<section class="main-content col-lg-12 col-md-12 col-sm-12">
					
					<div class="row"> 
						{{foreach from=$allProduct item=item name=product key=key}}
	                        <!-- Product Item -->
	                        <div class="col-lg-4 col-md-4 col-sm-4 product">
	                            
	                            <div class="product-image">
									{{if $item.sale == 1}}<span class="product-tag">{{l}}Sale{{/l}} {{$item.pt_sale}}</span>{{/if}}
									<img src="{{$BASE_URL}}{{$item.main_image}}" alt="Product1">
								</div>
								
								<div class="product-info">
									<h5><a href="products_page_v1.html">{{$item.title}}</a></h5>
									<span class="price">
										{{if $item.sale == 1}}
											{{$item.gia_sale}} {{$item.tiente}}
										{{else}}
											{{$item.price}} {{$item.tiente}}
										{{/if}}
									</span>
								</div>
								<div class="product-actions">
									<span class="add-to-cart">
										<span class="action-wrapper">
											<i class="icons icon-basket-2"></i>
											<a href="{{$item.url}}"><span class="action-name">{{l}}Xem Thêm{{/l}}</span></a>
										</span>
									</span>
								</div>
								
	                            
	                        </div>
	                        <!-- Product Item -->
                        
                        {{/foreach}}
                        
						<br style="clear: both;"/>
                        {{if $countAllPages > 1}}
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="pagination">
                            	{{if $prevPage}}
                                <a href="?page={{$prevPage}}"><div class="previous"><i class="icons icon-left-dir"></i></div></a>
                                {{/if}}
                                
                                <a href="#"><div class="page-button">{{$currentPage}}</div></a>
                                
                                {{foreach from=$nextPages item=item}}
                                <a href="?page={{$item}}"><div class="next"><i class="icons icon-right-dir"></i></div></a>
                                {{/foreach}}
                            </div>
                        </div>
                        {{/if}}
                        
                    </div>
						
				</section>
				<!-- /Main Content -->
			</div>
			<!-- /Content -->