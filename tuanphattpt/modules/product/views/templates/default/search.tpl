<div class="container">
			<div class="row">
				<div class="category-label col-md-12 col-sm-12 col-xs-12">
					{{$name_cate}}
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12 category-col-right col-md-push-9">
					<div class="recommend-box col-md-12 col-sm-12">
						<div class="recommend-label col-md-12 col-sm-12">
							{{l}}GỢI Ý CHO BẠN{{/l}}
						</div>
						<div class="recommend-content col-md-12 col-sm-12">
						
							{{foreach from=$allFlasher item=item key=key}}
								<div class="hot-news-item col-md-12 col-sm-6">
									<img src="{{$BASE_URL}}{{$item.main_image}}">
									<br class="cb">
									<a href="{{$item.url}}">{{$item.intro_text}}</a>
								</div>
							{{/foreach}}
							
						</div>
					</div>
				</div>
				<div class="category-col-left product-list col-md-9 col-sm-12 col-xs-12 col-md-pull-3">
					{{foreach from=$allProduct item=item }}
						<div class="col-md-4 col-sm-4">
							<div class="col-md-12 col-sm-12 product-item">
								<div class="product-image col-md-12 col-sm-12">
									
									<img src="{{$BASE_URL}}{{$item.main_image}}"/>
									<div class="detail">
										<span class="detail-label">{{l}}Loại{{/l}}: </span><span>{{$item.cname}}</span> <br/> <br/>
										<span class="detail-label">{{l}}Vị{{/l}}: </span><span>{{$item.type}}</span> <br/> <br/>
										<span class="detail-label">{{l}}Thời gian giao{{/l}}: </span><span>{{$item.time_gh}} {{l}}Phút{{/l}}</span> <br/> <br/>
										<span class="detail-label">{{l}}Tối thiểu{{/l}}: </span><span>{{$item.so_luong}}{{l}}/Sản Phẩm{{/l}}</span> <br/>
									</div>
									</a>
								</div>
								<p class="name"><a href="{{$item.url}}">{{$item.title}}</a></p>
								<p class="price"><a href="{{$item.url}}">{{$item.price}} {{l}}VNĐ{{/l}}</a></p>
							</div>
						</div>
					{{/foreach}}
					
					{{if $countAllPages > 1}}					
						<!-- Pagination -->
						<div class="col-md-12 col-sm-12">
							<ul class="pagination">
						
									{{if $first}}
			                            <li><a href="?page=1" title="{{l}}First Page{{/l}}"><span aria-hidden="true">&laquo;</span></a></li>
			                            {{/if}}
			                            {{if $prevPage}}
			                            <li><a href="?page={{$prevPage}}" title="{{l}}Previous Page{{/l}}"><span aria-hidden="true">&lsaquo;</span></a></li>
			                            {{/if}}
			                            
			                            {{foreach from=$prevPages item=item}}
			                            <li><a href="?page={{$item}}" title="{{$item}}">{{$item}}</a></li>
			                            {{/foreach}}
			                            
			                            <li class="active"><a href="#"   title="{{$currentPage}}">{{$currentPage}}</a></li>
			                            
			                            {{foreach from=$nextPages item=item}}
			                            <li><a href="?page={{$item}}"  title="{{$item}}">{{$item}}</a></li>
			                            {{/foreach}}
			                            
			                            {{if $nextPage}}
			                            <li><a href="?page={{$nextPage}}"  title="{{l}}Next Page{{/l}}"><span aria-hidden="true">&rsaquo;</span></a></li>
			                            {{/if}}
			                            {{if $last}}
			                            <li><a href="?page={{$countAllPages}}"  title="{{l}}Last Page{{/l}}"><span aria-hidden="true">&raquo;</span></a></li>
			                            {{/if}}	
							</ul>
						</div>	<!-- Pagination Ends-->
						
					{{/if}}	
					
					
				</div>
				
			</div>
		</div>