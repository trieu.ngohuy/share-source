<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        {{sticker name=common_breadcrumbs}}
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            {{sticker name=common_left_panel}}
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- category-slider 
                <div class="category-slider">
                    <ul class="owl-carousel owl-style2" data-dots="false" data-loop="true" data-nav = "true" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-items="1">
                        {{foreach from=$cat.images item=imageItem}}
                        <li>
                            <img src="{{$imageItem}}" alt="{{$cat.title}} - slider">
                        </li>
                        {{/foreach}}
                    </ul>
                </div>
                ./category-slider -->
                <!-- subcategories -->
                <div class="subcategories">
                    <ul>
                        <li class="current-categorie">
                            <a href="{{$cat.url}}">{{$cat.name}}</a>
                        </li>
                        {{foreach from=$cat.detail item=subCatItem}}
                        <li>
                            <a href="{{$subCatItem.url}}">{{$subCatItem.name}}</a>
                        </li>
                        {{/foreach}}
                    </ul>
                </div>
                <!-- ./subcategories -->
                <!-- view-product-list-->
                <div id="view-product-list" class="view-product-list">
                    <h2 class="page-heading">
                        <span class="page-heading-title">{{$cat.name}}</span>
                    </h2>
                    <!-- PRODUCT LIST -->
                    <ul class="row product-list grid">
                        {{assign var='count' value='1'}}
                        {{foreach from=$allProduct item=productItem}}
                        <li class="col-sx-12 col-sm-3">
                            {{sticker name=product_item}}
                        </li>
                        {{if $count % 4 == 0}}<div class="clearfix"></div>{{/if}}
                            {{assign var='count' value=$count+1}}
                        {{/foreach}}
                    </ul>
                    <!-- ./PRODUCT LIST -->
                </div>
                <!-- ./view-product-list-->
                {{sticker name=common_paging}}
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>