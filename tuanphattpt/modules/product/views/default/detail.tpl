<div class="container">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div itemscope itemtype="http://schema.org/Product" id="product-2895"
                 class="post-2895 product type-product status-publish has-post-thumbnail product_cat-den-led-nha-xuong first instock shipping-taxable product-type-simple">
                <div class="row">
                    <div class="col-lg-7 featured-image">
                        <div class="product-zoom-image">
                            <img class="xzoom" src="{{$objDetail.main_image}}" xoriginal="{{$objDetail.main_image}}"/>
                        </div>
                        <div id="product-gallery-image" class="thumbnails-slider xzoom-thumbs" style="z-index: 9;">
                            {{foreach from=$objDetail.arrImages item=item}}
                            <div>
                                <a href="{{$item}}" class="xzoom-a">
                                    <img class="xzoom-gallery" width="150" height="150" src="{{$item}}" xpreview="{{$item}}" title="{{$objDetail.title}}" style="margin-bottom: 0px; margin-left: 0px;"/>
                                </a>
                            </div>
                            {{/foreach}}
                        </div>
                    </div>
                    <div class="col-lg-5 entry-summary">
                        <h1 itemprop="name" class="product_title entry-title">{{$objDetail.title}}</h1>
                        <div class="product_meta">
                            <div class="posted_in">{{l}}Danh mục{{/l}}: <a href="{{$objCategory.url}}"
                                                                           rel="tag">{{$objCategory.name}}</a></div>
                            <span class="sku_wrapper">{{l}}Mã{{/l}}: <span class="sku">{{$objDetail.code}}</span></span>
                        </div>
                        <div class="woocommerce-product-details__short-description">
                            <p>{{$objDetail.intro_text}}</p>
                        </div>
                        <a href="#thongso" class="view-spec"><i class="fa fa-tasks" aria-hidden="true"></i> {{l}}Xem thông số{{/l}}</a>
                        <div class="product-cta">
                            <div class="row margin-lr-0">
                                <a class="col-6 toggleCTA">
                                    <div class="float-l"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                    <div style="pointer-events: none;">
                                        <div class="help">{{l}}Tư vấn &amp; đặt mua{{/l}}</div>
                                        <div class="text">{{$config.hotline}}</div>
                                    </div>
                                </a>
                                <a class="col-6" href="javascript:void(Tawk_API.toggle())">
                                    <div class="float-l"><i class="fa fa-comments-o" aria-hidden="true"></i></div>
                                    <div>
                                        <div class="help">{{l}}Hỗ trợ trực tuyến{{/l}}</div>
                                        <div class="text">{{l}}Live Chat{{/l}}</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                {{if $objDetail.images2 != ''}}
                <h2 class="application-title">Ứng dụng sản phẩm</h2>
                <div class="application-gallery slick-initialized slick-slider">
                    <div aria-live="polite" class="slick-list draggable">
                        <div class="slick-track"
                             style="opacity: 1; width: 1088px; transform: translate3d(0px, 0px, 0px);" role="listbox">

                            {{foreach from=$objDetail.arrImages2 key=key item=item}}
                            <div class="slick-slide slick-active"
                                 data-slick-index="{{$key}}"
                                 aria-hidden="false" style="width: 272px;" tabindex="-1" role="option"
                                 aria-describedby="slick-slide{{$key}}">
                                <a href="{{$item}}" tabindex="0" class="app-img">
                                    <img src="{{$item}}" alt="{{$data.name}}">
                                </a>
                            </div>
                            {{/foreach}}

                        </div>
                    </div>
                </div>
                <div id="imgViewer">
                    <img src="{{$objDetail.main_image}}" alt="{{$objDetail.name}}"/>
                </div>
                {{/if}}
                <div class="woocommerce-tabs wc-tabs-wrapper" id="thongso">
                    <ul class="tabs wc-tabs">
                        {{if $objDetail.full_text}}
                        <li class="construct_tab_tab"><a href="#tab-description_tab">{{l}}Mô Tả{{/l}}</a></li>
                        {{/if}}
                        {{if $objDetail.spec}}
                        <li class="construct_tab_tab"><a href="#tab-construct_tab">{{l}}Thông số{{/l}}</a></li>
                        {{/if}}
                        {{if $objDetail.pickup}}
                        <li class="construct_tab_tab"><a href="#tab-pickup_tab">{{l}}Chọn đèn{{/l}}</a></li>
                        {{/if}}
                        {{if $objDetail.setup}}
                        <li class="setup_tab_tab"><a href="#tab-setup_tab">{{l}}Lắp đặt{{/l}}</a></li>
                        {{/if}}
                        {{if $objDetail.madeup}}
                        <li class="setup_tab_tab"><a href="#tab-madeup_tab">{{l}}Cấu tạo{{/l}}</a></li>
                        {{/if}}
                        {{if $objDetail.show_project === '1'}}
                        <li class="setup_tab_tab"><a href="#tab-project_tab">{{l}}Dự án{{/l}}</a></li>
                        {{/if}}
                        <li class="warranty_tab_tab"><a href="#tab-warranty_tab">{{l}}Bảo hành{{/l}}</a></li>
                        <li class="document_tab_tab"><a href="#tab-document_tab">{{l}}Tài liệu{{/l}}</a></li>
                    </ul>
                    <div class="row margin-lr-0">
                        {{if $objDetail.full_text}}
                        <div class="col-md-8 woocommerce-Tabs-panel woocommerce-Tabs-panel--construct_tab panel entry-content wc-tab"
                             id="tab-description_tab">
                            {{$objDetail.full_text}}
                        </div>
                        {{/if}}
                        {{if $objDetail.spec}}
                        <div class="col-md-8 woocommerce-Tabs-panel woocommerce-Tabs-panel--construct_tab panel entry-content wc-tab"
                             id="tab-construct_tab">
                            {{$objDetail.spec}}
                        </div>
                        {{/if}}
                        {{if $objDetail.pickup}}
                        <div class="col-md-8 woocommerce-Tabs-panel woocommerce-Tabs-panel--construct_tab panel entry-content wc-tab"
                             id="tab-pickup_tab">
                            {{$objDetail.pickup}}
                        </div>
                        {{/if}}
                        {{if $objDetail.setup}}
                        <div class="col-md-8 woocommerce-Tabs-panel woocommerce-Tabs-panel--setup_tab panel entry-content wc-tab"
                             id="tab-setup_tab">
                            {{$objDetail.setup}}
                        </div>
                        {{/if}}
                        {{if $objDetail.madeup}}
                        <div class="col-md-8 woocommerce-Tabs-panel woocommerce-Tabs-panel--setup_tab panel entry-content wc-tab"
                             id="tab-madeup_tab">
                            {{$objDetail.madeup}}
                        </div>
                        {{/if}}
                        {{if $objDetail.show_project === '1'}}
                        <div class="col-md-8 woocommerce-Tabs-panel woocommerce-Tabs-panel--setup_tab panel entry-content wc-tab"
                             id="tab-project_tab">
                            <div class="row">
                                {{foreach from=$objDetail.arrProjects item=projectItem}}
                                <div class="col-md-4 related-project">
                                    <a href="{{$projectItem.url}}">
                                        <img src="{{$projectItem.main_image}}" class="attachment-large size-large wp-post-image" alt="{{$projectItem.title}}">
                                        <p class="project-title">{{$projectItem.title}}</p>
                                    </a>
                                </div>
                                {{/foreach}}
                            </div>
                        </div>
                        {{/if}}
                        <div class="col-md-8 woocommerce-Tabs-panel woocommerce-Tabs-panel--warranty_tab panel entry-content wc-tab"
                             id="tab-warranty_tab">
                            {{$objDetail.maintaince}}
                        </div>
                        <div class="col-md-8 woocommerce-Tabs-panel woocommerce-Tabs-panel--document_tab panel entry-content wc-tab"
                             id="tab-document_tab">
                            {{$objDetail.doc}}
                        </div>
                        <div class="col-md-4">
                            <div class="related_products">
                                <section class="related products">
                                    <h2>{{l}}Sản phẩm cùng loại{{/l}}</h2>
                                    <ul class="products columns-2">
                                        {{foreach from=$arrRelatedProducts key=key item=item}}
                                        <li class="post-{{$item.product_gid}} product type-product status-publish has-post-thumbnail product_cat-den-led-nha-xuong {{if $key % 2 === 0}}first{{else}}last{{/if}} instock shipping-taxable product-type-simple"
                                            data-toggle="popover" title="Mã SP: {{$item.code}}" data-trigger="hover"
                                            data-popover-content="#pop-{{$item.product_gid}}"
                                            data-placement="auto">
                                            <a href="{{$item.url}}"
                                               class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img
                                                        width="450" height="450"
                                                        src="{{$item.main_image}}"
                                                        class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image"
                                                        alt="{{$item.title}}"
                                                        srcset="{{$item.main_image}}"
                                                        sizes="(max-width: 450px) 100vw, 450px">
                                                <h2 class="woocommerce-loop-product__title">{{$item.title}}</h2> <span class="price">{{l}}Liên hệ{{/l}}</span> </a>
                                            <a href="{{$item.url}}" data-quantity="1"
                                               class="button product_type_simple ajax_add_to_cart"
                                               data-product_id="2482" data-product_sku="{{$item.code}}"
                                               aria-label="Đọc thêm về &ldquo;{{$item.title}}&rdquo;"
                                               rel="nofollow">{{l}}Đọc tiếp{{/l}}</a>
                                            <div class="hidden" id="pop-{{$item.product_gid}}">
                                                {{$item.spec_intro}}
                                            </div>
                                        </li>
                                        {{/foreach}}
                                    </ul>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
                <meta itemprop="url" content="/san-pham/den-led-nha-xuong/den-led-low-bay/"/>
            </div>
        </main>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function ($) {

        //Phone icon handle
        PhoneIconHandle($);

        //Product slide
        ProductSlide($);

        //Application img click
        $(document).on("click","a.app-img", function () {
            //Get src
            var src = $(this).children('img').attr('src');

            //Set viewer image
            $('#imgViewer img').attr('src', src);

            //Display
            $('#imgViewer').fadeIn('slow');

            //Prevent execure href
            event.preventDefault();
        });
        //Click outside viewer image
        $(document).mouseup(function (e) {
            var container = $("#imgViewer img");

            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                $('#imgViewer').fadeOut('fast');
            }
        });
    });

    /*
    Product slide handle
    */
    function ProductSlide($){
        //Product slide Xzoom init
        $(".xzoom").xzoom({tint: '#333', Xoffset: 15});

        //Thumbnails click
        $(document).on("click","a.xzoom-a", function () {
            //Prevent execure href
            event.preventDefault();
            //Set main image
            $('.xzoom').attr('src',$(this).attr('href'));
            $('.xzoom').attr('xoriginal',$(this).attr('href'));
        });
    }
    /*
    Phone icon handle
     */
    function PhoneIconHandle($) {
        //Click icon phone
        $('#icoPhone').click(function () {
            $('.ctaMobileModal').fadeIn("slow");
        });
        //Click close support screen
        $('.ctaMobileModal .close').click(function () {
            $('.ctaMobileModal').css('display', 'none');
        });
        //Click outside support screen
        $(document).mouseup(function (e) {
            var container = $(".ctaMobileModalWrap");

            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                $('.ctaMobileModal').css('display', 'none');
            }
        });
    }
</script>