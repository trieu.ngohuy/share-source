<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Computers, Accessories, Services </span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Categories</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    {{foreach from=$childCategory item=item}}
                                        <li class="active">
                                            <span></span><a href="{{$BASE_URL}}category/{{$item.alias}}">{{$item.name}}</a>
                                            <ul>
                                                {{foreach from=$item.child item=childItem}}
                                                    <li><span></span><a href="{{$BASE_URL}}category/{{$item.alias}}">{{$childItem.name}}</a></li>
                                                {{/foreach}}
                                            </ul>
                                        </li>
                                    {{/foreach}}
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                <!-- block filter -->
                <div class="block left-module">
                    <p class="title_block">SUPPLIER COUNTRY</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-filter-price">
                            <!-- filter categgory -->
                            <div class="layered-content">
                                <ul class="check-box-list">
                                    <li>
                                        <input type="checkbox" id="c1" name="cc" />
                                        <label for="c1">
                                            <span class="button"></span>
                                            Vietnamese<span class="count">(10)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="c2" name="cc2" />
                                        <label for="c2">
                                            <span class="button"></span>
                                            United State<span class="count">(10)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="c3" name="cc2" />
                                        <label for="c3">
                                            <span class="button"></span>
                                            Australia<span class="count">(10)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="c4" name="cc2" />
                                        <label for="c4">
                                            <span class="button"></span>
                                            Brunei<span class="count">(10)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="c5" name="cc2" />
                                        <label for="c5">
                                            <span class="button"></span>
                                            Canada<span class="count">(10)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="c6" name="cc2" />
                                        <label for="c6">
                                            <span class="button"></span>
                                            Chile<span class="count">(10)</span>
                                        </label>
                                    </li>

                                    <li>
                                        <input type="checkbox" id="c7" name="cc2" />
                                        <label for="c7">
                                            <span class="button"></span>
                                            Japan<span class="count">(10)</span>
                                        </label>
                                    </li>

                                    <li>
                                        <input type="checkbox" id="c8" name="cc2" />
                                        <label for="c8">
                                            <span class="button"></span>
                                            Malaysia<span class="count">(10)</span>
                                        </label>
                                    </li>

                                    <li>
                                        <input type="checkbox" id="c9" name="cc2" />
                                        <label for="c9">
                                            <span class="button"></span>
                                            Mexico<span class="count">(10)</span>
                                        </label>
                                    </li>

                                    <li>
                                        <input type="checkbox" id="c10" name="cc2" />
                                        <label for="c10">
                                            <span class="button"></span>
                                            New Zealand<span class="count">(10)</span>
                                        </label>
                                    </li>

                                    <li>
                                        <input type="checkbox" id="c11" name="cc2" />
                                        <label for="c11">
                                            <span class="button"></span>
                                            Peru<span class="count">(10)</span>
                                        </label>
                                    </li>

                                    <li>
                                        <input type="checkbox" id="c12" name="cc2" />
                                        <label for="c12">
                                            <span class="button"></span>
                                            Singapore<span class="count">(10)</span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->

                    </div>
                </div>
                <!-- ./block filter  -->

                <!-- left silide -->
                <div class="col-left-slide left-module">
                    <ul class="owl-carousel owl-style2" data-loop="true" data-nav = "false" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-items="1" data-autoplay="true">
                        <li><a href="#"><img src="assets/data/slide-left.jpg" alt="slide-left"></a></li>
                        <li><a href="#"><img src="assets/data/slide-left2.jpg" alt="slide-left"></a></li>
                        <li><a href="#"><img src="assets/data/slide-left3.png" alt="slide-left"></a></li>
                    </ul>

                </div>
                <!--./left silde-->
                <!-- SPECIAL -->
                <div class="block left-module">
                    <p class="title_block">SPECIAL PRODUCTS</p>
                    <div class="block_content">
                        <ul class="products-block">
                            <li>
                                <div class="products-block-left">
                                    <a href="#">
                                        <img src="assets/data/product-100x122.jpg" alt="SPECIAL PRODUCTS">
                                    </a>
                                </div>
                                <div class="products-block-right">
                                    <p class="product-name">
                                        <a href="#">Woman Within Plus Size Flared</a>
                                    </p>
                                    <p class="product-price">$38,95</p>
                                    <p class="product-star">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </p>
                                </div>
                            </li>
                        </ul>
                        <div class="products-block">
                            <div class="products-block-bottom">
                                <a class="link-all" href="#">All Products</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ./SPECIAL -->
                <!-- TAGS -->
                <div class="block left-module">
                    <p class="title_block">TAGS</p>
                    <div class="block_content">
                        <div class="tags">
                            <a href="#"><span class="level1">actual</span></a>
                            <a href="#"><span class="level2">adorable</span></a>
                            <a href="#"><span class="level3">change</span></a>
                            <a href="#"><span class="level4">consider</span></a>
                            <a href="#"><span class="level3">phenomenon</span></a>
                            <a href="#"><span class="level4">span</span></a>
                            <a href="#"><span class="level1">spanegs</span></a>
                            <a href="#"><span class="level5">spanegs</span></a>
                            <a href="#"><span class="level1">actual</span></a>
                            <a href="#"><span class="level2">adorable</span></a>
                            <a href="#"><span class="level3">change</span></a>
                            <a href="#"><span class="level4">consider</span></a>
                            <a href="#"><span class="level2">gives</span></a>
                            <a href="#"><span class="level3">change</span></a>
                            <a href="#"><span class="level2">gives</span></a>
                            <a href="#"><span class="level1">good</span></a>
                            <a href="#"><span class="level3">phenomenon</span></a>
                            <a href="#"><span class="level4">span</span></a>
                            <a href="#"><span class="level1">spanegs</span></a>
                            <a href="#"><span class="level5">spanegs</span></a>
                        </div>
                    </div>
                </div>
                <!-- ./TAGS -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <div class="product-tab">
                    <ul class="nav-tab">
                        <li class="active">
                            <a aria-expanded="false" data-toggle="tab" href="#product-detail">Product</a>
                        </li>
                        <li>
                            <a aria-expanded="true" data-toggle="tab" href="#information">Seller</a>
                        </li>
                    </ul>
                    <div class="tab-container">
                        <div id="product-detail" class="tab-panel active">


                        </div>
                        <div id="information" class="tab-panel">

                        </div>

                    </div>
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        loadSeller(1);
        loadProduct(1);
    });

    function loadSeller(page) {
        $.ajax({
            type: 'GET',
            url: "load-seller/{{$category.product_category_id}}?page="+page,
            success: function(data){
                $('#information').html(data);
            }
        });
    }

    function loadProduct(page) {
        $.ajax({
            type: 'GET',
            url: "load-product-category/{{$category.product_category_id}}?page="+page,
            data: {
                displayNum : $('#displayNum').val(),
                displaySort : $('#displaySort').val()
            },
            success: function(data){
                $('#product-detail').html(data);
            }
        });
    }
</script>
<style>
    .homeslider{
    display: none;
}
.vertical-menu-content{
    display: none !important;
}
.title:hover + div{
    display: block;
}
</style>