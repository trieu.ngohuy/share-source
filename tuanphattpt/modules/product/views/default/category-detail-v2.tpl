<div class="container all-product">
    <div class="row">
        <aside class="sidebar" style="z-index: 9;">
            <section class="widget woocommerce_product_categories-2 woocommerce widget_product_categories">
                <h3>{{l}}Sản phẩm{{/l}}</h3>
                <ul class="product-categories">
                    {{foreach from=$arrCat item=item}}
                    <li class="cat-item cat-item-9 {{if $item.alias === $objCat.alias}}current-cat{{/if}}"><a href="{{$item.url}}">{{$item.name}}</a>
                    </li>
                    {{/foreach}}
                </ul>
                <div class="ctaSidebar">
                    {{foreach from=$arrContacts key=key item=item}}
                    <a class="ctaButton" href="{{if $item.type === 'phone'}}tel:{{$item.val}}{{else}}mailto:{{$item.val}}{{/if}}">
                        <span>{{$key}}</span>
                        {{if $item.type === 'phone'}}
                        {{$item.val}}
                        {{else}}
                        <span style="font-weight: bold;color: #fff;font-size: 17px;">{{$item.val}}</span>
                        {{/if}}
                    </a>
                    {{/foreach}}

                </div>
            </section>
        </aside>
        <main class="main">
            <div id="primary" class="content-area">
                <main id="main" class="site-main" role="main">
                    <nav class="woocommerce-breadcrumb"><a href="https://www.potech.com.vn">Trang chủ</a>&nbsp;&#47;&nbsp;<a href="/san-pham/">Sản phẩm</a>&nbsp;&#47;&nbsp;{{$objCat.name}}
                    </nav>
                    <header class="woocommerce-products-header">
                        <h1 class="woocommerce-products-header__title page-title">{{$objCat.name}}</h1>
                    </header>
                    <p class="woocommerce-result-count"> {{l}}Hiển thị{{/l}} {{$from}}&ndash;{{$to}} {{l}}trong{{/l}} {{$countAllProducts}} {{l}}kết quả{{/l}}</p>
                    <form class="woocommerce-ordering" method="get" style="display: none;">
                        <select name="orderby" class="orderby">                            
                            <option value="date" {{if $orderby === 'date'}}selected=""{{/if}}>{{l}}Thứ tự theo sản phẩm mới{{/l}}</option>
                            <option value="popularity" {{if $orderby === 'popularity'}}selected=""{{/if}}>{{l}}Thứ tự theo mức độ phổ biến{{/l}}</option>
                            <option value="price" {{if $orderby === 'price'}}selected=""{{/if}}>{{l}}Thứ tự theo giá: thấp đến cao{{/l}}</option>
                            <option value="price-desc" {{if $orderby === 'price-desc'}}selected=""{{/if}}>{{l}}Thứ tự theo giá: cao xuống thấp{{/l}}</option>
                        </select> 
                    </form>
                    <ul class="products columns-3">
                        {{foreach from=$arrProducts item=item}}
                        <li class="post-2482 product type-product status-publish has-post-thumbnail product_cat-den-led-nha-xuong  instock shipping-taxable product-type-simple" data-toggle="popover" title="{{l}}Mã SP{{/l}}: {{$item.code}}" data-trigger="hover" data-popover-content="#pop-{{$item.product_gid}}"
                            data-placement="auto">
                            <a href="{{$item.url}}" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                <img width="450" height="450" src="{{$item.main_image}}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image"
                                     alt="{{$item.title}}"
                                     sizes="(max-width: 450px) 100vw, 450px">
                                <h2 class="woocommerce-loop-product__title">{{$item.title}}</h2>
                                <span class="price">{{l}}Liên hệ{{/l}}</span>
                            </a>

                            <a href="{{$item.url}}" data-quantity="1" class="button product_type_simple ajax_add_to_cart" data-product_id="{{$item.product_gid}}" data-product_sku="{{$item.code}}" aria-label="Đọc thêm về &ldquo;{{$item.title}}&rdquo;"
                               rel="nofollow">{{l}}Đọc tiếp{{/l}}</a>

                            <div class="hidden" id="pop-{{$item.product_gid}}">
                                {{$item.spec_intro}}
                            </div>
                        </li>
                        {{/foreach}}
                    </ul>                    
                    {{if $countAllPages > 1}}
                    <nav class="woocommerce-pagination">
                        <ul class='page-numbers'>
                            {{if $prevPage}}
                            <li><a class="prev page-numbers" href="?page={{$prevPage}}">←</a></li>
                                {{/if}}

                            {{foreach from=$prevPages item=item}}                            
                            <li><a class='page-numbers' href='?page={{$item}}'>{{$item}}</a></li>
                                {{/foreach}}

                            <li><span aria-current='page' class='page-numbers current'>{{$currentPage}}</span></li>

                            {{foreach from=$nextPages item=item}}                            
                            <li><a class='page-numbers' href='?page={{$item}}'>{{$item}}</a></li>
                                {{/foreach}}

                            {{if $nextPage}}
                            <li><a class="next page-numbers" href="?page={{$nextPage}}">&rarr;</a></li>
                                {{/if}}
                        </ul>
                    </nav>
                    {{/if}}
                </main>
            </div>
        </main>
    </div>
</div>
<hr>
<div class="container">
    <div class="cat-content row">
        <div class="col-md-8 hidden-sm-down">
            {{$objCat.description}}
        </div>
        <div class="col-md-4">
            <div class="recent_portfolio">
                <h3>{{l}}Khách hàng của TUẤN PHÁT{{/l}}</h3>
                <ul>
                    {{foreach from=$arrProjects item=item}}
                    <li>
                        <a class="single_portfolio" href="{{$item.url}}" rel="bookmark" title="{{$item.title}}">
                            <div class="portfolio_thumb"><img src="{{$item.main_image}}"></div>
                            <div class="portfolio_title"> {{$item.title}}</div>
                        </a>
                        <div class="related-product">
                            <div class="row p-0">
                                {{foreach from=$item.products item=item2}}
                                <a class="col-md-6" href="{{$item2.url}}">
                                    <table>
                                        <tr>
                                            <td class="align-middle" style="width: 30%;">
                                                <div class="product-image">
                                                    <img src="{{$item2.main_image}}" alt="{{$item2.title}}">
                                                </div>
                                            </td>
                                            <td class="align-middle">
                                                <div class="product-title">{{$item2.title}}</div>
                                            </td>
                                        </tr>
                                    </table>
                                </a>
                                {{/foreach}}
                            </div>
                        </div>
                    </li>
                    {{/foreach}}
                </ul>
            </div>
        </div>
    </div>
</div>