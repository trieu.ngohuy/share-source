<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{l}}Tìm kiếm sản phẩm{{/l}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">

            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <!--Products panels-->
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>{{l}}Từ khóa{{/l}}: {{$key}}</h4></div>
                    <div class="panel-body">
                        <!-- PRODUCT LIST -->
                        <ul class="row product-list grid category-products">
                            {{assign var='count' value='1'}}
                            {{foreach from=$data.products item=item}}
                            <li class="col-sm-3 col-md-3 col-lg-3">
                                {{sticker name=cat_product_item}}
                            </li>					
                            {{if $count % 4 == 0}}<div class="clearfix"></div>{{/if}}
                            {{assign var='count' value=$count+1}}
                            {{/foreach}}
                        </ul>
                        <!-- ./PRODUCT LIST -->
                    </div>
                </div>
                {{sticker name=common_paging}}
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>