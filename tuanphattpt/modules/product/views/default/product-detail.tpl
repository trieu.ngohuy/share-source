<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <a title="{{$product.title}}">{{$product.title}}</a>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- ./block category  -->
                <!-- block best sellers -->
                <div class="block left-module">
                    <p class="title_block">{{l}}SPECIAL PRODUCT{{/l}}</p>
                    <div class="block_content">
                        <div class="owl-carousel owl-best-sell" data-loop="true" data-nav = "false" data-margin = "0" data-autoplayTimeout="1000" data-autoplay="true" data-autoplayHoverPause = "true" data-items="1">
                            {{foreach from=$productSpecial item=item}}
                            <ul class="products-block best-sell">
                                {{foreach from=$item item=itemProduct}}
                                <li>
                                    <div class="products-block-left">
                                        <a href="{{$BASE_URL}}product/{{$itemProduct.alias}}">
                                            <img src="{{$BASE_URL}}{{$itemProduct.icon}}" alt="{{$itemProduct.title}}">
                                        </a>
                                    </div>
                                    <div class="products-block-right">
                                        <p class="product-name">
                                            <a href="{{$BASE_URL}}product/{{$itemProduct.alias}}">{{$itemProduct.title}}</a>
                                        </p>
                                        <p class="product-price">
                                            {{if $itemProduct.price_sale != ''}}
                                                {{$itemProduct.price_sale}} {{$itemProduct.pname2}} / {{$itemProduct.pname}}
                                            {{else}}
                                                {{l}}Call{{/l}}
                                            {{/if}}
                                        </p>
                                    </div>
                                </li>
                                {{/foreach}}
                            </ul>
                            {{/foreach}}
                        </div>
                    </div>
                </div>
                <!-- ./block best sellers  -->
                <div class="block left-module">
                    {{foreach from=$adv item=item}}
					    <div class="col-xs-12 col-sm-12 col-md-12 m10t qc" style="padding: 0px">
					        <a href="{{$item.link}}"><img alt="Banner right 1" src="{{$BASE_URL}}{{$item.images}}"></a>
					    </div>
					{{/foreach}}
                </div>
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- Product -->
                <div id="product">
                    <div class="primary-box row">
                        <div class="pb-left-column col-xs-12 col-sm-6">
                            <!-- product-imge-->
                            <div class="product-image">
                                <div class="product-full">
                                    <img id="product-zoom" src='{{$BASE_URL}}{{$product.icon}}' data-zoom-image="{{$BASE_URL}}{{$product.icon}}"/>
                                </div>
                                <div class="product-img-thumb" id="gallery_01">
                                    <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="true">
                                        {{foreach from=$product.images item=item}}
                                        <li>
                                            <a href="#" data-image="{{$BASE_URL}}{{$item.zoom}}" data-zoom-image="{{$BASE_URL}}{{$item.full}}">
                                                <img id="product-zoom"  src="{{$BASE_URL}}{{$item.thumb}}" />
                                            </a>
                                        </li>
                                        {{/foreach}}
                                    </ul>
                                </div>
                            </div>
                            <!-- product-imge-->
                        </div>
                        <div class="pb-right-column col-xs-12 col-sm-6">
                            <h1 class="product-name">{{$product.title}}</h1>
                            <div class="product-price-group">
                                <span class="price">
                                    {{l}}Giá FOB{{/l}} :
                                    {{if $product.price_sale != ''}}
                                        {{$product.price_sale}} {{$product.pname2}} / {{$product.pname}}
                                    {{else}}
                                        {{l}} Call / (Min. Order) {{/l}}
                                    {{/if}}
                                </span>
                            </div>
                            <div class="info-orther">
                                {{l}}Khả năng cung cấp{{/l}} :
                                {{$product.supply_ability}} / {{$product.csname}} / {{$product.cstname}}
                            </div>
                            <div class="info-orther">
                                {{l}}Số lượng tối thiểu{{/l}} :
                                {{if $product.so_luong != ''}}
                                    {{$product.so_luong}} {{$product.pname}}
                                {{/if}}
                            </div>
                            <div class="info-orther">
                                {{l}}Mã Hải Cảng{{/l}} :
                                {{if $product.mhc != ''}}
                                    {{$product.mhc}}
                                {{/if}}
                            </div>
                            <div class="info-orther">
                                {{l}}Điều Khoản Thanh Toán{{/l}} :
                                {{foreach from=$product.thanhtoan item=item key=key}}
									{{$item.name}}{{if $key < $product.thanhtoan|@count -1}},{{/if}}
								{{/foreach}}
                            </div>
                            <div class="info-orther">
                                {{sticker name=front_hs_standard}}
                            </div>
                            <div class="product-desc">
                                {{$product.intro_text}}
                            </div>
                            <div class="entry-ci">
	                            <div class="entry-more">
		                            <a href="{{$BASE_URL}}{{$company.alias}}/contact-us" class="contact_now" style=" background: #f7b92a none repeat scroll 0 0;color: #fff;border: 1px solid #eaeaea;padding: 10px 12px;" >{{l}}Contact Now{{/l}}</a>
		                            <a href="#" style="background: #eee none repeat scroll 0 0;border: 1px solid #eaeaea;padding: 10px 12px;">Chat</a>
		                        </div>
		                 	</div>
                        </div>
                    </div>
                    <!-- tab product -->
                    <div class="product-tab" style="margin: 10px 0px 0px;">
                        <ul class="nav-tab">
                            <li class="active">
                                <a aria-expanded="false" data-toggle="tab" href="#product-detail">Product Details</a>
                            </li>
                            {{if $lock != 1}}
                            <li>
                                <a aria-expanded="true" data-toggle="tab" href="#information">Company Profile</a>
                            </li>
                            {{/if}}

                        </ul>
                        <div class="tab-container">
                            <div id="product-detail" class="tab-panel active">
                            	<h5 style="color: #db0303;font-weight: bold;margin-top: 10px;">{{l}}Quick Details{{/l}}</h5>
                            	<table class="table table-striped" style="border-bottom: 1px solid #ccc;margin-top: 4px;">
									<tbody>
									  <tr>
										<td scope="row" class="col-md-2 aaa">{{l}}Brand Name{{/l}}</td>
										<td>{{$company.pcompany_name}}</td>
										<td class="col-md-2 aaa">{{l}}Place of origin{{/l}}</td>
										<td>{{$product.ctname}}</td>
									  </tr>
									  <tr>
										<td scope="row" class="aaa">{{l}}Model Number{{/l}}</td>
										<td>{{$product.model}}</td>
										<td class="aaa">{{l}}Color{{/l}}</td>
										<td>
											{{foreach from=$product.color item=item key=key}}
												{{$item.name}}{{if $key < $product.color|@count -1}},{{/if}}
											{{/foreach}}
										</td>
																																																																																																																																																																																																																																																																																																																																																																																																																																																																						</td>
									  </tr>
									  <tr>
										<td scope="row" class="aaa">{{l}}Type{{/l}}</td>
										<td>{{$product.type}}</td>
										<td class="aaa">{{l}}Weight{{/l}}</td>
										<td>{{$product.trong_luong}}  {{$product.tlname}}</td>
									  </tr>
									</tbody>
							  	</table>
                            	
                            	<h5 style="color: #db0303;font-weight: bold;margin-top: 30px;">{{l}}Đóng gói & Giao hàng{{/l}}</h5>
                            	<table class="table table-striped" style="border-bottom: 1px solid #ccc;margin-top: 4px;">
									<tbody>
									  <tr>
										<td scope="row" class="col-md-3 aaa">{{l}}Cách thức đóng gói:{{/l}}</td>
										<td>{{$product.type_package}}</td>
									  </tr>
									  <tr>
										<td scope="row" class="col-md-3 aaa">{{l}}Thời gian giao hàng:{{/l}}</td>
										<td>{{$product.time_gh}}</td>
																																																																																																																																																																																																																																																																																																																																																																																																																																																																						</td>
									  </tr>
									</tbody>
							  	</table>
							  	
                            	
                            	
                            	
                            	<h5  style="color: #db0303;font-weight: bold;margin-top: 30px;">{{l}}Specification{{/l}}</h5>
                                
                                <div  style="border-bottom: 1px solid #ccc;margin-top: 4px;">{{$product.full_text}}</div>
                                <h5  style="color: #db0303;font-weight: bold;margin-top: 30px;">{{l}}Dịch Vụ{{/l}}</h5>
                            	<div  style="border-bottom: 1px solid #ccc;margin-top: 4px;">
                            	{{$product.dichvu}}
                            	</div>
<!--                            	<h5 style="color: #db0303;font-weight: bold;margin-top: 30px;">{{l}}Thông Tin Công Ty{{/l}}</h5>-->
<!--                            	<div  style="border-bottom: 1px solid #ccc;margin-top: 4px;">-->
<!--                            	{{$product.company_profile}}-->
<!--                            	</div>-->
                            	
                            	<br class="cb"/>
                            	{{if $isLogin == false}}
                            	
                            	{{else}}
                            	<h2 class="page-heading">
						            <span class="page-heading-title2">Contact Us</span>
						        </h2>
                            	<div class="col-sm-12 p0">
                            		{{if isset($message)}}
						                {{if $message.success == true}}
						                    <div class="alert alert-success" role="alert">
						                        {{$message.message}}
						                    </div>
						                {{else}}
						                    <div class="alert alert-warning" role="alert">
						                        {{$message.message}}
						                    </div>
						                {{/if}}
						            {{/if}}
                            		<form role="form" method="post" action="">
				                        <div class="contact-form-box col-sm-6 p0" style="padding: 0px 10px 0px 0px">
				                            <div class="form-selector">
				                            	<label>{{l}}Name{{/l}}<span>*</span></label>
						                        <input type="text" required name="data[name_sender]" id="name_sender" class="form-control input-sm" />
						                        <input type="hidden" class="form-control input-sm" name="data[type]" value="2"/>
						                        
				                            </div>
				                            
				                            <div class="form-selector">
				                                <label><span>*</span> {{l}}Email{{/l}}</label>
				                                <input type="email"  class="form-control input-sm" name="data[email]"  id="email" required />
				                            </div>
				                            <div class="form-selector">
				                                <label>{{l}}Phone{{/l}}</label>
							                        <input type="text" class="form-control input-sm"  name="data[phone]" id="phone" />
				                            </div>
				                            <div class="form-selector">
				                                <label>{{l}}Company Name{{/l}}</label>
							                        <input type="text" class="form-control input-sm"  name="data[name_company]" id="name_company" />
				                            </div>
											 <div class="form-selector">
				                               <label>{{l}}Company Address{{/l}}</label>
							                        <input type="text" class="form-control input-sm"  name="data[add_company]" id="add_company" />
				                            </div>
				                            <div class="form-selector">
				                                <button type="submit " class="form-control input-sm"  id="btn-contact" class="btn" style="background: rgb(247, 185, 42) none repeat scroll 0px 0px; color: rgb(255, 255, 255); border: 1px solid rgb(234, 234, 234); margin: 10px 0px 0px; width: 100px; padding: 10px 12px; height: 38px;">Send</button>
				                            </div>
				                            
				                        </div>
				                        <div class="contact-form-box col-sm-6 p0">
				                           
				                            <div class="form-selector">
				                                <label>{{l}}Subject{{/l}}<em>*</em></label>
							                        <input type="text" class="form-control input-sm"  required name="data[name]" id="name" value="{{$product.title}}" />
				                            </div>
				                            <div class="form-selector">
				                                <label>{{l}}Messsage{{/l}}<em>*</em></label>
							                        <textarea required class="form-control input-sm"  name="data[description]" class="form-control input-sm" rows="10" id="description"></textarea>
				                            </div>
				                        </div>
				                    </form>
				                    
				                    
            
            
				                </div>
                				<br class="cb"/>
                				{{/if}}
                				
                            </div>
                            <div id="information" class="tab-panel">
                                <dl class="dl-horizontal">
                                    <dt><b>{{l}}Tên công ty{{/l}}</b></dt><dd><a href="{{$BASE_URL}}{{$company.alias}}.htm" style="font-weight: bold;text-transform: uppercase;">{{$company.pcompany_name}}</a></dd>
                                    <dt><b>{{l}}Ảnh đại diện{{/l}}</b></dt><dd><img style="height: 100px" src="{{$BASE_URL}}{{$company.image}}"/></dd>
                                    <dt><b>{{l}}Địa chỉ công ty{{/l}}</b></dt><dd>{{$company.padd}}</dd>
                                    <dt><b>{{l}}Sản phẩm chính{{/l}}</b></dt><dd>{{$company.pproduct_chinh}}</dd>
                                    <dt><b>{{l}}Năm đăng ký{{/l}}</b></dt><dd>{{$company.namdangky}}</dd>
                                    <dt><b>{{l}}Website{{/l}}</b></dt><dd>{{$company.website}}</dd>
                                    <dt><b>{{l}}Số lượng nhân viên{{/l}}</b></dt><dd>{{$company.sonhanvien}}</dd>
<!--                                    <dt><b>{{l}}Chủ sở hữu{{/l}}</b></dt><dd>{{$company.full_name}}</dd>-->
                                    <dt><b>{{l}}Quy mô công ty{{/l}}</b></dt><dd>{{$company.quymo}}</dd>
                                    <dt><b>{{l}}Đặc điểm công ty{{/l}}</b></dt><dd>{{$company.pnote}}</dd>
                                    <dt><b>{{l}}Mô tả chi tiết{{/l}}</b></dt><dd>{{$company.pabout}}</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                    <!-- ./tab product -->
                    <!-- box product -->
                    <div class="page-product-box">
                        <h3 class="heading">Related Products</h3>
                        <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":3}}'>
                            {{foreach from=$productRelated item=item}}
                            <li>
                                <div class="product-container">
                                    <div class="left-block">
                                        <a href="{{$BASE_URL}}product/{{$item.alias}}">
                                            <img class="img-responsive" alt="product" src="{{$BASE_URL}}{{$item.icon}}" />
                                        </a>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="{{$BASE_URL}}product/{{$item.alias}}">{{$item.title}}</a></h5>

                                        <div class="content_price">
                                            <span class="price product-price">
                                                {{if $item.price_sale != ''}}
                                                    {{$item.price_sale}} {{$item.pname2}} / {{$item.pname}}
                                                {{else}}
                                                    {{l}} Call / (Min. Order) {{/l}}
                                                {{/if}}
                                            </span>
                                        </div>
                                        <p>
                                            {{l}}from {{/l}}: {{$item.ctname}} <br/>
                                            <a href="{{$BASE_URL}}{{$item.user_alias}}.htm"><b>{{$item.name_company}}</b></a>
                                        </p>
                                    </div>
                                </div>
                            </li>
                            {{/foreach}}
                        </ul>
                    </div>
                    <!-- ./box product -->
                    <!-- box product -->
                    <div class="page-product-box">
                        <h3 class="heading">{{l}}You might also like{{/l}}</h3>
                        <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":3}}'>
                            {{foreach from=$productRecommend item=item}}
                            <li>
                                <div class="product-container">
                                    <div class="left-block">
                                        <a href="{{$BASE_URL}}product/{{$item.alias}}">
                                            <img class="img-responsive" alt="product" src="{{$BASE_URL}}{{$item.icon}}" />
                                        </a>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="{{$BASE_URL}}product/{{$item.alias}}">{{$item.title}}</a></h5>

                                        <div class="content_price">
                                            <span class="price product-price">
                                                {{if $item.price_sale != ''}}
                                                    {{$item.price_sale}} {{$item.pname2}} / {{$item.pname}}
                                                {{else}}
                                                    {{l}} Call / (Min. Order) {{/l}}
                                                {{/if}}
                                            </span>
                                        </div>
                                        <p>
                                            {{l}}from {{/l}}: {{$item.ctname}} <br/>
                                            <a href="{{$BASE_URL}}{{$item.user_alias}}.htm"><b>{{$item.name_company}}</b></a>
                                        </p>
                                    </div>
                                </div>
                            </li>
                            {{/foreach}}
                        </ul>
                    </div>
                    <!-- ./box product -->
                </div>
                <!-- Product -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>