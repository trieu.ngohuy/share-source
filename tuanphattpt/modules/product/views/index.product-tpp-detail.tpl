<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{$category.name}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                {{sticker name=front_default_tpp}}
                <!-- ./block category  -->
                <!-- block filter -->

                <!-- ./block filter  -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <div class="product-tab">
                    <ul class="nav-tab">
                        <li class="active">
                            <a aria-expanded="true" data-toggle="tab">{{l}}Product{{/l}}</a>
                        </li>
                    </ul>
                    <div class="tab-container">
                        <div class="tab-panel active">
                            <!-- view-product-list-->
                            <div id="view-product-list" class="view-product-list">
                                <h2 class="page-heading">
                                    <span class="page-heading-title">{{$tpp.name}}</span>
                                </h2>
                                <!-- PRODUCT LIST -->
                                <ul class="row product-list grid">
                                    {{foreach from=$allProduct item=item}}
                                    <li class="col-sx-12 col-sm-3">
                                        <div class="product-container">
                                            <div class="left-block">
                                                <a href="{{$BASE_URL}}product/{{$item.alias}}">
                                                    <img class="img-responsive" alt="product" src="{{$BASE_URL}}{{$item.icon}}" />
                                                </a>
                                            </div>
                                            <div class="right-block">
                                                <h5 class="product-name"><a href="{{$BASE_URL}}product/{{$item.alias}}">{{$item.title}}</a></h5>

                                                <div class="content_price">
                                                    <span class="price product-price">
                                                        {{if $item.price_sale != ''}}
                                                            {{$item.price_sale}} {{$item.pname2}} / {{$item.pname}}
                                                        {{else}}
                                                            {{l}}Call / (Min. Order){{/l}}
                                                        {{/if}}
                                                    </span>
                                                </div>
                                                <div class="info-orther1">
                                                    <p><b>{{$item.name_company}}</b></p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    {{/foreach}}
                                    <br class="cb" style="clear:both">
                                </ul>
                                <!-- ./PRODUCT LIST -->
                            </div>
                            <!-- ./view-product-list-->
                            <div class="sortPagiBar">
                                {{if $countAllPages > 1}}
                                <div class="bottom-pagination">
                                    <nav>
                                        <ul class="pagination">
                                            {{if $prevPage}}
                                            <li>
                                                <a href="?page={{$prevPage}}" aria-label="Prev">
                                                    <span aria-hidden="true">&laquo; Prev</span>
                                                </a>
                                            </li>
                                            {{/if}}

                                            {{foreach from=$prevPages item=item}}
                                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                            {{/foreach}}

                                            <li class="active"><a>{{$currentPage}}</a></li>

                                            {{foreach from=$nextPages item=item}}
                                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                            {{/foreach}}

                                            {{if $nextPage}}
                                            <li>
                                                <a href="?page={{$nextPage}}" aria-label="Next">
                                                    <span aria-hidden="true">Next &raquo;</span>
                                                </a>
                                            </li>
                                            {{/if}}
                                        </ul>
                                    </nav>
                                </div>
                                {{/if}}
                            </div>
                            <br class="cb">
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

