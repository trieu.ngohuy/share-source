<div class="sortPagiBar clearfix">
    <span class="page-noite">Showing {{$firstItemInPage}} to {{$lastItemInPage}} of {{$count}} ({{$countAllPages}} Page)</span>
    {{if $countAllPages > 1}}
    <div class="bottom-pagination">
        <nav>
            <ul class="pagination">
                {{if $prevPage}}
                <li>
                    <a href="javascript:loadSeller({{$prevPage}})" aria-label="Prev">
                        <span aria-hidden="true">&laquo; Prev</span>
                    </a>
                </li>
                {{/if}}

                {{foreach from=$prevPages item=item}}
                <li><a href="javascript:loadSeller({{$item}})">{{$item}}</a></li>
                {{/foreach}}

                <li class="active"><a>{{$currentPage}}</a></li>

                {{foreach from=$nextPages item=item}}
                <li><a href="javascript:loadSeller({{$item}})">{{$item}}</a></li>
                {{/foreach}}

                {{if $nextPage}}
                <li>
                    <a href="javascript:loadSeller({{$nextPage}})" aria-label="Next">
                        <span aria-hidden="true">Next &raquo;</span>
                    </a>
                </li>
                {{/if}}
            </ul>
        </nav>
    </div>
    {{/if}}
</div>
<ul class="blog-posts">
    {{foreach from=$users item=item}}
    <li class="post-item">
        <article class="entry">
            <div class="row">
                <div class="col-sm-4">
                    <div class="entry-thumb image-hover2">
                        <a href="#">
                            <img src="{{$BASE_URL}}{{$item.image}}" alt="Blog">
                        </a>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="entry-ci">
                        <h3 class="entry-title"><a href="{{$BASE_URL}}{{$item.alias}}.html"><b>{{$item.name_company}}</b></a></h3>

                        <div class="entry-excerpt">
                            <p><b>Main products:</b> {{$item.sanphamchinh}}</p>
                            <p><b>Year Company Registered:</b> {{$item.namdangky}}</p>
                            <p><span>Viet Nam | </span> <a>Contact Detail ></a></p>
                        </div>
                        <div class="entry-more">
                            <a href="#" class="contact_now" >Contact Now</a>
                            <a href="#">Chat</a>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </li>
    {{/foreach}}
</ul>
<div class="sortPagiBar clearfix">
    {{if $countAllPages > 1}}
    <div class="bottom-pagination">
        <nav>
            <ul class="pagination">
                {{if $prevPage}}
                <li>
                    <a href="javascript:loadSeller({{$prevPage}})" aria-label="Prev">
                        <span aria-hidden="true">&laquo; Prev</span>
                    </a>
                </li>
                {{/if}}

                {{foreach from=$prevPages item=item}}
                <li><a href="javascript:loadSeller({{$item}})">{{$item}}</a></li>
                {{/foreach}}

                <li class="active"><a>{{$currentPage}}</a></li>

                {{foreach from=$nextPages item=item}}
                <li><a href="javascript:loadSeller({{$item}})">{{$item}}</a></li>
                {{/foreach}}

                {{if $nextPage}}
                <li>
                    <a href="javascript:loadSeller({{$nextPage}})" aria-label="Next">
                        <span aria-hidden="true">Next &raquo;</span>
                    </a>
                </li>
                {{/if}}
            </ul>
        </nav>
    </div>
    {{/if}}
</div>