<?php

require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/product/models/EstoreCategory.php';
require_once 'modules/content/models/Content.php';
require_once 'modules/user/models/User.php';
require_once 'modules/hs/models/HsCategory.php';
require_once 'modules/country/models/CountryCategory.php';
require_once 'modules/properties/models/PropertiesCategory.php';
require_once 'modules/message/models/MessageCategory.php';
require_once 'modules/banner/models/Banner.php';

class product_IndexController extends Nine_Controller_Action
{

    public function searchAction()
    {
        $objProduct = new Models_Product();
        $objUser = new Models_User();
        $objEstoreCategory = new Models_EstoreCategory();


        $key = $this->_getParam('key', false);
        $mode = $this->_getParam('cat', false);
        $aliasEstore = $this->_getParam('alias-estore', false);
        if (false == $key) {
            $key = null;
        }

        Nine_Common::getConfig($this->view);

        if (false != $aliasEstore) {

            $estore = $objUser->getByAlias($aliasEstore);
            $estoreCategory = $objEstoreCategory->getAllCategoryEnableNoParentByUserId($estore['user_id']);

            $numRowPerPage = 12;
            $currentPage = $this->_getParam("page", false);
            if ($currentPage == false) {
                $currentPage = 1;
            }
            $allProduct = $objProduct->getAllProductSearch($key, $estore['user_id'], $numRowPerPage, ($currentPage - 1) * $numRowPerPage);

            foreach ($allProduct as &$item) {
                $item ['icon'] = Nine_Function::getThumbImage(@$item ['icon'], 162, 162, false, false);
                $item['title'] = substr($item['title'], 0, 30);

                $item['title'] = substr($item['title'], 0, 43);
            }
            unset($item);
            $objBanner = new Models_Banner();
            $this->view->adv = $objBanner->getAllBanner(array('user_id' => $estore['user_id'], 'banner_category_gid' => 141, 'page' => 3, 'location' => $estore['template'] + 1), 'sorting');
            $this->setLayout('front-estore-' . $estore['template']);
            $templatePath = Nine_Registry::getModuleName() . '/views/estore-' . $estore['template'];

            $count = count($objProduct->getAllProductSearch($key, $estore['user_id']));
            $this->setPagination($numRowPerPage, $currentPage, $count);
            $this->view->allProduct = $allProduct;
            $this->view->estore = $estore;
            $this->view->estoreCategory = $estoreCategory;
            $this->view->count = $count;
            $this->view->html = $this->view->render($templatePath);
        } else {

            //$this->setLayout('front');
            $templatePath = Nine_Registry::getModuleName() . '/views/default';

            /*
             * Set paging info
             */
            $numRowPerPage = 20;
            $currentPage = $this->_getParam("page", false);
            if ($currentPage == false) {
                $currentPage = 1;
            }
            /*
             * Get list search data
             */
            $searchData = array();
            if ($mode == -1) {
                //Get list contents      
                $searchData['contents'] = Nine_Query::getListContents(array(
                    'title LIKE ?' => "%{$key}%"
                ), null, $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
                foreach ($searchData['contents'] as &$value) {
                    $value['title'] = mb_substr($value['title'], 0, 25);
                }
                unset($value);
                //Get list products
                $searchData['products'] = Nine_Query::getListProducts(array(
                    'title LIKE ?' => "%{$key}%"
                ), null, $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
            } else {
                //Get list products by product_category_gid
                $searchData['products'] = Nine_Query::getListProductsByCatId($mode, $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
            }
            foreach ($searchData['products'] as &$value) {
                $value['title'] = mb_substr($value['title'], 0, 25);
            }
            unset($value);

            $count = count($searchData);
            $this->setPagination($numRowPerPage, $currentPage, $count);
            $this->view->mode = $mode;
            $this->view->data = $searchData;
            $this->view->key = $key;
            /*
             * Set layout
             */
            Nine_Common::setLayoutAndView($this, 'Search: ' . $key, 'category', 'search');
        }
        $this->view->key = $key;
        //$templatePath .= '/search.' . Nine_Constant::VIEW_SUFFIX;
    }

    private function _redirectToNotFoundPage()
    {
        $this->_redirect("");
    }

    /*
     * Create order 
     */

    function OrderBy($orderby)
    {
        if ($orderby === 'date') {
            return array('created_date ASC');
        }
        if ($orderby === 'popularity') {
            return array('count_view ASC');
        }
        if ($orderby === 'price') {
            return array('price ASC');
        }
        if ($orderby === 'price-desc') {
            return array('price DESC');
        }
    }

    /**
     * Category detail
     */
    public function categoryDetailAction()
    {
        //Get alias
        $alias = $this->_getParam('alias', false);

        if ($alias !== false) {

            //Get paging
            $page = $this->_getParam('page', false);
            $itemPerPage = 12;
            $currentPage = 1;
            if ($page !== false) {
                $currentPage = $page;
            }

            //Get order by
            $orderBy = $this->_getParam('orderby', false);
            if ($orderBy === false) {
                $orderBy = 'date';
            }
            //$arrOrderBy = $this->OrderBy($orderBy);
            $arrOrderBy = array('sorting ASC');

            //Get category detail
            $objCat = @reset(Nine_Read::GetListData('Models_ProductCategory', array(
                'alias' => $alias
            )));
            $this->view->objCat = $objCat;

            //Get reverse id
            $arrAllCats = Nine_Read::GetListData('Models_ProductCategory');
            $reverObj = Nine_Common::GetReverseId($arrAllCats, $objCat['product_category_gid']);
            $reverObj['arr'][] = $objCat['product_category_gid'];
            $reverObj['str'] = $objCat['product_category_gid'] . ',' . $reverObj['str'];
            //Set page title
            $this->view->pageTitle = $objCat['name'];

            //Get list categories
            $this->view->arrCat = Nine_Read::GetListData('Models_ProductCategory', array(
                'parent_id' => 0
            ));

            //Get list product
            $arrProducts = Nine_Read::GetListData('Models_Product', array(
                'product_category_gid IN (?)' => explode(',', $reverObj['str'])
            ), $arrOrderBy, 'detail', $itemPerPage, ($currentPage - 1) * $itemPerPage);
            
            //Reset product name if longer than 1 row
            foreach ($arrProducts as &$value) {
                if(strlen($value['title']) > 23){
                    $value['title'] = mb_substr($value['title'], 0, 19) . '...';
                }
            }
            unset($value);
            $this->view->arrProducts = $arrProducts;

            $arrAllProducts = Nine_Read::GetListData('Models_Product', array(
                'product_category_gid' => $objCat['product_category_gid']
            ), $arrOrderBy, 'detail');
            

            $this->setPagination($itemPerPage, $currentPage, count($arrAllProducts));

            //Get config
            $config = Nine_Common::getConfig($this->view);
            $this->view->objConfig = $config;

            //Get project
            $arrProjects = Nine_Read::GetListData('Models_Project', array(
                'displayMode' => 2
            ));
            //Get list products in project
            foreach ($arrProjects as &$item){
                //List project id
                $tmp = explode('||', $item['product_id']);
                $arrProducts = array();
                for($i = 0 ; $i < count($tmp) ; $i ++){
                    array_push($arrProducts, @reset(Nine_Read::GetListData('Models_Product', array(
                        'product_gid' => (int) $tmp[$i]
                    ), array(), 'detail')));
                }
                $item['products'] = $arrProducts;
            }
            $this->view->arrProjects = $arrProjects;

            //Paging info
            $this->view->countAllProducts = count($arrAllProducts);
            $this->view->from = $currentPage > 1 ? $itemPerPage * $currentPage - 9 : 1;
            $this->view->to = count($arrAllProducts) > $itemPerPage ? $itemPerPage * $currentPage : count($arrAllProducts);

            //Order by info
            $this->view->orderby = $orderBy;

            /*
             * Set layout file and template
             */
            Nine_Common::setLayoutAndView($this, $objCat['name'], 'category', 'category-detail-v2');
        }
    }


    /*
     * Product detail
     */
    public function productDetailAction()
    {

        try {
            //Get id
            $alias = $this->_getParam('alias', false);
            $objProduct = array();
            if($alias !==  false){
                //Get product detail
                $objProduct = @reset(Nine_Read::GetListData('Models_Product', array(
                    'alias' => $alias
                ), array(), 'detail'));

                //Convert list slide to array
                $objProduct['slide'] = explode('||', $objProduct ['slide']);
                $objProduct['arrImages2'] = explode('||', $objProduct ['images2']);
                $url = Nine_Route::url();
                foreach ($objProduct ['arrImages2'] as &$item){
                    $item = $url['path'] . $item;
                }
                //Get list project
                if($objProduct['show_project']){
                    $objProduct['arrProjects'] = $this->GetListProject($objProduct['product_gid']);
                }

                $this->view->objDetail = $objProduct;


                //Get cat detail
                $objCategory = @reset(Nine_Read::GetListData('Models_ProductCategory', array(
                    'product_category_gid' => $objProduct['product_category_gid']
                ), array(), 'index'));
                $this->view->objCategory = $objCategory;

                //Get same category products
                $arrRelatedProducts = Nine_Read::GetListData('Models_Product', array(
                    'product_category_gid' => $objProduct['product_category_gid']
                ), array(), 'detail');
                $this->view->arrRelatedProducts = $arrRelatedProducts;

                //Get config
                $config = Nine_Common::getConfig($this->view);
                $this->view->config = $config;

                //Get url
                $url = Nine_Route::url();
                $this->view->path = $url['path'];
            }
            /* SET LAYOUT AND TEMPLATE */
            Nine_Common::setLayoutAndView($this, $objProduct['title'], 'detail', 'detail');
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /*
     * Get list projects
     */
    function GetListProject($gid){
        $arr = array();
        //Get projects has product_id like gid
        $arrProjects = Nine_Read::GetListData('Models_Project', array(
            'product_id LIKE ?' => '%'.$gid.'%'
        ), array(), 'detail');
        foreach ($arrProjects as $item){
            $arrProductId = explode('||', $item['product_id']);
            if(in_array($gid, $arrProductId)){
                array_push($arr, $item);
            }
        }
        return $arr;
    }
    /*
     * Get cat
     */

    public function GetCat($alias)
    {
        if ($alias == 'hot-product' || $alias == 'san-pham-hot') {
            $cat['name'] = Nine_Language::translate("Sản phẩm hot");
            $cat['product_category_gid'] = 0;
            $cat['url'] = '';
        } else if ($alias == 'sale-product' || $alias == 'san-pham-sale') {
            $cat['name'] = Nine_Language::translate("Sản phẩm sale");
            $cat['product_category_gid'] = 0;
            $cat['url'] = '';
        } else if ($alias == 'special-product' || $alias == 'san-pham-special') {
            $cat['name'] = Nine_Language::translate("Sản phẩm đặc biệt");
            $cat['product_category_gid'] = 0;
            $cat['url'] = '';
        } else if ($alias == 'new-product' || $alias == 'san-pham-moi') {
            $cat['name'] = Nine_Language::translate("Sản phẩm mới");
            $cat['product_category_gid'] = 0;
            $cat['url'] = '';
        } else {
            $cat = @reset(Nine_Query::getListProductCategory(array(
                'alias' => $alias,
            )));
            $cat['detail'] = Nine_Query::getListProductCategory(array(
                'parent_id' => $cat['product_category_gid']
            ), null, 5, null);
        }

        $this->view->cat = $cat;
        return $cat;
    }

    /*
     * Get all product
     */

    public function GetAllProduct($alias, $cat, $numRowPerPage, $tempPage, $listId)
    {
        if ($alias == 'hot-product' || $alias == 'san-pham-hot') {

            $allProduct = Nine_Query::getListProducts(array(
                'tinnhanh' => 1,
                'check_product' => 2
            ), null, $numRowPerPage, ($tempPage - 1) * $numRowPerPage);
            $countProduct = Nine_Query::getListProducts(array(
                'tinnhanh' => 1,
                'check_product' => 2
            ));
            /*
             * Paging
             */
            $this->setPagination($numRowPerPage, $tempPage, count($countProduct));
        } else if ($alias == 'sale-product' || $alias == 'san-pham-sale') {
            $allProduct = Nine_Query::getListProducts(array(
                'sale' => 1,
                'check_product' => 2
            ), null, $numRowPerPage, ($tempPage - 1) * $numRowPerPage);
            $countProduct = Nine_Query::getListProducts(array(
                'sale' => 1,
                'check_product' => 2
            ));
            /*
             * Paging
             */
            $this->setPagination($numRowPerPage, $tempPage, count($countProduct));
        } else if ($alias == 'special-product' || $alias == 'san-pham-special') {
            $allProduct = Nine_Query::getListProducts(array(
                'dacbiet' => 1,
                'check_product' => 2
            ), null, $numRowPerPage, ($tempPage - 1) * $numRowPerPage);
            $countProduct = Nine_Query::getListProducts(array(
                'dacbiet' => 1,
                'check_product' => 2
            ));
            /*
             * Paging
             */
            $this->setPagination($numRowPerPage, $tempPage, count($countProduct));
        } else if ($alias == 'new-product' || $alias == 'san-pham-moi') {
            $allProduct = Nine_Query::getListProducts(array(
                'check_product' => 2
            ), array('created_date ASC'), $numRowPerPage, ($tempPage - 1) * $numRowPerPage);
            $countProduct = Nine_Query::getListProducts(array(
                'check_product' => 2
            ), array('created_date ASC'));
            /*
             * Paging
             */
            $this->setPagination($numRowPerPage, $tempPage, count($countProduct));
        } else {

            $allProduct = Nine_Query::getListProducts(array(
                'product_category_gid IN (?)' => explode(",", $listId),
                'check_product' => 2
            ), null, $numRowPerPage, ($tempPage - 1) * $numRowPerPage);
            $countProduct = Nine_Query::getListProducts(array(
                'product_category_gid IN (?)' => explode(",", $listId),
                'check_product' => 2
            ));
            /*
             * Paging
             */
            $this->setPagination($numRowPerPage, $tempPage, count($countProduct));
        }

        $this->view->allProduct = $allProduct;
        return $allProduct;
    }

    public
    function loadSellerAction()
    {
        $this->_helper->layout->disableLayout();
        $objUser = new Models_User();

        $productCategoryGid = $this->_getParam('product_category_gid', false);
        $currentPage = $this->_getParam('page', false);
        $ids = $this->_getParam('ids', false);

        if ($ids != false && $ids != '') {
            $users = $objUser->getAllUsersWithGroup(array('ids' => $ids), 'product_category_gid ASC', 8, ($currentPage - 1) * 8);
            $count = count($objUser->getAllUsersWithGroup(array('ids' => $ids)));

            $this->setPagination(8, $currentPage, $count);
            $this->view->users = $users;
            $this->view->count = $count;
            $this->view->ids = $ids;
            $this->view->firstItemInPage = ($currentPage - 1) * 8 + 1;
            $this->view->lastItemInPage = ($currentPage - 1) * 8 + count($objUser->getAllUsersWithGroup(array('ids' => $ids), 'product_category_gid ASC', 8, ($currentPage - 1) * 8));
        }
    }

    public
    function loadProductCategoryGetidsAction()
    {
        $this->_helper->layout->disableLayout();
        $objProduct = new Models_Product();
        $objProductCategory = new Models_ProductCategory();
        $objUser = new Models_User();

        $productCategoryGid = $this->_getParam('product_category_gid', false);


        $category = $objProductCategory->getCategoryById($productCategoryGid);
        $gidCategory = $category['gid_string'];
        $condition['str_category'] = $gidCategory;
        $ids = array();
        $allProduct = $objProduct->getAllProductByArrCat($condition);
        foreach ($allProduct as &$item) {
            $user = $objUser->getByUserId($item['user_id']);
            $item['name_company'] = $user['name_company'];
            if (!in_array($item['user_id'], $ids)) {
                $ids[] = $item['user_id'];
            }
        }
        unset($item);
        echo trim(implode(",", $ids));
        die;
    }

    public
    function loadProductCategoryAction()
    {
        $this->_helper->layout->disableLayout();
        $objProduct = new Models_Product();
        $objProductCategory = new Models_ProductCategory();
        $objUser = new Models_User();

        $productCategoryGid = $this->_getParam('product_category_gid', false);
        $currentPage = $this->_getParam("page", false);
        $displayNum = $this->_getParam('displayNum', false);
        $displaySort = $this->_getParam('displaySort', false);
        $suplierCountry = $this->_getParam('suplierCountry', false);

        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        } else {
            $numRowPerPage = 8;
        }

        /**
         * Get order by
         */
        if (false !== $displaySort) {
            $orderBy = $displaySort;
        } else {
            $orderBy = 'title';
        }


        $category = $objProductCategory->getCategoryById($productCategoryGid);
        $gidCategory = $category['gid_string'];

        $condition['str_category'] = $gidCategory;

        if (@$suplierCountry != '0') {
            $condition['xuat_xu'] = $suplierCountry;
        }

        $allProduct = $objProduct->getAllProductByArrCat($condition, $orderBy . ' ASC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
        foreach ($allProduct as &$item) {
            $user = $objUser->getByUserId($item['user_id']);
            $item['name_company'] = $user['pcompany_name'];
            $item['icon'] = Nine_Function::getThumbImage(@$item['icon'], 250, 250, false, false, false);
        }
        unset($item);
        $count = count($objProduct->getAllProductByArrCat($condition));
        $this->setPagination($numRowPerPage, $currentPage, $count);

        $this->view->allProduct = $allProduct;
        $this->view->displayNum = $numRowPerPage;
        $this->view->displaySort = $orderBy;
    }

    /*
     * Get parent cat detail
     */

    function GetParentCat($product_category_gid)
    {
        /*
         * Get all cats
         */
        $allCat = Nine_Query::getListProductCategory(array());

        /*
         * Get list product category id
         */
        $listReverseId = Nine_Query::getListProductCategroryReverseId($allCat, $product_category_gid);
        //echo "list reverse: " . $listReverseId;
        //echo 'a: ' . substr($listReverseId, 0, strlen($listReverseId) - 2) . "-";
        $listReverseId = explode(",", substr($listReverseId, 0, strlen($listReverseId) - 2));

        //echo 'last: ' . end($listReverseId);
        $listId = Nine_Query::getListProductCategroryId($allCat, end($listReverseId));
        if ($listId == "") {
            $listId .= ", " . end($listReverseId);
        } else {
            $listId .= end($listReverseId);
        }
        //echo 'list: ' . $listId;

        /*
         * Get root product category detail
         */
        $rootCat = @reset(Nine_Query::getListProductCategory(array(
            'product_category_gid' => end($listReverseId)
        )));
        $url = Nine_Route::url();
        if (count($rootCat['images']) > 0) {
            foreach ($rootCat['images'] as &$rootCatItem) {
                $rootCatItem = $url['path'] . $rootCatItem;
            }
            unset($rootCatItem);
        }
        $this->view->rootCat = $rootCat;

        // Get list sub category of root parent
        $listCategories = Nine_Query::getListProductCategory(array(
            'parent_id' => end($listReverseId)
        ));
        foreach ($listCategories as &$value) {
            $value['subCat'] = Nine_Query::getListProductCategory(array(
                'parent_id' => $value['product_category_gid']
            ));
        }
        unset($value);
        $this->view->childCategory = $listCategories;

        /*
         * get list product
         */
        $allProduct = Nine_Query::getListProducts(array(
            'product_category_gid IN (?)' => explode(",", $listId),
            'check_product' => 2,
            'dacbiet' => 1
        ));
        $this->view->leftPanelSpecial = $allProduct;
    }

    /*
     * Get special products
     */

    private
    function GetSpecialProducts()
    {
        $specialProducts = Nine_Query::getListProducts(array(
            'dacbiet' => 1
        ), null, 10, null);
        $this->view->leftPanelSpecial = $specialProducts;
    }

    public
    function productTppDetailAction()
    {
        $objProduct = new Models_Product();
        $objUser = new Models_User();
        $objCountry = new Models_CountryCategory();

        $this->setLayout('front-default-category');
        $alias = $this->_getParam('alias', false);

        if (false != $alias) {
            $tpp = $objCountry->getCategoryByAlias($alias);

            $numRowPerPage = 20;
            $currentPage = $this->_getParam("page", false);
            if ($currentPage == false) {
                $currentPage = 1;
            }

            $allProduct = $objProduct->getAllProductByTpp($tpp['country_category_gid'], $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
            foreach ($allProduct as &$item) {
                $item ['icon'] = Nine_Function::getThumbImage(@$item ['icon'], 162, 162, false, false);
                $user = $objUser->getByUserId($item['user_id']);
                $item['title'] = Nine_Function::subStringAtBlank(trim(strip_tags($item['title'])), 15);
                $item['name_company'] = $user['name_company'];
            }
            unset($item);

            $count = count($objProduct->getAllProductByTpp($tpp['country_category_gid']));
            $this->setPagination($numRowPerPage, $currentPage, $count);
            $this->view->tpp = $tpp;
            $this->view->allProduct = $allProduct;
        }
    }

}
