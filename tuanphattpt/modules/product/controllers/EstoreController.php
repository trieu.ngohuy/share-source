<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/user/models/User.php';
require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/product/models/EstoreCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/properties/models/PropertiesCategory.php';
require_once 'modules/country/models/CountryCategory.php';
require_once 'modules/hs/models/HsCategory.php';

class product_EstoreController extends Nine_Controller_Action
{
    public function manageProductAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $this->setLayout('estore');

        $objProduct = new Models_Product();
        $objLang    = new Models_Lang();
        $objCat     = new Models_EstoreCategory();

        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_product', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $this->view->headTitle(Nine_Language::translate('manage Product '));



        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
        if($currentPage == false){
            $currentPage = 1;
            $this->session->productDisplayNum = null;
            $this->session->productCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objProduct->update(array('check_product' => $value), array('product_gid=?' => $id));
            $this->session->productMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("Product Order is edited successfully")
            );
        }
		
        $objEstoreCat = new Models_EstoreCategory();
        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->productDisplayNum;
        } else {
            $this->session->productDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->productCondition;
        } else {
            $this->session->productCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        if(@$condition['keyword'] != ''){
            $condition['keyword'] = $objProduct->convert_vi_to_en($condition['keyword']);
            $condition['keyword'] = preg_replace("/( )/", '-', $condition['keyword']);
        }

        /**
         * Get all categories
         */
        $condition_cat = array();
        $condition_cat['user_id'] = $user['user_id'];
        
        
        $allCatEstores = Nine_Query::getListEstoreCategory(array(
                'user_id' => $user['user_id'],
                'enabled' => 1,
                'genabled' => 1
            ));
        $this->view->allCats = $allCatEstores;
        
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_product', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                if(isset($allLangs[$index])){
                	unset($allLangs[$index]);	
                }
            }
        }

        /**
         * Condition follow by User ID (Estore)
         */
        $condition['user_id'] = $user['user_id'];

        /**
         * Get all products
         */
        $numRowPerPage= $numRowPerPage;
        
        $allProduct = $objProduct->getAllProductEstore($condition, array('sorting ASC', 'product_gid DESC', 'product_id ASC'),
            $numRowPerPage,
            ($currentPage - 1) * $numRowPerPage
        );
//        echo '<pre>';print_r($allProduct);die;
        /**
         * Count all products
         */
        $count = count($objProduct->getallProduct($condition));
        /**
         * Format
         */
        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allProduct[0]['product_gid'];
        foreach ($allProduct as $index=>$product) {
            /**
             * Change date
             */
        	$product['estore_category'] = current($objEstoreCat->getByColumns(array('estore_category_gid'=>$product['estore_category_gid']))->toArray());
            if (0 != $product['created_date']) {
                $product['created_date'] = date($config['dateFormat'], $product['created_date']);
            } else {
                $product['created_date'] = '';
            }
            if (0 != $product['publish_up_date']) {
                $product['publish_up_date'] = date($config['dateFormat'], $product['publish_up_date']);
            } else {
                $product['publish_up_date'] = '';
            }
            if (0 != $product['publish_down_date']) {
                $product['publish_down_date'] = date($config['dateFormat'], $product['publish_down_date']);
            } else {
                $product['publish_down_date'] = '';
            }
            if ($tmpGid != $product['product_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $product['product_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $product;
                $tmp2['hit'] = 0;
            }
            $tmp2['hit']    += $product['hit'];
            $tmp2['langs'][]  = $product;
            /**
             * Final element
             */
            if ($index == count($allProduct) - 1) {
                $tmp[] = $tmp2;
            }
            
        }
        $allProduct = $tmp;
        $export = $this->_getParam('export', false);
        if($export != false){
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $style = "style = 'border: 1px solid'";

            if(Nine_Language::getCurrentLangId() == 1){
                $header = "<tr>";
                $header .= "<td $style> <b> </b></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> <b>Status </b></td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style>  </td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 1 </td>";
                $header .= "<td $style> Approved Pending </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style> </td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 2 </td>";
                $header .= "<td $style> Approved </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 3 </td>";
                $header .= "<td $style> Not Approved </td>";
                $header .= "</tr>";
                $textHeader = array(
                    "<b>No</b>",
                    "<b>Category</b>",
                    "<b>Estore Upload</b>",
                    "<b>Created Date</b>",
                    "<b>Status</b>",
                    "<b>Title</b>"
                );
            }else{
                $header = "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> <b>T?nh Tr?ng S?n Ph?m </b></td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style>  </td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 1 </td>";
                $header .= "<td $style> �ang Ch? Duy?t </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style>  </td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 2 </td>";
                $header .= "<td $style> �? Duy?t </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 3 </td>";
                $header .= "<td $style> Kh�ng Duy?t </td>";
                $header .= "</tr>";

                $textHeader = array(
                    mb_convert_encoding("<b>S? Th? T?</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Danh M?c S?n Ph?m</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Gian H�ng Upload</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ng�y Kh?i T?o</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>T?nh Tr?ng</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ti�u �?</b>",'HTML-ENTITIES','UTF-8')
                );
            }

            $header .= "<tr>";
            foreach ($textHeader as $text) {
                $header .= "<td $style>" . utf8_encode($text) . "</td>";
            }
            $header .= "</tr>";

            $content = '';
            $no = 1;

            foreach ($allProduct as $item) {
                $content .="<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['cname'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['full_name'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['check_product'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['title'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .="</tr>";
                $no++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-product-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();

        }
        

        /**
         * Set values for tempalte
         */
//        echo print_r($condition); die;
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allProduct = $allProduct;
        $this->view->productMessage = $this->session->productMessage;
        $this->session->productMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_product');
        $this->view->EditPermisision = $this->checkPermission('edit_product');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_product');
        $this->view->DeletePermisision = $this->checkPermission('delete_product');
        $this->view->allLangs = $allLangs;

        $this->view->menu = array(
            0=>'product',
            1=>'manager-product'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-product-hunt',
                'url'	=>	Nine_Registry::getBaseUrl().'estore/manage-product/' . $user['alias'] . '.html',
                'name'	=>	Nine_Language::translate('Manager Product')
            )

        );

        $this->view->alias = $user['alias'] . '.html';
        $this->view->username = $user['alias'];
    }

    public function newProductAction()
    {
    	
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $this->setLayout('estore');

        $objCat = new Models_ProductCategory();
        $objEstoreCat = new Models_EstoreCategory();
        $objLang = new Models_Lang();
        $objProduct = new Models_Product();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_product', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data = $this->_getParam('data', false);
        $user_id = $this->_getParam('id', Nine_Registry::getLoggedInUserId());
        /**
         * Get all categories
         */
        $user['product_category_gid'] = explode('||', trim(@$user['product_category_gid'], '||'));
        $user['product_category_gid'] = implode(',', $user['product_category_gid']);

        
        $allCatEstores = Nine_Query::getListEstoreCategory(array(
                'user_id' => $user['user_id'],
                'enabled' => 1,
                'genabled' => 1
            ));
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        // foreach ($allLangs as $index => $lang) {
        //     if (false == $this->checkPermission('new_product', null, $lang['lang_id'])) {
        //         /**
        //          * Clear data
        //          */
        //         unset($data[$lang['lang_id']]);
        //         /**
        //          * Disappear this language
        //          */
        //         if(isset($allLangs[$index])){
        //         	unset($allLangs[$index]);	
        //         }
        //     }
        // }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new product
             */
        	if(isset($data['color'])){
                $data['color'] = '||'.implode("||", $data['color']).'||';
            }
        	if(isset($data['thanhtoan'])){
                $data['thanhtoan'] = '||'.implode("||", $data['thanhtoan']).'||';
            }
            $newProduct = $data;
            
            $category = current($objEstoreCat->getByColumns(array('estore_category_gid'=>$newProduct['estore_category_gid']))->toArray());
            $newProduct['product_category_gid'] = $category['parent_id'];
            
            $newProduct['created_date'] = time();
            $newProduct['user_id'] = $user['user_id'];
            /**
             * Change format date
             */
            if (null == $newProduct['publish_up_date']) {
                unset($newProduct['publish_up_date']);
            } else {
                $tmp = explode('/', $newProduct['publish_up_date']);
                $newProduct['publish_up_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (null == $newProduct['publish_down_date']) {
                unset($newProduct['publish_down_date']);
            } else {
                $tmp = explode('/', $newProduct['publish_down_date']);
                $newProduct['publish_down_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            /**
             * Sorting
             */
            if (null == $newProduct['sorting']) {
                unset($newProduct['sorting']);
            }
            if (false == $this->checkPermission('new_product', null, '*')) {
                $newProduct['genabled']          = 0;
                $newProduct['publish_up_date']   = null;
                $newProduct['publish_down_date'] = null;
                $newProduct['sorting']           = 1;
            }

            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newProduct[$lang['lang_id']]['intro_text'], $newProduct[$lang['lang_id']]['full_text'])= Nine_Function::splitTextWithReadmore($newProduct[$lang['lang_id']]['full_text']);
                if($newProduct[$lang['lang_id']]['alias'] == ""){
                    $newProduct[$lang['lang_id']]['alias'] = $objProduct->convert_vi_to_en($newProduct[$lang['lang_id']]['title']);
                    $newProduct[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newProduct[$lang['lang_id']]['alias']));
                }
                
            }
            if($newProduct[1]['alias'] == ''){
            	$newProduct[1]['alias'] = $newProduct[2]['alias'];
            	$newProduct[1]['title'] = $newProduct[2]['title'];
            }else if($newProduct[2]['alias'] == ''){
            	$newProduct[2]['alias'] = $newProduct[1]['alias'];
            	$newProduct[2]['title'] = $newProduct[1]['title'];
            }

            /**
             * Remove empty images
             */
            if (is_array($newProduct['images'])) {
                foreach ($newProduct['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newProduct['images'][$index]);
                    } else {
                        $newProduct['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newProduct['icon'] = Nine_Function::getImagePath($newProduct['icon']);
            $newProduct['images'] = implode('||', $newProduct['images']).'||';
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > $newProduct['sorting']) {
                    $newProduct['sorting'] = 1;
                }
                $objProduct->increaseSorting($newProduct['sorting'], 1);

                if($newProduct['publish_up_date'] == ''){
                    $newProduct['publish_up_date'] = date();
                }
             	$gid = $objProduct->insert($newProduct);
                
                $product = $objProduct->getByColumns(array('product_gid = ?' => $gid))->toArray();
                foreach ($product as $tem){
                	$update = array('alias'=>$tem['alias']);
                	$objProduct->update($update, array('product_id=?' => $tem['product_id']));
                }
                
                /**
                 * Message
                 */
                $this->session->productMessage =  array(
                    'success' => true,
                    'message' => Nine_Language::translate('Product is created successfully.')
                );
//                echo "<pre>HERE";die;
                $this->_redirect('estore/manage-product/' . $user['alias'] . '.html');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1,'color'=>array(),'thanhtoan'=>array());
            $data['user_id'] = $user_id;
        }
        /**
         * Prepare for template
         */
        $this->view->allCatEstores = $allCatEstores;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Product'));
        $this->view->fullPermisison = $this->checkPermission('new_product', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_product');
        $this->view->menu = array(
            0=>'product',
            1=>'new-product'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-product-hunt',
                'url'	=>	Nine_Registry::getBaseUrl().'estore/manage-product/' . $user['alias'] . '.html',
                'name'	=>	Nine_Language::translate('Manager Product')
            ),
            1=>array(
                'icon' 	=> 	'fa-plus',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('New Product')
            )

        );

        $objCategoryProperties = new Models_PropertiesCategory;

        $unitProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(45);
        $this->view->unitProperties = $unitProperties;
        
        $colorProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(39);
        $this->view->colorProperties = $colorProperties;
        
        $trongluongProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(27);
        $this->view->trongluongProperties = $trongluongProperties;
        
        $dvtProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(53);
        $this->view->dvtProperties = $dvtProperties;
        
        $thanhtoanProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(515);
        $this->view->thanhtoanProperties = $thanhtoanProperties;
        
        $donggoiProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(67);
        $this->view->donggoiProperties = $donggoiProperties;
        
        $caseProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(53);
        $this->view->caseProperties = $caseProperties;
        
        $timeProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(549);
        $this->view->timeProperties = $timeProperties;

        $objCountry = new Models_CountryCategory();
        $allCountry = $objCountry->getAllCategoriesParentNull();
        $this->view->allCountry = $allCountry;

        $objHs = new Models_HsCategory();
    	$allHsQt = $objHs->getAllByParentID(array('parent_id'=>25));
    	
        $this->view->allHsQt = $allHsQt;
        
        
        
        $allHsVn = $objHs->getAllByParentID(array('parent_id'=>27));
        $this->view->allHsVn = $allHsVn;

        $this->view->alias = $user['alias'] . '.html';
    }

    public function editProductAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $this->setLayout('estore');

        $objProduct = new Models_Product();
        $objEstoreCat     = new Models_EstoreCategory();
        $objCat = new Models_ProductCategory();
        $objLang    = new Models_Lang();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_product', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false);
        if (false == $gid) {
            $this->_redirect('estore/manage-product/' . $user['alias'] . '.html');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_product', null, '*'))
            ||  (false != $lid && false == $this->checkPermission('edit_product', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data   = $this->_getParam('data', false);
		

        if (isset($user['category_id']) && is_array($user['category_id'])) {
            $user['category_id'] = '||' . implode("||", $user['category_id']) . '||';
        } else {
            $user['category_id'] = '';
        }


        /**
         * Get all categories
         */
        $allCatEstores = Nine_Query::getListEstoreCategory(array(
                'user_id' => $user['user_id'],
                'enabled' => 1,
                'genabled' => 1
            ));

        /**
         * Get all product languages
         */

        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allProductLangs = $objProduct->setAllLanguages(true)->getByColumns(array('product_gid=?' => $gid, 'user_id=?' => $user['user_id']))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('edit_product', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                if(isset($allLangs[$index])){
                	unset($allLangs[$index]);	
                }
                unset($allProductLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }
		
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new product
             */
            if(isset($data['color'])){
                $data['color'] = '||'.implode("||", $data['color']).'||';
            }
        	if(isset($data['thanhtoan'])){
                $data['thanhtoan'] = '||'.implode("||", $data['thanhtoan']).'||';
            }

            $newProduct = $data;
			$category = current($objEstoreCat->getByColumns(array('estore_category_gid'=>$newProduct['estore_category_gid']))->toArray());
            $newProduct['product_category_gid'] = $category['parent_id'];
            if (false == $this->checkPermission('new_product', null, '*')) {
                unset($newProduct['genabled']);
                unset($newProduct['publish_up_date']);
                unset($newProduct['publish_down_date']);
                unset($newProduct['sorting']);
            }

            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newProduct[$lang['lang_id']]['intro_text'], $newProduct[$lang['lang_id']]['full_text'])= Nine_Function::splitTextWithReadmore($newProduct[$lang['lang_id']]['full_text']);
                if($newProduct[$lang['lang_id']]['alias'] == ""){
                    $newProduct[$lang['lang_id']]['alias'] = $objProduct->convert_vi_to_en($newProduct[$lang['lang_id']]['title']);
                    $newProduct[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newProduct[$lang['lang_id']]['alias']));
                }
            }
        	if($newProduct[1]['alias'] == ''){
            	$newProduct[1]['alias'] = $newProduct[2]['alias'];
            	$newProduct[1]['title'] = $newProduct[2]['title'];
            }else if($newProduct[2]['alias'] == ''){
            	$newProduct[2]['alias'] = $newProduct[1]['alias'];
            	$newProduct[2]['title'] = $newProduct[1]['title'];
            }
            /**
             * Remove empty images
             */
            if (is_array($newProduct['images'])) {
                foreach ($newProduct['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newProduct['images'][$index]);
                    } else {
                        $newProduct['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newProduct['images'] = implode('||', $newProduct['images']);
            if (null == $newProduct['icon']) {
                unset($newProduct['icon']);
            } else {
                $newProduct['icon'] = Nine_Function::getImagePath($newProduct['icon']);
            }
            try {
                /**
                 * Update
                 */
                $objProduct->update($newProduct, array('product_gid=?' => $gid));
            	$product = $objProduct->getByColumns(array('product_gid = ?' => $gid))->toArray();
                foreach ($product as $tem){
                	$update = array('alias'=>$tem['alias']);
                	$objProduct->update($update, array('product_id=?' => $tem['product_id']));
                }
                /**
                 * Message
                 */
                $this->session->productMessage =  array(
                    'success' => true,
                    'message' => Nine_Language::translate('Product is updated successfully.')
                );

                $this->_redirect('estore/manage-product/' . $user['alias'] . '.html');
            } catch (Exception $e) {

                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allProductLangs);

            if (false == $data) {
                $this->session->productMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("Product doesn't exist.")
                );
                $this->_redirect('estore/manage-product/' . $user['alias'] . '.html');
            }
            /**
             * Change date
             */
            if (0 != $data['publish_up_date']) {
                $data['publish_up_date'] = date('d/m/Y', $data['publish_up_date']);
            } else {
                $data['publish_up_date'] = '';
            }
            if (0 != $data['publish_down_date']) {
                $data['publish_down_date'] = date('d/m/Y', $data['publish_down_date']);
            }
            else {
                $data['publish_down_date'] = '';
            }
            /**
             * Format image
             */
            $data['images'] = explode('||', trim($data['images'].'||'));
            if (! is_array($data['images'])) {
                $data['images'] = array();
            }
            $data['images'] = array_pad($data['images'], 50, null);
            /**
             * Get all lang products
             */
            foreach ($allProductLangs as $product) {
                /**
                 * Rebuild readmore DIV
                 */
                $product['full_text'] = Nine_Function::combineTextWithReadmore($product['intro_text'], $product['full_text']);
                $data[$product['lang_id']] = $product;
            }

            /**
             * Add deleteable field
             */
            if (null != @$data['estore_category_gid']) {
                $cat = @reset($objEstoreCat->getByColumns(array('estore_category_gid' => $data['estore_category_gid']))->toArray());
                $data['product_deleteable'] = @$cat['product_deleteable'];
            }

            if (null != @$data['product_category_gid']) {
                $cat = @reset($objCat->getByColumns(array('product_category_gid' => $data['product_category_gid']))->toArray());
                $data['product_deleteable'] = @$cat['product_deleteable'];
            }
        }
        $data['color'] = explode("||", trim($data['color'].'||'));
        $data['thanhtoan'] = explode("||", trim($data['thanhtoan'].'||'));
        /**
         * Prepare for template
         */
        $this->view->allCatEstores = $allCatEstores;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Product'));
        $this->view->fullPermisison = $this->checkPermission('edit_product', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_product');
        $this->view->menu = array(
            0=>'product',
            1=>'manager-product'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-product-hunt',
                'url'	=>	Nine_Registry::getBaseUrl().'estore/manage-product/' . $user['alias'] . '.html',
                'name'	=>	Nine_Language::translate('Manager Product')
            ),
            1=>array(
                'icon' 	=> 	'fa-pencil',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('Edit Product')
            )

        );

        $objCategoryProperties = new Models_PropertiesCategory;

        $unitProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(45);
        $this->view->unitProperties = $unitProperties;
        
        $colorProperties =$objCategoryProperties->getAllCategoriesOnparentkeyUser(39);
        $this->view->colorProperties = $colorProperties;
        
        $trongluongProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(27);
        $this->view->trongluongProperties = $trongluongProperties;
        
        $dvtProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(53);
        $this->view->dvtProperties = $dvtProperties;
        
        $thanhtoanProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(515);
        $this->view->thanhtoanProperties = $thanhtoanProperties;
        
        $donggoiProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(67);
        $this->view->donggoiProperties = $donggoiProperties;
        
        $caseProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(53);
        $this->view->caseProperties = $caseProperties;
        
        $timeProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(549);
        $this->view->timeProperties = $timeProperties;

        $objCountry = new Models_CountryCategory();
        $allCountry = $objCountry->getAllCategoriesParentNull();
        $this->view->allCountry = $allCountry;

        $objHs = new Models_HsCategory();
    	$allHsQt = $objHs->getAllByParentID(array('parent_id'=>25));
    	
        $this->view->allHsQt = $allHsQt;
        
        
        
        $allHsVn = $objHs->getAllByParentID(array('parent_id'=>27));
        $this->view->allHsVn = $allHsVn;

        $this->view->alias = $user['alias'] . '.html';
    }

    public function deleteProductAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $objProduct = new Models_Product();
        $objCat = new Models_EstoreCategory();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_product')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid = $this->_getParam('gid', false);

        if (false == $gid) {
            $this->_redirect('estore/manage-product/' . $user['alias'] . '.html');
        }

        $gids = explode('_', trim($gid, '_'));

        try {
            foreach ($gids as $gid) {

                $product = @reset($objProduct->getByColumns(array('product_gid=?'=>$gid, 'user_id=?'=>$user['user_id']))->toArray());
                $cat = @reset($objCat->getByColumns(array('estore_category_gid=?'=>$product['estore_category_gid']))->toArray());
                if ( 0 == $cat['product_deleteable']){
                    $this->session->productMessage = array(
                        'success' => false,
                        'message' => Nine_Language::translate('Can NOT delete product ('. $gid .'). Please try again')
                    );
                    $this->_redirect('estore/manage-product/' . $user['alias'] . '.html');
                }
                else {
                    $objProduct->delete(array('product_gid=?' => $gid));
                }

            }
            $this->session->productMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Product is deleted successfully')
            );
        } catch (Exception $e) {
            $this->session->productMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT delete this product. Please try again')
            );
        }
        $this->_redirect('estore/manage-product/' . $user['alias'] . '.html');
    }

    public function tinnhanhProductAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $objProduct = new Models_Product();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);

        if (false == $gid) {
            $this->_redirect('estore/manage-product/' . $user['alias'] . '.html');
        }

        $gids = explode('_', trim($gid, '_'));

        /**
         * Change general status
         * Check full permission
         */
        if (false == $this->checkPermission('edit_product', null, '*')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        try {
            foreach ($gids as $gid) {
                $objProduct->update(array('tinnhanh' => 1), array('product_gid=?' => $gid, 'user_id=?' => $user['user_id']));
            }
            $this->session->productMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Product is Flashes successfully')
            );
        } catch (Exception $e) {
            $this->session->productMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT activate this product. Please try again')
            );
        }

        $this->_redirect('estore/manage-product/' . $user['alias'] . '.html');
    }

    public function distinnhanhProductAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $objProduct = new Models_Product();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);

        if (false == $gid) {
            $this->_redirect('estore/manage-product/' . $user['alias'] . '.html');
        }

        $gids = explode('_', trim($gid, '_'));

        /**
         * Change general status
         * Check full permission
         */
        if (false == $this->checkPermission('edit_product', null, '*')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        try {
            foreach ($gids as $gid) {
                $objProduct->update(array('tinnhanh' => 0), array('product_gid=?' => $gid, 'user_id=?' => $user['user_id']));
            }
            $this->session->productMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Product is Dis Flashes successfully')
            );
        } catch (Exception $e) {
            $this->session->productMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT activate this product. Please try again')
            );
        }

        $this->_redirect('estore/manage-product/' . $user['alias'] . '.html');
    }

    public function manageCategoryAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $this->setLayout('estore');

        $objLang    = new Models_Lang();
        $objCategory     = new Models_EstoreCategory();


        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $this->view->headTitle(Nine_Language::translate('manage Category '));


        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);

        if($currentPage == false){
            $currentPage = 1;
            $this->session->categoryDisplayNum = null;
            $this->session->categoryCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objCategory->update(array('sorting' => $value), array('estore_category_gid=?' => $id));
            $this->session->categoryMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("Edit sort numbers successfully")
            );
        }

        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->categoryDisplayNum;
        } else {
            $this->session->categoryDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        $condition['user_id'] = $user['user_id'];
        if (false === $condition) {
            $condition = $this->session->categoryCondition;
        } else {
            $this->session->categoryCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }

        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_category', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                if(isset($allLangs[$index])){
                	unset($allLangs[$index]);	
                }
            }
        }

        /**
         * Get all categorys
         */
        $numRowPerPage = $numRowPerPage;
        $allCategories  = $objCategory->getallCategories($condition, array('parent_id'),
            $numRowPerPage,
            ($currentPage - 1) * $numRowPerPage
        );
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Count all categorys
         */
        $count = count($objCategory->getallCategories($condition));
        /**
         * Format
         */

        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allCategories[0]['estore_category_gid'];
        foreach ($allCategories as $index=>$category) {
            /**
             * Change date
             */

            if (0 != $category['created_date']) {
                $category['created_date'] = date($config['dateFormat'], $category['created_date']);
            } else {
                $category['created_date'] = '';
            }
            if ($tmpGid != $category['estore_category_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $category['estore_category_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $category;
            }
            $tmp2['langs'][]  = $category;
            /**
             * Final element
             */
            if ($index == count($allCategories) - 1) {
                $tmp[] = $tmp2;
            }

        }

        $allCategories = $tmp;
        $export = $this->_getParam('export', false);
        if($export != false){
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $style = "style = 'border: 1px solid'";

            if(Nine_Language::getCurrentLangId() == 1){
                $textHeader = array(
                    "<b>No</b>",
                    "<b>Parent Category</b>",
                    "<b>Created Date</b>",
                    "<b>Title</b>"
                );
            }else{
                $textHeader = array(
                    mb_convert_encoding("<b>S? Th? T?</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Danh M?c Cha</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ng�y Kh?i T?o</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ti�u �?</b>",'HTML-ENTITIES','UTF-8')
                );
            }

            $header = "<tr>";
            foreach ($textHeader as $text) {
                $header .= "<td $style>" . utf8_encode($text) . "</td>";
            }
            $header .= "</tr>";

            $content = '';
            $no = 1;

            foreach ($allCategories as $item) {
                $content .="<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['parent'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['name'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .="</tr>";
                $no++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-category-product-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();

        }
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCategories = $allCategories;
        $this->view->categoryMessage = $this->session->categoryMessage;
        $this->session->categoryMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_category');
        $this->view->allLangs = $allLangs;

        $this->view->menu = array(
            0=>'product',
            1=>'manager-category'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-product-hunt',
                'url'	=>	Nine_Registry::getBaseUrl().'estore/manage-category/' . $user['alias'] . '.html',
                'name'	=>	Nine_Language::translate('Manager Category Product')
            )

        );

        $this->view->username = $user['alias'];
        $this->view->alias = $user['alias'] . '.html';
    }

    public function newCategoryAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $this->setLayout('estore');

        $objLang = new Models_Lang();
        $objCategory = new Models_EstoreCategory();
        $objProduct = new Models_Product();
       
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data = $this->_getParam('data', false);
        /**
         * Get all categories
         */
        $objCat     = new Models_ProductCategory();
        $allCats = Nine_Query::buildTree(Nine_Query::getListProductCategory(array()));
//        echo "<pre>";print_r($allCats);die;
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_category', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                if(isset($allLangs[$index])){
                	unset($allLangs[$index]);	
                }
            }
        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCategory = $data;
            $newCategory['created_date'] = time();
            $newCategory['user_id'] = $user['user_id'];
            /**
             * Sorting
             */
            if (null == $newCategory['sorting']) {
                unset($newCategory['sorting']);
            }
            if (false == $this->checkPermission('new_category', null, '*')) {
                $newCategory['genabled']          = 0;
                $newCategory['sorting']           = 1;
            }

            /**
             * Remove empty images
             */
            if (is_array($newCategory['images'])) {
                foreach ($newCategory['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newCategory['images'][$index]);
                    } else {
                        $newCategory['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newCategory['images'] = implode('||', $newCategory['images']);
            foreach ($allLangs as $index => $lang) {
                if($newCategory[$lang['lang_id']]['alias'] == ""){
                    $newCategory[$lang['lang_id']]['alias'] = $objProduct->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
                    $newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newCategory[$lang['lang_id']]['alias']));
                }
            }
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > @$newCategory['sorting']) {
                    $newCategory['sorting'] = 1;
                }
                if (null == $newCategory['parent_id']) {
                    $newCategory['parent_id'] = NULL;
                }
                $objCategory->increaseSorting($newCategory['sorting'], 1);

                $gid = $objCategory->insert($newCategory);

                /**
                 * Update id string
                 */
                $objCategory->update(array('gid_string'	=>	$gid), array('estore_category_gid = ?' => $gid));
                $category = @reset($objCategory->getByColumns(array('estore_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);
                /**
                 * Message
                 */
                $this->session->categoryMessage =  array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is created successfully.')
                );

                $this->_redirect('estore/manage-category/' . $user['alias'] . '.html');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Category'));
        $this->view->fullPermisison = $this->checkPermission('new_category', null, '*');

        $this->view->menu = array(
            0=>'product',
            1=>'new-category'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-product-hunt',
                'url'	=>	Nine_Registry::getBaseUrl().'estore/manage-category/' .$user['alias'] . '.html',
                'name'	=>	Nine_Language::translate('Manager Category Product')
            ),
            1=>array(
                'icon' 	=> 	'fa-plus',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('New Category Product')
            )

        );

        $this->view->alias = $user['alias'] . '.html';
    }

    public function editCategoryAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        $objCat     = new Models_ProductCategory();
        $allCats = Nine_Query::buildTree(Nine_Query::getListProductCategory(array()));

        $this->setLayout('estore');

        $objCategory     = new Models_EstoreCategory();
        $objLang    = new Models_Lang();
        $objProduct = new Models_Product();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false);
        if (false == $gid) {
            $this->_redirect('estore/manage-category/' . $user['alias'] . '.html');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_category', null, '*'))
            ||  (false != $lid && false == $this->checkPermission('edit_category', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data   = $this->_getParam('data', false);

 
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allCategoryLangs = $objCategory->setAllLanguages(true)->getByColumns(array('estore_category_gid=?' => $gid, 'user_id=?' => $user['user_id']))->toArray();
//        echo "<pre>";print_r($allCategoryLangs);die;
        /**
         * Check permisison for each language
         */
        
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('edit_category', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                if(isset($allLangs[$index])){
                	unset($allLangs[$index]);	
                }
                
                unset($allCategoryLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCategory = $data;
            /**
             * Sorting
             */
            if (null == $newCategory['sorting']) {
                unset($newCategory['sorting']);
            }
            if (false == $this->checkPermission('new_category', null, '*')) {
                unset($newCategory['genabled']);
                unset($newCategory['sorting']);
            }

            /**
             * Remove empty images
             */
            if (is_array($newCategory['images'])) {
                foreach ($newCategory['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newCategory['images'][$index]);
                    } else {
                        $newCategory['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newCategory['images'] = implode('||', $newCategory['images']);
            foreach ($allLangs as $index => $lang) {
                if($newCategory[$lang['lang_id']]['alias'] == ""){
                    $newCategory[$lang['lang_id']]['alias'] = $objProduct->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
                    $newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newCategory[$lang['lang_id']]['alias']));
                }
            }
            try {
                /**
                 * Update
                 */
                if (null == $newCategory['parent_id']) {
                    $newCategory['parent_id'] = NULL;
                }
                /**
                 * Delete gid in parent
                 */
                $oldCategory = @reset($allCategoryLangs);
//                echo "<pre>";print_r($oldCategory);die;
                $objCategory->deleteGidString($oldCategory['parent_id'], $oldCategory['gid_string']);

                /**
                 * Update new data
                 */
                $objCategory->update($newCategory, array('estore_category_gid=?' => $gid));

                /**
                 * Update new id string
                 */
                $category = @reset($objCategory->getByColumns(array('estore_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);

                /**
                 * Message
                 */
                $this->session->categoryMessage =  array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is updated successfully.')
                );

                $this->_redirect('estore/manage-category/' . $user['alias'] . '.html');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allCategoryLangs);
            if (false == $data) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("Category doesn't exist.")
                );
                $this->_redirect('estore/manage-category/' . $user['alias'] . '.html');
            }
            /**
             * Format image
             */
            $data['images'] = explode('||', $data['images']);
            if (! is_array($data['images'])) {
                $data['images'] = array();
            }
            $data['images'] = array_pad($data['images'], 50, null);
            /**
             * Get all lang categorys
             */
            foreach ($allCategoryLangs as $category) {
                $data[$category['lang_id']] = $category;
            }

            /**
             * Get all child category
             */
            $allChildCats = explode(',',trim($data['gid_string'],','));
            unset($allChildCats[0]);
            foreach($allCats as $key =>	$item) {
                if (false != in_array($item, $allChildCats)) {
                    unset($allCats[$key]);
                }
            }
        }
        /**
         * Remove it and its childs from category list
         */
        $oldData = @reset($allCategoryLangs);
        /**
         * Prepare for template
         */
       
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Category'));
        $this->view->fullPermisison = $this->checkPermission('edit_category', null, '*');
        $this->view->menu = array(
            0=>'product',
            1=>'manager-category'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-product-hunt',
                'url'	=>	Nine_Registry::getBaseUrl().'estore/manage-category/' . $user['alias'] . '.html',
                'name'	=>	Nine_Language::translate('Manager Category Product')
            ),
            1=>array(
                'icon' 	=> 	'fa-pencil',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('Edit Category Product')
            )

        );

        $this->view->alias = $user['alias'] . '.html';
    }

    public function enableCategoryAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $objCategory = new Models_EstoreCategory();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);

        if (false == $gid) {
            $this->_redirect('estore/manage-category/' . $user['alias'] . '.html');
        }

        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_category', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objCategory->update(array('genabled' => 1), array('estore_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is enable successfully')
                );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                );
            }

        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_category', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objCategory->update(array('enabled' => 1), array('estore_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is enable successfully')
                );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                );
            }
        }


        $this->_redirect('estore/manage-category/' . $user['alias'] . '.html');
    }


    public function disableCategoryAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $objCategory = new Models_EstoreCategory();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);

        if (false == $gid) {
            $this->_redirect('estore/manage-category/' . $user['alias'] . '.html');
        }

        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_category', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objCategory->update(array('genabled' => 0), array('estore_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is disable successfully')
                );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                );
            }

        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_category', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objCategory->update(array('enabled' => 0), array('estore_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is disable successfully')
                );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                );
            }
        }


        $this->_redirect('estore/manage-category/' . $user['alias'] . '.html');
    }

    public function deleteCategoryAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $objCategory = new Models_EstoreCategory();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_category')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid = $this->_getParam('gid', false);

        if (false == $gid) {
            $this->_redirect('estore/manage-category/' . $user['alias'] . '.html');
        }

        $gids = explode('_', trim($gid, '_'));

        try {
            foreach ($gids as $gid) {

                $cat = @reset($objCategory->getByColumns(array('estore_category_gid=?'=>$gid))->toArray());
                if ( 0 == $cat['product_deleteable']){
                    $this->session->categoryMessage = array(
                        'success' => false,
                        'message' => Nine_Language::translate('Can NOT delete category ('. $gid .'). Please try again')
                    );
                    $this->_redirect('product/admin/manage-category');
                }
                else {
                    $objCategory->delete(array('estore_category_gid=?' => $gid));
                }
            }
            $this->session->categoryMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Category is deleted successfully')
            );
        } catch (Exception $e) {
            $this->session->categoryMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT delete this category. Please try again')
            );
        }
        $this->_redirect('estore/manage-category/' . $user['alias'] . '.html');
    }
}