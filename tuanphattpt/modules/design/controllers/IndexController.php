<?php
require_once 'modules/design/models/DesignRegister.php';
require_once 'modules/design/models/Params.php';
class design_IndexController extends Nine_Controller_Action
{

    /*
     * Index function
     */
    public function indexAction()
    {
        try {

           //Get link
            $this->view->link = Nine_Route::_('design/index/index');

            //Get list design
            $arrDesign = Nine_Read::GetListData('Models_Design');
            $this->view->arrDesign = $arrDesign;

            //Get config
            Nine_Common::getConfig($this->view);

            //Save design register
            $data = $this->_getParam('data', false);
            if (false != $data) {
                $newObj = $data;
                $newObj['place'] = Nine_Common::convert_vi_to_en($data['place']);
                $newObj['created_date'] = time();
                $obj = new Models_DesignRegister();
                $return = $obj->insert($newObj);
                if($return){
                    $this->view->err = array(
                        'status' => true,
                        'message' => Nine_Language::translate('Đăng ký chiếu sáng thành công.')
                    );
                }else{
                    $this->view->err = array(
                        'status' => false,
                        'message' =>  Nine_Language::translate('Đăng ký chiếu sáng thất bại.')
                    );
                }
            }

            /*
             * Set layout
             */
            Nine_Common::setLayoutAndView($this, Nine_Language::translate('CHƯƠNG TRÌNH THIẾT KẾ CHIẾU SÁNG NHÀ XƯỞNG MIỄN PHÍ'), 'design', 'index');
        } catch (Exception $exc) {
            echo $exc->getMessage();
            echo $exc->getTraceAsString();
        }
    }
    /*
     * Calculating
     */
    function calculationAction(){
        try {
            //Get list params
            $arrParams = Nine_Read::GetListData('Models_Params');
            $this->view->arrParams = $arrParams;
            
            //Set layout
            Nine_Common::setLayoutAndView($this, Nine_Language::translate('TÍNH TOÁN CHIẾU SÁNG NHÀ XƯỞNG'), 'calculation', 'calculation');
        } catch (Exception $exc) {
            echo $exc->getMessage();
            echo $exc->getTraceAsString();
        }
    }
}
