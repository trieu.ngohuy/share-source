<?php
require_once 'modules/design/models/Params.php';
require_once 'modules/design/models/DesignRegister.php';
class design_AdminController extends Nine_Controller_Action_Admin
{
    /*
     * Manager
     */
    public function manageParamsAction()
    {
        //Set title
        $this->view->headTitle(Nine_Language::translate('Manage Params'));

        /*
         * Paging
         */
        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
        if($currentPage == false){
            $currentPage = 1;
            $this->session->contactDisplayNum = null;
            $this->session->contactCondition = null;
        }
        if (false === $displayNum) {
            $displayNum = $this->session->contactDisplayNum;
        } else {
            $this->session->contactDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }

        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        $tmpCondition = $condition;
        if (false !== $condition) {
            $currentPage = 1;
        }
        if (false == $condition || $condition['code'] === '') {
            $condition = array();
            $tmpCondition = array();
        }else{
            //Adjust condition
            $tmpCondition = array(
                'code LIKE ?' => '%'.$condition['code'].'%'
            );
        }
        /**
         * Get all contacts
         */
        $numRowPerPage= $numRowPerPage;
        $allParams = Nine_Read::GetListData('Models_Params', $tmpCondition, array('thongso_id ASC'),'index',
            $numRowPerPage,
            ($currentPage - 1) * $numRowPerPage
        );

        /**
         * Count all contacts
         */
        $count = count(Nine_Read::GetListData('Models_Params', $tmpCondition));

        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allParams = $allParams;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;

        $this->view->menu = array(
            0=>'params',
            1=>'manager-params'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-contact-hunt',
                'url'	=>	Nine_Registry::getBaseUrl().'admin/design/admin/manage-params',
                'name'	=>	Nine_Language::translate('Manager Params')
            )

        );
    }
    /*
     * New
     */
    public function newParamsAction()
    {

        $objParams = new Models_Params();
        $id = $this->_getParam('id', false);

        if($id != false){
            $this->_redirect('design/admin/manage-params');
        }

        $data = $this->_getParam('data', false);

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new design
             */
            $newParams = $data;
            $newParams['created_date'] = time();
            $newParams['alias'] = $newParams['code'];

            try {

                $objParams->insert($newParams);
                /**
                 * Message
                 */
                $this->session->designMessage =  array(
                    'success' => true,
                    'message' => Nine_Language::translate('Params is created successfully.')
                );

                $this->_redirect('design/admin/manage-params');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now. ' . $e));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $this->view->id = $id;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->menu = array(
            0=>'params',
            1=>'new-params'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-newspaper-o',
                'url'	=>	Nine_Registry::getBaseUrl().'admin/design/admin/manage-params',
                'name'	=>	Nine_Language::translate('Manager Params')
            ),
            1=>array(
                'icon' 	=> 	'fa-plus',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('New Params')
            )

        );
    }
    /*
     * Edit
     */
    public function editParamsAction()
    {
        $objParams = new Models_Params();

        $gid     = $this->_getParam('id', false);
        if (false == $gid) {
            $this->_redirect('design/admin/manage-params');
        }

        $data   = $this->_getParam('data', false);

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new design
             */
            $newParams = $data;
            $newParams['alias'] = $data['code'];

            try {
                /**
                 * Update
                 */
                $objParams->update($newParams, array('thongso_id=?' => $gid));
                /**
                 * Message
                 */
                $this->session->designMessage =  array(
                    'success' => true,
                    'message' => Nine_Language::translate('Params is updated successfully.')
                );

                $this->_redirect('design/admin/manage-params');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset(Nine_Read::GetListData('Models_Params', array(
                'thongso_id' => $gid
            )));

            if (false == $data) {
                $this->session->designMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("Params doesn't exist.")
                );
                $this->_redirect('design/admin/manage-params');
            }

        }

        /**
         * Prepare for template
         */

        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Params'));

        $this->view->menu = array(
            0=>'params',
            1=>'manager-params'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-newspaper-o',
                'url'	=>	Nine_Registry::getBaseUrl().'admin/design/admin/manage-params',
                'name'	=>	Nine_Language::translate('Manager Params')
            ),
            1=>array(
                'icon' 	=> 	'fa-pencil',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('Edit Params')
            )

        );
    }

    /*
     * Delete
     */
    public function deleteParamsAction()
    {
        $obj = new Models_Params();

        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('design/admin/manage-params');
        }

        $gids = explode('_', trim($id, '_'));

        try {
            foreach ($gids as $gid) {

                $contact = @reset($obj->getByColumns(array('thongso_id=?'=>$gid))->toArray());
                $obj->delete(array('thongso_id=?' => $gid));
            }
            $this->session->contactMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Params is deleted successfully')
            );
        } catch (Exception $e) {
            $this->session->contactMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT delete this contact. Please try again')
            );
        }
        $this->_redirect('design/admin/manage-params');
    }

    /*
     * Manager design
     */
    public function manageDesignAction()
    {
        $objDesign = new Models_Design();
        $objLang    = new Models_Lang();

        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_design', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $this->view->headTitle(Nine_Language::translate('manage Design '));


        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
        if($currentPage == false){
            $currentPage = 1;
            $this->session->designDisplayNum = null;
            $this->session->designCondition = null;
        }
        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->designDisplayNum;
        } else {
            $this->session->designDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        $tmpCondition = $condition;
        if (false === $condition) {
            $condition = $this->session->designCondition;
            $tmpCondition = array();
        } else {
            $this->session->designCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        if(@$condition['keyword'] != ''){
            //Adjust condition
            $tmpCondition = array(
                'title LIKE ?' => '%'.$condition['keyword'].'%'
            );
        }else{
            $tmpCondition = array();
        }
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_design', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        /**
         * Get all designs
         */
        $numRowPerPage= $numRowPerPage;
        $allDesign = Nine_Read::GetListData('Models_Design',$tmpCondition, array('design_gid DESC', 'design_id ASC'),
            $numRowPerPage,
            ($currentPage - 1) * $numRowPerPage
        );

        /**
         * Count all designs
         */
        $count = count(Nine_Read::GetListData('Models_Design',$tmpCondition));
        /**
         * Format
         */
        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allDesign[0]['design_gid'];

        foreach ($allDesign as $index=>$design) {
            if ($tmpGid != $design['design_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $design['design_gid'];
            }
            $tmp2['langs'][]  = $design;
            $tmp2['created_date']  = $design['created_date'];
            $tmp2['design_gid']  = $design['design_gid'];
            /**
             * Final element
             */
            if ($index == count($allDesign) - 1) {
                $tmp[] = $tmp2;
            }
        }
        $allDesign = $tmp;

        /**
         * Set values for tempalte
         */
//        echo print_r($condition); die;

        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allDesign = $allDesign;
        $this->view->designMessage = $this->session->designMessage;
        $this->session->designMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_design');
        $this->view->EditPermisision = $this->checkPermission('edit_design');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_design');
        $this->view->DeletePermisision = $this->checkPermission('delete_design');
        $this->view->allLangs = $allLangs;

        $this->view->menu = array(
            0=>'design',
            1=>'manager-design'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-newspaper-o',
                'url'	=>	Nine_Registry::getBaseUrl().'admin/design/admin/manage-design',
                'name'	=>	Nine_Language::translate('Manager Design')
            )

        );
    }
    /*
     * New design
     */
    public function newDesignAction()
    {
        $objLang = new Models_Lang();
        $objDesign = new Models_Design();
        $id = $this->_getParam('gid', false);

        if($id != false){
            $id = explode("_", $id);
            if(count($id) != 2){
                $this->_redirect('design/admin/manage-design');
            }
        }
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_design', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data = $this->_getParam('data', false);
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_design', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new design
             */
            $newDesign = $data;
            $newDesign['created_date'] = time();

            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                if($newDesign[$lang['lang_id']]['alias'] == ""){
                    $newDesign[$lang['lang_id']]['alias'] = Nine_Common::convert_vi_to_en($newDesign[$lang['lang_id']]['title']);
                    $newDesign[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newDesign[$lang['lang_id']]['alias']));
                }
            }

            //Upload file
            $tmp = Nine_Common::UploadFile($_FILES['file']);
            if(isset($tmp['status'])){
                $this->view->designMessage =  array(
                    'success' => false,
                    'message' => $tmp['message']
                );
                goto Behind;
            }else{
                $newDesign['file'] = $tmp;
            }
            try {

                $objDesign->insert($newDesign);
                /**
                 * Message
                 */
                $this->session->designMessage =  array(
                    'success' => true,
                    'message' => Nine_Language::translate('Design is created successfully.')
                );
//                echo "<pre>HERE";die;
                $this->_redirect('design/admin/manage-design');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        Behind:
        /**
         * Prepare for template
         */
        $this->view->id = $id;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Design'));
        $this->view->fullPermisison = $this->checkPermission('new_design', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_design');
        $this->view->menu = array(
            0=>'design',
            1=>'new-design'
        );
        
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-newspaper-o',
                'url'	=>	Nine_Registry::getBaseUrl().'admin/design/admin/manage-design',
                'name'	=>	Nine_Language::translate('Manager Design')
            ),
            1=>array(
                'icon' 	=> 	'fa-plus',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('New Design')
            )

        );
    }

    /*
     * Edit design
     */
    public function editDesignAction()
    {
        $objDesign = new Models_Design();
        $objLang    = new Models_Lang();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_design', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false);
        if (false == $gid) {
            $this->_redirect('design/admin/manage-design');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_design', null, '*'))
            ||  (false != $lid && false == $this->checkPermission('edit_design', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data   = $this->_getParam('data', false);

        /**
         * Get all design languages
         */

        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allDesignLangs = $objDesign->setAllLanguages(true)->getByColumns(array('design_gid=?' => $gid))->toArray();
        //print_r($allDesignLangs);
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_design', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                //unset($allLangs[$index]);
                unset($allDesignLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new design
             */
            $newDesign = $data;
            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {

                if($newDesign[$lang['lang_id']]['alias'] == ""){
                    $newContent[$lang['lang_id']]['alias'] = Nine_Common::convert_vi_to_en($newDesign[$lang['lang_id']]['title']);
                    $newContent[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newContent[$lang['lang_id']]['alias']));
                }
            }
            /*
             * File
             */
            $tmp = Nine_Common::UploadFile($_FILES['file']);
            if(isset($tmp['status'])){
                $this->view->designMessage =  array(
                    'success' => false,
                    'message' => $tmp['message']
                );
                goto Behind;
            }else{
                $newDesign['file'] = $tmp;
            }
            try {
                /**
                 * Update
                 */
                $objDesign->update($newDesign, array('design_gid=?' => $gid));
                /**
                 * Message
                 */
                $this->session->designMessage =  array(
                    'success' => true,
                    'message' => Nine_Language::translate('Design is updated successfully.')
                );

                $this->_redirect('design/admin/manage-design');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allDesignLangs);

            if (false == $data) {
                $this->session->designMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("Design doesn't exist.")
                );
                $this->_redirect('design/admin/manage-design');
            }
            /**
             * Get all lang contents
             */
            foreach ($allDesignLangs as $content) {
                /**
                 * Rebuild readmore DIV
                 */
                $data[$content['lang_id']] = $content;
            }
            /*
             * File
             */
            $image = explode('/', $data['file']);
            $fileName = end($image);
            $this->view->fileName = $fileName;
            /**
             * Get all lang designs
             */
            foreach ($allDesignLangs as $design) {
                /**
                 * Rebuild readmore DIV
                 */
                $data[$design['lang_id']] = $design;
            }
        }
        Behind:
        /**
         * Prepare for template
         */
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Design'));
        $this->view->fullPermisison = $this->checkPermission('edit_design', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_design');
        $this->view->menu = array(
            0=>'design',
            1=>'manager-design'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-newspaper-o',
                'url'	=>	Nine_Registry::getBaseUrl().'admin/design/admin/manage-design',
                'name'	=>	Nine_Language::translate('Manager Design')
            ),
            1=>array(
                'icon' 	=> 	'fa-pencil',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('Edit Design')
            )

        );
    }
    /*
     * Manager design register
     */
    public function manageDesignRegisterAction()
    {
        //Set title
        $this->view->headTitle(Nine_Language::translate('Manage Design Register'));

        /*
         * Paging
         */
        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
        if($currentPage == false){
            $currentPage = 1;
            $this->session->contactDisplayNum = null;
            $this->session->contactCondition = null;
        }
        if (false === $displayNum) {
            $displayNum = $this->session->contactDisplayNum;
        } else {
            $this->session->contactDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }

        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        $tmpCondition = $condition;
        if (false !== $condition) {
            $currentPage = 1;
        }

        if (false == $condition || $condition['place'] === '') {
            $condition = array();
            $tmpCondition = array();
        }else{
            //Adjust condition
            $tmpCondition = array(
                'place LIKE ?' => '%'.$condition['place'].'%'
            );
        }
        /**
         * Get all contacts
         */
        $numRowPerPage= $numRowPerPage;
        $allDesignRegister = Nine_Read::GetListData('Models_DesignRegister', $tmpCondition, array('register_id ASC'),'index',
            $numRowPerPage,
            ($currentPage - 1) * $numRowPerPage
        );

        /**
         * Count all contacts
         */
        $count = count(Nine_Read::GetListData('Models_DesignRegister', $tmpCondition));

        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allDesignRegister = $allDesignRegister;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;

        $this->view->menu = array(
            0=>'design-register',
            1=>'manager-design-register'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-contact-hunt',
                'url'	=>	Nine_Registry::getBaseUrl().'admin/design/admin/manage-design-register',
                'name'	=>	Nine_Language::translate('Manager Design Register')
            )

        );
    }
    /*
     * Delete design register
     */
    public function deleteDesignRegisterAction()
    {
        $obj = new Models_DesignRegister();

        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('design/admin/manage-design-register');
        }

        $gids = explode('_', trim($id, '_'));

        try {
            foreach ($gids as $gid) {

                $obj->delete(array('register_id=?' => $gid));
            }
            $this->session->contactMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Design Register is deleted successfully')
            );
        } catch (Exception $e) {
            $this->session->contactMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT delete this contact. Please try again')
            );
        }
        $this->_redirect('design/admin/manage-design-register');
    }
}