<?php
return array(
    'permissionRules' => array(
        'see_design' => array(//External permission
            'See all articles',//Full control name
            '9_lang',//table
            'lang_id', //Check permisison column
            'name'),//Display column at permission interface
        'new_design' => array(//External permission
            'Create new article',//Full control name
            '9_lang',//table
            'lang_id', //Check permisison column
            'name'),//Display column at permission inteface
        'edit_design' => array(//External permission
            'Edit existed articles',//Full control name
            '9_lang',//table
            'lang_id', //Check permisison column
            'name'),//Display column at permission inteface

        'genabled_design' => 'Genabled existed articles',
        'delete_design' => 'Delete existed articles'
    ),

);
