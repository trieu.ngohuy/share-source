<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <div class="main-content section">                
                <div id="nf-form-6-cont" class="nf-form-cont formDKTK" aria-live="polite"
                     aria-labelledby="nf-form-title-6" aria-describedby="nf-form-errors-6" role="form">
                    <span id="nf-form-title-6" class="nf-form-title">
                        <h3>{{l}}Đăng ký thiết kế chiếu sáng nhà xưởng{{/l}}</h3>
                    </span>
                    {{if isset($err)}}
                    <br>
                    <p style="color: #ffffff; background: #52b84b; padding: 10px;">
                        {{$err.message}}
                    </p>
                    <br>
                    {{/if}}
                    <div class="nf-form-wrap ninja-forms-form-wrap">
                        <div class="nf-response-msg"></div>
                        <div class="nf-debug-msg"></div>
                        <div class="nf-before-form">
                            <nf-section></nf-section>
                        </div>
                        <div class="nf-form-layout">
                            <form role="form" method="post" action="{{$link}}">
                                <div>
                                    <div class="nf-before-form-content">
                                        <nf-section>
                                            <div class="nf-form-fields-required">{{l}}Các trường được đánh dấu{{/l}} <span
                                                    class="ninja-forms-req-symbol">*</span> {{l}}là bắt buộc{{/l}}
                                            </div>
                                        </nf-section>
                                    </div>
                                    <div class="nf-form-content ">
                                        <nf-fields-wrap>
                                            <nf-field>
                                                <div id="nf-field-45-container"
                                                     class="nf-field-container listselect-container  label-above  list-container">
                                                    <div class="nf-before-field">
                                                        <nf-section></nf-section>
                                                    </div>
                                                    <div class="nf-field">
                                                        <div id="nf-field-45-wrap"
                                                             class="field-wrap listselect-wrap list-wrap list-select-wrap"
                                                             data-field-id="45">


                                                            <div class="nf-field-label"><label for="nf-field-45"
                                                                                               id="nf-label-field-45"
                                                                                               class="">{{l}}Ứng dụng nhà xưởng{{/l}} </label></div>

                                                            <div class="nf-field-element">
                                                                <select name="data[place]" id="nf-field-45"
                                                                        name="nf-field-45" aria-invalid="false"
                                                                        aria-describedby="nf-error-45"
                                                                        class="ninja-forms-field nf-element"
                                                                        aria-labelledby="nf-label-field-45">

                                                                    <option value="Sản suất" selected="selected">{{l}}Sản suất{{/l}}
                                                                    </option>
                                                                    <option value="Kho">{{l}}Kho{{/l}}</option>
                                                                    <option value="Kiểm tra">{{l}}Kiểm tra{{/l}}</option>
                                                                    <option value="Chung">{{l}}Chung{{/l}}</option>
                                                                </select>
                                                                <div for="nf-field-45"></div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                    <div class="nf-after-field">
                                                        <nf-section>
                                                            <div class="nf-input-limit"></div>

                                                            <div id="nf-error-45" class="nf-error-wrap nf-error"
                                                                 role="alert"></div>

                                                        </nf-section>
                                                    </div>
                                                </div>
                                            </nf-field>
                                            <nf-field>
                                                <div id="nf-field-46-container"
                                                     class="nf-field-container number-container  label-above ">
                                                    <div class="nf-before-field">
                                                        <nf-section></nf-section>
                                                    </div>
                                                    <div class="nf-field">
                                                        <div id="nf-field-46-wrap" class="field-wrap number-wrap"
                                                             data-field-id="46">


                                                            <div class="nf-field-label"><label for="nf-field-46"
                                                                                               id="nf-label-field-46"
                                                                                               class="">{{l}}Chiều dài nhà xưởng (m){{/l}}<span
                                                                        class="ninja-forms-req-symbol">*</span>
                                                                </label></div>

                                                            <div class="nf-field-element">
                                                                <input name="data[dai]" id="nf-field-46"
                                                                       name="nf-field-46" aria-invalid="false"
                                                                       aria-describedby="nf-error-46"
                                                                       class="ninja-forms-field nf-element"
                                                                       aria-labelledby="nf-label-field-46" required=""
                                                                       type="number" value=""
                                                                       min="" max="" step="1">
                                                            </div>


                                                        </div>
                                                    </div>
                                                    <div class="nf-after-field">
                                                        <nf-section>
                                                            <div class="nf-input-limit"></div>

                                                            <div id="nf-error-46" class="nf-error-wrap nf-error"
                                                                 role="alert"></div>

                                                        </nf-section>
                                                    </div>
                                                </div>
                                            </nf-field>
                                            <nf-field>
                                                <div id="nf-field-47-container"
                                                     class="nf-field-container number-container  label-above ">
                                                    <div class="nf-before-field">
                                                        <nf-section></nf-section>
                                                    </div>
                                                    <div class="nf-field">
                                                        <div id="nf-field-47-wrap" class="field-wrap number-wrap"
                                                             data-field-id="47">


                                                            <div class="nf-field-label"><label for="nf-field-47"
                                                                                               id="nf-label-field-47"
                                                                                               class="">{{l}}Chiều rộng nhà xưởng (m){{/l}}<span
                                                                        class="ninja-forms-req-symbol">*</span>
                                                                </label></div>

                                                            <div class="nf-field-element">
                                                                <input name="data[rong]" id="nf-field-47"
                                                                       name="nf-field-47" aria-invalid="false"
                                                                       aria-describedby="nf-error-47"
                                                                       class="ninja-forms-field nf-element"
                                                                       aria-labelledby="nf-label-field-47" required=""
                                                                       type="number" value=""
                                                                       min="" max="" step="1"></div>


                                                        </div>
                                                    </div>
                                                    <div class="nf-after-field">
                                                        <nf-section>
                                                            <div class="nf-input-limit"></div>

                                                            <div id="nf-error-47" class="nf-error-wrap nf-error"
                                                                 role="alert"></div>

                                                        </nf-section>
                                                    </div>
                                                </div>
                                            </nf-field>
                                            <nf-field>
                                                <div id="nf-field-48-container"
                                                     class="nf-field-container number-container  label-above ">
                                                    <div class="nf-before-field">
                                                        <nf-section></nf-section>
                                                    </div>
                                                    <div class="nf-field">
                                                        <div id="nf-field-48-wrap" class="field-wrap number-wrap"
                                                             data-field-id="48">


                                                            <div class="nf-field-label"><label for="nf-field-48"
                                                                                               id="nf-label-field-48"
                                                                                               class="">{{l}}Chiều cao treo đèn (m){{/l}}<span
                                                                        class="ninja-forms-req-symbol">*</span>
                                                                </label></div>

                                                            <div class="nf-field-element">
                                                                <input name="data[cao]" id="nf-field-48"
                                                                       name="nf-field-48" aria-invalid="false"
                                                                       aria-describedby="nf-error-48"
                                                                       class="ninja-forms-field nf-element"
                                                                       aria-labelledby="nf-label-field-48" required=""
                                                                       type="number" value=""
                                                                       min="" max="" step="1"></div>


                                                        </div>
                                                    </div>
                                                    <div class="nf-after-field">
                                                        <nf-section>
                                                            <div class="nf-input-limit"></div>

                                                            <div id="nf-error-48" class="nf-error-wrap nf-error"
                                                                 role="alert"></div>

                                                        </nf-section>
                                                    </div>
                                                </div>
                                            </nf-field>
                                            <nf-field>
                                                <div id="nf-field-49-container"
                                                     class="nf-field-container number-container  label-above ">
                                                    <div class="nf-before-field">
                                                        <nf-section></nf-section>
                                                    </div>
                                                    <div class="nf-field">
                                                        <div id="nf-field-49-wrap" class="field-wrap number-wrap"
                                                             data-field-id="49">


                                                            <div class="nf-field-label"><label for="nf-field-49"
                                                                                               id="nf-label-field-49"
                                                                                               class="">{{l}}Độ rọi yêu cầu (lux){{/l}}</label></div>

                                                            <div class="nf-field-element">
                                                                <input name="data[doroi]" id="nf-field-49"
                                                                       name="nf-field-49" aria-invalid="false"
                                                                       aria-describedby="nf-error-49"
                                                                       class="ninja-forms-field nf-element"
                                                                       aria-labelledby="nf-label-field-49" type="number"
                                                                       value="" min="" max=""
                                                                       step="1"></div>


                                                        </div>
                                                    </div>
                                                    <div class="nf-after-field">
                                                        <nf-section>
                                                            <div class="nf-input-limit"></div>

                                                            <div id="nf-error-49" class="nf-error-wrap nf-error"
                                                                 role="alert"></div>

                                                        </nf-section>
                                                    </div>
                                                </div>
                                            </nf-field>
                                            <nf-field>
                                                <div id="nf-field-50-container"
                                                     class="nf-field-container html-container  label-above ">
                                                    <div class="nf-before-field">
                                                        <nf-section></nf-section>
                                                    </div>
                                                    <div class="nf-field">
                                                        <div id="nf-field-50-wrap" class="field-wrap html-wrap"
                                                             data-field-id="50">


                                                            <div class="nf-field-label"><label for="nf-field-50"
                                                                                               id="nf-label-field-50"
                                                                                               class="">HTML </label>
                                                            </div>

                                                            <div class="nf-field-element">
                                                                <hr>
                                                            </div>


                                                        </div>
                                                    </div>
                                                    <div class="nf-after-field">
                                                        <nf-section>
                                                            <div class="nf-input-limit"></div>

                                                            <div id="nf-error-50" class="nf-error-wrap nf-error"
                                                                 role="alert"></div>

                                                        </nf-section>
                                                    </div>
                                                </div>
                                            </nf-field>
                                            <nf-field>
                                                <div id="nf-field-41-container"
                                                     class="nf-field-container firstname-container  label-above ">
                                                    <div class="nf-before-field">
                                                        <nf-section></nf-section>
                                                    </div>
                                                    <div class="nf-field">
                                                        <div id="nf-field-41-wrap" class="field-wrap firstname-wrap"
                                                             data-field-id="41">


                                                            <div class="nf-field-label"><label for="nf-field-41"
                                                                                               id="nf-label-field-41"
                                                                                               class="">{{l}}Tên{{/l}} <span
                                                                        class="ninja-forms-req-symbol">*</span>
                                                                </label></div>

                                                            <div class="nf-field-element">
                                                                <input type="text" value=""
                                                                       class="ninja-forms-field nf-element"
                                                                       id="nf-field-41" name="data[name]"
                                                                       autocomplete="given-name" aria-invalid="false"
                                                                       aria-describedby="nf-error-41"
                                                                       aria-labelledby="nf-label-field-41"
                                                                       required=""></div>


                                                        </div>
                                                    </div>
                                                    <div class="nf-after-field">
                                                        <nf-section>
                                                            <div class="nf-input-limit"></div>

                                                            <div id="nf-error-41" class="nf-error-wrap nf-error"
                                                                 role="alert"></div>

                                                        </nf-section>
                                                    </div>
                                                </div>
                                            </nf-field>
                                            <nf-field>
                                                <div id="nf-field-42-container"
                                                     class="nf-field-container phone-container  label-above  textbox-container">
                                                    <div class="nf-before-field">
                                                        <nf-section></nf-section>
                                                    </div>
                                                    <div class="nf-field">
                                                        <div id="nf-field-42-wrap"
                                                             class="field-wrap phone-wrap textbox-wrap"
                                                             data-field-id="42">


                                                            <div class="nf-field-label"><label for="nf-field-42"
                                                                                               id="nf-label-field-42"
                                                                                               class="">{{l}}SĐT{{/l}} <span
                                                                        class="ninja-forms-req-symbol">*</span>
                                                                </label></div>

                                                            <div class="nf-field-element">
                                                                <input type="tel" value=""
                                                                       class="ninja-forms-field nf-element"
                                                                       id="nf-field-42" name="data[phone]"
                                                                       autocomplete="tel" aria-invalid="false"
                                                                       aria-describedby="nf-error-42"
                                                                       aria-labelledby="nf-label-field-42" required="">
                                                            </div>


                                                        </div>
                                                    </div>
                                                    <div class="nf-after-field">
                                                        <nf-section>
                                                            <div class="nf-input-limit"></div>

                                                            <div id="nf-error-42" class="nf-error-wrap nf-error"
                                                                 role="alert"></div>

                                                        </nf-section>
                                                    </div>
                                                </div>
                                            </nf-field>
                                            <nf-field>
                                                <div id="nf-field-43-container"
                                                     class="nf-field-container email-container  label-above ">
                                                    <div class="nf-before-field">
                                                        <nf-section></nf-section>
                                                    </div>
                                                    <div class="nf-field">
                                                        <div id="nf-field-43-wrap" class="field-wrap email-wrap"
                                                             data-field-id="43">


                                                            <div class="nf-field-label"><label for="nf-field-43"
                                                                                               id="nf-label-field-43"
                                                                                               class="">Email <span
                                                                        class="ninja-forms-req-symbol">*</span>
                                                                </label></div>

                                                            <div class="nf-field-element">
                                                                <input type="email" value=""
                                                                       class="ninja-forms-field nf-element"
                                                                       id="nf-field-43" name="data[email]"
                                                                       autocomplete="email" aria-invalid="false"
                                                                       aria-describedby="nf-error-43"
                                                                       aria-labelledby="nf-label-field-43"
                                                                       required=""></div>


                                                        </div>
                                                    </div>
                                                    <div class="nf-after-field">
                                                        <nf-section>
                                                            <div class="nf-input-limit"></div>

                                                            <div id="nf-error-43" class="nf-error-wrap nf-error"
                                                                 role="alert"></div>

                                                        </nf-section>
                                                    </div>
                                                </div>
                                            </nf-field>
                                            <nf-field>
                                                <div id="nf-field-51-container"
                                                     class="nf-field-container textarea-container  label-above ">
                                                    <div class="nf-before-field">
                                                        <nf-section></nf-section>
                                                    </div>
                                                    <div class="nf-field">
                                                        <div id="nf-field-51-wrap" class="field-wrap textarea-wrap"
                                                             data-field-id="51">


                                                            <div class="nf-field-label"><label for="nf-field-51"
                                                                                               id="nf-label-field-51"
                                                                                               class="">{{l}}Lời nhắn (Nếu có){{/l}}</label></div>

                                                            <div class="nf-field-element">
                                                                <textarea id="nf-field-51" name="data[full_text]"
                                                                          aria-invalid="false"
                                                                          aria-describedby="nf-error-51"
                                                                          class="ninja-forms-field nf-element"
                                                                          aria-labelledby="nf-label-field-51"></textarea>
                                                            </div>


                                                        </div>
                                                    </div>
                                                    <div class="nf-after-field">
                                                        <nf-section>
                                                            <div class="nf-input-limit"></div>

                                                            <div id="nf-error-51" class="nf-error-wrap nf-error"
                                                                 role="alert"></div>

                                                        </nf-section>
                                                    </div>
                                                </div>
                                            </nf-field>
                                            <nf-field>
                                                <div id="nf-field-44-container"
                                                     class="nf-field-container submit-container  label-above  textbox-container">
                                                    <div class="nf-before-field">
                                                        <nf-section></nf-section>
                                                    </div>
                                                    <div class="nf-field">
                                                        <div id="nf-field-44-wrap"
                                                             class="field-wrap submit-wrap textbox-wrap"
                                                             data-field-id="44">
                                                            <div class="nf-field-label"></div>
                                                            <div class="nf-field-element">
                                                                <button id="nf-field-44" class="cta_btn" type="submit"
                                                                        style="background-color: #ffab40 !important; border: #ffab40; font-size: 15px; cursor: pointer;">{{l}}Gửi{{/l}}</button>
                                                            </div>
                                                            <div class="nf-error-wrap"></div>
                                                        </div>
                                                    </div>
                                                    <div class="nf-after-field">
                                                        <nf-section>
                                                            <div class="nf-input-limit"></div>

                                                            <div id="nf-error-44" class="nf-error-wrap nf-error"
                                                                 role="alert"></div>

                                                        </nf-section>
                                                    </div>
                                                </div>
                                            </nf-field>
                                        </nf-fields-wrap>
                                    </div>
                                    <div class="nf-after-form-content">
                                        <nf-section>
                                            <div id="nf-form-errors-6" class="nf-form-errors" role="alert">
                                                <nf-errors></nf-errors>
                                            </div>
                                            <div class="nf-form-hp">
                                                <nf-section><label for="nf-field-hp-6" aria-hidden="true">

                                                        <input id="nf-field-hp-6" name="nf-field-hp"
                                                               class="nf-element nf-field-hp" type="text" value="">
                                                    </label></nf-section>
                                            </div>
                                        </nf-section>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="nf-after-form">
                            <nf-section></nf-section>
                        </div>
                    </div>
                </div>
                <script>
                    var formDisplay = 1;
                    var nfForms = nfForms || [];
                    var form = [];
                    form.id = '6';
                    form.settings ={"objectType":"Form Setting","editActive":true,"title":"\u0110\u0103ng k\u00fd thi\u1ebft k\u1ebf chi\u1ebfu s\u00e1ng nh\u00e0 x\u01b0\u1edfng","show_title":1,"clear_complete":0,"hide_complete":0,"default_label_pos":"above","wrapper_class":"formDKTK","element_class":"","key":"","add_submit":0,"currency":"","logged_in":false,"not_logged_in_msg":"","sub_limit_msg":"The Form has reached it's submission limit.","calculations":[],"formContentData":["ung_dung_nha_xuong_1496376991746","chieu_dai_nha_xuong_m_1496377098465","chieu_rong_nha_xuong_m_1496377120332","chieu_cao_treo_den_m_1496377147722","do_roi_yeu_cau_lux_1496377178976","html_1496377193008","ten_1495787943413","sdt_1495787947662","email_1495787935242","loi_nhan_neu_co_1496822992724","gui_1495787974225"],"drawerDisabled":false,"changeEmailErrorMsg":"H\u00e3y nh\u1eadp m\u1ed9t \u0111\u1ecba ch\u1ec9 email h\u1ee3p l\u1ec7!","confirmFieldErrorMsg":"Nh\u1eefng tr\u01b0\u1eddng n\u00e0y ph\u1ea3i kh\u1edbp!","fieldNumberNumMinError":"L\u1ed7i s\u1ed1 t\u1ed1i thi\u1ec3u","fieldNumberNumMaxError":"L\u1ed7i s\u1ed1 t\u1ed1i \u0111a","fieldNumberIncrementBy":"H\u00e3y t\u0103ng theo ","formErrorsCorrectErrors":"H\u00e3y s\u1eeda l\u1ed7i tr\u01b0\u1edbc khi g\u1eedi m\u1eabu n\u00e0y.","validateRequiredField":"\u0110\u00e2y l\u00e0 m\u1ed9t tr\u01b0\u1eddng b\u1eaft bu\u1ed9c.","honeypotHoneypotError":"L\u1ed7i Honeypot","fieldsMarkedRequired":"C\u00e1c tr\u01b0\u1eddng \u0111\u01b0\u1ee3c \u0111\u00e1nh d\u1ea5u <span class=\"ninja-forms-req-symbol\">*<\/span> l\u00e0 b\u1eaft bu\u1ed9c","unique_field_error":"A form with this value has already been submitted.","ninjaForms":"Ninja Forms","changeDateErrorMsg":"Please enter a valid date!","fieldTextareaRTEInsertLink":"Ch\u00e8n li\u00ean k\u1ebft","fieldTextareaRTEInsertMedia":"Ch\u00e8n ph\u01b0\u01a1ng ti\u1ec7n","fieldTextareaRTESelectAFile":"Ch\u1ecdn t\u1eadp tin","fileUploadOldCodeFileUploadInProgress":"\u0110ang t\u1ea3i t\u1eadp tin l\u00ean.","fileUploadOldCodeFileUpload":"T\u1ea2I T\u1eacP TIN L\u00caN","currencySymbol":"&#36;","thousands_sep":".","decimal_point":",","dateFormat":"d\/m\/Y","startOfWeek":"1","of":"c\u1ee7a","previousMonth":"Previous Month","nextMonth":"Next Month","months":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthsShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"weekdays":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"weekdaysShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"weekdaysMin":["Su","Mo","Tu","We","Th","Fr","Sa"],"currency_symbol":"","beforeForm":"","beforeFields":"","afterFields":"","afterForm":""};
                    form.fields = [{"objectType":"Field","objectDomain":"fields","editActive":false,"order":1,"type":"listselect","label":"\u1ee8ng d\u1ee5ng nh\u00e0 x\u01b0\u1edfng","key":"ung_dung_nha_xuong_1496376991746","label_pos":"above","required":false,"options":[{"errors":[],"max_options":0,"label":"S\u1ea3n su\u1ea5t","value":"S\u1ea3n su\u1ea5t","calc":"","selected":0,"order":0,"settingModel":{"settings":false,"hide_merge_tags":false,"error":false,"name":"options","type":"option-repeater","label":"Options <a href=\"#\" class=\"nf-add-new\">Add New<\/a> <a href=\"#\" class=\"extra nf-open-import-tooltip\"><i class=\"fa fa-sign-in\" aria-hidden=\"true\"><\/i> Import<\/a>","width":"full","group":"","value":[{"label":"One","value":"one","calc":"","selected":0,"order":0},{"label":"Two","value":"two","calc":"","selected":0,"order":1},{"label":"Three","value":"three","calc":"","selected":0,"order":2}],"columns":{"label":{"header":"Label","default":""},"value":{"header":"Value","default":""},"calc":{"header":"Calc Value","default":""},"selected":{"header":"<span class=\"dashicons dashicons-yes\"><\/span>","default":0}}},"manual_value":true},{"errors":[],"max_options":0,"label":"Kho","value":"Kho","calc":"","selected":0,"order":1,"settingModel":{"settings":false,"hide_merge_tags":false,"error":false,"name":"options","type":"option-repeater","label":"Options <a href=\"#\" class=\"nf-add-new\">Add New<\/a> <a href=\"#\" class=\"extra nf-open-import-tooltip\"><i class=\"fa fa-sign-in\" aria-hidden=\"true\"><\/i> Import<\/a>","width":"full","group":"","value":[{"label":"One","value":"one","calc":"","selected":0,"order":0},{"label":"Two","value":"two","calc":"","selected":0,"order":1},{"label":"Three","value":"three","calc":"","selected":0,"order":2}],"columns":{"label":{"header":"Label","default":""},"value":{"header":"Value","default":""},"calc":{"header":"Calc Value","default":""},"selected":{"header":"<span class=\"dashicons dashicons-yes\"><\/span>","default":0}}},"manual_value":true},{"errors":[],"max_options":0,"label":"Ki\u1ec3m tra","value":"Ki\u1ec3m tra","calc":"","selected":0,"order":2,"settingModel":{"settings":false,"hide_merge_tags":false,"error":false,"name":"options","type":"option-repeater","label":"Options <a href=\"#\" class=\"nf-add-new\">Add New<\/a> <a href=\"#\" class=\"extra nf-open-import-tooltip\"><i class=\"fa fa-sign-in\" aria-hidden=\"true\"><\/i> Import<\/a>","width":"full","group":"","value":[{"label":"One","value":"one","calc":"","selected":0,"order":0},{"label":"Two","value":"two","calc":"","selected":0,"order":1},{"label":"Three","value":"three","calc":"","selected":0,"order":2}],"columns":{"label":{"header":"Label","default":""},"value":{"header":"Value","default":""},"calc":{"header":"Calc Value","default":""},"selected":{"header":"<span class=\"dashicons dashicons-yes\"><\/span>","default":0}}},"manual_value":true},{"errors":[],"max_options":0,"order":3,"new":false,"options":[],"label":"Chung","value":"Chung","calc":"","selected":0,"settingModel":{"settings":false,"hide_merge_tags":false,"error":false,"name":"options","type":"option-repeater","label":"Options <a href=\"#\" class=\"nf-add-new\">Add New<\/a> <a href=\"#\" class=\"extra nf-open-import-tooltip\"><i class=\"fa fa-sign-in\" aria-hidden=\"true\"><\/i> Import<\/a>","width":"full","group":"","value":[{"label":"One","value":"one","calc":"","selected":0,"order":0},{"label":"Two","value":"two","calc":"","selected":0,"order":1},{"label":"Three","value":"three","calc":"","selected":0,"order":2}],"columns":{"label":{"header":"Label","default":""},"value":{"header":"Value","default":""},"calc":{"header":"Calc Value","default":""},"selected":{"header":"<span class=\"dashicons dashicons-yes\"><\/span>","default":0}}},"manual_value":true}],"container_class":"","element_class":"","admin_label":"","help_text":"","desc_text":"","drawerDisabled":false,"id":45,"beforeField":"","afterField":"","parentType":"list","element_templates":["listselect","input"],"old_classname":"list-select","wrap_template":"wrap"},{"objectType":"Field","objectDomain":"fields","editActive":false,"order":2,"label":"Chi\u1ec1u d\u00e0i nh\u00e0 x\u01b0\u1edfng (m)","type":"number","key":"chieu_dai_nha_xuong_m_1496377098465","label_pos":"above","required":1,"default":"","placeholder":"","container_class":"","element_class":"","manual_key":false,"admin_label":"","help_text":"","desc_text":"","num_min":"","num_max":"","num_step":1,"drawerDisabled":false,"id":46,"beforeField":"","afterField":"","parentType":"number","element_templates":["number","input"],"old_classname":"","wrap_template":"wrap"},{"objectType":"Field","objectDomain":"fields","editActive":false,"order":3,"label":"Chi\u1ec1u r\u1ed9ng nh\u00e0 x\u01b0\u1edfng (m)","type":"number","key":"chieu_rong_nha_xuong_m_1496377120332","label_pos":"above","required":1,"default":"","placeholder":"","container_class":"","element_class":"","manual_key":false,"admin_label":"","help_text":"","desc_text":"","num_min":"","num_max":"","num_step":1,"drawerDisabled":false,"id":47,"beforeField":"","afterField":"","parentType":"number","element_templates":["number","input"],"old_classname":"","wrap_template":"wrap"},{"objectType":"Field","objectDomain":"fields","editActive":false,"order":4,"label":"Chi\u1ec1u cao treo \u0111\u00e8n (m)","type":"number","key":"chieu_cao_treo_den_m_1496377147722","label_pos":"above","required":1,"default":"","placeholder":"","container_class":"","element_class":"","manual_key":false,"admin_label":"","help_text":"","desc_text":"","num_min":"","num_max":"","num_step":1,"drawerDisabled":false,"id":48,"beforeField":"","afterField":"","parentType":"number","element_templates":["number","input"],"old_classname":"","wrap_template":"wrap"},{"objectType":"Field","objectDomain":"fields","editActive":false,"order":5,"label":"\u0110\u1ed9 r\u1ecdi y\u00eau c\u1ea7u (lux)","type":"number","key":"do_roi_yeu_cau_lux_1496377178976","label_pos":"above","required":0,"default":"","placeholder":"","container_class":"","element_class":"","manual_key":false,"admin_label":"","help_text":"","desc_text":"","num_min":"","num_max":"","num_step":1,"drawerDisabled":false,"id":49,"beforeField":"","afterField":"","parentType":"number","element_templates":["number","input"],"old_classname":"","wrap_template":"wrap"},{"objectType":"Field","objectDomain":"fields","editActive":false,"order":6,"label":"HTML","type":"html","default":"<hr>","container_class":"","element_class":"","key":"html_1496377193008","drawerDisabled":false,"id":50,"beforeField":"","afterField":"","label_pos":"above","parentType":"html","value":"<hr>","element_templates":["html","input"],"old_classname":"","wrap_template":"wrap"},{"objectType":"Field","objectDomain":"fields","editActive":false,"order":7,"label":"T\u00ean","type":"firstname","key":"ten_1495787943413","label_pos":"above","required":1,"default":"","placeholder":"","container_class":"","element_class":"","admin_label":"","help_text":"","desc_text":"","custom_name_attribute":"fname","personally_identifiable":1,"id":41,"beforeField":"","afterField":"","parentType":"firstname","element_templates":["firstname","input"],"old_classname":"","wrap_template":"wrap"},{"objectType":"Field","objectDomain":"fields","editActive":false,"order":8,"label":"S\u0110T","type":"phone","key":"sdt_1495787947662","label_pos":"above","required":1,"default":"","placeholder":"","container_class":"","element_class":"","input_limit":"","input_limit_type":"characters","input_limit_msg":"Character(s) left","manual_key":false,"disable_input":"","admin_label":"","help_text":"","desc_text":"","disable_browser_autocomplete":"","mask":"","custom_mask":"","drawerDisabled":false,"custom_name_attribute":"phone","personally_identifiable":1,"id":42,"beforeField":"","afterField":"","parentType":"textbox","element_templates":["tel","textbox","input"],"old_classname":"","wrap_template":"wrap"},{"objectType":"Field","objectDomain":"fields","editActive":false,"order":9,"label":"Email","type":"email","key":"email_1495787935242","label_pos":"above","required":1,"default":"","placeholder":"","container_class":"","element_class":"","admin_label":"","help_text":"","desc_text":"","drawerDisabled":false,"custom_name_attribute":"email","personally_identifiable":1,"id":43,"beforeField":"","afterField":"","parentType":"email","element_templates":["email","input"],"old_classname":"","wrap_template":"wrap"},{"objectType":"Field","objectDomain":"fields","editActive":false,"order":10,"type":"textarea","label":"L\u1eddi nh\u1eafn (N\u1ebfu c\u00f3)","key":"loi_nhan_neu_co_1496822992724","label_pos":"above","required":false,"default":"","placeholder":"","container_class":"","element_class":"","input_limit":"","input_limit_type":"characters","input_limit_msg":"Character(s) left","manual_key":false,"disable_input":"","admin_label":"","help_text":"","desc_text":"","disable_browser_autocomplete":"","textarea_rte":"","disable_rte_mobile":"","textarea_media":"","drawerDisabled":false,"id":51,"beforeField":"","afterField":"","parentType":"textarea","element_templates":["textarea","input"],"old_classname":"","wrap_template":"wrap"},{"objectType":"Field","objectDomain":"fields","editActive":false,"order":11,"label":"G\u1eedi","type":"submit","processing_label":"\u0110ang x\u1eed l\u00fd","container_class":"","element_class":"","key":"gui_1495787974225","drawerDisabled":false,"id":44,"beforeField":"","afterField":"","label_pos":"above","parentType":"textbox","element_templates":["submit","button","input"],"old_classname":"","wrap_template":"wrap-no-label"}];
                    nfForms.push(form);
                </script>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-12">
                    <h1>{{l}}Chương trình thiết kế chiếu sáng nhà xưởng miễn phí{{/l}}</h1>
                    <p>{{l}}Nhằm hỗ trợ các doanh nghiệp có nhu cầu về thiết kế chiếu sáng nhà xưởng công ty TNHH Công Nghệ Năng Lượng POTECH tổ chức chương trình thiết kế chiếu sáng nhà xưởng miễn phí sử dụng thông số của đèn TUANPHAT.{{/l}}</p>
                    <p>{{l}}Để đăng ký tham gia chương trình thiết kế miễn phí bạn hãy thể liên hệ trực tiếp đến số điện thoại hoặc sử dụng biểu mẫu phía bên trái, bạn sẽ được các chuyên gia tư vấn thiết kế và gửi lại mẫu thiết kế chiếu sáng cho nhà xưởng của bạn.{{/l}}</p>
                </div>
                <div class="col-md-4">
                    <div>
                        <div class="supportWidget">
                            <h2>{{l}}Xem mẫu thiết kế{{/l}}</h2>
                            <ul class="list" role="tablist">
                                {{foreach from=$arrDesign key=key item=item}}
                                <li class="nav-item">
                                    <a class="nav-link {{if $key === 0 }}active{{/if}}" data-toggle="tab"
                                       href="#panel{{$item.design_gid}}" role="tab" data-number="{{$item.design_gid}}" id="item{{$item.design_gid}}">{{$item.title}}</a>
                                </li>
                                {{/foreach}}
                            </ul>
                        </div>
                        <div class="supportWidget">
                            <h2>{{l}}Tư vấn miễn phí{{/l}}</h2>
                            <div class="sub_headline">{{l}}Thông tin thêm về chương trình hoặc thắc mắc liên quan.{{/l}}</div>
                            <div class="text-center"><a href="" class="cta_btn">{{$config.hotline}}</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="tab-content">
                        {{foreach from=$arrDesign key=key item=item}}
                        <div class="tab-pane fade show {{if $key === 0 }}active{{/if}}" id="panel{{$item.design_gid}}" role="tabpanel">
                            <object class="vanilla-pdf-embed" data="{{$item.file}}#page=1&view=FitH"
                                    type="application/pdf" width="100%" height="100%">
                                <p><a href="{{$item.file}}">Download the PDF file .</a></p>
                            </object>
                        </div>
                        {{/foreach}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        //Change pdf file click
        $('.nav-link').click(function () {
            //get data id
            var id = $(this).attr("data-number");
            $('.tab-pane').removeClass('active');
            $('.nav-link').removeClass('active');
            $('#panel' + id).addClass('active');
            $('#item' + id).addClass('active');
        });
        $('.ctaMobileV2').css('display', 'none');
    });
</script>