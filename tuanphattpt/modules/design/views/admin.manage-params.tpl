<div class="page-header">
    <h1>
        {{l}}Params{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}List All Params{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">

                {{if $allParams|@count <= 0}}
                <div class="alert alert-info">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{l}}No contact with above conditions.{{/l}}
                </div>
                {{/if}}


                <table id="simple-table" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                    <form id="top-search" name="search" method="post" action="">
                        <th class="center">
                            <button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
                        </th>
                        <th class="center">
                            <input class="check-all" type="checkbox"/>
                        </th>
                        <th>{{l}}Code{{/l}}
                            <input type="text" name="condition[code]" id="username" value="{{$condition.code}}"
                                   placeholder="{{l}}Find code{{/l}}" class="form-control" />
                        </th>

                        <th>{{l}}Công Suất{{/l}}

                        </th>
                        <th>{{l}}Quang Thông{{/l}}

                        </th>
                        <th>{{l}}Dài{{/l}}

                        </th>
                        <th>{{l}}Rộng{{/l}}

                        </th>
                        <th>{{l}}Cao{{/l}}

                        </th>
                        <th>{{l}}Độ Rọi{{/l}}

                        </th>
                        <th>{{l}}Trọng lượng{{/l}}

                        </th>
                        <th>{{l}}Số Giờ{{/l}}

                        </th>
                        <th>{{l}}Created{{/l}}</th>
                        <th>{{l}}Action{{/l}}</th>
                    </form>
                    </tr>
                    </thead>
                    <tbody>
                        {{if $allParams|@count > 0}}
                    <form action="" method="post" name="sortForm">
                        {{foreach from=$allParams item=item name=contact key=key}}
                        <tr>
                            <td class="center">{{$key+1}}</td>
                            <td class="center">
                                <input type="checkbox" value="{{$item.thongso_id}}" name="allItems" class="allItems"/>
                            </td>
                            <td>
                                {{$item.code}}

                            </td>
                            <td>
                                {{$item.congsuat}}

                            </td>
                            <td>
                                {{$item.quangthong}}

                            </td>
                            <td>
                                {{$item.dai}}

                            </td>
                            <td>
                                {{$item.rong}}

                            </td>
                            <td>
                                {{$item.cao}}

                            </td>
                            <td>
                                {{$item.doroi}}

                            </td>
                            <td>
                                {{$item.weight}}

                            </td>
                            <td>
                                {{$item.sogio}}

                            </td>

                            <td>{{$item.created_date}}</td>
                            <td>
                                <span class="tooltip-area">
                                    <a href="{{$APP_BASE_URL}}design/admin/edit-params/id/{{$item.thongso_id}}"
                                       class="btn btn-default btn-sm" title="Edit"><i
                                            class="fa fa-pencil"></i></a>
                                </span>
                            </td>
                        </tr>
                        {{/foreach}}
                    </form>
                    {{/if}}
                    </tbody>
                </table>


            </div><!-- /.span -->
        </div><!-- /.row -->


        <div class="col-lg-12">
            <div class="form-group col-lg-4">
                <label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
                <div class="col-lg-6">
                    <select id="action" class="form-control">
                        <option value=";">{{l}}Choose an action...{{/l}}</option>
                        <option value="DeleteData('{{$APP_BASE_URL}}design/admin/delete-params/id/');">{{l}}Delete{{/l}}</option>
                    </select>
                </div>
                <div class="col-lg-6">
                    <a href="javascript:RunAction();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to
                        selected{{/l}}</a>
                </div>
            </div>
            <div class="form-group col-lg-2">
                <label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
                <div class="col-lg-12">
                    <form class="search" name="search" method="post" action="">
                        <select name="displayNum" onchange="this.parentNode.submit();" class="form-control">
                            <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
                            <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
                            <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
                            <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
                            <option value="1000000000" {{if $displayNum >= 1000000000}}
                                    selected="selected" {{/if}}>{{l}}All{{/l}}</option>
                        </select>
                    </form>
                </div>
            </div>
            {{if $countAllPages > 1}}
            <div class="col-lg-6 pagination">
                {{if $first}}
                <a href="?page=1" class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
                {{/if}}
                {{if $prevPage}}
                <a href="?page={{$prevPage}}" class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
                {{/if}}

                {{foreach from=$prevPages item=item}}
                <a href="?page={{$item}}" class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                <a href="#" class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>

                {{foreach from=$nextPages item=item}}
                <a href="?page={{$item}}" class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                {{if $nextPage}}
                <a href="?page={{$nextPage}}" class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
                {{/if}}
                {{if $last}}
                <a href="?page={{$countAllPages}}" class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}}
                    &raquo;</a>
                    {{/if}}

            </div>
            {{/if}}
        </div>
    </div>


</div>
</div>

<script type="text/javascript" defer src="{{$LAYOUT_HELPER_URL}}libs/common/js/admin.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //Admin ready
        AdminReady();
    });
</script>