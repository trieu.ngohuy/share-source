<div class="page-header">
    <div class="container">
        <h1>{{l}}Tính toán chiếu sáng nhà xưởng{{/l}}</h1>
    </div>
</div>
<div class="container">
    <div class="content" id="divCalculationWrap">
        {{sticker name=front_popup}}
    </div>
</div>
<br>
<script type="text/javascript">
    var arrData = {{$arrParams|@json_encode}};
    var msgKhuyendung = '{{l}}TUANPHAT KHUYÊN DÙNG{{/l}}';
    var msgCustom = '{{l}}TÙY CHỌN ĐÈN TUANPHAT{{/l}}';
    var msgCode = '{{l}}Mã đèn{{/l}}';
    var msgCongsuat = '{{l}}Công suất (W){{/l}}';
    var msgQuangthong = '{{l}}Quang thông (Lumen){{/l}}';
    var msgSoluong = '{{l}}Số lượng đèn{{/l}}';
    var msgTuoitho = '{{l}}Tuổi thọ đèn{{/l}}';
    var msgYear = '{{l}}năm{{/l}}';
    //Document Ready
    jQuery(document).ready(function(){
        jQuery('div#divPopup').css({
            'display' : 'block',
            'position' : 'relative',
            'padding-left': '0px',
            'padding-right': '0px',
        });
        jQuery('div#divPopup #imgClose').css('display', 'none');
        jQuery('div#divPopup h3, div#divPopup p').css({
            'background' : '#52b84b',
            'color' : '#fff',
            'padding': '5px'
        });
        jQuery('div#divPopup h3').css({
            'margin-bottom' : '0px'
        });
        jQuery('#divCalculationWrap').css({
            'margin-top' : '-80px'
        });
        jQuery('#divPopup .tab-content').css({
            'padding-left' : '0px',
            'padding-right' : '0px'
        });
        jQuery('.ctaMobileV2').css('display', 'none');
    });
</script>
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}front/js/calculation.js"></script>
