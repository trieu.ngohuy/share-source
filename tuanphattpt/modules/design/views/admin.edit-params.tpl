<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<script type="text/javascript">
    //<![CDATA[

    jQuery(document).ready(function () {
        CKFinder.setupCKEditor(null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/');
        jQuery("#images").sortable();
        jQuery("#images").disableSelection();
        //Make slug
        {{foreach from=$allLangs item=item name=langDiv}}
        jQuery('#title{{$smarty.foreach.langDiv.iteration}}').makeSlug({
            slug: jQuery('#alias{{$smarty.foreach.langDiv.iteration}}')
        });
        {{/foreach}}
        //Display images
        jQuery(".input_image[value!='']").parent().find('div').each(function (index, element) {
            jQuery(this).toggle();
        });
    });
    var imgId;

    function chooseImage(id) {
        imgId = id;
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();
        finder.basePath = '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/'; // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.selectActionFunction = setFileField;
        finder.popup();
    }

    // This is a sample function which is called when a file is selected in CKFinder.
    function setFileField(fileUrl) {
        document.getElementById('chooseImage_img' + imgId).src = fileUrl;
        document.getElementById('chooseImage_input' + imgId).value = fileUrl;
        document.getElementById('chooseImage_div' + imgId).style.display = '';
        document.getElementById('chooseImage_noImage_div' + imgId).style.display = 'none';
    }

    function clearImage(imgId) {
        document.getElementById('chooseImage_img' + imgId).src = '';
        document.getElementById('chooseImage_input' + imgId).value = '';
        document.getElementById('chooseImage_div' + imgId).style.display = 'none';
        document.getElementById('chooseImage_noImage_div' + imgId).style.display = '';
    }

    function addMoreImg() {
        jQuery("ul#images > li.hidden").filter(":first").removeClass('hidden');
    }

    //]]>
</script>
<style type="text/css">
    #images {
        list-style-type: none;
        margin: 0;
        padding: 0;
    }

    #images li {
        margin: 10px;
        float: left;
        text-align: center;
        height: 180px;
    }
</style>


<div class="page-header">
    <h1>
        {{l}}Params{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}Edit Params{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">
                {{if $errors|@count > 0}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{if $errors.main}}
                    </strong> {{$errors.main}}
                    {{else}}
                    {{l}}Please check following information again{{/l}}
                    {{/if}}
                </div>
                {{/if}}


                <div class="col-sm-12 widget-container-col ui-sortable">

                    <div class="tabbable tabs-left">

                        <ul class="nav nav-tabs" id="myTab2">
                            <li {{if $lid == ''}}class="active"{{/if}}>
                                <a href="#tab0" data-toggle="tab" aria-expanded="true">
                                    <i class="pink ace-icon fa fa-home bigger-110"></i>
                                    {{l}}Basic{{/l}}
                                </a>
                            </li>
                        </ul>

                        <form action="" method="post" class="form-horizontal">
                            <div class="tab-content">

                                <div id="tab0" class="tab-pane {{if $lid == ''}}active{{/if}}">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Code{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="data[code]"
                                                   value="{{$data.code}}" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Công Suất{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control" name="data[congsuat]"
                                                   value="{{$data.congsuat}}" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Quang Thông{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control" name="data[quangthong]"
                                                   value="{{$data.quangthong}}" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Dài{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control" name="data[dai]"
                                                   value="{{$data.dai}}" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Rộng{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control" name="data[rong]"
                                                   value="{{$data.rong}}" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Cao{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control" name="data[cao]"
                                                   value="{{$data.cao}}" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Độ rọi{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control" name="data[doroi]"
                                                   value="{{$data.doroi}}" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Trọng lượng{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control" name="data[weight]"
                                                   value="{{$data.weight}}" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Số giờ{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control" name="data[sogio]"
                                                   value="{{$data.sogio}}" required="">
                                        </div>
                                    </div>
                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Save{{/l}}
                                            </button>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>


            </div><!-- /.span -->

        </div><!-- /.row -->


    </div>


</div>
