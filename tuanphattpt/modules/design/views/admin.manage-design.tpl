<div class="page-header">
    <h1>
        {{l}}Design{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}List All Design{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">

                <table id="simple-table" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <form id="top-search" name="search" method="post" action="">

                            <th class="center">
                                <button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
                            </th>
                            <th class="center">
                                <input class="check-all" type="checkbox" />
                            </th>
                            <th>{{l}}Created{{/l}}</th>


                            <th colspan="2">
                                <div class="col-sm-4">
                                    <input  type="text" name="condition[keyword]" id="username" value="{{$condition.keyword}}"" placeholder="{{l}}Find Title{{/l}}" class="form-control" />
                                </div>
                            </th>
                        </form>
                    </tr>
                    </thead>

                    <tbody>
                    {{if $allDesign|@count > 0}}
                    <form action="" method="post" name="sortForm">
                        {{foreach from=$allDesign item=item name=design key=key}}

                        <tr>
                            <td class="center">{{$key+1}}</td>
                            <td class="center"><input type="checkbox" value="{{$item.design_gid}}" name="allDesign" class="allDesign"/></td>

                            <td>{{$item.created_date}}</td>

                            <td colspan="2" style="padding:0px;">
                                <!-- All languages -->
                                <table style="border-collapse: separate;margin-bottom: 0px" class="table table-bordered table-striped">
                                    <tbody id="table{{$item.design_id}}">

                                    {{foreach from=$item.langs item=item2}}
                                    <tr>
                                        <td width="90%">{{$item2.title}}</td>
                                        <td  class="center">
                                            <span class="tooltip-area">
                                                    <a href="{{$APP_BASE_URL}}design/admin/edit-design/gid/{{$item.design_gid}}/lid/{{$item2.lang_id}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                                </span>
                                        </td>
                                    </tr>
                                    {{/foreach}}

                                    </tbody>
                                </table>
                            </td>

                        </tr>
                        {{/foreach}}
                    </form>
                    {{/if}}
                    </tbody>
                </table>


            </div><!-- /.span -->
        </div><!-- /.row -->


        <div class="col-lg-12">
            <div class="form-group col-lg-4">
                <label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
                <div class="col-lg-6">
                    <select id="action" class="form-control" >
                        <option value=";">{{l}}Choose an action...{{/l}}</option>
                        {{p name=delete_design module=design}}
                        <option value="deleteDesign();">{{l}}Delete{{/l}}</option>
                        {{/p}}
                        {{if $CheckGenalbel}}
                        <option value="enableDesign();">{{l}}Enable{{/l}}</option>
                        <option value="disableDesign();">{{l}}Disable{{/l}}</option>
                        {{/if}}
                    </select>
                </div>
                <div class="col-lg-6">
                    <a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
                </div>
            </div>
            <div class="form-group col-lg-2">
                <label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
                <div class="col-lg-12">
                    <form class="search" name="search" method="post" action="">
                        <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
                            <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
                            <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
                            <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
                            <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
                            <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
                        </select>
                    </form>
                </div>
            </div>
            {{if $countAllPages > 1}}
            <div class="col-lg-6 pagination">
                {{if $first}}
                <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
                {{/if}}
                {{if $prevPage}}
                <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
                {{/if}}

                {{foreach from=$prevPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>

                {{foreach from=$nextPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                {{if $nextPage}}
                <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
                {{/if}}
                {{if $last}}
                <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
                {{/if}}

            </div>
            {{/if}}
        </div>
    </div>


</div>
</div>

<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        $('.close').click(function () {
            $(this).parent().hide("slow");
        });
        $('.check-all').click(function () {
            if (this.checked) { // check select status
                $('.allDesign').each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"
                });
            } else {
                $('.allDesign').each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"
                });
            }
        });
    });

    function applySelected()
    {
        var task = document.getElementById('action').value;
        eval(task);
    }
    function enableDesign()
    {
        var all = document.getElementsByName('allDesign');
        var tmp = '';
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
            }
        }
        if ('' == tmp) {
            alert('Please choose an design');
        }
        window.location.href = '{{$APP_BASE_URL}}design/admin/enable-design/gid/' + tmp;
    }

    function disableDesign()
    {
        var all = document.getElementsByName('allDesign');
        var tmp = '';
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
            }
        }
        if ('' == tmp) {
            alert('Please choose an design');
        }
        window.location.href = '{{$APP_BASE_URL}}design/admin/disable-design/gid/' + tmp;
    }

    function deleteDesign()
    {
        var all = document.getElementsByName('allDesign');
        var tmp = '';
        var count = 0;
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
                count++;
            }
        }
        if ('' == tmp) {
            alert('Please choose an design');
            return;
        } else {
            result = confirm('Are you sure you want to delete ' + count + ' design(s)?');
            if (false == result) {
                return;
            }
        }
        window.location.href = '{{$APP_BASE_URL}}design/admin/delete-design/gid/' + tmp;
    }


    function deleteADesign(id)
    {
        result = confirm('Are you sure you want to delete this design?');
        if (false == result) {
            return;
        }
        window.location.href = '{{$APP_BASE_URL}}design/admin/delete-design/gid/' + id;
    }
</script>