<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<script type="text/javascript">
    //<![CDATA[

    jQuery(document).ready(function () {
    {{foreach from=$allLangs item=item name=langDiv}}
        jQuery('#name{{$smarty.foreach.langDiv.iteration}}').makeSlug({
            slug: jQuery('#alias{{$smarty.foreach.langDiv.iteration}}')
        });
    {{/foreach}}

    });

    //]]>
</script>
<div class="page-header">
    <h1>
        {{l}}Design{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}New Design{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">


                <div class="col-sm-12 widget-container-col ui-sortable">

                    {{if $designMessage|@count > 0}}
                    <div class="alert alert-danger">
                        <button class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>
                        {{$designMessage.message}}.
                    </div>
                    {{/if}}
                    <div class="tabbable tabs-left">

                        <ul class="nav nav-tabs" id="myTab2">
                            {{if $fullPermisison}}
                            <li class="active">
                                <a href="#tab0" data-toggle="tab" aria-expanded="true">
                                    <i class="pink ace-icon fa fa-home bigger-110"></i>
                                    {{l}}Basic{{/l}}
                                </a>
                            </li>
                            {{/if}}

                            {{foreach from=$allLangs item=item index=index name=langTab}}
                            <li>
                                <a href="#tab{{$item.lang_id}}" data-toggle="tab" aria-expanded="true">
                                    <image style="vertical-align:middle;" src="{{$BASE_URL}}{{$item.lang_image}}"> {{$item.name}}
                                </a>
                            </li>
                            {{/foreach}}
                        </ul>

                        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <div class="tab-content">

                                <div id="tab0" class="tab-pane active">
                                    <div class="form-group has-info">
                                        <label class="control-label col-md-2">{{l}}Attach File{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="file" id="id-input-file-2" class="form-control" name="file" />
                                        </div>
                                    </div>
                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Save{{/l}}
                                            </button>
                                        </div>
                                    </div>

                                </div>

                                {{foreach from=$allLangs item=item name=langDiv}}
                                <div class="tab-pane" id="tab{{$item.lang_id}}">
                                    <div class="form-group has-info">
                                        <label class="control-label col-md-2">{{l}}Title{{/l}}</label>
                                        <div class="col-md-10">
                                            <input id="name{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][title]"  value="{{$data[$item.lang_id].title}}" >
                                        </div>
                                    </div>

                                    <div class="form-group has-info">
                                        <label class="control-label col-md-2">{{l}}Alias{{/l}}</label>
                                        <div class="col-md-10">
                                            <input id="alias{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][alias]"  value="{{$data[$item.lang_id].alias}}" >.html
                                        </div>
                                    </div>

                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Save{{/l}}
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                {{/foreach}}

                            </div>

                        </form>

                    </div>

                </div>



            </div><!-- /.span -->

        </div><!-- /.row -->



    </div>


</div>
<script type="text/javascript">
    jQuery(function ($) {
        $('#id-input-file-2').ace_file_input({
            no_file: 'No File ...',
            btn_choose: 'Choose',
            btn_change: 'Change',
            droppable: false,
            onchange: null,
            thumbnail: false //| true | large
                    //whitelist:'gif|png|jpg|jpeg'
                    //blacklist:'exe|php'
                    //onchange:''
                    //
        });
    });
</script>

