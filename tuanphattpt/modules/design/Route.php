<?php
class Route_Design
{
    public function build($linkArr, $params = array())
    {
        /*
         * Check current language
         */
        $result = implode('/', $linkArr);
        if ('index' == @$linkArr[2]) {
            if(Nine_Language::getCurrentLangCode() == 'vi'){
                $result = "thiet-ke-chieu-sang-nha-xuong-mien-phi.html";
            }else{
                $result = "lighting-house-designs-free.html";
            }
        }else{
            if(Nine_Language::getCurrentLangCode() == 'vi'){
                $result = "tinh-toan-chieu-sang.html";
            }else{
                $result = "calculation-of-lights-house.html";
            }
        }

        return $result;
    }

    public function parse()
    {
        $router = Nine_Controller_Front::getInstance()->getRouter();
        /*
         * Design (En)
         */
        $designEn = new Zend_Controller_Router_Route_Regex(
            'lighting-house-designs-free.html',
            array(
                'module'     => 'design',
                'controller' => 'index',
                'action'     => 'index'
            )
        );
        $router->addRoute('designEn', $designEn);
        /*
         * Design (Vi)
         */
        $designVi = new Zend_Controller_Router_Route_Regex(
            'thiet-ke-chieu-sang-nha-xuong-mien-phi.html',
            array(
                'module'     => 'design',
                'controller' => 'index',
                'action'     => 'index'
            )
        );
        $router->addRoute('designVi', $designVi);
        /*
         * Calculation (En)
         */
        $calculationEn = new Zend_Controller_Router_Route_Regex(
            'calculation-of-lights-house.html',
            array(
                'module'     => 'design',
                'controller' => 'index',
                'action'     => 'calculation'
            )
        );
        $router->addRoute('calculationEn', $calculationEn);
        /*
         * Calculation (Vi)
         */
        $calculationVi = new Zend_Controller_Router_Route_Regex(
            'tinh-toan-chieu-sang.html',
            array(
                'module'     => 'design',
                'controller' => 'index',
                'action'     => 'calculation'
            )
        );
        $router->addRoute('calculationVi', $calculationVi);
    }
}