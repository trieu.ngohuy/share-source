<?php

class Route_Banner
{
    /**
     * Build friendly URL
     * @param array $linkArr 0 => <module>, 1 => controller, 2 => action, 3...n param/value
     * @param array $params param => value
     *
     * @return string friendly URL
     */
    public function build($linkArr, $params = array())
    {

        $result = implode('/', $linkArr);
        
        return $result;

    }

    /**
     * Parse friendly URL
     */
    public function parse()
    {
        $router = Nine_Controller_Front::getInstance()->getRouter();
       

        $route = new Zend_Controller_Router_Route_Regex(
            'estore/manage-banner/(.*).html',
            array(
                'module' => 'banner',
                'controller' => 'estore',
                'action' => 'manage-banner'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('banner', $route);

        $route1 = new Zend_Controller_Router_Route_Regex(
            'estore/new-banner/(.*).html',
            array(
                'module' => 'banner',
                'controller' => 'estore',
                'action' => 'new-banner'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('banner1', $route1);

        $route2 = new Zend_Controller_Router_Route_Regex(
            'estore/edit-banner/(.*)/([0-9]+)',
            array(
                'module' => 'banner',
                'controller' => 'estore',
                'action' => 'edit-banner'
            ),
            array(
                1 => 'alias',
                2 => 'gid'
            )
        );
        $router->addRoute('banner2', $route2);
        
        $route3 = new Zend_Controller_Router_Route_Regex(
            'estore/delete-banner/(.*)/(.*)',
            array(
                'module' => 'banner',
                'controller' => 'estore',
                'action' => 'delete-banner'
            ),
            array(
                1 => 'alias',
                2 => 'gid',
            )
        );
        $router->addRoute('banner3', $route3);
        

    }
}