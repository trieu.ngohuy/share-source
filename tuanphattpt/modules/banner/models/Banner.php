<?php
/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2009 visualidea.org
 * @license    http://license.visualidea.org
 * @version    v 1.0 2009-04-15
 */
require_once 'Nine/Model.php';
require_once 'modules/banner/models/BannerCategory.php';

class Models_Banner extends Nine_Model
{ 
    /**
     * The primary key column or columns.
     * A compound key should be declared as an array.
     * You may declare a single-column primary key
     * as a string.
     *
     * @var mixed
     */
    protected $_primary = 'banner_id';
    /**
     * The field name what we use to group all language rows together
     * 
     * @var string
     */
    protected $_groupField = 'banner_gid';
    /**
     * Let system know this is miltilingual table or not.
     * If this table has multilingual fields, Zend_Db_Table_Select object
     * will be inserted language condition automatically.
     * 
     * @var array
     */
    protected $_multilingualFields = array('title', 'alias', 'intro_text', 'full_text', 'hit', 
                                           'last_view_date',  'meta_keywords', 'meta_description', 'enabled','tag','images');
    
    /**
     * Constructor.
     *
     * Supported params for $config are:
     * - db              = user-supplied instance of database connector,
     *                     or key name of registry instance.
     * - name            = table name.
     * - primary         = string or array of primary key(s).
     * - rowClass        = row class name.
     * - rowsetClass     = rowset class name.
     * - referenceMap    = array structure to declare relationship
     *                     to parent tables.
     * - dependentTables = array of child tables.
     * - metadataCache   = cache for information from adapter describeTable().
     *
     * @param  mixed $config Array of user-specified config options, or just the Db Adapter.
     * @return void
     */
    public function __construct($config = array())
    {
        $this->_name = $this->_prefix . 'banner';
        return parent::__construct($config); 
    }

    /**
     * Get all banner with conditions
     * 
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
//    public function getAllBannersWithDefaultLang($condition = array(), $order = null, $count = null, $offset = null)
//    {
//        $select = $this->select()
//                ->setIntegrityCheck(false)
//                ->from(array('s' => $this->_name), array('banner_id', 'senabled' => 'enabled', 'publish_up_date' => 'publish_up_date', 'publish_down_date' => 'publish_down_date' , 'ssorting' => 'sorting', 'created_date' => 'created_date'))
//                ->join(array('sl' => $this->_prefix . 'banner_lang'), 's.banner_id = sl.banner_id')
//                ->join(array('sc' => $this->_prefix . 'banner_category'), 's.banner_category_gid = sc.banner_category_gid', array('cname' => 'name'))
//                ->order($order)
//                ->limit($count, $offset);
//        /**
//         * Conditions
//         */
//        if (null != @$condition['keyword']) {
//            $select->where($this->getAdapter()->quoteInto('sl.title LIKE ?', "%{$condition['keyword']}%"));
//        }
//        if (null != @$condition['banner_category_gid']) {
//            $select->where("s.banner_category_gid = ?", $condition['banner_category_gid']);
//        }
//        if (null != @$condition['lang_id']) {
//            $select->where("sl.lang_id = ?", $condition['lang_id']);
//        }
//        
//        return $this->fetchAll($select)->toArray();
//    }

    /**
     * Get all banner with conditions
     * 
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
	public function getAllBannerByRandom() 
    {
    	$select = $this->select()
                ->where('genabled = 1');
         
         $idString = '';
         $sql = "SELECT banner_gid FROM {$this->_name} Where tag != '' AND lang_id = '".Nine_Language::getCurrentLangId()."' ORDER BY RAND() LIMIT 20";
         $ids = $this->_db->fetchAll($sql);
     
            foreach ($ids as $row) {
                $idString .= $row['banner_gid'] . ',';
            }
    	if($idString == ""){
        	return array();
        }
        $select->where('banner_gid IN (' . trim($idString, ',') .')');
        $select->where('banner_category_gid !=?', '116');
        $select->where('banner_category_gid !=?', '118');
        $select->where('lang_id=?', Nine_Language::getCurrentLangId());
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllBannerByParentRandom($parent_id) 
    {
    	$select = $this->select()
                ->where('genabled = 1');
         
         
         $sqlcate = "SELECT banner_category_gid FROM 9_banner_category Where parent_id = '".$parent_id."' AND lang_id = '".Nine_Language::getCurrentLangId()."'";
         $idCatAll = '';
     
    	$idcates = $this->_db->fetchAll($sqlcate);
        foreach ($idcates as $rows) {
           $idCatAll .= $rows['banner_category_gid'] . ',';
        }
    	if($idCatAll == ""){
        	return array();
        }  
        
         $idString = '';
         $sql = "SELECT banner_gid FROM {$this->_name} Where banner_category_gid IN(".trim($idCatAll, ',').") AND tag != '' AND lang_id = '".Nine_Language::getCurrentLangId()."' ORDER BY RAND() LIMIT 20";
         $ids = $this->_db->fetchAll($sql);
     
            foreach ($ids as $row) {
                $idString .= $row['banner_gid'] . ',';
            }
    	if($idString == ""){
        	return array();
        }
        $select->where('banner_gid IN (' . trim($idString, ',') .')');
        $select->where('banner_category_gid !=?', '116');
        $select->where('banner_category_gid !=?', '118');
        $select->where('lang_id=?', Nine_Language::getCurrentLangId());
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllBannerByCateRandom($cateid) 
    {
    	$select = $this->select()
                ->where('genabled = 1');
         
                
        
         $idString = '';
         $sql = "SELECT banner_gid FROM {$this->_name} Where banner_category_gid = '".$cateid."' AND tag != '' AND lang_id = '".Nine_Language::getCurrentLangId()."' ORDER BY RAND() LIMIT 20";
         $ids = $this->_db->fetchAll($sql);
     
            foreach ($ids as $row) {
                $idString .= $row['banner_gid'] . ',';
            }
        if($idString == ""){
        	return array();
        }
        $select->where('banner_gid IN (' . trim($idString, ',') .')');
        $select->where('lang_id=?', Nine_Language::getCurrentLangId());
        $select->where('banner_category_gid !=?', '116');
        $select->where('banner_category_gid !=?', '118');
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
    public function getAllBannerByDate($dateto , $datefrom , $user_id){
    	$select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->where('c.lang_id=?', 2);
        $select->where('c.user_id= ?' , $user_id);
        $select->where('c.created_date > ?' , $dateto);
        $select->where('c.created_date < ?' , $datefrom);
       	return $this->fetchAll($select)->toArray();
    }
    public function getAllBanner($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->joinLeft(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username'))
                ->joinLeft(array('p' => $this->_prefix . 'user_profile'), 'u.user_id = p.user_id AND p.lang_id = '.Nine_Language::getCurrentLangId(), array('padd' => 'add', 'pcompany_name' => 'company_name', 'pnote' => 'note', 'pabout' => 'about', 'pproduct_chinh' => 'product_chinh', 'pproduct_khac' => 'product_khac'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
    	if (null != @$condition['user_id']) {
        	
        	$select->where('c.user_id= ?' , $condition['user_id']);
        	
        }
    	if (null != @$condition['page']) {
        	
        	$select->where('c.page= ?' , $condition['page']);
        	
        }
    	if (null != @$condition['location']) {
        	
        	$select->where('c.location= ?' , $condition['location']);
        	
        }
        if (null != @$condition['keyword']) {
            $sql = "SELECT banner_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('alias LIKE ?', "%{$condition['keyword']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['banner_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.banner_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['banner_category_gid']) {
        	
        	$sql = "SELECT gid_string FROM {$this->_prefix}" . 'banner_category' . " WHERE " . $this->getAdapter()->quoteInto('banner_category_gid = ? ', $condition['banner_category_gid']);
        	$ids = @reset($this->_db->fetchAll($sql));
        	if (null == $ids) {
        	    $ids = '0';
        	}
            /**
             * Add to select object
             */
            //$select->where('c.banner_category_gid IN (' . trim($ids['banner_category_gid'], ',') .')');
        	
        }
        
    	if (null != @$condition['genabled']) {
        	
        	$sql = "SELECT banner_gid FROM {$this->_name} WHERE genabled = " . $condition['genabled'];
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['banner_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.banner_gid IN (' . trim($idString, ',') .')');
        	
        }
        
    	if (null != @$condition['username']) {
            
            $select->where($this->getAdapter()->quoteInto('u.username LIKE ?', "%{$condition['username']}%" ));
        }
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAlltinlienquan($gid,$id)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.banner_category_gid = ?', $gid)
                ->where('c.banner_id <> ?', $id)
                ->order('c.banner_id Desc')
                ->limit(10);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    
	public function getAllBannerNew()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.created_date DESC")
                ->limit(10);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
 	public function getAllBannerByArrCat($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
        if (null != @$condition['keyword']) {
            $sql = "SELECT banner_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('alias LIKE ?', "%{$condition['keyword']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['banner_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.banner_gid IN (' . trim($idString, ',') .')');
        }
    	if (null != @$condition['name']) {
            $sql = "SELECT banner_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('alias LIKE ?', "%{$condition['name']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['banner_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.banner_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['banner_category_gid']) {
        	
        	$sql = "SELECT gid_string FROM {$this->_prefix}" . 'banner_category' . " WHERE " . $this->getAdapter()->quoteInto('banner_category_gid = ? ', $condition['banner_category_gid']);
        	$ids = @reset($this->_db->fetchAll($sql));
        	if (null == $ids) {
        	    $ids = '0';
        	}
            /**
             * Add to select object
             */
            $select->where('c.banner_category_gid IN (' . trim($ids['gid_string'], ',') .')');
        	
        }
        
    	if (null != @$condition['genabled']) {
        	
        	$sql = "SELECT banner_gid FROM {$this->_name} WHERE genabled = " . $condition['genabled'];
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['banner_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.banner_gid IN (' . trim($idString, ',') .')');
        	
        }
        if(null != @$condition['str_category']){
        	$select->where('c.banner_category_gid IN (' . trim($condition['str_category'], ',') .')');
        }
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllBannerNewByCatGid($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.created_date DESC")
                ->limit(4);
         $select->where('cc.parent_id = ? or cc.parent_id IS NULL and c.banner_category_gid=?' , $gid , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllBannerByCatGid($arrgid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.banner_category_gid IN(?)', $arrgid)
                ->limit(2)
                ->order("c.created_date DESC");
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    
	public function getAllBannerNewByCatGidIndex($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.genabled=?', 1)
                ->order("c.created_date DESC")
                ->limit(5);
         $select->where('c.banner_category_gid=?' , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllBannerNewByCatGidCateLimit($gid , $limit)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.genabled=?', 1)
                ->order("c.created_date DESC")
                ->limit($limit);
         $select->where('c.banner_category_gid=?' , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllBannerNewByCatGidDetail($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.created_date DESC");
         $select->where('cc.parent_id = ? or cc.parent_id IS NULL and c.banner_category_gid=?' , $gid , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    public function getAllBannerNewByCatGidCon($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.created_date DESC");
         $select->where('c.banner_category_gid=?' , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getBannerHotOne()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.like DESC")
                ->limit(1);
        /**
         * Conditions
         */
        
        return @reset($this->fetchAll($select)->toArray());
    }
	public function getBannerEvent()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.banner_category_gid=?', '116')
                ->where('c.genabled=?', '1')
                ->order("c.publish_up_date DESC");
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllBannerHot()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.like DESC")
                ->limit(5);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllBannerView()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.count_view DESC")
                ->order(" c.publish_up_date DESC")
                ->limit(4);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllBannerComment()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.comment DESC")
                ->order("c.publish_up_date DESC")
                ->limit(4);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
public function getAllBannerLike()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.like DESC")
                ->order("c.publish_up_date DESC")
                ->limit(4);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    
	public function getAllBannerTinNhanh()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'banner_category'), 'c.banner_category_gid = cc.banner_category_gid', array('cname' => 'name','banner_deleteable' => 'banner_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.tinnhanh=?', '1')
                ->order("c.publish_up_date DESC")
                ->limit(4);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    
    
	public function getAllEnabledBannerByCategory( $catGid, $condition = array(), $order = null, $count = null, $offset = null)
    {
    	
    	$objCat = new Models_BannerCategory();
    	
    	$cat = @reset($objCat->getByColumns(array('banner_category_gid=?' => $catGid))->toArray());
    	
    	$gidStr = @trim($cat['gid_string'].',0',','); 
    	/**
    	 * Get all enabled categories
    	 */
    	$select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_prefix . 'banner_category'), array('banner_category_gid'))
                ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
                ->where('cc.banner_category_gid IN (' . $gidStr .')');
                
        $cats   = $this->fetchAll($select)->toArray();
        $gidStr = '';
        foreach ($cats as $cat) {
        	$gidStr .= $cat['banner_category_gid'] . ',';
        }
        $gidStr = @trim($gidStr.'0',',');
    	
        /**
         * Get all enabled banners in enabled categories
         */
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->where('banner_category_gid IN (' . $gidStr .')')
                ->order($order)
                ->limit($count, $offset);

         /**
          * Condition
          */       
         if (null != @$condition['exclude_banner_gids']) {
         	$gidStr = trim($condition['exclude_banner_gids'].',0',',');
         	$select->where('banner_gid NOT IN (' . $gidStr .')');
         }
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledBannersearch( $search, $order = null, $count = null, $offset = null)
    {
    	
    	
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->order($order)
                ->limit($count, $offset);
         
        $idString = '';

        $sql = "SELECT banner_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('title LIKE ?', "%{$search}%");
        $ids = $this->_db->fetchAll($sql);
    	
		foreach ($ids as $row) {
     		$idString .= $row['banner_gid'] . ',';
   		}
   		
   		if($idString != ""){
   		}else{
   			$sql = "SELECT banner_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('full_text LIKE ?', "%{$search}%") ;
	   		$ids = $this->_db->fetchAll($sql);
			foreach ($ids as $row) {
	     		$idString .= $row['banner_gid'] . ',';
	   		}
   		}
    	
    	
   		
   		$search = $this->convert_vi_to_en($search);
   		$search = str_replace(" ", "-" , $search);
   		
   		if($idString != ""){
   		}else{
   			$sql = "SELECT banner_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('alias LIKE ?', "%{$search}%");
	   		$ids = $this->_db->fetchAll($sql);
	    	
			foreach ($ids as $row) {
	     		$idString .= $row['banner_gid'] . ',';
	   		}
   		}
    	
       
        $select->where('banner_gid IN (' . trim($idString, ',') .')');
        $select->where('banner_category_gid !=?', '116');
        $select->where('banner_category_gid !=?', '118');
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledBannerTag( $search, $order = null, $count = null, $offset = null)
    {
    	
    	
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->order($order)
                ->limit($count, $offset);
         
         $idString = '';
         $sql = "SELECT banner_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('tag LIKE ?', "%{$search}%");
         $ids = $this->_db->fetchAll($sql);
         if (empty($ids)) {
         	return array();
            }
            foreach ($ids as $row) {
                $idString .= $row['banner_gid'] . ',';
            }
        
       
        $select->where('banner_gid IN (' . trim($idString, ',') .')');
        $select->where('banner_category_gid !=?', '116');
        $select->where('banner_category_gid !=?', '118');
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledBannerTagAdmin( $search, $id_s = null)
    {
    	
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->order("created_date DESC");
         
         $idString = '';
         $sql = "SELECT banner_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('tag LIKE ?', "%{$search}%" )."";
         $ids = $this->_db->fetchAll($sql);
         if (empty($ids)) {
         	return array();
            }
            foreach ($ids as $row) {
                $idString .= $row['banner_gid'] . ',';
            }
        
       	if(trim($id_s, ',') != ""){
       		$select->where('banner_gid NOT IN (' . trim($id_s, ',') .')');
       	}
        $select->where('banner_gid IN (' . trim($idString, ',') .')');
        
        $select->where('lang_id = 2');
        $select->where('banner_category_gid !=?', '116');
        $select->where('banner_category_gid !=?', '118');
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledBannerByidUser( $user, $condition = array(), $order = null, $count = null, $offset = null)
    {
    	
    	$objCat = new Models_BannerCategory();
    	
                
        
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->where('user_id = ?' , $user)
                ->order($order)
                ->limit($count, $offset);

    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledBannerByidUserLike( $user, $condition = array(), $order = null, $count = null, $offset = null)
    {
    	
    	$objCat = new Models_BannerCategory();
    	
                
        
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->order($order)
                ->limit($count, $offset);
        $select->where('user_like LIKE ?', ',' . $user .',');

    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledBannerByCategory2( $catGid, $condition = array(), $order = null, $count = null, $offset = null)
    {
    	
    	$objCat = new Models_BannerCategory();
    	
    	$cat = @reset($objCat->getByColumns(array('banner_category_gid=?' => $catGid))->toArray());
    	
    	$gidStr = @trim($cat['gid_string'].',0',','); 
    	/**
    	 * Get all enabled categories
    	 */
    	$select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_prefix . 'banner_category'), array('banner_category_gid'))
                ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
                ->where('cc.banner_category_gid IN (' . $gidStr .')');
                    
        $cats   = $this->fetchAll($select)->toArray();
        $gidStr = '';
        foreach ($cats as $cat) {
        	$gidStr .= $cat['banner_category_gid'] . ',';
        }
        $gidStr = @trim($gidStr.'0',',');
    	
        /**
         * Get all enabled banners in enabled categories
         */
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->where('banner_category_gid IN (' . $gidStr .')')
                ->order($order)
                ->limit($count, $offset);

         /**
          * Condition
          */       
         if (null != @$condition['exclude_banner_gids']) {
         	$gidStr = trim($condition['exclude_banner_gids'].',0',',');
         	$select->where('banner_gid NOT IN (' . $gidStr .')');
         }
               
    	return $this->fetchAll($select)->toArray();
    }
    public function getLatestBannerByCategory( $catGid )
    {
    	$allBanner = $this->getAllEnabledBannerByCategory($catGid, array(), array('sorting ASC','banner_gid DESC','banner_id DESC'),1,0);
    	return @reset($allBanner);
    }
    public function convert_vi_to_en($str) {
	    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
	    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
	    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
	    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
	    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
	    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
	    $str = preg_replace("/(đ)/", 'd', $str);
	    $str = preg_replace("/(D)/", 'd', $str);
	    $str = preg_replace("/(L)/", 'l', $str);
	    $str = preg_replace("/(K)/", 'k', $str);
	    $str = preg_replace("/(Q)/", 'q', $str);
	    $str = preg_replace("/(R)/", 'r', $str);
	    $str = preg_replace("/(T)/", 't', $str);
	    $str = preg_replace("/(N)/", 'n', $str);
	    $str = preg_replace("/(C)/", 'c', $str);
	    $str = preg_replace("/(B)/", 'b', $str);
	    $str = preg_replace("/(M)/", 'm', $str);
	    
	    $str = preg_replace("/(O)/", 'o', $str);
	    $str = preg_replace("/(P)/", 'p', $str);
	    $str = preg_replace("/(S)/", 's', $str);
	    $str = preg_replace("/(G)/", 'g', $str);
	    $str = preg_replace("/(H)/", 'h', $str);
	    $str = preg_replace("/(V)/", 'v', $str);
	    $str = preg_replace("/(X)/", 'x', $str);
	    $str = preg_replace("/(R)/", 'r', $str);
	    $str = preg_replace("/(E)/", 'e', $str);
	    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'a', $str);
	    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'e', $str);
	    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'i', $str);
	    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'o', $str);
	    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'u', $str);
	    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'y', $str);
	    $str = preg_replace("/(Đ)/", 'd', $str);
	    //$str = str_replace(" ", "-", str_replace("&*#39;","",$str));
	 
	    return $str;
	 
	}
    public function increaseSorting($startPos = 1, $num = 1)
    {
        $sql = "UPDATE {$this->_name} SET sorting = sorting + " . intval($num) . " WHERE sorting >= " . intval($startPos);
        
        $this->_db->query($sql);
    }
    
    /**
     * Get banner by gid
     * 
     * @param int $gid
     * @return Zend_Db_Table_Row
     */
    public function getBannerByGid($gid)
    {
        $this->setAllLanguages(false);
        $select = $this->select()
                ->where('banner_gid=?', $gid);
                
        return $this->fetchRow($select);
    }
//    public function getBannerByUrl($url, $langId)
//    {
//        $url = $this->getAdapter()->quote($url);
//        $langId = intval($langId);
//        $time = time();
//        
//        $query = "
//                 SELECT *
//                 FROM ( SELECT * 
//                        FROM {$this->_prefix}banner_lang 
//                        WHERE lang_id = {$langId} AND enabled = 1 AND url = {$url} 
//                       ) AS sl
//                 JOIN ( SELECT banner_id, banner_category_gid, enabled AS senabled, publish_up_date, publish_down_date, sorting AS ssorting, created_date, layout,image
//                        FROM {$this->_prefix}banner 
//                        WHERE enabled = 1 AND publish_up_date <= {$time} AND (publish_down_date = 0 OR publish_down_date > {$time} )
//                       ) AS s
//                 ON s.banner_id = sl.banner_id
//                 LIMIT 0,1
//        ";
////        echo $query;die;
//        $result =$this->_db->fetchRow($query);
//         $array = explode(" ", $result['title']);
//         
//        foreach ($array as $item){
//           $result['titleeach'][] = substr($item,0,1); 
//           $result['titleeach'][] = substr($item,1,strlen($item)-1); 
//        }
//        return $result;
//    }
}