<?php
require_once 'modules/banner/models/Banner.php';
require_once 'modules/banner/models/BannerCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/user/models/User.php';
class banner_EstoreController extends Nine_Controller_Action
{
    public function manageBannerAction()
    {
    $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        
    	$this->setLayout('estore');
        $objBanner = new Models_Banner();
        $objLang    = new Models_Lang();
        $objCat     = new Models_BannerCategory();
        
        /**
         * Check permission
         */
        
        $this->view->headTitle(Nine_Language::translate('manage Banner '));
        
        
        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
    	if($currentPage == false){
        	$currentPage = 1;
        	$this->session->bannerDisplayNum = null;
        	$this->session->bannerCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objBanner->update(array('sorting' => $value), array('banner_gid=?' => $id));
            $this->session->bannerMessage = array(
                                       'success' => true,
                                       'message' => Nine_Language::translate("Banner Order is edited successfully")
                                   );
        }
        
        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->bannerDisplayNum;
        } else {
            $this->session->bannerDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->bannerCondition;
        } else {
            $this->session->bannerCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        if(@$condition['keyword'] != ''){
        	$condition['keyword'] = $objBanner->convert_vi_to_en($condition['keyword']);
        	$condition['keyword'] = preg_replace("/( )/", '-', $condition['keyword']);
        }
        
        /**
         * Get all categories
         */
        $this->view->allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_banner', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        /**
         * Get all banners
         */
        $condition['user_id'] = $user['user_id'];
        $numRowPerPage= $numRowPerPage;
        $allBanner = $objBanner->getAllBanner($condition, array('sorting ASC', 'banner_gid DESC', 'banner_id ASC'),
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage
                                                  );
         
//        echo '<pre>';print_r($allBanner);die;
        /**
         * Count all banners
         */
        $count = count($objBanner->getallBanner($condition));
        /**
         * Format
         */
        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allBanner[0]['banner_gid'];
        foreach ($allBanner as $index=>$banner) {
            /**
             * Change date
             */
            if (0 != $banner['created_date']) {
                $banner['created_date'] = date($config['dateFormat'], $banner['created_date']);
            } else {
                $banner['created_date'] = '';
            }
            if (0 != $banner['publish_up_date']) {
                $banner['publish_up_date'] = date($config['dateFormat'], $banner['publish_up_date']);
            } else {
                $banner['publish_up_date'] = '';
            }
            if (0 != $banner['publish_down_date']) {
                $banner['publish_down_date'] = date($config['dateFormat'], $banner['publish_down_date']);
            } else {
                $banner['publish_down_date'] = '';
            }
            if ($tmpGid != $banner['banner_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $banner['banner_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $banner;
                $tmp2['hit'] = 0;
            }
            $tmp2['hit']    += $banner['hit'];
            $tmp2['langs'][]  = $banner;
            /**
             * Final element
             */
            if ($index == count($allBanner) - 1) {
                $tmp[] = $tmp2;
            }
        }
        $allBanner = $tmp;
        
    	$export = $this->_getParam('export', false);
		if($export != false){
			  	$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
		
				$style = "style = 'border: 1px solid'";
		
				if(Nine_Language::getCurrentLangId() == 1){
					$textHeader = array(
						"No",
						"Category Name",
						"Estore",
						"Created",
						"Sort",
						"Title"
					);
				}else{
					$textHeader = array(
						mb_convert_encoding("Số Thứ Tự",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Tên Vị Trí",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Tên Gian Hàng",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Ngày Khởi Tạo",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Thứ Tự Ưu Tiên",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Tiêu Đề",'HTML-ENTITIES','UTF-8')
					);
				}
				
				$header = "<tr>";
				foreach ($textHeader as $text) {
					$header .= "<td $style>" . utf8_encode($text) . "</td>";
				}
				$header .= "</tr>";
		
				$content = '';
				$no = 1;
				
				foreach ($allBanner as $item) {
					$content .="<tr>";
					$content .= "<td $style>" . $no . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['cname'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['uname'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['sorting'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" .  mb_convert_encoding($item['title'],'HTML-ENTITIES','UTF-8') . "</td>";
					$content .="</tr>";
					$no++;
				}
		
				header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("content-disposition: attachment;filename=thong-ke-banner-" . date('d-m-Y H:i:s') . ".xls");
		
				$xlsTbl = $header;
				$xlsTbl .= $content;
		
				echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
				exit();
				
		}
        /**
         * Set values for tempalte
         */
//        echo print_r($condition); die;
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allBanner = $allBanner;
        $this->view->bannerMessage = $this->session->bannerMessage;
        $this->session->bannerMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_banner');
        $this->view->EditPermisision = $this->checkPermission('edit_banner');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_banner');
        $this->view->DeletePermisision = $this->checkPermission('delete_banner');
        $this->view->allLangs = $allLangs;
        
        $this->view->menu = array(
        	0=>'banner',
        	1=>'manager-banner'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-banner-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/banner/admin/manage-banner',
        		'name'	=>	Nine_Language::translate('Manager Banner')
        		)
        	
        );
        $this->view->alias = $user['alias'] . '.html';
        $this->view->username = $user['alias'];
    }
	public function cleanData(&$str)
	{
	    if($str == 't') $str = 'TRUE';
	    if($str == 'f') $str = 'FALSE';
	    if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
	      $str = "'$str";
	    }
	    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
	    $str = mb_convert_encoding($str, 'UTF-16LE', 'UTF-8');
	}
    public function newBannerAction()
    {
    $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
    	$this->setLayout('estore');
        $objCat = new Models_BannerCategory();
        $objLang = new Models_Lang();
        $objBanner = new Models_Banner();
        
        /**
         * Check permission
         */
        $id = $this->_getParam('id', false);
        $data = $this->_getParam('data', false);
        /**
         * Get all categories
         */
        $allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new banner
             */
            $newBanner = $data;
            $newBanner['created_date'] = time();
            /**
             * Change format date
             */
            if (null == $newBanner['publish_up_date']) {
                unset($newBanner['publish_up_date']);
            } else {
                $tmp = explode('/', $newBanner['publish_up_date']);
                $newBanner['publish_up_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (null == $newBanner['publish_down_date']) {
                unset($newBanner['publish_down_date']);
            } else {
                $tmp = explode('/', $newBanner['publish_down_date']);
                $newBanner['publish_down_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            /**
             * Sorting
             */
            if (null == $newBanner['sorting']) {
                unset($newBanner['sorting']);
            }
            if (false == $this->checkPermission('new_banner', null, '*')) {
                $newBanner['genabled']          = 0;
                $newBanner['publish_up_date']   = null;
                $newBanner['publish_down_date'] = null;
                $newBanner['sorting']           = 1;
            }
        
            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newBanner[$lang['lang_id']]['intro_text'], $newBanner[$lang['lang_id']]['full_text'])= Nine_Function::splitTextWithReadmore($newBanner[$lang['lang_id']]['full_text']);
            	if($newBanner[$lang['lang_id']]['alias'] == ""){
            		$newBanner[$lang['lang_id']]['alias'] = $objBanner->convert_vi_to_en($newBanner[$lang['lang_id']]['title']);
            		$newBanner[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newBanner[$lang['lang_id']]['alias']));
            	}
            	 if (is_array($newBanner[$lang['lang_id']]['images'])) {
	             	foreach ($newBanner[$lang['lang_id']]['images'] as $index => $image) {
	                    if (null == $image) {
	                        unset($newBanner[$lang['lang_id']]['images'][$index]);
	                    } else {
	                        $newBanner[$lang['lang_id']]['images'][$index] = Nine_Function::getImagePath($image);
	                    }
	                }
	            }
	            $newBanner[$lang['lang_id']]['images'] = implode('||', $newBanner[$lang['lang_id']]['images']);
            }
            
            /**
             * Remove empty images
             */
           
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > $newBanner['sorting']) {
                    $newBanner['sorting'] = 1;
                }
                $objBanner->increaseSorting($newBanner['sorting'], 1);
                
                
                
                if($newBanner['publish_up_date'] == ''){
                	$newBanner['publish_up_date'] = date();
                }
                $newBanner['user_id'] = $user['user_id'];
                $objBanner->insert($newBanner);
                /**
                 * Message
                 */
                $this->session->bannerMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Banner is created successfully.')
                        );
//                echo "<pre>HERE";die;
                $this->_redirect('estore/manage-banner/' . $user['alias'] . '.html');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Banner'));
        $this->view->fullPermisison = $this->checkPermission('new_banner', null, '*'); 
        $this->view->CheckGenalbel = $this->checkPermission('genabled_banner');
         $this->view->menu = array(
        	0=>'banner',
        	1=>'new-banner'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-banner-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/banner/admin/manage-banner',
        		'name'	=>	Nine_Language::translate('Manager Banner')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-plus',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('New Banner')
        		)
        	
        );
        $this->view->alias = $user['alias'] . '.html';
        $this->view->username = $user['alias'];
    }
    public function editBannerAction()
    {
    	$this->setLayout('estore');
    	$url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        
        $objBanner = new Models_Banner();
        $objCat     = new Models_BannerCategory();
        $objLang    = new Models_Lang();
        /**
         * Check permission
         */
        
        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false); 
        if (false == $gid) {
            $this->_redirect('estore/manage-banner/' . $user['alias'] . '.html');
        }
        
        $data   = $this->_getParam('data', false);
        
        /**
         * Get all categories
         */
        $allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all banner languages
         */
        
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allBannerLangs = $objBanner->setAllLanguages(true)->getByColumns(array('banner_gid=?' => $gid))->toArray();
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new banner
             */
            $newBanner = $data;
            /**
             * Change format date
             */
            if (null != $newBanner['publish_up_date']) {
                $tmp = explode('/', $newBanner['publish_up_date']);
                $newBanner['publish_up_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (null != $newBanner['publish_down_date']) {
                $tmp = explode('/', $newBanner['publish_down_date']);
                $newBanner['publish_down_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            /**
             * Sorting
             */
         	if (null == $newBanner['sorting']){
                unset($newBanner['sorting']);
            }
            if (false == $this->checkPermission('new_banner', null, '*')) {
                unset($newBanner['genabled']);
                unset($newBanner['publish_up_date']);
                unset($newBanner['publish_down_date']);
                unset($newBanner['sorting']);
            }
        
            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newBanner[$lang['lang_id']]['intro_text'], $newBanner[$lang['lang_id']]['full_text'])= Nine_Function::splitTextWithReadmore($newBanner[$lang['lang_id']]['full_text']);
            	if($newBanner[$lang['lang_id']]['alias'] == ""){
            		$newBanner[$lang['lang_id']]['alias'] = $objBanner->convert_vi_to_en($newBanner[$lang['lang_id']]['title']);
            		$newBanner[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newBanner[$lang['lang_id']]['alias']));
            	}
            	if (is_array($newBanner[$lang['lang_id']]['images'])) {
	             	foreach ($newBanner[$lang['lang_id']]['images'] as $index => $image) {
	                    if (null == $image) {
	                        unset($newBanner[$lang['lang_id']]['images'][$index]);
	                    } else {
	                        $newBanner[$lang['lang_id']]['images'][$index] = Nine_Function::getImagePath($image);
	                    }
	                }
	            }
	            $newBanner[$lang['lang_id']]['images'] = implode('||', $newBanner[$lang['lang_id']]['images']);
            }
            /**
             * Remove empty images
             */
            try {
                /**                
                 * Update
                 */
            	$newBanner['user_id'] = $user['user_id'];
                $objBanner->update($newBanner, array('banner_gid=?' => $gid));
                /**
                 * Message
                 */
                $this->session->bannerMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Banner is updated successfully.')
                        );
                
                $this->_redirect('estore/manage-banner/' . $user['alias'] . '.html');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allBannerLangs);
            
            if (false == $data) {
                $this->session->bannerMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate("Banner doesn't exist.")
                                           );
                $this->_redirect('estore/manage-banner/' . $user['alias'] . '.html');
            }
            /**
             * Change date
             */
            if (0 != $data['publish_up_date']) {
                 $data['publish_up_date'] = date('d/m/Y', $data['publish_up_date']);
            } else {
                $data['publish_up_date'] = '';
            }
            if (0 != $data['publish_down_date']) {
                 $data['publish_down_date'] = date('d/m/Y', $data['publish_down_date']);
            } 
        	else {
                $data['publish_down_date'] = '';
            }
            /**
             * Format image
             */
           
            /**
             * Get all lang banners
             */
            foreach ($allBannerLangs as $banner) {
                /**
                 * Rebuild readmore DIV
                 */
                $banner['full_text'] = Nine_Function::combineTextWithReadmore($banner['intro_text'], $banner['full_text']);
                $data[$banner['lang_id']] = $banner;
                $banner['images'] = explode('||', $banner['images']);
	            if (! is_array($banner['images'])) {
	                $banner['images'] = array();
	            }
	            $banner['images'] = array_pad($banner['images'], 50, null);
            }
            
            /**
             * Add deleteable field
             */
            if (null != @$data['banner_category_gid']) {
            	$cat = @reset($objCat->getByColumns(array('banner_category_gid' => $data['banner_category_gid']))->toArray());
            	$data['banner_deleteable'] = @$cat['banner_deleteable'];
            }
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Banner'));
        $this->view->fullPermisison = $this->checkPermission('edit_banner', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_banner');
        $this->view->menu = array(
        	0=>'banner',
        	1=>'manager-banner'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-banner-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/banner/admin/manage-banner',
        		'name'	=>	Nine_Language::translate('Manager Banner')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-pencil',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('Edit Banner')
        		)
        	
        );
        $this->view->alias = $user['alias'] . '.html';
        $this->view->username = $user['alias'];
    }

    public function deleteBannerAction()
    {
    $this->setLayout('estore');
    	$url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        $objBanner = new Models_Banner();
        $objCat = new Models_BannerCategory();
        /**
         * Check permission
         */
        
        $gid = $this->_getParam('gid', false);
        
        if (false == $gid) {
            $this->_redirect('estore/manage-banner/' . $user['alias'] . '.html');
        }
        
        $gids = explode('_', trim($gid, '_'));
        
        try {
            foreach ($gids as $gid) {
            	
            	$banner = @reset($objBanner->getByColumns(array('banner_gid=?'=>$gid))->toArray());
            	$cat = @reset($objCat->getByColumns(array('banner_category_gid=?'=>$banner['banner_category_gid']))->toArray());
            	if ( 0 == $cat['banner_deleteable']){
            		$this->session->bannerMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete banner ('. $gid .'). Please try again')
                                           );
                    $this->_redirect('estore/manage-banner/' . $user['alias'] . '.html');
            	}
            	else {
            		$objBanner->delete(array('banner_gid=?' => $gid));
            	}
              
            }
            $this->session->bannerMessage = array(
                                               'success' => true,
                                               'message' => Nine_Language::translate('Banner is deleted successfully')
                                           );
        } catch (Exception $e) {
            $this->session->bannerMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete this banner. Please try again')
                                           );
        }
        $this->_redirect('estore/manage-banner/' . $user['alias'] . '.html');
    }
   
    public function changeStringAction()
    {
    	$objBanner = new Models_Banner();
    	$str = $this->_getParam("string","");
    	$str = $objBanner->convert_vi_to_en($str);
    	$str = str_replace(" ", "-", trim($str));
    	echo $str;die;
    }
}