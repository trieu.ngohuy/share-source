<?php
require_once 'modules/banner/models/Banner.php';
require_once 'modules/banner/models/BannerCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/user/models/User.php';
class banner_AdminController extends Nine_Controller_Action_Admin 
{
    public function manageBannerAction()
    {
        $objBanner = new Models_Banner();
        $objLang    = new Models_Lang();
        $objCat     = new Models_BannerCategory();
        
        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_banner', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $this->view->headTitle(Nine_Language::translate('manage Banner '));
        
        
        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
    	if($currentPage == false){
        	$currentPage = 1;
        	$this->session->bannerDisplayNum = null;
        	$this->session->bannerCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objBanner->update(array('sorting' => $value), array('banner_gid=?' => $id));
            $this->session->bannerMessage = array(
                                       'success' => true,
                                       'message' => Nine_Language::translate("Banner Order is edited successfully")
                                   );
        }
        
        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->bannerDisplayNum;
        } else {
            $this->session->bannerDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->bannerCondition;
        } else {
            $this->session->bannerCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        if(@$condition['keyword'] != ''){
        	$condition['keyword'] = $objBanner->convert_vi_to_en($condition['keyword']);
        	$condition['keyword'] = preg_replace("/( )/", '-', $condition['keyword']);
        }
        
        /**
         * Get all categories
         */
        $this->view->allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_banner', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        /**
         * Get all banners
         */
        $numRowPerPage= $numRowPerPage;
        $allBanner = $objBanner->getAllBanner($condition, array('sorting ASC', 'banner_gid DESC', 'banner_id ASC'),
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage
                                                  );
         
//        echo '<pre>';print_r($allBanner);die;
        /**
         * Count all banners
         */
        $count = count($objBanner->getallBanner($condition));
        /**
         * Format
         */
        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allBanner[0]['banner_gid'];
        foreach ($allBanner as $index=>$banner) {
            /**
             * Change date
             */
            if (0 != $banner['created_date']) {
                $banner['created_date'] = date($config['dateFormat'], $banner['created_date']);
            } else {
                $banner['created_date'] = '';
            }
            if (0 != $banner['publish_up_date']) {
                $banner['publish_up_date'] = date($config['dateFormat'], $banner['publish_up_date']);
            } else {
                $banner['publish_up_date'] = '';
            }
            if (0 != $banner['publish_down_date']) {
                $banner['publish_down_date'] = date($config['dateFormat'], $banner['publish_down_date']);
            } else {
                $banner['publish_down_date'] = '';
            }
            if ($tmpGid != $banner['banner_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $banner['banner_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $banner;
                $tmp2['hit'] = 0;
            }
            $tmp2['hit']    += $banner['hit'];
            $tmp2['langs'][]  = $banner;
            /**
             * Final element
             */
            if ($index == count($allBanner) - 1) {
                $tmp[] = $tmp2;
            }
        }
        $allBanner = $tmp;
        
    	$export = $this->_getParam('export', false);
		if($export != false){
			  	$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
		
				$style = "style = 'border: 1px solid'";
		
				if(Nine_Language::getCurrentLangId() == 1){
					$textHeader = array(
						"No",
						"Category Name",
						"Estore",
						"Created",
						"Sort",
						"Title"
					);
				}else{
					$textHeader = array(
						mb_convert_encoding("Số Thứ Tự",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Tên Vị Trí",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Tên Gian Hàng",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Ngày Khởi Tạo",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Thứ Tự Ưu Tiên",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Tiêu Đề",'HTML-ENTITIES','UTF-8')
					);
				}
				
				$header = "<tr>";
				foreach ($textHeader as $text) {
					$header .= "<td $style>" . utf8_encode($text) . "</td>";
				}
				$header .= "</tr>";
		
				$content = '';
				$no = 1;
				
				foreach ($allBanner as $item) {
					$content .="<tr>";
					$content .= "<td $style>" . $no . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['cname'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['uname'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['sorting'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" .  mb_convert_encoding($item['title'],'HTML-ENTITIES','UTF-8') . "</td>";
					$content .="</tr>";
					$no++;
				}
		
				header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("content-disposition: attachment;filename=thong-ke-banner-" . date('d-m-Y H:i:s') . ".xls");
		
				$xlsTbl = $header;
				$xlsTbl .= $content;
		
				echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
				exit();
				
		}
        /**
         * Set values for tempalte
         */
//        echo print_r($condition); die;
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allBanner = $allBanner;
        $this->view->bannerMessage = $this->session->bannerMessage;
        $this->session->bannerMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_banner');
        $this->view->EditPermisision = $this->checkPermission('edit_banner');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_banner');
        $this->view->DeletePermisision = $this->checkPermission('delete_banner');
        $this->view->allLangs = $allLangs;
        
        $this->view->menu = array(
        	0=>'banner',
        	1=>'manager-banner'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-banner-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/banner/admin/manage-banner',
        		'name'	=>	Nine_Language::translate('Manager Banner')
        		)
        	
        );
    }
	public function cleanData(&$str)
	{
	    if($str == 't') $str = 'TRUE';
	    if($str == 'f') $str = 'FALSE';
	    if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
	      $str = "'$str";
	    }
	    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
	    $str = mb_convert_encoding($str, 'UTF-16LE', 'UTF-8');
	}
    public function newBannerAction()
    {
        $objCat = new Models_BannerCategory();
        $objLang = new Models_Lang();
        $objBanner = new Models_Banner();
        
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_banner', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        $id = $this->_getParam('id', false);
        $data = $this->_getParam('data', false);
        /**
         * Get all categories
         */
        $allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_banner', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new banner
             */
            $newBanner = $data;
            $newBanner['created_date'] = time();
            /**
             * Change format date
             */
            if (null == $newBanner['publish_up_date']) {
                unset($newBanner['publish_up_date']);
            } else {
                $tmp = explode('/', $newBanner['publish_up_date']);
                $newBanner['publish_up_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (null == $newBanner['publish_down_date']) {
                unset($newBanner['publish_down_date']);
            } else {
                $tmp = explode('/', $newBanner['publish_down_date']);
                $newBanner['publish_down_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            /**
             * Sorting
             */
            if (null == $newBanner['sorting']) {
                unset($newBanner['sorting']);
            }
            if (false == $this->checkPermission('new_banner', null, '*')) {
                $newBanner['genabled']          = 0;
                $newBanner['publish_up_date']   = null;
                $newBanner['publish_down_date'] = null;
                $newBanner['sorting']           = 1;
            }
        
            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newBanner[$lang['lang_id']]['intro_text'], $newBanner[$lang['lang_id']]['full_text'])= Nine_Function::splitTextWithReadmore($newBanner[$lang['lang_id']]['full_text']);
            	if($newBanner[$lang['lang_id']]['alias'] == ""){
            		$newBanner[$lang['lang_id']]['alias'] = $objBanner->convert_vi_to_en($newBanner[$lang['lang_id']]['title']);
            		$newBanner[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newBanner[$lang['lang_id']]['alias']));
            	}
            	 if (is_array($newBanner[$lang['lang_id']]['images'])) {
	             	foreach ($newBanner[$lang['lang_id']]['images'] as $index => $image) {
	                    if (null == $image) {
	                        unset($newBanner[$lang['lang_id']]['images'][$index]);
	                    } else {
	                        $newBanner[$lang['lang_id']]['images'][$index] = Nine_Function::getImagePath($image);
	                    }
	                }
	            }
	            $newBanner[$lang['lang_id']]['images'] = implode('||', $newBanner[$lang['lang_id']]['images']);
            }
            
            /**
             * Remove empty images
             */
           
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > $newBanner['sorting']) {
                    $newBanner['sorting'] = 1;
                }
                $objBanner->increaseSorting($newBanner['sorting'], 1);
                
                
                
                if($newBanner['publish_up_date'] == ''){
                	$newBanner['publish_up_date'] = date();
                }
                
                $objBanner->insert($newBanner);
                /**
                 * Message
                 */
                $this->session->bannerMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Banner is created successfully.')
                        );
//                echo "<pre>HERE";die;
                $this->_redirect('banner/admin/manage-banner');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $objUser = new Models_User();
        $condition['group_id'] = 3;
        $allUsers = $objUser->getAllUsersWithGroup($condition, 'user_id DESC' );
        $this->view->allUsers = $allUsers;
        
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Banner'));
        $this->view->fullPermisison = $this->checkPermission('new_banner', null, '*'); 
        $this->view->CheckGenalbel = $this->checkPermission('genabled_banner');
         $this->view->menu = array(
        	0=>'banner',
        	1=>'new-banner'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-banner-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/banner/admin/manage-banner',
        		'name'	=>	Nine_Language::translate('Manager Banner')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-plus',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('New Banner')
        		)
        	
        );
    }
    public function editBannerAction()
    {
        $objBanner = new Models_Banner();
        $objCat     = new Models_BannerCategory();
        $objLang    = new Models_Lang();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_banner', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false); 
        if (false == $gid) {
            $this->_redirect('banner/admin/manage-banner');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_banner', null, '*'))
        ||  (false != $lid && false == $this->checkPermission('edit_banner', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $data   = $this->_getParam('data', false);
        
        /**
         * Get all categories
         */
        $allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all banner languages
         */
        
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allBannerLangs = $objBanner->setAllLanguages(true)->getByColumns(array('banner_gid=?' => $gid))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_banner', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allBannerLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new banner
             */
            $newBanner = $data;
            /**
             * Change format date
             */
            if (null != $newBanner['publish_up_date']) {
                $tmp = explode('/', $newBanner['publish_up_date']);
                $newBanner['publish_up_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (null != $newBanner['publish_down_date']) {
                $tmp = explode('/', $newBanner['publish_down_date']);
                $newBanner['publish_down_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            /**
             * Sorting
             */
         	if (null == $newBanner['sorting']){
                unset($newBanner['sorting']);
            }
            if (false == $this->checkPermission('new_banner', null, '*')) {
                unset($newBanner['genabled']);
                unset($newBanner['publish_up_date']);
                unset($newBanner['publish_down_date']);
                unset($newBanner['sorting']);
            }
        
            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newBanner[$lang['lang_id']]['intro_text'], $newBanner[$lang['lang_id']]['full_text'])= Nine_Function::splitTextWithReadmore($newBanner[$lang['lang_id']]['full_text']);
            	if($newBanner[$lang['lang_id']]['alias'] == ""){
            		$newBanner[$lang['lang_id']]['alias'] = $objBanner->convert_vi_to_en($newBanner[$lang['lang_id']]['title']);
            		$newBanner[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newBanner[$lang['lang_id']]['alias']));
            	}
            	if (is_array($newBanner[$lang['lang_id']]['images'])) {
	             	foreach ($newBanner[$lang['lang_id']]['images'] as $index => $image) {
	                    if (null == $image) {
	                        unset($newBanner[$lang['lang_id']]['images'][$index]);
	                    } else {
	                        $newBanner[$lang['lang_id']]['images'][$index] = Nine_Function::getImagePath($image);
	                    }
	                }
	            }
	            $newBanner[$lang['lang_id']]['images'] = implode('||', $newBanner[$lang['lang_id']]['images']);
            }
            /**
             * Remove empty images
             */
            try {
                /**                
                 * Update
                 */
                $objBanner->update($newBanner, array('banner_gid=?' => $gid));
                /**
                 * Message
                 */
                $this->session->bannerMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Banner is updated successfully.')
                        );
                
                $this->_redirect('banner/admin/manage-banner');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allBannerLangs);
            
            if (false == $data) {
                $this->session->bannerMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate("Banner doesn't exist.")
                                           );
                $this->_redirect('banner/admin/manage-banner');
            }
            /**
             * Change date
             */
            if (0 != $data['publish_up_date']) {
                 $data['publish_up_date'] = date('d/m/Y', $data['publish_up_date']);
            } else {
                $data['publish_up_date'] = '';
            }
            if (0 != $data['publish_down_date']) {
                 $data['publish_down_date'] = date('d/m/Y', $data['publish_down_date']);
            } 
        	else {
                $data['publish_down_date'] = '';
            }
            /**
             * Format image
             */
           
            /**
             * Get all lang banners
             */
            foreach ($allBannerLangs as $banner) {
                /**
                 * Rebuild readmore DIV
                 */
                $banner['full_text'] = Nine_Function::combineTextWithReadmore($banner['intro_text'], $banner['full_text']);
                $data[$banner['lang_id']] = $banner;
                $banner['images'] = explode('||', $banner['images']);
	            if (! is_array($banner['images'])) {
	                $banner['images'] = array();
	            }
	            $banner['images'] = array_pad($banner['images'], 50, null);
            }
            
            /**
             * Add deleteable field
             */
            if (null != @$data['banner_category_gid']) {
            	$cat = @reset($objCat->getByColumns(array('banner_category_gid' => $data['banner_category_gid']))->toArray());
            	$data['banner_deleteable'] = @$cat['banner_deleteable'];
            }
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Banner'));
        $this->view->fullPermisison = $this->checkPermission('edit_banner', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_banner');
        $objUser = new Models_User();
        $condition['group_id'] = 3;
        $allUsers = $objUser->getAllUsersWithGroup($condition, 'user_id DESC' );
        $this->view->allUsers = $allUsers;
        $this->view->menu = array(
        	0=>'banner',
        	1=>'manager-banner'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-banner-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/banner/admin/manage-banner',
        		'name'	=>	Nine_Language::translate('Manager Banner')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-pencil',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('Edit Banner')
        		)
        	
        );
    }

    public function enableBannerAction()
    {
        $objBanner = new Models_Banner();
        $objUser = new Models_User();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('banner/admin/manage-banner');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_banner', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBanner->update(array('genabled' => 1), array('banner_gid=?' => $gid));
                   $news = $objBanner->getBannerByGid ( $gid )->toArray ();
                   $user = $objUser->getByUserId($news['user_id'])->toArray();
                   $user['redic'] = $user['redic'] + 5;
                   $objUser->update($user, array('user_id =?' => $news['user_id']));
                }
                $this->session->bannerMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Banner is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->bannerMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this banner. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_banner', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBanner->update(array('enabled' => 1), array('banner_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->bannerMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Banner is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->bannerMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this banner. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('banner/admin/manage-banner');
    }
	public function tinnhanhBannerAction()
    {
        $objBanner = new Models_Banner();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('banner/admin/manage-banner');
        }
        
        $gids = explode('_', trim($gid, '_'));
       
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_banner', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBanner->update(array('tinnhanh' => 1), array('banner_gid=?' => $gid));
                }
                $this->session->bannerMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Banner is Flashes successfully')
                                               );
            } catch (Exception $e) {
                $this->session->bannerMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this banner. Please try again')
                                               );
            }
            
        $this->_redirect('banner/admin/manage-banner');
    }
	public function distinnhanhBannerAction()
    {
        $objBanner = new Models_Banner();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('banner/admin/manage-banner');
        }
        
        $gids = explode('_', trim($gid, '_'));
       
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_banner', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBanner->update(array('tinnhanh' => 0), array('banner_gid=?' => $gid));
                }
                $this->session->bannerMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Banner is Dis Flashes successfully')
                                               );
            } catch (Exception $e) {
                $this->session->bannerMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this banner. Please try again')
                                               );
            }
            
        $this->_redirect('banner/admin/manage-banner');
    }
    
    public function disableBannerAction()
    {
        $objBanner = new Models_Banner();
         $objUser = new Models_User();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('banner/admin/manage-banner');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_banner', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBanner->update(array('genabled' => 0), array('banner_gid=?' => $gid));
                   $news = $objBanner->getBannerByGid ( $gid )->toArray ();
                   $user = $objUser->getByUserId($news['user_id'])->toArray();
                   $user['redic'] = $user['redic'] - 5;
                   $objUser->update($user, array('user_id =?' => $news['user_id']));
                }
                $this->session->bannerMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Banner is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->bannerMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this banner. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_banner', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBanner->update(array('enabled' => 0), array('banner_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->bannerMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Banner is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->bannerMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this banner. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('banner/admin/manage-banner');
    }
    
    public function deleteBannerAction()
    {
        $objBanner = new Models_Banner();
        $objCat = new Models_BannerCategory();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_banner')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid = $this->_getParam('gid', false);
        
        if (false == $gid) {
            $this->_redirect('banner/admin/manage-banner');
        }
        
        $gids = explode('_', trim($gid, '_'));
        
        try {
            foreach ($gids as $gid) {
            	
            	$banner = @reset($objBanner->getByColumns(array('banner_gid=?'=>$gid))->toArray());
            	$cat = @reset($objCat->getByColumns(array('banner_category_gid=?'=>$banner['banner_category_gid']))->toArray());
            	if ( 0 == $cat['banner_deleteable']){
            		$this->session->bannerMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete banner ('. $gid .'). Please try again')
                                           );
                    $this->_redirect('banner/admin/manage-banner');
            	}
            	else {
            		$objBanner->delete(array('banner_gid=?' => $gid));
            	}
              
            }
            $this->session->bannerMessage = array(
                                               'success' => true,
                                               'message' => Nine_Language::translate('Banner is deleted successfully')
                                           );
        } catch (Exception $e) {
            $this->session->bannerMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete this banner. Please try again')
                                           );
        }
        $this->_redirect('banner/admin/manage-banner');
    }
    
    /**************************************************************************************************
     *                                         CATEGORY's FUNCTIONS
     **************************************************************************************************/
    public function manageCategoryAction()
    {
        $objLang    = new Models_Lang();
        $objCategory     = new Models_BannerCategory();
        
        
        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $this->view->headTitle(Nine_Language::translate('manage Category '));
        
        
        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
        
        if($currentPage == false){
        	$currentPage = 1;
        	$this->session->categoryDisplayNum = null;
        	$this->session->categoryCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objCategory->update(array('sorting' => $value), array('banner_category_gid=?' => $id));
            $this->session->categoryMessage = array(
                                       'success' => true,
                                       'message' => Nine_Language::translate("Edit sort numbers successfully")
                                   );
        }
        
        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->categoryDisplayNum;
        } else {
            $this->session->categoryDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->categoryCondition;
        } else {
            $this->session->categoryCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_category', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        /**
         * Get all categorys
         */
        $numRowPerPage = $numRowPerPage;
        $allCategories  = $objCategory->setAllLanguages(true)->getallCategories($condition, array('sorting ASC', 'banner_category_gid DESC', 'banner_category_id ASC'),
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage
                                                  );
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Count all categorys
         */
        $count = count($objCategory->setAllLanguages(true)->getallCategories($condition));
        /**
         * Format
         */
        
        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allCategories[0]['banner_category_gid'];
        foreach ($allCategories as $index=>$category) {
            /**
             * Change date
             */
        	
            if (0 != $category['created_date']) {
                $category['created_date'] = date($config['dateFormat'], $category['created_date']);
            } else {
                $category['created_date'] = '';
            }
            if ($tmpGid != $category['banner_category_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $category['banner_category_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $category;
            }
            $tmp2['langs'][]  = $category;
            /**
             * Final element
             */
            if ($index == count($allCategories) - 1) {
                $tmp[] = $tmp2;
            }
            
        }
        
        $allCategories = $tmp;
        
    	$export = $this->_getParam('export', false);
		if($export != false){
			  	$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
		
				$style = "style = 'border: 1px solid'";
		
				if(Nine_Language::getCurrentLangId() == 1){
					$textHeader = array(
						"No",
						"Parent Category",
						"Created",
						"Sort",
						"Title"
					);
				}else{
					$textHeader = array(
						mb_convert_encoding("Số Thứ Tự",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Nhóm Trên",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Ngày Khởi Tạo",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Thứ Tự Ưu Tiên",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("Tiêu Đề",'HTML-ENTITIES','UTF-8')
					);
				}
				
				$header = "<tr>";
				foreach ($textHeader as $text) {
					$header .= "<td $style>" . utf8_encode($text) . "</td>";
				}
				$header .= "</tr>";
		
				$content = '';
				$no = 1;
				
				foreach ($allCategories as $item) {
					$content .="<tr>";
					$content .= "<td $style>" . $no . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['parent'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['sorting'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" .  mb_convert_encoding($item['name'],'HTML-ENTITIES','UTF-8') . "</td>";
					$content .="</tr>";
					$no++;
				}
		
				header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("content-disposition: attachment;filename=thong-ke-nhom-banner-" . date('d-m-Y H:i:s') . ".xls");
		
				$xlsTbl = $header;
				$xlsTbl .= $content;
		
				echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
				exit();
				
		}
		
//        echo print_r($allCategories);die;
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCategories = $allCategories;
        $this->view->categoryMessage = $this->session->categoryMessage;
        $this->session->categoryMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_category');
        $this->view->allLangs = $allLangs;
        
        $this->view->menu = array(
        	0=>'banner',
        	1=>'manager-category-banner'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-banner-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/banner/admin/manage-category',
        		'name'	=>	Nine_Language::translate('Manager Category Banner')
        		)
        	
        );
    }
    
    public function newCategoryAction()
    {
        $objLang = new Models_Lang();
        $objCategory = new Models_BannerCategory;
        $objBanner = new Models_Banner();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $data = $this->_getParam('data', false);
        /**
         * Get all categories
         */
        $allCats = $objCategory->buildTree($objCategory->getAll(array('sorting ASC'))->toArray());
//        echo "<pre>";print_r($allCats);die; 
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_category', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCategory = $data;
            $newCategory['created_date'] = time();
            /**
             * Sorting
             */
            if (null == $newCategory['sorting']) {
                unset($newCategory['sorting']);
            }
            if (false == $this->checkPermission('new_category', null, '*')) {
                $newCategory['genabled']          = 0;
                $newCategory['sorting']           = 1;
            }
        	
            /**
             * Remove empty images
             */
            if (is_array($newCategory['images'])) {
                foreach ($newCategory['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newCategory['images'][$index]);
                    } else {
                        $newCategory['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newCategory['images'] = implode('||', $newCategory['images']);
        	foreach ($allLangs as $index => $lang) {
            	if($newCategory[$lang['lang_id']]['alias'] == ""){
            		$newCategory[$lang['lang_id']]['alias'] = $objBanner->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
            		$newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newCategory[$lang['lang_id']]['alias']));
            	}
            }
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > @$newCategory['sorting']) {
                    $newCategory['sorting'] = 1;
                }
                if (null == $newCategory['parent_id']) {
                	$newCategory['parent_id'] = NULL;
                }
                $objCategory->increaseSorting($newCategory['sorting'], 1);

                $gid = $objCategory->insert($newCategory);
                
                /**
                 * Update id string
                 */
                $objCategory->update(array('gid_string'	=>	$gid), array('banner_category_gid = ?' => $gid));
                $category = @reset($objCategory->getByColumns(array('banner_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);
                /**
                 * Message
                 */
                $this->session->categoryMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Category is created successfully.')
                        );
                
                $this->_redirect('banner/admin/manage-category');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Category'));
        $this->view->fullPermisison = $this->checkPermission('new_category', null, '*'); 
        
        $this->view->menu = array(
        	0=>'banner',
        	1=>'new-category-banner'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-banner-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/banner/admin/manage-category',
        		'name'	=>	Nine_Language::translate('Manager Category Banner')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-plus',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('New Category Banner')
        		)
        	
        );
    }
    

    
    public function editCategoryAction()
    {
        $objCategory     = new Models_BannerCategory();
        $objLang    = new Models_Lang();
        $objBanner = new Models_Banner();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false); 
        if (false == $gid) {
            $this->_redirect('banner/admin/manage-category');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_category', null, '*'))
        ||  (false != $lid && false == $this->checkPermission('edit_category', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $data   = $this->_getParam('data', false);
        
        /**
         * Get all categories
         */
        $allCats = $objCategory->buildTree($objCategory->getAll(array('sorting ASC'))->toArray());
        
        /**
         * Get all category languages
         */
        
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allCategoryLangs = $objCategory->setAllLanguages(true)->getByColumns(array('banner_category_gid=?' => $gid))->toArray();
//        echo "<pre>";print_r($allCategoryLangs);die; 
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_category', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allCategoryLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCategory = $data;
            /**
             * Sorting
             */
            if (null == $newCategory['sorting']) {
                unset($newCategory['sorting']);
            }
            if (false == $this->checkPermission('new_category', null, '*')) {
                unset($newCategory['genabled']);
                unset($newCategory['sorting']);
            }
        
            /**
             * Remove empty images
             */
            if (is_array($newCategory['images'])) {
                foreach ($newCategory['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newCategory['images'][$index]);
                    } else {
                        $newCategory['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newCategory['images'] = implode('||', $newCategory['images']);
        	foreach ($allLangs as $index => $lang) {
            	if($newCategory[$lang['lang_id']]['alias'] == ""){
            		$newCategory[$lang['lang_id']]['alias'] = $objBanner->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
            		$newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newCategory[$lang['lang_id']]['alias']));
            	}
            }
            try {
                /**                
                 * Update
                 */
              	if (null == $newCategory['parent_id']) {
                	$newCategory['parent_id'] = NULL;
                }
                /**
                 * Delete gid in parent
                 */
                $oldCategory = @reset($allCategoryLangs);
//                echo "<pre>";print_r($oldCategory);die; 
                $objCategory->deleteGidString($oldCategory['parent_id'], $oldCategory['gid_string']);
                
                /**
                 * Update new data
                 */
                $objCategory->update($newCategory, array('banner_category_gid=?' => $gid));
                
                /**
                 * Update new id string
                 */
                $category = @reset($objCategory->getByColumns(array('banner_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);
                
                /**
                 * Message
                 */
                $this->session->categoryMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Category is updated successfully.')
                        );
                
                $this->_redirect('banner/admin/manage-category');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allCategoryLangs);
            if (false == $data) {
                $this->session->categoryMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate("Category doesn't exist.")
                                           );
                $this->_redirect('banner/admin/manage-category');
            }
            /**
             * Format image
             */
            $data['images'] = explode('||', $data['images']);
            if (! is_array($data['images'])) {
                $data['images'] = array();
            }
            $data['images'] = array_pad($data['images'], 50, null);
            /**
             * Get all lang categorys
             */
            foreach ($allCategoryLangs as $category) {
                $data[$category['lang_id']] = $category;
            }
            
             /**
             * Get all child category
             */
            $allChildCats = explode(',',trim($data['gid_string'],','));
            unset($allChildCats[0]);
           	foreach($allCats as $key =>	$item) {
           		if (false != in_array($item, $allChildCats)) {
           			unset($allCats[$key]);
           		}
           	}
        }
        /**
         * Remove it and its childs from category list
         */
        $oldData = @reset($allCategoryLangs);
        foreach ($allCats as $index => $item) {
            if (false !== strpos(",{$oldData['gid_string']},", ",{$item['banner_category_gid']},")) {
                unset($allCats[$index]);
            }
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Category'));
        $this->view->fullPermisison = $this->checkPermission('edit_category', null, '*');
         $this->view->menu = array(
        	0=>'banner',
        	1=>'manager-category-banner'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-banner-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/banner/admin/manage-category',
        		'name'	=>	Nine_Language::translate('Manager Category Banner')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-pencil',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('Edit Category Banner')
        		)
        	
        );
    }

    public function enableCategoryAction()
    {
        $objCategory = new Models_BannerCategory;
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('banner/admin/manage-category');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_category', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('genabled' => 1), array('banner_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_category', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('enabled' => 1), array('banner_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('banner/admin/manage-category');
    }

    
    public function disableCategoryAction()
    {
        $objCategory = new Models_BannerCategory;
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('banner/admin/manage-category');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_category', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('genabled' => 0), array('banner_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_category', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('enabled' => 0), array('banner_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('banner/admin/manage-category');
    }
    
    public function deleteCategoryAction()
    {
        $objCategory = new Models_BannerCategory;
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_category')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid = $this->_getParam('gid', false);
        
        if (false == $gid) {
            $this->_redirect('banner/admin/manage-category');
        }
        
        $gids = explode('_', trim($gid, '_'));
        
        try {
            foreach ($gids as $gid) {
            	
            	$cat = @reset($objCategory->getByColumns(array('banner_category_gid=?'=>$gid))->toArray());
            	if ( 0 == $cat['banner_deleteable']){
            		$this->session->categoryMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete category ('. $gid .'). Please try again')
                                           );
                    $this->_redirect('banner/admin/manage-category');
            	}
            	else {
            		$objCategory->delete(array('banner_category_gid=?' => $gid));
            	}
            }
            $this->session->categoryMessage = array(
                                               'success' => true,
                                               'message' => Nine_Language::translate('Category is deleted successfully')
                                           );
        } catch (Exception $e) {
            $this->session->categoryMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete this category. Please try again')
                                           );
        }
        $this->_redirect('banner/admin/manage-category');
    }
    public function changeStringAction()
    {
    	$objBanner = new Models_Banner();
    	$str = $this->_getParam("string","");
    	$str = $objBanner->convert_vi_to_en($str);
    	$str = str_replace(" ", "-", trim($str));
    	echo $str;die;
    }
}