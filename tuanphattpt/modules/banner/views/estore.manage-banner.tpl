


<div class="page-header">
	<h1>
		{{l}}Banner{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}List All Banner{{/l}}
		</small>
	</h1>
</div>
<a class="btn btn-lg btn-success" href="?export=true&page=1">
	<i class="ace-icon fa fa-file-excel-o"></i>
	Export 
</a>
<br class="cb">
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">
			
								{{if $allBanner|@count <= 0}}
		                        <div class="alert alert-info">
		                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
										{{l}}No banner with above conditions.{{/l}}
								</div>
		                        {{/if}}
		                        
		                        {{if $bannerMessage|@count > 0 && $bannerMessage.success == true}}
		                        <div class="alert alert-success">
		                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
										{{$categoryMessage.message}}.
								</div>
		                        {{/if}}
		                        
		                        {{if $bannerMessage|@count > 0 && $bannerMessage.success == false}}
		                        <div class="alert alert-danger">
		                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
										{{$bannerMessage.message}}.
								</div>
		                        {{/if}}
		                        
                
				<table id="simple-table" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<form id="top-search" name="search" method="post" action="">
							
				                                   	
				                                   	
								<th class="center">
									<button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
								</th>
								<th class="center">
									<input class="check-all" type="checkbox" />
								</th>
                                <th>{{l}}Category{{/l}}</th>
                                   	<th>{{l}}Created{{/l}}</th>
<!--                                   	<th class="center">-->
<!--                                        {{l}}Flashes{{/l}}-->
<!--                                   	</th>-->
                                   	<th class="center">
                                        {{l}}Sort{{/l}}
                                        {{if $fullPermisison}}
                                        <a href="javascript:document.sortForm.submit();"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/save_16.png" alt="Save sorting"></a>
                                        {{/if}}
                                   	</th>
                                   	<th colspan="5">
                                   		<div class="col-sm-12">
                                   			{{l}}Title{{/l}}
                                   		</div>
                                   		<div class="col-sm-4">
                                   			<input  type="text" name="condition[keyword]" id="username" value="{{$condition.keyword}}"" placeholder="{{l}}Find Title{{/l}}" class="form-control" />
                                   		</div>
                                   		<div class="col-sm-4">
											<select class=" form-control" name="condition[banner_category_gid]" onchange="this.form.submit();">
												<option value="">{{l}}All categories{{/l}}</option>
					                            {{foreach from=$allCats item=item}}
					                                <option value="{{$item.banner_category_gid}}" {{if $condition.banner_category_gid== $item.banner_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
					                            {{/foreach}}
											</select>
										</div>
										<div class="col-sm-4">
											<select class=" form-control" name="condition[genabled]" onchange="this.form.submit();">
												<option value="">{{l}}All Published{{/l}}</option>
			                                <option value="1" {{if $condition.genabled == 1}}selected="selected"{{/if}}>{{l}}Published{{/l}}</option>
			                                <option value="0" {{if $condition.genabled != "" && $condition.genabled == 0}}selected="selected"{{/if}}>{{l}}No Published{{/l}}</option>
											</select>
										</div>
                                   	</th>
                        	</form>
						</tr>
					</thead>

					<tbody>
						{{if $allBanner|@count > 0}}
							<form action="" method="post" name="sortForm">
							{{foreach from=$allBanner item=item name=banner key=key}}
													<tr>
														<td class="center">{{$key+1}}</td>
														<td class="center">{{if 1 == $item.banner_deleteable}}<input type="checkbox" value="{{$item.banner_gid}}" name="allBanner" class="allBanner"/>{{else}}<img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/unselect.png" alt="Do not delete, enable, disable" />{{/if}}</td>
					                                    <td  width="10%">
					                                    	{{$item.cname}}
					                                    </td>
					                                    <td>{{$item.created_date}}</td>
<!--					                                    <td class="center">-->
<!--					                                        {{if $item.tinnhanh == '1' }}-->
<!--					                                        	<a href="{{$APP_BASE_URL}}banner/admin/distinnhanh-banner/gid/{{$item.banner_gid}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png"></a>-->
<!--					                                        {{else}}-->
<!--					                                        	<a href="{{$APP_BASE_URL}}banner/admin/tinnhanh-banner/gid/{{$item.banner_gid}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png"></a>-->
<!--					                                  		{{/if}}-->
<!--					                                    </td>-->
					                                    <td class="center">
					                                        {{if $fullPermisison}}
					                                        <input name="data[{{$item.banner_gid}}]" value="{{$item.sorting}}" size="3" style="text-align: center;"></input>
					                                        {{else}}
					                                        {{$item.sorting}}
					                                        {{/if}}
					                                    </td>
					                                    <td colspan="5" style="padding:0px;">
					                                    <!-- All languages -->
					                                        <table style="border-collapse: separate;margin-bottom: 0px" class="table table-bordered table-striped">
					                                            <tbody id="table{{$item.banner_id}}">
					                                                <tr>
					                                                    <td width="39%">--</td>
					                                                    <td width="5%">{{$item.hit}}</td>
					                                                    <td width="5%"  class="center">
					                                                        	<span class="tooltip-area">
																					<a href="{{$APP_BASE_URL}}estore/edit-banner/{{$username}}/{{$item.banner_gid}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
																				</span>
					                                                    </td>
					                                                    <td width=5%">
					                                                        {{$item.banner_gid}}
					                                                    </td>
					                                                </tr>
					                                                
					                                                {{foreach from=$item.langs item=item2}}
					                                                <tr>
					                                                    <td><image style="vertical-align:middle;" src="{{$BASE_URL}}{{$item2.lang_image}}"> {{$item2.title}}</td>
					                                                    <td>{{$item2.hit}}</td>
					                                                    <td  class="center">
					                                                        	<span class="tooltip-area">
																					<a href="{{$APP_BASE_URL}}estore/edit-banner/{{$username}}/{{$item.banner_gid}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
																				</span>
					                                                    </td>
					                                                    <td>
					                                                        {{$item2.banner_id}}
					                                                    </td>
					                                                </tr>
					                                                {{/foreach}}
					                                                
					                                            </tbody>
					                                        </table>
					                                    </td>
                                    
													</tr>
												{{/foreach}}
												</form>
						{{/if}}
					</tbody>
				</table>
				
				
			</div><!-- /.span -->
		</div><!-- /.row -->
		
		
		<div class="col-lg-12">
							<div class="form-group col-lg-4">
								<label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
								<div class="col-lg-6">
									<select id="action" class="form-control" >
		                               <option value=";">{{l}}Choose an action...{{/l}}</option>
			                            <option value="deleteBanner();">{{l}}Delete{{/l}}</option>
		                            </select>
		                        </div>
		                        <div class="col-lg-6">
	                            	<a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
	                            </div>
							</div>
							<div class="form-group col-lg-2">
								<label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
								<div class="col-lg-12">
									<form class="search" name="search" method="post" action="">
		                                <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
		                                    <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
		                                    <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
		                                    <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
		                                    <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
		                                    <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
		                                </select>
		                            </form>
		                        </div>
							</div>
							{{if $countAllPages > 1}}
							<div class="col-lg-6 pagination">
								{{if $first}}
	                            <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
	                            {{/if}}
	                            {{if $prevPage}}
	                            <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
	                            {{/if}}
	                            
	                            {{foreach from=$prevPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>
	                            
	                            {{foreach from=$nextPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            {{if $nextPage}}
	                            <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
	                            {{/if}}
	                            {{if $last}}
	                            <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
	                            {{/if}}
                            
							</div>
							{{/if}}
						</div>
					</div>
		
		
	</div>
</div>

<script language="javascript" type="text/javascript">
$(document).ready(function(){
    $('.close').click(function(){
		$(this).parent().hide( "slow");
    });
    $('.check-all').click(function(){
    	if(this.checked) { // check select status
            $('.allBanner').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.allBanner').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
});

function applySelected()
{
	var task = document.getElementById('action').value;
	eval(task);
}

function deleteBanner()
{
    var all = document.getElementsByName('allBanner');
    var tmp = '';
    var count = 0;
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
             count++;
        }
    }
    if ('' == tmp) {
        alert('Please choose an banner');
        return;
    } else {
    	result = confirm('Are you sure you want to delete ' + count + ' banner(s)?');
        if (false == result) {
            return;
        }
    }
    window.location.href = '{{$APP_BASE_URL}}estore/delete-banner/{{$username}}/' + tmp;
}


function deleteABanner(id)
{
    result = confirm('Are you sure you want to delete this banner?');
    if (false == result) {
        return;
    }
    window.location.href = '{{$APP_BASE_URL}}estore/delete-banner/{{$username}}/' + id;
}
</script>