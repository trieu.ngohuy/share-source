<?php
/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Vi
 * @copyright  Copyright (c) 2009 visualidea.org
 * @license    http://license.visualidea.org
 * @version    v 1.0 2009-04-15
 */
require_once 'Nine/Model.php';
require_once 'modules/mail/models/MailStore.php';
class Models_Mail extends Nine_Model
{ 
    /**
     * The primary key column or columns.
     * A compound key should be declared as an array.
     * You may declare a single-column primary key
     * as a string.
     *
     * @var mixed
     */
    protected $_primary = 'mail_id';
    
    /**
     * The field name what we use to group all language rows together
     * 
     * @var string
     */
    protected $_groupField = 'mail_gid';
    
    /**
     * Let system know this is miltilingual table or not.
     * If this table has multilingual fields, Zend_Db_Table_Select object
     * will be inserted language condition automatically.
     * 
     * @var array
     */
    protected $_multilingualFields = array('subject','content','enabled');
    
    /**
     * Constructor.
     *
     * Supported params for $config are:
     * - db              = user-supplied instance of database connector,
     *                     or key name of registry instance.
     * - name            = table name.
     * - primary         = string or array of primary key(s).
     * - rowClass        = row class name.
     * - rowsetClass     = rowset class name.
     * - referenceMap    = array structure to declare relationship
     *                     to parent tables.
     * - dependentTables = array of child tables.
     * - metadataCache   = cache for information from adapter describeTable().
     *
     * @param  mixed $config Array of user-specified config options, or just the Db Adapter.
     * @return void
     */
    public function __construct($config = array())
    {
        $this->_name = $this->_prefix . 'mail';
        return parent::__construct($config); 
    }

    /**
     * Get all mails with conditions
     * 
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
    public function getAllSystemMails($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('m' => $this->_name))
                ->join(array('l' => $this->_prefix . 'lang'), 'm.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
//        if (null != @$condition['keyword']) {
//            $select->join(array('l' => $this->_prefix . 'mail_lang'), 'm.mail_id = ml.mail_id', array('subject'))
//                    ->where($this->getAdapter()->quoteInto('ml.subject LIKE ?', "%{$condition['keyword']}%"))
//                    ->group('m.mail_id');
//        }
        
        return $this->fetchAll($select)->toArray();
    }
    

    /**
     * Send HTML mail from DB mail template
     * 
     * @param string $mailTemplateName
     * @param array $data
     * @param string|array $to 
     * @param string $subject If null, subject from DB will be chosen
     */
    public function sendHtmlMail($mailTemplateName, $data = array(), $to = array(), $subject = null)
    {
    	if (!is_array($to)) {
            $to = array($to);
        }
        
        /**
         * Get mail template
         */
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('m' => $this->_name))
                ->where('m.name=?', $mailTemplateName)
                ->where('m.lang_id=?', Nine_Language::getCurrentLangId());
        
        $mail = @$this->fetchRow($select)->toArray(); 
		
        if (null == $mail) {
            return true;
        }
        /**
         * Change key
         */
        $newKey = array();
        foreach ($data as $index => $item) {
            $newKey['['. strtoupper($index) . ']'] = $item;
        }
        $data = $newKey;
        
        /**
         * Insert data file for the first time call
         */
        if (null == $mail['data'] && ! empty($data)) {
            $this->update(array('data' => implode('<br/>', array_keys($data))), array('mail_gid=?' => $mail['mail_gid']));
        }
        /**
         * Replace subject
         */
        if (null == $subject) {
            /**
             * Replace subject with DATA
             */
            $mail['subject'] = str_replace(array_keys($data), $data, $mail['subject']);
        } else {
            $mail['subject'] = $subject;
        }
        /**
         * Replace content
         */
        $mail['content'] = str_replace(array_keys($data), $data, $mail['content']);
        
        $fromMail = Nine_Registry::getConfig('fromMail');
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        if (null != $fromMail) {
            $headers .= "From: {$fromMail}" . "\r\n" .
                "Reply-To: {$fromMail}" . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
        }
        
        /**
         * Send mail
         */
        $objMailStore = new Models_MailStore();
        foreach ($to as $email) {
        	$mail_store = array (
        			'type'    => 1,
        			'mail_gid'	=> $mail['mail_gid'],
        			'from'    => $fromMail,
        			'to'      => $email,
                    'subject' => $mail['subject'],
        			'content' => $mail['content'],
        			'date'    => time() //20110709
        					);
            @mail($email, $mail['subject'], $mail['content'], $headers);
            $objMailStore->insert($mail_store);
        }
    }
}