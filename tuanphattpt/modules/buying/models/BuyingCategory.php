<?php
/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2009 visualidea.org
 * @license    http://license.visualidea.org
 * @version    v 1.0 2009-04-15
 */
require_once 'Nine/Model.php';
class Models_BuyingCategory extends Nine_Model
{ 
    /**
     * The primary key column or columns.
     * A compound key should be declared as an array.
     * You may declare a single-column primary key
     * as a string.
     *
     * @var mixed
     */
    protected $_primary = 'buying_category_id';
     /**
     * The field name what we use to group all language rows together
     * 
     * @var string
     */
    protected $_groupField = 'buying_category_gid';
    /**
     * Let system know this is miltilingual table or not.
     * If this table has multilingual fields, Zend_Db_Table_Select object
     * will be inserted language condition automatically.
     * 
     * @var array
     */
    protected $_multilingualFields = array('name', 'enabled', 'alias', 'description');
    
    /**
     * Constructor.
     *
     * Supported params for $config are:
     * - db              = user-supplied instance of database connector,
     *                     or key name of registry instance.
     * - name            = table name.
     * - primary         = string or array of primary key(s).
     * - rowClass        = row class name.
     * - rowsetClass     = rowset class name.
     * - referenceMap    = array structure to declare relationship
     *                     to parent tables.
     * - dependentTables = array of child tables.
     * - metadataCache   = cache for information from adapter describeTable().
     *
     * @param  mixed $config Array of user-specified config options, or just the Db Adapter.
     * @return void
     */
    public function __construct($config = array())
    {
        $this->_name = $this->_prefix . 'buying_category';
        return parent::__construct($config); 
    }

    /**
     * Get all categories with conditions
     * 
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
    public function getAllCategories($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_name))
                ->join(array('p' => $this->_prefix . 'properties_category'), 'cc.current = p.properties_category_gid', array('pname' => 'name'))
                ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.buying_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
                ->joinLeft(array('pp' => $this->_prefix . 'user_profile'), 'cc.user_id = pp.user_id AND pp.lang_id = '.Nine_Language::getCurrentLangId(), array('padd' => 'add', 'pcompany_name' => 'company_name', 'pnote' => 'note', 'pabout' => 'about', 'pproduct_chinh' => 'product_chinh', 'pproduct_khac' => 'product_khac'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
        
        if (null != @$condition['keyword']) {
            $sql = "SELECT buying_category_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('name LIKE ?', "%{$condition['keyword']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['buying_category_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('cc.buying_category_gid IN (' . trim($idString, ',') .')');
        }
    	if (null != @$condition['full_name']) {
            $select->where($this->getAdapter()->quoteInto('pp.company_name LIKE ?', "%{$condition['full_name']}%"));
        }
        if (null != @$condition['status']) {
        	$select->where('cc.status = '.$condition['status']);
        }
    	if (null != @$condition['genabled']) {
        	$select->where('cc.genabled = '.$condition['genabled']);
        }
        return $this->fetchAll($select)->toArray();
    }
 	public function getAllCategoriesParentNull()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_name))
                ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.buying_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
                ->where('cc.buying_deleteable = ?', 1)
                ->order("sorting");
        /**
         * Conditions
         */
        
    	 $select->where('cc.parent_id IS NULL');
        
        return $this->fetchAll($select)->toArray();
    }
	public function getInforCategories($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_name))
                ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.buying_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
                ->where('cc.buying_category_gid = ?', $gid);
        
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllParentCategoryBuying($id)
    {
	     $ids = explode('_', $id);
	     if(@$_SESSION['string_cate'] != ""){
	         $string_cate = $_SESSION['string_cate'] ;
	     }
	     else{
	      $string_cate = "";
	     }
         $string_cates = explode('_', $string_cate);
         $count = 0 ;
	     foreach ($ids as $id){
	      	$select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
	        ->where('c.parent_id = ?', $id);
	        $allCates = $this->fetchAll($select)->toArray();
	        foreach ($allCates as $item){
	          if(!in_array($item['buying_category_gid'] , $string_cates)){
		          $count++;
		          $string_cates[] = $item['buying_category_gid'];
	          }
	        }
	     }
         if($count == 0){
	         $_SESSION['string_cate'] = implode("_", $string_cates);
	         return false;
         }else{
	         $_SESSION['string_cate'] = implode("_", $string_cates);
	         return $this->getAllParentCategoryBuying(implode("_", $string_cates));
         }
	}
	public function getAllCategoriesOnparentkey($key)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_name))
                ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.buying_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
                ->order("sorting");
        /**
         * Conditions
         */
        $select->where('cc.parent_id = ?',$key);
        
        return $this->fetchAll($select)->toArray();
    }

    public function getCategoryByAlias($alias) {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('cc' => $this->_name))
            ->join(array('p' => $this->_prefix . 'properties_category'), 'cc.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
            ->join(array('p2' => $this->_prefix . 'properties_category'), 'cc.current = p2.properties_category_gid', array('pname2' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.buying_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'));
        /**
         * Conditions
         */
        $select->where('cc.alias = ?',$alias);

        return current($this->fetchAll($select)->toArray());
    }

    public function getCategoryByGid($gid) {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('cc' => $this->_name))
            ->join(array('p' => $this->_prefix . 'properties_category'), 'cc.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
            ->join(array('p2' => $this->_prefix . 'properties_category'), 'cc.current = p2.properties_category_gid', array('pname2' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.buying_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'));
        /**
         * Conditions
         */
        $select->where('cc.buying_category_gid = ?',$gid);

        return $this->fetchRow($select);
    }

    public function getAllEnabledCategory($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('cc' => $this->_name))
            ->join(array('p' => $this->_prefix . 'properties_category'), 'cc.current = p.properties_category_gid', array('pname' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.buying_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('cc.genabled=?' , 1)
            ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
            ->order($order)
            ->limit($count, $offset);
        /**
         * Conditions
         */

        if (null != @$condition['keyword']) {
            $sql = "SELECT buying_category_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('name LIKE ?', "%{$condition['keyword']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['buying_category_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('cc.buying_category_gid IN (' . trim($idString, ',') .')');
        }
    	if (null != @$condition['user_id_reply']) {
            $sql = "SELECT buying_category_gid FROM 9_buying WHERE user_id = ".$condition['user_id_reply'];
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['buying_category_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('cc.buying_category_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['full_name']) {
            $sql = "SELECT buying_category_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('full_name LIKE ?', "%{$condition['full_name']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['buying_category_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('cc.buying_category_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['status']) {
            $select->where('cc.status = '.$condition['status']);
        }

        if (null != @$condition['user_id']) {
            $select->where('cc.user_id = '.$condition['user_id']);
        }
    	if (null != @$condition['view']) {
            $select->where('cc.view = '.$condition['view']);
        }
        return $this->fetchAll($select)->toArray();
    }
    
	public function getAllBuyingCatWithParent( $parentId, $depth = 1 )
    {
    	$select = $this->select()
                ->order(array('sorting ASC', 'name ASC'))
                ->where('lang_id=?', Nine_Language::getCurrentLangId())
                ->where("parent_id = ?", $parentId);
               
        
        $result = $this->fetchAll($select)->toArray();
        
    	/**
         * Finish?
         */
        $depth--;
        if (0 == $depth) {
        	return $result;
        } else {
        	/**
        	 * Call recursive
        	 */
        	foreach ($result as &$item) {
        		$item['child'] = $this->getAllBuyingCatWithParent($item['buying_category_gid'], $depth); 
        	}
        	unset($item);
        	
        	return $result;
        }
    }
    
	public function increaseSorting($startPos = 1, $num = 1)
    {
        $sql = "UPDATE {$this->_name} SET sorting = sorting + " . intval($num) . " WHERE sorting >= " . intval($startPos);
        
        $this->_db->query($sql);
    }
    
    
    public function updateGidString($gid, $gidString) 
    {
    	/**
    	 * Get current update node
    	 */
    	if (null == $gid) {
    		return;
    	}
    	else {
    		$category = @reset($this->getByColumns(array('buying_category_gid = ?'	=>	$gid))->toArray());
    		$this->update(array('gid_string'	=> $this->concatGidString($category['gid_string'],$gidString)),array('buying_category_gid = ?' => $gid));
    		return $this->updateGidString($category['parent_id'],$gidString);
    	}
    }
    
    public function deleteGidString($gid, $gidString)
    {
    	/**
    	 * Get current update node
    	 */
    	if (null == $gid){
    		return;
    	}
    	else {
    		$category = @reset($this->getByColumns(array('buying_category_gid = ?'	=>	$gid))->toArray());
    		$this->update(array('gid_string' => $this->removeGidString($category['gid_string'],$gidString)), array('buying_category_gid = ?' => $gid));
    		return $this->deleteGidString($category['parent_id'], $gidString);
    	}
    }
    
    
    public function concatGidString($oldGidStr, $concatGidStr) 
    {
    	if(null == $oldGidStr){
    		return $concatGidStr;
    	}
    	
    	$oldGidString = explode(',', trim($oldGidStr,','));
    	$concatGidString = explode(',', trim($concatGidStr,','));
    	
    	foreach($concatGidString as $item){
    		if (false == in_array($item, $oldGidString)){
    			$oldGidString[] = $item;
    		}
    	}
    	return implode(',', $oldGidString);
    }
    
    private function removeGidString($oldGidStr, $removedGidStr)
    {
    	if(null == $oldGidStr){
    		return null;
    	}
    	
    	$oldGidString = explode(',', trim($oldGidStr,','));
    	$removedGidString = explode(',', trim($removedGidStr,','));
    	
//    	echo "<pre>";print_r($removedGidString);die; 
    	foreach($oldGidString as $key => $item){
    		if (false != in_array($item, $removedGidString)){
    			unset($oldGidString[$key]);
    		}
    	}
//    	echo "<pre>";print_r($oldGidString);die; 
    	return implode(',', $oldGidString);
    }

    /**
     * Get all categories with conditions
     *
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
    public function getAllCategoriesEstore($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('cc' => $this->_name))
            ->join(array('b' => $this->_prefix . 'buying'), 'cc.buying_category_gid = b.buying_category_gid', array('user_id'))
            ->join(array('p' => $this->_prefix . 'properties_category'), 'cc.current = p.properties_category_gid', array('pname' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.buying_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('b.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('b.user_id=?', Nine_Registry::getLoggedInUserId())
            ->order($order)
            ->limit($count, $offset);
        /**
         * Conditions
         */

        if (null != @$condition['keyword']) {
            $sql = "SELECT buying_category_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('name LIKE ?', "%{$condition['keyword']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['buying_category_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('cc.buying_category_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['full_name']) {
            $sql = "SELECT buying_category_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('full_name LIKE ?', "%{$condition['full_name']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['buying_category_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('cc.buying_category_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['status']) {
            $select->where('cc.status = '.$condition['status']);
        }

        return $this->fetchAll($select)->toArray();
    }

    /**
     * Get all new categories with conditions
     *
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
    public function getNewCategoriesEstore($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('cc' => $this->_name))
            ->joinLeft(array('p2' => $this->_prefix . 'user_profile'), 'cc.user_id = p2.user_id AND p2.lang_id = '.Nine_Language::getCurrentLangId(), array('padd' => 'add', 'pcompany_name' => 'company_name', 'pnote' => 'note', 'pabout' => 'about', 'pproduct_chinh' => 'product_chinh', 'pproduct_khac' => 'product_khac'))
            ->joinLeft(array('p' => $this->_prefix . 'properties_category'), 'cc.current = p.properties_category_gid', array('pname' => 'name'))
            ->joinLeft(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.buying_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
            ->order($order)
            ->limit($count, $offset);
        /**
         * Conditions
         */

        if (null != @$condition['keyword']) {
            $sql = "SELECT buying_category_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('name LIKE ?', "%{$condition['keyword']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['buying_category_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('cc.buying_category_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['full_name']) {
            $sql = "SELECT buying_category_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('full_name LIKE ?', "%{$condition['full_name']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['buying_category_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('cc.buying_category_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['status']) {
            $select->where('cc.status = '.$condition['status']);
        }
        if (null != @$condition['product_category_gid']) {
            $select->where('cc.product_category_gid IN (' . $condition['product_category_gid'] . ')');
        }

        return $this->fetchAll($select)->toArray();
    }

    public function getStatus($status) {
        $st = '';
        switch ($status) {
            case 1: $st = 'Chờ báo giá';
                break;
            case 2: $st = 'Đang báo giá';
                break;
            case 3: $st = 'Hết hạn';
                break;
            case 4: $st = 'Đã chốt';
                break;
            default: $st = 'Chờ báo giá';
        }

        return $st;
    }
}