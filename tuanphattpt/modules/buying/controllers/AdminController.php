<?php
require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/buying/models/Buying.php';
require_once 'modules/buying/models/BuyingCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/user/models/User.php';
require_once 'modules/properties/models/PropertiesCategory.php';
require_once 'modules/country/models/CountryCategory.php';
require_once 'modules/hs/models/HsCategory.php';

include_once 'modules/mail/models/Mail.php';
include_once 'modules/mail/models/phpmailer.php';
include_once 'modules/mail/models/smtp.php';
class buying_AdminController extends Nine_Controller_Action_Admin 
{
	public function sendmail_smtp($data)
    {
        // Mail
        define('MAIL_PROTOCOL', 'smtp');
        define('MAIL_HOST', 'ssl://smtp.googlemail.com');
        define('MAIL_PORT', '465');
        define('MAIL_USER', 'luutn.it@gmail.com');
        define('MAIL_PASS', 'Linh2204');
        define('MAIL_SEC', 'ssl');
        define('MAIL_FROM', 'no.reply.C&D@gmail.com');


        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPAuth = TRUE;
        $mail->SMTPSecure = MAIL_SEC;
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASS;
        $mail->CharSet = "UTF-8";
        $mail->WordWrap = 50;

        $mail->IsHTML(TRUE);
        $mail->From = MAIL_USER;
        $mail->FromName = $data['name_from'];
        $to = $data['email_to'];
        $name = $data['name_to'];
        if (is_array($to)) {
            foreach ($to as $key => $sto) {
                $mail->AddAddress($sto, '');
            }
        } else {
            $mail->AddAddress($to, $name);
        }

        $mail->AddReplyTo(MAIL_USER, "C&D");
        $mail->Subject = $data['title_email'];
        $mail->Body = $data['content'];
        $mail->AltBody = $data['content'];
        if (!$mail->Send()) {
            return false;
        } else {
            return true;
        }
    }
    public function manageBuyingAction()
    {
        $objBuying = new Models_Buying();
        $objLang    = new Models_Lang();
        $objCat     = new Models_BuyingCategory();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_buying', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $this->view->headTitle(Nine_Language::translate('manage Buying '));
        
        
        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
    	if($currentPage == false){
        	$currentPage = 1;
        	$this->session->buyingDisplayNum = null;
        	$this->session->buyingCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objBuying->update(array('sorting' => $value), array('buying_gid=?' => $id));
            $this->session->buyingMessage = array(
                                       'success' => true,
                                       'message' => Nine_Language::translate("Buying Order is edited successfully")
                                   );
        }
        
        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->buyingDisplayNum;
        } else {
            $this->session->buyingDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->buyingCondition;
        } else {
            $this->session->buyingCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        if(@$condition['keyword'] != ''){
        	$condition['keyword'] = $objBuying->convert_vi_to_en($condition['keyword']);
        	$condition['keyword'] = preg_replace("/( )/", '-', $condition['keyword']);
        }
        
        /**
         * Get all categories
         */
        $this->view->allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_buying', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        /**
         * Get all buyings
         */
        $numRowPerPage= $numRowPerPage;
        $allBuying = $objBuying->setAllLanguages(true)->getAllBuying($condition, array('sorting ASC', 'buying_gid DESC', 'buying_id ASC'),
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage
                                                  );
//        echo '<pre>';print_r($allBuying);die;
        /**
         * Count all buyings
         */
        $count = count($objBuying->setAllLanguages(true)->getallBuying($condition));
        /**
         * Format
         */
        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allBuying[0]['buying_gid'];
        foreach ($allBuying as $index=>$buying) {
            /**
             * Change date
             */
            if (0 != $buying['created_date']) {
                $buying['created_date'] = date($config['dateFormat'], $buying['created_date']);
            } else {
                $buying['created_date'] = '';
            }
            if (0 != $buying['publish_up_date']) {
                $buying['publish_up_date'] = date($config['dateFormat'], $buying['publish_up_date']);
            } else {
                $buying['publish_up_date'] = '';
            }
            if (0 != $buying['publish_down_date']) {
                $buying['publish_down_date'] = date($config['dateFormat'], $buying['publish_down_date']);
            } else {
                $buying['publish_down_date'] = '';
            }
            if ($tmpGid != $buying['buying_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $buying['buying_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $buying;
                $tmp2['hit'] = 0;
            }
            $tmp2['hit']    += $buying['hit'];
            $tmp2['langs'][]  = $buying;
            /**
             * Final element
             */
            if ($index == count($allBuying) - 1) {
                $tmp[] = $tmp2;
            }
        }
        $allBuying = $tmp;
        /**
         * Set values for tempalte
         */
//        echo print_r($condition); die;
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allBuying = $allBuying;
        $this->view->buyingMessage = $this->session->buyingMessage;
        $this->session->buyingMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_buying');
        $this->view->EditPermisision = $this->checkPermission('edit_buying');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_buying');
        $this->view->DeletePermisision = $this->checkPermission('delete_buying');
        $this->view->allLangs = $allLangs;
        
        $this->view->menu = array(
        	0=>'buying',
        	1=>'manager-buying'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-buying-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/buying/admin/manage-buying',
        		'name'	=>	Nine_Language::translate('Manager Buying')
        		)
        	
        );
        
        $ids = $objBuying->getAllParent();
    	
		$this->view->ids = $ids;          
    }
    
    public function newBuyingAction()
    {
        $objCat = new Models_BuyingCategory();
        $objLang = new Models_Lang();
        $objBuying = new Models_Buying();
        $objUser = new Models_User();
        
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_buying', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        $id = $this->_getParam('id', false);
        $data = $this->_getParam('data', false);
        /**
         * Get all categories
         */
        $allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_buying', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new buying
             */
            $newBuying = $data;
            $newBuying['created_date'] = time();
            /**
             * Change format date
             */
            if (null == $newBuying['publish_up_date']) {
                unset($newBuying['publish_up_date']);
            } else {
                $tmp = explode('/', $newBuying['publish_up_date']);
                $newBuying['publish_up_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (null == $newBuying['publish_down_date']) {
                unset($newBuying['publish_down_date']);
            } else {
                $tmp = explode('/', $newBuying['publish_down_date']);
                $newBuying['publish_down_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            /**
             * Sorting
             */
            if (null == $newBuying['sorting']) {
                unset($newBuying['sorting']);
            }
            if (false == $this->checkPermission('new_buying', null, '*')) {
                $newBuying['genabled']          = 0;
                $newBuying['publish_up_date']   = null;
                $newBuying['publish_down_date'] = null;
                $newBuying['sorting']           = 1;
            }
        
            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newBuying[$lang['lang_id']]['intro_text'], $newBuying[$lang['lang_id']]['full_text'])= Nine_Function::splitTextWithReadmore($newBuying[$lang['lang_id']]['full_text']);
            	if($newBuying[$lang['lang_id']]['alias'] == ""){
            		$newBuying[$lang['lang_id']]['alias'] = $objBuying->convert_vi_to_en($newBuying[$lang['lang_id']]['title']);
            		$newBuying[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newBuying[$lang['lang_id']]['alias']));
            	}
            }
            
            /**
             * Remove empty images
             */
            if (is_array($newBuying['images'])) {
                foreach ($newBuying['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newBuying['images'][$index]);
                    } else {
                        $newBuying['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newBuying['icon'] = Nine_Function::getImagePath($newBuying['icon']);
            $newBuying['images'] = implode('||', $newBuying['images']);
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > $newBuying['sorting']) {
                    $newBuying['sorting'] = 1;
                }
                $objBuying->increaseSorting($newBuying['sorting'], 1);
                if($id != false){
                	$newBuying['user_id'] = $id;
                }else{
                	$newBuying['user_id'] = Nine_Registry::getLoggedInUserId();
                }
                
                
                if($newBuying['publish_up_date'] == ''){
                	$newBuying['publish_up_date'] = date();
                }
                
                $objBuying->insert($newBuying);
                
                
                /**
                 * Message
                 */
                $this->session->buyingMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Buying is created successfully.')
                        );
//                echo "<pre>HERE";die;
                $this->_redirect('buying/admin/manage-buying');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Buying'));
        $this->view->fullPermisison = $this->checkPermission('new_buying', null, '*'); 
        $this->view->CheckGenalbel = $this->checkPermission('genabled_buying');
         $this->view->menu = array(
        	0=>'buying',
        	1=>'new-buying'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-buying-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/buying/admin/manage-buying',
        		'name'	=>	Nine_Language::translate('Manager Buying')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-plus',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('New Buying')
        		)
        	
        );
    }
    public function editBuyingAction()
    {
        $objBuying = new Models_Buying();
        $objCat     = new Models_BuyingCategory();
        $objLang    = new Models_Lang();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_buying', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false); 
        if (false == $gid) {
            $this->_redirect('buying/admin/manage-buying');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_buying', null, '*'))
        ||  (false != $lid && false == $this->checkPermission('edit_buying', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $data   = $this->_getParam('data', false);
        
        /**
         * Get all categories
         */
        $allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all buying languages
         */
        
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allBuyingLangs = $objBuying->setAllLanguages(true)->getByColumns(array('buying_gid=?' => $gid))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_buying', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allBuyingLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new buying
             */
            $newBuying = $data;
            /**
             * Change format date
             */
            if (null != $newBuying['publish_up_date']) {
                $tmp = explode('/', $newBuying['publish_up_date']);
                $newBuying['publish_up_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (null != $newBuying['publish_down_date']) {
                $tmp = explode('/', $newBuying['publish_down_date']);
                $newBuying['publish_down_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            /**
             * Sorting
             */
         	if (null == $newBuying['sorting']){
                unset($newBuying['sorting']);
            }
            if (false == $this->checkPermission('new_buying', null, '*')) {
                unset($newBuying['genabled']);
                unset($newBuying['publish_up_date']);
                unset($newBuying['publish_down_date']);
                unset($newBuying['sorting']);
            }
        
            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newBuying[$lang['lang_id']]['intro_text'], $newBuying[$lang['lang_id']]['full_text'])= Nine_Function::splitTextWithReadmore($newBuying[$lang['lang_id']]['full_text']);
            	if($newBuying[$lang['lang_id']]['alias'] == ""){
            		$newBuying[$lang['lang_id']]['alias'] = $objBuying->convert_vi_to_en($newBuying[$lang['lang_id']]['title']);
            		$newBuying[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newBuying[$lang['lang_id']]['alias']));
            	}
            }
            /**
             * Remove empty images
             */
            if (is_array($newBuying['images'])) {
                foreach ($newBuying['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newBuying['images'][$index]);
                    } else {
                        $newBuying['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newBuying['images'] = implode('||', $newBuying['images']);
            $newBuying['icon'] = Nine_Function::getImagePath($newBuying['icon']);
            $user = Nine_Registry::getLoggedInUser()->toArray();
            $newBuying['user_id'] = $user['user_id'];
            try {
                /**                
                 * Update
                 */
                $objBuying->update($newBuying, array('buying_gid=?' => $gid));
                /**
                 * Message
                 */
                $this->session->buyingMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Buying is updated successfully.')
                        );
                
                $this->_redirect('buying/admin/manage-buying');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allBuyingLangs);
            
            if (false == $data) {
                $this->session->buyingMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate("Buying doesn't exist.")
                                           );
                $this->_redirect('buying/admin/manage-buying');
            }
            /**
             * Change date
             */
            if (0 != $data['publish_up_date']) {
                 $data['publish_up_date'] = date('d/m/Y', $data['publish_up_date']);
            } else {
                $data['publish_up_date'] = '';
            }
            if (0 != $data['publish_down_date']) {
                 $data['publish_down_date'] = date('d/m/Y', $data['publish_down_date']);
            } 
        	else {
                $data['publish_down_date'] = '';
            }
            /**
             * Format image
             */
            $data['images'] = explode('||', $data['images']);
            if (! is_array($data['images'])) {
                $data['images'] = array();
            }
            $data['images'] = array_pad($data['images'], 50, null);
            /**
             * Get all lang buyings
             */
            foreach ($allBuyingLangs as $buying) {
                /**
                 * Rebuild readmore DIV
                 */
                $buying['full_text'] = Nine_Function::combineTextWithReadmore($buying['intro_text'], $buying['full_text']);
                $data[$buying['lang_id']] = $buying;
            }
            
            /**
             * Add deleteable field
             */
            if (null != @$data['buying_category_gid']) {
            	$cat = @reset($objCat->getByColumns(array('buying_category_gid' => $data['buying_category_gid']))->toArray());
            	$data['buying_deleteable'] = @$cat['buying_deleteable'];
            }
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Buying'));
        $this->view->fullPermisison = $this->checkPermission('edit_buying', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_buying');
        $this->view->menu = array(
        	0=>'buying',
        	1=>'manager-buying'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-buying-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/buying/admin/manage-buying',
        		'name'	=>	Nine_Language::translate('Manager Buying')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-pencil',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('Edit Buying')
        		)
        	
        );
    }

    public function enableBuyingAction()
    {
        $objBuying = new Models_Buying();
        $objUser = new Models_User();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('buying/admin/manage-buying');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_buying', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBuying->update(array('genabled' => 1), array('buying_gid=?' => $gid));
                   $news = $objBuying->getBuyingByGid ( $gid )->toArray ();
                   $user = $objUser->getByUserId($news['user_id'])->toArray();
                   $user['redic'] = $user['redic'] + 5;
                   $objUser->update($user, array('user_id =?' => $news['user_id']));
                }
                $this->session->buyingMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Buying is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->buyingMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this buying. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_buying', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBuying->update(array('enabled' => 1), array('buying_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->buyingMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Buying is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->buyingMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this buying. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('buying/admin/manage-buying');
    }
	public function tinnhanhBuyingAction()
    {
        $objBuying = new Models_Buying();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('buying/admin/manage-buying');
        }
        
        $gids = explode('_', trim($gid, '_'));
       
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_buying', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBuying->update(array('tinnhanh' => 1), array('buying_gid=?' => $gid));
                }
                $this->session->buyingMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Buying is Flashes successfully')
                                               );
            } catch (Exception $e) {
                $this->session->buyingMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this buying. Please try again')
                                               );
            }
            
        $this->_redirect('buying/admin/manage-buying');
    }
	public function distinnhanhBuyingAction()
    {
        $objBuying = new Models_Buying();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('buying/admin/manage-buying');
        }
        
        $gids = explode('_', trim($gid, '_'));
       
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_buying', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBuying->update(array('tinnhanh' => 0), array('buying_gid=?' => $gid));
                }
                $this->session->buyingMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Buying is Dis Flashes successfully')
                                               );
            } catch (Exception $e) {
                $this->session->buyingMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this buying. Please try again')
                                               );
            }
            
        $this->_redirect('buying/admin/manage-buying');
    }
    
    public function disableBuyingAction()
    {
        $objBuying = new Models_Buying();
         $objUser = new Models_User();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('buying/admin/manage-buying');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_buying', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBuying->update(array('genabled' => 0), array('buying_gid=?' => $gid));
                   $news = $objBuying->getBuyingByGid ( $gid )->toArray ();
                   $user = $objUser->getByUserId($news['user_id'])->toArray();
                   $user['redic'] = $user['redic'] - 5;
                   $objUser->update($user, array('user_id =?' => $news['user_id']));
                }
                $this->session->buyingMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Buying is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->buyingMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this buying. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_buying', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBuying->update(array('enabled' => 0), array('buying_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->buyingMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Buying is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->buyingMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this buying. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('buying/admin/manage-buying');
    }
    
    public function deleteBuyingAction()
    {
        $objBuying = new Models_Buying();
        $objCat = new Models_BuyingCategory();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_buying')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid = $this->_getParam('gid', false);
        
        if (false == $gid) {
            $this->_redirect('buying/admin/manage-buying');
        }
        
        $gids = explode('_', trim($gid, '_'));
        
        try {
            foreach ($gids as $gid) {
            	
            	$buying = @reset($objBuying->getByColumns(array('buying_gid=?'=>$gid))->toArray());
            	$cat = @reset($objCat->getByColumns(array('buying_category_gid=?'=>$buying['buying_category_gid']))->toArray());
            	if ( 0 == $cat['buying_deleteable']){
            		$this->session->buyingMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete buying ('. $gid .'). Please try again')
                                           );
                    $this->_redirect('buying/admin/manage-buying');
            	}
            	else {
            		$objBuying->delete(array('buying_gid=?' => $gid));
            	}
              
            }
            $this->session->buyingMessage = array(
                                               'success' => true,
                                               'message' => Nine_Language::translate('Buying is deleted successfully')
                                           );
        } catch (Exception $e) {
            $this->session->buyingMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete this buying. Please try again')
                                           );
        }
        $this->_redirect('buying/admin/manage-buying');
    }
    
    /**************************************************************************************************
     *                                         CATEGORY's FUNCTIONS
     **************************************************************************************************/
    public function manageCategoryAction()
    {
        $objLang    = new Models_Lang();
        $objCategory     = new Models_BuyingCategory();
        $objCategoryProduct     = new Models_ProductCategory();
        $objUser = new Models_User();
        $objBuying = new Models_Buying();

        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $this->view->headTitle(Nine_Language::translate('manage Category '));

        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
        
        if($currentPage == false){
        	$currentPage = 1;
        	$this->session->categoryDisplayNum = null;
        	$this->session->categoryCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objCategory->update(array('status' => $value), array('buying_category_gid=?' => $id));
            $this->session->categoryMessage = array(
                                       'success' => true,
                                       'message' => Nine_Language::translate("Edit sort numbers successfully")
                                   );
        }

        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->categoryDisplayNum;
        } else {
            $this->session->categoryDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->categoryCondition;
        } else {
            $this->session->categoryCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();

        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_category', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        /**
         * Get all categorys
         */
        $numRowPerPage = $numRowPerPage;
        $allCategories  = $objCategory->getallCategories($condition, array('sorting ASC', 'buying_category_gid DESC', 'buying_category_id ASC'),
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage
                                                  );

        /**
         * Count all categorys
         */
        $count = count($objCategory->getallCategories($condition));

        /**
         * Format
         */
        
        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allCategories[0]['buying_category_gid'];
        $allUserId = array();

        foreach ($allCategories as $index=>$category) {
        	if ($category['end_date'] < time()){
        		$objCategory->update(array('status' => 3), array('buying_category_gid=?' => $category['buying_category_gid']));
        	}
        	
        	if($category['price'] != null) {
                $category['price'] = number_format($category['price']);
            }
        	if (0 != $category['end_date']) {
                $category['end_date'] = date($config['dateFormat'], $category['end_date']);
            } else {
                $category['end_date'] = '';
            }
            /**
             * Change date
             */
        	
            if (0 != $category['created_date']) {
                $category['created_date'] = date($config['dateFormat'], $category['created_date']);
            } else {
                $category['created_date'] = '';
            }

            if ($tmpGid != $category['buying_category_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $category['buying_category_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $category;
            }
            $tmp2['langs'][]  = $category;
            /**
             * Final element
             */
            if ($index == count($allCategories) - 1) {
                $tmp[] = $tmp2;
            }
        }

        $allCategories = $tmp;

        foreach($allCategories as &$category){
            $conditionBuying = array('buying_category_gid'=>$category['buying_category_gid']);

            $allBuying = $objBuying->getAllBuying($conditionBuying, array('sorting ASC', 'buying_gid DESC') );

            foreach ($allBuying as $index=>$buying) {
                $allUserId[] = $buying['user_id'];
            }

			$category_product = current($objCategoryProduct->getByColumns(array('product_category_gid'=>$category['product_category_gid']))->toArray());
			if($category_product['parent_id'] != ''){
	            $conditionUser = array(
	                'group_id' => 3,
	                'product_category_gid' => $category_product['parent_id'],
	            	'get_user_buying' => 1
	            );
			}else{
	            $conditionUser = array(
	                'group_id' => 3,
	                'product_category_gid' => $category['product_category_gid'],
	            	'get_user_buying' => 1
	            );
	       	}

            $allUsers = $objUser->getAllUsersWithGroup($conditionUser, 'user_id DESC');

            foreach ($allUsers as $index=>$user){
                if(in_array($user['user_id'], $allUserId)){
                    $allUsers[$index]['quote'] = 1;
                }else{
                    $allUsers[$index]['quote'] = 0;
                }
            }
            $category['no_company'] = count($allUsers);
            $category['companies'] = $allUsers;
        }
        unset($category);

    	$export = $this->_getParam('export', false);
		if($export != false){
			  	$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
		
				$style = "style = 'border: 1px solid'";
				
				if(Nine_Language::getCurrentLangId() == 1){
					$header = "<tr>";
					$header .= "<td $style> <b>User Active </b></td>";
					$header .= "<td $style></td>";
					$header .= "<td $style> <b>Status </b></td>";
					$header .= "</tr>";
					$header .= "<tr>";
					$header .= "<td $style> 1 </td>";
					$header .= "<td $style>Active</td>";
					$header .= "<td $style> 1 </td>";
					$header .= "<td $style> Pending Quotation </td>";
					$header .= "</tr>";
					$header .= "<tr>";
					$header .= "<td $style> 0 </td>";
					$header .= "<td $style>No Active</td>";
					$header .= "<td $style> 2 </td>";
					$header .= "<td $style> Showing Quotation </td>";
					$header .= "</tr>";
					$header .= "<tr>";
					$header .= "<td $style></td>";
					$header .= "<td $style></td>";
					$header .= "<td $style> 3 </td>";
					$header .= "<td $style> Expired </td>";
					$header .= "</tr>";
					$header .= "<tr>";
					$header .= "<td $style></td>";
					$header .= "<td $style></td>";
					$header .= "<td $style> 4 </td>";
					$header .= "<td $style> Closed </td>";
					$header .= "</tr>";
					$textHeader = array(
						"<b>No</b>",
						"<b>User Active</b>",
						"<b>Full Name</b>",
						"<b>Company</b>",
						"<b>Price</b>",
						"<b>Created Date</b>",
						"<b>End Date</b>",
						"<b>Status</b>",
						"<b>Title</b>"
					);
				}else{
					$header = "<tr>";
					$header .= "<td $style><b> Tình Trạng Thành Viên </b></td>";
					$header .= "<td $style></td>";
					$header .= "<td $style> <b>Tình Trạng Yêu Cầu </b></td>";
					$header .= "</tr>";
					$header .= "<tr>";
					$header .= "<td $style> 1 </td>";
					$header .= "<td $style>Chính Thức</td>";
					$header .= "<td $style> 1 </td>";
					$header .= "<td $style> Đang Chờ Báo Giá </td>";
					$header .= "</tr>";
					$header .= "<tr>";
					$header .= "<td $style> 0 </td>";
					$header .= "<td $style>Không Chính Thức</td>";
					$header .= "<td $style> 2 </td>";
					$header .= "<td $style> Đang Báo Giá </td>";
					$header .= "</tr>";
					$header .= "<tr>";
					$header .= "<td $style></td>";
					$header .= "<td $style></td>";
					$header .= "<td $style> 3 </td>";
					$header .= "<td $style> Hết Hạn </td>";
					$header .= "</tr>";
					$header .= "<tr>";
					$header .= "<td $style></td>";
					$header .= "<td $style></td>";
					$header .= "<td $style> 4 </td>";
					$header .= "<td $style> Đã Đóng </td>";
					$header .= "</tr>";
					
					$textHeader = array(
						mb_convert_encoding("<b>Số Thứ Tự</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Tình Trạng Thành Viên</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Tên Đầy Đủ</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Tên Công Ty</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Giá Tiền</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Ngày Khởi Tạo</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Ngày Kết Thúc</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Tình Trạng</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Tiêu Đề</b>",'HTML-ENTITIES','UTF-8')
					);
				}
				
				$header .= "<tr>";
				foreach ($textHeader as $text) {
					$header .= "<td $style>" . utf8_encode($text) . "</td>";
				}
				$header .= "</tr>";
		
				$content = '';
				$no = 1;
				
				foreach ($allCategories as $item) {
					$content .="<tr>";
					$content .= "<td $style>" . $no . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['user_id'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['full_name'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['congty'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" .  mb_convert_encoding($item['price'].' '.$item['pname'] ,'HTML-ENTITIES','UTF-8') . "</td>";
					$content .= "<td $style>" .  mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8') . "</td>";
					$content .= "<td $style>" .  mb_convert_encoding($item['end_date'],'HTML-ENTITIES','UTF-8') . "</td>";
					$content .= "<td $style>" .  mb_convert_encoding($item['status'],'HTML-ENTITIES','UTF-8') . "</td>";
					$content .= "<td $style>" .  mb_convert_encoding($item['name'],'HTML-ENTITIES','UTF-8') . "</td>";
					$content .="</tr>";
					$no++;
				}
		
				header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("content-disposition: attachment;filename=thong-ke-buying-" . date('d-m-Y H:i:s') . ".xls");
		
				$xlsTbl = $header;
				$xlsTbl .= $content;
		
				echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
				exit();
				
		}
//        echo print_r($allCategories);die;
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCategories = $allCategories;
        $this->view->categoryMessage = $this->session->categoryMessage;
        $this->session->categoryMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_category');
        $this->view->allLangs = $allLangs;
        
        $this->view->menu = array(
        	0=>'buying',
        	1=>'manager-category-buying'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-buying-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/buying/admin/manage-category',
        		'name'	=>	Nine_Language::translate('Manager Category Buying')
        		)
        	
        );
    }
    
    public function newCategoryAction()
    {
        $objLang = new Models_Lang();
        $objCategory = new Models_BuyingCategory;
        $objBuying = new Models_Buying();
        $objUser = new Models_User();
        
        
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $data = $this->_getParam('data', false);
        /**
         * Get all users
         */
        $allUsers = $objUser->getAllUsersWithGroup();

        /**
         * Get all categories
         */
        $allCats = $objCategory->buildTree($objCategory->getAll(array('sorting ASC'))->toArray());
//        echo "<pre>";print_r($allCats);die; 
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_category', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCategory = $data;
            $newCategory['created_date'] = time();
            $user = $objUser->getUser($data['user_id']);
            $newCategory['full_name'] = $user['full_name'];
            $newCategory['status'] = 2;
            /**
             * Sorting
             */
        	if (null == $newCategory['end_date']) {
                unset($newCategory['end_date']);
            } else {
                $tmp = explode('-', $newCategory['end_date']);
                $newCategory['end_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (false == $this->checkPermission('new_category', null, '*')) {
                $newCategory['genabled']          = 0;
                $newCategory['sorting']           = 1;
            }
            $userSession = Nine_Registry::getLoggedInUser();
            if(isset($_FILES['images']) && $_FILES['images']['tmp_name'] != '') {
                $fileName = basename($_FILES['images']['name']);
                $fileTmp = $_FILES['images']['tmp_name'];
                $uploadPath = 'media/userfiles/user/' . $userSession['user_id'] . '/';
                $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                $uploadFile = $uploadDir . $fileName;

//                $ext_allow = array (
//                    'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX'
//                );
                $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
//                if(in_array($ext, $ext_allow)) {
                move_uploaded_file($fileTmp, $uploadFile);
                $newCategory['images'] = $uploadPath . $fileName;
//                }
            }

        	foreach ($allLangs as $index => $lang) {
                if($newCategory[$lang['lang_id']]['name'] == ""){
                    $newCategory[$lang['lang_id']]['name'] = $newCategory['name'];
                }

            	if($newCategory[$lang['lang_id']]['alias'] == ""){
            		$newCategory[$lang['lang_id']]['alias'] = $objBuying->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
            		$newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newCategory[$lang['lang_id']]['alias']));
            	}
            }
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > @$newCategory['sorting']) {
                    $newCategory['sorting'] = 1;
                }
                if (null == $newCategory['parent_id']) {
                	$newCategory['parent_id'] = NULL;
                }
                $objCategory->increaseSorting($newCategory['sorting'], 1);

                $gid = $objCategory->insert($newCategory);
                
//                $cate_id = $data['product_category_id'];
//                $all_estore = $objUser->getByColumns($columns);
                
                /**
                 * Update id string
                 */
                $objCategory->update(array('gid_string'	=>	$gid), array('buying_category_gid = ?' => $gid));
                $category = @reset($objCategory->getByColumns(array('buying_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);
                /**
                 * Message
                 */
                $this->session->categoryMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Category is created successfully.')
                        );
                
                $this->_redirect('buying/admin/manage-category');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
            if(isset($data['end_date']) && $data['end_date'] != ''){
            	$data['end_date']  = date('d-m-Y', $data['end_date'] );	
            }else{
            	$data['end_date'] = date("d-m-Y",time());	
            }
            
            
        }
        /**
         * Prepare for template
         */
        $this->view->allUsers = $allUsers;
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Category'));
        $this->view->fullPermisison = $this->checkPermission('new_category', null, '*'); 
        
        $this->view->menu = array(
        	0=>'buying',
        	1=>'new-category-buying'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-buying-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/buying/admin/manage-category',
        		'name'	=>	Nine_Language::translate('Manager Category Buying')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-plus',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('New Category Buying')
        		)
        	
        );
        $objCategoryProperti = new Models_PropertiesCategory();
        $allCatsProperties = $objCategoryProperti->buildTree($objCategory->getAllBuyingCatWithParent(45));
        $this->view->allCatsProperties = $allCatsProperties;
        
        
        $objCatProduct     = new Models_ProductCategory();
        $allCatsProduct = $objCatProduct->buildTree($objCatProduct->getAll(array('sorting ASC'))->toArray());
        
        $this->view->allCatsProduct = $allCatsProduct;
        
        $objCategoryProperties = new Models_PropertiesCategory;
        
        $unitProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(45));
        $this->view->unitProperties = $unitProperties;
        
        $colorProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(39));
        $this->view->colorProperties = $colorProperties;
        
        $trongluongProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(27));
        $this->view->trongluongProperties = $trongluongProperties;
        
        $dvtProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(53));
        $this->view->dvtProperties = $dvtProperties;
        
        $thanhtoanProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(37));
        $this->view->thanhtoanProperties = $thanhtoanProperties;
        
        $donggoiProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(67));
        $this->view->donggoiProperties = $donggoiProperties;
        
        
        
    	$objCountry = new Models_CountryCategory();
    	$allCountry = $objCountry->getAllCategoriesParentNull();
        $this->view->allCountry = $allCountry;
        
        $objHs = new Models_HsCategory();
    	$allHsQt = $objHs->buildTree($objHs->getAllCategoriesOnparentkeyUser(25));
        $this->view->allHsQt = $allHsQt;
        
        $allHsVn = $objHs->buildTree($objHs->getAllCategoriesOnparentkeyUser(27));
        $this->view->allHsVn = $allHsVn;
        
    }
    

    
    public function editCategoryAction()
    {
        $objCategory     = new Models_BuyingCategory();
        $objLang    = new Models_Lang();
        $objBuying = new Models_Buying();
        $objUser = new  Models_User();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false); 
        if (false == $gid) {
            $this->_redirect('buying/admin/manage-category');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_category', null, '*'))
        ||  (false != $lid && false == $this->checkPermission('edit_category', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $data   = $this->_getParam('data', false);
        $enabled    = $this->_getParam('enabled', false); 
        if($enabled != false){
	    	foreach ($enabled as $key=>$value) {
	            $objBuying->update(array('enabled' => $value), array('buying_gid=?' => $key));
	            $this->session->buyingMessage = array(
	                                       'success' => true,
	                                       'message' => Nine_Language::translate("Buying Order is edited successfully")
	                                   );
	        }
    	}
        /**
         * Get all users
         */
        $allUsers = $objUser->getAllUsersWithGroup();
        
        /**
         * Get all categories
         */
        $allCats = $objCategory->buildTree($objCategory->getAll(array('sorting ASC'))->toArray());
        
        /**
         * Get all category languages
         */
        
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allCategoryLangs = $objCategory->setAllLanguages(true)->getByColumns(array('buying_category_gid=?' => $gid))->toArray();
//        echo "<pre>";print_r($allCategoryLangs);die; 
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_category', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allCategoryLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }
        
        $errors = array();
        if (false !== $data) {

            /**
             * Insert new category
             */
            $newCategory = $data;
            /**
             * Sorting
             */
        	if (null == $newCategory['end_date']) {
                unset($newCategory['end_date']);
            } else {
                $tmp = explode('-', $newCategory['end_date']);
                $newCategory['end_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (false == $this->checkPermission('new_category', null, '*')) {
                unset($newCategory['genabled']);
                unset($newCategory['sorting']);
            }

            $userSession = Nine_Registry::getLoggedInUser();
            if(isset($_FILES['images']) && $_FILES['images']['tmp_name'] != '') {
                $fileName = basename($_FILES['images']['name']);
                $fileTmp = $_FILES['images']['tmp_name'];
                $uploadPath = 'media/userfiles/user/' . $userSession['user_id'] . '/';
                $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                $uploadFile = $uploadDir . $fileName;

                $ext_allow = array (
                    'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX'
                );
                $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
                if(in_array($ext, $ext_allow)) {
                    move_uploaded_file($fileTmp, $uploadFile);
                    $newCategory['images'] = $uploadPath . $fileName;
                }
            }

        	foreach ($allLangs as $index => $lang) {
            	if($newCategory[$lang['lang_id']]['alias'] == ""){
            		$newCategory[$lang['lang_id']]['alias'] = $objBuying->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
            		$newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newCategory[$lang['lang_id']]['alias']));
            	}
            }
            try {
                /**                
                 * Update
                 */
              	if (null == $newCategory['parent_id']) {
                	$newCategory['parent_id'] = NULL;
                }
                /**
                 * Delete gid in parent
                 */
                $oldCategory = @reset($allCategoryLangs);
//                echo "<pre>";print_r($oldCategory);die; 
                $objCategory->deleteGidString($oldCategory['parent_id'], $oldCategory['gid_string']);
                
                /**
                 * Update new data
                 */
                $objCategory->update($newCategory, array('buying_category_gid=?' => $gid));
                
                /**
                 * Update new id string
                 */
                $category = @reset($objCategory->getByColumns(array('buying_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);
                
                /**
                 * Message
                 */
                $this->session->categoryMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Category is updated successfully.')
                        );
                
                $this->_redirect('buying/admin/manage-category');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allCategoryLangs);
            if (false == $data) {
                $this->session->categoryMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate("Category doesn't exist.")
                                           );
                $this->_redirect('buying/admin/manage-category');
            }
            /**
             * Format image
             */
            $image = explode('/', $data['images']);
            $fileName = end($image);

            /**
             * Get all lang categorys
             */
            foreach ($allCategoryLangs as $category) {
                $data[$category['lang_id']] = $category;
            }
            
             /**
             * Get all child category
             */
            $allChildCats = explode(',',trim($data['gid_string'],','));
            unset($allChildCats[0]);
           	foreach($allCats as $key =>	$item) {
           		if (false != in_array($item, $allChildCats)) {
           			unset($allCats[$key]);
           		}
           	}
        }
        
		if($data['end_date'] != ''){
        	$data['end_date'] = date("d-m-Y",$data['end_date']);
		}else{
			$data['end_date'] = date("d-m-Y",time());
		}
        /**
         * Remove it and its childs from category list
         */
        $oldData = @reset($allCategoryLangs);
        foreach ($allCats as $index => $item) {
            if (false !== strpos(",{$oldData['gid_string']},", ",{$item['buying_category_gid']},")) {
                unset($allCats[$index]);
            }
        }
        /**
         * Prepare for template
         */
        $this->view->allUsers = $allUsers;
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Category'));
        $this->view->fullPermisison = $this->checkPermission('edit_category', null, '*');
         $this->view->menu = array(
        	0=>'buying',
        	1=>'manager-category-buying'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-buying-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/buying/admin/manage-category',
        		'name'	=>	Nine_Language::translate('Manager Category Buying')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-pencil',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('Edit Category Buying')
        		)
        	
        );
        $objCategoryProperti = new Models_PropertiesCategory();
        $allCatsProperties = $objCategoryProperti->getAllCategoriesOnparentkey(45);
        $this->view->allCatsProperties = $allCatsProperties;
        
        $allCatsPropertiesRequest = $objCategoryProperti->getAllCategoriesOnparentkey(49);
        $this->view->allCatsPropertiesRequest = $allCatsPropertiesRequest;
        
        $objCategoryProperti = new Models_PropertiesCategory();
        $allCatsProperties = $objCategoryProperti->buildTree($objCategory->getAllBuyingCatWithParent(45));
        $this->view->allCatsProperties = $allCatsProperties;
        
        
        $objCatProduct     = new Models_ProductCategory();
        $allCatsProduct = $objCatProduct->buildTree($objCatProduct->getAll(array('sorting ASC'))->toArray());
        
        $this->view->allCatsProduct = $allCatsProduct;
        
        $objCategoryProperties = new Models_PropertiesCategory;
        
        $unitProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(45));
        $this->view->unitProperties = $unitProperties;
        
        $colorProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(39));
        $this->view->colorProperties = $colorProperties;
        
        $trongluongProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(27));
        $this->view->trongluongProperties = $trongluongProperties;
        
        $dvtProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(53));
        $this->view->dvtProperties = $dvtProperties;
        
        $thanhtoanProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(37));
        $this->view->thanhtoanProperties = $thanhtoanProperties;
        
        $donggoiProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(67));
        $this->view->donggoiProperties = $donggoiProperties;
        
        
        
    	$objCountry = new Models_CountryCategory();
    	$allCountry = $objCountry->getAllCategoriesParentNull();
        $this->view->allCountry = $allCountry;
        
        $objHs = new Models_HsCategory();
    	$allHsQt = $objHs->buildTree($objHs->getAllCategoriesOnparentkeyUser(25));
        $this->view->allHsQt = $allHsQt;
        
        $allHsVn = $objHs->buildTree($objHs->getAllCategoriesOnparentkeyUser(27));
        $this->view->allHsVn = $allHsVn;
        
        $objBuying = new Models_Buying();

		$conditionBuying = array('buying_category_gid'=>$gid);
		$allUserId = array();
		$user_active = 0;
		$allBuying = $objBuying->getAllBuying($conditionBuying, array('sorting ASC', 'buying_gid DESC', 'buying_id ASC') );
		$tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allBuying[0]['buying_gid'];
        foreach ($allBuying as $index=>$buying) {
            /**
             * Change date
             */
        	if($buying['enabled'] == 1){
        		$user_active = $buying['user_id'];
        	}
        	$allUserId[] = $buying['user_id'];
            if (0 != $buying['created_date']) {
                $buying['created_date'] = date('d-m-Y', $buying['created_date']);
            } else {
                $buying['created_date'] = '';
            }
            if (0 != $buying['publish_up_date']) {
                $buying['publish_up_date'] = date('d-m-Y', $buying['publish_up_date']);
            } else {
                $buying['publish_up_date'] = '';
            }
            if (0 != $buying['publish_down_date']) {
                $buying['publish_down_date'] = date('d-m-Y', $buying['publish_down_date']);
            } else {
                $buying['publish_down_date'] = '';
            }
            if ($tmpGid != $buying['buying_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $buying['buying_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $buying;
                $tmp2['hit'] = 0;
            }
            $tmp2['hit']    += $buying['hit'];
            $tmp2['langs'][]  = $buying;
            /**
             * Final element
             */
            if ($index == count($allBuying) - 1) {
                $tmp[] = $tmp2;
            }
        }
        $allBuying = $tmp;


        $selectedQuote = false;
        foreach ($allBuying as $index=>$buying) {
            if ($buying['enabled'] == 2) {
                $selectedQuote = true;
                break;
            }
        }

		$this->view->allBuying = $allBuying;
		
		$objUser = new Models_User();
		$conditionUser = array(
			'group_id' => 3,
			'product_category_gid' => $data['product_category_gid']
		);
        $allUser = $objUser->getAllUsersWithGroup($conditionUser, 'user_id DESC');

        foreach ($allUsers as $index=>$user){
        	if(in_array($user['user_id'], $allUserId)){
        		$allUsers[$index]['quote'] = 1;
        	}else{
        		$allUsers[$index]['quote'] = 0;
        	}
        }
        $this->view->gid = $gid;
        $this->view->selectedQuote = $selectedQuote;
        $this->view->fileName = $fileName;
        $this->view->allUser = $allUser;
        $this->view->user_active =  $user_active;
    }

    public function enableCategoryAction()
    {
        $objCategory = new Models_BuyingCategory;
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('buying/admin/manage-category');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_category', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('genabled' => 1), array('buying_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_category', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('enabled' => 1), array('buying_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('buying/admin/manage-category');
    }

    
    public function disableCategoryAction()
    {
        $objCategory = new Models_BuyingCategory;
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('buying/admin/manage-category');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_category', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('genabled' => 0), array('buying_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_category', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('enabled' => 0), array('buying_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('buying/admin/manage-category');
    }
    
    public function deleteCategoryAction()
    {
        $objCategory = new Models_BuyingCategory;
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_category')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid = $this->_getParam('gid', false);
        
        if (false == $gid) {
            $this->_redirect('buying/admin/manage-category');
        }
        
        $gids = explode('_', trim($gid, '_'));
        
        try {
            foreach ($gids as $gid) {
            	
            	$cat = @reset($objCategory->getByColumns(array('buying_category_gid=?'=>$gid))->toArray());
            	if ( 0 == $cat['buying_deleteable']){
            		$this->session->categoryMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete category ('. $gid .'). Please try again')
                                           );
                    $this->_redirect('buying/admin/manage-category');
            	}
            	else {
            		$objCategory->delete(array('buying_category_gid=?' => $gid));
            	}
            }
            $this->session->categoryMessage = array(
                                               'success' => true,
                                               'message' => Nine_Language::translate('Category is deleted successfully')
                                           );
        } catch (Exception $e) {
            $this->session->categoryMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete this category. Please try again')
                                           );
        }
        $this->_redirect('buying/admin/manage-category');
    }

    public function changeStringAction()
    {
    	$objBuying = new Models_Buying();
    	$str = $this->_getParam("string","");
    	$str = $objBuying->convert_vi_to_en($str);
    	$str = str_replace(" ", "-", trim($str));
    	echo $str;die;
    }

    public function agreeQuoteAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $objBuying = new Models_Buying();
        $objBuyingCategory = new Models_BuyingCategory();

        $gid = $this->_request->getParam('gid', false);
        $category_gid = $this->_request->getParam('category_gid', false);
        if (false == $gid) {
            $this->_redirect('buying/admin/manage-category');
        }

        if ($gid != null && $gid != '') {
            $objBuying->update(array('enabled' => 2), array('buying_gid=?' => $gid));
            $objBuyingCategory->update(array('status' => 4), array('buying_category_gid=?' => $category_gid));
            $this->_redirect('buying/admin/edit-category/gid/' . $category_gid);
        } else {
            $this->_redirect('buying/admin/manage-category');
        }

    }
}