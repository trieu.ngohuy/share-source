<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/buying/models/Buying.php';
require_once 'modules/buying/models/BuyingCategory.php';
require_once 'modules/properties/models/PropertiesCategory.php';
require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/user/models/User.php';
require_once 'modules/country/models/CountryCategory.php';
require_once 'modules/hs/models/HsCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/user/models/Group.php';
include_once 'modules/mail/models/Mail.php';
include_once 'modules/mail/models/phpmailer.php';
include_once 'modules/mail/models/smtp.php';

class buying_EstoreController extends Nine_Controller_Action
{
    public function manageBuyingRequestAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
        }

        $this->setLayout('estore');

        $this->view->alias = $user['alias'] . '.html';
        $this->view->username = $user['alias'];
        $objLang    = new Models_Lang();
        $objCategory     = new Models_BuyingCategory();
        $objUser = new Models_User();
        $objBuying = new Models_Buying();

        /**
         * Check permission
         */
//        if (false == $this->checkPermission('see_category', null, '?')) {
//            $this->_forwardToNoPermissionPage();
//            return;
//        }

        $this->view->headTitle(Nine_Language::translate('manage Category '));

        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);

        if($currentPage == false){
            $currentPage = 1;
            $this->session->categoryDisplayNum = null;
            $this->session->categoryCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objCategory->update(array('status' => $value), array('buying_category_gid=?' => $id));
            $this->session->categoryMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("Edit sort numbers successfully")
            );
        }

        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->categoryDisplayNum;
        } else {
            $this->session->categoryDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->categoryCondition;
        } else {
            $this->session->categoryCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }

        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_category', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        /**
         * Get all categorys
         */
        $numRowPerPage = $numRowPerPage;
        $allCategories  = $objCategory->getAllCategoriesEstore($condition, array('sorting ASC', 'buying_category_gid DESC', 'buying_category_id ASC'),
            $numRowPerPage,
            ($currentPage - 1) * $numRowPerPage
        );

        /**
         * Count all categorys
         */
        $count = count($objCategory->getAllCategoriesEstore($condition));
        /**
         * Format
         */

        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allCategories[0]['buying_category_gid'];
        $allUserId = array();
        foreach ($allCategories as $index=>$category) {
            if ($category['end_date'] < time()){
                $objCategory->update(array('status' => 3), array('buying_category_gid=?' => $category['buying_category_gid']));
            }

            $category['price'] != null ?number_format($category['price']) : '';
            if (0 != $category['end_date']) {
                $category['end_date'] = date($config['dateFormat'], $category['end_date']);
            } else {
                $category['end_date'] = '';
            }
            /**
             * Change date
             */

            if (0 != $category['created_date']) {
                $category['created_date'] = date($config['dateFormat'], $category['created_date']);
            } else {
                $category['created_date'] = '';
            }

            if ($tmpGid != $category['buying_category_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $category['buying_category_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $category;
            }
            $tmp2['langs'][]  = $category;
            /**
             * Final element
             */
            if ($index == count($allCategories) - 1) {
                $tmp[] = $tmp2;
            }
        }

        $allCategories = $tmp;

        $export = $this->_getParam('export', false);
        if($export != false){
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $style = "style = 'border: 1px solid'";

            if(Nine_Language::getCurrentLangId() == 1){
                $header = "<tr>";
                $header .= "<td $style> <b>User Active </b></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> <b>Status </b></td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style> 1 </td>";
                $header .= "<td $style>Active</td>";
                $header .= "<td $style> 1 </td>";
                $header .= "<td $style> Pending Quotation </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style> 0 </td>";
                $header .= "<td $style>No Active</td>";
                $header .= "<td $style> 2 </td>";
                $header .= "<td $style> Showing Quotation </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 3 </td>";
                $header .= "<td $style> Expired </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 4 </td>";
                $header .= "<td $style> Closed </td>";
                $header .= "</tr>";
                $textHeader = array(
                    "<b>No</b>",
                    "<b>User Active</b>",
                    "<b>Full Name</b>",
                    "<b>Company</b>",
                    "<b>Price</b>",
                    "<b>Created Date</b>",
                    "<b>End Date</b>",
                    "<b>Status</b>",
                    "<b>Title</b>"
                );
            }else{
                $header = "<tr>";
                $header .= "<td $style><b> T?nh Tr?ng Th�nh Vi�n </b></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> <b>T?nh Tr?ng Y�u C?u </b></td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style> 1 </td>";
                $header .= "<td $style>Ch�nh Th?c</td>";
                $header .= "<td $style> 1 </td>";
                $header .= "<td $style> �ang Ch? B�o Gi� </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style> 0 </td>";
                $header .= "<td $style>Kh�ng Ch�nh Th?c</td>";
                $header .= "<td $style> 2 </td>";
                $header .= "<td $style> �ang B�o Gi� </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 3 </td>";
                $header .= "<td $style> H?t H?n </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 4 </td>";
                $header .= "<td $style> �? ��ng </td>";
                $header .= "</tr>";

                $textHeader = array(
                    mb_convert_encoding("<b>S? Th? T?</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>T?nh Tr?ng Th�nh Vi�n</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>T�n �?y �?</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>T�n C�ng Ty</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Gi� Ti?n</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ng�y Kh?i T?o</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ng�y K?t Th�c</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>T?nh Tr?ng</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ti�u �?</b>",'HTML-ENTITIES','UTF-8')
                );
            }

            $header .= "<tr>";
            foreach ($textHeader as $text) {
                $header .= "<td $style>" . utf8_encode($text) . "</td>";
            }
            $header .= "</tr>";

            $content = '';
            $no = 1;

            foreach ($allCategories as $item) {
                $content .="<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['user_id'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['full_name'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['congty'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['price'].' '.$item['pname'] ,'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['end_date'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['status'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['name'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .="</tr>";
                $no++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-buying-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();

        }
//        echo print_r($allCategories);die;
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCategories = $allCategories;
        $this->view->categoryMessage = $this->session->categoryMessage;
        $this->session->categoryMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_category');
        $this->view->allLangs = $allLangs;

        $this->view->menu = array(
            0=>'buying',
            1=>'manager-category-buying'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-buying-hunt',
                'url'	=>	Nine_Registry::getBaseUrl().'admin/buying/admin/manage-category',
                'name'	=>	Nine_Language::translate('Manager Category Buying')
            )
        );
    }

    public function newBuyingRequestAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();

        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
        }

        $this->setLayout('estore');

        $this->view->alias = $user['alias'] . '.html';
        $this->view->username = $user['alias'];
        $objLang    = new Models_Lang();
        $objCategory     = new Models_BuyingCategory();
        $objUser = new Models_User();
        $objBuying = new Models_Buying();

        /**
         * Check permission
         */
//        if (false == $this->checkPermission('see_category', null, '?')) {
//            $this->_forwardToNoPermissionPage();
//            return;
//        }

        $this->view->headTitle(Nine_Language::translate('manage Category '));

        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);

        if($currentPage == false){
            $currentPage = 1;
            $this->session->categoryDisplayNum = null;
            $this->session->categoryCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objCategory->update(array('status' => $value), array('buying_category_gid=?' => $id));
            $this->session->categoryMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("Edit sort numbers successfully")
            );
        }

        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->categoryDisplayNum;
        } else {
            $this->session->categoryDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->categoryCondition;
        } else {
            $this->session->categoryCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }

        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_category', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

//        $product_category_gid = explode('||', $userSession['product_category_gid']);
//        $condition['product_category_gid'] = implode(',', array_diff($product_category_gid, array('')));
        $condition['user_id'] = $user['user_id'];
        /**
         * Get all categorys
         */
        $numRowPerPage = $numRowPerPage;
        $allCategories  = $objCategory->getNewCategoriesEstore($condition, array('sorting ASC', 'buying_category_gid DESC', 'buying_category_id ASC'),
            $numRowPerPage,
            ($currentPage - 1) * $numRowPerPage
        );

        /**
         * Count all categorys
         */
        $count = count($objCategory->getNewCategoriesEstore($condition));
        /**
         * Format
         */

        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allCategories[0]['buying_category_gid'];
        $allUserId = array();
        foreach ($allCategories as $index=>$category) {
            if ($category['end_date'] < time()){
                $objCategory->update(array('status' => 3), array('buying_category_gid=?' => $category['buying_category_gid']));
            }

            $category['price'] = $category['price'] != null ?number_format($category['price']) : '';
            if (0 != $category['end_date']) {
                $category['end_date'] = date($config['dateFormat'], $category['end_date']);
            } else {
                $category['end_date'] = '';
            }
            /**
             * Change date
             */

            if (0 != $category['created_date']) {
                $category['created_date'] = date($config['dateFormat'], $category['created_date']);
            } else {
                $category['created_date'] = '';
            }

            if ($tmpGid != $category['buying_category_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $category['buying_category_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $category;
            }
            $tmp2['langs'][]  = $category;
            /**
             * Final element
             */
            if ($index == count($allCategories) - 1) {
                $tmp[] = $tmp2;
            }
        }

        $allCategories = $tmp;

        $export = $this->_getParam('export', false);
        if($export != false){
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $style = "style = 'border: 1px solid'";

            if(Nine_Language::getCurrentLangId() == 1){
                $header = "<tr>";
                $header .= "<td $style> <b>User Active </b></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> <b>Status </b></td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style> 1 </td>";
                $header .= "<td $style>Active</td>";
                $header .= "<td $style> 1 </td>";
                $header .= "<td $style> Pending Quotation </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style> 0 </td>";
                $header .= "<td $style>No Active</td>";
                $header .= "<td $style> 2 </td>";
                $header .= "<td $style> Showing Quotation </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 3 </td>";
                $header .= "<td $style> Expired </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 4 </td>";
                $header .= "<td $style> Closed </td>";
                $header .= "</tr>";
                $textHeader = array(
                    "<b>No</b>",
                    "<b>User Active</b>",
                    "<b>Full Name</b>",
                    "<b>Company</b>",
                    "<b>Price</b>",
                    "<b>Created Date</b>",
                    "<b>End Date</b>",
                    "<b>Status</b>",
                    "<b>Title</b>"
                );
            }else{
                $header = "<tr>";
                $header .= "<td $style><b> T?nh Tr?ng Th�nh Vi�n </b></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> <b>T?nh Tr?ng Y�u C?u </b></td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style> 1 </td>";
                $header .= "<td $style>Ch�nh Th?c</td>";
                $header .= "<td $style> 1 </td>";
                $header .= "<td $style> �ang Ch? B�o Gi� </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style> 0 </td>";
                $header .= "<td $style>Kh�ng Ch�nh Th?c</td>";
                $header .= "<td $style> 2 </td>";
                $header .= "<td $style> �ang B�o Gi� </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 3 </td>";
                $header .= "<td $style> H?t H?n </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 4 </td>";
                $header .= "<td $style> �? ��ng </td>";
                $header .= "</tr>";

                $textHeader = array(
                    mb_convert_encoding("<b>S? Th? T?</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>T?nh Tr?ng Th�nh Vi�n</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>T�n �?y �?</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>T�n C�ng Ty</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Gi� Ti?n</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ng�y Kh?i T?o</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ng�y K?t Th�c</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>T?nh Tr?ng</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ti�u �?</b>",'HTML-ENTITIES','UTF-8')
                );
            }

            $header .= "<tr>";
            foreach ($textHeader as $text) {
                $header .= "<td $style>" . utf8_encode($text) . "</td>";
            }
            $header .= "</tr>";

            $content = '';
            $no = 1;

            foreach ($allCategories as $item) {
                $content .="<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['user_id'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['full_name'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['congty'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['price'].' '.$item['pname'] ,'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['end_date'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['status'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['name'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .="</tr>";
                $no++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-buying-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();

        }
//        echo print_r($allCategories);die;
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCategories = $allCategories;
        $this->view->categoryMessage = $this->session->categoryMessage;
        $this->session->categoryMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_category');
        $this->view->allLangs = $allLangs;

        $this->view->menu = array(
            0=>'buying',
            1=>'new-category-buying'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-buying-hunt',
                'url'	=>	Nine_Registry::getBaseUrl().'admin/buying/admin/manage-category',
                'name'	=>	Nine_Language::translate('Manager Category Buying')
            )

        );
    }

    public function editBuyingRequestAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
        }

        $this->setLayout('estore');

        $objCategory     = new Models_BuyingCategory();
        $objLang    = new Models_Lang();
        $objBuying = new Models_Buying();
        $objUser = new  Models_User();
        /**
         * Check permission
         */
//        if (false == $this->checkPermission('edit_category', null, '?')) {
//            $this->_forwardToNoPermissionPage();
//            return;
//        }

        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false);
        if (false == $gid) {
            $this->_redirect('buying/admin/manage-category');
        }
        /**
         * Check permission
         */
//        if ((false == $lid && false == $this->checkPermission('edit_category', null, '*'))
//            ||  (false != $lid && false == $this->checkPermission('edit_category', null, $lid))) {
//            $this->_forwardToNoPermissionPage();
//            return;
//        }

        $data   = $this->_getParam('data', false);
        $enabled    = $this->_getParam('enabled', false);
        if($enabled != false){
            foreach ($enabled as $key=>$value) {
                $objBuying->update(array('enabled' => $value), array('buying_gid=?' => $key));
                $this->session->buyingMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate("Buying Order is edited successfully")
                );
            }
        }
        /**
         * Get all users
         */
        $condition['group_id'] = 4;
        $allUsers = $objUser->getAllUsersWithGroup($condition);

        /**
         * Get all category languages
         */

        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allCategoryLangs = $objCategory->setAllLanguages(true)->getByColumns(array('buying_category_gid=?' => $gid))->toArray();
//        echo "<pre>";print_r($allCategoryLangs);die;
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index=>$lang) {
            if (false == $this->checkPermission('edit_category', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allCategoryLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }

        $errors = array();
        /**
         * Get old data
         */
        $data = @reset($allCategoryLangs);
        if (false == $data) {
            $this->session->categoryMessage = array(
                'success' => false,
                'message' => Nine_Language::translate("Category doesn't exist.")
            );
            $this->_redirect('buying/admin/manage-category');
        }
        /**
         * Format image
         */
        $image = explode('/', $data['images']);
        $fileName = end($image);

        /**
         * Get all lang categorys
         */
        foreach ($allCategoryLangs as $category) {
            $data[$category['lang_id']] = $category;
        }

        /**
         * Get all child category
         */
        $allChildCats = explode(',',trim($data['gid_string'],','));
        unset($allChildCats[0]);

        if($data['end_date'] != ''){
            $data['end_date'] = date("d-m-Y",$data['end_date']);
        }
        /**
         * Remove it and its childs from category list
         */
        $oldData = @reset($allCategoryLangs);

        /**
         * Prepare for template
         */
        $this->view->allUsers = $allUsers;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Category'));
        $this->view->fullPermisison = $this->checkPermission('edit_category', null, '*');
        $this->view->menu = array(
            0=>'buying',
            1=>'manager-category-buying'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-buying-hunt',
                'url'	=>	Nine_Registry::getBaseUrl().'admin/buying/admin/manage-category',
                'name'	=>	Nine_Language::translate('Manager Category Buying')
            ),
            1=>array(
                'icon' 	=> 	'fa-pencil',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('Edit Category Buying')
            )

        );

        $objCatProduct     = new Models_ProductCategory();
        $allCatsProduct = $objCatProduct->getByColumns(array('product_category_gid=?' => $data['product_category_gid']));
        $this->view->catProduct = $allCatsProduct[0]['name'];

        $objCategoryProperties = new Models_PropertiesCategory;
        $unitProperties = $objCategoryProperties->getByColumns(array('properties_category_gid=?' => $data['current']));
        $this->view->unitProperties = $unitProperties[0]['name'];

        $dvtProperties = $objCategoryProperties->getByColumns(array('properties_category_gid=?' => $data['so_luong_gid']));
        $this->view->dvtProperties = $dvtProperties[0]['name'];

        $thanhtoanProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(37));
        $this->view->thanhtoanProperties = $thanhtoanProperties;

        $donggoiProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(67));
        $this->view->donggoiProperties = $donggoiProperties;



        $objCountry = new Models_CountryCategory();
        $allCountry = $objCountry->getByColumns(array('country_category_gid=?' => $data['country_category_gid']));
        $this->view->allCountry = $allCountry[0]['name'];

        $objHs = new Models_HsCategory();
        $allHsQt = $objHs->buildTree($objHs->getAllCategoriesOnparentkeyUser(25));
        $this->view->allHsQt = $allHsQt;

        $allHsVn = $objHs->buildTree($objHs->getAllCategoriesOnparentkeyUser(27));
        $this->view->allHsVn = $allHsVn;

        $objBuying = new Models_Buying();
        $conditionBuying = array('buying_category_gid'=>$gid, 'user_id'=>$user['user_id']);
        $allUserId = array();
        $user_active = 0;
        $allBuying = $objBuying->getAllBuying($conditionBuying, array('sorting ASC', 'buying_gid DESC', 'buying_id ASC') );
        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allBuying[0]['buying_gid'];
        foreach ($allBuying as $index=>$buying) {
            /**
             * Change date
             */
            if($buying['enabled'] == 1){
                $user_active = $buying['user_id'];
            }
            $allUserId[] = $buying['user_id'];
            if (0 != $buying['created_date']) {
                $buying['created_date'] = date('d-m-Y', $buying['created_date']);
            } else {
                $buying['created_date'] = '';
            }
            if (0 != $buying['publish_up_date']) {
                $buying['publish_up_date'] = date('d-m-Y', $buying['publish_up_date']);
            } else {
                $buying['publish_up_date'] = '';
            }
            if (0 != $buying['publish_down_date']) {
                $buying['publish_down_date'] = date('d-m-Y', $buying['publish_down_date']);
            } else {
                $buying['publish_down_date'] = '';
            }
            if ($tmpGid != $buying['buying_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $buying['buying_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $buying;
                $tmp2['hit'] = 0;
            }
            $tmp2['hit']    += $buying['hit'];
            $tmp2['langs'][]  = $buying;
            /**
             * Final element
             */
            if ($index == count($allBuying) - 1) {
                $tmp[] = $tmp2;
            }
        }
        $allBuying = $tmp;

        $quote = false;
        if(count($allBuying) > 0) {
            $quote = true;
        }

        $objUser = new Models_User();
        $allUsers = $objUser->getByColumns(array('user_id=?'=>$data['user_id']));

        $allDvtProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(53));
        $this->view->allDvtProperties = $allDvtProperties;

        $allUnitProperties = $objCategoryProperties->buildTree($objCategoryProperties->getAllCategoriesOnparentkeyUser(45));
        $this->view->allUnitProperties = $allUnitProperties;

        $this->view->gid = $gid;
        $this->view->fileName = $fileName;
        $this->view->allUsers = $allUsers[0]['full_name'];
        $this->view->user_active =  $user_active;
        $this->view->alias = $user['alias'] . '.html';
        $this->view->username = $user['alias'];
        $this->view->allBuying = $allBuying;
        $this->view->quote = $quote;
    }
	public function sendmail_smtp($data)
    {
        // Mail
        define('MAIL_PROTOCOL', 'smtp');
        define('MAIL_HOST', 'ssl://smtp.googlemail.com');
        define('MAIL_PORT', '465');
        define('MAIL_USER', 'luutn.it@gmail.com');
        define('MAIL_PASS', 'Linh2204');
        define('MAIL_SEC', 'ssl');
        define('MAIL_FROM', 'no.reply.C&D@gmail.com');


        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPAuth = TRUE;
        $mail->SMTPSecure = MAIL_SEC;
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASS;
        $mail->CharSet = "UTF-8";
        $mail->WordWrap = 50;

        $mail->IsHTML(TRUE);
        $mail->From = MAIL_USER;
        $mail->FromName = $data['name_from'];
        $to = $data['email_to'];
        $name = $data['name_to'];
        if (is_array($to)) {
            foreach ($to as $key => $sto) {
                $mail->AddAddress($sto, '');
            }
        } else {
            $mail->AddAddress($to, $name);
        }

        $mail->AddReplyTo(MAIL_USER, "C&D");
        $mail->Subject = $data['title_email'];
        $mail->Body = $data['content'];
        $mail->AltBody = $data['content'];
        if (!$mail->Send()) {
            return false;
        } else {
            return true;
        }
    }
    public function quoteBuyingRequestAction()
    {
        $url = $this->_request->getParams();
        $objCategory     = new Models_BuyingCategory();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
        }
        $data = $this->_getParam('data', false);
        $gid = $data['buying_category_gid'];
        $allCategoryLangs = $objCategory->setAllLanguages(true)->getByColumns(array('buying_category_gid=?' => $gid))->toArray();
        $allCategoryLangs =current($allCategoryLangs);
       
        if(false != $data) {

            $objBuying = new Models_Buying();
            $newBuying = $data;
            $newBuying['created_date'] = time();
            $newBuying['user_id'] = $user['user_id'];
            $newBuying['tiente'] = 'VND';
            $newBuying['price'] = $newBuying['price_sale'];
            $newBuying['enabled'] = 1;
            $newBuying['title'] = $user['full_name'];
            $newBuying['intro_text'] = $newBuying['full_text'];
            $newBuying['name_company'] = $user['name_company'];

            if(isset($_FILES['files']) && $_FILES['files']['tmp_name'] != '') {
                $fileName = basename($_FILES['files']['name']);
                $fileTmp = $_FILES['files']['tmp_name'];
                $uploadPath = 'media/userfiles/user/' . $userSession['user_id'] . '/';
                $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                $uploadFile = $uploadDir . $fileName;

                $ext_allow = array (
                    'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX'
                );
                $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
                if(in_array($ext, $ext_allow)) {
                    move_uploaded_file($fileTmp, $uploadFile);
                    $newBuying['file_attached'] = $uploadPath . $fileName;
                }
            }
            
           
            try {
            
            	$user_send = $objUser->getUser($allCategoryLangs['user_id']);
            	$objBuying->insert($newBuying);
            
            	$content = '<p style="margin-bottom:15px; font-size: 16px; line-height: 25px;">';

                    $content .= Nine_Language::translate('You have a new Quote for your Buing Request from C&D.COM<br/> Please visit the following link for details<br/> ');
                    $content .= '</p>';
                    $content .= '<p> <a href="' . Nine_Registry::getBaseUrl() . 'buying-request/' . $allCategoryLangs['alias'] . '>' . Nine_Language::translate('Click Here') . '</a></p><br/>
                    	This email was sent from C&D.COM<br/>
Read our Privacy Policy and Terms of Use<br/>

 
[Address]: 2nd floor, 132-134 Dien Bien Phu, Dakao Ward, Ho Chi Minh City, Vietnam<br/>
[Phone]: (+84) 8 3820 1386<br/>
[Website]: http://www.C&D  -  http://www.miphabros.com<br/>
[Email]: support@C&D.com<br/>
                    
                    ';



                    $data_send = array(
                        'name_to' => "C&D Service ",
                        'email_to' => array($user_send['email']),
                        'title_email' => Nine_Language::translate('New Message from C&D '),
                        'name_from' => Nine_Language::translate("C&D Service "),
                        'content' => $content
                    );
                    $this->sendmail_smtp($data_send);
            	
            } catch (Exception $e) {
            	echo '<pre>';
            	echo print_r($e);
            	echo '</pre>';die;
            }
            
            
        }
        
		$objCategory->update(array('view'=>1), array('buying_category_gid = ? '=>$allCategoryLangs['buying_category_gid']));
        $this->_redirect('buying-request/' . $allCategoryLangs['alias']);
    }
}