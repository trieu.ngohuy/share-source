<?php
require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/buying/models/Buying.php';
require_once 'modules/buying/models/BuyingCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/user/models/User.php';
require_once 'modules/properties/models/PropertiesCategory.php';
require_once 'modules/country/models/CountryCategory.php';
require_once 'modules/hs/models/HsCategory.php';

class buying_IndexController extends Nine_Controller_Action
{
    public function postBuyingRequestAction()
    {
        if(!$this->auth->isLogin()) {
            $this->_redirect('');
        }
        $userSession = Nine_Registry::getLoggedInUser();

        $this->setLayout('front-default-category');
        $objLang = new Models_Lang();
        $objCategory = new Models_BuyingCategory;
        $objBuying = new Models_Buying();

        $data = $this->_getParam('data', false);
		
        /**
         * Get all categories
         */
        $allCats = $objCategory->buildTree($objCategory->getAll(array('sorting ASC'))->toArray());
//        echo "<pre>";print_r($allCats);die;
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        
        $user = Nine_Registry::getLoggedInUser();
        if($user != ''){
	        if($user['group_id'] != 3){
	        	$count_buying = count($objCategory->getByColumns(array('user_id=?' => $user['user_id']))->toArray());
	        	if($count_buying > 3){
	        		$this->view->lock = 1;
	        	}
	        }
        }else{
        	$this->view->lock = 1;
        }
        
        
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_category', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */

            $newCategory = $data;
            $newCategory['created_date'] = time();
            $newCategory['user_id'] = $userSession['user_id'];
            $newCategory['full_name'] = $userSession['full_name'];
            $newCategory['email'] = $userSession['email'];
            $newCategory['phone'] = $userSession['phone'];
            $newCategory['status'] = 2;
            $newCategory['alias'] = $this->makeURLSafeString($newCategory['name']);
            /**
             * Sorting
             */
            if (null == $newCategory['end_date']) {
                unset($newCategory['end_date']);
            } else {
                $tmp = explode('-', $newCategory['end_date']);
                $newCategory['end_date'] = mktime(0, 0, 0, $tmp[1], $tmp[2], $tmp[0]);
            }
            if (false == $this->checkPermission('new_category', null, '*')) {
                $newCategory['genabled']          = 0;
                $newCategory['sorting']           = 1;
            }


            if(isset($_FILES['images']) && $_FILES['images']['tmp_name'] != '') {
                $fileName = basename($_FILES['images']['name']);
                $fileTmp = $_FILES['images']['tmp_name'];
                $uploadPath = 'media/userfiles/user/' . $userSession['user_id'] . '/';
                $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                $uploadFile = $uploadDir . $fileName;
//
//                $ext_allow = array (
//                    'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX'
//                );
                $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
//                if(in_array($ext, $ext_allow)) {
                    move_uploaded_file($fileTmp, $uploadFile);
                    $newCategory['images'] = $uploadPath . $fileName;
//                }
            }

            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > @$newCategory['sorting']) {
                    $newCategory['sorting'] = 1;
                }
                if (null == $newCategory['parent_id']) {
                    $newCategory['parent_id'] = NULL;
                }
                $objCategory->increaseSorting($newCategory['sorting'], 1);

                $gid = $objCategory->insert($newCategory);

                /**
                 * Update id string
                 */
                $objCategory->update(array('gid_string'	=>	$gid), array('buying_category_gid = ?' => $gid));
                $category = @reset($objCategory->getByColumns(array('buying_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);
                /**
                 * Message
                 */
                $this->session->categoryMessage =  array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is created successfully.')
                );

                $this->_redirect('');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Category'));
        $this->view->fullPermisison = $this->checkPermission('new_category', null, '*');

        $objCategoryProperti = new Models_PropertiesCategory();
        $allCatsProperties = $objCategoryProperti->buildTree($objCategory->getAllBuyingCatWithParent(45));
        $this->view->allCatsProperties = $allCatsProperties;

        $objCatProduct     = new Models_ProductCategory();
        $allCatsProduct = $objCatProduct->buildTree($objCatProduct->getAll(array('sorting ASC'))->toArray());

        $this->view->allCatsProduct = $allCatsProduct;

        $objCategoryProperties = new Models_PropertiesCategory;

        $unitProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(45);
        $this->view->unitProperties = $unitProperties;

        $dvtProperties = $objCategoryProperties->getAllCategoriesOnparentkeyUser(53);
        $this->view->dvtProperties = $dvtProperties;


        $objCountry = new Models_CountryCategory();
        $allCountry = $objCountry->getAllCategoriesParentNull();
        $this->view->allCountry = $allCountry;
        
        
        
        
        
        
    }

    public function buyingRequestAction()
    {
        $objBuyingCategory = new Models_BuyingCategory();
        $objCountry = new Models_CountryCategory();
        $this->setLayout('front-default-category');

        $numRowPerPage = 10;
        $currentPage = $this->_getParam("page",false);
        if($currentPage == false){
            $currentPage = 1;
        }

        $condition = array();
        $condition['genabled'] = 1;
        $buyingRequest = $objBuyingCategory->getAllEnabledCategory($condition, 'buying_category_gid DESC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
        foreach ($buyingRequest as &$item) {
            $country = $objCountry->getCategoryByGid($item['country_category_gid']);
            $item['country'] = $country['name'];
            $item['country_image'] = $country['images'];
            $item['description'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $item['description'] ) ), 30 );
            $item['name'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $item['name'] ) ), 30 );
            $item['price'] = number_format(floatval($item['price']), 0, '.', ',');
            if($item['end_date'] < time()){
            	$item['h_quote'] = 2;
            }else{
            	$item['h_quote'] = 1;
            }
            unset($item);
        }


        $count = count($objBuyingCategory->getAllEnabledCategory($condition, 'country_category_gid DESC'));

        $this->setPagination($numRowPerPage, $currentPage, $count);

        $this->view->buyingRequest = $buyingRequest;
    }

    public function buyingRequestDetailAction()
    {
        $objBuyingCategory = new Models_BuyingCategory();
        $objBuying = new Models_Buying();
        $objCountry = new Models_CountryCategory();
        $objUser = new Models_User();
        $objPropertiesCategory = new Models_PropertiesCategory();

        $this->setLayout('front-default-category');

        $alias = $this->_getParam('alias', false);
	
        if(false == $alias) {
            $this->_redirect('/');
        } else {
            $buyingRequest = $objBuyingCategory->setAllLanguages(true)->getCategoryByAlias($alias);
            
            $country = $objCountry->getCategoryByGid($buyingRequest['country_category_gid']);
            $buyingRequest['price'] = number_format(floatval($buyingRequest['price']), 0, '.', ',');
        	if($buyingRequest['end_date'] < time()){
            	$buyingRequest['h_quote'] = 2;
            }else{
            	$buyingRequest['h_quote'] = 1;
            }
            $buyingRequest['end_date'] = date('d-m-Y', $buyingRequest['end_date']);
            $buyingRequest['created_date'] = date('d-m-Y', $buyingRequest['created_date']);
 			
            $conditionBuying = array('buying_category_gid' => $buyingRequest['buying_category_gid']);
            $allBuying = $objBuying->getAllBuying($conditionBuying, array('sorting ASC', 'buying_gid DESC'));
			$tmp    = array();
	        $tmp2   = false;
	        $tmpGid = @$allBuying[0]['buying_gid'];
	        foreach ($allBuying as $index=>$buying) {
	            /**
	             * Change date
	             */
	        	if($buying['enabled'] == 1){
	        		$user_active = $buying['user_id'];
	        	}
	        	$allUserId[] = $buying['user_id'];
	        	$user = $objUser->getByUserId($buying['user_id']);
                $user['category_id'] = explode("||", trim($user['category_id'],'||'));
                
                foreach ($user['category_id'] as $key=>&$item_category) {
                	if($item_category != ''){
	                    $properties = $objPropertiesCategory->getAllCategories(array('properties_category_gid' => $item_category));
	                    $user['category_id'][$key] = $properties[0]['name'];
                		
                	}
                }
                
                $user['category_id'] = implode(', ', $user['category_id']);
                $user['category_id'] = trim($user['category_id'],',');
                $buying['user'] = $user;
                $buying['created_date'] = date('d-m-Y', $buying['created_date']);
                
	            if (0 != $buying['publish_up_date']) {
	                $buying['publish_up_date'] = date('d-m-Y', $buying['publish_up_date']);
	            } else {
	                $buying['publish_up_date'] = '';
	            }
	            if (0 != $buying['publish_down_date']) {
	                $buying['publish_down_date'] = date('d-m-Y', $buying['publish_down_date']);
	            } else {
	                $buying['publish_down_date'] = '';
	            }
	            if ($tmpGid != $buying['buying_gid']) {
	                $tmp[]  = $tmp2;
	                $tmp2   = false;
	                $tmpGid = $buying['buying_gid'];
	            }
	            if (false === $tmp2) {
	                $tmp2        = $buying;
	                $tmp2['hit'] = 0;
	            }
	            $tmp2['hit']    += $buying['hit'];
	            $tmp2['langs'][]  = $buying;
	            /**
	             * Final element
	             */
	            if ($index == count($allBuying) - 1) {
	                $tmp[] = $tmp2;
	            }
	        }
	        $allBuying = $tmp;
	        
            $this->view->buyingRequest = $buyingRequest;
            $this->view->pageTitle = $buyingRequest['name'];
            $this->view->country = $country;
            $this->view->allBuying = $allBuying;

            if($this->auth->isLogin()) {
                $this->view->userLogin = Nine_Registry::getLoggedInUser();
            }

            $this->view->isLogin = $this->auth->isLogin();
        }
    }
}