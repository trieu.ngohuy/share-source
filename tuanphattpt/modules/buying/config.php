<?php
return array (
    'permissionRules' => array(
                        'see_buying' => array(//External permission
                                            'See all articles',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission interface
                        'new_buying' => array(//External permission
                                            'Create new article',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
                        'edit_buying' => array(//External permission
                                            'Edit existed articles',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
						
						'genabled_buying' => 'Genabled existed articles',
                        'delete_buying' => 'Delete existed articles',

                        'see_category_buying' => array(//External permission
                                            'See all categories',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission interface
                        'new_category_buying' => array(//External permission
                                            'Create new category',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
                        'edit_category_buying' => array(//External permission
                                            'Edit existed categories',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
                        'delete_category_buying' => 'Delete existed categories',
                    ),
  
);
