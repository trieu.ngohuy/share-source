<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{l}}Buying Request{{/l}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                {{sticker name=front_default_category}}
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <div class="product-tab">
                    <ul class="nav-tab">

                        <li>
                            <a aria-expanded="true" data-toggle="tab">{{l}}Buying Request{{/l}}</a>
                        </li>
                    </ul>
                    <div class="tab-container">
                        <div id="product-detail" class="tab-panel active">
                            <div class="sortPagiBar clearfix">
                                <span class="page-noite">Showing {{$firstItemInPage}} to {{$lastItemInPage}} of {{$count}} ({{$countAllPages}} Page)</span>
                                {{if $countAllPages > 1}}
                                <div class="bottom-pagination">
                                    <nav>
                                        <ul class="pagination">
                                            {{if $prevPage}}
                                            <li>
                                                <a href="?page={{$prevPage}}" aria-label="Prev">
                                                    <span aria-hidden="true">&laquo; Prev</span>
                                                </a>
                                            </li>
                                            {{/if}}

                                            {{foreach from=$prevPages item=item}}
                                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                            {{/foreach}}

                                            <li class="active"><a>{{$currentPage}}</a></li>

                                            {{foreach from=$nextPages item=item}}
                                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                            {{/foreach}}

                                            {{if $nextPage}}
                                            <li>
                                                <a href="?page={{$nextPage}}" aria-label="Next">
                                                    <span aria-hidden="true">Next &raquo;</span>
                                                </a>
                                            </li>
                                            {{/if}}
                                        </ul>
                                    </nav>
                                </div>
                                {{/if}}
                            </div>
                            <ul class="blog-posts">
                                {{foreach from=$buyingRequest item=item}}
                                <li class="post-item col-md-6 col-sm-6">
                                    <article class="entry">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="entry-ci" {{if $item.h_quote == 2}} style="color: #ccc  !important"{{/if}}>
                                                    <h3 class="entry-title"><img style="width: 19px;margin-right: 10px;" src="{{$BASE_URL}}{{$item.country_image}}"/><a href="{{$BASE_URL}}buying-request/{{$item.alias}}"><b  style="font-size: 16px; {{if $item.h_quote == 2}} color: #ccc  !important{{/if}}">{{$item.name}}</b></a></h3>
                                                    <h4 class="entry-title" style="font-size: 14px; margin: 10px 0px 10px 30px;">
                                                        {{if $item.description == null || $item.description == ''}}
                                                            {{l}}No Description{{/l}}
                                                        {{else}}
                                                            {{$item.description}}
                                                        {{/if}}
                                                    </h4>
                                                    <div class="entry-excerpt">
                                                        <p><b>{{l}}Qty{{/l}}:</b> {{$item.soluong}}</p>
                                                        <p><b>{{l}}Target Price{{/l}}:</b> {{$item.price}} {{$item.pname}}</p>
                                                        <p><b>{{l}}Time left{{/l}}:</b> {{$item.end_date|date_format:"%d-%m-%Y"}}</p>
                                                        <p>
                                                            <b>{{l}}File Attached{{/l}}:</b>

                                                            {{if $item.images != null}}
                                                                <a href="{{$BASE_URL}}{{$item.images}}">{{l}}Dowload File{{/l}}</a>
                                                            {{else}}
                                                                {{l}}No File Attached{{/l}}
                                                            {{/if}}
                                                        </p>
                                                    </div>
                                                    <div class="entry-more">
                                                        <a href="#" class="contact_now" >{{l}}Contact Now{{/l}}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                {{/foreach}}
                            </ul>
                            <div class="sortPagiBar clearfix col-md-12">
                                {{if $countAllPages > 1}}
                                <div class="bottom-pagination">
                                    <nav>
                                        <ul class="pagination">
                                            {{if $prevPage}}
                                            <li>
                                                <a href="?page={{$prevPage}}" aria-label="Prev">
                                                    <span aria-hidden="true">&laquo; Prev</span>
                                                </a>
                                            </li>
                                            {{/if}}

                                            {{foreach from=$prevPages item=item}}
                                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                            {{/foreach}}

                                            <li class="active"><a>{{$currentPage}}</a></li>

                                            {{foreach from=$nextPages item=item}}
                                            <li><a href="?page={{$item}}">{{$item}}</a></li>
                                            {{/foreach}}

                                            {{if $nextPage}}
                                            <li>
                                                <a href="?page={{$nextPage}}" aria-label="Next">
                                                    <span aria-hidden="true">Next &raquo;</span>
                                                </a>
                                            </li>
                                            {{/if}}
                                        </ul>
                                    </nav>
                                </div>
                                {{/if}}
                            </div>
                            <br class="cb">
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
