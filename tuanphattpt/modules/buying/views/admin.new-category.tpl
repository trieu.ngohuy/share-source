<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>  
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<script type="text/javascript">
    //<![CDATA[

    jQuery(document).ready(function (){
        {{foreach from=$allLangs item=item name=langDiv}}
        jQuery('#name{{$smarty.foreach.langDiv.iteration}}').makeSlug({
            slug: jQuery('#alias{{$smarty.foreach.langDiv.iteration}}')
        });
        {{/foreach}}
        
    });

//]]>
</script>
<div class="page-header">
	<h1>
		{{l}}Buying Category{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}New Buying Category{{/l}}
		</small>
	</h1>
</div>

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
			<div class="row">
				<div class="col-xs-12">
						{{if $errors|@count > 0}}
	                        <div class="alert alert-danger">
	                        	<button class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
									{{if $errors.main}}
	                                   </strong> {{$errors.main}}
	                                {{else}}
	                                   {{l}}Please check following information again{{/l}}
	                                {{/if}} 
							</div>
	                	{{/if}}
	                        
	                        
	                	<div class="col-sm-12 widget-container-col ui-sortable">
	                	
										<div class="tabbable tabs-left">
										
											<ul class="nav nav-tabs" id="myTab2">
												{{if $fullPermisison}}
													<li class="active">
														<a href="#tab0" data-toggle="tab" aria-expanded="true">
															<i class="pink ace-icon fa fa-home bigger-110"></i>
															{{l}}Basic{{/l}}
														</a>
													</li>
												{{/if}}
												
												{{foreach from=$allLangs item=item index=index name=langTab}}
													<li>
														<a href="#tab{{$item.lang_id}}" data-toggle="tab" aria-expanded="true">
															<image style="vertical-align:middle;" src="{{$BASE_URL}}{{$item.lang_image}}"> {{$item.name}}
														</a>
													</li>
												{{/foreach}}
											</ul>
													
											<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
												<div class="tab-content">
												
													<div id="tab0" class="tab-pane active">
									                	<div class="col-md-6 panel-footer">
															<div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Title{{/l}}</label>
																<div class="col-md-10">
																	<input type="text" class="form-control" name="data[name]"  value="{{$data.name}}" >
																</div>
															</div>
															<div class="form-group has-info">
																	<label class="control-label col-md-2">{{l}}Product Category{{/l}}</label>
																	<div class="col-md-10">
																			<select name="data[product_category_gid]" class="chosen-select form-control" id="form-field-select-3">
																				<option value="">{{l}}No parent{{/l}}</option>
											                                    {{foreach from=$allCatsProduct item=item}}
											                                        <option value="{{$item.product_category_gid}}" {{if $data.product_category_gid == $item.product_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
											                                    {{/foreach}}
																			</select>
																	</div>
															</div>
															
															<div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Price{{/l}}</label>
																<div class="col-md-5">
																		<input type="text" class="form-control" name="data[price]"  value="{{$data.price}}" >
																</div>
																<div class="col-md-5">
																		<select name="data[current]" class="chosen-select form-control">
																			{{foreach from=$unitProperties item=item}}
																				{{if $item.parent_id != ''}}
										                                        	<option value="{{$item.properties_category_gid}}" {{if $data.current == $item.properties_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
										                                    	{{/if}}
										                                    {{/foreach}}
																		</select>
																</div>
															</div>
															
															<div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Số Lượng{{/l}}</label>
																<div class="col-md-5">
																		<input type="text" class="form-control" name="data[so_luong]"  value="{{$data.so_luong}}" >
																</div>
																<div class="col-md-5">
																		<select name="data[so_luong_gid]" class="chosen-select form-control">
																			{{foreach from=$dvtProperties item=item}}
																				{{if $item.parent_id != ''}}
										                                        	<option value="{{$item.properties_category_gid}}" {{if $item.properties_category_gid == $data.so_luong }} selected="selected"{{/if}}>{{$item.name}}</option>
										                                    	{{/if}}
										                                    {{/foreach}}
																		</select>
																</div>
															</div>
															
															<div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Ngày Hết Hạn{{/l}}</label>
																<div class="col-md-10">
																		<input type="text" class="form-control date-picker" id="id-date-picker-1" type="text" data-date-format="dd-mm-yyyy" name="data[end_date]"  value="{{$data.end_date}}" >
																</div>
															</div>

															<br class="cb">
														</div>
														<div class="col-md-6 panel-footer">
															<div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Nơi Giao Hàng{{/l}}</label>
																<div class="col-md-5">
																		<input type="text" class="form-control" name="data[add]"  value="{{$data.add}}" >
																</div>
																<div class="col-md-5">
																		<select name="data[country_category_gid]" class="chosen-select form-control">
																			{{foreach from=$allCountry item=item}}
										                                        	<option value="{{$item.country_category_gid}}" {{if $data.country_category_gid == $item.country_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
										                                    {{/foreach}}
																		</select>
																</div>
															</div>
															
															<div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Điều Kiện Thanh Toán / Vận Chuyển{{/l}}</label>
																<div class="col-md-10">
																		<input type="text" class="form-control" name="data[buy]"  value="{{$data.buy}}" >
																</div>
															</div>

															<div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Customer{{/l}}</label>
																<div class="col-md-10">
																	<select name="data[user_id]" class="chosen-select form-control" id="form-field-select-3">
																		{{foreach from=$allUsers item=item}}
																		<option value="{{$item.user_id}}" {{if $data.user_id == $item.user_id}}selected="selected"{{/if}}>{{$item.full_name}} - {{$item.name_company}}</option>
																		{{/foreach}}
																	</select>
																</div>
															</div>

															<div class="form-group has-info">
																<label class="control-label col-md-2">{{l}}Attach File{{/l}}</label>
																<div class="col-md-10">
																	<input type="file" id="id-input-file-2" class="form-control" name="images" />
																</div>
															</div>
															
															<br class="cb">
														</div>
														<br class="cb">
														<div class="clearfix form-actions">
															<div class="col-md-offset-3 col-md-9">
																<button class="btn btn-info" type="submit">
																	<i class="ace-icon fa fa-check bigger-110"></i>
																	{{l}}Save{{/l}}
																</button>
															</div>
														</div>
														
													</div>
													
													{{foreach from=$allLangs item=item name=langDiv}}
														<div class="tab-pane" id="tab{{$item.lang_id}}">
																<div class="form-group has-info">
																	<label class="control-label col-md-2">{{l}}Name{{/l}}</label>
																	<div class="col-md-10">
																			<input id="name{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][name]"  value="{{$data[$item.lang_id].name}}" >
																	</div>
																</div>
																
																<div class="form-group has-info">
																	<label class="control-label col-md-2">{{l}}Alias{{/l}}</label>
																	<div class="col-md-10">
																			<input id="alias{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][alias]"  value="{{$data[$item.lang_id].alias}}" >.html
																	</div>
																</div>
																
								                                <div class="form-group has-info">
																	<label class="control-label col-md-2">{{l}}Content{{/l}}</label>
																	<div class="col-md-10">
																			<textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][description]" rows="20" cols="90">{{$data[$item.lang_id].description}}</textarea>
																	</div>
																</div>
								                                
								                                <div class="clearfix form-actions">
																	<div class="col-md-offset-3 col-md-9">
																		<button class="btn btn-info" type="submit">
																			<i class="ace-icon fa fa-check bigger-110"></i>
																			{{l}}Save{{/l}}
																		</button>
																	</div>
																</div> 
														</div>
													
													{{/foreach}}
													
												</div>
											
											</form>
											
										</div>
										
									</div>
					
					
					
				</div><!-- /.span -->
				
			</div><!-- /.row -->
		
		
		
		</div>
		
		
	</div>
<script type="text/javascript">
	jQuery(function($) {
		$('#id-input-file-1 , #id-input-file-2').ace_file_input({
			no_file:'No File ...',
			btn_choose:'Choose',
			btn_change:'Change',
			droppable:false,
			onchange:null,
			thumbnail:false //| true | large
			//whitelist:'gif|png|jpg|jpeg'
			//blacklist:'exe|php'
			//onchange:''
			//
		});
	});
</script>

			