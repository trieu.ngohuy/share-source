


<div class="page-header">
    <h1>
        {{l}}Category Buying{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}List All Category Buying{{/l}}
        </small>
    </h1>
</div>
<a class="btn btn-lg btn-success" href="?export=true&page=1">
    <i class="ace-icon fa fa-file-excel-o"></i>
    Export
</a>
<br class="cb">
<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">
                {{if $allCategories|@count <= 0}}
                <div class="alert alert-info">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{l}}No category with above conditions.{{/l}}
                </div>
                {{/if}}

                {{if $categoryMessage|@count > 0 && $categoryMessage.success == true}}
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$categoryMessage.message}}.
                </div>
                {{/if}}

                {{if $categoryMessage|@count > 0 && $categoryMessage.success == false}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$categoryMessage.message}}.
                </div>
                {{/if}}


                <table id="simple-table" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <form id="top-search" name="search" method="post" action="">
                            <th class="center">
                                <button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
                            </th>
                            <th>{{l}}Full Name{{/l}}
                                <input  type="text" name="condition[full_name]" value="{{$condition.full_name}}"" placeholder="{{l}}Find Name{{/l}}" class="form-control" />
                            </th>
                            <th>{{l}}Company{{/l}}</th>
                            <th>{{l}}Price{{/l}}</th>
                            <th>{{l}}Currency{{/l}}</th>
                            <th>{{l}}Created{{/l}}</th>
                            <th>{{l}}Ngày Hết Hạn{{/l}}</th>
                            <th class="center">
                                {{l}}Status{{/l}}
                                <select name="condition[status]" class="form-control" onchange="this.form.submit();">
                                    <option value="" >{{l}}Tất Cả{{/l}}</option>
                                    <option value="1" {{if $condition.status == "1"}}selected="selected"{{/if}}>{{l}}Đang Chờ Báo Giá{{/l}}</option>
                                    <option value="2" {{if $condition.status == "2"}}selected="selected"{{/if}}>{{l}}Đang Báo Giá{{/l}}</option>
                                    <option value="3" {{if $condition.status == "3"}}selected="selected"{{/if}}>{{l}}Hết Hạn{{/l}}</option>
                                    <option value="4" {{if $condition.status == "4"}}selected="selected"{{/if}}>{{l}}Đã Đóng{{/l}}</option>
                                </select>
                            </th>
                            <th colspan="4">{{l}}Detail{{/l}}
                                <input  type="text" name="condition[keyword]" id="username" value="{{$condition.keyword}}"" placeholder="{{l}}Find Name{{/l}}" class="form-control" />
                            </th>
                        </form>
                    </tr>
                    </thead>

                    <tbody>
                    {{if $allCategories|@count > 0}}
                    {{foreach from=$allCategories item=item name=category key=key}}
                    <tr>
                        <td class="center">{{$key+1}}</td>
                        <td>{{$item.full_name}}</td>
                        <td>{{$item.pcompany_name}}</td>
                        <td>{{$item.price}}</td>
                        <td>{{$item.pname}}</td>
                        <td>{{$item.created_date}}</td>
                        <td>{{$item.end_date}}</td>
                        <td class="center">
                            <select name="data[{{$item.buying_category_gid}}]" class="form-control">
                                <option value="1" {{if $item.status == "1"}}selected="selected"{{/if}}>{{l}}Đang Chờ Báo Giá{{/l}}</option>
                                <option value="2" {{if $item.status == "2"}}selected="selected"{{/if}}>{{l}}Đang Báo Giá{{/l}}</option>
                                <option value="3" {{if $item.status == "3"}}selected="selected"{{/if}}>{{l}}Hết Hạn{{/l}}</option>
                                <option value="4" {{if $item.status == "4"}}selected="selected"{{/if}}>{{l}}Đã Chốt{{/l}}</option>
                            </select>
                        </td>

                        <td colspan="5" style="padding:0px;">
                            <!-- All languages -->
                            <table style="border-collapse: separate;margin-bottom: 0px;" class="table table-bordered table-striped">
                                <tbody id="table{{$item.buying_category_id}}">
                                <tr>
                                    <td width="75%">--</td>
                                    <td width="7%">
                                        {{if $item.genabled == '1'}}
                                            <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png">
                                        {{else}}
                                            <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png">
                                        {{/if}}
                                    </td>
                                    <td width="10%"  class="center">
                                        {{p name=edit_category_buying module=buying expandId=$langId}}
                                        <span class="tooltip-area">
                                            <a href="{{$APP_BASE_URL}}estore/edit-buying-request/{{$username}}/{{$item.buying_category_gid}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                        </span>
                                        {{/p}}

                                    </td>
                                    <td width=8%">
                                        {{$item.buying_category_gid}}
                                    </td>
                                </tr>

                                {{foreach from=$item.langs item=item2}}
                                <tr>
                                    <td><image style="vertical-align:middle;" src="{{$BASE_URL}}{{$item2.lang_image}}"> {{$item2.name}}</td>
                                    <td>
                                        {{assign var='langId' value=$item2.lang_id}}
                                        {{if $item.genabled == '1'}}
                                        <!-- FULL PERMISSION -->
                                        <!-- O cho nao co cai p này chính là permission (phân quyền đó ku) -->
                                            {{if $item2.enabled == '1' }}
                                                <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png">
                                            {{else}}
                                                <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png">
                                            {{/if}}
                                        {{else}}
                                            <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png">
                                        {{/if}}
                                    </td>

                                    <td class="center">

                                        {{p name=edit_category_buying module=buying expandId=$langId}}
                                        <span class="tooltip-area">
                                            <a href="{{$APP_BASE_URL}}estore/edit-buying-request/{{$username}}/{{$item.buying_category_gid}}/{{$item2.lang_id}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                        </span>
                                        {{/p}}
                                        {{np name=edit_category_buying module=buying expandId=$langId}}
                                        --
                                        {{/np}}
                                    </td>
                                    <td>
                                        {{$item2.buying_category_id}}
                                    </td>
                                </tr>
                                {{/foreach}}

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    {{/foreach}}
                    {{/if}}
                    </tbody>
                </table>


            </div><!-- /.span -->
        </div><!-- /.row -->


        <div class="col-lg-12">
            <div class="form-group col-lg-2">
                <label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
                <div class="col-lg-12">
                    <form class="search" name="search" method="post" action="">
                        <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
                            <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
                            <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
                            <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
                            <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
                            <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
                        </select>
                    </form>
                </div>
            </div>
            {{if $countAllPages > 1}}
            <div class="col-lg-6 pagination">
                {{if $first}}
                <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
                {{/if}}
                {{if $prevPage}}
                <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
                {{/if}}

                {{foreach from=$prevPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>

                {{foreach from=$nextPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                {{if $nextPage}}
                <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
                {{/if}}
                {{if $last}}
                <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
                {{/if}}

            </div>
            {{/if}}
        </div>
    </div>
</div>
</div>
