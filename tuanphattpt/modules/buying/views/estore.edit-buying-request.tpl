<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>


<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">
                {{if $errors|@count > 0}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{if $errors.main}}
                    </strong> {{$errors.main}}
                    {{else}}
                    {{l}}Please check following information again{{/l}}
                    {{/if}}
                </div>
                {{/if}}


                <div class="col-sm-12 widget-container-col ui-sortable">
                    {{if $quote == true}}
                        <div class="page-header">
                            <h1>
                                {{l}}Buying Request Quote{{/l}}
                            </h1>
                        </div>
                        <table id="simple-table" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="col-sm-1">{{l}}Estore{{/l}}</th>
                                <th class="col-sm-1">{{l}}Status{{/l}}</th>
                                <th class="col-sm-1">{{l}}Quote Date{{/l}}</th>
                                <th  class="col-sm-5">{{l}}Action{{/l}}</th>
                            </tr>
                            </thead>

                            <tbody>
                                {{foreach from=$allBuying item=item name=buying key=key}}
                                    <tr>
                                        <td width="40%">
                                            <a target="_blank" href="{{$APP_BASE_URL}}user/admin/edit-user/id/{{$item.user_id}}" >{{$item.name_company}}</a>
                                        </td>
                                        <td width="20%">
                                            {{if $item.enabled == 1}}
                                                {{l}}Đã báo giá{{/l}}
                                            {{elseif $item.enabled == 2}}
                                                {{l}}Đã được chọn{{/l}}
                                            {{/if}}
                                        </td>
                                        <td width="20%">{{$item.created_date}}</td>

                                        <td width="20%">
                                            <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#buying_{{$item.buying_gid}}">
                                                {{l}}Quotation Detail{{/l}}
                                            </button>
                                            <div class="modal fade" id="buying_{{$item.buying_gid}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Quotation Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table class="table table-striped">
                                                                <tr>
                                                                    <td>{{l}}Price{{/l}}</td>
                                                                    <td>
                                                                        {{if $item.price_sale != null}}
                                                                        {{$item.price_sale|number_format:0:".":","}} {{$item.pname}}
                                                                        {{else}}
                                                                        {{l}}No Price{{/l}}
                                                                        {{/if}}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>{{l}}Quantity{{/l}}</td>
                                                                    <td>{{$item.quantity}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>{{l}}Unit{{/l}}</td>
                                                                    <td>{{$item.pname2}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Quotation Content</td>
                                                                    <td>{{$item.full_text}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Files Attached</td>
                                                                    {{if $item.file_attached != ''}}
                                                                    <td><a href="{{$BASE_URL}}{{$item.file_attached}}">Download Files</a></td>
                                                                    {{else}}
                                                                    {{l}}No attach file{{/l}}
                                                                    {{/if}}
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                {{/foreach}}
                            </tbody>
                        </table>
                    {{/if}}
                    <div class="page-header">
                        <h1>
                            {{l}}Buying Request{{/l}}
                            <small>
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                {{l}}Edit Buying Request{{/l}}
                            </small>
                        </h1>
                    </div>
                    <div class="tabbable tabs-left">
                        </ul>

                        <div class="tab-content" style="margin-bottom: 50px">

                            <div id="tab0" class="tab-pane active">
                                <div class="col-md-12 panel-footer">
                                    <div class="col-md-6 panel-footer">
                                        <div class="col-md-12">
                                            <label class="control-label col-md-4 text-right text-info">{{l}}Product Category{{/l}} : </label>
                                            <label class="control-label col-md-8">
                                                {{$catProduct}}
                                            </label>
                                        </div>


                                        <div class="col-md-12">
                                            <label class="control-label col-md-4 text-right text-info">{{l}}Price{{/l}} : </label>
                                            <label class="control-label col-md-8">
                                                {{$data.price|number_format:0:" ":","}} {{$unitProperties}}
                                            </label>
                                        </div>

                                        <div class="col-md-12">
                                            <label class="control-label col-md-4 text-right text-info">{{l}}Số Lượng{{/l}} : </label>
                                            <label class="control-label col-md-8">
                                                {{$data.so_luong}} {{$dvtProperties}}
                                            </label>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="control-label col-md-4 text-right text-info">{{l}}Attach File{{/l}} : </label>
                                            <label class="control-label col-md-8">
                                                {{if $data.images != ''}}
                                                <a href="{{$BASE_URL}}{{$data.images}}">{{$fileName}}</a>
                                                {{/if}}
                                            </label>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="control-label col-md-4 text-right text-info">{{l}}Description{{/l}} : </label>
                                            <label class="control-label col-md-8">
                                                {{$data.description}}
                                            </label>
                                        </div>
                                        <br class="cb">
                                    </div>
                                    <div class="col-md-6 panel-footer">
                                        <div class="col-md-12">
                                            <label class="control-label col-md-3 text-right text-info">{{l}}Ngày Hết Hạn{{/l}} : </label>
                                            <label class="control-label col-md-3">
                                                {{$data.end_date}}
                                            </label>
                                            <label class="control-label col-md-3 text-right text-info">{{l}}Tình trạng{{/l}} : </label>
                                            <label class="control-label col-md-3">
                                                {{if $data.status == "1"}}
                                                    {{l}}Đang Chờ Báo Giá{{/l}}
                                                {{elseif $data.status == "2"}}
                                                    {{l}}Đang Báo Giá{{/l}}
                                                {{elseif $data.status == "3"}}
                                                    {{l}}Hết Hạn{{/l}}
                                                {{elseif $data.status == "4"}}
                                                    {{l}}Đã Chốt{{/l}}
                                                {{/if}}
                                            </label>
                                        </div>

                                        <div class="col-md-12">
                                            <label class="control-label col-md-3 text-right text-info">{{l}}Nơi Giao Hàng{{/l}} : </label>
                                            <label class="control-label col-md-3">
                                                {{$data.add}}
                                            </label>
                                            <label class="control-label col-md-3 text-right text-info">{{l}}Quốc gia{{/l}} : </label>
                                            <label class="control-label col-md-3">
                                                {{$allCountry}}
                                           </label>
                                        </div>

                                        <div class="col-md-12">
                                            <label class="control-label col-md-3 text-right text-info">{{l}}Thanh Toán / Vận Chuyển{{/l}} : </label>
                                            <label class="control-label col-md-9">
                                                {{$data.buy}}
                                            </label>
                                        </div>

                                        <div class="col-md-12">
                                            <label class="control-label col-md-3 text-right text-info">{{l}}Customer{{/l}} : </label>
                                            <label class="control-label col-md-9">
                                                {{$allUsers}}
                                            </label>
                                        </div>
                                        <br class="cb">
                                    </div>
                                </div>
                                <br class="cb">
                            </div>
                        </div>
                    </div>

                </div>



            </div><!-- /.span -->

        </div><!-- /.row -->



    </div>


</div>
{{if $quote == false}}
<div class="row">
    <div class="col-xs-12">
        <div class="col-sm-12 widget-container-col ui-sortable">
            <form id="quote-form" class="form-horizontal" action="{{$BASE_URL}}estore/quote-buying-request/{{$alias}}" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label  class="col-sm-2 control-label">{{l}}Price{{/l}}</label>
                    <div class="col-sm-5">
                        <input type="text" name="data[price_sale]" class="form-control"/>
                    </div>
                    <div class="col-sm-5">
                        <select name="data[tiente]" class="chosen-select form-control" id="form-field-select-3">
                            {{foreach from=$allUnitProperties item=item}}
                            {{if $item.parent_id != ''}}
                            <option value="{{$item.properties_category_gid}}">{{$item.name}}</option>
                            {{/if}}
                            {{/foreach}}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{l}}Quantity{{/l}}</label>
                    <div class="col-sm-5">
                        <input type="text" name="data[quantity]" class="form-control"/>
                    </div>
                    <div class="col-sm-5">
                        <select name="data[cachtinh]" class="chosen-select form-control" id="form-field-select-3">
                            {{foreach from=$allDvtProperties item=item}}
                            {{if $item.parent_id != ''}}
                                <option value="{{$item.properties_category_gid}}">{{$item.name}}</option>
                            {{/if}}
                            {{/foreach}}
                        </select>
                    </div>
                </div>
                <div class="form-group has-info">
                    <label class="col-sm-2 control-label">{{l}}Content quote{{/l}}</label>
                    <div class="col-sm-10">
                        <textarea style="float:left;" class="text-input textarea ckeditor" name="data[full_text]" rows="10" cols="40"></textarea>
                    </div>
                </div>
                <div class="form-group has-info">
                    <label class="control-label col-md-2">{{l}}Attach File{{/l}}</label>
                    <div class="col-md-10">
                        <input type="file" class="form-control" id="id-input-file-1" id name="files" />
                    </div>
                </div>
                <input type="hidden" name="data[buying_category_gid]" value="{{$data.buying_category_gid}}">
                <div class="form-group">
                    <label class="control-label col-md-2"></label>
                    <div class="col-md-10">
                        <button type="submit" id="submit" class="btn btn-primary">
                            {{l}}Quote{{/l}}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
{{/if}}

<script type="text/javascript">
    jQuery(function($) {
        $('#id-input-file-1 , #id-input-file-2').ace_file_input({
            no_file:'{{l}}Chưa có tệp ...{{/l}}',
            btn_choose:'{{l}}Chọn tệp{{/l}}',
            btn_change:'{{l}}Thay đổi tệp{{/l}}',
            droppable:false,
            onchange:null,
            thumbnail:false //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            //blacklist:'exe|php'
            //onchange:''
            //
        });
    });
</script>


