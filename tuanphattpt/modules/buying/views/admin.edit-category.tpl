<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>  
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<script type="text/javascript">
    //<![CDATA[

    jQuery(document).ready(function (){
        {{foreach from=$allLangs item=item name=langDiv}}
        jQuery('#name{{$smarty.foreach.langDiv.iteration}}').makeSlug({
            slug: jQuery('#alias{{$smarty.foreach.langDiv.iteration}}')
        });
        {{/foreach}}
        
    });

//]]>
</script>


<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
			<div class="row">
				<div class="col-xs-12">
						{{if $errors|@count > 0}}
	                        <div class="alert alert-danger">
	                        	<button class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
									{{if $errors.main}}
	                                   </strong> {{$errors.main}}
	                                {{else}}
	                                   {{l}}Please check following information again{{/l}}
	                                {{/if}} 
							</div>
	                	{{/if}}
	                     
	                        
	                	<div class="col-sm-12 widget-container-col ui-sortable">
							{{if $selectedQuote == true}}
								<div class="page-header" >
									<h1 style="color: red">
										{{l}}Estore Selected{{/l}}
									</h1>
								</div>
								<table id="simple-table" class="table table-striped table-bordered table-hover">
									<thead>
									<tr>
										<th class="col-sm-1">{{l}}Estore{{/l}}</th>
										<th class="col-sm-1">{{l}}Quote Date{{/l}}</th>
										<th  class="col-sm-5">{{l}}Action{{/l}}</th>
									</tr>
									</thead>

									<tbody>
									{{if $allBuying|@count > 0}}
										{{foreach from=$allBuying item=item name=buying key=key}}
											{{if $item.enabled == 2}}
											<tr>
												<td width="50%">
													<a target="_blank" href="{{$APP_BASE_URL}}user/admin/edit-user/id/{{$item.user_id}}" >{{$item.name_company}}</a>
												</td>
												<td width="25%">{{$item.created_date}}</td>

												<td width="25%">
													<button class="btn btn-sm btn-info" data-toggle="modal" data-target="#buying_{{$item.buying_gid}}">
														{{l}}Quotation Detail{{/l}}
													</button>
													<div class="modal fade" id="buying_{{$item.buying_gid}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
																	<h4 class="modal-title" id="myModalLabel">Quotation Detail</h4>
																</div>
																<div class="modal-body">
																	<table class="table table-striped">
																		<tr>
																			<td>{{l}}Price{{/l}}</td>
																			<td>
																				{{if $item.price_sale != null}}
																				{{$item.price_sale|number_format:0:".":","}} {{$item.tiente}}
																				{{else}}
																				{{l}}No Price{{/l}}
																				{{/if}}
																			</td>
																		</tr>
																		<tr>
																			<td>{{l}}Quantity{{/l}}</td>
																			<td>{{$item.quantity}}</td>
																		</tr>
																		<tr>
																			<td>{{l}}Unit{{/l}}</td>
																			<td>{{$item.cachtinh}}</td>
																		</tr>
																		<tr>
																			<td>Quotation Content</td>
																			<td>{{$item.full_text}}</td>
																		</tr>
																		<tr>
																			<td>Files Attached</td>
																			{{if $item.file_attached != ''}}
																				<td><a href="{{$BASE_URL}}{{$item.file_attached}}">Download Files</a></td>
																			{{else}}
																				{{l}}No attach file{{/l}}
																			{{/if}}
																		</tr>
																	</table>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
											{{/if}}
										{{/foreach}}
									{{/if}}
									</tbody>
								</table>
							{{/if}}
							<div class="page-header">
								<h1>
									{{l}}Buying Request Quote{{/l}}
								</h1>
							</div>
							<table id="simple-table" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th class="col-sm-1">{{l}}Estore{{/l}}</th>
										<th class="col-sm-1">{{l}}Quote Date{{/l}}</th>
										<th  class="col-sm-5">{{l}}Action{{/l}}</th>
									</tr>
								</thead>

								<tbody>
									{{if $allBuying|@count > 0}}
										{{foreach from=$allBuying item=item name=buying key=key}}
											{{if $item.enabled == 1}}
											<tr>
												<td width="50%">
													<a target="_blank" href="{{$APP_BASE_URL}}user/admin/edit-user/id/{{$item.user_id}}" >{{$item.name_company}}</a>
												</td>
												<td width="25%">{{$item.created_date}}</td>

												<td width="25%">
													{{if $selectedQuote == false}}
														<button class="btn btn-sm btn-info" onclick="window.location.href='{{$APP_BASE_URL}}buying/admin/agree-quote/category_gid/{{$gid}}/gid/{{$item.buying_gid}}'">
															{{l}}Agree quote{{/l}}
														</button>
													{{/if}}
													<button class="btn btn-sm btn-info" data-toggle="modal" data-target="#buying_{{$item.buying_gid}}">
														{{l}}Quotation Detail{{/l}}
													</button>
													<div class="modal fade" id="buying_{{$item.buying_gid}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
																	<h4 class="modal-title" id="myModalLabel">Quotation Detail</h4>
																</div>
																<div class="modal-body">
																	<table class="table table-striped">
																		<tr>
																			<td>{{l}}Price{{/l}}</td>
																			<td>
																				{{if $item.price_sale != null}}
																					{{$item.price_sale|number_format:0:".":","}} {{$item.tiente}}
																				{{else}}
																					{{l}}No Price{{/l}}
																				{{/if}}
																			</td>
																		</tr>
																		<tr>
																			<td>{{l}}Quantity{{/l}}</td>
																			<td>{{$item.quantity}}</td>
																		</tr>
																		<tr>
																			<td>{{l}}Unit{{/l}}</td>
																			<td>{{$item.cachtinh}}</td>
																		</tr>
																		<tr>
																			<td>Quotation Content</td>
																			<td>{{$item.full_text}}</td>
																		</tr>
																		<tr>
																			<td>Files Attached</td>
																			{{if $item.file_attached != ''}}
																				<td><a href="{{$BASE_URL}}{{$item.file_attached}}">Download Files</a></td>
																			{{else}}
																				{{l}}No attach file{{/l}}
																			{{/if}}
																		</tr>
																	</table>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
											{{/if}}
										{{/foreach}}
									{{/if}}
								</tbody>
							</table>

							<div class="page-header">
								<h1>
									{{l}}Buying Request{{/l}}
									<small>
										<i class="ace-icon fa fa-angle-double-right"></i>
										{{l}}Edit Buying Request{{/l}}
									</small>
								</h1>
							</div>
							<div class="tabbable tabs-left">
								<ul class="nav nav-tabs" id="myTab2">
									{{if $fullPermisison}}
										<li class="active">
											<a href="#tab0" data-toggle="tab" aria-expanded="true">
												<i class="pink ace-icon fa fa-home bigger-110"></i>
												{{l}}Basic{{/l}}
											</a>
										</li>
									{{/if}}

									{{foreach from=$allLangs item=item index=index name=langTab}}
										<li>
											<a href="#tab{{$item.lang_id}}" data-toggle="tab" aria-expanded="true">
												<image style="vertical-align:middle;" src="{{$BASE_URL}}{{$item.lang_image}}"> {{$item.name}}
											</a>
										</li>
									{{/foreach}}
								</ul>

								<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
									<div class="tab-content" style="margin-bottom: 50px">

										<div id="tab0" class="tab-pane active">
											<div class="col-md-12 panel-footer">
												<div class="col-md-6 panel-footer">
													<div class="form-group has-info">
														<label class="control-label col-md-2">{{l}}Title{{/l}}</label>
														<div class="col-md-10">
															<input type="text" class="form-control" name="data[name]"  value="{{$data.name}}" >
														</div>
													</div>
													<div class="form-group has-info">
															<label class="control-label col-md-2">{{l}}Product Category{{/l}}</label>
															<div class="col-md-10">
																	<select name="data[product_category_gid]" class="chosen-select form-control" id="form-field-select-3">
																		<option value="">{{l}}No parent{{/l}}</option>
																		{{foreach from=$allCatsProduct item=item}}
																			<option value="{{$item.product_category_gid}}" {{if $data.product_category_gid == $item.product_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
																		{{/foreach}}
																	</select>
															</div>
													</div>

													<div class="form-group has-info">
														<label class="control-label col-md-2">{{l}}Price{{/l}}</label>
														<div class="col-md-5">
																<input type="text" class="form-control" name="data[price]"  value="{{$data.price}}" >
														</div>
														<div class="col-md-5">
																<select name="data[current]" class="chosen-select form-control">
																	{{foreach from=$unitProperties item=item}}
																		{{if $item.parent_id != ''}}
																			<option value="{{$item.properties_category_gid}}" {{if $data.current == $item.properties_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
																		{{/if}}
																	{{/foreach}}
																</select>
														</div>
													</div>

													<div class="form-group has-info">
														<label class="control-label col-md-2">{{l}}Số Lượng{{/l}}</label>
														<div class="col-md-5">
																<input type="text" class="form-control" name="data[so_luong]"  value="{{$data.so_luong}}" >
														</div>
														<div class="col-md-5">
																<select name="data[so_luong_gid]" class="chosen-select form-control">
																	{{foreach from=$dvtProperties item=item}}
																		{{if $item.parent_id != ''}}
																			<option value="{{$item.properties_category_gid}}" {{if $item.properties_category_gid == $data.so_luong }} selected="selected"{{/if}}>{{$item.name}}</option>
																		{{/if}}
																	{{/foreach}}
																</select>
														</div>
													</div>
													<div class="form-group has-info">
														<label class="control-label col-md-2">{{l}}Ngày Hết Hạn{{/l}}</label>
														<div class="col-md-5">
															<input type="text" class="form-control date-picker" id="id-date-picker-1" type="text" data-date-format="dd-mm-yyyy" name="data[end_date]"  value="{{$data.end_date}}" >
														</div>
														<div class="col-md-5">
															<select name="data[status]" class="chosen-select form-control">
																<option value="2" {{if $data.status == "2"}}selected="selected"{{/if}}>{{l}}Đang Báo Giá{{/l}}</option>
																<option value="3" {{if $data.status == "3"}}selected="selected"{{/if}}>{{l}}Hết Hạn{{/l}}</option>
																<option value="4" {{if $data.status == "4"}}selected="selected"{{/if}}>{{l}}Đã Chốt{{/l}}</option>
															</select>
														</div>
													</div>
													<br class="cb">
												</div>
												<div class="col-md-6 panel-footer">
													<div class="form-group has-info">
														<label class="control-label col-md-2">{{l}}Nơi Giao Hàng{{/l}}</label>
														<div class="col-md-5">
																<input type="text" class="form-control" name="data[add]"  value="{{$data.add}}" >
														</div>
														<div class="col-md-5">
																<select name="data[country_category_gid]" class="chosen-select form-control">
																	{{foreach from=$allCountry item=item}}
																			<option value="{{$item.country_category_gid}}" {{if $data.country_category_gid == $item.country_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
																	{{/foreach}}
																</select>
														</div>
													</div>

													<div class="form-group has-info">
														<label class="control-label col-md-2">{{l}}Điều Kiện Thanh Toán / Vận Chuyển{{/l}}</label>
														<div class="col-md-10">
																<input type="text" class="form-control" name="data[buy]"  value="{{$data.buy}}" >
														</div>
													</div>

													<div class="form-group has-info">
														<label class="control-label col-md-2">{{l}}Customer{{/l}}</label>
														<div class="col-md-10">
															<select name="data[user_id]" class="chosen-select form-control" id="form-field-select-3">
																{{foreach from=$allUsers item=item}}
																<option value="{{$item.user_id}}" {{if $data.user_id == $item.user_id}}selected="selected"{{/if}}>{{$item.full_name}}</option>
																{{/foreach}}
															</select>
														</div>
													</div>
													<div class="form-group has-info">
														<label class="control-label col-md-2">{{l}}Attach File{{/l}}</label>
														<div class="col-md-10">
															<input type="file" id="id-input-file-2" class="form-control" name="images" />
															{{if $data.images != ''}}
																<a href="{{$BASE_URL}}{{$data.images}}">{{$fileName}}</a>
															{{/if}}
														</div>
													</div>

													<br class="cb">
												</div>
											</div>
											<br class="cb">

											<div class="clearfix form-actions">
												<div class="col-md-offset-3 col-md-9">
													<button class="btn btn-info" type="submit">
														<i class="ace-icon fa fa-check bigger-110"></i>
														{{l}}Save{{/l}}
													</button>
												</div>
											</div>

										</div>

										{{foreach from=$allLangs item=item name=langDiv}}
											<div class="tab-pane" id="tab{{$item.lang_id}}">
												<div class="form-group has-info">
													<label class="control-label col-md-2">{{l}}Name{{/l}}</label>
													<div class="col-md-10">
															<input id="name{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][name]"  value="{{$data[$item.lang_id].name}}" >
													</div>
												</div>

												<div class="form-group has-info">
													<label class="control-label col-md-2">{{l}}Alias{{/l}}</label>
													<div class="col-md-10">
															<input id="alias{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][alias]"  value="{{$data[$item.lang_id].alias}}" >.html
													</div>
												</div>

												<div class="form-group has-info">
													<label class="control-label col-md-2">{{l}}Content{{/l}}</label>
													<div class="col-md-10">
															<textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][description]" rows="20" cols="90">{{$data[$item.lang_id].description}}</textarea>
													</div>
												</div>

												<div class="clearfix form-actions">
													<div class="col-md-offset-3 col-md-9">
														<button class="btn btn-info" type="submit">
															<i class="ace-icon fa fa-check bigger-110"></i>
															{{l}}Save{{/l}}
														</button>
													</div>
												</div>
											</div>

										{{/foreach}}

									</div>
								</form>
							</div>

						</div>
					
					
					
				</div><!-- /.span -->
				
			</div><!-- /.row -->
		
		
		
		</div>


	</div>
<script type="text/javascript">
	jQuery(function($) {
		$('#id-input-file-1 , #id-input-file-2').ace_file_input({
			no_file:'No File ...',
			btn_choose:'Choose',
			btn_change:'Change',
			droppable:false,
			onchange:null,
			thumbnail:false //| true | large
			//whitelist:'gif|png|jpg|jpeg'
			//blacklist:'exe|php'
			//onchange:''
			//
		});
	});
</script>
