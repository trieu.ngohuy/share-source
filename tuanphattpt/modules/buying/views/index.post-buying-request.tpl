
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script> 

<script type="text/javascript">

    jQuery(document).ready(function (){
        CKFinder.setupCKEditor( null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/' );
    });
</script>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="page-subheading post-buying-request-title">{{l}}NEW BUYING REQUEST{{/l}}</h3>
        </div>
        
        
        <div class="col-sm-12 page-content page-contact">
        {{if $lock == 1}}
        	<div class="alert alert-danger">
		                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
										{{l}}Số lượng post buying của tài khoản bạn đã hết . Vui lòng nâng cấp lên thành viên chính thức để tiếp tục tham gia các chức năng vip của chúng tôi.{{/l}}
								</div>
        {{else}}
            <form role="form" method="post">
                
                <div class="col-sm-6">
                    <div class="contact-form-box">
                    	<label>{{l}}Danh mục sản phẩm{{/l}}</label>
                        <div class="form-group">
                            <select class="selectpicker form-control" data-live-search="true" name="data[product_category_gid]">
                                {{foreach from=$allCatsProduct item=item}}
                                    <option value="{{$item.product_category_gid}}">{{$item.name}}</option>
                                {{/foreach}}
                            </select>
                        </div>
                        
                        <label>{{l}}Tiêu đề{{/l}}</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="data[name]" required>
                        </div>
                        
                    	<label>{{l}}Giá{{/l}}</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="data[price]" placeholder="{{l}}Giá{{/l}}" style="width: 49.5%; display: inline" required>
                            <select class="form-control" name="data[current]" style="width: 49.5%; display: inline">
                                {{foreach from=$unitProperties item=item}}
                                    {{if $item.parent_id != ''}}
                                        <option value="{{$item.properties_category_gid}}">{{$item.name}}</option>
                                    {{/if}}
                                {{/foreach}}
                            </select>
                        </div>
                        <label for="exampleInputEmail1">{{l}}Số lượng{{/l}}</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="data[so_luong]" placeholder="{{l}}Số lượng{{/l}}" style="width: 49.5%; display: inline" required>
                            <select class="form-control" name="data[so_luong_gid]" style="width: 49.5%; display: inline">
                                {{foreach from=$dvtProperties item=item}}
                                {{if $item.parent_id != ''}}
                                    <option value="{{$item.properties_category_gid}}">{{$item.name}}</option>
                                {{/if}}
                                {{/foreach}}
                            </select>
                        </div>
                        <label>{{l}}Ngày hết hạn{{/l}}</label>
                        <div class="form-group">
                            <input type="date" class="form-control" name="data[end_date]">
                        </div>
                        <label>{{l}}Nơi giao{{/l}}</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="data[add]" style="width: 49.5%; display: inline">
                            <select class="form-control" name="data[country_category_gid]" style="width: 49.5%; display: inline">
                                {{foreach from=$allCountry item=item}}
                                    <option value="{{$item.country_category_gid}}">{{$item.name}}</option>
                                {{/foreach}}
                            </select>
                        </div>
                        <label>{{l}}Điều kiện thanh toán / vận chuyển{{/l}}</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="data[buy]">
                        </div>
                        <label>{{l}}Tệp đính kèm{{/l}}</label>
                        <div class="form-group">
                            <input type="file" name="imges">
                        </div>
                        <button type="submit" class="btn btn-default" style="background: rgb(247, 185, 42) none repeat scroll 0px 0px; color: rgb(255, 255, 255); border: 1px solid rgb(234, 234, 234); margin: 10px 0px 0px; width: 100px; padding: 10px 12px; height: 38px;">Submit</button>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="contact-form-box">
                        <label>{{l}}Nội dung{{/l}}</label>
                        <div class="form-group" >
                            <textarea class="form-control input-sm textarea ckeditor" rows="4" name="data[description]" ></textarea>
                        </div>


                    </div>
                </div>
            </form>
            {{/if}}
        </div>
        
        
    </div>
</div>