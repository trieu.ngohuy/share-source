<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{$buyingRequest.name}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row buydetail">
            <div class="col-md-12">
                <h2>{{$buyingRequest.name}}</h2>
                <dl class="dl-horizontal">
                    <dt>{{l}}Estimate order quantity{{/l}}</dt>
                    <dd>{{$buyingRequest.so_luong}} {{$buyingRequest.pname}}</dd>
                    <dt>{{l}}Target price{{/l}}</dt>
                    <dd>{{$buyingRequest.price}} {{$buyingRequest.pname2}}</dd>
                    <dt>{{l}}Time left{{/l}}</dt>
                    <dd>{{$buyingRequest.end_date}}</dd>
                    <dt>{{l}}Date posted{{/l}}</dt>
                    <dd>{{$buyingRequest.created_date}}</dd>
                    <dt>{{l}}Location Delivery{{/l}}</dt>
                    <dd><img style="width: 20px;margin: 5px 0px;" src="{{$BASE_URL}}{{$country.images}}"/> {{$country.name}},{{$buyingRequest.add}}</dd>
                    <dt>{{l}}Tình Trạng{{/l}}</dt>
                    <dd>
						{{if $buyingRequest.status == "2"}}{{l}}Đang Báo Giá{{/l}}{{/if}}
						{{if $buyingRequest.status == "3"}}{{l}}Hết Hạn{{/l}}{{/if}}
						{{if $buyingRequest.status == "4"}}{{l}}Đã Chốt{{/l}}{{/if}}
					</dd>
                </dl>
            </div>
            {{if $isLogin == true && $userLogin.group_id == 3 && $buyingRequest.h_quote == 1}}
<!--            <div class="col-md-12">-->
<!---->
<!--                <button class="btn btn-warning text-uppercase" data-toggle="modal" data-target="#requester">-->
<!--                    {{l}}Infomation Requester{{/l}}-->
<!--                </button>-->
<!---->
<!--                <div class="modal fade" id="requester" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">-->
<!--                    <div class="modal-dialog">-->
<!--                        <div class="modal-content">-->
<!--                            <div class="modal-header">-->
<!--                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>-->
<!--                                <h4 class="modal-title" id="myModalLabel">{{l}}Infomation Requester{{/l}}</h4>-->
<!--                            </div>-->
<!--                            <div class="modal-body">-->
<!--                                <dl class="dl-horizontal">-->
<!--                                    <dt>{{l}}Fullname{{/l}}</dt><dd>{{$buyingRequest.full_name}}</dd>-->
<!--                                    <dt>{{l}}Email{{/l}}</dt><dd>{{$buyingRequest.email}}</dd>-->
<!--                                    <dt>{{l}}Phone{{/l}}</dt><dd>{{$buyingRequest.phone}}</dd>-->
<!--                                </dl>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            {{/if}}
            <div class="col-md-12 buydetail" style="margin: 20px 0px 0px;">
                <h2>{{l}}Request Detail{{/l}}</h2>
                <p>
                    {{$buyingRequest.description}}
                </p>
            </div>
            {{if $isLogin == true && $userLogin.group_id == 3 && $buyingRequest.h_quote == 1 &&  $buyingRequest.status != "4" &&  $buyingRequest.status != "3"}}
            <div class="col-md-12">
                <a class="btn btn-warning text-uppercase" target="_blank" href="{{$BASE_URL}}estore/edit-buying-request/{{$userLogin.alias}}/{{$buyingRequest.buying_category_gid}}">
                    {{l}}Quote{{/l}}
                </a>
            </div>
            {{/if}}

            <div class="col-md-12 buydetail" style="margin: 20px 0px;">
                <h2>{{l}}Quotations Record{{/l}}</h2>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>{{l}}COMPANY NAME{{/l}}</th>
                            <th>{{l}}BUSINESS TYPE{{/l}}</th>
                            <th>{{l}}LOCATION{{/l}}</th>
                            <th>{{l}}TIME QUOTED{{/l}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{foreach from=$allBuying item=item}}
                        <tr>
                            <td>*****************</td>
                            <td>{{$item.user.category_id}}</td>
                            <td>{{$item.user.ctname}}</td>
                            <td>{{$item.created_date}}</td>
                        </tr>
                        {{/foreach}}
                    </tbody>
                </table>
            </div>
        </div>
        <!-- ./row-->
    </div>
</div>
