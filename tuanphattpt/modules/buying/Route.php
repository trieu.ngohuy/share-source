<?php

class Route_Buying
{
    public function parse()
    {
        $router = Nine_Controller_Front::getInstance()->getRouter();
        $route = new Zend_Controller_Router_Route_Regex(
            'estore/manage-buying-request/(.*).html',
            array(
                'module' => 'buying',
                'controller' => 'estore',
                'action' => 'manage-buying-request'
            ),
            array(1 => 'alias')
        );
        $router->addRoute('buying', $route);

        $route1 = new Zend_Controller_Router_Route_Regex(
            'estore/new-buying-request/(.*).html',
            array(
                'module' => 'buying',
                'controller' => 'estore',
                'action' => 'new-buying-request'
            ),
            array(1 => 'alias')
        );
        $router->addRoute('buying1', $route1);

        $route2 = new Zend_Controller_Router_Route_Regex(
            'estore/edit-buying-request/(.*)/([0-9]+)',
            array(
                'module' => 'buying',
                'controller' => 'estore',
                'action' => 'edit-buying-request'
            ),
            array(
                1 => 'alias',
                2 => 'gid'
            )
        );
        $router->addRoute('buying2', $route2);

        $route3 = new Zend_Controller_Router_Route_Regex(
            'estore/edit-buying-request/(.*)/([0-9]+)/([0-9]+)',
            array(
                'module' => 'buying',
                'controller' => 'estore',
                'action' => 'edit-buying-request'
            ),
            array(
                1 => 'alias',
                2 => 'gid',
                3 => 'lid'
            )
        );
        $router->addRoute('buying3', $route3);

        $route4 = new Zend_Controller_Router_Route_Regex(
            'estore/quote-buying-request/(.*).html',
            array(
                'module' => 'buying',
                'controller' => 'estore',
                'action' => 'quote-buying-request'
            ),
            array(
                1 => 'alias',
            )
        );
        $router->addRoute('buying4', $route4);

        $route5 = new Zend_Controller_Router_Route_Regex(
            'post-buying-request',
            array(
                'module' => 'buying',
                'controller' => 'index',
                'action' => 'post-buying-request'
            )
        );
        $router->addRoute('buying5', $route5);

        $route6 = new Zend_Controller_Router_Route_Regex(
            'buying-request',
            array(
                'module' => 'buying',
                'controller' => 'index',
                'action' => 'buying-request'
            )
        );
        $router->addRoute('buying6', $route6);

        $route7 = new Zend_Controller_Router_Route_Regex(
            'buying-request/(.*)',
            array(
                'module' => 'buying',
                'controller' => 'index',
                'action' => 'buying-request-detail'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('buying7', $route7);
    }
}