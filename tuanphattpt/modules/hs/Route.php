<?php

class Route_Hs
{
    /**
     * Build friendly URL
     * @param array $linkArr 0 => <module>, 1 => controller, 2 => action, 3...n param/value
     * @param array $params param => value
     *
     * @return string friendly URL
     */
    public function build($linkArr, $params = array())
    {

        $result = implode('/', $linkArr);

        /**
         * Link is detail
         */
        if ('detail' == @$linkArr[2]) {
            /**
             * Structure: pages/<id>/<alias>.html
             */
            $result = "feedback/pages/{$linkArr[4]}/";
            if (null != $params['alias']) {
                $result .= urlencode($params['alias']);
            }

        }

        return $result;

    }

    /**
     * Parse friendly URL
     */
    public function parse()
    {
        $router = Nine_Controller_Front::getInstance()->getRouter();
        $route1 = new Zend_Controller_Router_Route_Regex(
            'hs-code',
            array(
                'module' => 'hs',
                'controller' => 'index',
                'action' => 'hs-code'
            )
        );
        $router->addRoute('hs1', $route1);

        $route2 = new Zend_Controller_Router_Route_Regex(
            'hs-code-vn',
            array(
                'module' => 'hs',
                'controller' => 'index',
                'action' => 'hs-code-vn'
            )
        );
        $router->addRoute('hs2', $route2);

    }
}