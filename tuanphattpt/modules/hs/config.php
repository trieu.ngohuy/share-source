<?php
return array (
    'permissionRules' => array(
                        'see_hs' => array(//External permission
                                            'See all articles',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission interface
                        'new_hs' => array(//External permission
                                            'Create new article',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
                        'edit_hs' => array(//External permission
                                            'Edit existed articles',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
						
						'genabled_hs' => 'Genabled existed articles',
                        'delete_hs' => 'Delete existed articles',
  
));
