<div class="container hs-code">
    <div class="row">
        <div class="col-sm-12">
            <h3>{{l}}HS CODE{{/l}}</h3>
        </div>
        <div class="col-sm-12">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
	                    <th>
	                        {{l}}TIER{{/l}}
	                    </th>
	                    <th>
	                        {{l}}HS CODE{{/l}}
	                    </th>
	                    <th>
	                        {{l}}PRODUCT DESCRIPTION{{/l}}
	                    </th>
	                    <th>
	                    	{{l}}STANDARDS REGULATION{{/l}}
	                    </th>
	                    
	                </tr>
                </thead>

                <tbody>
                {{foreach from=$hsCode item=item}}
                   <tr>
	                    <td>{{$item.lever}}</td>
	                    <td>{{$item.code}}</td>
	                    <td>{{$item.description}}</td>
	                    <td>{{$item.standards}}</td>
	                </tr>
                {{/foreach}}
                </tbody>
            </table>
        </div>
        <div class="col-md-12 sortPagiBar">
            {{if $countAllPages > 1}}
            <div class="bottom-pagination">
                <nav>
                    <ul class="pagination">
                        {{if $prevPage}}
                        <li>
                            <a href="?page={{$prevPage}}" aria-label="Prev">
                                <span aria-hidden="true">&laquo; Prev</span>
                            </a>
                        </li>
                        {{/if}}

                        {{foreach from=$prevPages item=item}}
                        <li><a href="?page={{$item}}">{{$item}}</a></li>
                        {{/foreach}}

                        <li class="active"><a>{{$currentPage}}</a></li>

                        {{foreach from=$nextPages item=item}}
                        <li><a href="?page={{$item}}">{{$item}}</a></li>
                        {{/foreach}}

                        {{if $nextPage}}
                        <li>
                            <a href="?page={{$nextPage}}" aria-label="Next">
                                <span aria-hidden="true">Next &raquo;</span>
                            </a>
                        </li>
                        {{/if}}
                    </ul>
                </nav>
            </div>
            {{/if}}
        </div>
    </div>
</div>
