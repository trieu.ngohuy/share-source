<?php
require_once 'modules/hs/models/HsCategory.php';

require_once 'libs/Nine/Controller/Action.php';

class hs_IndexController extends Nine_Controller_Action
{
    public function hsCodeAction()
    {
        $this->setLayout('front-default-category');
        $objHsCategory = new Models_HsCategory();

        $numRowPerPage = 20;
        $currentPage = $this->_getParam("page",false);
        $key = $this->_getParam("key",false);
        if($currentPage == false){
            $currentPage = 1;
            $this->session->key = null;
        }
        $condition = array();
		$condition['parent_id'] = 25;
    	if($key != false){
			$condition['keyword'] = $key;
			$this->session->key = $key;
			
		}else{
			$condition['keyword'] = $this->session->key;
		}
        $hsCode = $objHsCategory->getAllByParentID($condition, null,  $numRowPerPage, ($currentPage - 1) * $numRowPerPage);

        /**
         * Count all category
         */
        $count = count($objHsCategory->getAllByParentID($condition));
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->hsCode = $hsCode;
    }

    public function hsCodeVnAction()
    {
        $this->setLayout('front-default-category');
        $objHsCategory = new Models_HsCategory();

        $numRowPerPage = 20;
        $currentPage = $this->_getParam("page",false);
        $key = $this->_getParam("key",false);
        if($currentPage == false){
            $currentPage = 1;
            $this->session->key = null;
        }
		$condition = array();
		$condition['parent_id'] = 27;
		if($key != false){
			$condition['keyword'] = $key;
			$this->session->key = $key;
		}else{
			$condition['keyword'] = $this->session->key;
		}
        $hsCodeVn = $objHsCategory->getAllByParentID($condition, null,  $numRowPerPage, ($currentPage - 1) * $numRowPerPage);

        /**
         * Count all category
         */
        $count = count($objHsCategory->getAllByParentID($condition));

        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->hsCodeVn = $hsCodeVn;
    }
}