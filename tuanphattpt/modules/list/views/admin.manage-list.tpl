
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.js"></script>
			<div class="widget-top-search">
				<span class="icon"><a href="#" class="close-header-search"><i class="fa fa-times"></i></a></span>
				<form id="top-search" name="search" method="post" action="{{$APP_BASE_URL}}list/admin/manage-list">
						<h2><strong>Quick</strong> Search</h2>
						<div class="input-group col-sm-12">
							<div class="col-sm-4">
								<input  type="text" name="condition[name]" value="{{$condition.name}}"" placeholder="Find something..." class="form-control" />
							</div>
							<div class="col-sm-1">
								<button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
							</div>
							
						</div>
				</form>
			</div>
		
			<div id="main">
		
		
				<ol class="breadcrumb">
					<li><a href="{{$APP_BASE_URL}}default/admin/index">Dashboard</a></li>
					<li class="active">Manager List</li>
				</ol>
				<!-- //breadcrumb-->
				
				<div id="content">
				
					<div class="row">
						
						<div class="col-lg-12">
							<section class="panel">
								<header class="panel-heading">
									<h2><strong>Manager</strong> List </h2>
									<label class="color">List all List<em><strong> table </strong></em></label>
									
								</header>
								
								
								{{if $allLists|@count <= 0}}
		                        <div class="alert alert-info">
		                        	<a href="#" class="close"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
										<strong>Heads up!</strong> {{l}}No list with above condition.{{/l}}
								</div>
		                        {{/if}}
		                        
		                        {{if $listMessage|@count > 0 && $listMessage.success == true}}
		                        <div class="alert alert-success">
		                        	<a href="#" class="close"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
										<strong>Well done!</strong> {{$listMessage.message}}.
								</div>
		                        {{/if}}
		                        
		                        {{if $listMessage|@count > 0 && $listMessage.success == false}}
		                        <div class="alert alert-danger">
		                        	<a href="#" class="close"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
										<strong>Oh snap!</strong> {{$listMessage.message}}.
								</div>
		                        {{/if}}
		                        
                        		{{if $allLists|@count > 0}}
								<div class="panel-body">
									<div class="table-responsive">
										<table cellpadding="0" cellspacing="0" border="0" class="table table-striped">
											<thead>
												<tr>
													<th width="5%"><button class="btn btn-theme-inverse btn-transparent btn-header-search"><i class="fa fa-search"></i></button></th>
													<th><input class="check-all" type="checkbox" /></th>
				                                   	<th>ID</th>
				                                   	<th>Name</th>
				                                   	<th>Description</th>
				                                   	<th>Action</th>
												</tr>
											</thead>
											<tbody align="center">
												{{foreach from=$allLists item=item key=key}}
													<tr>
														<td>{{$key+1}}</td>
														<td><input type="checkbox" value="{{$item.list_id}}" name="allList" class="allList"/></td>
					                                    <td>{{$item.list_id}}</td>
					                                    <td>
					                                        {{$item.name}}
					                                    </td>
					                                    <td>
					                                        {{$item.description}}
					                                    </td>
					                                    <td class="center">
					                                        <span class="tooltip-area">
					                                        	<a href="{{$APP_BASE_URL}}list/admin/manage-list/id/{{$item.list_id}}" class="btn btn-default btn-sm"><i class="fa fa-table"></i></a>
																<a href="{{$APP_BASE_URL}}list/admin/edit-list/id/{{$item.list_id}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                    							<a href="{{$APP_BASE_URL}}list/admin/delete-list/id/{{$item.list_id}}"  class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash-o"></i></a>
															</span>
					                                    </td>
                                    
													</tr>
												{{/foreach}}
											</tbody>
										</table>
									</div>
								</div>
								{{/if}}
								
							</section>
							
						</div>
						
						<div class="col-lg-12">
							<div class="form-group col-lg-6">
								<label class="control-label col-lg-12">Action</label>
								<div class="col-lg-6">
									<select id="action" class="form-control" >
		                                <option value=";">Choose an action</option>
                                		<option value="deleteCategoryValue();">Delete</option>
		                            </select>
		                        </div>
		                        <div class="col-lg-6">
	                            	<a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
	                            </div>
							</div>
							
							<div class="form-group col-lg-2">
								<label class="control-label col-lg-12">Display Num</label>
								<div class="col-lg-12">
									<form class="search" name="search" method="post" action="{{$APP_BASE_URL}}list/admin/manage-list">
		                                <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
		                                    <option value="5" {{if $displayNum == 5}} selected="selected" {{/if}}>5</option>
		                                    <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
		                                    <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
		                                    <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
		                                    <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
		                                    <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
		                                </select>
		                            </form>
		                        </div>
							</div>
							
							{{if $countAllPages > 1}}
							<div class="form-group col-lg-4">
							<label class="control-label col-lg-12">&nbsp; </label>
								{{if $first}}
	                            <a href="?page=1"  class="btn btn-theme" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
	                            {{/if}}
	                            {{if $prevPage}}
	                            <a href="?page={{$prevPage}}"  class="btn btn-theme" title="{{l}}Previous Page{{/l}}">&laquo;</a>
	                            {{/if}}
	                            
	                            {{foreach from=$prevPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-theme" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            <a href="#"  class="btn btn-theme active" title="{{$currentPage}}">{{$currentPage}}</a>
	                            
	                            {{foreach from=$nextPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-theme" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            {{if $nextPage}}
	                            <a href="?page={{$nextPage}}"  class="btn btn-theme" title="{{l}}Next Page{{/l}}">&raquo;</a>
	                            {{/if}}
	                            {{if $last}}
	                            <a href="?page={{$countAllPages}}"  class="btn btn-theme" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
	                            {{/if}}
                            
							</div>
							{{/if}}
						</div>
					</div>
					<!-- //content > row-->
					
			</div>
				<!-- //content-->
				
				
		</div>


            
<script language="javascript" type="text/javascript">
$(document).ready(function(){
    document.getElementById('listname').select();
    document.getElementById('listname').focus();
    $('.close').click(function(){
		$(this).parent().hide( "slow");
    });
    $('.check-all').click(function(){
    	if(this.checked) { // check select status
            $('.allList').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.allList').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
});
function applySelected()
{
    var task = document.getElementById('action').value;
    eval(task);
}
function deleteCategoryValue()
{
    var all = document.getElementsByName('allList');
    var tmp = '';
    var count = 0;
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
             count++;
        }
    }
    if ('' == tmp) {
        alert('Please choose a value');
        return;
    } else {
        result = confirm('Are you sure you want to delete ' + count + ' value(s)?');
        if (false == result) {
            return;
        }
    }
    window.location.href = '{{$APP_BASE_URL}}list/admin/delete-List/id/' + tmp;
}
</script>
