<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.js"></script>
<script type="text/javascript">
$().ready(function() {
    // validate signup form on keyup and submit
    $("#editlist").validate({
        rules: {
            'data[name]': "required"
        },
        messages: {
            'data[name]': ""
        }
        
        
    }); 
});
</script> 

<script type="text/javascript">
    //<![CDATA[
    
        // Replace the <textarea id="editor"> with an CKEditor
        // instance, using default configurations.
        
//        CKFinder.setupCKEditor( null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/' );
//        CKEDITOR.replaceAll( 'editor');
    
//]]>
</script>
<style type="text/css">
.cke_top {
    background-color: #dddddd;
}
</style>


	<div id="main">
		<form action="" method="post">
			<div id="content">
				<div class="row">
					<div class="tabbable">
						<h2><strong>Edit</strong> List </h2>
						<hr>
						<ul class="nav nav-tabs" data-provide="tabdrop">
								<li class="active">
									<a href="#tab0" data-toggle="tab">General</a>
								</li>
						</ul>
						<div class="tab-content">
							
							<div class="form-horizontal tab-pane fade in active" id="tab0">
								{{if $errors|@count > 0}}
			                        <div class="alert alert-danger">
			                        	<a href="#" class="close"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
											<strong>Oh snap!</strong> {{$userMessage.message}}.
											{{if $errors.main}}
			                                   <strong>Oh snap!</strong> {{$errors.main}}
			                                {{else}}
			                                    <strong>Oh snap!</strong> {{l}}Please check following information again{{/l}}
			                                {{/if}} 
									</div>
			                	{{/if}}
			                	
                                <div class="form-group">
										<label class="control-label col-md-3">Parent Category</label>
										<div class="col-md-9">
												<select name="data[parent_id]" class="form-control">
													<option value="">Parent List</option>
				                                    {{foreach from=$list item=item}}
				                                        {{if $data.list_id != $item.list_id}} <option value="{{$item.list_id}}" {{if $item.list_id == $data.parent_id}}selected="selected" {{/if}}>{{$item.name}}</option>{{/if}}
				                                    {{/foreach}}
												</select>
										</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-3">Name</label>
									<div class="col-md-9">
											<input type="text" class="form-control" name="data[name]"  value="{{$data.name}}" >
									</div>
								</div>
								
                                
                                <div class="form-group">
									<label class="control-label col-md-3">Custom Value</label>
									<div class="col-md-9">
											<textarea class="form-control" name="data[custom_value]"  >{{$data.custom_value}}</textarea>
									</div>
								</div>
								
								<footer class="panel-footer">
										<button type="submit" class="btn btn-theme">Save</button>
								</footer>                                
			                	
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>