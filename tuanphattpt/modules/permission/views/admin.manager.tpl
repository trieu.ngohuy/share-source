<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.treeview.js" type="text/javascript"></script>
<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/css/jquery.treeview.css" />
<div class="page-header">
	<h1>
		{{l}}Permission{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}List All Group User Permission{{/l}}
		</small>
	</h1>
</div>
<div class="alert alert-success">
                {{p name='rescan_permission' module='permission'}}<a href="{{$APP_BASE_URL}}permission/admin/rescan"><img style="vertical-align: middle;" src="{{$LAYOUT_HELPER_URL}}admin/images/icons/refresh_16.png">Rescan all permissions</a>{{/p}}
			           	</div>
                        
                        {{if $permissionMessage|@count > 0 && $permissionMessage.success == true}}
                        <div class="alert alert-success">
                        	<button class="close" data-dismiss="alert">
								<i class="ace-icon fa fa-times"></i>
							</button>
                        	{{$permissionMessage.message}}.
						</div>
                        {{/if}}
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">
			
				<div class="col-sm-3">
					<div class="widget-box">
						<div class="widget-header widget-header-flat">
							<h4 class="widget-title">{{l}}Group{{/l}}</h4>
						</div>
	
						<div class="widget-body">
							<div class="widget-main">
								<div class="row">
									
									<div class="panel-body" style="display: block;">
											<ul id="group" class="filetree">
						                        <li><span class="group">Group</span>
						                            <ul>
						                                {{foreach from=$allGroups item=item}}
						                                <li><span class="file"><a href="javascript:chooseGroup({{$item.group_id}}, '{{$item.name}}');"><span style="color:{{$item.color}}">{{$item.name}}</span></a></span></li>
						                                {{/foreach}}
						                            </ul>
						                        </li>
						                    </ul>
									</div>
								</div>
	
							</div>
						</div>
					
					</div>
				</div>
				
				
				<div class="col-sm-3">
					<div class="widget-box">
						<div class="widget-header widget-header-flat">
							<h4 class="widget-title">{{l}}Access{{/l}}</h4>
						</div>
	
						<div class="widget-body">
							<div class="widget-main">
								<div class="row">
									<div class="panel-body" style="display: block;">
											<ul id="module" class="filetree">
						                        <li><span class="global">Applications</span>
						                            <ul>
						                                {{foreach from=$allApps item=item}}
						                                <li><span class="file"><a href="javascript:chooseAccess('application::{{$item}}');">{{$item}}</a></span></li>
						                                {{/foreach}}
						                            </ul>
						                        </li>
						                        <li><span class="module">Modules</span>
						                            <ul>
						                                {{foreach from=$allModules item=item}}
						                                <li><span class="file"><a href="javascript:chooseAccess('{{$item}}');">{{$item}}</a></span></li>
						                                {{/foreach}}
						                            </ul>
						                        </li>
						                    </ul>
									</div>
									
								</div>
	
							</div>
						</div>
					</div>
					
					
					
				</div>
				
				<div class="col-sm-6">
					<div class="widget-box">
						<div class="widget-header widget-header-flat">
							<h4 class="widget-title"><strong id="group_name">???</strong> >>> <strong id="access_name">???</strong></h4>
						</div>
	
						<div class="widget-body">
							<div class="widget-main">
								<div class="row">
									<div class="panel-body content-box-content" style="display: block; " id="permission_box">
										<p>Please select group and access.</p>
									</div>
								</div>
	
							</div>
						</div>
					</div>
					
					
				</div>
				
				
			</div>
			
		</div>
		
	</div>
	
</div>

<script language="javascript" type="text/javascript">
$(document).ready(function(){
    
    // second example
    $("#group").treeview();
    $("#module").treeview();
});

var groupId = '';
var access = '';

function chooseGroup(id, name) {
	groupId = id;
	$("#group_name").html(name);
	loadPermission();
}
function chooseAccess(name, atype) {
	access = name;
    $("#access_name").html(name);
    loadPermission();
}
function loadPermission() {
	if ('' == groupId || '' == access) {
		return;
	}
	$("#permission_box").html('Loading...');
	
	$.ajax({
	       type: "POST",
	       cache: false,
	       url: "{{$APP_BASE_URL}}permission/admin/get-permission",
	       data: {
    		   'groupId': groupId,
    		   'access': access
		   },
	       success: function(msg){
			   $("#permission_box").html(msg);
			   $('tbody > tr:odd').addClass("alt-row"); // Add class "alt-row" to even table rows // Add class "alt-row" to even table rows
	       }
	     });
	
}

function enablePermission(gid, pid, expandid) {
//    $("#permission_box").html('Loading...');
    
    $.ajax({
           type: "POST",
           cache: false,
           url: "{{$APP_BASE_URL}}permission/admin/enable-permission",
           data: {
               'gid': gid,
               'pid': pid,
               'expandid': expandid
           },
           success: function(msg){
               $("#permission_box").html(msg);
               $('tbody > tr:odd').addClass("alt-row"); // Add class "alt-row" to even table rows
           }
         });
}

function enableAllPermissions(gid, pid) {
//    $("#permission_box").html('Loading...');
    
    $.ajax({
           type: "POST",
           cache: false,
           url: "{{$APP_BASE_URL}}permission/admin/enable-all-permissions",
           data: {
               'gid': gid,
               'pid': pid
           },
           success: function(msg){
               $("#permission_box").html(msg);
               $('tbody > tr:odd').addClass("alt-row"); // Add class "alt-row" to even table rows
           }
         });
}

function disablePermission(gid, pid, expandid) {
//    $("#permission_box").html('Loading...');
    
    $.ajax({
           type: "POST",
           cache: false,
           url: "{{$APP_BASE_URL}}permission/admin/disable-permission",
           data: {
               'gid': gid,
               'pid': pid,
               'expandid': expandid
           },
           success: function(msg){
               $("#permission_box").html(msg);
               $('tbody > tr:odd').addClass("alt-row"); // Add class "alt-row" to even table rows
           }
         });
}
function disableAllPermissions(gid, pid) {
//    $("#permission_box").html('Loading...');
    
    $.ajax({
           type: "POST",
           cache: false,
           url: "{{$APP_BASE_URL}}permission/admin/disable-all-permissions",
           data: {
               'gid': gid,
               'pid': pid
           },
           success: function(msg){
               $("#permission_box").html(msg);
               $('tbody > tr:odd').addClass("alt-row"); // Add class "alt-row" to even table rows
           }
         });
}
</script>
