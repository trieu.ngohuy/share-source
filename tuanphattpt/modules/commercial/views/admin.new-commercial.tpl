<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<script type="text/javascript">
    //<![CDATA[

    jQuery(document).ready(function (){
        CKFinder.setupCKEditor( null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/' );
        jQuery( "#images" ).sortable();
        jQuery( "#images" ).disableSelection();
        //Make slug
        {{foreach from=$allLangs item=item name=langDiv}}
        jQuery('#name{{$smarty.foreach.langDiv.iteration}}').makeSlug({
            slug: jQuery('#alias{{$smarty.foreach.langDiv.iteration}}')
        });
        {{/foreach}}

    });
    var imgId;
    function chooseImage(id)
    {
        imgId = id;
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();
        finder.basePath = '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/'; // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.selectActionFunction = setFileField;
        finder.popup();
    }
    // This is a sample function which is called when a file is selected in CKFinder.
    function setFileField( fileUrl )
    {
        document.getElementById( 'chooseImage_img' + imgId ).src = fileUrl;
        document.getElementById( 'chooseImage_input' + imgId).value = fileUrl;
        document.getElementById( 'chooseImage_div' + imgId).style.display = '';
        document.getElementById( 'chooseImage_noImage_div' + imgId ).style.display = 'none';
    }
    function clearImage(imgId)
    {
        document.getElementById( 'chooseImage_img' + imgId ).src = '';
        document.getElementById( 'chooseImage_input' + imgId ).value = '';
        document.getElementById( 'chooseImage_div' + imgId).style.display = 'none';
        document.getElementById( 'chooseImage_noImage_div' + imgId).style.display = '';
    }

    function addMoreImg()
    {
        jQuery("ul#images > li.hidden").filter(":first").removeClass('hidden');
    }

    //]]>
</script>
<style type="text/css">
    #images { list-style-type: none; margin: 0; padding: 0;}
    #images li { margin: 10px; float: left; text-align: center;  height: 180px;}
</style>


<div class="page-header">
    <h1>
        {{l}}Commercial{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}New Commercial{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">
                {{if $errors|@count > 0}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{if $errors.main}}
                    </strong> {{$errors.main}}
                    {{else}}
                    {{l}}Please check following information again{{/l}}
                    {{/if}}
                </div>
                {{/if}}


                <div class="col-sm-12 widget-container-col ui-sortable">

                    <div class="tabbable tabs-left">
                        <form action="" method="post" class="form-horizontal">
                            <div class="tab-content">
                                <div id="tab0" class="tab-pane active">

                                    <div class="form-group has-info">
                                        <label class="control-label col-md-2">{{l}}Link Website{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="data[link]"  value="{{$data.link}}" >
                                        </div>
                                    </div>
                                    <div class="form-group has-info">
                                        <label class="control-label col-md-2">{{l}}Location{{/l}}</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="data[location]">
                                                <option value="1">{{l}}Home Page{{/l}}</option>
                                                <option value="2">{{l}}Children Page{{/l}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <footer class="panel-footer">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{l}}Images:{{/l}}</label>
                                            <div class="col-md-9">

                                                <ul id="images">
                                                    <li>
                                                        <input type="hidden" id="chooseImage_input0icon" name="data[images]">
                                                        <div id="chooseImage_div0icon" {{if $data.images == ''}} style="display: none;" {{/if}}>
                                                            <img src="{{$BASE_URL}}{{$data.images}}" id="chooseImage_img0icon" style="max-width: 150px; max-height:150px; border:dashed thin;"/>
                                                        </div>
                                                        <div id="chooseImage_noImage_div0icon" style="width: 150px; border: thin dashed; text-align: center; padding:70px 0px; {{if $data.images != ''}} display: none; {{/if}}">
                                                            No image
                                                        </div>
                                                        <br/>
                                                        <a href="javascript:chooseImage('0icon');">{{l}}Choose image{{/l}}</a>
                                                        |
                                                        <a href="javascript:clearImage('0icon');">{{l}}Delete{{/l}}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>


                                    </footer>
                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Save{{/l}}
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </form>

                    </div>

                </div>

            </div><!-- /.span -->

        </div><!-- /.row -->
    </div>
</div>

