<?php
require_once 'modules/commercial/models/Commercial.php';

class commercial_AdminController extends Nine_Controller_Action_Admin
{
    public function manageCommercialAction()
    {

        $this->view->headTitle(Nine_Language::translate('Manage Commercial'));

        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page", false);
        $displayNum = $this->_getParam('displayNum', false);

        if ($currentPage == false) {
            $currentPage = 1;
            $this->session->groupDisplayNum = null;
        }

        /**
         * Get number of groups per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->groupDisplayNum;
        } else {
            $this->session->groupDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Load all groups
         */
        $objCommercial = new Models_Commercial();
        $allCommercials = $objCommercial->getAll('commercial_id DESC', $numRowPerPage,
            ($currentPage - 1) * $numRowPerPage)->toArray();
        /**
         * Count all groups
         */
        $count = count($allCommercials);
        /**
         * Load all groups
         */
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCommercials = $allCommercials;
        $this->view->groupMessage = $this->session->groupMessage;
        $this->session->groupMessage = null;
        $this->view->displayNum = $numRowPerPage;

        $this->view->menu = array(
            0 => 'commercial',
            1 => 'manager-commercial'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-users',
                'url' => Nine_Registry::getBaseUrl() . 'admin/commercial/admin/manage-commercial',
                'name' => Nine_Language::translate('Manager Commercial')
            )
        );
    }

    public function newCommercialAction()
    {
        $objCommercial = new Models_Commercial();

        $data = $this->_getParam('data', false);
        /**
         * Get all categories
         */

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCommercial = $data;
            $newCommercial['created_date'] = time();

            /**
             * Remove empty images
             */

            $newCommercial['images'] = Nine_Function::getImagePath($newCommercial['images']);

            try {
                $objCommercial->insert($newCommercial);

                /**
                 * Message
                 */
                $this->session->categoryMessage =  array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is created successfully.')
                );

                $this->_redirect('commercial/admin/manage-commercial');
            } catch (Exception $e) {
                echo '<pre>';
                echo print_r($e);
                echo '<pre>';
                die;
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Commercial'));

        $this->view->menu = array(
            0=>'commercial',
            1=>'new-commercial'
        );

        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-university',
                'url'	=>	Nine_Registry::getBaseUrl().'admin/commercial/admin/manage-commercial',
                'name'	=>	Nine_Language::translate('Manager Commercial')
            ),
            1=>array(
                'icon' 	=> 	'fa-plus',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('New Commercial')
            )

        );
    }

    public function editCommercialAction()
    {
        $id = $this->_getParam('id', false);
        $data = $this->_getParam('data', false);

        if (false == $id) {
            $this->_redirect('commercial/admin/manage-commercial');
        }

        $objCommercial = new Models_Commercial();
        $errors = array();
        /**
         * Get old group
         */
        $oldCommercial = $objCommercial->find($id)->current()->toArray();

        if(empty($oldCommercial)) {
            $this->_redirect('commercial/admin/manage-commercial');
        }

        if (false !== $data) {
            /**
             * Update new commercial
             */
            /**
             * Insert new category
             */
            $newCommercial = $data;
            $newCommercial['created_date'] = time();

            /**
             * Remove empty images
             */
            if($newCommercial['images'] != '') {
                $newCommercial['images'] = Nine_Function::getImagePath($newCommercial['images']);
            } else {
                unset($newCommercial['images']);
            }

            try {
                $objCommercial->update($newCommercial, array('commercial_id=?' => $id));

                /**
                 * Message
                 */
                $this->session->categoryMessage =  array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is edit successfully.')
                );

                $this->_redirect('commercial/admin/manage-commercial');
            } catch (Exception $e) {
                echo '<pre>';
                echo print_r($e);
                echo '<pre>';
                die;
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }

        } else {
            /**
             * Get current commercial
             */
            $data = $oldCommercial;
        }
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Commercial'));

        $this->view->menu = array(
            0=>'commercial',
            1=>'edit-commercial'
        );

        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-university',
                'url'	=>	Nine_Registry::getBaseUrl().'admin/commercial/admin/manage-commercial',
                'name'	=>	Nine_Language::translate('Manager Commercial')
            ),
            1=>array(
                'icon' 	=> 	'fa-plus',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('Edit Commercial')
            )

        );
    }

    public function disableCommercialAction()
    {
        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('commercial/admin/manage-commercial');
        }

        $objCommercial = new Models_Commercial();

        $objCommercial->update(array('enabled' => 0), array('commercial_id=?' => $id));
        $this->_redirect('commercial/admin/manage-commercial');
    }

    public function enableCommercialAction()
    {
        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('commercial/admin/manage-commercial');
        }

        $objCommercial = new Models_Commercial();

        $objCommercial->update(array('enabled' => 1), array('commercial_id=?' => $id));
        $this->_redirect('commercial/admin/manage-commercial');
    }

    public function deleteCommercialAction()
    {
        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('commercial/admin/manage-commercial');
        }

        $objCommercial = new Models_Commercial();

        $objCommercial->delete(array('commercial_id=?' => $id));
        $this->_redirect('commercial/admin/manage-commercial');
    }
}