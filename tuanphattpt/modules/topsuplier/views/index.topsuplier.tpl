<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        {{sticker name=common_breadcrumbs}}
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Categories</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    {{foreach from=$category item=item}}
                                    <li class="active">
                                        <span></span><a href="{{$BASE_URL}}category/{{$item.alias}}">{{$item.name}}</a>
                                    </li>
                                    {{/foreach}}
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <div class="col-xs-12 col-sm-12">
                    <ul class="blog-posts">
                        <li class="post-item" style="border: 0px;">
                            <article class="entry">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="entry-thumb image-hover2">
                                            <a href="{{$BASE_URL}}{{$user.alias}}.htm">
                                                <img src="{{$BASE_URL}}{{$topsuplier.images}}" alt="{{$user.name_company}}">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="entry-ci">
                                            <h3 class="entry-title"><a href="{{$BASE_URL}}{{$user.alias}}.htm"><b>{{$user.name_company}}</b></a></h3>

                                            <div class="entry-excerpt">
                                                <p><b>{{l}}Main products{{/l}}:</b> {{$user.sanphamchinh}}</p>
                                                <p><b>{{l}}Year Company Registered{{/l}}:</b> {{$user.namdangky}}</p>
                                            </div>
                                            <div class="entry-more">
                                                <a href="{{$BASE_URL}}{{$user.alias}}/contact-us" class="contact_now" >Contact Now</a>
                                                <a href="#">Chat</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-12">
                    <div class="product-tab">
                        <ul class="nav-tab">
                            <li class="active">
                                <a aria-expanded="true" data-toggle="tab">{{l}}INTRODUCTION{{/l}}</a>
                            </li>
                        </ul>
                        <div class="tab-container">
                            <div class="tab-panel active">
                                <!-- view-product-list-->
                                <div id="view-product-list" class="view-product-list" style="margin: 0px;">
                                    {{$topsuplier.description}}
                                    <h2 class="page-heading">
                                        <span class="page-heading-title">{{l}}Product{{/l}}</span>
                                    </h2>
                                    <!-- PRODUCT LIST -->
                                    <ul class="row product-list grid estore-product-slide">
                                        {{foreach from=$allProduct item=item}}
                                        <li class="col-sx-12 col-sm-3">
                                            <div class="product-container">
                                                <div class="left-block">
                                                    <a href="{{$BASE_URL}}product/{{$item.alias}}">
                                                        <img class="img-responsive" alt="product" src="{{$BASE_URL}}{{$item.main_image}}" />
                                                    </a>
                                                </div>
                                                <hr>
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="{{$BASE_URL}}product/{{$item.alias}}">{{$item.title}}</a></h5>

                                                    <div class="content_price">
                                                        <span class="price product-price">Call / (Min. Order)</span>
                                                    </div>
                                                    <div class="info-orther1">
                                                        <p><b>{{$item.name_company}}</b></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        {{/foreach}}
                                        <br class="cb" style="clear:both">
                                    </ul>
                                    <!-- ./PRODUCT LIST -->
                                </div>
                                <!-- ./view-product-list-->
                                <div class="sortPagiBar">
                                    {{if $countAllPages > 1}}
                                    <div class="bottom-pagination">
                                        <nav>
                                            <ul class="pagination">
                                                {{if $prevPage}}
                                                <li>
                                                    <a href="?page={{$prevPage}}" aria-label="Prev">
                                                        <span aria-hidden="true">&laquo; Prev</span>
                                                    </a>
                                                </li>
                                                {{/if}}

                                                {{foreach from=$prevPages item=item}}
                                                <li><a href="?page={{$item}}">{{$item}}</a></li>
                                                    {{/foreach}}

                                                <li class="active"><a>{{$currentPage}}</a></li>

                                                {{foreach from=$nextPages item=item}}
                                                <li><a href="?page={{$item}}">{{$item}}</a></li>
                                                    {{/foreach}}

                                                {{if $nextPage}}
                                                <li>
                                                    <a href="?page={{$nextPage}}" aria-label="Next">
                                                        <span aria-hidden="true">Next &raquo;</span>
                                                    </a>
                                                </li>
                                                {{/if}}
                                            </ul>
                                        </nav>
                                    </div>
                                    {{/if}}
                                </div>
                                <br class="cb">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

