<?php

class Route_Topsuplier
{

    /**
     * Parse friendly URL
     */
    public function parse()
    {
        $router = Nine_Controller_Front::getInstance()->getRouter();
        $route = new Zend_Controller_Router_Route_Regex(
            'topsuplier/(.*)',
            array(
                'module' => 'topsuplier',
                'controller' => 'index',
                'action' => 'topsuplier'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('topsuplier', $route);


    }
}