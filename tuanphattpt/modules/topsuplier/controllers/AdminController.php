<?php

require_once 'modules/content/models/Content.php';
require_once 'modules/topsuplier/models/TopsuplierCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/user/models/User.php';
require_once 'modules/user/models/UserProfile.php';

class topsuplier_AdminController extends Nine_Controller_Action_Admin {

    public function manageCategoryAction() {
        $objLang = new Models_Lang();
        $objCategory = new Models_TopsuplierCategory();


        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_topsuplier', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $this->view->headTitle(Nine_Language::translate('manage Category '));


        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page", false);

        $displayNum = $this->_getParam('displayNum', false);

        if ($currentPage == false) {
            $currentPage = 1;
            $this->session->categoryDisplayNum = null;
            $this->session->categoryCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id => $value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objCategory->update(array('sorting' => $value), array('topsuplier_category_gid=?' => $id));
            $this->session->categoryMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("Edit sort numbers successfully")
            );
        }

        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->categoryDisplayNum;
        } else {
            $this->session->categoryDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->categoryCondition;
        } else {
            $this->session->categoryCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }

        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_topsuplier', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        /**
         * Get all categorys
         */
        $numRowPerPage = $numRowPerPage;
        $allCategories = $objCategory->setAllLanguages(true)->getallCategories($condition, array('topsuplier_category_gid', 'topsuplier_category_gid DESC', 'topsuplier_category_id ASC'), $numRowPerPage, ($currentPage - 1) * $numRowPerPage
        );
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Count all categorys
         */
        $count = count($objCategory->setAllLanguages(true)->getallCategories($condition));
        /**
         * Format
         */
        $tmp = array();
        $tmp2 = false;
        $tmpGid = @$allCategories[0]['topsuplier_category_gid'];
        $objUserProfile = new Models_UserProfile();
        foreach ($allCategories as $index => $category) {
            /**
             * Change date
             */
            $profile = @reset($objUserProfile->getByColumns(array('user_id = ?' => $category['user_id']))->toArray());
            $category['pcompany_name'] = $profile['company_name'];

            if (0 != $category['created_date']) {
                $category['created_date'] = date($config['dateFormat'], $category['created_date']);
            } else {
                $category['created_date'] = '';
            }
            if ($tmpGid != $category['topsuplier_category_gid']) {
                $tmp[] = $tmp2;
                $tmp2 = false;
                $tmpGid = $category['topsuplier_category_gid'];
            }
            if (false === $tmp2) {
                $tmp2 = $category;
            }
            $tmp2['langs'][] = $category;
            /**
             * Final element
             */
            if ($index == count($allCategories) - 1) {
                $tmp[] = $tmp2;
            }
        }

        $allCategories = $tmp;
        $export = $this->_getParam('export', false);
        if ($export != false) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $style = "style = 'border: 1px solid'";

            if (Nine_Language::getCurrentLangId() == 1) {
                $textHeader = array(
                    "<b>No</b>",
                    "<b>Estore</b>",
                    "<b>Created Date</b>",
                    "<b>Title</b>"
                );
            } else {
                $textHeader = array(
                    mb_convert_encoding("<b>Số Thứ Tự</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Gian Hàng</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Ngày Khởi Tạo</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Tiêu Đề</b>", 'HTML-ENTITIES', 'UTF-8')
                );
            }

            $header = "<tr>";
            foreach ($textHeader as $text) {
                $header .= "<td $style>" . utf8_encode($text) . "</td>";
            }
            $header .= "</tr>";

            $content = '';
            $no = 1;

            foreach ($allCategories as $item) {
                $content .= "<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['username'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['created_date'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['name'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "</tr>";
                $no++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-topsuplier-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();
        }
//        echo print_r($allCategories);die;
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCategories = $allCategories;
        $this->view->categoryMessage = $this->session->categoryMessage;
        $this->session->categoryMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_topsuplier');
        $this->view->allLangs = $allLangs;

        $this->view->menu = array(
            0 => 'setting',
            1 => 'topsuplier',
            2 => 'manager-topsuplier'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-university',
                'url' => Nine_Registry::getBaseUrl() . 'admin/topsuplier/admin/manage-category',
                'name' => Nine_Language::translate('Manager Category Topsuplier')
            )
        );

        $allCats = $objCategory->buildTree($objCategory->getAllCategoriesParentNull());
        $this->view->allCats = $allCats;
    }

    public function newCategoryAction() {
        $objLang = new Models_Lang();
        $objCategory = new Models_TopsuplierCategory;
        $objContent = new Models_Content();

        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_topsuplier', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data = $this->_getParam('data', false);
        /**
         * Get all categories
         */
        $allCats = $objCategory->buildTree($objCategory->getAll(array('sorting ASC'))->toArray());
//        echo "<pre>";print_r($allCats);die; 
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_topsuplier', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCategory = $data;
            $newCategory['created_date'] = time();
            /**
             * Sorting
             */
            if (null == $newCategory['sorting']) {
                unset($newCategory['sorting']);
            }
            if (false == $this->checkPermission('new_topsuplier', null, '*')) {
                $newCategory['genabled'] = 0;
                $newCategory['sorting'] = 1;
            }


            /**
             * Remove empty images
             */
            if (is_array($newCategory['images'])) {
                foreach ($newCategory['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newCategory['images'][$index]);
                    } else {
                        $newCategory['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newCategory['images'] = implode('||', $newCategory['images']);


            foreach ($allLangs as $index => $lang) {
                if ($newCategory[$lang['lang_id']]['alias'] == "") {
                    $newCategory[$lang['lang_id']]['alias'] = $objContent->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
                    $newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", $newCategory[$lang['lang_id']]['alias']));
                }
            }
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > @$newCategory['sorting']) {
                    $newCategory['sorting'] = 1;
                }
                if (null == $newCategory['parent_id']) {
                    $newCategory['parent_id'] = NULL;
                }
                $objCategory->increaseSorting($newCategory['sorting'], 1);

                $gid = $objCategory->insert($newCategory);

                /**
                 * Update id string
                 */
                $objCategory->update(array('gid_string' => $gid), array('topsuplier_category_gid = ?' => $gid));
                $category = @reset($objCategory->getByColumns(array('topsuplier_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);
                /**
                 * Message
                 */
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is created successfully.')
                );

                $this->_redirect('topsuplier/admin/manage-category');
            } catch (Exception $e) {
                echo '<pre>';
                echo print_r($e);
                echo '<pre>';
                die;
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Category'));
        $this->view->fullPermisison = $this->checkPermission('new_topsuplier', null, '*');

        $this->view->menu = array(
            0 => 'setting',
            1 => 'topsuplier',
            2 => 'new-topsuplier'
        );

        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-university',
                'url' => Nine_Registry::getBaseUrl() . 'admin/topsuplier/admin/manage-category',
                'name' => Nine_Language::translate('Manager Category Topsuplier')
            ),
            1 => array(
                'icon' => 'fa-plus',
                'url' => '',
                'name' => Nine_Language::translate('New Category Topsuplier')
            )
        );
        $allUsers = Nine_Query::getListUsers(array(
            'group_id' => 3
        ));
        $this->view->allUsers = $allUsers;
    }

    public function editCategoryAction() {
        $objCategory = new Models_TopsuplierCategory();
        $objLang = new Models_Lang();
        $objContent = new Models_Content();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_topsuplier', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        if (false == $gid) {
            $this->_redirect('topsuplier/admin/manage-category');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_topsuplier', null, '*')) || (false != $lid && false == $this->checkPermission('edit_topsuplier', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data = $this->_getParam('data', false);

        /**
         * Get all categories
         */
        $allCats = $objCategory->buildTree($objCategory->getAll(array('sorting ASC'))->toArray());

        /**
         * Get all category languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allCategoryLangs = $objCategory->setAllLanguages(true)->getByColumns(array('topsuplier_category_gid=?' => $gid))->toArray();
//        echo "<pre>";print_r($allCategoryLangs);die; 
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_topsuplier', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allCategoryLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCategory = $data;
            /**
             * Sorting
             */
            if (null == $newCategory['sorting']) {
                unset($newCategory['sorting']);
            }
            if (false == $this->checkPermission('new_topsuplier', null, '*')) {
                unset($newCategory['genabled']);
                unset($newCategory['sorting']);
            }

            /**
             * Remove empty images
             */
            if (is_array($newCategory['images'])) {
                foreach ($newCategory['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newCategory['images'][$index]);
                    } else {
                        $newCategory['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newCategory['images'] = implode('||', $newCategory['images']);


            foreach ($allLangs as $index => $lang) {
                if ($newCategory[$lang['lang_id']]['alias'] == "") {
                    $newCategory[$lang['lang_id']]['alias'] = $objContent->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
                    $newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", $newCategory[$lang['lang_id']]['alias']));
                }
            }
            try {
                /**
                 * Update
                 */
                if (null == $newCategory['parent_id']) {
                    $newCategory['parent_id'] = NULL;
                }
                /**
                 * Delete gid in parent
                 */
                $oldCategory = @reset($allCategoryLangs);
//                echo "<pre>";print_r($oldCategory);die; 
                $objCategory->deleteGidString($oldCategory['parent_id'], $oldCategory['gid_string']);

                /**
                 * Update new data
                 */
                $objCategory->update($newCategory, array('topsuplier_category_gid=?' => $gid));

                /**
                 * Update new id string
                 */
                $category = @reset($objCategory->getByColumns(array('topsuplier_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);

                /**
                 * Message
                 */
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is updated successfully.')
                );

                $this->_redirect('topsuplier/admin/manage-category');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allCategoryLangs);
            if (false == $data) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("Category doesn't exist.")
                );
                $this->_redirect('topsuplier/admin/manage-category');
            }
            /**
             * Format image
             */
            $data['images'] = explode('||', $data['images']);
            if (!is_array($data['images'])) {
                $data['images'] = array();
            }
            $data['images'] = array_pad($data['images'], 50, null);
            /**
             * Get all lang categorys
             */
            foreach ($allCategoryLangs as $category) {
                $data[$category['lang_id']] = $category;
            }

            /**
             * Get all child category
             */
            $allChildCats = explode(',', trim($data['gid_string'], ','));
            unset($allChildCats[0]);
            foreach ($allCats as $key => $item) {
                if (false != in_array($item, $allChildCats)) {
                    unset($allCats[$key]);
                }
            }
        }
        /**
         * Remove it and its childs from category list
         */
        $oldData = @reset($allCategoryLangs);
        foreach ($allCats as $index => $item) {
            if (false !== strpos(",{$oldData['gid_string']},", ",{$item['topsuplier_category_gid']},")) {
                unset($allCats[$index]);
            }
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Category'));
        $this->view->fullPermisison = $this->checkPermission('edit_topsuplier', null, '*');
        $this->view->menu = array(
            0 => 'setting',
            1 => 'topsuplier',
            2 => 'manager-topsuplier'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-university',
                'url' => Nine_Registry::getBaseUrl() . 'admin/topsuplier/admin/manage-category',
                'name' => Nine_Language::translate('Manager Category Topsuplier')
            ),
            1 => array(
                'icon' => 'fa-pencil',
                'url' => '',
                'name' => Nine_Language::translate('Edit Category Topsuplier')
            )
        );
        $objUser = new Models_User();
        $condition['group_id'] = 3;
        $allUsers = $objUser->getAllUsersWithGroupNotUser($condition, 'user_id DESC');
        $this->view->allUsers = $allUsers;
    }

    public function enableCategoryAction() {
        $objCategory = new Models_TopsuplierCategory;
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);

        if (false == $gid) {
            $this->_redirect('topsuplier/admin/manage-category');
        }

        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_topsuplier', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objCategory->update(array('genabled' => 1), array('topsuplier_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is enable successfully')
                );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                );
            }
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_topsuplier', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objCategory->update(array('enabled' => 1), array('topsuplier_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is enable successfully')
                );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                );
            }
        }


        $this->_redirect('topsuplier/admin/manage-category');
    }

    public function disableCategoryAction() {
        $objCategory = new Models_TopsuplierCategory;
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);

        if (false == $gid) {
            $this->_redirect('topsuplier/admin/manage-category');
        }

        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_topsuplier', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objCategory->update(array('genabled' => 0), array('topsuplier_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is disable successfully')
                );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                );
            }
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_topsuplier', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objCategory->update(array('enabled' => 0), array('topsuplier_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is disable successfully')
                );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                );
            }
        }


        $this->_redirect('topsuplier/admin/manage-category');
    }

    public function deleteCategoryAction() {
        $objCategory = new Models_TopsuplierCategory;
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_topsuplier')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid = $this->_getParam('gid', false);

        if (false == $gid) {
            $this->_redirect('topsuplier/admin/manage-category');
        }

        $gids = explode('_', trim($gid, '_'));

        try {
            foreach ($gids as $gid) {

                $cat = @reset($objCategory->getByColumns(array('topsuplier_category_gid=?' => $gid))->toArray());
                if (0 == $cat['topsuplier_deleteable']) {
                    $this->session->categoryMessage = array(
                        'success' => false,
                        'message' => Nine_Language::translate('Can NOT delete category (' . $gid . '). Please try again')
                    );
                    $this->_redirect('topsuplier/admin/manage-category');
                } else {
                    $objCategory->delete(array('topsuplier_category_gid=?' => $gid));
                }
            }
            $this->session->categoryMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Category is deleted successfully')
            );
        } catch (Exception $e) {
            $this->session->categoryMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT delete this category. Please try again')
            );
        }
        $this->_redirect('topsuplier/admin/manage-category');
    }

    public function changeStringAction() {
        $objContent = new Models_Content();
        $str = $this->_getParam("string", "");
        $str = $objContent->convert_vi_to_en($str);
        $str = str_replace(" ", "-", trim($str));
        echo $str;
        die;
    }

}
