<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/content/models/Content.php';
require_once 'modules/topsuplier/models/TopsuplierCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/user/models/User.php';
require_once 'modules/topsuplier/models/TopsuplierCategory.php';
require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/EstoreCategory.php';
class topsuplier_IndexController extends Nine_Controller_Action {

    public function topsuplierAction() {
        $objTopsuplier = new Models_TopsuplierCategory();
        $objUser = new Models_User();
        $objProduct = new Models_Product();
        $objProductCategory = new Models_ProductCategory();
        $objEstoreCategory = new Models_EstoreCategory();

        $this->setLayout('detail');

        $alias = $this->_getParam('alias', false);

        /*
         * Get estore infomation and list products
         */
        if (false != $alias) {
            $topsuplier = $objTopsuplier->setAllLanguages(true)->getByAlias($alias);

            $topsuplier = @reset($objTopsuplier->getAllCategories(array('category_gid' => $topsuplier['topsuplier_category_gid'])));
            $category = $objEstoreCategory->getByColumns(array(
                    'user_id' => $topsuplier['user_id']
                    ))->toArray();
            

            if ($topsuplier['user_id'] != '') {

                $user = $objUser->getByUserId($topsuplier['user_id']);
                /*
                 * Get bread crumb
                 */
                $this->view->breadCrumbs = Nine_Common::getBreadCumbs(
                                -1, 'topsuplier', array(
                            'name' => $user['name_company'],
                            'url' => "topsuplier/" . $user['alias']
                                ), null
                );

                $numRowPerPage = 20;
                $currentPage = $this->_getParam("page", false);
                if ($currentPage == false) {
                    $currentPage = 1;
                }


                $allProduct = $objProduct->getProductRelatedByUserID($user['user_id'], null, $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
                $url = Nine_Route::url();
                foreach ($allProduct as &$item) {
                    $tmp = explode('||', $item ['images']);
                    $item ['main_image'] = $tmp [0];
                    $item['title'] = substr($item['title'], 0, 25);
                }
                unset($item);
                $count = count($objProduct->getProductRelatedByUserID($user['user_id']));
                $this->setPagination($numRowPerPage, $currentPage, $count);
            } else {
                //$this->_redirect('/');
            }

            $this->view->category = $category;
            $this->view->topsuplier = $topsuplier;
            $this->view->allProduct = $allProduct;
            $this->view->user = $user;
        } else {
            $this->_redirect('/');
        }
    }

}
