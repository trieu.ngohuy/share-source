


<div class="page-header">
	<h1>
		{{l}}Category Newlater{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}List All Category Newlater{{/l}}
		</small>
	</h1>
</div>

<br class="cb">
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">
				<table id="simple-table" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<form id="top-search" name="search" method="post" action="">
								<th class="center">
								</th>
                                <th>{{l}}Email{{/l}}</th>
                                <th>{{l}}User Register{{/l}}</th>
                        	</form>
						</tr>
					</thead>

					<tbody>
							<form action="" method="post" name="sortForm">
							{{foreach from=$allFollow.follow item=item name=category key=key}}
								<tr>
									<td class="center">{{$key+1}}</td>
                                    <td>
										{{$item.email}}
									</td>
                                    <td>{{$item.pcompany_name}}</td>
								</tr>
							{{/foreach}}
							</form>
					</tbody>
				</table>
				
				
			</div>
		</div>
		
		
		<div class="col-lg-12">
							<div class="form-group col-lg-4">
								<label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
		                        <div class="col-lg-6">
	                            	<a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
	                            </div>
							</div>
							{{if $countAllPages > 1}}
							<div class="col-lg-6 pagination">
								{{if $first}}
	                            <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
	                            {{/if}}
	                            {{if $prevPage}}
	                            <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
	                            {{/if}}
	                            
	                            {{foreach from=$prevPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>
	                            
	                            {{foreach from=$nextPages item=item}}
	                            <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
	                            {{/foreach}}
	                            
	                            {{if $nextPage}}
	                            <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
	                            {{/if}}
	                            {{if $last}}
	                            <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
	                            {{/if}}
                            
							</div>
							{{/if}}
						</div>
					</div>
		
		
	</div>
</div>

<script language="javascript" type="text/javascript">
$(document).ready(function(){
    $('.close').click(function(){
		$(this).parent().hide( "slow");
    });
    $('.check-all').click(function(){
    	if(this.checked) { // check select status
            $('.allCategories').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.allCategories').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
});

function applySelected()
{
	var task = document.getElementById('action').value;
	eval(task);
}
function enableCategory()
{
    var all = document.getElementsByName('allCategories');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('Please choose an category');
    }
    window.location.href = '{{$APP_BASE_URL}}newlater/admin/enable-category/gid/' + tmp;
}

function disableCategory()
{
    var all = document.getElementsByName('allCategories');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('Please choose an category');
    }
    window.location.href = '{{$APP_BASE_URL}}newlater/admin/disable-category/gid/' + tmp;
}

function deleteCategory()
{
    var all = document.getElementsByName('allCategories');
    var tmp = '';
    var count = 0;
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
             count++;
        }
    }
    if ('' == tmp) {
        alert('Please choose an category');
        return;
    } else {
    	result = confirm('Are you sure you want to delete ' + count + ' category(s)?');
        if (false == result) {
            return;
        }
    }
    window.location.href = '{{$APP_BASE_URL}}newlater/admin/delete-category/gid/' + tmp;
}


function deleteACategory(id)
{
    result = confirm('Are you sure you want to delete this category?');
    if (false == result) {
        return;
    }
    window.location.href = '{{$APP_BASE_URL}}newlater/admin/delete-category/gid/' + id;
}
</script>