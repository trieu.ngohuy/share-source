<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<div class="page-header">
    <h1>
        {{l}}Newsletter{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}Send Newsletter{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">
                <div class="col-sm-12 widget-container-col ui-sortable">

                    <div class="tabbable tabs-left">

                        <form action="" method="post" class="form-horizontal">
                            <div class="tab-content">
                                <div class="form-group has-info">
                                    <label class="control-label col-md-2">{{l}}Subject{{/l}}</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="data[subject]"/>
                                    </div>
                                </div>
                                <div class="form-group has-info">
                                    <label class="control-label col-md-2">{{l}}Content{{/l}}</label>
                                    <div class="col-md-10">
                                        <textarea style="float:left;" class="text-input textarea ckeditor"  name="data[content]" rows="20" cols="90"></textarea>
                                    </div>
                                </div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-2 col-md-10">
                                        <div class="col-md-6 text-left">
                                            <button class="btn btn-default" type="button" onclick="window.location.href='manage-newsletter'">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Back{{/l}}
                                            </button>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Send{{/l}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /.span -->
        </div><!-- /.row -->
    </div>
</div>
			