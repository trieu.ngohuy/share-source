<?php

require_once 'modules/content/models/Content.php';
require_once 'modules/newlater/models/NewlaterCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/user/models/User.php';
require_once 'modules/mail/models/Mail.php';
require_once 'modules/mail/models/MailStore.php';

class newlater_AdminController extends Nine_Controller_Action_Admin {

    public function sendNewsletterAction() {
        $objCategory = new Models_NewlaterCategory();
        $toUsers = $objCategory->getAllCategories();
        $data = $this->_getParam('data', false);
        if (false != $data) {
            foreach ($toUsers as $toUser) {
                /* Start send mail */
                try {
                    $subject = $data['subject'];
                    $data['body'] = $data['content'];
                    unset($data['subject']);
                    unset($data['content']);
                    $objMail = new Models_Mail();
                    $objMail->sendHtmlMail('newsletter', $data, $toUser['name'], $subject);
                } catch (Exception $e) {
                    echo '<pre>';
                    echo print_r($e);
                    echo '<pre>';
                    die;
                }
                /* End */
            }
            $this->session->categoryMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("Đã gửi email cho các thành viên.")
            );
            $this->_redirect('newlater/admin/manage-category');
        }
        $this->view->menu = array(
            0 => 'newlater',
            1 => 'manager-newlater',
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-globe',
                'url' => Nine_Registry::getBaseUrl() . 'admin/newlater/admin/manage-category',
                'name' => Nine_Language::translate('Manager Category Newlater')
            )
        );
    }

    /*
     * Manager news letter
     */
    public function manageCategoryAction() {
        //Set title
        $this->view->headTitle(Nine_Language::translate('Manage NewsLetter'));
        
        /*
         * Paging
         */
        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
    	if($currentPage == false){
        	$currentPage = 1;
        	$this->session->contactDisplayNum = null;
        	$this->session->contactCondition = null;
        }
        if (false === $displayNum) {
            $displayNum = $this->session->contactDisplayNum;
        } else {
            $this->session->contactDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }

        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        $tmpCondition = $condition;
        if (false !== $condition) {
            $currentPage = 1;
        }
        if (false == $condition || $condition['email'] === '') {
            $condition = array();
            $tmpCondition = array();
        }else{
            //Adjust condition
            $tmpCondition = array(
                'email LIKE ?' => '%'.$condition['email'].'%'
            );
        }
        
        /**
         * Get all contacts
         */
        $numRowPerPage= $numRowPerPage;
        $allContact = Nine_Read::GetListData('Models_NewlaterCategory', $tmpCondition, array('newlater_category_id ASC'),'index',
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage
                                                  );

        /**
         * Count all contacts
         */
        $count = count(Nine_Read::GetListData('Models_NewlaterCategory', $tmpCondition));

        /**
         * Set values for tempalte
         */
//        echo print_r($condition); die;
        $this->setPagination($numRowPerPage, $currentPage, $count);
        
        $this->view->allNewsLetter = $allContact;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;

        $this->view->menu = array(
        	0=>'newsletter',
        	1=>'manager-newsletter'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-contact-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/newlater/admin/manage-category',
        		'name'	=>	Nine_Language::translate('Manager NewsLetter')
        		)

        );
    }

    public function newCategoryAction() {
        $objLang = new Models_Lang();
        $objCategory = new Models_NewlaterCategory;
        $objContent = new Models_Content();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_newlater', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data = $this->_getParam('data', false);
        /**
         * Get all categories
         */
        $allCats = $objCategory->buildTree($objCategory->getAll(array('sorting ASC'))->toArray());
//        echo "<pre>";print_r($allCats);die; 
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_newlater', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCategory = $data;
            $newCategory['created_date'] = time();
            /**
             * Sorting
             */
            if (null == $newCategory['sorting']) {
                unset($newCategory['sorting']);
            }
            if (false == $this->checkPermission('new_newlater', null, '*')) {
                $newCategory['genabled'] = 0;
                $newCategory['sorting'] = 1;
            }


            /**
             * Remove empty images
             */
            if (is_array($newCategory['images'])) {
                foreach ($newCategory['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newCategory['images'][$index]);
                    } else {
                        $newCategory['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newCategory['images'] = implode('||', $newCategory['images']);


            foreach ($allLangs as $index => $lang) {
                if ($newCategory[$lang['lang_id']]['alias'] == "") {
                    $newCategory[$lang['lang_id']]['alias'] = $objContent->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
                    $newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", $newCategory[$lang['lang_id']]['alias']));
                }
            }
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > @$newCategory['sorting']) {
                    $newCategory['sorting'] = 1;
                }
                if (null == $newCategory['parent_id']) {
                    $newCategory['parent_id'] = NULL;
                }
                $objCategory->increaseSorting($newCategory['sorting'], 1);

                $gid = $objCategory->insert($newCategory);

                /**
                 * Update id string
                 */
                $objCategory->update(array('gid_string' => $gid), array('newlater_category_gid = ?' => $gid));
                $category = @reset($objCategory->getByColumns(array('newlater_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);
                /**
                 * Message
                 */
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is created successfully.')
                );

                $this->_redirect('newlater/admin/manage-category');
            } catch (Exception $e) {
                echo '<pre>';
                echo print_r($e);
                echo '<pre>';
                die;
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Category'));
        $this->view->fullPermisison = $this->checkPermission('new_newlater', null, '*');

        $this->view->menu = array(
            0 => 'setting',
            1 => 'newlater',
            2 => 'new-newlater'
        );

        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-globe',
                'url' => Nine_Registry::getBaseUrl() . 'admin/newlater/admin/manage-category',
                'name' => Nine_Language::translate('Manager Category Newlater')
            ),
            1 => array(
                'icon' => 'fa-plus',
                'url' => '',
                'name' => Nine_Language::translate('New Category Newlater')
            )
        );
    }

    public function editCategoryAction() {
        $objCategory = new Models_NewlaterCategory();
        $objLang = new Models_Lang();
        $objContent = new Models_Content();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_newlater', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        if (false == $gid) {
            $this->_redirect('newlater/admin/manage-category');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_newlater', null, '*')) || (false != $lid && false == $this->checkPermission('edit_newlater', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data = $this->_getParam('data', false);

        /**
         * Get all categories
         */
        $allCats = $objCategory->buildTree($objCategory->getAll(array('sorting ASC'))->toArray());

        /**
         * Get all category languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allCategoryLangs = $objCategory->setAllLanguages(true)->getByColumns(array('newlater_category_gid=?' => $gid))->toArray();
//        echo "<pre>";print_r($allCategoryLangs);die; 
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_newlater', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allCategoryLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCategory = $data;
            /**
             * Sorting
             */
            if (null == $newCategory['sorting']) {
                unset($newCategory['sorting']);
            }
            if (false == $this->checkPermission('new_newlater', null, '*')) {
                unset($newCategory['genabled']);
                unset($newCategory['sorting']);
            }

            /**
             * Remove empty images
             */
            if (is_array($newCategory['images'])) {
                foreach ($newCategory['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newCategory['images'][$index]);
                    } else {
                        $newCategory['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newCategory['images'] = implode('||', $newCategory['images']);


            foreach ($allLangs as $index => $lang) {
                if ($newCategory[$lang['lang_id']]['alias'] == "") {
                    $newCategory[$lang['lang_id']]['alias'] = $objContent->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
                    $newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", $newCategory[$lang['lang_id']]['alias']));
                }
            }
            try {
                /**
                 * Update
                 */
                if (null == $newCategory['parent_id']) {
                    $newCategory['parent_id'] = NULL;
                }
                /**
                 * Delete gid in parent
                 */
                $oldCategory = @reset($allCategoryLangs);
//                echo "<pre>";print_r($oldCategory);die; 
                $objCategory->deleteGidString($oldCategory['parent_id'], $oldCategory['gid_string']);

                /**
                 * Update new data
                 */
                $objCategory->update($newCategory, array('newlater_category_gid=?' => $gid));

                /**
                 * Update new id string
                 */
                $category = @reset($objCategory->getByColumns(array('newlater_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);

                /**
                 * Message
                 */
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is updated successfully.')
                );

                $this->_redirect('newlater/admin/manage-category');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allCategoryLangs);
            if (false == $data) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("Category doesn't exist.")
                );
                $this->_redirect('newlater/admin/manage-category');
            }
            /**
             * Format image
             */
            $data['images'] = explode('||', $data['images']);
            if (!is_array($data['images'])) {
                $data['images'] = array();
            }
            $data['images'] = array_pad($data['images'], 50, null);
            /**
             * Get all lang categorys
             */
            foreach ($allCategoryLangs as $category) {
                $data[$category['lang_id']] = $category;
            }

            /**
             * Get all child category
             */
            $allChildCats = explode(',', trim($data['gid_string'], ','));
            unset($allChildCats[0]);
            foreach ($allCats as $key => $item) {
                if (false != in_array($item, $allChildCats)) {
                    unset($allCats[$key]);
                }
            }
        }
        /**
         * Remove it and its childs from category list
         */
        $oldData = @reset($allCategoryLangs);
        foreach ($allCats as $index => $item) {
            if (false !== strpos(",{$oldData['gid_string']},", ",{$item['newlater_category_gid']},")) {
                unset($allCats[$index]);
            }
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Category'));
        $this->view->fullPermisison = $this->checkPermission('edit_newlater', null, '*');
        $this->view->menu = array(
            0 => 'setting',
            1 => 'newlater',
            2 => 'manager-newlater'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-globe',
                'url' => Nine_Registry::getBaseUrl() . 'admin/newlater/admin/manage-category',
                'name' => Nine_Language::translate('Manager Category Newlater')
            ),
            1 => array(
                'icon' => 'fa-pencil',
                'url' => '',
                'name' => Nine_Language::translate('Edit Category Newlater')
            )
        );
    }

    public function enableCategoryAction() {
        $objCategory = new Models_NewlaterCategory;
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);

        if (false == $gid) {
            $this->_redirect('newlater/admin/manage-category');
        }

        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_newlater', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objCategory->update(array('genabled' => 1), array('newlater_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is enable successfully')
                );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                );
            }
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_newlater', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objCategory->update(array('enabled' => 1), array('newlater_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is enable successfully')
                );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                );
            }
        }


        $this->_redirect('newlater/admin/manage-category');
    }

    public function disableCategoryAction() {
        $objCategory = new Models_NewlaterCategory;
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);

        if (false == $gid) {
            $this->_redirect('newlater/admin/manage-category');
        }

        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_newlater', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objCategory->update(array('genabled' => 0), array('newlater_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is disable successfully')
                );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                );
            }
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_newlater', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objCategory->update(array('enabled' => 0), array('newlater_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Category is disable successfully')
                );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                );
            }
        }


        $this->_redirect('newlater/admin/manage-category');
    }

    public function deleteCategoryAction() {
        $objCategory = new Models_NewlaterCategory;
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_newlater')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid = $this->_getParam('gid', false);

        if (false == $gid) {
            $this->_redirect('newlater/admin/manage-category');
        }

        $gids = explode('_', trim($gid, '_'));

        try {
            foreach ($gids as $gid) {

                $cat = @reset($objCategory->getByColumns(array('newlater_category_gid=?' => $gid))->toArray());
                if (0 == $cat['newlater_deleteable']) {
                    $this->session->categoryMessage = array(
                        'success' => false,
                        'message' => Nine_Language::translate('Can NOT delete category (' . $gid . '). Please try again')
                    );
                    $this->_redirect('newlater/admin/manage-category');
                } else {
                    $objCategory->delete(array('newlater_category_gid=?' => $gid));
                }
            }
            $this->session->categoryMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Category is deleted successfully')
            );
        } catch (Exception $e) {
            $this->session->categoryMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT delete this category. Please try again')
            );
        }
        $this->_redirect('newlater/admin/manage-category');
    }

    public function changeStringAction() {
        $objContent = new Models_Content();
        $str = $this->_getParam("string", "");
        $str = $objContent->convert_vi_to_en($str);
        $str = str_replace(" ", "-", trim($str));
        echo $str;
        die;
    }
/*
     * Save news letter
     */

    public function saveAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        try {
            //Check if data submit            
            $data = $this->_getParam('data', false);                 
            if ($this->getRequest()->isPost() && $data !== false) {
                $obj = new Models_NewlaterCategory();
                $data['name'] = $data['email'];
                $data['alias'] = Nine_Common::makeUpAlias($data['email']);
                $data['created_date'] = time();
                $obj->insert($data);
                echo json_encode(array(
                    'status' => true
                ));
            }
        } catch (Exception $exc) {
            echo json_encode(array(
                'status' => false,
                'message' => $exc->getMessage() . '<br>' . $exc->getTraceAsString()
            ));
        }
    }
}
