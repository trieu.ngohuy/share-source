            <h2>{{l}}Manage Feedbacks{{/l}}</h2>
            
             <!-- End .shortcut-buttons-set -->
            
            <div class="clear"></div> <!-- End .clear -->
           
            
            <div class="content-box"><!-- Start Content Box -->
                
                <div class="content-box-header">
                    
                    <div style="float:left;">
                        <a name="listoffeedback"><h3>{{l}}All Feedbacks{{/l}}</h3></a>
                   </div>     
                   <div style="float:right;padding-right:20px;padding-top:5px;">
                        <form class="search" name="search" method="post" action="{{$APP_BASE_URL}}feedback/admin/manage-feedback">
                            
                          Name:  <input class="text-input small-input" type="text" name="condition[customer]" id="listcustomer" value="{{$condition.customer}}"/>
                            	<select name="condition[category]">
	                                <option value="">{{l}}All categories{{/l}}</option>
		                            {{foreach from=$categories item=item}}
		                                <option value="{{$item.list_id}}" {{if $condition.category == $item.list_id}}selected="selected"{{/if}}>{{$item.name}}</option>
		                            {{/foreach}}
	                            </select> 
                            <input class="button" type="submit" value="Search" />
                        </form>
                        
                   </div>
                    
                </div> <!-- End .content-box-header -->
                
                <div class="content-box-content">
                    
                    <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
                        
                       
                       
                        <!-- MESSAGE HERE -->
                        {{if $allFeedbacks|@count <= 0}}
                        <div class="notification information png_bg">
                            <a href="#" class="close"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                            <div>
                                {{l}}No feedback with above condition.{{/l}}
                            </div>
                        </div>
                        {{/if}}
                        
                        {{if $feedbackMessage|@count > 0 && $feedbackMessage.success == true}}
                        <div class="notification success png_bg">
                            <a href="#" class="close"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                            <div>
                                {{$feedbackMessage.message}}
                            </div>
                        </div>
                        {{/if}}
                        
                        {{if $feedbackMessage|@count > 0 && $feedbackMessage.success == false}}
                        <div class="notification error png_bg">
                            <a href="#" class="close"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                            <div>
                                {{$feedbackMessage.message}}
                            </div>
                        </div>
                        {{/if}}
                        
                        <!-- END MESSAGE -->
                        
                        
                        
                        
                        {{if $allFeedbacks|@count > 0}}
                        <table>
                            <thead>
                                <tr>
                                    <th><input class="check-all" type="checkbox" /></th>
                                   <th>ID</th>
                                   <th>{{l}}Customer{{/l}}</th>
                                   <th>{{l}}Question{{/l}}</th>
                                   <th>{{l}}Created date{{/l}}</th>
                                   <th>{{l}}Category{{/l}}</th>
                                   <th>{{l}}Enabled{{/l}}</th>
                                   <th>{{l}}Action{{/l}}</th>
                                </tr>
                                
                            </thead>
                         
                         
                            <tbody>
                            
                            {{foreach from=$allFeedbacks item=item}}
                                <tr>
                                 <td><input type="checkbox" value="{{$item.feedback_id}}" name="allFeedbacks"/></td>
                                    <td>{{$item.feedback_id}}</td>
                                    <td>
                                        <b>{{$item.customer}} ({{$item.telephone_no}})</b> <br />
                                    	{{$item.email}}
                                    </td>
                                    <td>
                                        {{$item.s_question}}
                                    </td>
                                    <td>
                                    	{{$item.created_date}}
                                    </td>
                                    <td>
                                    	{{$item.category_name}}
                                    </td>
                                    <td class="center">
                                        {{if $item.enabled == '1'}}
                                            <a href="{{$APP_BASE_URL}}feedback/admin/disable-feedback/id/{{$item.feedback_id}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png"></a>
                                        {{else}}
                                            <a href="{{$APP_BASE_URL}}feedback/admin/enable-feedback/id/{{$item.feedback_id}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png"></a>
                                        {{/if}}
                                    </td>
                                    <td class="center">
                                    	<a href="{{$APP_BASE_URL}}feedback/admin/edit-feedback/id/{{$item.feedback_id}}" title="Edit item"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/pencil.png"  alt="Edit item" /></a>
                                    	<a href="javascript:deleteAFeedback({{$item.feedback_id}});" title="Delete item"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/cross.png"  alt="Delete item" /></a>
                                    </td>
                                </tr>
                            {{/foreach}}  
                              
                            </tbody>
                        </table>
                        
                        <br/>
                        <div class="bulk-actions align-left" style="float: left;padding-top: 14px;">
                            <select id="action">
                                <option value=";">{{l}}Choose an action...{{/l}}</option>
                                <option value="deleteFeedback();">{{l}}Delete{{/l}}</option>
                                <option value="enableFeedback();">{{l}}Enable{{/l}}</option>
                                <option value="disableFeedback();">{{l}}Disable{{/l}}</option>
                            </select>
                            <a class="button" href="javascript:applySelected();">{{l}}Apply to selected{{/l}}</a>
                        </div>
                        <div class="bulk-actions align-left" style="float: left;padding-top: 10px; padding-left:30px;">
                            <form class="search" name="search" method="post" action="{{$APP_BASE_URL}}list/admin/manage-list">
                                {{l}}Display {{/l}}#
                                <select name="displayNum" onchange="this.parentNode.submit();" style="clear: both;">
                                    <option value="5" {{if $displayNum == 5}} selected="selected" {{/if}}>5</option>
                                    <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
                                    <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
                                    <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
                                    <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
                                    <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
                                </select>
                            </form>
                        </div>
                        
                        
                        
                        <!-- PAGINATION -->
                        {{if $countAllPages > 1}}
                        <div class="pagination" style="float: right;">
                            {{if $first}}
                            <a href="?page=1" class="number" title="First Page">&laquo; First</a>
                            {{/if}}
                            {{if $prevPage}}
                            <a href="?page={{$prevPage}}" class="number" title="Previous Page">&laquo;</a>
                            {{/if}}
                            
                            {{foreach from=$prevPages item=item}}
                            <a href="?page={{$item}}" class="number" title="{{$item}}">{{$item}}</a>
                            {{/foreach}}
                            
                            <a href="#" class="number current" title="{{$currentPage}}">{{$currentPage}}</a>
                            
                            {{foreach from=$nextPages item=item}}
                            <a href="?page={{$item}}" class="number" title="{{$item}}">{{$item}}</a>
                            {{/foreach}}
                            
                            {{if $nextPage}}
                            <a href="?page={{$nextPage}}" class="number" title="Next Page">&raquo;</a>
                            {{/if}}
                            {{if $last}}
                            <a href="?page={{$countAllPages}}" class="number" title="Last Page">Last &raquo;</a>
                            {{/if}}
                        </div> <!-- End .pagination -->
                        {{/if}}
                    {{/if}}    
                        
                        <div class="clear"></div>
                    </div> <!-- End #tab1 -->
                    
                    
                </div> <!-- End .content-box-content -->
                
            </div> <!-- End .content-box -->
            
            
            <div class="clear"></div>
            
<script language="javascript" type="text/javascript">
$(document).ready(function(){
    document.getElementById('listcustomer').select();
    document.getElementById('listcustomer').focus();
});

function applySelected()
{
    var task = document.getElementById('action').value;
    eval(task);
}
function enableFeedback()
{
    var all = document.getElementsByName('allFeedbacks');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('{{l}}Please choose an feedback{{/l}}');
    }
    window.location.href = '{{$APP_BASE_URL}}feedback/admin/enable-feedback/id/' + tmp;
}

function disableFeedback()
{
    var all = document.getElementsByName('allFeedbacks');
    var tmp = '';
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
        }
    }
    if ('' == tmp) {
        alert('{{l}}Please choose an feedback{{/l}}');
    }
    window.location.href = '{{$APP_BASE_URL}}feedback/admin/disable-feedback/id/' + tmp;
}

function deleteFeedback()
{
    var all = document.getElementsByName('allFeedbacks');
    var tmp = '';
    var count = 0;
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
             tmp = tmp + '_' + all[i].value;
             count++;
        }
    }
    if ('' == tmp) {
        alert('{{l}}Please choose an feedback{{/l}}');
        return;
    } else {
    	result = confirm('{{l}}Are you sure you want to delete{{/l}} ' + count + ' {{l}}feedback(s){{/l}}?');
        if (false == result) {
            return;
        }
    }
    window.location.href = '{{$APP_BASE_URL}}feedback/admin/delete-feedback/id/' + tmp;
}


function deleteAFeedback(id)
{
    result = confirm('{{l}}Are you sure you want to delete this feedback{{/l}}?');
    if (false == result) {
        return;
    }
    window.location.href = '{{$APP_BASE_URL}}feedback/admin/delete-feedback/id/' + id;
}
</script>
