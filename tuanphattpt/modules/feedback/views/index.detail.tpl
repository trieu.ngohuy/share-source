 <div class="w700 float_left">
        <div class="bg_tt1">
          <p class="tt p5t">{{l}}Question{{/l}} & {{l}}Answer{{/l}}</p>
        </div>
        
        <div class="box_news bg_gray"> {{if $feedback.main_image}}<img src="{{$BASE_URL}}{{$feedback.main_image}}" width="199" />{{/if}}<span >{{l}}Question{{/l}}:</span>
          <p></p>
          {{$feedback.question}}
          <div class="cb"></div>
        </div>
        
        <div class="box_news p5b p5t boder_all"><span >{{l}}Answer{{/l}}:</span>
          <p></p>
          {{$feedback.answer}}
          <div class="cb"></div>
        </div>
        
      </div>