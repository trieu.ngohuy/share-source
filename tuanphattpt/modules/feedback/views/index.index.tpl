<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}front/js/jquery.validate.js"></script>
<script type="text/javascript">
$().ready(function() {
    // validate signup form on keyup and submit
    $("#sendfeedback").validate({
        rules: {
            'data[customer]': "required",
            'data[question]': "required",
            'data[captcha]': "required"
        },
        messages: {
            'data[customer]': "",
            'data[question]': "",
            'data[captcha]': ""
        }
    });
});

function submitForm()
{
	$('#sendfeedback').submit();
}
</script>

<div class="w700 float_left">
        <div class="bg_tt1">
          <p class="tt p5t">{{l}}Question & Answer{{/l}}</p>
        </div>
        <div class=" p5t m10b">
        	<!-- PANIVIGATION -->
        	{{if $countAllPages > 1}}
	          <div class="pages float_right">
	            <ul>
	              {{if $first}}
	              	<li><a href="?page=1"><img src="{{$LAYOUT_HELPER_URL}}front/images/bulet_left1.png"/></a></li>
	              {{/if}}
	              {{if $prevPage}}
	              <li><a href="?page={{$prevPage}}"><img src="{{$LAYOUT_HELPER_URL}}front/images/bulet_left1_1.png"/></a></li>
	              {{/if}}
	              {{foreach from=$prevPages item=item}}
	              	<li><a href="?page={{$item}}">{{$item}}</a></li>
	              {{/foreach}}
	              <li class="current"><a href="#">{{$currentPage}}</a></li>
	              
	              {{foreach from=$nextPages item=item}}
                  	<li><a href="?page={{$item}}">{{$item}}</a></li>
                  {{/foreach}}
	              
	              {{if $nextPage}}
	              	<li><a href="#"><img src="{{$LAYOUT_HELPER_URL}}front/images/bulet_right1_1.png"/></a></li>
	              {{/if}}
	              {{if $last}}
	              	<li><a href="#"><img src="{{$LAYOUT_HELPER_URL}}front/images/bulet_right1.png"/></a></li>
	              {{/if}}
	            </ul>
	          </div>
	        <!-- END PANAVIGATION -->
          	{{/if}}
          <div class="cb"></div>
        </div>
        
        {{foreach from=$allFeedbacks item=item}}
        	<div class="box_news bg_gray"> {{if null != $item.main_image}} <a href="#"><img src="{{$BASE_URL}}{{$item.main_image}}" width="199"/></a>{{/if}}<span >{{l}}Question{{/l}}:</span>
          	<p></p>
          	{{$item.question}}
          	<div class="cb"></div>
        	</div>
	        <div class="box_news p5b p5t boder_all"></a><span >{{l}}Answer{{/l}}:</span>
	          <p></p>
	          {{$item.answer}}
	          <span><a href="{{$item.url}}">{{l}}Detail{{/l}}</a></span>
	          <div class="cb"></div>
	         
	        </div>
        {{/foreach}}
        {{if $feedbackMessage|@count > 0}}
        	<div class="box_news p5b p5t boder_all">
          	<p></p>
          	{{$feedbackMessage.message}}
          	<div class="cb"></div>
        	</div>
        {{/if}}
        <form id="sendfeedback" method="post" action="{{$BASE_URL}}feedback/index/send-feedback">
        <div class="box_comment m10b m10t">
	        <label>{{l}}Name{{/l}}<span style="color:red">*</span></label>
	        <input class="txt_comment w495" name="data[customer]" type="text" /><br class="cb" />
	        <label>{{l}}E-mail{{/l}} </label>
	        <input class="txt_comment w495" name="data[email]" type="text" />
	        <label>{{l}}Telephone No{{/l}} </label>
	        <input class="txt_comment w495" name="data[telephone_no]" type="text" />
	        <br class="cb" />
	        <label>{{l}}Address{{/l}} </label>
	        <input class="txt_comment w495" name="" type="text" /><br class="cb" />
	        <label>{{l}}Question{{/l}}<span style="color:red">*</span> </label>
	        <textarea name="data[question]" class=" w495" cols="" rows=""></textarea><br class="cb" />
	        <label>{{l}}Code{{/l}}<span style="color:red">*</span></label>
	        <input class="txt_comment w115" name="data[captcha]" type="text" /><span class="float_left m5l"><img src="{{$LAYOUT_HELPER_URL}}front/captcha/create_image.php?r={{$randomNumber}}" style="padding-top:4px;" /></span><br class="cb" />
	        <label>&nbsp;</label><input onclick="javascript:submitForm();" name="" type="image" src="{{$LAYOUT_HELPER_URL}}front/images/bt_gui_{{$LANG_CODE}}.png" /><br class="cb" />
	       
        </div>
        </form>
        
      </div>