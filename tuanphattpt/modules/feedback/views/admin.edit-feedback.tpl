<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script> 
<script type="text/javascript">

function chooseImage()
{
    // You can use the "CKFinder" class to render CKFinder in a page:
    var finder = new CKFinder();
    finder.basePath = '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/'; // The path for the installation of CKFinder (default = "/ckfinder/").
    finder.selectActionFunction = SetFileField;
    finder.popup();

    // It can also be done in a single line, calling the "static"
    // Popup( basePath, width, height, selectFunction ) function:
    // CKFinder.Popup( '../../', null, null, SetFileField ) ;
    //
    // The "Popup" function can also accept an object as the only argument.
    // CKFinder.Popup( { BasePath : '../../', selectActionFunction : SetFileField } ) ;
} 
// This is a sample function which is called when a file is selected in CKFinder.
function SetFileField( fileUrl )
{
    document.getElementById( 'chooseImage_img' ).src = fileUrl;
    document.getElementById( 'chooseImage_input' ).value = fileUrl;
    document.getElementById( 'chooseImage_div').style.display = '';
    document.getElementById( 'chooseImage_noImage_div' ).style.display = 'none';
}

function clearImage()
{
    document.getElementById( 'chooseImage_img').src = '';
    document.getElementById( 'chooseImage_input' ).value = '';
    document.getElementById( 'chooseImage_div' ).style.display = 'none';
    document.getElementById( 'chooseImage_noImage_div').style.display = '';
}
</script>

		<form method="post" action="">
		<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>{{l}}Feedback box{{/l}}</h3>
					
					<ul class="content-box-tabs">
						<li><a href="#tab1" class="default-tab">Feedback</a></li> <!-- href must be unique and match the id of target div -->
						{{p name='edit_feedback' module='feedback'}}
						<li><a href="#tab2">Answer</a></li>
						{{/p}}
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					<div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
						
						<!-- Feedback Information -->
						<fieldset>
							
							<p>
								<label><strong>{{l}}Name: {{/l}}</strong></label>	<strong>{{$feedback.customer}}</strong>
							</p>
							<p>
								<label><strong>{{l}}Created date: {{/l}}</strong></label>	{{$feedback.created_date}}
							</p>
							
							
							{{p name='edit_feedback' module='feedback'}}
							<p>
								<input type="hidden" id="chooseImage_input" name="feedback[image]" value="{{$feedback.image}}">
								<div id="chooseImage_div" style="{{if !$feedback.image}} display:none{{/if}}">
                                                <img src="{{$BASE_URL}}{{$feedback.image}}" id="chooseImage_img" style="max-width: 150px; border:dashed thin;"></img><br/>
                               </div>
                               <div id="chooseImage_noImage_div" style="width: 150px; border: thin dashed; text-align: center; padding:70px 0px; {{if $feedback.image}} display:none{{/if}} ">
                               	No image
                               </div>
                               <br/>
                               <a href="javascript:chooseImage();">Choose image</a>
                               | 
                               <a href="javascript:clearImage();">Delete</a>
							</p>
							
							<p>
									<label><strong>{{l}}Categories: {{/l}}</strong></label>      
									<span>
										<select id="feedback[category]" name="feedback[category]" class="small-input">
											{{foreach from=$categories item=item}}
												<option {{if $feedback.category == $item.list_id}} selected="selected" {{/if}} value="{{$item.list_id}}">{{$item.name}}</option>
											{{/foreach}}
										</select> 
									</span>
							</p>
							{{/p}}
							
							<p>
								<strong>{{l}}Question: {{/l}}</strong>
							</p>
							<p>
								<textarea class="text-input textarea wysiwyg" id="uestion]" name="question"cols="79" rows="15" readonly="readonly">{{$feedback.question}}</textarea>
							</p>
						</fieldset>
						<!-- End feedback Information -->
						<!-- Comment -->
						<fieldset>
							<p><strong>{{l}}Comments{{/l}}</strong></p>
	                        {{if $allComments|@count > 0}}
	                        	
	                        		{{foreach from=$allComments item=item}}
	                        			<p>
	                        				<font style="font-weight: bold;{{if $item.color}} color: {{$item.color}}; {{/if}} ">{{$item.full_name}}</font> ({{$item.username}}) - <span>{{$item.created_date}}</span></p>
	                        			<p>
	                        				<textarea class="text-input textarea wysiwyg" id="textarea" name="textfield{{$item.feedback_comment_id}}"cols="79" rows="5" readonly="readonly">{{$item.comment}}</textarea>
	                        			</p>
	                        		{{/foreach}}
	                        	
	                        {{/if}}
	                        
	                        		<p><textarea class="text-input textarea wysiwyg" id="comment[message]" name="comment[message]" cols="79" rows="5"></textarea></p>
	                        		<p>
										<input class="button" type="button" value="&laquo; {{l}}Back{{/l}}" onclick="javascript:history.back();" />
										<input class="button" type="submit" value="{{l}}Send{{/l}}" />
									</p>
	                        
                        </fieldset>
						<!-- Comment -->
						
					</div> <!-- End #tab1 -->
					
					<div class="tab-content hidden" id="tab2">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p>
									<strong>{{l}}Enter your answer {{/l}}</strong>
								</p>
								<p>
									<textarea class="text-input textarea wysiwyg" id="feedback[answer]" name="feedback[answer]" cols="79" rows="15">{{$feedback.answer}}</textarea>
								</p>
								<p>
									<strong>{{l}}Finished {{/l}}</strong>
									<input type="radio" name="feedback[finished]" {{if 1 == $feedback.finished }} checked="checked" {{/if}} value="1"/><br />
								</p>
								<p>
									<input class="button" type="button" value="&laquo; {{l}}Back{{/l}}" onclick="javascript:history.back();" />
									<input class="button" type="submit" value="{{l}}Send{{/l}}" />
								</p>
								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
					</div> <!-- End #tab2 -->        
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
		</form>