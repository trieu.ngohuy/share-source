<?php
require_once 'modules/feedback/models/Feedback.php';
require_once 'modules/mail/models/Mail.php';
require_once 'modules/user/models/User.php';

class feedback_IndexController extends Nine_Controller_Action
{
	public function indexAction()
	{
		$this->view->headTitle(Nine_Language::translate('Feedbacks'));
		$this->view->menuId = "feedback";
		
		$config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultFeedbackNumberRowPerPage");
        $currentPage = $this->_getParam("page",1);
		
        $condition = array(
        				'finished' => 1, 
        				'enabled' => 1
        				);
		/**
		 * Get all feedback
		 */
		$objFeedback = new Models_Feedback();
		$allFeedbacks = $objFeedback->getAllFeedbacks($condition , 'feedback_id DESC',
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage);
        
        /**
         * Modified all feedback
         */
        foreach($allFeedbacks as &$feedback){
        	
        	if (null != @$feedback['image']) {
        		$feedback['main_image'] = Nine_Function::getThumbImage(trim($feedback['image']),199);
        	}
        	$feedback['question'] = Nine_Function::subStringAtBlank(trim(strip_tags($feedback['question'])),345);
        	$feedback['answer'] = Nine_Function::subStringAtBlank(trim(strip_tags($feedback['answer'])),145);
        	$feedback['url'] = Nine_Route::_("feedback/index/detail/id/{$feedback['feedback_id']}",array('alias' => "feedback"));
        }
        unset($feedback);                                           
        
        /**
         * Count all feedback
         */
        $count = count($objFeedback->getAllFeedbacks($condition));
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allFeedbacks = $allFeedbacks;
        $this->view->feedbackMessage = $this->session->feedbackMessage;
        $this->session->feedbackMessage = null;
        
        /**
         * random number
         */
        $this->view->randomNumber = rand(0, 1000000);
        
	}
	
	public function detailAction()
	{
		
		
		$id = $this->_getParam('id',false);
		if (false == $id) {
			$this->_redirectToNotFoundPage();
		}
		
		$objFeedback = new Models_Feedback();
		$feedback = @reset($objFeedback->find($id)->toArray());
		if (null == $feedback) {
			$this->_redirectToNotFoundPage();
		}
		
		if(null != @$feedback['image']) {
			$feedback['main_image'] = Nine_Function::getThumbImage($feedback['image'],199);
		}
		$feedback['question'] = nl2br($feedback['question']);
		$feedback['answer'] = nl2br($feedback['answer']);
		
		$this->view->feedback = $feedback;
		
		$this->view->headTitle(Nine_Language::translate('Feedbacks'));
		$this->view->menuId = "feedback";
	}
	
	
	public function sendFeedbackAction(){
		$data = $this->_getParam('data',false);
		$error = array();
		if (false != $data) {
//			echo "<pre>";print_r($_SESSION);die;
			if (null != $_SESSION['captcha'] && $_SESSION['captcha'] == strtoupper(@$data['captcha'])) {

				$objFeedback = new Models_Feedback();
				try {
					$newFeedback = array(
										'customer'	=>	@$data['customer'],
										'email'		=>	@$data['email'],
										'telephone_no'	=>	@$data['telephone_no'],
										'question'	=>	@$data['question'],
										'category'	=>	8,
										'created_date'	=> time()
									);
					
					$objFeedback->insert($newFeedback);
					
					/**
					 * Send mail to admin
					 */
//					$objMail = new Models_Mail();
//
//					$mailValues = array(
//                    				'customer'	=>	$newFeedback['customer'],
//                    				'email'	=>	$newFeedback['email'],
//									'telephone_no'	=>	$newFeedback['telephone_no'],
//									'question'	=>	$newFeedback['question']
//                    			);
//					$objUser = new Models_User();
//                    $admin = @reset($objUser->getByColumns(array('username' => 'admin'))->toArray()) ; 
////                   	echo "<pre>";print_r($admin);die;
//                    $objMail->sendHtmlMail('feedback',$mailValues,$newFeedback['email']);
					
					$this->session->feedbackMessage = array(
		                                               'success' => true,
		                                               'message' => Nine_Language::translate('Feedback is send successfully')
		                                           );
		                                           
				}
				catch (Exception $e) {
					 $this->session->feedbackMessage = array(
		                                               'success' => false,
		                                               'message' => Nine_Language::translate('Can NOT send this feedback. Please try again')
		                                           );
				}
				
			}
			else {
				unset($_SESSION['captcha']);
				$this->session->feedbackMessage = array(
		                                               'success' => false,
		                                               'message' => Nine_Language::translate('Code is not match. Please type again')
		                                           );
			}
		}
		/**
		 * Redirect to feedback
		 */
		$this->_redirect('feedback');
	}
	
	
	public function sendFeedbackAjaxAction()
	{
		 /**
         * Ajax data
         */
        $this->setLayout('default');
        
        $data = $this->_getParam('data', false);
        $feedbackMessage = null;
     	if (false !== $data) {
     		parse_str($data);
     		if (null != $_SESSION['captcha'] && $_SESSION['captcha'] == strtoupper(@$data['captcha'])) {
				$objFeedback = new Models_Feedback();
				try {
					$newFeedback = array(
										'customer'	=>	@$data['customer'],
										'email'		=>	@$data['email'],
										'telephone_no'	=>	@$data['telephone_no'],
										'question'	=>	@$data['question'],
										'category'	=>	8,
										'created_date'	=> time()
									);
					
					$objFeedback->insert($newFeedback);
					$feedbackMessage = array(
		                                               'success' => true,
		                                               'message' => Nine_Language::translate('Feedback is send successfully')
		                                           );
				}
				catch (Exception $e) {
					$feedbackMessage = array(
		                                               'success' => false,
		                                               'message' => Nine_Language::translate('Can NOT send this feedback. Please try again')
		                                           );
				}
				
			}
			else {
				$feedbackMessage = array(
		                                               'success' => false,
		                                               'message' => Nine_Language::translate('Code is not match. Please type again')
		                                           );
			}
     	}
		echo json_encode($feedbackMessage);die;
	}
	private function _redirectToNotFoundPage()
	{
	    $this->_redirect("");
	}
}