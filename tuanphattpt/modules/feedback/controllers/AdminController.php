<?php

require_once 'modules/feedback/models/Feedback.php';
require_once 'modules/feedback/models/FeedbackComment.php';
require_once 'modules/list/models/List.php';
require_once 'modules/user/models/User.php';

class feedback_AdminController extends Nine_Controller_Action_Admin 
{

    public function manageFeedbackAction()
    {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_feedback')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $this->view->headTitle(Nine_Language::translate('Manage Feedbacks'));
        $this->view->menu = array('managefeedback');
        
        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",1);
        $displayNum = $this->_getParam('displayNum', false);
        
        /**
         * Get number of lists per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->feedbackDisplayNum;
        } else {
            $this->session->feedbackDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->feedbackCondition;
        } else {
            $this->session->feedbackCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        
        /**
         * Get all feedback
         */
        $objFeedback = new Models_Feedback();
        $allFeedbacks = $objFeedback->getAllFeedbacks($condition, 'feedback_id DESC',
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage);
        
        $objList = new Models_List();
        foreach ($allFeedbacks as &$item) {
        	$item['created_date'] = date($config['dateFormat'],$item['created_date']);
        	$list = $objList->getListById($item['category']);
        	$item['category_name'] = $list['name'];
        	$item['s_question'] = substr($item['question'], 0, 200).'...';
        }
        unset($item);
        /**
         * Count all lists
         */
        $count = count($objFeedback->getAllFeedbacks($condition));
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allFeedbacks = $allFeedbacks;
        $this->view->feedbackMessage = $this->session->feedbackMessage;
        $this->session->feedbackMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        
        $categories = $objList->getLists(3);
        $this->view->categories = $categories;
        
    }
    
    public function editFeedbackAction() {
    	$objFeedback = new Models_Feedback();
        $objComment = new Models_FeedbackComment();
        $objUser = new Models_User();
        
        $errors = array();
    	/**
         * Check permission
         */
        if (false == $this->checkPermission('see_feedback')) {
            $this->_forwardToNoPermissionPage();
            return;
        } 
        
        $id   = $this->_getParam('id', false);
		$comment = $this->_getParam('comment',false);
		$feedback = $this->_getParam('feedback',false);
		
        if (false == $id ) {
            $this->_redirect('feedback/admin/manage-feedback');
        }
        if (false != $comment) {
        	if (null != @$comment['message']) {
	        	$newComment = array(
	        					'feedback_id'	=>	@$id,
	        					'user_id'		=>	Nine_Registry::getLoggedInUserId(),
	        					'comment'		=>	@$comment['message'],
	        					'created_date'	=>	time()
	        				);
	        	$objComment->insert($newComment);
        	}
        }
        
        if (false != $feedback) {
        	/**
        	 * Check permission to update/reply feedback
        	 */
        	
	        if (false != $this->checkPermission('edit_feedback')) {
	        	
	        	if( null != @$feedback['image']){
	        		$feedback['image'] = Nine_Function::getImagePath($feedback['image']);
	        	}
	        	
        		$objFeedback->update($feedback, array('feedback_id=?'	=>	@$id));
	        } 
        }
        /**
         * Get feedback & all comments
         */
    	$feedback = $objFeedback->getFeedback($id);
    	$feedback['created_date'] = date('d/m/Y h:i:s A',$feedback['created_date']);
    	
        $allComments = $objComment->getAllCommentsByFeedBackId($id);
        
        foreach ($allComments as &$item) {
        	$item['created_date'] = date('d/m/Y h:i:s A',$item['created_date']);
        	$user = $objUser->getUserWithGroup($item['user_id']);
        	$item['full_name'] = $user['full_name'];
        	$item['username'] = $user['username'];
        	$item['color']	=	$user['gcolor'];
        }
        unset($item);
        
       
        
        if (empty($feedback)) {
            /**
             * Feedback doesn't exsit
             */
            
            $this->session->feedbackMessage = array(
                                           'success' => false,
                                           'message' => Nine_Language::translate('Feedback does NOT exist')
                                       );
            $this->_redirect('feedback/admin/manage-feedback#listoffeedback');
        }
        /**
         * Prepare for template
         */
        $this->view->errors = $errors;
        $this->view->feedback = $feedback;
        $this->view->allComments = $allComments;
       
        
        $this->view->headTitle(Nine_Language::translate('Edit feedback'));
        $this->view->menu = array('managefeedback', 'editfeedback');
        $this->view->feedbackMessage = $this->session->feedbackMessage;
        $this->session->feedbackMessage = null;
        
        $objList = new Models_List();
        $categories = $objList->getLists(3);
        $this->view->categories = $categories;
    }
    
    
	public function enableFeedbackAction()
    {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_feedback')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        $id = $this->_getParam('id', false);
        
        if (false == $id) {
            $this->_redirect('feedback/admin/manage-feedback');
        }
        
        $ids = explode('_', trim($id, '_'));
        
        $objFeedback = new Models_Feedback();
        try {
            foreach ($ids as $id) {
               $objFeedback->update(array('enabled' => 1), array('feedback_id=?' => $id));
            }
            $this->session->feedbackMessage = array(
                                               'success' => true,
                                               'message' => Nine_Language::translate('Feedback is activated successfully')
                                           );
        } catch (Exception $e) {
            $this->session->feedbackMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT enable this feedback. Please try again')
                                           );
        }
        $this->_redirect('feedback/admin/manage-feedback#listoffeedback');
    }

    
    
    public function disableFeedbackAction()
    {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_feedback')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $id = $this->_getParam('id', false);
        
        if (false == $id) {
            $this->_redirect('feedback/admin/manage-feedback');
        }
        
        $ids = explode('_', trim($id, '_'));
        
        $objFeedback = new Models_Feedback();
        try {
            foreach ($ids as $id) {
               $objFeedback->update(array('enabled' => 0), array('feedback_id=?' => $id));
            }
            $this->session->feedbackMessage = array(
                                               'success' => true,
                                               'message' => Nine_Language::translate('Feedback is deactived successfully')
                                           );
        } catch (Exception $e) {
            $this->session->feedbackMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT disable this feedback. Please try again')
                                           );
        }
        $this->_redirect('feedback/admin/manage-feedback#listoffeedback');
    }

    public function deleteFeedbackAction()
    {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_feedback')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $id = $this->_getParam('id', false);
        
        if (false == $id) {
            $this->_redirect('feedback/admin/manage-feedback');
        }
        
        $ids = explode('_', trim($id, '_'));
        
        $objFeedback = new Models_Feedback();
        try {
            foreach ($ids as $id) {
               $objFeedback->delete( array('feedback_id=?' => $id));
            }
            $this->session->feedbackMessage = array(
                                               'success' => true,
                                               'message' => Nine_Language::translate('Feedback is deleted successfully')
                                           );
        } catch (Exception $e) {
            $this->session->feedbackMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete this feedback. Please try again')
                                           );
        }
        $this->_redirect('feedback/admin/manage-feedback#listoffeedback');
    }
    
}