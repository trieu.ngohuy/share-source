<?php
class Route_Feedback
{
    /**
     * Build friendly URL
     * @param array $linkArr 0 => <module>, 1 => controller, 2 => action, 3...n param/value
     * @param array $params param => value
     * 
     * @return string friendly URL
     */
    public function build($linkArr, $params = array()) 
    {
    	
    	$result = implode('/', $linkArr);
    	
    	/**
    	 * Link is detail
    	 */
    	if ('detail' == @$linkArr[2]) {
    		/**
	    	 * Structure: pages/<id>/<alias>.html
	    	 */
    		$result = "feedback/pages/{$linkArr[4]}/";
	    	if (null != $params['alias']) {
	    		$result .= urlencode($params['alias']);
	    	}
    		 
    	}
    	
    	return $result;
    	
    }
    /**
     * Parse friendly URL
     */
    public function parse()
    {
         $router = Nine_Controller_Front::getInstance()->getRouter();
         $route  = new Zend_Controller_Router_Route_Regex(
                                                 'feedback/pages/([0-9]+)/(.*).html',
                                                 array(
                                                    'module'     => 'feedback',
                                                    'controller' => 'index',
                                                    'action'     => 'detail'
                                                 ),
                                                 array(1 =>'id')
                                            );
         $router->addRoute('content3', $route);  
         
    }
}