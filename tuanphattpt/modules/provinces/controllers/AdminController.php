<?php

require_once 'modules/provinces/models/Provinces.php';

class provinces_AdminController extends Nine_Controller_Action_Admin {

    public function manageProvincesAction() {
        /**
         * Check permission
         */
//        if (false == $this->checkPermission('see_provinces')) {
//            $this->_forwardToNoPermissionPage();
//            return;
//        }

        $this->view->headTitle(Nine_Language::translate('Manage Provinces'));
        $this->view->menu = array('manageprovinces');

        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page", 1);
        $displayNum = $this->_getParam('displayNum', false);

        /**
         * Get number of lists per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->provincesDisplayNum;
        } else {
            $this->session->provincesDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->provincesCondition;
        } else {
            $this->session->provincesCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }

        /**
         * Get all provinces
         */
        $objProvinces = new Models_Provinces();
        $allProvinces = $objProvinces->getByColumns($condition, 'province_id DESC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage)->toArray();
        //$objProvinces->getAllProvinces($condition, 'provinces_id DESC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
        
        /**
         * Count all lists
         */
        $count = count($objProvinces);
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allProvinces = $allProvinces;
        $this->view->provincesMessage = $this->session->provincesMessage;
        $this->session->provincesMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;

        $this->view->menu = array(
            0=>'provinces',
            1=>'manager-provinces'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon'  =>  'fa-product-hunt',
                'url'   =>  Nine_Registry::getBaseUrl().'admin/provinces/admin/manage-provinces',
                'name'  =>  Nine_Language::translate('Manager provinces')
                )
            
        );
    }

    public function newProvincesAction() {
        /**
         * Check permission
         */
        $objProvinces = new Models_Provinces();
//        if (false == $this->checkPermission('new_provinces')) {
//            $this->_forwardToNoPermissionPage();
//            return;
//        }

        $data = $this->_getParam('data', false);
        $this->view->menu = array(
            0=>'provinces',
            1=>'edit-provinces'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon'  =>  'fa-product-hunt',
                'url'   =>  Nine_Registry::getBaseUrl().'admin/provinces/admin/manage-provinces',
                'name'  =>  Nine_Language::translate('Manager provinces')
                )
            
        );
        //get list profinvces
        $allProvinces = $allProvinces = $objProvinces->getByColumns(array())->toArray();
        foreach ($allProvinces as &$value) {
            if($value['type'] == 2){
                $value['name'] = '..'.$value['name'] ;
            }
            if($value['type'] == 3){
                $value['name'] = '....'.$value['name'] ;
            }
        }
        unset($value);
        $errors = array();
        if (false !== $data) {


            try {
                $this->session->provincesMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Provinces is created successfully.')
                );

                $objProvinces->insert($data);


                $this->_redirect('provinces/admin/manage-provinces');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        }
        /**
         * Prepare for template
         */
        $this->view->errors = $errors;
        $this->view->data = $data;        
        $this->view->listData = $allProvinces;
        $this->view->headTitle(Nine_Language::translate('New provinces'));

        $this->view->menu = array(
            0 => 'provinces',
            1 => 'new-provinces'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-provinces',
                'url' => Nine_Registry::getBaseUrl() . 'admin/provinces/admin/manage-provinces',
                'name' => Nine_Language::translate('Manager Provinces')
            ),
            1 => array(
                'icon' => 'fa-provinces-plus',
                'url' => '',
                'name' => Nine_Language::translate('New Provinces')
            )
        );
    }

    public function editProvincesAction() {
        $objProvinces = new Models_Provinces();

        $errors = array();
        /**
         * Check permission
         */
//        if (false == $this->checkPermission('see_provinces')) {
//            $this->_forwardToNoPermissionPage();
//            return;
//        }

        $id = $this->_getParam('id', false);
        $data = $this->_getParam('data', false);
        $this->view->menu = array(
            0=>'provinces',
            1=>'edit-provinces'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon'  =>  'fa-product-hunt',
                'url'   =>  Nine_Registry::getBaseUrl().'admin/provinces/admin/manage-provinces',
                'name'  =>  Nine_Language::translate('Manager provinces')
                )
            
        );
        if ($data != false) {
            

            try {
                $this->session->provincesMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Provinces is created successfully.')
                );

                $objProvinces->update($data, array('province_id=?' => $id));


                $this->_redirect('provinces/admin/manage-provinces');
            } catch (Exception $e) {
                $this->view->data = $data;
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get provinces & all comments
             */
            $template = @reset($objProvinces->getByColumns(array(
                'province_id' => $id
            ))->toArray());
            //get list profinvces
            $allProvinces = $allProvinces = $objProvinces->getByColumns(array())->toArray();
            foreach ($allProvinces as &$value) {
            if($value['type'] == 2){
                $value['name'] = '..'.$value['name'] ;
            }
            if($value['type'] == 3){
                $value['name'] = '....'.$value['name'] ;
            }
        }
        unset($value);
            if (empty($template)) {
                /**
                 * Provinces doesn't exsit
                 */
                $this->session->provincesMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Provinces does NOT exist')
                );
                $this->_redirect('provinces/admin/manage-provinces#listofprovinces');
            }
            /**
             * Prepare for template
             */
            $this->view->data = $template;
            $this->view->listData = $allProvinces;
        }

        $this->view->headTitle(Nine_Language::translate('Edit provinces'));
        $this->view->provincesMessage = $this->session->provincesMessage;
        $this->view->menu = array(
            0 => 'provinces',
            1 => 'edit-provinces'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-provinces',
                'url' => Nine_Registry::getBaseUrl() . 'admin/provinces/admin/manage-provinces',
                'name' => Nine_Language::translate('Manager Provinces')
            ),
            1 => array(
                'icon' => 'fa-provinces-plus',
                'url' => '',
                'name' => Nine_Language::translate('Edit Provinces')
            )
        );
    }

    public function deleteProvincesAction() {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_provinces')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('provinces/admin/manage-provinces');
        }

        $ids = explode('_', trim($id, '_'));

        $objProvinces = new Models_Provinces();
        try {
            foreach ($ids as $id) {
                $objProvinces->delete(array('province_id=?' => $id));
            }
            $this->session->provincesMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Provinces is deleted successfully')
            );
        } catch (Exception $e) {
            $this->session->provincesMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT delete this provinces. Please try again' . $e)
            );
        }
        $this->_redirect('provinces/admin/manage-provinces#listofprovinces');
    }

}
