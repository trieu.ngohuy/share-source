<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>  
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<script type="text/javascript">
    //<![CDATA[

    jQuery(document).ready(function () {
        CKFinder.setupCKEditor(null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/');
        jQuery("#images").sortable();
        jQuery("#images").disableSelection();
        //Make slug
    {{foreach from=$allLangs item=item name=langDiv}}
        jQuery('#title{{$smarty.foreach.langDiv.iteration}}').makeSlug({
            slug: jQuery('#alias{{$smarty.foreach.langDiv.iteration}}')
        });
    {{/foreach}}
        //Display images
        jQuery(".input_image[value!='']").parent().find('div').each(function (index, element) {
            jQuery(this).toggle();
        });
    });
    var imgId;
    function chooseImage(id)
    {
        imgId = id;
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();
        finder.basePath = '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/'; // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.selectActionFunction = setFileField;
        finder.popup();
    }
    // This is a sample function which is called when a file is selected in CKFinder.
    function setFileField(fileUrl)
    {
        document.getElementById('chooseImage_img' + imgId).src = fileUrl;
        document.getElementById('chooseImage_input' + imgId).value = fileUrl;
        document.getElementById('chooseImage_div' + imgId).style.display = '';
        document.getElementById('chooseImage_noImage_div' + imgId).style.display = 'none';
    }
    function clearImage(imgId)
    {
        document.getElementById('chooseImage_img' + imgId).src = '';
        document.getElementById('chooseImage_input' + imgId).value = '';
        document.getElementById('chooseImage_div' + imgId).style.display = 'none';
        document.getElementById('chooseImage_noImage_div' + imgId).style.display = '';
    }

    function addMoreImg()
    {
        jQuery("ul#images > li.hidden").filter(":first").removeClass('hidden');
    }

//]]>
</script>
<style type="text/css">
    #images { list-style-type: none; margin: 0; padding: 0;}
    #images li { margin: 10px; float: left; text-align: center;  height: 180px;}
</style>

<div class="page-header">
    <h1>
        {{l}}Provincess{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}Edit Provincess{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">
                {{if $errors|@count > 0}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{if $errors.main}}
                    </strong> {{$errors.main}}
                    {{else}}
                    {{l}}Please check following information again{{/l}}
                    {{/if}} 
                </div>
                {{/if}}


                <div class="col-sm-12 widget-container-col ui-sortable">

                    <div class="tabbable tabs-left">

                        <ul class="nav nav-tabs" id="myTab2">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab" aria-expanded="true">
                                    <i class="pink ace-icon fa fa-home bigger-110"></i>
                                    {{l}}Basic{{/l}}
                                </a>
                            </li>
                        </ul>

                        <form action="" method="post" class="form-horizontal"  enctype="multipart/form-data">
                            <div class="tab-content">

                                <div id="tab1" class="tab-pane active ">
                                    <div class="form-group has-info">
                                        <label class="control-label col-md-2">{{l}}Parent{{/l}}</label>
                                        <div class="col-md-10">
                                            <select class="form-control" id="sel1" name="data[parent_id]">
                                                {{foreach from=$listData item = item}}
                                                    <option value="{{$item.province_id}}" {{if $data.parent_id == $item.province_id}} selected=""{{/if}}>{{$item.name}}</option>
                                                {{/foreach}}
                                              </select>
                                        </div>
                                    </div>
                                    <div class="form-group has-info">
                                        <label class="control-label col-md-2">{{l}}Provinces name{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="data[name]" value="{{$data.name}}" data-fv-notempty-message="{{l}}Name is empty{{/l}}">
                                        </div>
                                    </div>

                                    <br style="clear: both;">
                                    <div class="form-group has-info">
                                        <label class="control-label col-md-2">{{l}}Type{{/l}}</label>
                                        <div class="col-md-10">
                                            <select class="form-control" id="sel1" name="data[type]">
                                                <option value="1" {{if $data.type == 1}} selected=""{{/if}}>Tỉnh / Thành phố</option>
                                                <option value="2" {{if $data.type == 2}} selected=""{{/if}}>Quận / Huyện</option>
                                                <option value="3" {{if $data.type == 3}} selected=""{{/if}}>Phường / xã</option>
                                              </select>
                                        </div>
                                    </div>
                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Save{{/l}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /.span -->

        </div><!-- /.row -->



    </div>


</div>


<script type="text/javascript">
    jQuery(document).ready(function () {
        $('#select_avatar').click(function (event) {
            $('#ip_avatar').click();
        });

        $('#ip_avatar').change(function (event) {
            var tmppath = URL.createObjectURL(event.target.files[0]);
            $('#display_avatar').attr('src', URL.createObjectURL(event.target.files[0]));
            $("#no_avatar").css("display", "none");
            $("#avatar").css("display", "block");
        });
        $("#form-field-select-3").change(function () {

            alert('Selected value: ' + $(this).val());
        });

    });


</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPjYBFujl3KApMN7hsuWbx65tcADTkxdk&libraries=places"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        initAutocomplete();
    });

    var gmarkers = [];
    function initAutocomplete() {

//			    	var myLatLng = {lat:parseFloat($("#Event_lattitude").val()), lng: parseFloat($("#Event_longtitude").val())};

        if ($("#Event_lattitude").val() != '' && $("#Event_longtitude").val() != '') {
            var myLatLng = {lat:parseFloat($("#Event_lattitude").val()), lng: parseFloat($("#Event_longtitude").val())};
        } else {
            var myLatLng = {lat: 10.797840, lng: 106.692098};
        }

        var map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        for (i = 0; i < gmarkers.length; i++) {
            gmarkers[i].setMap(null);
        }

        var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: myLatLng
        });
        gmarkers.push(marker);

        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // [START region_getplaces]
        // Listen for the event fired when the provinces selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {


                $("#Event_lattitude").val(place.geometry.location.lat());
                $("#Event_longtitude").val(place.geometry.location.lng());




            });
            map.fitBounds(bounds);

            var myLatLng = {lat: parseFloat($("#Event_lattitude").val()), lng: parseFloat($("#Event_longtitude").val())};



            map.setZoom(14);
            map.setCenter(new google.maps.LatLng(parseFloat($("#Event_lattitude").val()), parseFloat($("#Event_longtitude").val())));

            for (i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);
            }

            var marker = new google.maps.Marker({
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: myLatLng
            });
            gmarkers.push(marker);
        });




    }

</script>