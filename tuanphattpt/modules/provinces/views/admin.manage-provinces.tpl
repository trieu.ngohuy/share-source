


<div class="page-header">
    <h1>
        {{l}}Provinces{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}List All Provinces{{/l}}
        </small>
    </h1>
</div>
<a class="btn btn-lg btn-success" href="?export=true&page=1">
    <i class="ace-icon fa fa-file-excel-o"></i>
    Export 
</a>
<br class="cb">
<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">

                {{if $allProvinces|@count <= 0}}
                <div class="alert alert-info">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{l}}No provinces with above conditions.{{/l}}
                </div>
                {{/if}}

                {{if $provincesMessage|@count > 0 && $provincesMessage.success == true}}
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$provincesMessage.message}}.
                </div>
                {{/if}}

                {{if $provincesMessage|@count > 0 && $provincesMessage.success == false}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$provincesMessage.message}}.
                </div>
                {{/if}}

                <table id="simple-table" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                    <form id="top-search" name="search" method="post" action="">
                        <th class="center">
                            <button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
                        </th>
                        <th class="center">

                            <input class="check-all" type="checkbox" />
                        </th>
                        <th>ID</th>
                        <th class="center"> 
                            {{l}}Name{{/l}}
                            <input  type="text" name="condition[name]" id="name" value="{{$condition.name}}"" placeholder="{{l}}Find Provincesname{{/l}}" class="form-control" />
                        </th>
                        <th>{{l}}Type{{/l}}</th>
                        <th>{{l}}Action{{/l}}</th>
                    </form>
                    </tr>
                    </thead>

                    <tbody>
                        {{if $allProvinces|@count > 0}}
                        {{foreach from=$allProvinces item=item key=key}}
                        <tr>
                            <td class="center">{{$key+1}}</td>
                            <td class="center">
                                <input type="checkbox" value="{{$item.provinces_id}}" name="allProvinces" class="allProvinces"/>
                            </td>
                            <td>{{$item.province_id}}</td>
                            <td>{{$item.name}}</td>
                            <td>{{if $item.type == 1}}{{l}}Tỉnh/Thành phố{{/l}}{{/if}}
                            {{if $item.type == 2}}{{l}}Quận / Huyện{{/l}}{{/if}}
                            {{if $item.type == 3}}{{l}}Phường / Xã{{/l}}{{/if}}</td>
                            <td class="center">
                                <span class="tooltip-area">
                                    <a href="{{$APP_BASE_URL}}provinces/admin/edit-provinces/id/{{$item.province_id}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a href="javascript:deleteAProvinces({{$item.province_id}});"  class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash-o"></i></a>

                                </span>
                            </td>

                        </tr>
                        {{/foreach}}
                        {{/if}}
                    </tbody>
                </table>


            </div><!-- /.span -->
        </div><!-- /.row -->


        <div class="col-lg-12">
            <div class="form-group col-lg-4">
                <label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
                <div class="col-lg-6">
                    <select id="action" class="form-control" >
                        <option value=";">{{l}}Choose an action...{{/l}}</option>
                        <option value="deleteProvinces();">{{l}}Delete Provinces{{/l}}</option>
                        <option value="enableProvinces();">{{l}}Enable Provinces{{/l}}</option>
                        <option value="disableProvinces();">{{l}}Disable Provinces{{/l}}</option>
                    </select>
                </div>
                <div class="col-lg-6">
                    <a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
                </div>
            </div>
            <div class="form-group col-lg-2">
                <label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
                <div class="col-lg-12">
                    <form class="search" name="search" method="post" action="">
                        <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
                            <option value="5" {{if $displayNum == 5}} selected="selected" {{/if}}>5</option>
                            <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
                            <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
                            <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
                            <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
                            <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
                        </select>
                    </form>
                </div>
            </div>
            {{if $countAllPages > 1}}
            <div class="col-lg-6 pagination">
                {{if $first}}
                <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
                {{/if}}
                {{if $prevPage}}
                <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
                {{/if}}

                {{foreach from=$prevPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>

                {{foreach from=$nextPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                {{if $nextPage}}
                <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
                {{/if}}
                {{if $last}}
                <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
                {{/if}}

            </div>
            {{/if}}
        </div>
    </div>


</div>
</div>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        document.getElementById('name').select();
        document.getElementById('name').focus();
        $('.close').click(function () {
            $(this).parent().hide("slow");
        });
        $('.check-all').click(function () {
            if (this.checked) { // check select status
                $('.allProvinces').each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.allProvinces').each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });
    });

    function applySelected()
    {
        var task = document.getElementById('action').value;
        eval(task);
    }
    function enableProvinces()
    {
        var all = document.getElementsByName('allProvinces');
        var tmp = '';
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
            }
        }
        if ('' == tmp) {
            alert('{{l}}Please choose an provinces{{/l}}');
                    }
                    window.location.href = '{{$APP_BASE_URL}}provinces/admin/enable-provinces/id/' + tmp;
                }

                function disableProvinces()
                {
                    var all = document.getElementsByName('allProvinces');
                    var tmp = '';
                    for (var i = 0; i < all.length; i++) {
                        if (all[i].checked) {
                            tmp = tmp + '_' + all[i].value;
                        }
                    }
                    if ('' == tmp) {
                        alert('{{l}}Please choose an provinces{{/l}}');
                                }
                                window.location.href = '{{$APP_BASE_URL}}provinces/admin/disable-provinces/id/' + tmp;
                            }

                            function deleteProvinces()
                            {
                                var all = document.getElementsByName('allProvinces');
                                var tmp = '';
                                var count = 0;
                                for (var i = 0; i < all.length; i++) {
                                    if (all[i].checked) {
                                        tmp = tmp + '_' + all[i].value;
                                        count++;
                                    }
                                }
                                if ('' == tmp) {
                                    alert('{{l}}Please choose an provinces{{/l}}');
                                                return;
                                            } else {
                                                result = confirm('{{l}}Are you sure you want to delete{{/l}} ' + count + ' {{l}}provinces(s){{/l}}?');
                                                if (false == result) {
                                                    return;
                                                }
                                            }
                                            window.location.href = '{{$APP_BASE_URL}}provinces/admin/delete-provinces/id/' + tmp;
                                        }


                                        function deleteAProvinces(id)
                                        {
                                            result = confirm('{{l}}Are you sure you want to delete this provinces{{/l}}?');
                                            if (false == result) {
                                                return;
                                            }
                                            window.location.href = '{{$APP_BASE_URL}}provinces/admin/delete-provinces/id/' + id;
                                        }
</script>
