<<style>
<!--
dd, dt {
    line-height: 20px   !important;
}
-->
</style>
<div class="columns-container">
    <div class="container" id="columns">
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{l}}Gửi Yêu Cầu Kết Nối{{/l}}</span>
        </div>

        <div class="row">
            {{sticker name=front_user_menu}}
            <div class="col-md-4 contact-info">
                <h4 class="widget-title">{{l}}Bên A{{/l}}</h4>
                <dl class="dl-horizontal">
                    <dt>{{l}}Công Ty{{/l}}</dt>
                    <dd>
                       	{{$user_a.pcompany_name}}
                    </dd>
                     <dt>{{l}}Số Điện Thoại{{/l}}</dt>
                    <dd>
                       	{{$user_a.phone}}
                    </dd>
                    <dt>{{l}}Email{{/l}}</dt>
                    <dd>
                       	{{$user_a.email_company}}
                    </dd>
                    <dt>{{l}}Tiêu Đề Buying{{/l}}</dt>
                    <dd>
                       	{{$buyingRequest.name}}
                    </dd>
                    <dt>{{l}}Nội Dung{{/l}}</dt>
                    <dd>
                       	{{$buyingRequest.description}}
                    </dd>
                </dl>

                <h4 class="widget-title">{{l}}Bên B{{/l}}</h4>
                <dl class="dl-horizontal">
                    <dt>{{l}}Công Ty{{/l}}</dt>
                    <dd>
                       	{{$user_b.pcompany_name}}
                    </dd>
                     <dt>{{l}}Số Điện Thoại{{/l}}</dt>
                    <dd>
                       	{{$user_b.phone}}
                    </dd>
                    <dt>{{l}}Email{{/l}}</dt>
                    <dd>
                       	{{$user_b.email_company}}
                    </dd>
                    <dt>{{l}}Tiêu Đề Buying{{/l}}</dt>
                    <dd>
                       	{{$qoute.title}}
                    </dd>
                    <dt>{{l}}Nội Dung{{/l}}</dt>
                    <dd>
                       	{{$qoute.full_text}}
                    </dd>
                </dl>
            </div>

            <div class="col-md-8">
                <div class="col-md-12 col-sm-6 widget-container-col" id="connect_bank">
                	<div class="widget-box">
                        <div class="widget-header">
                            <h5 class="widget-title">{{l}}Nội Dung Gửi Yêu Cầu{{/l}}</h5>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="timeline-container">
                                	{{$bank.content}}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="widget-box">
                        <div class="widget-header">
                            <h5 class="widget-title">{{$data.title}}</h5>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="timeline-container">
                                	{{$data.full_text}}
				                	<a href="{{$BASE_URL}}{{$bank.file}}" target="_blank" >
				                		<i class="fa fa-download" style="font-size: 20px;margin: 0px 10px 0px 0px;"></i>
				                    	<span class="page-heading-title2">{{l}}Download File Yêu Cầu{{/l}}</span>
				                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
<script>
	function getBank(){
		var id = $('#select_bank').val();
			
		$.ajax({
			  url: "{{$BASE_URL}}bank/index/get-bank/id/"+id,
			}).done(function(data ) {
				$('#connect_bank').html(data);
			});
	}
</script>