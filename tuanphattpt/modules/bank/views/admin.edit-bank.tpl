<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>  
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<script type="text/javascript">
    //<![CDATA[

    jQuery(document).ready(function (){
        CKFinder.setupCKEditor( null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/' );
        jQuery( "#images" ).sortable();
        jQuery( "#images" ).disableSelection();
        //Make slug
        {{foreach from=$allLangs item=item name=langDiv}}
        jQuery('#title{{$smarty.foreach.langDiv.iteration}}').makeSlug({
            slug: jQuery('#alias{{$smarty.foreach.langDiv.iteration}}')
        });
        {{/foreach}}
        	//Display images
            jQuery(".input_image[value!='']").parent().find('div').each( function (index, element){
                jQuery(this).toggle();
            });
    });
    var imgId;
    function chooseImage(id)
    {
        imgId = id;
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();
        finder.basePath = '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/'; // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.selectActionFunction = setFileField;
        finder.popup();
    } 
    // This is a sample function which is called when a file is selected in CKFinder.
    function setFileField( fileUrl )
    {
        document.getElementById( 'chooseImage_img' + imgId ).src = fileUrl;
        document.getElementById( 'chooseImage_input' + imgId).value = fileUrl;
        document.getElementById( 'chooseImage_div' + imgId).style.display = '';
        document.getElementById( 'chooseImage_noImage_div' + imgId ).style.display = 'none';
    }
    function clearImage(imgId)
    {
        document.getElementById( 'chooseImage_img' + imgId ).src = '';
        document.getElementById( 'chooseImage_input' + imgId ).value = '';
        document.getElementById( 'chooseImage_div' + imgId).style.display = 'none';
        document.getElementById( 'chooseImage_noImage_div' + imgId).style.display = '';
    }

    function addMoreImg()
    {
        jQuery("ul#images > li.hidden").filter(":first").removeClass('hidden');
    }

//]]>
</script>
<style type="text/css">
    #images { list-style-type: none; margin: 0; padding: 0;}
    #images li { margin: 10px; float: left; text-align: center;  height: 180px;}
</style>


<div class="page-header">
	<h1>
		{{l}}Bank{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}Edit Bank{{/l}}
		</small>
	</h1>
</div>

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
			<div class="row">
				<div class="col-xs-12">
						{{if $errors|@count > 0}}
	                        <div class="alert alert-danger">
	                        	<button class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
									{{if $errors.main}}
	                                   </strong> {{$errors.main}}
	                                {{else}}
	                                   {{l}}Please check following information again{{/l}}
	                                {{/if}} 
							</div>
	                	{{/if}}
	                        
	                        
	                	<div class="col-sm-12 widget-container-col ui-sortable">
	                	
										<div class="tabbable tabs-left">
										
											<ul class="nav nav-tabs" id="myTab2">
												{{if $fullPermisison}}
													<li {{if $lid == ''}}class="active"{{/if}}>
														<a href="#tab0" data-toggle="tab" aria-expanded="true">
															<i class="pink ace-icon fa fa-home bigger-110"></i>
															{{l}}Basic{{/l}}
														</a>
													</li>
												{{/if}}
												
												{{foreach from=$allLangs item=item index=index name=langTab}}
													<li {{if $lid == $item.lang_id}}class="active"{{/if}}>
														<a href="#tab{{$item.lang_id}}" data-toggle="tab" aria-expanded="true">
															<image style="vertical-align:middle;" src="{{$BASE_URL}}{{$item.lang_image}}"> {{$item.name}}
														</a>
													</li>
												{{/foreach}}
											</ul>
													
											<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
												<div class="tab-content">
												
													<div id="tab0" class="tab-pane {{if $lid == ''}}active{{/if}}">
									                	
														<div class="form-group">
																<label class="control-label col-md-2">{{l}}Category{{/l}}</label>
																<div class="col-md-10">
																		<select name="data[bank_category_gid]"  class="chosen-select form-control" id="form-field-select-3">
																			{{foreach from=$allCats item=item}}
										                                        <option value="{{$item.bank_category_gid}}" {{if $data.bank_category_gid == $item.bank_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
										                                    {{/foreach}}
																		</select>
																</div>
														</div>
														
														<div class="form-group" style="display: none;">
																<label class="control-label col-md-2">{{l}}Published{{/l}}</label>
																<div class="col-md-10">
																		<label class="radio-inline">
																				<input type="radio" name="data[genabled]" value="1" {{if $data.genabled != '0'}}checked="checked"{{/if}}/> Yes
																		</label>
																		<label class="radio-inline">
																			<input type="radio" name="data[genabled]" value="0" {{if $data.genabled == '0'}}checked="checked"{{/if}}/> No
																		</label>
																</div>
														</div>
						                                
						                                
														<footer class="panel-footer">
															<div class="form-group">
																<label class="control-label col-md-3">{{l}}Images:{{/l}}</label>
																<div class="col-md-9">
									                                <ul id="images">
									                                    {{foreach from=$data.images item=item key=i name=images }} 
									                                     <li {{if $i >=1 && null == $item}}class="hidden"{{/if}}>
									                                            <input class="input_image" type="hidden" id="chooseImage_input{{$i}}" name="data[images][]" value="{{if $item}}{{$BASE_URL}}{{$item}}{{/if}}">
									                                            <div id="chooseImage_div{{$i}}" style="display: none;">
									                                                <img src="{{if $item}}{{$BASE_URL}}{{$item}}{{/if}}" id="chooseImage_img{{$i}}" style="max-width: 150px; max-height:150px; border:dashed thin;"></img>
									                                            </div>
									                                            <div id="chooseImage_noImage_div{{$i}}" style="width: 150px; border: thin dashed; text-align: center; padding:70px 0px;">
									                                                No image
									                                            </div>
									                                            <br/>
									                                            <a href="javascript:chooseImage({{$i}});">{{l}}Choose image{{/l}}</a>
									                                            | 
									                                            <a href="javascript:clearImage({{$i}});">{{l}}Delete{{/l}}</a>
									                                    </li>
									                                    {{/foreach}}
									                                </ul>
																</div>
															</div>
						                                
							                                
														</footer>   

														<div class="clearfix form-actions">
															<div class="col-md-offset-3 col-md-9">
																<button class="btn btn-info" type="submit">
																	<i class="ace-icon fa fa-check bigger-110"></i>
																	{{l}}Save{{/l}}
																</button>
															</div>
														</div>
														
													</div>
													
													{{foreach from=$allLangs item=item name=langDiv}}
														<div class="tab-pane {{if $lid == $item.lang_id}}active{{/if}}" id="tab{{$item.lang_id}}">
								                                <div class="form-group" style="display: none;">
																		<label class="control-label col-md-2">{{l}}Published{{/l}}</label>
																		<div class="col-md-10">
																				<label class="radio-inline">
																						<input type="radio" name="data[{{$item.lang_id}}][enabled]" value="1" {{if $data[$item.lang_id].enabled == '1'}}checked="checked"{{/if}}/> Yes
																				</label>
																				<label class="radio-inline">
																					<input type="radio" name="data[{{$item.lang_id}}][enabled]" value="0" {{if $data[$item.lang_id].enabled != '1'}}checked="checked"{{/if}}/> No
																				</label>
																		</div>
																</div>
																<div class="form-group has-info">
																	<label class="control-label col-md-2">{{l}}Attach File{{/l}}</label>
																	<div class="col-md-10">
																		<input type="file" id="id-input-file-2" class="form-control" name="file[{{$item.lang_id}}]" />
																	</div>
																</div>
																<div class="form-group">
																	<label class="control-label col-md-2">{{l}}Title{{/l}}</label>
																	<div class="col-md-10">
																			<input id="title{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][title]"  value="{{$data[$item.lang_id].title}}" >
																	</div>
																</div>
																
																<div class="form-group">
																	<label class="control-label col-md-2">{{l}}Alias{{/l}}</label>
																	<div class="col-md-10">
																			<input id="alias{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][alias]"  value="{{$data[$item.lang_id].alias}}" >.html
																	</div>
																</div>
																<div class="form-group">
																	<label class="control-label col-md-2">{{l}}Seo Title{{/l}}</label>
																	<div class="col-md-10">
																			<input type="text" class="form-control" name="data[{{$item.lang_id}}][seo_title]"  value="{{$data[$item.lang_id].seo_title}}" >
																	</div>
																</div>
																
																<div class="form-group">
																	<label class="control-label col-md-2">{{l}}Seo Keywords{{/l}}</label>
																	<div class="col-md-10">
																			<input type="text" class="form-control" name="data[{{$item.lang_id}}][seo_keywords]"  value="{{$data[$item.lang_id].seo_keywords}}" >
																	</div>
																</div>
																
																
																<div class="form-group">
																	<label class="control-label col-md-2">{{l}}Seo Description{{/l}}</label>
																	<div class="col-md-10">
																			<input type="text" class="form-control" name="data[{{$item.lang_id}}][seo_description]"  value="{{$data[$item.lang_id].seo_description}}" >
																	</div>
																</div>
																<div class="form-group">
																	<label class="control-label col-md-2">{{l}}Tag{{/l}}</label>
																	<div class="col-md-10">
																			<input type="text" class="form-control" name="data[{{$item.lang_id}}][tag]"  value="{{$data[$item.lang_id].tag}}" >
																	</div>
																</div>
																
								                                <div class="form-group">
																	<label class="control-label col-md-2">{{l}}Bank{{/l}}</label>
																	<div class="col-md-10">
																			<textarea style="float:left;" class="text-input textarea ckeditor"  name="data[{{$item.lang_id}}][full_text]" rows="20" cols="90">{{$data[$item.lang_id].full_text}}</textarea>
																	</div>
																</div>
								                                
								                                <div class="clearfix form-actions">
																	<div class="col-md-offset-3 col-md-9">
																		<button class="btn btn-info" type="submit">
																			<i class="ace-icon fa fa-check bigger-110"></i>
																			{{l}}Save{{/l}}
																		</button>
																	</div>
																</div> 
														</div>
													
													{{/foreach}}
													
												</div>
											
											</form>
											
										</div>
										
									</div>
					
					
					
				</div><!-- /.span -->
				
			</div><!-- /.row -->
		
		
		
		</div>
		
		
	</div>
			