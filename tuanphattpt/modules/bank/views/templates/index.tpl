<br class="cb">
<br class="cb">
<section id="clients">
		<div class="container">
    	<div class="row">
                <h2 class="heading wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">{{$name_cate}}</h2>
                <div class="separator_wrapper wow bounceIn" data-wow-duration="0.7s" data-wow-delay="0.7s">
                        <div class="separator_first_circle">
                        <img src="{{$LAYOUT_HELPER_URL}}front/assets/images/green-flower.png"  alt="green flower">
                        </div>
                    </div>
                <p class="helping-text wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="0.8s">{{$description}}</p>
        	<div class="col-md-12">
            	<div class="clients-grid">
            	
            		{{foreach from=$allContents item=item key=key}}
		        		<div class="media wow fadeInLeft" data-wow-duration="0.9s" data-wow-delay="0.9s">
		                  <div class="media-left">
		                    <a href="{{$item.url}}" class="hvr-overline-from-center">
		                      <img class="media-object" src="{{$BASE_URL}}{{$item.main_image}}" alt="{{$item.title}}">
		                    </a>
		                  </div>
		                  <div class="media-body">
		                    <p>{{$item.intro_text}}</p>
		                  </div>
		                  <div class="row">
		                  	<div class="col-md-12">
		                    	<div class="media-label">
		                            <p><strong>{{$item.title}}</strong></p>
		                        </div>
		                    </div>
		                  </div>
		                </div>
	                
	                {{/foreach}}
                </div>
        	</div>
         	</div>
     	</div>
</section>

<section id="blog" class="blog single section">
			<div class="container">
				<div class="row">
					<!-- Blog Left Side Begins -->
					<div class="col-md-12">
						
						{{if $countAllPages > 1}}					
						<!-- Pagination -->
						<div class="post-nav wow fadeInRight" data-animation="fadeInUp" data-animation-delay="300">
								<ul class="pagination">
									{{if $first}}
			                            <li><a href="?page=1" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a></li>
			                            {{/if}}
			                            {{if $prevPage}}
			                            <li><a href="?page={{$prevPage}}" title="{{l}}Previous Page{{/l}}">&laquo;</a></li>
			                            {{/if}}
			                            
			                            {{foreach from=$prevPages item=item}}
			                            <li><a href="?page={{$item}}" title="{{$item}}">{{$item}}</a></li>
			                            {{/foreach}}
			                            
			                            <li class="active"><a href="#"   title="{{$currentPage}}">{{$currentPage}}</a></li>
			                            
			                            {{foreach from=$nextPages item=item}}
			                            <li><a href="?page={{$item}}"  title="{{$item}}">{{$item}}</a></li>
			                            {{/foreach}}
			                            
			                            {{if $nextPage}}
			                            <li><a href="?page={{$nextPage}}"  title="{{l}}Next Page{{/l}}">&raquo;</a></li>
			                            {{/if}}
			                            {{if $last}}
			                            <li><a href="?page={{$countAllPages}}"  title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a></li>
			                            {{/if}}	
							</ul>
						</div>	<!-- Pagination Ends-->
						
						{{/if}}	
					</div><!-- Blog Left Side Ends -->
					
					
					
				</div>
			
			</div>
		</section><!-- Our Blog Section Ends -->