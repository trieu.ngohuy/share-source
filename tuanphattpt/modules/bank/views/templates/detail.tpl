<section id="blog" class="blog single section">
			<div class="container">
				<div class="row">
					<!-- Blog Left Side Begins -->
					<div class="col-md-8">
						<!-- Post -->
						
						<div class="post-item">
							<!-- Post Title -->
							<h2 class="wow fadeInLeft">{{$detail.title}}</h2>
							<div class="post wow fadeInUp">
								<!-- Image -->
								<img class="img-responsive" src="{{$BASE_URL}}{{$detail.main_image}}" alt="" />
								<div class="post-content wow fadeInUp">	
									{{$detail.full_text}}
								</div>
							</div>
						</div><!-- End Post -->	
						
						

						<!-- Comment Section -->
						
						
						
						<!-- Add Your Comments -->
						
						
					</div><!-- Blog Left Side Ends -->
					
					
					<!-- Blog Sidebar Begins -->
					<div class="col-md-4">
					
						<div class="sidebar">
						
							<!-- Search -->
							
							
							<!-- Popular Post -->
							<div class="blog/popular-post widget wow fadeInUp">
								<!-- Title -->
								<h2>{{l}}TIN LIÊN QUANG{{/l}}</h2>
								<ul class="popular-list list-unstyled">
									<!-- Item -->
									{{foreach from=$tinlienquan item=item}}
									<li>
										<!-- Post Image -->
										<a href="{{$item.url}}"><img src="{{$BASE_URL}}{{$item.main_image}}" alt="{{$item.title}}" /></a>
										<!-- Details -->
										<div class="content">
											<h3><a href="#">{{$item.title}}</a></h3>
										</div>
									</li>
									{{/foreach}}
								</ul>
							</div><!-- Popular Post Ends-->
							
							
							
						</div>
						
					</div><!-- Blog Sidebar Ends -->
					
				</div>
			
			</div>
		</section><!-- Our Blog Section Ends -->