<div class="container hs-code">
    <div class="row">
        {{sticker name=front_user_menu}}
        <div class="col-sm-12">
            <h3>{{l}}Bank List Connect{{/l}}</h3>
        </div>
        <div class="col-sm-12">

            <table class="table table-striped table-bordered">
            	<form id="top-search" name="search" method="post" action="">
                <thead>
                <tr>
                    <th>
                        {{l}}NO.{{/l}}
                    </th>
                    <th>
                        {{l}}TITLE{{/l}}
                    </th>
                    <th>
                        {{l}}CREATED DATE{{/l}}
                    </th>
                </tr>
                </thead>
                </form>

                <tbody>
                {{foreach from=$allContact item=item key=key}}
                <tr {{if $item.view == "0"}} style="font-weight: bold;"{{/if}}>
                    <td>{{$key+1}}</td>
                    <td>
                        <a href="{{$BASE_URL}}bank-detail/{{$item.connect_id}}">{{$item.buying.name}}</a>
                    </td>
                    <td>{{$item.created_date|date_format:"%d-%m-%Y"}}</td>
                </tr>
                {{/foreach}}
                </tbody>
            </table>
        </div>
        <div class="col-md-12 sortPagiBar">
            {{if $countAllPages > 1}}
            <div class="bottom-pagination">
                <nav>
                    <ul class="pagination">
                        {{if $prevPage}}
                        <li>
                            <a href="?page={{$prevPage}}" aria-label="Prev">
                                <span aria-hidden="true">&laquo; Prev</span>
                            </a>
                        </li>
                        {{/if}}

                        {{foreach from=$prevPages item=item}}
                        <li><a href="?page={{$item}}">{{$item}}</a></li>
                        {{/foreach}}

                        <li class="active"><a>{{$currentPage}}</a></li>

                        {{foreach from=$nextPages item=item}}
                        <li><a href="?page={{$item}}">{{$item}}</a></li>
                        {{/foreach}}

                        {{if $nextPage}}
                        <li>
                            <a href="?page={{$nextPage}}" aria-label="Next">
                                <span aria-hidden="true">Next &raquo;</span>
                            </a>
                        </li>
                        {{/if}}
                    </ul>
                </nav>
            </div>
            {{/if}}
        </div>
    </div>
</div>
