<br class="cb"/>
<br class="cb"/>
<section id="about">
	<div class="container">
        <div class="row">
            	<h2 class="heading wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">{{l}}About us{{/l}}</h2>
                <div class="separator_wrapper wow bounceIn" data-wow-duration="0.7s" data-wow-delay="0.7s">
                        <div class="separator_first_circle">
                        <img src="{{$LAYOUT_HELPER_URL}}front/assets/images/green-flower.png" alt="green flower">
                        </div>
                    </div>
                <p class="helping-text wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="0.8s">{{l}}Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer egestas, lorem ut suscipit facilisis, nisi neque commodo quam, non pretium dui arcu varius neque.{{/l}}</p>
                
                {{$news.full_text}}
				<div class="col-md-6 col-sm-12 wow bounceIn" data-wow-duration="0.9s" data-wow-delay="0.9s">
                
					<div class="circle-img green">
                    	<img src="{{$BASE_URL}}{{$news.images.0}}" class="hvr-rotate" alt=" green ">
                    </div>
                    <div class="circle-img blue">
                    	<img src="{{$BASE_URL}}{{$news.images.1}}" class="hvr-rotate" alt=" blue ">
                    </div>
              </div>
		</div>
    </div>
    <div class="stats">
    	<div class="container">
        	<ul class="list-inline" id="numbers">
                        <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.8s">
                        	<span id="number1">10</span>
                            <h4>{{l}}Khách Hàng{{/l}}</h4>
                        </li>
                        <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.8s">
                            
                            <span id="number2">10</span>
                            <h4>{{l}}Đối Tác{{/l}}</h4>
                        </li>
                        <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.8s">
                            
                            <span id="number3">10</span>
                            <h4>{{l}}Sản Phẩm{{/l}}</h4>
                        </li>
                    </ul>
        </div>
    </div>
</section>