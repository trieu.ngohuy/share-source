<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <a class="home" title="Trade Show">{{l}}Kết Nối Tài Chính{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span>{{$tradeShow.title}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- Blog category -->
                {{sticker name=front_default_category}}
                <!-- ./blog category  -->

                {{foreach from=$adv item=item}}
					    <div class="col-xs-12 col-sm-12 col-md-12 m10t qc" style="padding: 0px">
					        <a href="{{$item.link}}"><img alt="Banner right 1" src="{{$BASE_URL}}{{$item.images}}"></a>
					    </div>
					{{/foreach}}
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <h1 class="page-heading">
                    <span class="page-heading-title2">{{$tradeShow.title}}</span>
                </h1>
                <article class="entry-detail">
                    <div class="content-text clearfix">
                        {{$tradeShow.full_text}}
                    </div>
                </article>
               	{{if $tradeShow.file != null}}
                <h1 class="page-heading">
                	<a href="{{$BASE_URL}}{{$tradeShow.file}}">
                		<i class="fa fa-download" style="font-size: 20px;margin: 6px 10px 0px 0px;"></i>
                    	<span class="page-heading-title2">{{l}}Download File Tham Khảo{{/l}}</span>
                    
                    </a>
                </h1>
                {{/if}}
                <!-- Related Posts -->
                <div class="single-box">
                    <h2>{{l}}Sản Phẩm Kết Nối Tài Chính Liên Quan{{/l}}</h2>
                    <ul class="related-posts owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":2},"1000":{"items":3}}'>
                        {{foreach from=$newTradeShow item=item}}
                        <li class="post-item">
                            <article class="entry">
                                <div class="entry-thumb image-hover2">
                                    <a href="{{$BASE_URL}}bank/{{$item.bank_gid}}/{{$item.alias}}.html">
                                        <img src="{{$BASE_URL}}{{$item.images}}" alt="{{$item.title}}">
                                    </a>
                                </div>
                                <div class="entry-ci">
                                    <h3 class="entry-title" style="margin: 0px 0px 5px"><a href="{{$BASE_URL}}bank/{{$item.bank_gid}}/{{$item.alias}}.html" style="font-weight: bold;">{{$item.title}}</a></h3>
                                    <div class="entry-more">
                                    	{{$item.intro_text}}
                                        <a href="{{$BASE_URL}}bank/{{$item.bank_gid}}/{{$item.alias}}.html">{{l}}Read more{{/l}}</a>
                                    </div>
                                </div>
                            </article>
                        </li>
                        {{/foreach}}
                    </ul>
                </div>
                <!-- ./Related Posts -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>