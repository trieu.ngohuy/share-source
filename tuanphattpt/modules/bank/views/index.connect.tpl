<<style>
<!--
dd, dt {
    line-height: 20px   !important;
}
-->
</style>
<div class="columns-container">
    <div class="container" id="columns">
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{l}}Gửi Yêu Cầu Kết Nối{{/l}}</span>
        </div>

        <div class="row">
            {{sticker name=front_user_menu}}
            <div class="col-md-4 contact-info">
                <h4 class="widget-title">{{l}}Bên A{{/l}}</h4>
                <dl class="dl-horizontal">
                    <dt>{{l}}Công Ty{{/l}}</dt>
                    <dd>
                       	{{$user_a.pcompany_name}}
                    </dd>
                     <dt>{{l}}Số Điện Thoại{{/l}}</dt>
                    <dd>
                       	{{$user_a.phone}}
                    </dd>
                    <dt>{{l}}Email{{/l}}</dt>
                    <dd>
                       	{{$user_a.email_company}}
                    </dd>
                    <dt>{{l}}Tiêu Đề Buying{{/l}}</dt>
                    <dd>
                       	{{$buyingRequest.name}}
                    </dd>
                    <dt>{{l}}Nội Dung{{/l}}</dt>
                    <dd>
                       	{{$buyingRequest.description}}
                    </dd>
                </dl>

                <h4 class="widget-title">{{l}}Bên B{{/l}}</h4>
                <dl class="dl-horizontal">
                    <dt>{{l}}Công Ty{{/l}}</dt>
                    <dd>
                       	{{$user_b.pcompany_name}}
                    </dd>
                     <dt>{{l}}Số Điện Thoại{{/l}}</dt>
                    <dd>
                       	{{$user_b.phone}}
                    </dd>
                    <dt>{{l}}Email{{/l}}</dt>
                    <dd>
                       	{{$user_b.email_company}}
                    </dd>
                    <dt>{{l}}Tiêu Đề Buying{{/l}}</dt>
                    <dd>
                       	{{$qoute.title}}
                    </dd>
                    <dt>{{l}}Nội Dung{{/l}}</dt>
                    <dd>
                       	{{$qoute.full_text}}
                    </dd>
                </dl>
            </div>

            <div class="col-md-8">
                <div class="col-md-12 col-sm-6 widget-container-col" id="connect_bank">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h5 class="widget-title">{{l}}Chọn Sản Phẩm Kết Nối Tài Chính{{/l}}</h5>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="timeline-container">
                                	
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-6 widget-container-col">
                    <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
	                    <div class="form-group has-info col-md-12">
								<label class="control-label col-md-2">{{l}}Sản Phẩm Tài Chính{{/l}}</label>
								<div class="col-md-10">
										<select name="data[bank_gid]" class="form-control" onchange="getBank();" id="select_bank" required>
											<option value="" >{{l}}Selected{{/l}}</option>
											{{foreach from=$allTradeShow item=item}}
		                                        <option value="{{$item.bank_gid}}" >{{$item.title}}</option>
		                                    {{/foreach}}
										</select>
								</div>
						</div>
						
						<div class="form-group has-info col-md-12">
								<label class="control-label col-md-2">{{l}}Message{{/l}}</label>
								<div class="col-md-10">
										<textarea class="form-control input-sm" rows="6" name="data[content]" id="message" required></textarea>
								</div>
						</div>
						
						<div class="form-group has-info col-md-12">
								<label class="control-label col-md-2">{{l}}Attach File{{/l}}</label>
								<div class="col-md-10">
										<input type="file" class="form-control" name="images" required />
								</div>
						</div>
	
	                    <div class="col-md-12">
	                        <div class="col-md-offset-8 col-md-4">
	                            <button class="btn btn-info" type="submit">
	                                <i class="ace-icon fa fa-check bigger-110"></i>
	                                {{l}}Send{{/l}}
	                            </button>
	                        </div>
	                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
	function getBank(){
		var id = $('#select_bank').val();
			
		$.ajax({
			  url: "{{$BASE_URL}}bank/index/get-bank/id/"+id,
			}).done(function(data ) {
				$('#connect_bank').html(data);
			});
	}
</script>