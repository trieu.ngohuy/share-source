                    {{if $data != ''}}
                    <div class="widget-box">
                        <div class="widget-header">
                            <h5 class="widget-title">{{$data.title}}</h5>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="timeline-container">
                                	{{$data.full_text}}
                  
				                	<a href="{{$BASE_URL}}{{$data.file}}" target="_blank" >
				                		<i class="fa fa-download" style="font-size: 20px;margin: 0px 10px 0px 0px;"></i>
				                    	<span class="page-heading-title2">{{l}}Download Form Gửi Yêu Cầu{{/l}}</span>
				                    
				                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{else}}
                    	<div class="widget-box">
	                        <div class="widget-header">
	                            <h5 class="widget-title">{{l}}Chọn Sản Phẩm Kết Nối Tài Chính{{/l}}</h5>
	                        </div>
	                        <div class="widget-body">
	                            <div class="widget-main">
	                                <div class="timeline-container">
	                                	
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                    {{/if}}
               