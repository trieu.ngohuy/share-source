<?php

class Route_Bank
{
    /**
     * Build friendly URL
     * @param array $linkArr 0 => <module>, 1 => controller, 2 => action, 3...n param/value
     * @param array $params param => value
     *
     * @return string friendly URL
     */
    public function build($linkArr, $params = array())
    {
        $result = implode('/', $linkArr);
        if ('detail' == @$linkArr[2]) {
            /**
             * Structure: pages/<id>/<alias>.html
             */
            $result = "new/{$linkArr[4]}/";
            if (null != $params['alias']) {
                $result .= urlencode($params['alias']);
            }

        } else if ('index' == @$linkArr[2]) {
            /**
             * Category
             */
            $result = "news/{$linkArr[4]}/";
            if (null != @$params['alias']) {
                $result .= urlencode($params['alias']);
            }

        }
        return $result;
    }

    /**
     * Parse friendly URL
     */
    public function parse()
    {
        $router = Nine_Controller_Front::getInstance()->getRouter();
        $route = new Zend_Controller_Router_Route_Regex(
            'bank-category/([0-9]+)/(.*).html',
            array(
                'module' => 'bank',
                'controller' => 'index',
                'action' => 'index'
            ),
            array(1 => 'gid')
        );
        $router->addRoute('bank', $route);

        $route1 = new Zend_Controller_Router_Route_Regex(
            'bank/([0-9]+)/(.*).html',
            array(
                'module' => 'bank',
                'controller' => 'index',
                'action' => 'detail'
            ),
            array(1 => 'gid')
        );
        $router->addRoute('bank1', $route1);
         $route2 = new Zend_Controller_Router_Route_Regex(
            'connect/([0-9]+)/(.*).html',
            array(
                'module' => 'bank',
                'controller' => 'index',
                'action' => 'connect'
            ),
            array(1 => 'gid')
        );
        $router->addRoute('bank2', $route2);
        $route3 = new Zend_Controller_Router_Route_Regex(
            'bank/manager',
            array(
                'module' => 'bank',
                'controller' => 'index',
                'action' => 'manager'
            ),
            array(1 => 'gid')
        );
        $router->addRoute('bank3', $route3);
        $route4 = new Zend_Controller_Router_Route_Regex(
            'bank-detail/([0-9]+)',
            array(
                'module' => 'bank',
                'controller' => 'index',
                'action' => 'bank-detail'
            ),
            array(1 => 'gid')
        );
        $router->addRoute('bank4', $route4);

    }
}