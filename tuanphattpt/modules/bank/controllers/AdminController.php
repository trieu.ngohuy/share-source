<?php

require_once 'modules/bank/models/Bank.php';
require_once 'modules/bank/models/BankCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/user/models/User.php';
class bank_AdminController extends Nine_Controller_Action_Admin 
{
    public function manageBankAction()
    {
        $objBank = new Models_Bank();
        $objLang    = new Models_Lang();
        $objCat     = new Models_BankCategory();
        
        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_bank', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $this->view->headTitle(Nine_Language::translate('manage Bank '));
        
        
        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
    	if($currentPage == false){
        	$currentPage = 1;
        	$this->session->bankDisplayNum = null;
        	$this->session->bankCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objBank->update(array('sorting' => $value), array('bank_gid=?' => $id));
            $this->session->bankMessage = array(
                                       'success' => true,
                                       'message' => Nine_Language::translate("Bank Order is edited successfully")
                                   );
        }
        
        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->bankDisplayNum;
        } else {
            $this->session->bankDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->bankCondition;
        } else {
            $this->session->bankCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        if(@$condition['keyword'] != ''){
        	$condition['keyword'] = $objBank->convert_vi_to_en($condition['keyword']);
        	$condition['keyword'] = preg_replace("/( )/", '-', $condition['keyword']);
        }
        
        /**
         * Get all categories
         */
        $this->view->allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_bank', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        /**
         * Get all banks
         */
        $numRowPerPage= $numRowPerPage;
        $allBank = $objBank->getAllBank($condition, array('sorting ASC', 'bank_gid DESC', 'bank_id ASC'),
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage
                                                  );
//        echo '<pre>';print_r($allBank);die;
        /**
         * Count all banks
         */
        $count = count($objBank->getallBank($condition));
        /**
         * Format
         */
        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allBank[0]['bank_gid'];
        foreach ($allBank as $index=>$bank) {
            /**
             * Change date
             */
            if (0 != $bank['created_date']) {
                $bank['created_date'] = date($config['dateFormat'], $bank['created_date']);
            } else {
                $bank['created_date'] = '';
            }
            if (0 != $bank['publish_up_date']) {
                $bank['publish_up_date'] = date($config['dateFormat'], $bank['publish_up_date']);
            } else {
                $bank['publish_up_date'] = '';
            }
            if (0 != $bank['publish_down_date']) {
                $bank['publish_down_date'] = date($config['dateFormat'], $bank['publish_down_date']);
            } else {
                $bank['publish_down_date'] = '';
            }
            if ($tmpGid != $bank['bank_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $bank['bank_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $bank;
                $tmp2['hit'] = 0;
            }
            $tmp2['hit']    += $bank['hit'];
            $tmp2['langs'][]  = $bank;
            /**
             * Final element
             */
            if ($index == count($allBank) - 1) {
                $tmp[] = $tmp2;
            }
        }
        $allBank = $tmp;
    	$export = $this->_getParam('export', false);
		if($export != false){
			  	$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
		
				$style = "style = 'border: 1px solid'";
				
				if(Nine_Language::getCurrentLangId() == 1){
					
					$textHeader = array(
						"<b>No</b>",
						"<b>Category Bank</b>",
						"<b>Estore Upload</b>",
						"<b>Created Date</b>",
						"<b>Title</b>"
					);
				}else{
					
					$textHeader = array(
						mb_convert_encoding("<b>Số Thứ Tự</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Danh Mục Bài Viết</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Gian Hàng Upload</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Ngày Khởi Tạo</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Tiêu Đề</b>",'HTML-ENTITIES','UTF-8')
					);
				}
				
				$header = "<tr>";
				foreach ($textHeader as $text) {
					$header .= "<td $style>" . utf8_encode($text) . "</td>";
				}
				$header .= "</tr>";
		
				$bank = '';
				$no = 1;
				
				foreach ($allBank as $item) {
					$bank .="<tr>";
					$bank .= "<td $style>" . $no . "</td>";
					$bank .= "<td $style>" . mb_convert_encoding($item['cname'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$bank .= "<td $style>" . mb_convert_encoding($item['uname'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$bank .= "<td $style>" .  mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8') . "</td>";
					$bank .= "<td $style>" .  mb_convert_encoding($item['title'],'HTML-ENTITIES','UTF-8') . "</td>";
					$bank .="</tr>";
					$no++;
				}
		
				header("Bank-Type: application/vnd.ms-excel; charset=UTF-8");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("bank-disposition: attachment;filename=thong-ke-bank-" . date('d-m-Y H:i:s') . ".xls");
		
				$xlsTbl = $header;
				$xlsTbl .= $bank;
		
				echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
				exit();
				
		}
        /**
         * Set values for tempalte
         */
//        echo print_r($condition); die;
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allBank = $allBank;
        $this->view->bankMessage = $this->session->bankMessage;
        $this->session->bankMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_bank');
        $this->view->EditPermisision = $this->checkPermission('edit_bank');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_bank');
        $this->view->DeletePermisision = $this->checkPermission('delete_bank');
        $this->view->allLangs = $allLangs;
        
        $this->view->menu = array(
        	0=>'bank',
        	1=>'manager-bank'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-newspaper-o',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/bank/admin/manage-bank',
        		'name'	=>	Nine_Language::translate('Manager Bank')
        		)
        	
        );
    }
    
    public function newBankAction()
    {
        $objCat = new Models_BankCategory();
        $objLang = new Models_Lang();
        $objBank = new Models_Bank();
        $id = $this->_getParam('id', false);
        
        if($id != false){
        	$id = explode("_", $id);
        	if(count($id) != 2){
        		$this->_redirect('bank/admin/manage-bank');
        	}
        }
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_bank', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $data = $this->_getParam('data', false);
        /**
         * Get all categories
         */
        $allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_bank', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new bank
             */
            $newBank = $data;
            $newBank['created_date'] = time();
            /**
             * Change format date
             */
        
            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newBank[$lang['lang_id']]['intro_text'], $newBank[$lang['lang_id']]['full_text'])= Nine_Function::splitTextWithReadmore($newBank[$lang['lang_id']]['full_text']);
            	if($newBank[$lang['lang_id']]['alias'] == ""){
            		$newBank[$lang['lang_id']]['alias'] = $objBank->convert_vi_to_en($newBank[$lang['lang_id']]['title']);
            		$newBank[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newBank[$lang['lang_id']]['alias']));
            	}
            }
            
            /**
             * Remove empty images
             */
            if (is_array($newBank['images'])) {
                foreach ($newBank['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newBank['images'][$index]);
                    } else {
                        $newBank['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newBank['images'] = implode('||', $newBank['images']);
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > $newBank['sorting']) {
                    $newBank['sorting'] = 1;
                }
                $objBank->increaseSorting($newBank['sorting'], 1);
                
            	if($id != false){
                	$newBank['user_id'] = $id[1];
                	$newBank['bank_category_gid'] = $id[0];
                }else{
                	$newBank['user_id'] = Nine_Registry::getLoggedInUserId();
                }
                
                if($newBank['publish_up_date'] == ''){
                	$newBank['publish_up_date'] = date();
                }
            	if(isset($_FILES['file']) && $_FILES['file']['tmp_name'][1] != '') {
                        $fileName = basename($_FILES['file']['name'][1]);
                        $fileTmp = $_FILES['file']['tmp_name'][1];
                        $uploadPath = 'media/userfiles/user/1/';
                        $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                        $uploadFile = $uploadDir . $fileName;

                        $ext_allow = array (
                            'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX'
                        );
                        $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
                        if(in_array($ext, $ext_allow)) {
                            move_uploaded_file($fileTmp, $uploadFile);
                            $newBank[1]['file'] = $uploadPath . $fileName;
                        }
                }
        		if(isset($_FILES['file']) && $_FILES['file']['tmp_name'][2] != '') {
                        $fileName = basename($_FILES['file']['name'][2]);
                        $fileTmp = $_FILES['file']['tmp_name'][2];
                        $uploadPath = 'media/userfiles/user/1/';
                        $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                        $uploadFile = $uploadDir . $fileName;

                        $ext_allow = array (
                            'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX'
                        );
                        $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
                        if(in_array($ext, $ext_allow)) {
                            move_uploaded_file($fileTmp, $uploadFile);
                            $newBank[2]['file'] = $uploadPath . $fileName;
                        }
                }
                $objBank->insert($newBank);
                /**
                 * Message
                 */
                $this->session->bankMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Bank is created successfully.')
                        );
//                echo "<pre>HERE";die;
                $this->_redirect('bank/admin/manage-bank');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $this->view->id = $id;
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Bank'));
        $this->view->fullPermisison = $this->checkPermission('new_bank', null, '*'); 
        $this->view->CheckGenalbel = $this->checkPermission('genabled_bank');
         $this->view->menu = array(
        	0=>'bank',
        	1=>'new-bank'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-newspaper-o',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/bank/admin/manage-bank',
        		'name'	=>	Nine_Language::translate('Manager Bank')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-plus',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('New Bank')
        		)
        	
        );
    }
    public function editBankAction()
    {
        $objBank = new Models_Bank();
        $objCat     = new Models_BankCategory();
        $objLang    = new Models_Lang();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_bank', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false); 
        if (false == $gid) {
            $this->_redirect('bank/admin/manage-bank');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_bank', null, '*'))
        ||  (false != $lid && false == $this->checkPermission('edit_bank', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $data   = $this->_getParam('data', false);
        
        /**
         * Get all categories
         */
        $allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all bank languages
         */
        
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allBankLangs = $objBank->setAllLanguages(true)->getByColumns(array('bank_gid=?' => $gid))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_bank', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allBankLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new bank
             */
            $newBank = $data;
            /**
             * Change format date
             */
            /**
             * Sorting
             */
        
            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newBank[$lang['lang_id']]['intro_text'], $newBank[$lang['lang_id']]['full_text'])= Nine_Function::splitTextWithReadmore($newBank[$lang['lang_id']]['full_text']);
            	if($newBank[$lang['lang_id']]['alias'] == ""){
            		$newBank[$lang['lang_id']]['alias'] = $objBank->convert_vi_to_en($newBank[$lang['lang_id']]['title']);
            		$newBank[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newBank[$lang['lang_id']]['alias']));
            	}
            }
            /**
             * Remove empty images
             */
            if (is_array($newBank['images'])) {
                foreach ($newBank['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newBank['images'][$index]);
                    } else {
                        $newBank['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newBank['images'] = implode('||', $newBank['images']);
        		if(isset($_FILES['file']) && $_FILES['file']['tmp_name'][1] != '') {
                        $fileName = basename($_FILES['file']['name'][1]);
                        $fileTmp = $_FILES['file']['tmp_name'][1];
                        $uploadPath = 'media/userfiles/user/1/';
                        $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                        $uploadFile = $uploadDir . $fileName;

                        $ext_allow = array (
                            'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX','jpg'
                        );
                        $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
                        if(in_array($ext, $ext_allow)) {
                            move_uploaded_file($fileTmp, $uploadFile);
                            $newBank[1]['file'] = $uploadPath . $fileName;
                        }
                }
        		if(isset($_FILES['file']) && $_FILES['file']['tmp_name'][2] != '') {
                        $fileName = basename($_FILES['file']['name'][2]);
                        $fileTmp = $_FILES['file']['tmp_name'][2];
                        $uploadPath = 'media/userfiles/user/1/';
                        $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                        $uploadFile = $uploadDir . $fileName;

                        $ext_allow = array (
                            'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX','jpg'
                        );
                        $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
                        if(in_array($ext, $ext_allow)) {
                            move_uploaded_file($fileTmp, $uploadFile);
                            $newBank[2]['file'] = $uploadPath . $fileName;
                        }
                }
                
            try {
                /**                
                 * Update
                 */
                $objBank->update($newBank, array('bank_gid=?' => $gid));
                /**
                 * Message
                 */
                $this->session->bankMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Bank is updated successfully.')
                        );
                
                $this->_redirect('bank/admin/manage-bank');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allBankLangs);
            
            if (false == $data) {
                $this->session->bankMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate("Bank doesn't exist.")
                                           );
                $this->_redirect('bank/admin/manage-bank');
            }
            /**
             * Change date
             */
            if (0 != $data['publish_up_date']) {
                 $data['publish_up_date'] = date('d/m/Y', $data['publish_up_date']);
            } else {
                $data['publish_up_date'] = '';
            }
            if (0 != $data['publish_down_date']) {
                 $data['publish_down_date'] = date('d/m/Y', $data['publish_down_date']);
            } 
        	else {
                $data['publish_down_date'] = '';
            }
            /**
             * Format image
             */
            $data['images'] = explode('||', $data['images']);
            if (! is_array($data['images'])) {
                $data['images'] = array();
            }
            $data['images'] = array_pad($data['images'], 50, null);
            /**
             * Get all lang banks
             */
            foreach ($allBankLangs as $bank) {
                /**
                 * Rebuild readmore DIV
                 */
                $bank['full_text'] = Nine_Function::combineTextWithReadmore($bank['intro_text'], $bank['full_text']);
                $data[$bank['lang_id']] = $bank;
            }
            
            /**
             * Add deleteable field
             */
            if (null != @$data['bank_category_gid']) {
            	$cat = @reset($objCat->getByColumns(array('bank_category_gid' => $data['bank_category_gid']))->toArray());
            	$data['bank_deleteable'] = @$cat['bank_deleteable'];
            }
        }
    	
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Bank'));
        $this->view->fullPermisison = $this->checkPermission('edit_bank', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_bank');
        $this->view->menu = array(
        	0=>'bank',
        	1=>'manager-bank'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-newspaper-o',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/bank/admin/manage-bank',
        		'name'	=>	Nine_Language::translate('Manager Bank')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-pencil',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('Edit Bank')
        		)
        	
        );
    }

    public function enableBankAction()
    {
        $objBank = new Models_Bank();
        $objUser = new Models_User();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('bank/admin/manage-bank');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_bank', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBank->update(array('genabled' => 1), array('bank_gid=?' => $gid));
                }
                $this->session->bankMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Bank is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->bankMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this bank. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_bank', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBank->update(array('enabled' => 1), array('bank_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->bankMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Bank is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->bankMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this bank. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('bank/admin/manage-bank');
    }
	public function tinnhanhBankAction()
    {
        $objBank = new Models_Bank();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('bank/admin/manage-bank');
        }
        
        $gids = explode('_', trim($gid, '_'));
       
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_bank', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBank->update(array('tinnhanh' => 1), array('bank_gid=?' => $gid));
                }
                $this->session->bankMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Bank is Flashes successfully')
                                               );
            } catch (Exception $e) {
                $this->session->bankMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this bank. Please try again')
                                               );
            }
            
        $this->_redirect('bank/admin/manage-bank');
    }
	public function distinnhanhBankAction()
    {
        $objBank = new Models_Bank();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('bank/admin/manage-bank');
        }
        
        $gids = explode('_', trim($gid, '_'));
       
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_bank', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBank->update(array('tinnhanh' => 0), array('bank_gid=?' => $gid));
                }
                $this->session->bankMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Bank is Dis Flashes successfully')
                                               );
            } catch (Exception $e) {
                $this->session->bankMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this bank. Please try again')
                                               );
            }
            
        $this->_redirect('bank/admin/manage-bank');
    }
    
    public function disableBankAction()
    {
        $objBank = new Models_Bank();
         $objUser = new Models_User();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('bank/admin/manage-bank');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_bank', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBank->update(array('genabled' => 0), array('bank_gid=?' => $gid));
                }
                $this->session->bankMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Bank is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->bankMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this bank. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_bank', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objBank->update(array('enabled' => 0), array('bank_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->bankMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Bank is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->bankMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this bank. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('bank/admin/manage-bank');
    }
    
    public function deleteBankAction()
    {
        $objBank = new Models_Bank();
        $objCat = new Models_BankCategory();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_bank')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid = $this->_getParam('gid', false);
        
        if (false == $gid) {
            $this->_redirect('bank/admin/manage-bank');
        }
        
        $gids = explode('_', trim($gid, '_'));
        
        try {
            foreach ($gids as $gid) {
            	
            	$bank = @reset($objBank->getByColumns(array('bank_gid=?'=>$gid))->toArray());
            	$cat = @reset($objCat->getByColumns(array('bank_category_gid=?'=>$bank['bank_category_gid']))->toArray());
            	if ( 0 == $cat['bank_deleteable']){
            		$this->session->bankMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete bank ('. $gid .'). Please try again')
                                           );
                    $this->_redirect('bank/admin/manage-bank');
            	}
            	else {
            		$objBank->delete(array('bank_gid=?' => $gid));
            	}
              
            }
            $this->session->bankMessage = array(
                                               'success' => true,
                                               'message' => Nine_Language::translate('Bank is deleted successfully')
                                           );
        } catch (Exception $e) {
            $this->session->bankMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete this bank. Please try again')
                                           );
        }
        $this->_redirect('bank/admin/manage-bank');
    }
    
    /**************************************************************************************************
     *                                         CATEGORY's FUNCTIONS
     **************************************************************************************************/
    public function manageCategoryAction()
    {
        $objLang    = new Models_Lang();
        $objCategory     = new Models_BankCategory();
        
        
        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $this->view->headTitle(Nine_Language::translate('manage Category '));
        
        
        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
        
        if($currentPage == false){
        	$currentPage = 1;
        	$this->session->categoryDisplayNum = null;
        	$this->session->categoryCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objCategory->update(array('sorting' => $value), array('bank_category_gid=?' => $id));
            $this->session->categoryMessage = array(
                                       'success' => true,
                                       'message' => Nine_Language::translate("Edit sort numbers successfully")
                                   );
        }
        
        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->categoryDisplayNum;
        } else {
            $this->session->categoryDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->categoryCondition;
        } else {
            $this->session->categoryCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_category', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        /**
         * Get all categorys
         */
        $numRowPerPage = $numRowPerPage;
        $allCategories  = $objCategory->setAllLanguages(true)->getallCategories($condition, array('sorting ASC', 'bank_category_gid DESC', 'bank_category_id ASC'),
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage
                                                  );
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Count all categorys
         */
        $count = count($objCategory->setAllLanguages(true)->getallCategories($condition));
        /**
         * Format
         */
        
        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allCategories[0]['bank_category_gid'];
        foreach ($allCategories as $index=>$category) {
            /**
             * Change date
             */
        	
            if (0 != $category['created_date']) {
                $category['created_date'] = date($config['dateFormat'], $category['created_date']);
            } else {
                $category['created_date'] = '';
            }
            if ($tmpGid != $category['bank_category_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $category['bank_category_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $category;
            }
            $tmp2['langs'][]  = $category;
            /**
             * Final element
             */
            if ($index == count($allCategories) - 1) {
                $tmp[] = $tmp2;
            }
            
        }
        
        $allCategories = $tmp;
   	 $export = $this->_getParam('export', false);
		if($export != false){
			  	$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
		
				$style = "style = 'border: 1px solid'";
				
				if(Nine_Language::getCurrentLangId() == 1){
					$textHeader = array(
						"<b>No</b>",
						"<b>Parent Category</b>",
						"<b>Created Date</b>",
						"<b>Title</b>"
					);
				}else{
					$textHeader = array(
						mb_convert_encoding("<b>Số Thứ Tự</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Danh Mục Trên</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Ngày Khởi Tạo</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Tiêu Đề</b>",'HTML-ENTITIES','UTF-8')
					);
				}
				
				$header = "<tr>";
				foreach ($textHeader as $text) {
					$header .= "<td $style>" . utf8_encode($text) . "</td>";
				}
				$header .= "</tr>";
		
				$bank = '';
				$no = 1;
				
				foreach ($allCategories as $item) {
					$bank .="<tr>";
					$bank .= "<td $style>" . $no . "</td>";
					$bank .= "<td $style>" . mb_convert_encoding($item['parent'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$bank .= "<td $style>" .  mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8') . "</td>";
					$bank .= "<td $style>" .  mb_convert_encoding($item['name'],'HTML-ENTITIES','UTF-8') . "</td>";
					$bank .="</tr>";
					$no++;
				}
		
				header("Bank-Type: application/vnd.ms-excel; charset=UTF-8");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("bank-disposition: attachment;filename=thong-ke-category-bank-" . date('d-m-Y H:i:s') . ".xls");
		
				$xlsTbl = $header;
				$xlsTbl .= $bank;
		
				echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
				exit();
				
		}
//        echo print_r($allCategories);die;
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCategories = $allCategories;
        $this->view->categoryMessage = $this->session->categoryMessage;
        $this->session->categoryMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_category');
        $this->view->allLangs = $allLangs;
        
        $this->view->menu = array(
        	0=>'bank',
        	1=>'manager-category-bank'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-newspaper-o',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/bank/admin/manage-category',
        		'name'	=>	Nine_Language::translate('Manager Category Bank')
        		)
        	
        );
    }
    
    public function newCategoryAction()
    {
        $objLang = new Models_Lang();
        $objCategory = new Models_BankCategory;
        $objBank = new Models_Bank();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $data = $this->_getParam('data', false);
        /**
         * Get all categories
         */
        $allCats = $objCategory->buildTree($objCategory->getAll(array('sorting ASC'))->toArray());
//        echo "<pre>";print_r($allCats);die; 
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_category', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCategory = $data;
            $newCategory['created_date'] = time();
            /**
             * Sorting
             */
            if (null == $newCategory['sorting']) {
                unset($newCategory['sorting']);
            }
            if (false == $this->checkPermission('new_category', null, '*')) {
                $newCategory['genabled']          = 0;
                $newCategory['sorting']           = 1;
            }
        	
            /**
             * Remove empty images
             */
            if (is_array($newCategory['images'])) {
                foreach ($newCategory['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newCategory['images'][$index]);
                    } else {
                        $newCategory['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newCategory['images'] = implode('||', $newCategory['images']);
        	foreach ($allLangs as $index => $lang) {
            	if($newCategory[$lang['lang_id']]['alias'] == ""){
            		$newCategory[$lang['lang_id']]['alias'] = $objBank->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
            		$newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newCategory[$lang['lang_id']]['alias']));
            	}
            }
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > @$newCategory['sorting']) {
                    $newCategory['sorting'] = 1;
                }
                if (null == $newCategory['parent_id']) {
                	$newCategory['parent_id'] = NULL;
                }
                $objCategory->increaseSorting($newCategory['sorting'], 1);

                $gid = $objCategory->insert($newCategory);
                
                /**
                 * Update id string
                 */
                $objCategory->update(array('gid_string'	=>	$gid), array('bank_category_gid = ?' => $gid));
                $category = @reset($objCategory->getByColumns(array('bank_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);
                /**
                 * Message
                 */
                $this->session->categoryMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Category is created successfully.')
                        );
                
                $this->_redirect('bank/admin/manage-category');
            } catch (Exception $e) {
            	echo '<pre>';
            	echo print_r($e);
            	echo '<pre>';
            	die;
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Category'));
        $this->view->fullPermisison = $this->checkPermission('new_category', null, '*'); 
        
        $this->view->menu = array(
        	0=>'bank',
        	1=>'new-category-bank'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-newspaper-o',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/bank/admin/manage-category',
        		'name'	=>	Nine_Language::translate('Manager Category Bank')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-plus',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('New Category Bank')
        		)
        	
        );
    }
    

    
    public function editCategoryAction()
    {
        $objCategory     = new Models_BankCategory();
        $objLang    = new Models_Lang();
        $objBank = new Models_Bank();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false); 
        if (false == $gid) {
            $this->_redirect('bank/admin/manage-category');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_category', null, '*'))
        ||  (false != $lid && false == $this->checkPermission('edit_category', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $data   = $this->_getParam('data', false);
        
        /**
         * Get all categories
         */
        $allCats = $objCategory->buildTree($objCategory->getAll(array('sorting ASC'))->toArray());
        
        /**
         * Get all category languages
         */
        
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allCategoryLangs = $objCategory->setAllLanguages(true)->getByColumns(array('bank_category_gid=?' => $gid))->toArray();
//        echo "<pre>";print_r($allCategoryLangs);die; 
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_category', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allCategoryLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCategory = $data;
            /**
             * Sorting
             */
            if (null == $newCategory['sorting']) {
                unset($newCategory['sorting']);
            }
            if (false == $this->checkPermission('new_category', null, '*')) {
                unset($newCategory['genabled']);
                unset($newCategory['sorting']);
            }
        
            /**
             * Remove empty images
             */
            if (is_array($newCategory['images'])) {
                foreach ($newCategory['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newCategory['images'][$index]);
                    } else {
                        $newCategory['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newCategory['images'] = implode('||', $newCategory['images']);
        	foreach ($allLangs as $index => $lang) {
            	if($newCategory[$lang['lang_id']]['alias'] == ""){
            		$newCategory[$lang['lang_id']]['alias'] = $objBank->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
            		$newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newCategory[$lang['lang_id']]['alias']));
            	}
            }
            try {
                /**                
                 * Update
                 */
              	if (null == $newCategory['parent_id']) {
                	$newCategory['parent_id'] = NULL;
                }
                /**
                 * Delete gid in parent
                 */
                $oldCategory = @reset($allCategoryLangs);
//                echo "<pre>";print_r($oldCategory);die; 
                $objCategory->deleteGidString($oldCategory['parent_id'], $oldCategory['gid_string']);
                
                /**
                 * Update new data
                 */
                $objCategory->update($newCategory, array('bank_category_gid=?' => $gid));
                
                /**
                 * Update new id string
                 */
                $category = @reset($objCategory->getByColumns(array('bank_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);
                
                /**
                 * Message
                 */
                $this->session->categoryMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Category is updated successfully.')
                        );
                
                $this->_redirect('bank/admin/manage-category');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allCategoryLangs);
            if (false == $data) {
                $this->session->categoryMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate("Category doesn't exist.")
                                           );
                $this->_redirect('bank/admin/manage-category');
            }
            /**
             * Format image
             */
            $data['images'] = explode('||', $data['images']);
            if (! is_array($data['images'])) {
                $data['images'] = array();
            }
            $data['images'] = array_pad($data['images'], 50, null);
            /**
             * Get all lang categorys
             */
            foreach ($allCategoryLangs as $category) {
                $data[$category['lang_id']] = $category;
            }
            
             /**
             * Get all child category
             */
            $allChildCats = explode(',',trim($data['gid_string'],','));
            unset($allChildCats[0]);
           	foreach($allCats as $key =>	$item) {
           		if (false != in_array($item, $allChildCats)) {
           			unset($allCats[$key]);
           		}
           	}
        }
        /**
         * Remove it and its childs from category list
         */
        $oldData = @reset($allCategoryLangs);
        foreach ($allCats as $index => $item) {
            if (false !== strpos(",{$oldData['gid_string']},", ",{$item['bank_category_gid']},")) {
                unset($allCats[$index]);
            }
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Category'));
        $this->view->fullPermisison = $this->checkPermission('edit_category', null, '*');
         $this->view->menu = array(
        	0=>'bank',
        	1=>'manager-category-bank'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-newspaper-o',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/bank/admin/manage-category',
        		'name'	=>	Nine_Language::translate('Manager Category Bank')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-pencil',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('Edit Category Bank')
        		)
        	
        );
    }

    public function enableCategoryAction()
    {
        $objCategory = new Models_BankCategory;
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('bank/admin/manage-category');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_category', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('genabled' => 1), array('bank_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_category', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('enabled' => 1), array('bank_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('bank/admin/manage-category');
    }

    
    public function disableCategoryAction()
    {
        $objCategory = new Models_BankCategory;
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('bank/admin/manage-category');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_category', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('genabled' => 0), array('bank_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_category', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('enabled' => 0), array('bank_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('bank/admin/manage-category');
    }
    
    public function deleteCategoryAction()
    {
        $objCategory = new Models_BankCategory;
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_category')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid = $this->_getParam('gid', false);
        
        if (false == $gid) {
            $this->_redirect('bank/admin/manage-category');
        }
        
        $gids = explode('_', trim($gid, '_'));
        
        try {
            foreach ($gids as $gid) {
            	
            	$cat = @reset($objCategory->getByColumns(array('bank_category_gid=?'=>$gid))->toArray());
            	if ( 0 == $cat['bank_deleteable']){
            		$this->session->categoryMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete category ('. $gid .'). Please try again')
                                           );
                    $this->_redirect('bank/admin/manage-category');
            	}
            	else {
            		$objCategory->delete(array('bank_category_gid=?' => $gid));
            	}
            }
            $this->session->categoryMessage = array(
                                               'success' => true,
                                               'message' => Nine_Language::translate('Category is deleted successfully')
                                           );
        } catch (Exception $e) {
            $this->session->categoryMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete this category. Please try again')
                                           );
        }
        $this->_redirect('bank/admin/manage-category');
    }
    public function changeStringAction()
    {
    	$objBank = new Models_Bank();
    	$str = $this->_getParam("string","");
    	$str = $objBank->convert_vi_to_en($str);
    	$str = str_replace(" ", "-", trim($str));
    	echo $str;die;
    }
}