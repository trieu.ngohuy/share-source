<?php
require_once 'modules/content/models/Content.php';
require_once 'modules/content/models/ContentCategory.php';
require_once 'modules/user/models/User.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/product/models/EstoreCategory.php';
require_once 'modules/banner/models/Banner.php';
require_once 'modules/bank/models/Bank.php';
require_once 'modules/bank/models/ConnectBank.php';
require_once 'modules/bank/models/BankCategory.php';

include_once 'modules/mail/models/Mail.php';
include_once 'modules/mail/models/phpmailer.php';
include_once 'modules/mail/models/smtp.php';
require_once 'modules/buying/models/Buying.php';
require_once 'modules/buying/models/BuyingCategory.php';

class bank_IndexController extends Nine_Controller_Action {

	private function _redirectToNotFoundPage() {
		$this->_redirect ( "" );
	}
	public function getBankAction()
	{
		$id = $this->_getParam("id",false);
		$this->setLayout('default');
		if($id != ''){
			$objBank = new Models_Bank();
	        $objLang    = new Models_Lang();
	        $objCat     = new Models_BankCategory();
			$tradeShow = $objBank->getBankByGid($id);
			$this->view->data = $tradeShow;
		}else{
			$this->view->data = '';
		}
	}
public function sendmail_smtp($data)
    {
        // Mail
        define('MAIL_PROTOCOL', 'smtp');
        define('MAIL_HOST', 'ssl://smtp.googlemail.com');
        define('MAIL_PORT', '465');
        define('MAIL_USER', 'luutn.it@gmail.com');
        define('MAIL_PASS', 'Linh2204');
        define('MAIL_SEC', 'ssl');
        define('MAIL_FROM', 'no.reply.C&D@gmail.com');


        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPAuth = TRUE;
        $mail->SMTPSecure = MAIL_SEC;
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASS;
        $mail->CharSet = "UTF-8";
        $mail->WordWrap = 50;

        $mail->IsHTML(TRUE);
        $mail->From = MAIL_USER;
        $mail->FromName = $data['name_from'];
        $to = $data['email_to'];
        $name = $data['name_to'];
        if (is_array($to)) {
            foreach ($to as $key => $sto) {
                $mail->AddAddress($sto, '');
            }
        } else {
            $mail->AddAddress($to, $name);
        }

        $mail->AddReplyTo(MAIL_USER, "C&D");
        $mail->Subject = $data['title_email'];
        $mail->Body = $data['content'];
        $mail->AltBody = $data['content'];
        if (!$mail->Send()) {
            return false;
        } else {
            return true;
        }
    }
	public function bankDetailAction()
	{
		$objBank = new Models_Bank();
        $objLang    = new Models_Lang();
        $objCat     = new Models_BankCategory();
		$objUser = new Models_User();
		$gid = $this->_getParam("gid",false);
		$objBuyingCategory = new Models_BuyingCategory();
        $objBuying = new Models_Buying();
       	$objBankConnect = new Models_ConnectBank();
       	
		$this->setLayout('front-default-category');
		
		$bank = current($objBankConnect->getByColumns(array('connect_id=?' => $gid))->toArray());
		
       	$buyingRequest = current($objBuyingCategory->getByColumns(array('buying_category_gid=?' => $bank['buying_category_gid'] ,'lang_id=?'=> Nine_Language::getCurrentLangId() ))->toArray());
       	
       	$user_a = $objUser->getUser($buyingRequest['user_id']);
       	$qoute = current($objBuying->getByColumns(array('buying_category_gid=?' => $bank['buying_category_gid'],'enabled = ? ' => 2,'lang_id=?'=> Nine_Language::getCurrentLangId()))->toArray());
       	$user_b = $objUser->getUser($qoute['user_id']);
       	$this->view->user_a = $user_a;
       	$this->view->user_b = $user_b;
       	$this->view->buyingRequest = $buyingRequest;
       	$this->view->qoute = $qoute;
       	$allTradeShow = $objBank->getAllEnableBank();
		$this->view->allTradeShow = $allTradeShow;
		
		$tradeShow = $objBank->getBankByGid($bank['bank_gid']);
		$this->view->data = $tradeShow;
		$this->view->bank = $bank;
			
			
		$this->setLayout('front-default-category');
		
		
        $this->view->menu = 'bank';
		
		
	}
	public function managerAction()
	{
		$objBank = new Models_Bank();
        $objLang    = new Models_Lang();
        $objCat     = new Models_BankCategory();
		$objUser = new Models_User();
		
		$objBuyingCategory = new Models_BuyingCategory();
        $objBuying = new Models_Buying();
       	$objBankConnect = new Models_ConnectBank();
       	
		$this->setLayout('front-default-category');
		
		
         $allContact = $objBankConnect->getAll()->toArray();
         foreach ($allContact as &$item){
         	
         	$buyingRequest = current($objBuyingCategory->getByColumns(array('buying_category_gid=?' => $item['buying_category_gid'],'lang_id=?'=> Nine_Language::getCurrentLangId() ))->toArray());
	       	$user_a = $objUser->getUser($buyingRequest['user_id']);
	       	$qoute = current($objBuying->getByColumns(array('buying_category_gid=?' =>  $item['buying_category_gid'],'enabled = ? ' => 2,'lang_id=?'=> Nine_Language::getCurrentLangId()))->toArray());
	       	$user_b = $objUser->getUser($qoute['user_id']);
	       	$item['user_a'] = $user_a;
	       	$item['user_b'] = $user_b;
	       	$item['buying'] = $buyingRequest;
	       	$item['qoute'] = $qoute;
         }
         unset($item);
			
            $this->view->allContact = $allContact;
            $this->view->menu = 'bank';
		
		
	}
	
	public function connectAction()
	{
		$objBank = new Models_Bank();
        $objLang    = new Models_Lang();
        $objCat     = new Models_BankCategory();
		$objUser = new Models_User();
		
		$gid = $this->_getParam("gid",false);
		
		$objBuyingCategory = new Models_BuyingCategory();
        $objBuying = new Models_Buying();
       	$objBankConnect = new Models_ConnectBank();
       	
       	$buyingRequest = current($objBuyingCategory->getByColumns(array('buying_category_gid=?' => $gid,'lang_id=?'=> Nine_Language::getCurrentLangId() ))->toArray());
       	
       	$user_a = $objUser->getUser($buyingRequest['user_id']);
       	$qoute = current($objBuying->getByColumns(array('buying_category_gid=?' => $gid,'enabled = ? ' => 2,'lang_id=?'=> Nine_Language::getCurrentLangId()))->toArray());
       	$user_b = $objUser->getUser($qoute['user_id']);
       	$this->view->user_a = $user_a;
       	$this->view->user_b = $user_b;
       	$this->view->buyingRequest = $buyingRequest;
       	$this->view->qoute = $qoute;
       	$allTradeShow = $objBank->getAllEnableBank();
		$this->view->allTradeShow = $allTradeShow;
		$this->setLayout('front-default-category');
		
		$data = $this->_getParam('data', false);



            $errors = array();
            if (false !== $data) {
            	
                $newContact = $data;
                $newContact['created_date'] = time();
                $newContact['user_a'] = $user_a['user_id'];
                $newContact['view'] = 0;
                $newContact['user_b'] = $user_b['user_id'];
                $newContact['buying_category_gid'] = $gid;
                
                    
                /**
                 * Change format date
                 */


                try {
                    /**
                     * Increase all current sortings
                     */
						$userSession = Nine_Registry::getLoggedInUser();
                        if (isset($_FILES['images']) && $_FILES['images']['tmp_name'] != '') {
                            $fileName = basename($_FILES['images']['name']);
                            $fileTmp = $_FILES['images']['tmp_name'];
                            $uploadPath = 'media/userfiles/user/' . $userSession['user_id'] . '/';
                            $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                            $uploadFile = $uploadDir . $fileName;

                            $ext_allow = array(
                                'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX','txt'
                            );
                            $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
                            if (in_array($ext, $ext_allow)) {
                                move_uploaded_file($fileTmp, $uploadFile);
                                $newContact['file'] = $uploadPath . $fileName;
                            }
                        }
                        
	                    $bank_account = current($objUser->getByColumns(array('group_id=?' => 2))->toArray());
						
	                    
	                    $content = '<p style="margin-bottom:15px; font-size: 16px; line-height: 25px;">';
	                    $content .= Nine_Language::translate('Kết Nối Tài Chính');
	                    $content .= '</p>';
	                    $content .= '<p><b>' . Nine_Language::translate('Thành viên vừa gửi yêu cầu kết nối tài chính cho bạn . Vui lòng đăng nhập vào tài khoản bank account tại C&D để xem thông tin chi tiết.') . '</b></p>';
	                    
	                    
	                    $data_send = array(
	                        'name_to' => "User",
	                        'email_to' => array($bank_account['email']),
	                        'title_email' => Nine_Language::translate('Yêu cầu kết nối tài chính - C&D'),
	                        'name_from' => Nine_Language::translate("Admin C&D"),
	                        'content' => $content
	                    );
	                    $this->sendmail_smtp($data_send);
	                    
                    
                    
                        $objBankConnect->insert($newContact);

                    $this->_redirect('user/buying-request/'.$buyingRequest['alias']);

                } catch (Exception $e) {
                    echo '<pre>';
                    echo print_r($e);
                    echo '<pre>';
                    die;
                    $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
                }


            }

		/*Set Pagination*/
		
		
		
	}

	public function indexAction()
	{
		$objBank = new Models_Bank();
        $objLang    = new Models_Lang();
        $objCat     = new Models_BankCategory();
		$objUser = new Models_User();

		$this->setLayout('front-default-category');

		/*Set Pagination*/
		$gid = $this->_getParam("gid",false);
		
		$numRowPerPage = 10;
		$currentPage = $this->_getParam("page",false);
		if($currentPage == false){
			$currentPage = 1;
		}
		$condition = array(
			'bank_category_gid' => $gid
		);
		$allTradeShow = $objBank->getAllEnableBank($condition, 'created_date DESC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
		foreach ($allTradeShow as &$item) {
			$item['intro_text'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $item ['full_text'] ) ), 250 );
			$item ['images'] = Nine_Function::getThumbImage ( @$item ['images'], 345, 125 , false , false );
			unset($item);
		}
		
		/**
		 * Count all category
		 */
		$objBanner = new Models_Banner();
		$this->view->adv = $objBanner->getAllBanner(array('user_id'=>1,'banner_category_gid'=>141,'page'=>4,'location'=>1), 'sorting');
		$count = count($objBank->getAllEnableBank($condition));
		$this->setPagination($numRowPerPage, $currentPage, $count);

		$this->view->pageTitle = Nine_Language::translate('Tài Chính Ngân Hàng');
		$this->view->allTradeShow = $allTradeShow;
		$this->view->allItems = $count;
		$this->view->firstItemInPage = ($currentPage - 1) * $numRowPerPage + 1;
		$this->view->lastItemInPage =($currentPage - 1) * $numRowPerPage + count($objBanner->getAllBanner($condition, 'created_date DESC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage));
	}

	public function detailAction()
	{
		$objBank = new Models_Bank();
        $objLang    = new Models_Lang();
        $objCat     = new Models_BankCategory();

		$this->setLayout('front-default-category');

		$gid = $this->_getParam("gid",false);
		
		$tradeShow = $objBank->getBankByGid($gid);
		$newTradeShow = $objBank->getAllEnableBank(array(), 'created_date DESC', 10);
		foreach ($newTradeShow as &$item) {
			$item['intro_text'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $item ['full_text'] ) ), 250 );
			$item ['images'] = Nine_Function::getThumbImage ( @$item ['images'], 270, 160 , false , false );
			unset($item);
		}
		$objBanner = new Models_Banner();
		$this->view->adv = $objBanner->getAllBanner(array('user_id'=>1,'banner_category_gid'=>141,'page'=>4,'location'=>1), 'sorting');
		$this->view->pageTitle = $tradeShow['title'];
		$this->view->tradeShow = $tradeShow;
		$this->view->newTradeShow = $newTradeShow;

	}

}