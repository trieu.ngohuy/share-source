<?php
/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2009 visualidea.org
 * @license    http://license.visualidea.org
 * @version    v 1.0 2009-04-15
 */
require_once 'Nine/Model.php';
require_once 'modules/bank/models/BankCategory.php';

class Models_Bank extends Nine_Model
{ 
    /**
     * The primary key column or columns.
     * A compound key should be declared as an array.
     * You may declare a single-column primary key
     * as a string.
     *
     * @var mixed
     */
    protected $_primary = 'bank_id';
    /**
     * The field name what we use to group all language rows together
     * 
     * @var string
     */
    protected $_groupField = 'bank_gid';
    /**
     * Let system know this is miltilingual table or not.
     * If this table has multilingual fields, Zend_Db_Table_Select object
     * will be inserted language condition automatically.
     * 
     * @var array
     */
    protected $_multilingualFields = array('title', 'alias', 'intro_text', 'full_text', 'hit','file',
                                           'last_view_date',  'meta_keywords', 'meta_description', 'enabled','tag','add','time_start','seo_title','seo_keywords','seo_description');
    
    /**
     * Constructor.
     *
     * Supported params for $config are:
     * - db              = user-supplied instance of database connector,
     *                     or key name of registry instance.
     * - name            = table name.
     * - primary         = string or array of primary key(s).
     * - rowClass        = row class name.
     * - rowsetClass     = rowset class name.
     * - referenceMap    = array structure to declare relationship
     *                     to parent tables.
     * - dependentTables = array of child tables.
     * - metadataCache   = cache for information from adapter describeTable().
     *
     * @param  mixed $config Array of user-specified config options, or just the Db Adapter.
     * @return void
     */
    public function __construct($config = array())
    {
        $this->_name = $this->_prefix . 'bank';
        return parent::__construct($config); 
    }

    /**
     * Get all bank with conditions
     * 
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
//    public function getAllBanksWithDefaultLang($condition = array(), $order = null, $count = null, $offset = null)
//    {
//        $select = $this->select()
//                ->setIntegrityCheck(false)
//                ->from(array('s' => $this->_name), array('bank_id', 'senabled' => 'enabled', 'publish_up_date' => 'publish_up_date', 'publish_down_date' => 'publish_down_date' , 'ssorting' => 'sorting', 'created_date' => 'created_date'))
//                ->join(array('sl' => $this->_prefix . 'bank_lang'), 's.bank_id = sl.bank_id')
//                ->join(array('sc' => $this->_prefix . 'bank_category'), 's.bank_category_gid = sc.bank_category_gid', array('cname' => 'name'))
//                ->order($order)
//                ->limit($count, $offset);
//        /**
//         * Conditions
//         */
//        if (null != @$condition['keyword']) {
//            $select->where($this->getAdapter()->quoteInto('sl.title LIKE ?', "%{$condition['keyword']}%"));
//        }
//        if (null != @$condition['bank_category_gid']) {
//            $select->where("s.bank_category_gid = ?", $condition['bank_category_gid']);
//        }
//        if (null != @$condition['lang_id']) {
//            $select->where("sl.lang_id = ?", $condition['lang_id']);
//        }
//        
//        return $this->fetchAll($select)->toArray();
//    }

    /**
     * Get all bank with conditions
     * 
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
	public function getAllBankByRandom() 
    {
    	$select = $this->select()
                ->where('genabled = 1');
         
         $idString = '';
         $sql = "SELECT bank_gid FROM {$this->_name} Where tag != '' AND lang_id = '".Nine_Language::getCurrentLangId()."' ORDER BY RAND() LIMIT 20";
         $ids = $this->_db->fetchAll($sql);
     
            foreach ($ids as $row) {
                $idString .= $row['bank_gid'] . ',';
            }
    	if($idString == ""){
        	return array();
        }
        $select->where('bank_gid IN (' . trim($idString, ',') .')');
        $select->where('bank_category_gid !=?', '116');
        $select->where('bank_category_gid !=?', '118');
        $select->where('lang_id=?', Nine_Language::getCurrentLangId());
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllBankByParentRandom($parent_id) 
    {
    	$select = $this->select()
                ->where('genabled = 1');
         
         
         $sqlcate = "SELECT bank_category_gid FROM 9_bank_category Where parent_id = '".$parent_id."' AND lang_id = '".Nine_Language::getCurrentLangId()."'";
         $idCatAll = '';
     
    	$idcates = $this->_db->fetchAll($sqlcate);
        foreach ($idcates as $rows) {
           $idCatAll .= $rows['bank_category_gid'] . ',';
        }
    	if($idCatAll == ""){
        	return array();
        }  
        
         $idString = '';
         $sql = "SELECT bank_gid FROM {$this->_name} Where bank_category_gid IN(".trim($idCatAll, ',').") AND tag != '' AND lang_id = '".Nine_Language::getCurrentLangId()."' ORDER BY RAND() LIMIT 20";
         $ids = $this->_db->fetchAll($sql);
     
            foreach ($ids as $row) {
                $idString .= $row['bank_gid'] . ',';
            }
    	if($idString == ""){
        	return array();
        }
        $select->where('bank_gid IN (' . trim($idString, ',') .')');
        $select->where('bank_category_gid !=?', '116');
        $select->where('bank_category_gid !=?', '118');
        $select->where('lang_id=?', Nine_Language::getCurrentLangId());
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllBankByCateRandom($cateid) 
    {
    	$select = $this->select()
                ->where('genabled = 1');
         
                
        
         $idString = '';
         $sql = "SELECT bank_gid FROM {$this->_name} Where bank_category_gid = '".$cateid."' AND tag != '' AND lang_id = '".Nine_Language::getCurrentLangId()."' ORDER BY RAND() LIMIT 20";
         $ids = $this->_db->fetchAll($sql);
     
            foreach ($ids as $row) {
                $idString .= $row['bank_gid'] . ',';
            }
        if($idString == ""){
        	return array();
        }
        $select->where('bank_gid IN (' . trim($idString, ',') .')');
        $select->where('lang_id=?', Nine_Language::getCurrentLangId());
        $select->where('bank_category_gid !=?', '116');
        $select->where('bank_category_gid !=?', '118');
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
    public function getAllBankByDate($dateto , $datefrom , $user_id){
    	$select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->where('c.lang_id=?', 2);
        $select->where('c.user_id= ?' , $user_id);
        $select->where('c.created_date > ?' , $dateto);
        $select->where('c.created_date < ?' , $datefrom);
       	return $this->fetchAll($select)->toArray();
    }
    public function getAllBank($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
    	if (null != @$condition['username']) {
            
            $select->where($this->getAdapter()->quoteInto('u.username LIKE ?', "%{$condition['username']}%" ));
        }
        if (null != @$condition['keyword']) {
            $sql = "SELECT bank_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('alias LIKE ?', "%{$condition['keyword']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['bank_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.bank_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['bank_category_gid']) {
        	
        	$sql = "SELECT gid_string FROM {$this->_prefix}" . 'bank_category' . " WHERE " . $this->getAdapter()->quoteInto('bank_category_gid = ? ', $condition['bank_category_gid']);
        	$ids = @reset($this->_db->fetchAll($sql));
        	if (null == $ids) {
        	    $ids = '0';
        	}
            /**
             * Add to select object
             */
            $select->where('c.bank_category_gid IN (' . trim($ids['gid_string'], ',') .')');
        	
        }
        
    	if (null != @$condition['genabled']) {
        	
        	$sql = "SELECT bank_gid FROM {$this->_name} WHERE genabled = " . $condition['genabled'];
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['bank_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.bank_gid IN (' . trim($idString, ',') .')');
        	
        }

        if(null != @$condition['user_id']) {
            $select->where('c.user_id = ? ', $condition['user_id']);
        }

        if (null != @$condition['bank_category_gid_estore']) {
            $select->where('c.bank_category_gid IN (' . $condition['bank_category_gid_estore'] .')');
        }
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAlltinlienquan($gid,$id)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.bank_category_gid = ?', $gid)
                ->where('c.bank_id <> ?', $id)
                ->order('c.bank_id Desc')
                ->limit(10);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    
	public function getAllBankNew()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.created_date DESC")
                ->limit(3);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    
	public function getAllBankNewByCatGid($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.created_date DESC")
                ->limit(4);
         $select->where('cc.parent_id = ? or cc.parent_id IS NULL and c.bank_category_gid=?' , $gid , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    
	public function getAllBankNewByCatGidIndex($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.genabled=?', 1)
                ->order("c.created_date DESC")
                ->limit(5);
         $select->where('c.bank_category_gid=?' , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllBankNewByCatGidCateLimit($gid , $limit)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.genabled=?', 1)
                ->order("c.created_date DESC")
                ->limit($limit);
         $select->where('c.bank_category_gid=?' , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllBankNewByCatGidDetail($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.created_date DESC");
         $select->where('cc.parent_id = ? or cc.parent_id IS NULL and c.bank_category_gid=?' , $gid , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    public function getAllBankNewByCatGidCon($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.created_date DESC");
         $select->where('c.bank_category_gid=?' , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getBankHotOne()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.like DESC")
                ->limit(1);
        /**
         * Conditions
         */
        
        return @reset($this->fetchAll($select)->toArray());
    }
	public function getBankEvent()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.bank_category_gid=?', '116')
                ->where('c.genabled=?', '1')
                ->order("c.publish_up_date DESC");
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllBankHot()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.like DESC")
                ->limit(5);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllBankView()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.count_view DESC")
                ->order(" c.publish_up_date DESC")
                ->limit(4);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllBankComment()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.comment DESC")
                ->order("c.publish_up_date DESC")
                ->limit(4);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
public function getAllBankLike()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.like DESC")
                ->order("c.publish_up_date DESC")
                ->limit(4);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    
	public function getAllBankTinNhanh()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.tinnhanh=?', '1')
                ->order("c.publish_up_date DESC")
                ->limit(5);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    
    
	public function getAllEnabledBankByCategory( $catGid, $condition = array(), $order = null, $count = null, $offset = null)
    {
    	
    	$objCat = new Models_BankCategory();
    	
    	$cat = @reset($objCat->getByColumns(array('bank_category_gid=?' => $catGid))->toArray());
    	
    	$gidStr = @trim($cat['gid_string'].',0',','); 
    	/**
    	 * Get all enabled categories
    	 */
    	$select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_prefix . 'bank_category'), array('bank_category_gid'))
                ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
                ->where('cc.bank_category_gid IN (' . $gidStr .')');
                
        $cats   = $this->fetchAll($select)->toArray();
        $gidStr = '';
        foreach ($cats as $cat) {
        	$gidStr .= $cat['bank_category_gid'] . ',';
        }
        $gidStr = @trim($gidStr.'0',',');
    	
        /**
         * Get all enabled banks in enabled categories
         */
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->where('bank_category_gid IN (' . $gidStr .')')
                ->order($order)
                ->limit($count, $offset);

         /**
          * Condition
          */       
         if (null != @$condition['exclude_bank_gids']) {
         	$gidStr = trim($condition['exclude_bank_gids'].',0',',');
         	$select->where('bank_gid NOT IN (' . $gidStr .')');
         }
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledBanksearch( $search, $order = null, $count = null, $offset = null)
    {
    	
    	
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->order($order)
                ->limit($count, $offset);
         
        $idString = '';

        $sql = "SELECT bank_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('title LIKE ?', "%{$search}%");
        $ids = $this->_db->fetchAll($sql);
    	
		foreach ($ids as $row) {
     		$idString .= $row['bank_gid'] . ',';
   		}
   		
   		if($idString != ""){
   		}else{
   			$sql = "SELECT bank_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('full_text LIKE ?', "%{$search}%") ;
	   		$ids = $this->_db->fetchAll($sql);
			foreach ($ids as $row) {
	     		$idString .= $row['bank_gid'] . ',';
	   		}
   		}
    	
    	
   		
   		$search = $this->convert_vi_to_en($search);
   		$search = str_replace(" ", "-" , $search);
   		
   		if($idString != ""){
   		}else{
   			$sql = "SELECT bank_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('alias LIKE ?', "%{$search}%");
	   		$ids = $this->_db->fetchAll($sql);
	    	
			foreach ($ids as $row) {
	     		$idString .= $row['bank_gid'] . ',';
	   		}
   		}
    	
       
        $select->where('bank_gid IN (' . trim($idString, ',') .')');
        $select->where('bank_category_gid !=?', '116');
        $select->where('bank_category_gid !=?', '118');
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledBankTag( $search, $order = null, $count = null, $offset = null)
    {
    	
    	
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->order($order)
                ->limit($count, $offset);
         
         $idString = '';
         $sql = "SELECT bank_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('tag LIKE ?', "%{$search}%");
         $ids = $this->_db->fetchAll($sql);
         if (empty($ids)) {
         	return array();
            }
            foreach ($ids as $row) {
                $idString .= $row['bank_gid'] . ',';
            }
        
       
        $select->where('bank_gid IN (' . trim($idString, ',') .')');
        $select->where('bank_category_gid !=?', '116');
        $select->where('bank_category_gid !=?', '118');
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledBankTagAdmin( $search, $id_s = null)
    {
    	
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->order("created_date DESC");
         
         $idString = '';
         $sql = "SELECT bank_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('tag LIKE ?', "%{$search}%" )."";
         $ids = $this->_db->fetchAll($sql);
         if (empty($ids)) {
         	return array();
            }
            foreach ($ids as $row) {
                $idString .= $row['bank_gid'] . ',';
            }
        
       	if(trim($id_s, ',') != ""){
       		$select->where('bank_gid NOT IN (' . trim($id_s, ',') .')');
       	}
        $select->where('bank_gid IN (' . trim($idString, ',') .')');
        
        $select->where('lang_id = 2');
        $select->where('bank_category_gid !=?', '116');
        $select->where('bank_category_gid !=?', '118');
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledBankByidUser( $user, $condition = array(), $order = null, $count = null, $offset = null)
    {
    	
    	$objCat = new Models_BankCategory();
    	
                
        
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->where('user_id = ?' , $user)
                ->order($order)
                ->limit($count, $offset);

    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledBankByidUserLike( $user, $condition = array(), $order = null, $count = null, $offset = null)
    {
    	
    	$objCat = new Models_BankCategory();
    	
                
        
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->order($order)
                ->limit($count, $offset);
        $select->where('user_like LIKE ?', ',' . $user .',');

    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledBankByCategory2( $catGid, $condition = array(), $order = null, $count = null, $offset = null)
    {
    	
    	$objCat = new Models_BankCategory();
    	
    	$cat = @reset($objCat->getByColumns(array('bank_category_gid=?' => $catGid))->toArray());
    	
    	$gidStr = @trim($cat['gid_string'].',0',','); 
    	/**
    	 * Get all enabled categories
    	 */
    	$select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_prefix . 'bank_category'), array('bank_category_gid'))
                ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
                ->where('cc.bank_category_gid IN (' . $gidStr .')');
                    
        $cats   = $this->fetchAll($select)->toArray();
        $gidStr = '';
        foreach ($cats as $cat) {
        	$gidStr .= $cat['bank_category_gid'] . ',';
        }
        $gidStr = @trim($gidStr.'0',',');
    	
        /**
         * Get all enabled banks in enabled categories
         */
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->where('bank_category_gid IN (' . $gidStr .')')
                ->order($order)
                ->limit($count, $offset);

         /**
          * Condition
          */       
         if (null != @$condition['exclude_bank_gids']) {
         	$gidStr = trim($condition['exclude_bank_gids'].',0',',');
         	$select->where('bank_gid NOT IN (' . $gidStr .')');
         }
               
    	return $this->fetchAll($select)->toArray();
    }
    public function getLatestBankByCategory( $catGid )
    {
    	$allBank = $this->getAllEnabledBankByCategory($catGid, array(), array('sorting ASC','bank_gid DESC','bank_id DESC'),1,0);
    	return @reset($allBank);
    }
    public function convert_vi_to_en($str) {
	    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
	    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
	    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
	    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
	    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
	    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
	    $str = preg_replace("/(đ)/", 'd', $str);
	    $str = preg_replace("/(D)/", 'd', $str);
	    $str = preg_replace("/(L)/", 'l', $str);
	    $str = preg_replace("/(K)/", 'k', $str);
	    $str = preg_replace("/(Q)/", 'q', $str);
	    $str = preg_replace("/(R)/", 'r', $str);
	    $str = preg_replace("/(T)/", 't', $str);
	    $str = preg_replace("/(N)/", 'n', $str);
	    $str = preg_replace("/(C)/", 'c', $str);
	    $str = preg_replace("/(B)/", 'b', $str);
	    $str = preg_replace("/(M)/", 'm', $str);
	    
	    $str = preg_replace("/(O)/", 'o', $str);
	    $str = preg_replace("/(P)/", 'p', $str);
	    $str = preg_replace("/(S)/", 's', $str);
	    $str = preg_replace("/(G)/", 'g', $str);
	    $str = preg_replace("/(H)/", 'h', $str);
	    $str = preg_replace("/(V)/", 'v', $str);
	    $str = preg_replace("/(X)/", 'x', $str);
	    $str = preg_replace("/(R)/", 'r', $str);
	    $str = preg_replace("/(E)/", 'e', $str);
	    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'a', $str);
	    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'e', $str);
	    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'i', $str);
	    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'o', $str);
	    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'u', $str);
	    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'y', $str);
	    $str = preg_replace("/(Đ)/", 'd', $str);
	    //$str = str_replace(" ", "-", str_replace("&*#39;","",$str));
	 
	    return $str;
	 
	}
    public function increaseSorting($startPos = 1, $num = 1)
    {
        $sql = "UPDATE {$this->_name} SET sorting = sorting + " . intval($num) . " WHERE sorting >= " . intval($startPos);
        
        $this->_db->query($sql);
    }
    
    /**
     * Get bank by gid
     * 
     * @param int $gid
     * @return Zend_Db_Table_Row
     */
    public function getBankByGid($gid)
    {
        $select = $this->select()
        		->where('lang_id=?', Nine_Language::getCurrentLangId())
                ->where('bank_gid=?', $gid);
                
        return current($this->fetchAll($select)->toArray());
    }
//    public function getBankByUrl($url, $langId)
//    {
//        $url = $this->getAdapter()->quote($url);
//        $langId = intval($langId);
//        $time = time();
//        
//        $query = "
//                 SELECT *
//                 FROM ( SELECT * 
//                        FROM {$this->_prefix}bank_lang 
//                        WHERE lang_id = {$langId} AND enabled = 1 AND url = {$url} 
//                       ) AS sl
//                 JOIN ( SELECT bank_id, bank_category_gid, enabled AS senabled, publish_up_date, publish_down_date, sorting AS ssorting, created_date, layout,image
//                        FROM {$this->_prefix}bank 
//                        WHERE enabled = 1 AND publish_up_date <= {$time} AND (publish_down_date = 0 OR publish_down_date > {$time} )
//                       ) AS s
//                 ON s.bank_id = sl.bank_id
//                 LIMIT 0,1
//        ";
////        echo $query;die;
//        $result =$this->_db->fetchRow($query);
//         $array = explode(" ", $result['title']);
//         
//        foreach ($array as $item){
//           $result['titleeach'][] = substr($item,0,1); 
//           $result['titleeach'][] = substr($item,1,strlen($item)-1); 
//        }
//        return $result;
//    }
    public function getEstoreBankPage($gid)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->where('c.bank_id=?', $gid);

        $_bank = $this->fetchAll($select)->current()->toArray();
        $_bank_category_gid =  $_bank['bank_category_gid'];
        switch ($_bank_category_gid) {
            case 27 : $_estoreBankPage = 'manage-promotion';
                break;
            case 29 : $_estoreBankPage = 'manage-news';
                break;
            default : $_estoreBankPage = 'manage-estore-page';
        }
        return $_estoreBankPage;
    }

    public function getAllNewBank($user_id = null, $bank_category_gid = null , $limit = null)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
            ->where('c.enabled = 1 AND c.genabled = 1')
            ->order("c.created_date DESC")
            ->limit($limit);
        /**
         * Conditions
         */

        if(@$user_id != null) {
            $select->where('c.user_id=?', $user_id);
        }

        if(@$bank_category_gid != null) {
            $select->where('c.bank_category_gid=?', $bank_category_gid);
        }

        return $this->fetchAll($select)->toArray();
    }

    public function getAllEnableBank($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->joinLeft(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
            ->joinLeft(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->order($order)
            ->limit($count, $offset);
        /**
         * Conditions
         */
        if (null != @$condition['username']) {

            $select->where($this->getAdapter()->quoteInto('u.username LIKE ?', "%{$condition['username']}%" ));
        }
        if (null != @$condition['keyword']) {
            $sql = "SELECT bank_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('alias LIKE ?', "%{$condition['keyword']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['bank_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.bank_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['bank_category_gid']) {

            $sql = "SELECT gid_string FROM {$this->_prefix}" . 'bank_category' . " WHERE " . $this->getAdapter()->quoteInto('bank_category_gid = ? ', $condition['bank_category_gid']);
            $ids = @reset($this->_db->fetchAll($sql));
            if (null == $ids) {
                $ids = '0';
            }
            /**
             * Add to select object
             */
            $select->where('c.bank_category_gid IN (' . trim($ids['gid_string'], ',') .')');

        }

        if (null != @$condition['genabled']) {

            $sql = "SELECT bank_gid FROM {$this->_name} WHERE genabled = " . $condition['genabled'];
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['bank_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.bank_gid IN (' . trim($idString, ',') .')');

        }

        if(null != @$condition['user_id']) {
            $select->where('c.user_id = ? ', $condition['user_id']);
        }

        if (null != @$condition['bank_category_gid_estore']) {
            $select->where('c.bank_category_gid IN (' . $condition['bank_category_gid_estore'] .')');
        }

        return $this->fetchAll($select)->toArray();
    }

    public function getBankByAlias($alias, $user_id = null, $bank_category_gid = null)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->join(array('cc' => $this->_prefix . 'bank_category'), 'c.bank_category_gid = cc.bank_category_gid', array('cname' => 'name','bank_deleteable' => 'bank_deleteable'))
            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
            ->where('c.enabled = 1 AND c.genabled = 1')
            ->where('c.alias=?', $alias);

        if(@$user_id != null) {
            $select->where('c.user_id=?', $user_id);
        }

        if(@$bank_category_gid != null) {
            $select->where('c.bank_category_gid=?', $bank_category_gid);
        }

        return $this->fetchRow($select);
    }
}