<?php

require_once 'modules/templates/models/Templates.php';

class templates_AdminController extends Nine_Controller_Action_Admin {

    public function manageTemplatesAction() {
        /**
         * Check permission
         */
//        if (false == $this->checkPermission('see_templates')) {
//            $this->_forwardToNoPermissionPage();
//            return;
//        }

        $this->view->headTitle(Nine_Language::translate('Manage Templatess'));
        $this->view->menu = array('managetemplates');

        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page", 1);
        $displayNum = $this->_getParam('displayNum', false);

        /**
         * Get number of lists per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->templatesDisplayNum;
        } else {
            $this->session->templatesDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->templatesCondition;
        } else {
            $this->session->templatesCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }

        /**
         * Get all templates
         */
        $objTemplates = new Models_Templates();
        $allTemplatess = $objTemplates->getAllTemplatess($condition, 'templates_id DESC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
        $url = Nine_Route::url();
        foreach ($allTemplatess as &$value) {
            $value['image'] = $url['path'] . "/" . $value['image'];
        }
        unset($value);
        /**
         * Count all lists
         */
        $count = count($objTemplates->getAllTemplatess($condition));
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allTemplatess = $allTemplatess;
        $this->view->templatesMessage = $this->session->templatesMessage;
        $this->session->templatesMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
    }

    public function newTemplatesAction() {
        /**
         * Check permission
         */
        $objTemplates = new Models_Templates();
//        if (false == $this->checkPermission('new_templates')) {
//            $this->_forwardToNoPermissionPage();
//            return;
//        }

        $data = $this->_getParam('data', false);
        $errors = array();
        if (false !== $data) {

            $target_dir = 'media/userfiles/images/templates/';
            if ($_FILES["image"]["name"] != '') {
                $target_dir = $target_dir . basename($_FILES["image"]["name"]);
                $uploadOk = 1;

                // Check if file already exists
                if (file_exists($target_dir . $_FILES["image"]["name"])) {
                    $errors['erro'] = 1;
                    $errors['mess'] = Nine_Language::translate('Tên File Đã Tồn Tại');
                    $uploadOk = 0;
                }

                // Check file size
                if ($_FILES["image"]["size"] > 20000000) {
                    $errors['erro'] = 1;
                    $errors['mess'] = Nine_Language::translate('File Lớn Hơn 20Mb.');
                    $uploadOk = 0;
                }

                // Only GIF files allowed
                if ($_FILES["image"]["type"] != "image/gif" && $_FILES["image"]["type"] != "image/jpg" && $_FILES["image"]["type"] != "image/png" && $_FILES["image"]["type"] != "image/jpeg") {
                    $errors['erro'] = 1;
                    $errors['mess'] = Nine_Language::translate('Chỉ nhận file png / jpg / gif / jpeg .');
                    $uploadOk = 0;
                }

                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                    
                } else {
                    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir)) {
                        $newTemplates['image'] = $target_dir;
                        $data['image'] = $target_dir;
                    } else {
                        die;
                        $erro['mess'] = Nine_Language::translate('Sever Not Upload File . ');
                    }
                }
            }

            try {
                $data['created_date'] = time();
                $this->session->templatesMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Templates is created successfully.')
                );

                $objTemplates->insert($data);


                $this->_redirect('templates/admin/manage-templates');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        }
        /**
         * Prepare for template
         */
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New templates'));

        $this->view->menu = array(
            0 => 'templates',
            1 => 'new-templates'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-templates',
                'url' => Nine_Registry::getBaseUrl() . 'admin/templates/admin/manage-templates',
                'name' => Nine_Language::translate('Manager Templates')
            ),
            1 => array(
                'icon' => 'fa-templates-plus',
                'url' => '',
                'name' => Nine_Language::translate('New Templates')
            )
        );
    }

    public function editTemplatesAction() {
        $objTemplates = new Models_Templates();

        $errors = array();
        /**
         * Check permission
         */
//        if (false == $this->checkPermission('see_templates')) {
//            $this->_forwardToNoPermissionPage();
//            return;
//        }

        $id = $this->_getParam('id', false);
        $data = $this->_getParam('data', false);
        if ($data != false) {
            $target_dir = 'media/userfiles/images/templates/';
            if ($_FILES["image"]["name"] != '') {
                $target_dir = $target_dir . basename($_FILES["image"]["name"]);
                $uploadOk = 1;

                // Check if file already exists
                if (file_exists($target_dir . $_FILES["image"]["name"])) {
                    $errors['erro'] = 1;
                    $errors['mess'] = Nine_Language::translate('Tên File Đã Tồn Tại');
                    $uploadOk = 0;
                }

                // Check file size
                if ($_FILES["image"]["size"] > 20000000) {
                    $errors['erro'] = 1;
                    $errors['mess'] = Nine_Language::translate('File Lớn Hơn 20Mb.');
                    $uploadOk = 0;
                }

                // Only GIF files allowed
                if ($_FILES["image"]["type"] != "image/gif" && $_FILES["image"]["type"] != "image/jpg" && $_FILES["image"]["type"] != "image/png" && $_FILES["image"]["type"] != "image/jpeg") {
                    $errors['erro'] = 1;
                    $errors['mess'] = Nine_Language::translate('Chỉ nhận file png / jpg / gif / jpeg .');
                    $uploadOk = 0;
                }

                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                    
                } else {
                    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir)) {
                        $data['image'] = $target_dir;
                    } else {
                        die;
                        $erro['mess'] = Nine_Language::translate('Sever Not Upload File . ');
                    }
                }
            }

            try {
                $data['created_date'] = time();
                $this->session->templatesMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Templates is created successfully.')
                );

                $objTemplates->update($data, array('templates_id=?' => $id));


                $this->_redirect('templates/admin/manage-templates');
            } catch (Exception $e) {
                $this->view->data = $data;
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get templates & all comments
             */
            $template = $objTemplates->getTemplates($id);

            if (empty($template)) {
                /**
                 * Templates doesn't exsit
                 */
                $this->session->templatesMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Templates does NOT exist')
                );
                $this->_redirect('templates/admin/manage-templates#listoftemplates');
            }
            /**
             * Prepare for template
             */
            $this->view->data = $template;
        }

        $this->view->headTitle(Nine_Language::translate('Edit templates'));
        $this->view->templatesMessage = $this->session->templatesMessage;
        $this->view->menu = array(
            0 => 'templates',
            1 => 'edit-templates'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-templates',
                'url' => Nine_Registry::getBaseUrl() . 'admin/templates/admin/manage-templates',
                'name' => Nine_Language::translate('Manager Templates')
            ),
            1 => array(
                'icon' => 'fa-templates-plus',
                'url' => '',
                'name' => Nine_Language::translate('Edit Templates')
            )
        );
    }

    public function enableTemplatesAction() {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_templates')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('templates/admin/manage-templates');
        }

        $ids = explode('_', trim($id, '_'));

        $objTemplates = new Models_Templates();
        try {
            foreach ($ids as $id) {
                $objTemplates->update(array('enabled' => 1), array('templates_id=?' => $id));
            }
            $this->session->templatesMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Templates is activated successfully')
            );
        } catch (Exception $e) {
            $this->session->templatesMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT enable this templates. Please try again')
            );
        }
        $this->_redirect('templates/admin/manage-templates#listoftemplates');
    }

    public function disableTemplatesAction() {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_templates')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('templates/admin/manage-templates');
        }

        $ids = explode('_', trim($id, '_'));

        $objTemplates = new Models_Templates();
        try {
            foreach ($ids as $id) {
                $objTemplates->update(array('enabled' => 0), array('templates_id=?' => $id));
            }
            $this->session->templatesMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Templates is deactived successfully')
            );
        } catch (Exception $e) {
            $this->session->templatesMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT disable this templates. Please try again')
            );
        }
        $this->_redirect('templates/admin/manage-templates#listoftemplates');
    }

    public function deleteTemplatesAction() {
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_templates')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $id = $this->_getParam('id', false);

        if (false == $id) {
            $this->_redirect('templates/admin/manage-templates');
        }

        $ids = explode('_', trim($id, '_'));

        $objTemplates = new Models_Templates();
        try {
            foreach ($ids as $id) {
                $objTemplates->delete(array('templates_id=?' => $id));
            }
            $this->session->templatesMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Templates is deleted successfully')
            );
        } catch (Exception $e) {
            $this->session->templatesMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT delete this templates. Please try again')
            );
        }
        $this->_redirect('templates/admin/manage-templates#listoftemplates');
    }

}
