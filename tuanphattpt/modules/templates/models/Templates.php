<?php
/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2009 visualidea.org
 * @license    http://license.visualidea.org
 * @version    v 1.0 2009-04-15
 */
require_once 'Nine/Model.php';
class Models_Templates extends Nine_Model
{ 
    protected $_primary = 'templates_id';
    /**
     * Let system know this is miltilingual table or not.
     * If this table has multilingual fields, Zend_Db_Table_Select object
     * will be inserted language condition automatically.
     * 
     * @var array
     */
    protected $_multilingualFields = array();
    
    public function __construct($config = array())
    {
        $this->_name = $this->_prefix . 'templates';
        return parent::__construct($config); 
    }    
    public function getAlltemplatess($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->order($order)
                ->limit($count, $offset);
               
//        echo "<pre>";print_r($condition);die; 
        /**
         * Conditions
         */
        if (null != @$condition['customer']) {
            $select->where($this->getAdapter()->quoteInto('customer LIKE ?', "%{$condition['customer']}%"));
        }
        if (null != @$condition['category']) {
        	$select->where('category = ?', $condition['category']);
        }
        if (null != @$condition['enabled']) {
        	$select->where('enabled=?', $condition['enabled']);
        }
        
        if (null != @$condition['finished']) {
        	$select->where('finished=?', $condition['finished']);
        }
        
        return $this->fetchAll($select)->toArray();
    }
	
    public function gettemplates( $templatesId) {
    	$select = $this->select()
    			->where('templates_id=?',$templatesId);
    			
    	return @reset($this->fetchAll($select)->toArray());
    }
    
    
}