


<div class="page-header">
    <h1>
        {{l}}Templates{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}List All Templates{{/l}}
        </small>
    </h1>
</div>
<a class="btn btn-lg btn-success" href="?export=true&page=1">
    <i class="ace-icon fa fa-file-excel-o"></i>
    Export 
</a>
<br class="cb">
<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">

                {{if $allTemplatess|@count <= 0}}
                <div class="alert alert-info">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{l}}No templates with above conditions.{{/l}}
                </div>
                {{/if}}

                {{if $templatesMessage|@count > 0 && $templatesMessage.success == true}}
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$templatesMessage.message}}.
                </div>
                {{/if}}

                {{if $templatesMessage|@count > 0 && $templatesMessage.success == false}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$templatesMessage.message}}.
                </div>
                {{/if}}

                <table id="simple-table" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                    <form id="top-search" name="search" method="post" action="">
                        <th class="center">
                            <button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
                        </th>
                        <th class="center">

                            <input class="check-all" type="checkbox" />
                        </th>
                        <th>ID</th>
                        <th class="center"> 
                            {{l}}Templatesname{{/l}}
                            <input  type="text" name="condition[templatesname]" id="templatesname" value="{{$condition.templatesname}}"" placeholder="{{l}}Find Templatesname{{/l}}" class="form-control" />
                        </th>
                        <th>{{l}}Images{{/l}}</th>
                        <th>{{l}}Path{{/l}}</th>
                        <th>{{l}}Created Date{{/l}}</th>
                        <th>{{l}}Sorting{{/l}}</th>
                        <th>{{l}}Enabled{{/l}}</th>
                        <th>{{l}}Action{{/l}}</th>
                    </form>
                    </tr>
                    </thead>

                    <tbody>
                        {{if $allTemplatess|@count > 0}}
                        {{foreach from=$allTemplatess item=item key=key}}
                        <tr>
                            <td class="center">{{$key+1}}</td>
                            <td class="center">
                                <input type="checkbox" value="{{$item.templates_id}}" name="allTemplatess" class="allTemplatess"/>
                            </td>
                            <td>{{$item.templates_id}}</td>
                            <td>{{$item.name}}</td>
                            <td><img src="{{$item.image}}" width="100"/></td>
                            <td>{{$item.path}}</td>
                            <td>{{$item.created_date}}</td>
                            <td>{{$item.sorting}}</td>
                            <td>{{if $item.enabled == 1}}
                                <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png">
                                {{else}}
                                <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png">
                                {{/if}}
                            </td>
                            <td class="center">
                                <span class="tooltip-area">
                                    <a href="{{$APP_BASE_URL}}templates/admin/edit-templates/id/{{$item.templates_id}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a href="javascript:deleteATemplates({{$item.templates_id}});"  class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash-o"></i></a>

                                </span>
                            </td>

                        </tr>
                        {{/foreach}}
                        {{/if}}
                    </tbody>
                </table>


            </div><!-- /.span -->
        </div><!-- /.row -->


        <div class="col-lg-12">
            <div class="form-group col-lg-4">
                <label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
                <div class="col-lg-6">
                    <select id="action" class="form-control" >
                        <option value=";">{{l}}Choose an action...{{/l}}</option>
                        <option value="deleteTemplates();">{{l}}Delete Templates{{/l}}</option>
                        <option value="enableTemplates();">{{l}}Enable Templates{{/l}}</option>
                        <option value="disableTemplates();">{{l}}Disable Templates{{/l}}</option>
                    </select>
                </div>
                <div class="col-lg-6">
                    <a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
                </div>
            </div>
            <div class="form-group col-lg-2">
                <label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
                <div class="col-lg-12">
                    <form class="search" name="search" method="post" action="">
                        <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
                            <option value="5" {{if $displayNum == 5}} selected="selected" {{/if}}>5</option>
                            <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
                            <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
                            <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
                            <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
                            <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
                        </select>
                    </form>
                </div>
            </div>
            {{if $countAllPages > 1}}
            <div class="col-lg-6 pagination">
                {{if $first}}
                <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
                {{/if}}
                {{if $prevPage}}
                <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
                {{/if}}

                {{foreach from=$prevPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>

                {{foreach from=$nextPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                {{if $nextPage}}
                <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
                {{/if}}
                {{if $last}}
                <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
                {{/if}}

            </div>
            {{/if}}
        </div>
    </div>


</div>
</div>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        document.getElementById('templatesname').select();
        document.getElementById('templatesname').focus();
        $('.close').click(function () {
            $(this).parent().hide("slow");
        });
        $('.check-all').click(function () {
            if (this.checked) { // check select status
                $('.allTemplatess').each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.allTemplatess').each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });
    });

    function applySelected()
    {
        var task = document.getElementById('action').value;
        eval(task);
    }
    function enableTemplates()
    {
        var all = document.getElementsByName('allTemplatess');
        var tmp = '';
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
            }
        }
        if ('' == tmp) {
            alert('{{l}}Please choose an templates{{/l}}');
                    }
                    window.location.href = '{{$APP_BASE_URL}}templates/admin/enable-templates/id/' + tmp;
                }

                function disableTemplates()
                {
                    var all = document.getElementsByName('allTemplatess');
                    var tmp = '';
                    for (var i = 0; i < all.length; i++) {
                        if (all[i].checked) {
                            tmp = tmp + '_' + all[i].value;
                        }
                    }
                    if ('' == tmp) {
                        alert('{{l}}Please choose an templates{{/l}}');
                                }
                                window.location.href = '{{$APP_BASE_URL}}templates/admin/disable-templates/id/' + tmp;
                            }

                            function deleteTemplates()
                            {
                                var all = document.getElementsByName('allTemplatess');
                                var tmp = '';
                                var count = 0;
                                for (var i = 0; i < all.length; i++) {
                                    if (all[i].checked) {
                                        tmp = tmp + '_' + all[i].value;
                                        count++;
                                    }
                                }
                                if ('' == tmp) {
                                    alert('{{l}}Please choose an templates{{/l}}');
                                                return;
                                            } else {
                                                result = confirm('{{l}}Are you sure you want to delete{{/l}} ' + count + ' {{l}}templates(s){{/l}}?');
                                                if (false == result) {
                                                    return;
                                                }
                                            }
                                            window.location.href = '{{$APP_BASE_URL}}templates/admin/delete-templates/id/' + tmp;
                                        }


                                        function deleteATemplates(id)
                                        {
                                            result = confirm('{{l}}Are you sure you want to delete this templates{{/l}}?');
                                            if (false == result) {
                                                return;
                                            }
                                            window.location.href = '{{$APP_BASE_URL}}templates/admin/delete-templates/id/' + id;
                                        }
</script>
