<?php
return array (
    'permissionRules' => array(
                        'see_payment' => array(//External permission
                                            'See all articles',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission interface
                        'new_payment' => array(//External permission
                                            'Create new article',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
                        'edit_payment' => array(//External permission
                                            'Edit existed articles',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
						
						'genabled_payment' => 'Genabled existed articles',
                        'delete_payment' => 'Delete existed articles',
  
));
