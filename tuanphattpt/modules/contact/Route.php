<?php
class Route_Contact
{
    public function build($linkArr, $params = array())
    {
        /*
         * Check current language
         */
        if(Nine_Language::getCurrentLangCode() == 'vi'){
                $result = "lien-he.html";
            }else{
                $result = "contact-us.html";
            }
            return $result;
    }

    public function parse()
    {
        $router = Nine_Controller_Front::getInstance()->getRouter();
        /*
         * Contact (En)
         */
        $contactEn = new Zend_Controller_Router_Route_Regex(
            'contact-us.html',
            array(
                'module'     => 'contact',
                'controller' => 'index',
                'action'     => 'contact-us'
            )
        );
        $router->addRoute('contactEn', $contactEn);
        /*
         * Contact (Vi)
         */
        $contactVi = new Zend_Controller_Router_Route_Regex(
            'lien-he.html',
            array(
                'module'     => 'contact',
                'controller' => 'index',
                'action'     => 'contact-us'
            )
        );
        $router->addRoute('contactVi', $contactVi);
    }
}