<div class="page-header">
    <div class="container">
        <h1>{{l}}Liên hệ{{/l}}</h1>
    </div>
</div>
<div class="container small-content">
    <div class="row justify-content-lg-center">
        <div class="content col-lg-8">
            {{if isset($err)}}
            <div class="alert-message" style="{{if $err.status === true}}background-color: green;{{else}}background-color: red;{{/if}}">{{$err.message}}</div>
            {{/if}}
            <div class="map" style="overflow: hidden;">
                <iframe width="100%" height="400" frameborder="0" style="border:0" src="{{$config.google_map}}" allowfullscreen></iframe>
            </div>
            <hr id="diachi">
            {{if $currentLangCode === 'en'}}
            {{$config.HeadQuaters_en}}
            {{else}}
            {{$config.HeadQuaters}}
            {{/if}}
            <hr/>
            <h3 id="bieumaulienhe">{{l}}BIỂU MẪU LIÊN HỆ{{/l}}</h3>
            <p>&nbsp;</p>
            <div class="nf-form-content ">
                <form role="form" data-toggle="validator" method="post" action="{{$BASE_URL}}contact-us.html">
                    <nf-fields-wrap>
                        <nf-field>
                            <div id="nf-field-1-container" class="nf-field-container textbox-container  label-above ">
                                <div class="nf-before-field">
                                    <nf-section></nf-section>
                                </div>
                                <div class="nf-field">
                                    <div id="nf-field-1-wrap" class="field-wrap textbox-wrap" data-field-id="1">


                                        <div class="nf-field-label"><label for="nf-field-1" id="nf-label-field-1" class="">{{l}}Họ &amp; Tên{{/l}} <span class="ninja-forms-req-symbol">*</span> </label></div>

                                        <div class="nf-field-element"><input type="text" value="" class="ninja-forms-field nf-element" id="nf-field-1" name="data[title]" aria-invalid="false" aria-describedby="nf-error-1" aria-labelledby="nf-label-field-1" required=""></div>


                                    </div>
                                </div>
                                <div class="nf-after-field">
                                    <nf-section>
                                        <div class="nf-input-limit"></div>

                                        <div id="nf-error-1" class="nf-error-wrap nf-error" role="alert"></div>

                                    </nf-section>
                                </div>
                            </div>
                        </nf-field>
                        <nf-field>
                            <div id="nf-field-5-container" class="nf-field-container phone-container  label-above  textbox-container">
                                <div class="nf-before-field">
                                    <nf-section></nf-section>
                                </div>
                                <div class="nf-field">
                                    <div id="nf-field-5-wrap" class="field-wrap phone-wrap textbox-wrap" data-field-id="5">


                                        <div class="nf-field-label"><label for="nf-field-5" id="nf-label-field-5" class="">{{l}}Số điện thoại{{/l}} <span class="ninja-forms-req-symbol">*</span> </label></div>

                                        <div class="nf-field-element"><input type="tel" value="" class="ninja-forms-field nf-element" id="nf-field-5" name="data[phone]" autocomplete="tel" aria-invalid="false" aria-describedby="nf-error-5" aria-labelledby="nf-label-field-5" required=""></div>


                                    </div>
                                </div>
                                <div class="nf-after-field">
                                    <nf-section>
                                        <div class="nf-input-limit"></div>

                                        <div id="nf-error-5" class="nf-error-wrap nf-error" role="alert"></div>

                                    </nf-section>
                                </div>
                            </div>
                        </nf-field>
                        <nf-field>
                            <div id="nf-field-2-container" class="nf-field-container email-container  label-above ">
                                <div class="nf-before-field">
                                    <nf-section></nf-section>
                                </div>
                                <div class="nf-field">
                                    <div id="nf-field-2-wrap" class="field-wrap email-wrap" data-field-id="2">


                                        <div class="nf-field-label"><label for="nf-field-2" id="nf-label-field-2" class="">Email <span class="ninja-forms-req-symbol">*</span> </label></div>

                                        <div class="nf-field-element"><input type="email" value="" class="ninja-forms-field nf-element" id="nf-field-2" name="data[email]" autocomplete="email" aria-invalid="false" aria-describedby="nf-error-2" aria-labelledby="nf-label-field-2" required=""></div>


                                    </div>
                                </div>
                                <div class="nf-after-field">
                                    <nf-section>
                                        <div class="nf-input-limit"></div>

                                        <div id="nf-error-2" class="nf-error-wrap nf-error" role="alert"></div>

                                    </nf-section>
                                </div>
                            </div>
                        </nf-field>
                        <nf-field>
                            <div id="nf-field-3-container" class="nf-field-container textarea-container  label-above ">
                                <div class="nf-before-field">
                                    <nf-section></nf-section>
                                </div>
                                <div class="nf-field">
                                    <div id="nf-field-3-wrap" class="field-wrap textarea-wrap" data-field-id="3">
                                        <div class="nf-field-label"><label for="nf-field-3" id="nf-label-field-3" class="">{{l}}Thông điệp{{/l}} <span class="ninja-forms-req-symbol">*</span> </label></div>

                                        <div class="nf-field-element">
                                            <textarea id="nf-field-3" name="data[full_text]" aria-invalid="false" aria-describedby="nf-error-3" class="ninja-forms-field nf-element" aria-labelledby="nf-label-field-3" required=""></textarea>
                                        </div>

                                    </div>
                                </div>
                                <div class="nf-after-field">
                                    <nf-section>
                                        <div class="nf-input-limit"></div>

                                        <div id="nf-error-3" class="nf-error-wrap nf-error" role="alert"></div>

                                    </nf-section>
                                </div>
                            </div>
                        </nf-field>
                        <nf-field>
                            <div id="nf-field-4-container" class="nf-field-container submit-container  label-above  textbox-container">
                                <div class="nf-before-field">
                                    <nf-section></nf-section>
                                </div>
                                <div class="nf-field">
                                    <div id="nf-field-4-wrap" class="field-wrap submit-wrap textbox-wrap" data-field-id="4">
                                        <div class="nf-field-label"></div>
                                        <div class="nf-field-element"><button id="nf-field-4" class="ninja-forms-field nf-element btn-send" type="submit">{{l}}Gửi{{/l}}</button></div>
                                        <div class="nf-error-wrap"></div>
                                    </div>
                                </div>
                                <div class="nf-after-field">
                                    <nf-section>
                                        <div class="nf-input-limit"></div>

                                        <div id="nf-error-4" class="nf-error-wrap nf-error" role="alert"></div>

                                    </nf-section>
                                </div>
                            </div>
                        </nf-field>
                    </nf-fields-wrap>
                </form>
            </div>
        </div>
    </div>
</div>