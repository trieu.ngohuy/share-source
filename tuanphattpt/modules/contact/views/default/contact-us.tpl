<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        {{sticker name=common_breadcrumbs}}
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">{{l}}Liên hệ{{/l}}</span>
        </h2>
        <!-- ../page heading-->
        <div id="contact" class="page-content page-contact">
            <div id="message-box-conact"></div>
            <div class="row">
                <div class="col-sm-8">
                    <form role="form" data-toggle="validator" method="post" action="{{$BASE_URL}}contact-us.html">
                        <div class="col-sm-6">
                            <div class="form-selector">
                                <label>{{l}}To{{/l}}</label>
                                <select class="form-control input-sm" id="contact_category_gid" name="data[contact_category_gid]" required>
                                    <option value="">---{{l}}Chọn bộ phận hỗ trợ{{/l}}---</option>
                                    {{foreach from=$support item=item}}
                                    <option value="{{$item.contact_category_gid}}">{{$item.name}}</option>
                                    {{/foreach}}
                                </select>
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Fullname{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[tennguoigui]" id="tennguoigui" required />
                                <input type="hidden" class="form-control input-sm" name="data[type_contact]" value="1"/>
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Telephone{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[phone]" id="phone" required/>
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Email{{/l}}</label>
                                <input type="email" class="form-control input-sm" name="data[email]"  id="email" required />
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Company name{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[name_company]" id="name_company" required />
                            </div>
                            <div class="form-selector">
                                <label>{{l}}Company address{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[add_company]" id="add_company" />
                            </div>

                            <br/>
                            <div class="form-selector">
                                <button type="submit" id="btn-contact" class="btn btn-primary">Send</button>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Country{{/l}}</label>
                                <select class="form-control input-sm" name="data[quocgia]" id="quocgia" required>
                                    <option value="">{{l}}Chọn quốc gia{{/l}}</option>
                                    {{foreach from=$country item=item}}
                                    <option value="{{$item.country_category_gid}}">{{$item.name}}</option>
                                    {{/foreach}}
                                </select>
                            </div>
                            <div class="form-selector">
                                <label>{{l}}Business Type{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[type_business]" id="type_business" />
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Subject{{/l}}</label>
                                <input type="text" class="form-control input-sm" name="data[title]" id="title" required />
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Message{{/l}}</label>
                                <textarea class="form-control input-sm" rows="9" name="data[full_text]" id="message" required></textarea>
                            </div>
                            <div class="form-selector">
                                <label><span>*</span> {{l}}Security Code{{/l}}</label>
                                <input type="text" class="form-control" name="captcha" required data-fv-notempty-message="{{l}}The captcha{{/l}}">
                                <div class="box-code"><img src="{{$LAYOUT_HELPER_URL}}front/captcha/create_image.php?r={{$randomNumber}}" style="padding-top:4px;" /></div>
                                    {{if $erro != ''}}
                                <div class="alert alert-danger" >
                                    <button data-dismiss="alert" class="close" type="button">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    <p class="erro">
                                        {{$erro}}
                                    </p>
                                </div>
                                {{/if}}
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-4" id="contact_form_map">
                    <h3 class="page-subheading">Information</h3>
                    <ul class="store_info">
                        <li><i class="fa fa-home"></i>{{$config.WebsiteName}}</li>
                        <li><i class="fa fa-home"></i>{{$config.Add}}</li>
                        <li><i class="fa fa-phone"></i><span>{{$config.hotline}}</span></li>
                        <li><i class="fa fa-empire"></i><span>{{$config.Email}}</span></li>                        
                    </ul>
                </div>
            </div>
            <hr>
            <div clas="mapFrm">
                    <iframe src="{{$config.google_map}}" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->