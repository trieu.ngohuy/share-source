<div class="container">
    <div class="row content">
        <div class="col-md-12 col-sm-12 col-xs-12 content-top">
            <div class="news-label col-md-12 col-sm-12"> {{l}}LIÊN HỆ{{/l}}</div>
            <div class="col-md-6 col-sm-12 col-xs-12 recommend" style="padding:10px;background:#fff;" >
                <div class="account-info" style="border:0px;width:100%;margin:0px;">
                    <label>{{l}}CÔNG TY TNHH TƯ VẤN & THIẾT KẾ GIẢI PHÁP CNTT TRUNG NGHĨA{{/l}}</label>
                    <p><span>{{l}}Địa Chỉ :{{/l}}</span><b>{{l}}324/3 Hoàng Văn Thụ, Phường 4, Quận Tân Bình, Tp. Hồ Chí Minh {{/l}}</b></p>
                    <p><span>{{l}}Điện Thoại :{{/l}}</span><b>{{l}}090 141 8787{{/l}}</b></p>
                    <p><span>{{l}}Mã Số Thuế :{{/l}}</span><b>{{l}}‎0313932240{{/l}}</b></p>
                    <p><span>{{l}}Tài Khoản :{{/l}}</span><b>{{l}}‎060129223459 - Tại Sacombank - PGD Nguyễn Duy Dương{{/l}}</b></p>
                    <p><span>{{l}}Đại Diện :{{/l}}</span><b>{{l}}PHAN PHẠM HIẾU – Giám Đốc{{/l}}</b></p>
                </div>
                <div class="checkout-info col-md-12 col-sm-12 col-xs-12">
                    <label>{{l}}Để lại thông tin liên hệ nếu bạn có nhu cầu?{{/l}}</label>
                    <br class="cb">
                    <div class="form-group">
                        <label for="input-fullname" class="col-sm-4 control-label">{{l}}Họ tên{{/l}}</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="input-fullname" name="data[name]">
                        </div>
                        <br class="cb">
                    </div>
                    <div class="form-group">
                        <label for="input-phone" class="col-sm-4 control-label">{{l}}Điện thoại{{/l}}</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="input-phone" name="data[phone]">
                        </div>
                        <br class="cb">
                    </div>
                    <div class="form-group">
                        <label for="input-email" class="col-sm-4 control-label">{{l}}Email{{/l}}</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="input-email" name="data[email]">
                        </div>
                        <br class="cb">
                    </div>
                    <div class="form-group">
                        <label for="input-email" class="col-sm-4 control-label">{{l}}Nội Dung{{/l}}</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="input-email" name="data[message]"></textarea>
                        </div>
                        <br class="cb">
                    </div>
                    <div class="form-group">
                        <label for="input-email" class="col-sm-4 control-label" >{{l}}Captcha{{/l}}</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="input-email" name="data[captcha]">
                            <div class="box-code"><img src="{{$LAYOUT_HELPER_URL}}front/captcha/create_image.php?r={{$randomNumber}}" style="padding-top:4px;" /></div>
                        </div>
                        <br class="cb">
                    </div>
                    <div class="place-order text-center col-md-12 col-sm-12 col-xs-12" style="padding: 20px;">
                        <button class="btn btn-default" type="submit">{{l}}GỬI ĐƠN{{/l}}</button>
                    </div>
                </div>
            </div>

            <div class="col-md-5 col-sm-12 col-xs-12 map" style="padding:10px;background:#fff;float:right" >
                <div class="home-map col-md-12 col-xs-12" style="margin:0px;padding:0px;">
                    <!--MAP-->
                    <script src='https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBLozyHdzhBfYMVFJKvYlPOMok4P15pLr8'></script>
                    <div style='overflow:hidden;height:530px;width:100%;'>
                        <div id='gmap_canvas' style='height:530px;width:100%;'>

                        </div>
                    </div>
                    <script type='text/javascript'>
                        function init_map() {
                            var iconBase = 'images/';
                            var myOptions = {
                                zoom: 14,
                                center: new google.maps.LatLng(10.797979, 106.658024),
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            };
                            map = new google.maps.Map(
                                    document.getElementById('gmap_canvas'),
                                    myOptions
                                    );
                            marker = new google.maps.Marker(
                                    {
                                        map: map,
                                        position: new google.maps.LatLng(10.797979, 106.658024),
                                    }

                            );
                            // infowindow = new google.maps.InfoWindow({
                            // 	content:'<strong>Title</strong><br>Ho Chi Minh<br>'
                            // });
                            // google.maps.event.addListener(
                            // 	marker, 
                            // 	'click', 
                            // 	function(){
                            // 		infowindow.open(map,marker);
                            // 		}
                            // 	);
                            // infowindow.open(map,marker);
                        }
                        google.maps.event.addDomListener(window, 'load', init_map);
                    </script>
                    <!--END MAP-->
                    <!-- <img src="images/map.png"> -->

                </div>
            </div>
        </div>
    </div>

</div>
<!--CSS BLOCK-->
<style>
    .homeslider{
        display: none;
    }
    .vertical-menu-content{
        display: none !important;
    }
    .title:hover + div{
        display: block;
    }
</style>