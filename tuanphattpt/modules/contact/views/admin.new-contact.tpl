


<div class="page-header">
	<h1>
		{{l}}Contact{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}List All Contact{{/l}}
		</small>
	</h1>
</div>
<a class="btn btn-lg btn-success" href="?export=true&page=1">
	<i class="ace-icon fa fa-file-excel-o"></i>
	Export
</a>
<br class="cb">
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">

				{{if $allContact|@count <= 0}}
				<div class="alert alert-info">
					<button class="close" data-dismiss="alert">
						<i class="ace-icon fa fa-times"></i>
					</button>
					{{l}}No new contact.{{/l}}
				</div>
				{{/if}}

				{{if $contactMessage|@count > 0 && $contactMessage.success == true}}
				<div class="alert alert-success">
					<button class="close" data-dismiss="alert">
						<i class="ace-icon fa fa-times"></i>
					</button>
					{{$categoryMessage.message}}.
				</div>
				{{/if}}

				{{if $contactMessage|@count > 0 && $contactMessage.success == false}}
				<div class="alert alert-danger">
					<button class="close" data-dismiss="alert">
						<i class="ace-icon fa fa-times"></i>
					</button>
					{{$contactMessage.message}}.
				</div>
				{{/if}}


				<table id="simple-table" class="table table-striped table-bordered table-hover">
					<thead>
					<tr>
						<form id="top-search" name="search" method="post" action="">
							<th class="center">
								<button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
							</th>
							<th class="center">
								<input class="check-all" type="checkbox" />
							</th>
							<th>{{l}}Sender{{/l}}
								<input  type="text" name="condition[username]" id="username" value="{{$condition.username}}"" placeholder="{{l}}Find Username{{/l}}" class="form-control" />
							</th>

							<th>{{l}}Support{{/l}}</th>
							<th>{{l}}Created{{/l}}</th>
							<th>
								{{l}}Kiểu Thư Contact{{/l}}
									<select class=" form-control" name="type" onchange="this.form.submit();">
										<option value="">{{l}}All Contact{{/l}}</option>
										<option value="1" {{if $condition.type_contact == 1}}selected="selected"{{/if}}>{{l}}Liên Hệ Chính{{/l}}</option>
										<option value="2" {{if $condition.type_contact != "" && $condition.type_contact == 2}}selected="selected"{{/if}}>{{l}}Thư Hỏi Hàng{{/l}}</option>
									</select>
								</th>
							<th class="center">
								{{l}}Tình Trạng{{/l}}
								<a href="javascript:document.sortForm.submit();"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/save_16.png" alt="Save sorting"></a>
							</th>
							<th colspan="5">
								<div class="col-sm-12">
									{{l}}Title{{/l}}
								</div>
								<div class="col-sm-4">
									<input  type="text" name="condition[keyword]" id="username" value="{{$condition.keyword}}"" placeholder="{{l}}Find Title{{/l}}" class="form-control" />
								</div>
								<div class="col-sm-4">
									<select class=" form-control" name="condition[contact_category_gid]" onchange="this.form.submit();">
										<option value="">{{l}}All categories{{/l}}</option>
										{{foreach from=$allCats item=item}}
										<option value="{{$item.contact_category_gid}}" {{if $condition.contact_category_gid== $item.contact_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
										{{/foreach}}
									</select>
								</div>
								<div class="col-sm-4">
									<select class=" form-control" name="condition[genabled]" onchange="this.form.submit();">
										<option value="">{{l}}All Published{{/l}}</option>
										<option value="1" {{if $condition.genabled == 1}}selected="selected"{{/if}}>{{l}}Published{{/l}}</option>
										<option value="0" {{if $condition.genabled != "" && $condition.genabled == 0}}selected="selected"{{/if}}>{{l}}No Published{{/l}}</option>
									</select>
								</div>
							</th>
						</form>
					</tr>
					</thead>

					<tbody>
					{{if $allContact|@count > 0}}
					<form action="" method="post" name="sortForm">
						{{foreach from=$allContact item=item name=contact key=key}}
						<tr>
							<td class="center">{{$item.ticket_id}}</td>
							<td class="center">{{if 1 == $item.contact_deleteable}}<input type="checkbox" value="{{$item.contact_gid}}" name="allContact" class="allContact"/>{{else}}<img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/unselect.png" alt="Do not delete, enable, disable" />{{/if}}</td>
							<td>
								{{$item.tennguoigui}}

							</td>
							<td  width="10%">
								{{$item.cname}}
							</td>
							<td>{{$item.created_date}}</td>
							<td class="center">

								<select name="data[{{$item.contact_gid}}]" class="chosen-select form-control">
									<option value="0" {{if $item.enabled == "0"}}selected="selected"{{/if}}>{{l}}Chờ xử lý{{/l}}</option>
									<option value="1" {{if $item.enabled == "1"}}selected="selected"{{/if}}>{{l}}Đang xử lý {{/l}}</option>
									<option value="1" {{if $item.enabled == "2"}}selected="selected"{{/if}}>{{l}}Đã xử lý {{/l}}</option>
								</select>
							</td>
							<td colspan="5" >
								<!-- All languages -->
								<table style="border-collapse: separate;margin-bottom: 0px" class="table table-bordered table-striped">
									<tbody id="table{{$item.contact_id}}">

									{{foreach from=$item.langs item=item2}}
									<tr>
										<td width="95%"><image style="vertical-align:middle;" src="{{$BASE_URL}}{{$item2.lang_image}}"> {{$item2.title}}</td>

										<td width="5%"   class="center">
											{{p name=edit_contact module=contact expandId=$langId}}
											<span class="tooltip-area">
																<a href="{{$APP_BASE_URL}}contact/admin/edit-contact/gid/{{$item.contact_gid}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
															</span>
											{{/p}}
											{{np name=edit_contact module=contact expandId=$langId}}
											--
											{{/np}}
										</td>

									</tr>
									{{/foreach}}

									</tbody>
								</table>
							</td>

						</tr>
						{{/foreach}}
					</form>
					{{/if}}
					</tbody>
				</table>


			</div><!-- /.span -->
		</div><!-- /.row -->


		<div class="col-lg-12">
			<div class="form-group col-lg-4">
				<label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
				<div class="col-lg-6">
					<select id="action" class="form-control" >
						<option value=";">{{l}}Choose an action...{{/l}}</option>
						{{p name=delete_contact module=contact}}
						<option value="deleteContact();">{{l}}Delete{{/l}}</option>
						{{/p}}
						{{if $CheckGenalbel}}
						<option value="enableContact();">{{l}}Enable{{/l}}</option>
						<option value="disableContact();">{{l}}Disable{{/l}}</option>
						{{/if}}
					</select>
				</div>
				<div class="col-lg-6">
					<a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
				</div>
			</div>
			<div class="form-group col-lg-2">
				<label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
				<div class="col-lg-12">
					<form class="search" name="search" method="post" action="">
						<select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
							<option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
							<option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
							<option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
							<option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
							<option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
						</select>
					</form>
				</div>
			</div>
			{{if $countAllPages > 1}}
			<div class="col-lg-6 pagination">
				{{if $first}}
				<a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
				{{/if}}
				{{if $prevPage}}
				<a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
				{{/if}}

				{{foreach from=$prevPages item=item}}
				<a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
				{{/foreach}}

				<a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>

				{{foreach from=$nextPages item=item}}
				<a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
				{{/foreach}}

				{{if $nextPage}}
				<a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
				{{/if}}
				{{if $last}}
				<a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
				{{/if}}

			</div>
			{{/if}}
		</div>
	</div>


</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		$('.close').click(function(){
			$(this).parent().hide( "slow");
		});
		$('.check-all').click(function(){
			if(this.checked) { // check select status
				$('.allContact').each(function() { //loop through each checkbox
					this.checked = true;  //select all checkboxes with class "checkbox1"
				});
			}else{
				$('.allContact').each(function() { //loop through each checkbox
					this.checked = false; //deselect all checkboxes with class "checkbox1"
				});
			}
		});
	});

	function applySelected()
	{
		var task = document.getElementById('action').value;
		eval(task);
	}
	function enableContact()
	{
		var all = document.getElementsByName('allContact');
		var tmp = '';
		for (var i = 0; i < all.length; i++) {
			if (all[i].checked) {
				tmp = tmp + '_' + all[i].value;
			}
		}
		if ('' == tmp) {
			alert('Please choose an contact');
		}
		window.location.href = '{{$APP_BASE_URL}}contact/admin/enable-contact/gid/' + tmp;
	}

	function disableContact()
	{
		var all = document.getElementsByName('allContact');
		var tmp = '';
		for (var i = 0; i < all.length; i++) {
			if (all[i].checked) {
				tmp = tmp + '_' + all[i].value;
			}
		}
		if ('' == tmp) {
			alert('Please choose an contact');
		}
		window.location.href = '{{$APP_BASE_URL}}contact/admin/disable-contact/gid/' + tmp;
	}

	function deleteContact()
	{
		var all = document.getElementsByName('allContact');
		var tmp = '';
		var count = 0;
		for (var i = 0; i < all.length; i++) {
			if (all[i].checked) {
				tmp = tmp + '_' + all[i].value;
				count++;
			}
		}
		if ('' == tmp) {
			alert('Please choose an contact');
			return;
		} else {
			result = confirm('Are you sure you want to delete ' + count + ' contact(s)?');
			if (false == result) {
				return;
			}
		}
		window.location.href = '{{$APP_BASE_URL}}contact/admin/delete-contact/gid/' + tmp;
	}


	function deleteAContact(id)
	{
		result = confirm('Are you sure you want to delete this contact?');
		if (false == result) {
			return;
		}
		window.location.href = '{{$APP_BASE_URL}}contact/admin/delete-contact/gid/' + id;
	}
</script>