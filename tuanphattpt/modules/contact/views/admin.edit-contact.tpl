<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>  
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<script type="text/javascript">
    //<![CDATA[

    jQuery(document).ready(function (){
        CKFinder.setupCKEditor( null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/' );
        jQuery( "#images" ).sortable();
        jQuery( "#images" ).disableSelection();
        //Make slug
        {{foreach from=$allLangs item=item name=langDiv}}
        jQuery('#title{{$smarty.foreach.langDiv.iteration}}').makeSlug({
            slug: jQuery('#alias{{$smarty.foreach.langDiv.iteration}}')
        });
        {{/foreach}}
        	//Display images
            jQuery(".input_image[value!='']").parent().find('div').each( function (index, element){
                jQuery(this).toggle();
            });
    });
    var imgId;
    function chooseImage(id)
    {
        imgId = id;
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();
        finder.basePath = '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/'; // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.selectActionFunction = setFileField;
        finder.popup();
    } 
    // This is a sample function which is called when a file is selected in CKFinder.
    function setFileField( fileUrl )
    {
        document.getElementById( 'chooseImage_img' + imgId ).src = fileUrl;
        document.getElementById( 'chooseImage_input' + imgId).value = fileUrl;
        document.getElementById( 'chooseImage_div' + imgId).style.display = '';
        document.getElementById( 'chooseImage_noImage_div' + imgId ).style.display = 'none';
    }
    function clearImage(imgId)
    {
        document.getElementById( 'chooseImage_img' + imgId ).src = '';
        document.getElementById( 'chooseImage_input' + imgId ).value = '';
        document.getElementById( 'chooseImage_div' + imgId).style.display = 'none';
        document.getElementById( 'chooseImage_noImage_div' + imgId).style.display = '';
    }

    function addMoreImg()
    {
        jQuery("ul#images > li.hidden").filter(":first").removeClass('hidden');
    }

//]]>
</script>
<style type="text/css">
    #images { list-style-type: none; margin: 0; padding: 0;}
    #images li { margin: 10px; float: left; text-align: center;  height: 180px;}
</style>


<div class="page-header">
	<h1>
		{{l}}Contact{{/l}}
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			{{l}}Edit Contact{{/l}}
		</small>
	</h1>
</div>

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
			<div class="row">
				<div class="col-xs-12">
						{{if $errors|@count > 0}}
	                        <div class="alert alert-danger">
	                        	<button class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
									{{if $errors.main}}
	                                   </strong> {{$errors.main}}
	                                {{else}}
	                                   {{l}}Please check following information again{{/l}}
	                                {{/if}} 
							</div>
	                	{{/if}}

	                	<div class="col-sm-12 widget-container-col ui-sortable">
								<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
									<div class="col-md-3" >
										<div class="col-md-12 col-sm-6 widget-container-col">
											<div class="widget-box">
												<div class="widget-header">
													<h5 class="widget-title">Ticket</h5>
												</div>
												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group has-info">
															<label class="control-label col-md-4">{{l}}Status{{/l}}</label>
															<div class="col-md-8 p70">
																<label class="control-label" style="color: #000">
																{{if $data.enabled == "0"}}
																{{l}}Chờ xử lý{{/l}}
																{{elseif $data.enabled == "1"}}
																{{l}}Đang xử lý {{/l}}
																{{else}}
																{{l}}Đã xử lý{{/l}}
																{{/if}}
																</label>
															</div>
														</div>
														<div class="form-group has-info">
															<label class="control-label col-md-4">{{l}}Bộ phận{{/l}}</label>
															<div class="col-md-8 p70">
																<select name="data[contact_category_gid]">
																	{{foreach from=$allCats item=item}}
																	<option value="{{$item.contact_category_gid}}" {{if $data.contact_category_gid == $item.contact_category_gid}} selected {{/if}}>
																		{{$item.name}}
																	</option>
																	{{/foreach}}
																</select>
															</div>
														</div>
														<div class="form-group has-info">
															<label class="control-label col-md-4">{{l}}Created At{{/l}}</label>
															<div class="col-md-8 p70">
																<label class="control-label" style="color: #000">
																	{{$data.created_date|date_format:"%d-%m-%Y %H:%M:%S"}}
																</label>
															</div>
														</div>
														<div class="form-group has-info">
															<label class="control-label col-md-4">{{l}}Updated At{{/l}}</label>
															<div class="col-md-8 p70">
																<label class="control-label" style="color: #000">
																	{{$data.updated_date|date_format:"%d-%m-%Y %H:%M:%S"}}
																</label>
															</div>
														</div>
														<div class="form-group has-info">
															<label class="control-label col-md-4">{{l}}Ticket ID{{/l}}</label>
															<div class="col-md-8 p70">
																<label class="control-label" style="color: #000">
																	LH-{{$data.contact_gid}}
																</label>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="col-md-12 col-sm-6 widget-container-col">
											<div class="widget-box">
												<div class="widget-header">
													<h5 class="widget-title">{{l}}User Details{{/l}}</h5>
												</div>
												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group has-info">
															<label class="control-label col-md-4">{{l}}Người Gửi{{/l}}</label>
															<div class="col-md-8 p70">
																<label class="control-label" style="color: #000">
																	{{$data.tennguoigui}}
																</label>
															</div>
														</div>
														<div class="form-group has-info">
															<label class="control-label col-md-4">{{l}}Email{{/l}}</label>
															<div class="col-md-8 p70">
																<label class="control-label" style="color: #000">
																	{{$data.email}}
																</label>
															</div>
														</div>
														<div class="form-group has-info">
															<label class="control-label col-md-4">{{l}}Phone{{/l}}</label>
															<div class="col-md-8 p70">
																<label class="control-label" style="color: #000">
																	{{$data.phone}}
																</label>
															</div>
														</div>
														<div class="form-group has-info">
															<label class="control-label col-md-4">{{l}}Company{{/l}}</label>
															<div class="col-md-8 p70">
																<label class="control-label" style="color: #000">
																{{$data.name_company}}
																</label>
															</div>
														</div>
														<div class="form-group has-info">
															<label class="control-label col-md-4">{{l}}Address{{/l}}</label>
															<div class="col-md-8 p70">
																<label class="control-label" style="color: #000">
																{{$data.add_company}}
																</label>
															</div>
														</div>
														<div class="form-group has-info">
															<label class="control-label col-md-4">{{l}}Type Business{{/l}}</label>
															<div class="col-md-8 p70">
																<label class="control-label" style="color: #000">
																{{$data.type_business}}
																</label>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-9">
										<div class="col-md-12 col-sm-6 widget-container-col">
											<div class="widget-box">
												<div class="widget-header">
													<h5 class="widget-title">{{$titleContact}}</h5>
												</div>
												<div class="widget-body">
													<div class="widget-main">
														<div class="timeline-container">
															<div class="timeline-items">
																{{foreach from=$allContact item=item key=key}}
																	{{if $item.type == 1}}
																	<div class="timeline-item clearfix">
																		<div class="timeline-info">
																			<i class="timeline-indicator ace-icon fa fa-comment btn btn-grey no-hover"></i>
																		</div>
																		<div class="widget-box widget-color-grey">
																			<div class="widget-header widget-header-small">
																				<h5 class="widget-title smaller">{{l}}Người gửi{{/l}} : {{$data.tennguoigui}}</h5>

																				<span class="widget-toolbar no-border">
																					<i class="ace-icon fa fa-clock-o bigger-110"></i>
																					{{$item.created_date|date_format:"%d-%m-%Y %H:%M"}}
																				</span>
																			</div>

																			<div class="widget-body">
																				<div class="widget-main">
																					{{$item.full_text}}
																					{{if $item.images != null}}
																						<a href="{{$BASE_URL}}{{$item.images}}">{{l}}Download Attached File{{/l}}</a>
																					{{/if}}
																				</div>
																			</div>
																		</div>
																	</div>
																	{{else}}
																	<div class="timeline-item clearfix">
																		<div class="timeline-info">
																			<i class="timeline-indicator ace-icon fa fa-comment btn btn-primary no-hover"></i>
																		</div>
																		<div class="widget-box widget-color-blue2">
																			<div class="widget-header widget-header-small">
																				<h5 class="widget-title smaller">{{l}}Reply{{/l}} : {{$item.tennguoigui}}</h5>
																				<span class="widget-toolbar no-border">
																					<i class="ace-icon fa fa-clock-o bigger-110"></i>
																					{{$item.created_date|date_format:"%d-%m-%Y %H:%M"}}
																				</span>
																			</div>

																			<div class="widget-body">
																				<div class="widget-main">
																					{{$item.full_text}}
																					{{if $item.images != null}}
																						<a href="{{$BASE_URL}}{{$item.images}}">{{l}}Download Attached File{{/l}}</a>
																					{{/if}}
																				</div>
																			</div>
																		</div>
																	</div>
																	{{/if}}
																{{/foreach}}
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12 col-sm-6 widget-container-col">
											<div class="col-md-12">
												<div class="form-group has-info">
													<textarea style="float:left;" class="text-input textarea ckeditor"  name="description" rows="10" cols="40"></textarea>
												</div>
												<div class="form-group has-info">
													<label class="control-label col-md-2">{{l}}Attach File{{/l}}</label>
													<div class="col-md-10">
														<input type="file" id="id-input-file-2" class="form-control" name="images" />
													</div>
												</div>
											</div>

											<div class="col-md-12" >
												<div class="col-md-offset-8 col-md-4" style="padding: 0px;">
													<button class="btn btn-info" type="submit" style="float: right;">
														<i class="ace-icon fa fa-check bigger-110"></i>
														{{l}}Send{{/l}}
													</button>

													<button  style="float: right;" type="button" class="btn btn-warning" onclick="window.location.href='{{$APP_BASE_URL}}contact/admin/close-contact/gid/{{$gid}}'">
														<i class="ace-icon fa fa-close bigger-110"></i>
														{{l}}Close Contact{{/l}}
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
						</div>
				</div><!-- /.span -->
			</div><!-- /.row -->
	</div>
</div>

<script type="text/javascript">
	jQuery(function($) {
		$('#id-input-file-1 , #id-input-file-2').ace_file_input({
			no_file:'No File ...',
			btn_choose:'Choose',
			btn_change:'Change',
			droppable:false,
			onchange:null,
			thumbnail:false //| true | large
			//whitelist:'gif|png|jpg|jpeg'
			//blacklist:'exe|php'
			//onchange:''
			//
		});
	});
</script>
			