<?php
return array (
    'permissionRules' => array(
                        'see_banner' => array(//External permission
                                            'See all articles',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission interface
                        'new_banner' => array(//External permission
                                            'Create new article',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
                        'edit_banner' => array(//External permission
                                            'Edit existed articles',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
						
						'genabled_banner' => 'Genabled existed articles',
                        'delete_banner' => 'Delete existed articles',

                        'see_category' => array(//External permission
                                            'See all categories',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission interface
                        'new_category' => array(//External permission
                                            'Create new category',//Full control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
                        'edit_contact' => array(//External permission
                                            'Edit existed contact',//Fultl control name
                                            '9_lang',//table
                                            'lang_id', //Check permisison column
                                            'name'),//Display column at permission inteface
                        'delete_category' => 'Delete existed categories',
                    ),
  
);
