<?php
class contact_AdminController extends Nine_Controller_Action_Admin 
{
    /*
     * Manager contact
     */
    public function manageContactAction()
    {
        //Set title
        $this->view->headTitle(Nine_Language::translate('Manage Contact '));
        
        /*
         * Paging
         */
        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
    	if($currentPage == false){
        	$currentPage = 1;
        	$this->session->contactDisplayNum = null;
        	$this->session->contactCondition = null;
        }
        if (false === $displayNum) {
            $displayNum = $this->session->contactDisplayNum;
        } else {
            $this->session->contactDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }

        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false !== $condition) {
            $currentPage = 1;
        }
        if (false == $condition || $condition['title'] === '') {
            $condition = array();
        }

        //Get current lang
        $condition['lang_id'] = Nine_Language::getCurrentLangId();
        /**
         * Get all contacts
         */
        $numRowPerPage= $numRowPerPage;
        $allContact = Nine_Read::GetListData('Models_Contact', $condition, array('contact_id ASC'),'index',
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage
                                                  );

        /**
         * Count all contacts
         */
        $count = count(Nine_Read::GetListData('Models_Contact', $condition));

        /**
         * Set values for tempalte
         */
//        echo print_r($condition); die;
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allContact = $allContact;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;

        $this->view->menu = array(
        	0=>'contact',
        	1=>'manager-contact'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-contact-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/contact/admin/manage-contact',
        		'name'	=>	Nine_Language::translate('Manager Contact')
        		)

        );
    }
    
    public function newContactAction()
    {
        $objContact = new Models_Contact();
        $objLang    = new Models_Lang();
        $objCat     = new Models_ContactCategory();


        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_contact', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $this->view->headTitle(Nine_Language::translate('manage Contact '));


        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
        if($currentPage == false){
            $currentPage = 1;
            $this->session->contactDisplayNum = null;
            $this->session->contactCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $objContact->update(array('enabled' => $value), array('contact_gid=?' => $id));
            $this->session->contactMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("Contact Order is edited successfully")
            );
        }

        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->contactDisplayNum;
        } else {
            $this->session->contactDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        $type = $this->_getParam('type', null);
        
        
        if (false === $condition) {
            $condition = $this->session->contactCondition;
        } else {
            $this->session->contactCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        if(@$condition['keyword'] != ''){
            $condition['keyword'] = $objContact->convert_vi_to_en($condition['keyword']);
            $condition['keyword'] = preg_replace("/( )/", '-', $condition['keyword']);
        }
		$condition['type_contact'] = $type;
        /**
         * Get all categories
         */
        $this->view->allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_contact', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        /**
         * Get all contacts
         */
        $numRowPerPage= $numRowPerPage;
        $condition['no_parent'] = true;
        $condition['enabled'] = 0;
        $allContact = $objContact->getAllContact($condition, array('sorting ASC', 'contact_gid DESC', 'contact_id ASC'),
            $numRowPerPage,
            ($currentPage - 1) * $numRowPerPage
        );
//        echo '<pre>';print_r($allContact);die;
        /**
         * Count all contacts
         */
        $count = count($objContact->getallContact($condition));
        /**
         * Format
         */
        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allContact[0]['contact_gid'];
        foreach ($allContact as $index=>$contact) {
            /**
             * Change date
             */
            if (0 != $contact['created_date']) {
                $contact['created_date'] = date($config['dateFormat'], $contact['created_date']);
            } else {
                $contact['created_date'] = '';
            }
            if (0 != $contact['publish_up_date']) {
                $contact['publish_up_date'] = date($config['dateFormat'], $contact['publish_up_date']);
            } else {
                $contact['publish_up_date'] = '';
            }
            if (0 != $contact['publish_down_date']) {
                $contact['publish_down_date'] = date($config['dateFormat'], $contact['publish_down_date']);
            } else {
                $contact['publish_down_date'] = '';
            }
            if ($tmpGid != $contact['contact_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $contact['contact_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $contact;
                $tmp2['hit'] = 0;
            }
            $tmp2['hit']    += $contact['hit'];
            $tmp2['langs'][]  = $contact;
            /**
             * Final element
             */
            if ($index == count($allContact) - 1) {
                $tmp[] = $tmp2;
            }
        }
        $allContact = $tmp;
        $export = $this->_getParam('export', false);
        if($export != false){
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $style = "style = 'border: 1px solid'";

            if(Nine_Language::getCurrentLangId() == 1){
                $header = "<tr>";
                $header .= "<td $style> <b> </b></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> <b>Status </b></td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style>  </td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 0 </td>";
                $header .= "<td $style> Recently Send Request </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style>  </td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 1 </td>";
                $header .= "<td $style> Medium Response Requirements </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style> </td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 2 </td>";
                $header .= "<td $style> Already Browsing Requirements </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 3 </td>";
                $header .= "<td $style> Closed Request </td>";
                $header .= "</tr>";
                $textHeader = array(
                    "<b>No</b>",
                    "<b>Category</b>",
                    "<b>Name User Upload</b>",
                    "<b>Created Date</b>",
                    "<b>Status</b>",
                    "<b>Title</b>"
                );
            }else{
                $header = "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> <b>Tình Trạng Contact </b></td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style>  </td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 0 </td>";
                $header .= "<td $style> Vừa Gửi Yêu Cầu </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style>  </td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 1 </td>";
                $header .= "<td $style> Vừa Trả Lời Yêu Cầu  </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style>  </td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 2 </td>";
                $header .= "<td $style> Đã Duyệt Yêu Cầu </td>";
                $header .= "</tr>";
                $header .= "<tr>";
                $header .= "<td $style></td>";
                $header .= "<td $style></td>";
                $header .= "<td $style> 3 </td>";
                $header .= "<td $style> Đã Đóng Yêu Cầu </td>";
                $header .= "</tr>";

                $textHeader = array(
                    mb_convert_encoding("<b>Số Thứ Tự</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Bộ Phận Hỗ Trợ</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Người Gửi Bài</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ngày Khởi Tạo</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Tình Trạng</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Tiêu Đề</b>",'HTML-ENTITIES','UTF-8')
                );
            }

            $header .= "<tr>";
            foreach ($textHeader as $text) {
                $header .= "<td $style>" . utf8_encode($text) . "</td>";
            }
            $header .= "</tr>";

            $content = '';
            $no = 1;

            foreach ($allContact as $item) {
                $content .="<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['cname'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['tennguoigui'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['enabled'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['title'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .="</tr>";
                $no++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-contact-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();

        }
        /**
         * Set values for tempalte
         */
//        echo print_r($condition); die;
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allContact = $allContact;
        $this->view->contactMessage = $this->session->contactMessage;
        $this->session->contactMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_contact');
        $this->view->EditPermisision = $this->checkPermission('edit_contact');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_contact');
        $this->view->DeletePermisision = $this->checkPermission('delete_contact');
        $this->view->allLangs = $allLangs;

        $this->view->menu = array(
            0=>'contact',
            1=>'new-contact'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-contact-hunt',
                'url'	=>	Nine_Registry::getBaseUrl().'admin/contact/admin/manage-contact',
                'name'	=>	Nine_Language::translate('Manager Contact')
            )

        );

        $ids = $objContact->getAllParent();

        $this->view->ids = $ids;
    }
    public function editContactAction()
    {
        $objContact = new Models_Contact();
        $objCat     = new Models_ContactCategory();
        $objLang    = new Models_Lang();
        $objCountry = new Models_CountryCategory();

        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_contact', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false); 
        if (false == $gid) {
            $this->_redirect('contact/admin/manage-contact');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_contact', null, '*'))
        ||  (false != $lid && false == $this->checkPermission('edit_contact', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $data   = $this->_getParam('data', false);
        
        /**
         * Get all categories
         */
        $allCats = $objCat->buildTree($objCat->getAll(array('sorting ASC'))->toArray());
        /**
         * Get all contact languages
         */
        
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allContactLangs = $objContact->setAllLanguages(true)->getByColumns(array('contact_gid=?' => $gid))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_contact', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allContactLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new contact
             */
        	 $description   = $this->_getParam('description', false);

            /**
             * Insert new contact
             */
            $newContact = $data;
            $newContact['created_date'] = time();
            /**
             * Change format date
             */
            

            try {
                /**
                 * Increase all current sortings
                 */
                if ($description != null) {
                    $userSession = Nine_Registry::getLoggedInUser();
                    $newContact['user_id'] = Nine_Registry::getLoggedInUserId();
                    $newContact['tennguoigui'] = $userSession['full_name'];
                    $newContact['full_text'] = $description;
                    $newContact['parent_id'] = $gid;
                    $newContact[2]['full_text'] = $description;
                    $newContact[1]['full_text'] = $description;
					
                    if(isset($_FILES['images']) && $_FILES['images']['tmp_name'] != '') {
                        $fileName = basename($_FILES['images']['name']);
                        $fileTmp = $_FILES['images']['tmp_name'];
                        $uploadPath = 'media/userfiles/user/' . $userSession['user_id'] . '/';
                        $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                        $uploadFile = $uploadDir . $fileName;

                        $ext_allow = array (
                            'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX'
                        );
                        $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
                        if(in_array($ext, $ext_allow)) {
                            move_uploaded_file($fileTmp, $uploadFile);
                            $newContact['images'] = $uploadPath . $fileName;
                        }
                    }
                    $objContact->insert($newContact);
                    $objContact->update(array('user_view'=> 0), array('contact_gid=?' => $gid));

                    /*Start send mail*/
                    try{

                        $contact = $objContact->getContactByGid($gid);
                        $email = $contact['email'];
                        $data['username'] = $contact['tennguoigui'];
                        $data['message'] = $description;
                        $data['websitename'] = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost();
                        $objMail = new Models_Mail();
                        $objMail->sendHtmlMail('contact_admin', $data, $email);

                    }catch (Exception $e) {

                    }
                    /*End */


                    /**
                     * Message
                     */
                    $this->session->contactMessage = array(
                        'success' => true,
                        'message' => Nine_Language::translate('Contact is created successfully.')
                    );
                } else {
                    /**
                     * Message
                     */
                    $this->session->contactMessage = array(
                        'success' => true,
                        'message' => Nine_Language::translate('Not yet reply.')
                    );
                }

                $contact_category_gid = $newContact['contact_category_gid'];
                $updated_date = time();
                $objContact->update(array('updated_date' => $updated_date, 'contact_category_gid' => $contact_category_gid, 'enabled' => 1), array('contact_gid=?' => $gid));

                $this->_redirect('contact/admin/manage-contact');

            } catch (Exception $e) {
            	echo '<pre>';
            	echo print_r($newContact);
            	echo '<pre>';
            	die;
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }


        } else {
            /**
             * Get old data
             */
            $data = @reset($allContactLangs);
            if (false == $data) {
                $this->session->contactMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate("Contact doesn't exist.")
                                           );
                $this->_redirect('contact/admin/manage-contact');
            }
            /**
             * Change date
             */
            if (0 != $data['publish_up_date']) {
                 $data['publish_up_date'] = date('d/m/Y', $data['publish_up_date']);
            } else {
                $data['publish_up_date'] = '';
            }
            if (0 != $data['publish_down_date']) {
                 $data['publish_down_date'] = date('d/m/Y', $data['publish_down_date']);
            } 
        	else {
                $data['publish_down_date'] = '';
            }

            /**
             * Get all lang contacts
             */
            foreach ($allContactLangs as $contact) {
                /**
                 * Rebuild readmore DIV
                 */
                $contact['full_text'] = str_replace("\n", '<br/>',  Nine_Function::combineTextWithReadmore($contact['intro_text'], $contact['full_text']));
                $data[$contact['lang_id']] = $contact;
            }
            
            /**
             * Add deleteable field
             */
            if (null != @$data['contact_category_gid']) {
            	$cat = @reset($objCat->getByColumns(array('contact_category_gid' => $data['contact_category_gid']))->toArray());
            	$data['contact_deleteable'] = @$cat['contact_deleteable'];
            }
            $objContact->update(array('admin_view'=> 1), array('contact_gid=?' => $gid));
        }
        $country = $objCountry->getCategoryByGid($data['quocgia']);
        $data['quocgia'] = $country['name'];
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Contact'));
        $this->view->fullPermisison = $this->checkPermission('edit_contact', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_contact');
        $this->view->menu = array(
        	0=>'contact',
        	1=>'manager-contact'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-contact-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/contact/admin/manage-contact',
        		'name'	=>	Nine_Language::translate('Manager Contact')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-pencil',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('Edit Contact')
        		)
        	
        );

        $condition['parent_id'] = $gid;
        $allContact = $objContact->getAllContact($condition, array('created_date'));
        $this->view->allContact = $allContact;
        $this->view->titleContact = $allContact[0]['title'];
        $this->view->gid = $allContact[0]['contact_gid'];
    }

    public function enableContactAction()
    {
        $objContact = new Models_Contact();
        $objUser = new Models_User();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('contact/admin/manage-contact');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_contact', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objContact->update(array('genabled' => 1), array('contact_gid=?' => $gid));
                   $news = $objContact->getContactByGid ( $gid )->toArray ();
                   $user = $objUser->getByUserId($news['user_id'])->toArray();
                   $user['redic'] = $user['redic'] + 5;
                   $objUser->update($user, array('user_id =?' => $news['user_id']));
                }
                $this->session->contactMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Contact is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->contactMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this contact. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_contact', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objContact->update(array('enabled' => 1), array('contact_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->contactMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Contact is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->contactMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this contact. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('contact/admin/manage-contact');
    }
	public function tinnhanhContactAction()
    {
        $objContact = new Models_Contact();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('contact/admin/manage-contact');
        }
        
        $gids = explode('_', trim($gid, '_'));
       
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_contact', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objContact->update(array('tinnhanh' => 1), array('contact_gid=?' => $gid));
                }
                $this->session->contactMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Contact is Flashes successfully')
                                               );
            } catch (Exception $e) {
                $this->session->contactMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this contact. Please try again')
                                               );
            }
            
        $this->_redirect('contact/admin/manage-contact');
    }
	public function distinnhanhContactAction()
    {
        $objContact = new Models_Contact();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('contact/admin/manage-contact');
        }
        
        $gids = explode('_', trim($gid, '_'));
       
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_contact', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objContact->update(array('tinnhanh' => 0), array('contact_gid=?' => $gid));
                }
                $this->session->contactMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Contact is Dis Flashes successfully')
                                               );
            } catch (Exception $e) {
                $this->session->contactMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this contact. Please try again')
                                               );
            }
            
        $this->_redirect('contact/admin/manage-contact');
    }
    
    public function disableContactAction()
    {
        $objContact = new Models_Contact();
         $objUser = new Models_User();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('contact/admin/manage-contact');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_contact', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objContact->update(array('genabled' => 0), array('contact_gid=?' => $gid));
                   $news = $objContact->getContactByGid ( $gid )->toArray ();
                   $user = $objUser->getByUserId($news['user_id'])->toArray();
                   $user['redic'] = $user['redic'] - 5;
                   $objUser->update($user, array('user_id =?' => $news['user_id']));
                }
                $this->session->contactMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Contact is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->contactMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this contact. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_contact', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objContact->update(array('enabled' => 0), array('contact_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->contactMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Contact is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->contactMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this contact. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('contact/admin/manage-contact');
    }

    /*
     * Delete contact
     */
    public function deleteContactAction()
    {
        $objContact = new Models_Contact();
        
        $gid = $this->_getParam('gid', false);
        
        if (false == $gid) {
            $this->_redirect('contact/admin/manage-contact');
        }

        $gids = explode('_', trim($gid, '_'));
        
        try {
            foreach ($gids as $gid) {

            	$contact = @reset($objContact->getByColumns(array('contact_gid=?'=>$gid))->toArray());
                $objContact->delete(array('contact_gid=?' => $gid));
            }
            $this->session->contactMessage = array(
                                               'success' => true,
                                               'message' => Nine_Language::translate('Contact is deleted successfully')
                                           );
        } catch (Exception $e) {
            $this->session->contactMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete this contact. Please try again')
                                           );
        }
        $this->_redirect('contact/admin/manage-contact');
    }
    
    /**************************************************************************************************
     *                                         CATEGORY's FUNCTIONS
     **************************************************************************************************/
    public function manageCategoryAction()
    {
        $objLang    = new Models_Lang();
        $objCategory     = new Models_ContactCategory();
        
        
        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $this->view->headTitle(Nine_Language::translate('manage Category '));
        
        
        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);
        
        if($currentPage == false){
        	$currentPage = 1;
        	$this->session->categoryDisplayNum = null;
        	$this->session->categoryCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objCategory->update(array('sorting' => $value), array('contact_category_gid=?' => $id));
            $this->session->categoryMessage = array(
                                       'success' => true,
                                       'message' => Nine_Language::translate("Edit sort numbers successfully")
                                   );
        }
        
        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->categoryDisplayNum;
        } else {
            $this->session->categoryDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->categoryCondition;
        } else {
            $this->session->categoryCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_category', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        /**
         * Get all categorys
         */
        $numRowPerPage = $numRowPerPage;
        $allCategories  = $objCategory->setAllLanguages(true)->getallCategories($condition, array('sorting ASC', 'contact_category_gid DESC', 'contact_category_id ASC'),
                                                   $numRowPerPage,
                                                   ($currentPage - 1) * $numRowPerPage
                                                  );
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Count all categorys
         */
        $count = count($objCategory->setAllLanguages(true)->getallCategories($condition));
        /**
         * Format
         */
        
        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allCategories[0]['contact_category_gid'];
        foreach ($allCategories as $index=>$category) {
            /**
             * Change date
             */
        	
            if (0 != $category['created_date']) {
                $category['created_date'] = date($config['dateFormat'], $category['created_date']);
            } else {
                $category['created_date'] = '';
            }
            if ($tmpGid != $category['contact_category_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $category['contact_category_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $category;
            }
            $tmp2['langs'][]  = $category;
            /**
             * Final element
             */
            if ($index == count($allCategories) - 1) {
                $tmp[] = $tmp2;
            }
            
        }
        
        $allCategories = $tmp;
    	$export = $this->_getParam('export', false);
		if($export != false){
			  	$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
		
				$style = "style = 'border: 1px solid'";
				
				if(Nine_Language::getCurrentLangId() == 1){
					$textHeader = array(
						"<b>No</b>",
						"<b>Parent Category</b>",
						"<b>User Support</b>",
						"<b>Email</b>",
						"<b>Phone</b>",
						"<b>Created Date</b>",
						"<b>Title</b>"
					);
				}else{
					$textHeader = array(
						mb_convert_encoding("<b>Số Thứ Tự</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Danh Mục Trên</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Người Hỗ Trợ</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Email</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Số Điện Thoại</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Ngày Khởi Tạo</b>",'HTML-ENTITIES','UTF-8'),
						mb_convert_encoding("<b>Tiêu Đề</b>",'HTML-ENTITIES','UTF-8')
					);
				}
				
				$header = "<tr>";
				foreach ($textHeader as $text) {
					$header .= "<td $style>" . utf8_encode($text) . "</td>";
				}
				$header .= "</tr>";
		
				$content = '';
				$no = 1;
				
				foreach ($allCategories as $item) {
					$content .="<tr>";
					$content .= "<td $style>" . $no . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['parent'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" .  mb_convert_encoding($item['tennguoiphutrach'],'HTML-ENTITIES','UTF-8') . "</td>";
					$content .= "<td $style>" .  mb_convert_encoding($item['email'],'HTML-ENTITIES','UTF-8') . "</td>";
					$content .= "<td $style>" . mb_convert_encoding($item['phone'],'HTML-ENTITIES','UTF-8')  . "</td>";
					$content .= "<td $style>" .  mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8') . "</td>";
					$content .= "<td $style>" .  mb_convert_encoding($item['name'],'HTML-ENTITIES','UTF-8') . "</td>";
					$content .="</tr>";
					$no++;
				}
		
				header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("content-disposition: attachment;filename=thong-ke-category-product-" . date('d-m-Y H:i:s') . ".xls");
		
				$xlsTbl = $header;
				$xlsTbl .= $content;
		
				echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
				exit();
				
		}
//        echo print_r($allCategories);die;
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCategories = $allCategories;
        $this->view->categoryMessage = $this->session->categoryMessage;
        $this->session->categoryMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_category');
        $this->view->allLangs = $allLangs;
        
        $this->view->menu = array(
        	0=>'contact',
        	1=>'manager-category-contact'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-contact-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/contact/admin/manage-category',
        		'name'	=>	Nine_Language::translate('Manager Category Contact')
        		)
        	
        );
    }
    
    public function newCategoryAction()
    {
        $objLang = new Models_Lang();
        $objCategory = new Models_ContactCategory;
        $objContact = new Models_Contact();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $data = $this->_getParam('data', false);
        /**
         * Get all categories
         */
        $allCats = $objCategory->buildTree($objCategory->getAll(array('sorting ASC'))->toArray());
//        echo "<pre>";print_r($allCats);die; 
        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_category', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCategory = $data;
            $newCategory['created_date'] = time();
            /**
             * Sorting
             */
            if (null == $newCategory['sorting']) {
                unset($newCategory['sorting']);
            }
            if (false == $this->checkPermission('new_category', null, '*')) {
                $newCategory['genabled']          = 0;
                $newCategory['sorting']           = 1;
            }
        	
            /**
             * Remove empty images
             */
            if (is_array($newCategory['images'])) {
                foreach ($newCategory['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newCategory['images'][$index]);
                    } else {
                        $newCategory['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newCategory['images'] = implode('||', $newCategory['images']);
        	foreach ($allLangs as $index => $lang) {
            	if($newCategory[$lang['lang_id']]['alias'] == ""){
            		$newCategory[$lang['lang_id']]['alias'] = $objContact->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
            		$newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newCategory[$lang['lang_id']]['alias']));
            	}
            }
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > @$newCategory['sorting']) {
                    $newCategory['sorting'] = 1;
                }
                if (null == $newCategory['parent_id']) {
                	$newCategory['parent_id'] = NULL;
                }
                $objCategory->increaseSorting($newCategory['sorting'], 1);

                $gid = $objCategory->insert($newCategory);
                
                /**
                 * Update id string
                 */
                $objCategory->update(array('gid_string'	=>	$gid), array('contact_category_gid = ?' => $gid));
                $category = @reset($objCategory->getByColumns(array('contact_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);
                /**
                 * Message
                 */
                $this->session->categoryMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Category is created successfully.')
                        );
                
                $this->_redirect('contact/admin/manage-category');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Category'));
        $this->view->fullPermisison = $this->checkPermission('new_category', null, '*'); 
        
        $this->view->menu = array(
        	0=>'contact',
        	1=>'new-category-contact'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-contact-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/contact/admin/manage-category',
        		'name'	=>	Nine_Language::translate('Manager Category Contact')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-plus',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('New Category Contact')
        		)
        	
        );
    }
    

    
    public function editCategoryAction()
    {
        $objCategory     = new Models_ContactCategory();
        $objLang    = new Models_Lang();
        $objContact = new Models_Contact();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_category', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false); 
        if (false == $gid) {
            $this->_redirect('contact/admin/manage-category');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_category', null, '*'))
        ||  (false != $lid && false == $this->checkPermission('edit_category', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $data   = $this->_getParam('data', false);
        
        /**
         * Get all categories
         */
        $allCats = $objCategory->buildTree($objCategory->getAll(array('sorting ASC'))->toArray());
        
        /**
         * Get all category languages
         */
        
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allCategoryLangs = $objCategory->setAllLanguages(true)->getByColumns(array('contact_category_gid=?' => $gid))->toArray();
//        echo "<pre>";print_r($allCategoryLangs);die; 
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_category', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allCategoryLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }
        
        $errors = array();
        if (false !== $data) {
            /**
             * Insert new category
             */
            $newCategory = $data;
            /**
             * Sorting
             */
            if (null == $newCategory['sorting']) {
                unset($newCategory['sorting']);
            }
            if (false == $this->checkPermission('new_category', null, '*')) {
                unset($newCategory['genabled']);
                unset($newCategory['sorting']);
            }
        
            /**
             * Remove empty images
             */
            if (is_array($newCategory['images'])) {
                foreach ($newCategory['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newCategory['images'][$index]);
                    } else {
                        $newCategory['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newCategory['images'] = implode('||', $newCategory['images']);
        	foreach ($allLangs as $index => $lang) {
            	if($newCategory[$lang['lang_id']]['alias'] == ""){
            		$newCategory[$lang['lang_id']]['alias'] = $objContact->convert_vi_to_en($newCategory[$lang['lang_id']]['name']);
            		$newCategory[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;","",$newCategory[$lang['lang_id']]['alias']));
            	}
            }
            try {
                /**                
                 * Update
                 */
              	if (null == $newCategory['parent_id']) {
                	$newCategory['parent_id'] = NULL;
                }
                /**
                 * Delete gid in parent
                 */
                $oldCategory = @reset($allCategoryLangs);
//                echo "<pre>";print_r($oldCategory);die; 
                $objCategory->deleteGidString($oldCategory['parent_id'], $oldCategory['gid_string']);
                
                /**
                 * Update new data
                 */
                $objCategory->update($newCategory, array('contact_category_gid=?' => $gid));
                
                /**
                 * Update new id string
                 */
                $category = @reset($objCategory->getByColumns(array('contact_category_gid = ?' => $gid))->toArray());
                $objCategory->updateGidString($category['parent_id'], $category['gid_string']);
                
                /**
                 * Message
                 */
                $this->session->categoryMessage =  array(
                           'success' => true,
                           'message' => Nine_Language::translate('Category is updated successfully.')
                        );
                
                $this->_redirect('contact/admin/manage-category');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allCategoryLangs);
            if (false == $data) {
                $this->session->categoryMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate("Category doesn't exist.")
                                           );
                $this->_redirect('contact/admin/manage-category');
            }
            /**
             * Format image
             */
            $data['images'] = explode('||', $data['images']);
            if (! is_array($data['images'])) {
                $data['images'] = array();
            }
            $data['images'] = array_pad($data['images'], 50, null);
            /**
             * Get all lang categorys
             */
            foreach ($allCategoryLangs as $category) {
                $data[$category['lang_id']] = $category;
            }
            
             /**
             * Get all child category
             */
            $allChildCats = explode(',',trim($data['gid_string'],','));
            unset($allChildCats[0]);
           	foreach($allCats as $key =>	$item) {
           		if (false != in_array($item, $allChildCats)) {
           			unset($allCats[$key]);
           		}
           	}
        }
        /**
         * Remove it and its childs from category list
         */
        $oldData = @reset($allCategoryLangs);
        foreach ($allCats as $index => $item) {
            if (false !== strpos(",{$oldData['gid_string']},", ",{$item['contact_category_gid']},")) {
                unset($allCats[$index]);
            }
        }

        /**
         * Prepare for template
         */
        $this->view->allCats = $allCats;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Category'));
        $this->view->fullPermisison = $this->checkPermission('edit_category', null, '*');
         $this->view->menu = array(
        	0=>'contact',
        	1=>'manager-category-contact'
        );
        $this->view->breadcrumb = array(
        	0=>array(
        		'icon' 	=> 	'fa-contact-hunt',
        		'url'	=>	Nine_Registry::getBaseUrl().'admin/contact/admin/manage-category',
        		'name'	=>	Nine_Language::translate('Manager Category Contact')
        		),
        	1=>array(
        		'icon' 	=> 	'fa-pencil',
        		'url'	=>	'',
        		'name'	=>	Nine_Language::translate('Edit Category Contact')
        		)
        	
        );
    }

    public function enableCategoryAction()
    {
        $objCategory = new Models_ContactCategory;
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('contact/admin/manage-category');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_category', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('genabled' => 1), array('contact_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_category', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('enabled' => 1), array('contact_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is enable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT activate this category. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('contact/admin/manage-category');
    }

    
    public function disableCategoryAction()
    {
        $objCategory = new Models_ContactCategory;
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        
        if (false == $gid) {
            $this->_redirect('contact/admin/manage-category');
        }
        
        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_category', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('genabled' => 0), array('contact_category_gid=?' => $gid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                                               );
            }
        
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_category', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                   $objCategory->update(array('enabled' => 0), array('contact_category_gid=?' => $gid, 'lang_id=?' => $lid));
                }
                $this->session->categoryMessage = array(
                                                   'success' => true,
                                                   'message' => Nine_Language::translate('Category is disable successfully')
                                               );
            } catch (Exception $e) {
                $this->session->categoryMessage = array(
                                                   'success' => false,
                                                   'message' => Nine_Language::translate('Can NOT deactive this category. Please try again')
                                               );
            }
        }
        
        
        $this->_redirect('contact/admin/manage-category');
    }
    
    public function deleteCategoryAction()
    {
        $objCategory = new Models_ContactCategory;
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_category')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        
        $gid = $this->_getParam('gid', false);
        
        if (false == $gid) {
            $this->_redirect('contact/admin/manage-category');
        }
        
        $gids = explode('_', trim($gid, '_'));
        
        try {
            foreach ($gids as $gid) {
            	
            	$cat = @reset($objCategory->getByColumns(array('contact_category_gid=?'=>$gid))->toArray());
            	if ( 0 == $cat['contact_deleteable']){
            		$this->session->categoryMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete category ('. $gid .'). Please try again')
                                           );
                    $this->_redirect('contact/admin/manage-category');
            	}
            	else {
            		$objCategory->delete(array('contact_category_gid=?' => $gid));
            	}
            }
            $this->session->categoryMessage = array(
                                               'success' => true,
                                               'message' => Nine_Language::translate('Category is deleted successfully')
                                           );
        } catch (Exception $e) {
            $this->session->categoryMessage = array(
                                               'success' => false,
                                               'message' => Nine_Language::translate('Can NOT delete this category. Please try again')
                                           );
        }
        $this->_redirect('contact/admin/manage-category');
    }
    public function changeStringAction()
    {
    	$objContact = new Models_Contact();
    	$str = $this->_getParam("string","");
    	$str = $objContact->convert_vi_to_en($str);
    	$str = str_replace(" ", "-", trim($str));
    	echo $str;die;
    }

    public function closeContactAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $objContact = new Models_Contact();
        $gid = $this->_request->getParam('gid');
        if ($gid != null && $gid != '') {
            $objContact->update(array('enabled' =>  2), array('contact_gid=?' => $gid));
            $this->_redirect('contact/admin/manage-contact');
        } else {
            $this->_redirect('contact/admin/manage-contact');
        }
    }
}