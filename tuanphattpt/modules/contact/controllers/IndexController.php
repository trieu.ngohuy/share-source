<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/contact/models/Contact.php';
require_once 'modules/contact/models/ContactCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/user/models/User.php';
require_once 'modules/mail/models/Mail.php';
require_once 'modules/country/models/CountryCategory.php';

class contact_IndexController extends Nine_Controller_Action
{

    public function contactUsAction()
    {
        try {
            $objContact = new Models_Contact();
            //Get submit data
            $data = $this->_getParam('data', false);

            if (false != $data) {
                //Get submit data
                $newContact = $data;
                $newContact['alias'] = Nine_Common::convert_vi_to_en($data['title']);
                $newContact['created_date'] = time();

                $obj = $objContact->insert($newContact);
                if($obj){
                    $this->view->err = array(
                        'status' => true,
                        'message' => 'Liên hệ đã được gửi đi thành công. Chứng thôi sẽ phản hồi trong thời gian sớm nhất. Cảm ơn!'
                    );
                }else{
                    $this->view->err = array(
                        'status' => false,
                        'message' => 'Lỗi: Liên hệ chưa được gửi đi.'
                    );
                }
                $this->_redirect('/');
            }

            /*
             * Get config
             */
            $this->view->config = Nine_Query::getConfig();
            //Get current language code
            $this->view->currentLangCode = Nine_Language::getCurrentLangCode();
            /*
             * Set layout
             */
            Nine_Common::setLayoutAndView($this, Nine_Language::translate('Giỏ hàng'), 'contact', 'contact-us-v2');
        } catch (Exception $exc) {
            echo $exc->getMessage();
            echo $exc->getTraceAsString();
        }
    }

}
