


<div class="page-header">
    <h1>
        {{l}}Project{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}List All Project{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">

                {{if $allProject|@count <= 0}}
                <div class="alert alert-info">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{l}}No project with above conditions.{{/l}}
                </div>
                {{/if}}

                {{if $projectMessage|@count > 0 && $projectMessage.success == true}}
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$categoryMessage.message}}.
                </div>
                {{/if}}

                {{if $projectMessage|@count > 0 && $projectMessage.success == false}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$projectMessage.message}}.
                </div>
                {{/if}}


                <table id="simple-table" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                    <form id="top-search" name="search" method="post" action="">
                        <th class="center">
                            <button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
                        </th>
                        <th class="center">
                            <input class="check-all" type="checkbox" />
                        </th>
                        <th>{{l}}Created{{/l}}
                            <input  type="text" name="condition[created_date]" value="{{$condition.created_date}}" class="form-control date-picker" id="id-date-picker-1" type="text" data-date-format="dd-mm-yyyy"  />
                        </th>
                        <th class="center">
                            {{l}}Tình Trạng{{/l}}
                            {{if $fullPermisison}}
                            <a href="javascript:document.sortForm.submit();"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/save_16.png" alt="Save sorting"></a>
                                {{/if}}
                            <div class="col-sm-12">
                                <select class=" form-control" name="condition[check_project]" onchange="this.form.submit();">
                                    <option value="">{{l}}All Status{{/l}}</option>
                                    <option value="1" {{if $condition.check_project == 1}}selected="selected"{{/if}}>{{l}}Approved Pending{{/l}}</option>
                                    <option value="2" {{if $condition.check_project == 2}}selected="selected"{{/if}}>{{l}}Approved{{/l}}</option>
                                    <option value="3" {{if $condition.check_project == 3}}selected="selected"{{/if}}>{{l}}Not Approved{{/l}}</option>
                                </select>
                            </div>
                        </th>
                        <th colspan="3">
                            <div class="col-sm-12">
                                {{l}}Title{{/l}}
                            </div>
                            <div class="col-sm-12">
                                <input  type="text" name="condition[keyword]" id="username" value="{{$condition.keyword}}" placeholder="{{l}}Find Title{{/l}}" class="form-control" />
                            </div>


                        </th>
                    </form>
                    </tr>
                    </thead>

                    <tbody>
                        {{if $allProject|@count > 0}}
                    <form action="" method="post" name="sortForm">
                        {{foreach from=$allProject item=item name=project key=key}}
                        <tr>
                            <td class="center">{{$key+1}}</td>
                            
                            <td class="center">
                                <input type="checkbox" value="{{$item.project_gid}}" name="allProject" class="allProject"/>
                            </td>
                            
                            <td>{{$item.created_date}}</td>
                            <td class="center">
                                {{if $fullPermisison}}
                                <select class=" form-control" name="data[{{$item.project_gid}}]" {{if $item.check_project == 1}}style="color: red"{{/if}}>
                                    <option value="1" {{if $item.check_project == 1}}selected="selected"{{/if}}>{{l}}Approved Pending{{/l}}</option>
                                    <option value="2" {{if $item.check_project == 2}}selected="selected"{{/if}}>{{l}}Approved{{/l}}</option>
                                    <option value="3" {{if $item.check_project == 3}}selected="selected"{{/if}}>{{l}}Not Approved{{/l}}</option>
                                </select>
                                {{else}}
                                {{if $item.check_project == 1}}{{l}}Approved Pending{{/l}}{{/if}}
                                {{if $item.check_project == 2}}{{l}}Approved{{/l}}{{/if}}
                                {{if $item.check_project == 3}}{{l}}Not Approved{{/l}}{{/if}}
                                {{/if}}

                            </td>
                            <td colspan="3" style="padding:0px;">
                                <!-- All languages -->
                                <table style="border-collapse: separate;margin-bottom: 0px" class="table table-bordered table-striped">
                                    <tbody id="table{{$item.project_id}}">
                                        <tr>
                                            <td width="39%">--</td>

                                            <td width="5%"  class="center">
                                                {{if $EditPermisision}}
                                                <span class="tooltip-area">
                                                    <a href="{{$APP_BASE_URL}}project/admin/edit-project/gid/{{$item.project_gid}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                                </span>
                                                {{else}}
                                                --
                                                {{/if}}
                                            </td>
                                            <td width=5%">
                                                {{$item.project_gid}}
                                            </td>
                                        </tr>

                                        {{foreach from=$item.langs item=item2}}
                                        <tr>
                                            <td><image style="vertical-align:middle;" src="{{$url.path}}media/userfiles/images/icons/flags/language/16/vi.png"> {{$item2.title}}</td>

                                            <td  class="center">
                                                {{p name=edit_project module=project expandId=$langId}}
                                                <span class="tooltip-area">
                                                    <a href="{{$APP_BASE_URL}}project/admin/edit-project/gid/{{$item.project_gid}}/lid/{{$item2.lang_id}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                                </span>
                                                {{/p}}
                                                {{np name=edit_project module=project expandId=$langId}}
                                                --
                                                {{/np}}
                                            </td>
                                            <td>
                                                {{$item2.project_id}}
                                            </td>
                                        </tr>
                                        {{/foreach}}

                                    </tbody>
                                </table>
                            </td>

                        </tr>
                        {{/foreach}}
                    </form>
                    {{/if}}
                    </tbody>
                </table>


            </div><!-- /.span -->
        </div><!-- /.row -->


        <div class="col-lg-12">
            <div class="form-group col-lg-4">
                <label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
                <div class="col-lg-6">
                    <select id="action" class="form-control" >
                        <option value=";">{{l}}Choose an action...{{/l}}</option>
                        {{p name=delete_project module=project}}
                        <option value="deleteProject();">{{l}}Delete{{/l}}</option>
                        {{/p}}
                    </select>
                </div>
                <div class="col-lg-6">
                    <a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
                </div>
            </div>
            <div class="form-group col-lg-2">
                <label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
                <div class="col-lg-12">
                    <form class="search" name="search" method="post" action="">
                        <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
                            <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
                            <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
                            <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
                            <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
                            <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
                        </select>
                    </form>
                </div>
            </div>
            {{if $countAllPages > 1}}
            <div class="col-lg-6 pagination">
                {{if $first}}
                <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
                {{/if}}
                {{if $prevPage}}
                <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
                {{/if}}

                {{foreach from=$prevPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>

                {{foreach from=$nextPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                {{if $nextPage}}
                <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
                {{/if}}
                {{if $last}}
                <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
                {{/if}}

            </div>
            {{/if}}
        </div>
    </div>


</div>
</div>

<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        $('.close').click(function () {
            $(this).parent().hide("slow");
        });
        $('.check-all').click(function () {
            if (this.checked) { // check select status
                $('.allProject').each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.allProject').each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });
    });

    function applySelected()
    {
        var task = document.getElementById('action').value;
        eval(task);
    }
    function enableProject()
    {
        var all = document.getElementsByName('allProject');
        var tmp = '';
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
            }
        }
        if ('' == tmp) {
            alert('Please choose an project');
        }
        window.location.href = '{{$APP_BASE_URL}}project/admin/enable-project/gid/' + tmp;
    }

    function disableProject()
    {
        var all = document.getElementsByName('allProject');
        var tmp = '';
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
            }
        }
        if ('' == tmp) {
            alert('Please choose an project');
        }
        window.location.href = '{{$APP_BASE_URL}}project/admin/disable-project/gid/' + tmp;
    }

    function deleteProject()
    {
        var all = document.getElementsByName('allProject');
        var tmp = '';
        var count = 0;
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
                count++;
            }
        }
        if ('' == tmp) {
            alert('Please choose an project');
            return;
        } else {
            result = confirm('Are you sure you want to delete ' + count + ' project(s)?');
            if (false == result) {
                return;
            }
        }
        window.location.href = '{{$APP_BASE_URL}}project/admin/delete-project/gid/' + tmp;
    }


    function deleteAProject(id)
    {
        result = confirm('Are you sure you want to delete this project?');
        if (false == result) {
            return;
        }
        window.location.href = '{{$APP_BASE_URL}}project/admin/delete-project/gid/' + id;
    }
</script>