﻿<!--Multiple select-->

<!--Ckfinder-->
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<script type="text/javascript">
    //<![CDATA[

    jQuery(document).ready(function () {
        CKFinder.setupCKEditor(null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/');
        jQuery("#images").sortable();
        jQuery("#images").disableSelection();
        //Make slug
        {{foreach from=$allLangs item=item name=langDiv}}
        jQuery('#title{{$smarty.foreach.langDiv.iteration}}').makeSlug({
            slug: jQuery('#alias{{$smarty.foreach.langDiv.iteration}}')
        });
        {{/foreach}}

    });
    var imgId;

    function chooseImage(id) {
        imgId = id;
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();
        finder.basePath = '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/'; // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.selectActionFunction = setFileField;
        finder.popup();
    }

    // This is a sample function which is called when a file is selected in CKFinder.
    function setFileField(fileUrl) {
        document.getElementById('chooseImage_img' + imgId).src = fileUrl;
        document.getElementById('chooseImage_input' + imgId).value = fileUrl;
        document.getElementById('chooseImage_div' + imgId).style.display = '';
        document.getElementById('chooseImage_noImage_div' + imgId).style.display = 'none';
    }

    function clearImage(imgId) {
        document.getElementById('chooseImage_img' + imgId).src = '';
        document.getElementById('chooseImage_input' + imgId).value = '';
        document.getElementById('chooseImage_div' + imgId).style.display = 'none';
        document.getElementById('chooseImage_noImage_div' + imgId).style.display = '';
    }

    function addMoreImg() {
        jQuery("ul#images > li.hidden").filter(":first").removeClass('hidden');
    }

    //]]>
</script>
<style type="text/css">
    #images {
        list-style-type: none;
        margin: 0;
        padding: 0;
    }

    #images li {
        margin: 10px;
        float: left;
        text-align: center;
        height: 180px;
    }
</style>


<div class="page-header">
    <h1>
        {{l}}Project{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}New Project{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">
                {{if $errors|@count > 0}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{if $errors.main}}
                    </strong> {{$errors.main}}
                    {{else}}
                    {{l}}Please check following information again{{/l}}
                    {{/if}}
                </div>
                {{/if}}


                <div class="col-sm-12 widget-container-col ui-sortable">

                    <div class="tabbable tabs-left">

                        <ul class="nav nav-tabs" id="myTab2">
                            <li class="active">
                                <a href="#tab0" data-toggle="tab" aria-expanded="true">
                                    <i class="pink ace-icon fa fa-home bigger-110"></i>
                                    {{l}}Basic{{/l}}
                                </a>
                            </li>

                            {{foreach from=$allLangs item=item index=index name=langTab}}
                            <li>
                                <a href="#tab{{$item.lang_id}}" data-toggle="tab" aria-expanded="true">
                                    <image style="vertical-align:middle;"
                                           src="{{$BASE_URL}}{{$item.lang_image}}"> {{$item.name}}
                                </a>
                            </li>
                            {{/foreach}}
                        </ul>

                        <form action="" method="post" class="form-horizontal">
                            <div class="tab-content">

                                <div id="tab0" class="tab-pane active">

                                    <div class="form-group has-info">
                                        <label class="control-label col-md-2">{{l}}Vị trí hiển thị{{/l}}</label>
                                        <div class="col-md-10">
                                            <label class="radio-inline">
                                                <input type="radio" name="data[displayMode]" value="0" {{if $data.displayMode === '0' || isset($data.displayMode) === false}}checked="checked"{{/if}}/> Bình thường
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="data[displayMode]" value="1" {{if $data.displayMode === '1'}}checked="checked"{{/if}}/> Trang chủ
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="data[displayMode]" value="2" {{if $data.displayMode === '2'}}checked="checked"{{/if}}/> Danh mục sản phẩm
                                            </label>
                                        </div>
                                    </div>
                                            
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Danh sách sản phẩm{{/l}}</label>
                                        <div class="col-md-10">
                                            <!-- Build your select: -->
                                            <select id="example-getting-started" multiple="multiple" name="data[product_id][]">
                                                {{foreach from=$arrProducts item=item}}
                                                <option value="{{$item.product_gid}}" {{$item.selected}}>{{$item.title}}</option>
                                                {{/foreach}}
                                            </select>
                                            <!-- Initialize the plugin: -->
                                            <script type="text/javascript">
                                                $(document).ready(function() {
                                                    $('#example-getting-started').multiselect({
                                                        numberDisplayed: 5
                                                    });
                                                });
                                            </script>
                                        </div>
                                    </div>

                                    <footer class="panel-footer">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">{{l}}Images:{{/l}}</label>
                                            <div class="col-md-10">
                                                <p><a href="javascript:addMoreImg()">{{l}}+ Add more images{{/l}}</a>
                                                </p>
                                                <ul id="images">
                                                    {{section name=images loop=50}}
                                                    {{assign var="i" value=$smarty.section.images.iteration}}
                                                    <li {{if $i >2 && $i > $data.images|@count}}class="hidden"{{/if}}>
                                                        <input type="hidden" id="chooseImage_input{{$i}}"
                                                               name="data[images][]">
                                                        <div id="chooseImage_div{{$i}}" style="display: none;">
                                                            <img src="" id="chooseImage_img{{$i}}"
                                                                 style="max-width: 150px; max-height:150px; border:dashed thin;"></img>
                                                        </div>
                                                        <div id="chooseImage_noImage_div{{$i}}"
                                                             style="width: 150px; border: thin dashed; text-align: center; padding:70px 0px;">
                                                            No image
                                                        </div>
                                                        <br/>
                                                        <a href="javascript:chooseImage({{$i}});">{{l}}Choose
                                                            image{{/l}}</a>
                                                        |
                                                        <a href="javascript:clearImage({{$i}});">{{l}}Delete{{/l}}</a>
                                                    </li>
                                                    {{/section}}
                                                </ul>
                                            </div>
                                        </div>


                                    </footer>

                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Save{{/l}}
                                            </button>
                                        </div>
                                    </div>

                                </div>

                                {{foreach from=$allLangs item=item name=langDiv}}
                                <div class="tab-pane" id="tab{{$item.lang_id}}">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Title{{/l}}</label>
                                        <div class="col-md-10">
                                            <input id="title{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][title]"  value="{{$data[$item.lang_id].title}}" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Alias{{/l}}</label>
                                        <div class="col-md-10">
                                            <input id="alias{{$smarty.foreach.langDiv.iteration}}" type="text" class="form-control" name="data[{{$item.lang_id}}][alias]"  value="{{$data[$item.lang_id].alias}}" >.html
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Mô tả{{/l}}</label>
                                        <div class="col-md-10">
                                            <textarea style="float:left;" class="text-input textarea ckeditor"
                                                      name="data[{{$item.lang_id}}][intro_text]" rows="20"
                                                      cols="90">{{$data[$item.lang_id].intro_text}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">{{l}}Chi tiết{{/l}}</label>
                                        <div class="col-md-10">
                                            <textarea style="float:left;" class="text-input textarea ckeditor"
                                                      name="data[{{$item.lang_id}}][full_text]" rows="20"
                                                      cols="90">{{$data[$item.lang_id].full_text}}</textarea>
                                        </div>
                                    </div>


                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                {{l}}Save{{/l}}
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                {{/foreach}}

                            </div>

                        </form>

                    </div>

                </div>


            </div><!-- /.span -->

        </div><!-- /.row -->


    </div>


</div>
