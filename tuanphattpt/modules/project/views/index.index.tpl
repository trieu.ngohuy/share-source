<div class="page-header">
    <div class="container">
        <h1>{{l}}Dự án gần đây{{/l}}</h1>
    </div>
</div>
<div class="portfolio-showcase">
    <div class="container">
        <div class="row">
            {{foreach from=$arrProjects item=item}}
            <article class="col-md-4 post-1756 portfolio type-portfolio status-publish has-post-thumbnail hentry portfolio_category-du-an-nha-xuong">
                <div class="project-wrapper">
                    <a class="project-link" href="{{$item.url}}">
                        <div class="image-thumbnail"> <img src="{{$item.main_image}}" alt=""></div>
                        <h2 class="entry-title">{{$item.title}}</h2>
                    </a>
                    <div class="related-product">
                        <div class="row">
                            {{assign var='count' value=1}}
                            {{foreach from=$item.products item=item2}}
                            {{if $count <= 2}}
                            <a class="col-md-6" href="{{$item2.url}}">
                                <table>
                                    <tr>
                                        <td class="align-middle" style="width: 30%;">
                                            <div class="product-image"><img src="{{$item2.main_image}}"></div>
                                        </td>
                                        <td class="align-middle">
                                            <div class="product-title">{{$item2.title}}</div>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                            {{/if}}
                            {{assign var='count' value=$count+1}}
                            {{/foreach}}
                        </div>
                    </div>
                </div>
            </article>
            {{/foreach}}
        </div>
    </div>
</div>
{{if $countAllPages > 1}}
<div class="text-center">
    <nav>
        <ul class="pagination justify-content-center">
            <li class="disabled page-item">
                <span class="page-link"><span aria-hidden="true">{{l}}Trang{{/l}} {{$currentPage}} / {{$countAllPages}}</span></span>
            </li>
            {{if $first}}
            <li class="page-item">
                <a class="page-link" href="?page=1" aria-label="First">
                    «<span class="hidden-xs"> {{l}}First{{/l}}</span>
                </a>
            </li>
            {{/if}}

            {{if $prevPage}}
            <li class="page-item">
                <a class="page-link" href="?page={{$prevPage}}" aria-label="Previous">
                    ‹<span class="hidden-xs"> {{l}}Previous{{/l}}</span>
                </a>
            </li>
            {{/if}}

            {{foreach from=$prevPages item=item}}
            <li class='page-item'><a class='page-link' href='?page={{$item}}'>{{$item}}</a></li>
            {{/foreach}}

            <li class="page-item active"><span class='page-link'>{{$currentPage}} <span class="sr-only">(current)</span></span>
            </li>

            {{foreach from=$nextPages item=item}}
            <li class='page-item'><a class='page-link' href='?page={{$item}}'>{{$item}}</a></li>
            {{/foreach}}

            {{if $nextPage}}
            <li class='page-item'><a class='page-link' href="?page={{$nextPage}}" aria-label='Next'><span class='hidden-xs'>{{l}}Next{{/l}} </span>&rsaquo;</a></li>
            {{/if}}

            {{if $last}}
            <li class='page-item'><a class='page-link' href='?page={{$countAllPages}}' aria-label='Last'><span class='hidden-xs'>{{l}}Last{{/l}} </span>&raquo;</a></li>
            {{/if}}
        </ul>
    </nav>
</div>
{{/if}}