<article
        class="post-1756 portfolio type-portfolio status-publish has-post-thumbnail hentry portfolio_category-du-an-nha-xuong">
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <h1 class="entry-title">{{$objDetail.title}}</h1>
                    <p>{{$objDetail.intro_text}}</p>
                    <div class="hidden-sm-down">
                        <a href="#mota" class="cta">{{l}}Xem mô tả chi tiết{{/l}}</a> <a class="cta toggleCTA">{{l}}Liên hệ với TUẤN PHÁT{{/l}}</a></div>
                </div>
                <div class="col-lg-7">
                    <div class="portfolio-slider">
                        {{foreach from=$objDetail.arrImages item=item}}
                        <div><img src="{{$item}}" alt="{{$objDetail.title}}"/></div>
                        {{/foreach}}
                    </div>
                    <div class="portfolio-slider-small">
                        {{foreach from=$objDetail.arrImages item=item}}
                        <div><img src="{{$item}}" alt="{{$objDetail.title}}"/></div>
                        {{/foreach}}
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="entry-content" id="mota">
        <div class="container">
            <div class="mota">{{l}}Mô tả chi tiết{{/l}}</div>
            <div class="related-product">
                <p><strong>{{l}}Sản phẩm sử dụng (Click để xem thông số kỹ thuật){{/l}}</strong></p>
                <div class="row">
                    {{foreach from=$objDetail.products item=item}}
                    <a class="col-md-4" href="{{$item.url}}">
                        <div class="product-image"><img
                                    src="{{$item.main_image}}">
                        </div>
                        <div class="product-title">{{$item.title}}</div>
                    </a>
                    {{/foreach}}
                </div>
            </div>
            <hr>
            <h4>{{l}}CHI TIẾT{{/l}}</h4>
            {{$objDetail.full_text}}
            <div class="row"></div>
        </div>
    </div>
    <footer></footer>
</article>