<?php

class Route_Project {

    /**
     * Build friendly URL
     * @param array $linkArr 0 => <module>, 1 => controller, 2 => action, 3...n param/value
     * @param array $params param => value
     *
     * @return string friendly URL
     */
    public function build($linkArr, $params = array()) {

        $result = implode('/', $linkArr);
        if ('index' == @$linkArr[2]) {
            /*
             * Check current language
             */
            if (Nine_Language::getCurrentLangCode() == 'vi') {
                $result = "danh-sach-du-an";
            } else {
                $result = "list-project/";
            }
        } else if ('detail' == @$linkArr[2]) {
            /**
             * Category
             */
            //$result = "detail/{$linkArr[4]}/";
            if (Nine_Language::getCurrentLangCode() == 'vi') {
                $result = "chi-tiet-du-an/";
            }
            else{
                $result = "project-detail/";
            }
            
            if (null != @$params['alias']) {
                $result .= urlencode($params['alias']);
            }
        }
        return $result;
    }

    /**
     * Parse friendly URL
     */
    public function parse() {
        $router = Nine_Controller_Front::getInstance()->getRouter();
        /*
         * Category index rout (en)
         */
        $indexEn = new Zend_Controller_Router_Route_Regex(
                'list-project', array(
            'module' => 'project',
            'controller' => 'index',
            'action' => 'index'
                )
        );
        $router->addRoute('indexEn', $indexEn);
        
        /*
         * Category index rout (vi)
         */
        $indexVi = new Zend_Controller_Router_Route_Regex(
                'danh-sach-du-an', array(
            'module' => 'project',
            'controller' => 'index',
            'action' => 'index'
                )
        );
        $router->addRoute('indexVi', $indexVi);
        
        /*
         * project detail route (en)
         */
        $detailEn = new Zend_Controller_Router_Route_Regex(
                'project-detail/(.*)', array(
            'module' => 'project',
            'controller' => 'index',
            'action' => 'detail'
                ), array(1 => 'alias')
        );
        $router->addRoute('detailEn', $detailEn);
        /*
         * project detail route (vi)
         */
        $detailVi = new Zend_Controller_Router_Route_Regex(
                'chi-tiet-du-an/(.*)', array(
            'module' => 'project',
            'controller' => 'index',
            'action' => 'detail'
                ), array(1 => 'alias')
        );
        $router->addRoute('detailVi', $detailVi);
    }

}
