<?php

class project_IndexController extends Nine_Controller_Action {
    /*
     * Index action
     */
    public function indexAction()
    {
        try {

            //Get paging
            $page = $this->_getParam('page', false);
            $itemPerPage = 10;
            $currentPage = 1;
            if ($page !== false) {
                $currentPage = $page;
            }

            //Get list projects
            $arrProjects = Nine_Read::GetListData('Models_Project', array(), array(), 'detail', $itemPerPage, ($currentPage - 1) * $itemPerPage);

            $arrAllProjects = Nine_Read::GetListData('Models_Project', array(), array(), 'detail');
            //Get list products in project
            foreach ($arrProjects as &$item){
                //List project id
                $tmp = explode('||', $item['product_id']);
                $arrProducts = array();
                for($i = 0 ; $i < count($tmp) ; $i ++){
                    array_push($arrProducts, @reset(Nine_Read::GetListData('Models_Product', array(
                        'product_gid' => (int) $tmp[$i]
                    ), array(), 'detail')));
                }
                $item['products'] = $arrProducts;
            }
            $this->view->arrProjects = $arrProjects;

            //Set paging
            $this->setPagination($itemPerPage, $currentPage, count($arrAllProjects));
            $this->view->currentPage = $currentPage;

            /* SET LAYOUT AND TEMPLATE */
            Nine_Common::setLayoutAndView($this, Nine_Language::translate('Dự án'), 'project', '');
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    /*
     * Detail action
     */
    public function detailAction()
    {
        try {

            //Get paging
            $alias = $this->_getParam('alias', false);
            $objDetail = array();
            if($alias !== false){
                //Get list projects
                $objDetail = @reset(Nine_Read::GetListData('Models_Project', array(
                    'alias' => $alias
                ), array(), 'detail'));

                //List project id
                $tmp = explode('||', $objDetail['product_id']);
                $arrProducts = array();
                for($i = 0 ; $i < count($tmp) ; $i ++){
                    array_push($arrProducts, @reset(Nine_Read::GetListData('Models_Product', array(
                        'product_gid' => (int) $tmp[$i]
                    ), array(), 'detail')));
                }
                $objDetail['products'] = $arrProducts;
                $this->view->objDetail = $objDetail;

                //Get config
                $config = Nine_Common::getConfig($this->view);
                $this->view->config = $config;
            }

            /* SET LAYOUT AND TEMPLATE */
            Nine_Common::setLayoutAndView($this, $objDetail['title'], 'project-detail', 'detail');
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}
