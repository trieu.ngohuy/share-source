<?php
/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2009 visualidea.org
 * @license    http://license.visualidea.org
 * @version    v 1.0 2009-04-15
 */
require_once 'Nine/Model.php';

class Models_Project extends Nine_Model
{ 
    /**
     * The primary key column or columns.
     * A compound key should be declared as an array.
     * You may declare a single-column primary key
     * as a string.
     *
     * @var mixed
     */
    protected $_primary = 'project_id';
    /**
     * The field name what we use to group all language rows together
     * 
     * @var string
     */
    protected $_groupField = 'project_gid';
    /**
     * Let system know this is miltilingual table or not.
     * If this table has multilingual fields, Zend_Db_Table_Select object
     * will be inserted language condition automatically.
     * 
     * @var array
     */
    protected $_multilingualFields = array('title', 'alias', 'intro_text', 'full_text');
    
    /**
     * Constructor.
     *
     * Supported params for $config are:
     * - db              = user-supplied instance of database connector,
     *                     or key name of registry instance.
     * - name            = table name.
     * - primary         = string or array of primary key(s).
     * - rowClass        = row class name.
     * - rowsetClass     = rowset class name.
     * - referenceMap    = array structure to declare relationship
     *                     to parent tables.
     * - dependentTables = array of child tables.
     * - metadataCache   = cache for information from adapter describeTable().
     *
     * @param  mixed $config Array of user-specified config options, or just the Db Adapter.
     * @return void
     */
    public function __construct($config = array())
    {
        $this->_name = $this->_prefix . 'project';
        return parent::__construct($config); 
    }

    /**
     * Get all product with conditions
     * 
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
//    public function getAllProductsWithDefaultLang($condition = array(), $order = null, $count = null, $offset = null)
//    {
//        $select = $this->select()
//                ->setIntegrityCheck(false)
//                ->from(array('s' => $this->_name), array('product_id', 'senabled' => 'enabled', 'publish_up_date' => 'publish_up_date', 'publish_down_date' => 'publish_down_date' , 'ssorting' => 'sorting', 'created_date' => 'created_date'))
//                ->join(array('sl' => $this->_prefix . 'product_lang'), 's.product_id = sl.product_id')
//                ->join(array('sc' => $this->_prefix . 'product_category'), 's.product_category_gid = sc.product_category_gid', array('cname' => 'name'))
//                ->order($order)
//                ->limit($count, $offset);
//        /**
//         * Conditions
//         */
//        if (null != @$condition['keyword']) {
//            $select->where($this->getAdapter()->quoteInto('sl.title LIKE ?', "%{$condition['keyword']}%"));
//        }
//        if (null != @$condition['product_category_gid']) {
//            $select->where("s.product_category_gid = ?", $condition['product_category_gid']);
//        }
//        if (null != @$condition['lang_id']) {
//            $select->where("sl.lang_id = ?", $condition['lang_id']);
//        }
//        
//        return $this->fetchAll($select)->toArray();
//    }
/**
     * Get all product with conditions
     * 
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
    public function getCurrentProductByCondition($condition = array(), $order = null, $count = null, $offset = null) {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name', 'product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order($order)
                ->limit($count, $offset);


        if (null != @$condition['product_gid']) {

            $select->where('c.product_gid = ?', $condition['product_gid']);
        }
        if (null != @$condition['type_id']) {
            $select->where('t.type_id = ?', $condition['type_id']);
        }

        return current($this->fetchAll($select)->toArray());
    }
    /**
     * Get all product with conditions
     * 
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
	public function getAllProductByRandom() 
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
            ->join(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
            ->join(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name', 'name_company'=>'name_company', 'user_alias' => 'alias'))
            ->join(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
            ->order('RAND()')
            ->limit(20, 0);
        /**
         * Conditions
         */

        return $this->fetchAll($select)->toArray();
    }

    public function getAllProductSpecial()
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
            ->join(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
            ->join(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
            ->join(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('c.check_product=2')
            ->where('c.dacbiet = 1');

        /**
         * Conditions
         */

        return $this->fetchAll($select)->toArray();
    }
	public function getAllProductByParentRandom($parent_id) 
    {
    	$select = $this->select()
                ->where('genabled = 1');
         
         
         $sqlcate = "SELECT product_category_gid FROM 9_product_category Where parent_id = '".$parent_id."' AND lang_id = '".Nine_Language::getCurrentLangId()."'";
         $idCatAll = '';
     
    	$idcates = $this->_db->fetchAll($sqlcate);
        foreach ($idcates as $rows) {
           $idCatAll .= $rows['product_category_gid'] . ',';
        }
    	if($idCatAll == ""){
        	return array();
        }  
        
         $idString = '';
         $sql = "SELECT product_gid FROM {$this->_name} Where product_category_gid IN(".trim($idCatAll, ',').") AND tag != '' AND lang_id = '".Nine_Language::getCurrentLangId()."' ORDER BY RAND() LIMIT 20";
         $ids = $this->_db->fetchAll($sql);
     
            foreach ($ids as $row) {
                $idString .= $row['product_gid'] . ',';
            }
    	if($idString == ""){
        	return array();
        }
        $select->where('product_gid IN (' . trim($idString, ',') .')');
        $select->where('product_category_gid !=?', '116');
        $select->where('product_category_gid !=?', '118');
        $select->where('lang_id=?', Nine_Language::getCurrentLangId());
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllProductByCateRandom($cateid) 
    {
    	$select = $this->select()
                ->where('genabled = 1');
         
                
        
         $idString = '';
         $sql = "SELECT product_gid FROM {$this->_name} Where product_category_gid = '".$cateid."' AND tag != '' AND lang_id = '".Nine_Language::getCurrentLangId()."' ORDER BY RAND() LIMIT 20";
         $ids = $this->_db->fetchAll($sql);
     
            foreach ($ids as $row) {
                $idString .= $row['product_gid'] . ',';
            }
        if($idString == ""){
        	return array();
        }
        $select->where('product_gid IN (' . trim($idString, ',') .')');
        $select->where('lang_id=?', Nine_Language::getCurrentLangId());
        $select->where('product_category_gid !=?', '116');
        $select->where('product_category_gid !=?', '118');
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
    public function getAllProductByDate($dateto , $datefrom , $user_id){
    	$select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->where('c.lang_id=?', 2);
        $select->where('c.user_id= ?' , $user_id);
        $select->where('c.created_date > ?' , $dateto);
        $select->where('c.created_date < ?' , $datefrom);
       	return $this->fetchAll($select)->toArray();
    }
    public function AllNewProducts()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->joinLeft(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
                ->joinLeft(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
                ->joinLeft(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
                ->joinLeft(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('ct.lang_id=?', Nine_Language::getCurrentLangId());
        
        return $this->fetchAll($select)->toArray();
    }
    public function getAllProductEstoreNew()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'estore_category'), 'c.estore_category_gid = cc.estore_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->joinLeft(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
                ->joinLeft(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
                ->joinLeft(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
                ->joinLeft(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('ct.lang_id=?', Nine_Language::getCurrentLangId());
        
        return $this->fetchAll($select)->toArray();
    }
    public function AllSaleProducts()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'estore_category'), 'c.estore_category_gid = cc.estore_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->joinLeft(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
                ->joinLeft(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
                ->joinLeft(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
                ->joinLeft(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.sale=?', 1);
        
        return $this->fetchAll($select)->toArray();
    }
    public function getAllProductEstoreSale()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'estore_category'), 'c.estore_category_gid = cc.estore_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->joinLeft(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
                ->joinLeft(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
                ->joinLeft(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
                ->joinLeft(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.sale=?', 1);
        
        return $this->fetchAll($select)->toArray();
    }
    public function AllHotProducts()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'estore_category'), 'c.estore_category_gid = cc.estore_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->joinLeft(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
                ->joinLeft(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
                ->joinLeft(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
                ->joinLeft(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.tinnhanh=?', 1);
        
        return $this->fetchAll($select)->toArray();
    }
    public function getAllProductEstoreHot()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'estore_category'), 'c.estore_category_gid = cc.estore_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->joinLeft(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
                ->joinLeft(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
                ->joinLeft(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
                ->joinLeft(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.tinnhanh=?', 1);
        
        return $this->fetchAll($select)->toArray();
    }


    public function getAllProductEstore($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'estore_category'), 'c.estore_category_gid = cc.estore_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->joinLeft(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
                ->joinLeft(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
                ->joinLeft(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
                ->joinLeft(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
                
        if (null != @$condition['created_date']) {
           $time = explode("-", $condition['created_date']);
           $count_secont = mktime(0,0,0,$time[1],$time[0],$time[2]);
           
           $select->where('c.created_date > '.$count_secont);
           $select->where('c.created_date < '.$count_secont+86400);
        }
        if (null != @$condition['estore']) {
            $sql = "SELECT user_id FROM {$this->_prefix}" . 'user' ." WHERE " . $this->getAdapter()->quoteInto('full_name LIKE ?', "%{$condition['estore']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['user_id'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.user_id IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['keyword']) {
            $sql = "SELECT product_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('alias LIKE ?', "%{$condition['keyword']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['product_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.product_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['product_category_gid']) {
            
            $sql = "SELECT gid_string FROM {$this->_prefix}" . 'product_category' . " WHERE " . $this->getAdapter()->quoteInto('product_category_gid = ? ', $condition['product_category_gid']);
            $ids = @reset($this->_db->fetchAll($sql));
            if (null == $ids) {
                $ids = '0';
            }
            /**
             * Add to select object
             */
            $select->where('c.product_category_gid IN (' . trim($ids['gid_string'], ',') .')');
            
        }
        
        if (null != @$condition['genabled']) {
            
            $sql = "SELECT product_gid FROM {$this->_name} WHERE genabled = " . $condition['genabled'];
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['product_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.product_gid IN (' . trim($idString, ',') .')');
            
        }
        if (null != @$condition['check_product']) {
            
            $sql = "SELECT product_gid FROM {$this->_name} WHERE check_product = " . $condition['check_product'];
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['product_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.product_gid IN (' . trim($idString, ',') .')');
            
        }

        if (null != @$condition['user_id']) {
            $select->where('c.user_id = ?', $condition['user_id']);
        }

        if (null != @$condition['tinnhanh']) {
            $select->where('c.tinnhanh = ?', $condition['tinnhanh']);
        }
        
        return $this->fetchAll($select)->toArray();
    }


    public function getAllProduct($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                //->joinLeft(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
                //->joinLeft(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
                ->joinLeft(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
                //->joinLeft(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                //->where('p.lang_id=?', Nine_Language::getCurrentLangId())
                //->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
                //->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
                
    	if (null != @$condition['created_date']) {
           $time = explode("-", $condition['created_date']);
           $count_secont = mktime(0,0,0,$time[1],$time[0],$time[2]);
           
           $select->where('c.created_date > '.$count_secont);
           $select->where('c.created_date < '.$count_secont+86400);
        }
    	if (null != @$condition['estore']) {
            $sql = "SELECT user_id FROM {$this->_prefix}" . 'user' ." WHERE " . $this->getAdapter()->quoteInto('full_name LIKE ?', "%{$condition['estore']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['user_id'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.user_id IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['keyword']) {
            $sql = "SELECT product_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('alias LIKE ?', "%{$condition['keyword']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['product_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.product_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['product_category_gid']) {
        	
        	$sql = "SELECT gid_string FROM {$this->_prefix}" . 'product_category' . " WHERE " . $this->getAdapter()->quoteInto('product_category_gid = ? ', $condition['product_category_gid']);
        	$ids = @reset($this->_db->fetchAll($sql));
        	if (null == $ids) {
        	    $ids = '0';
        	}
            /**
             * Add to select object
             */
            $select->where('c.product_category_gid IN (' . trim($ids['gid_string'], ',') .')');
        	
        }
        
    	if (null != @$condition['genabled']) {
        	
        	$sql = "SELECT product_gid FROM {$this->_name} WHERE genabled = " . $condition['genabled'];
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['product_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.product_gid IN (' . trim($idString, ',') .')');
        	
        }
    	if (null != @$condition['check_product']) {
        	
        	$sql = "SELECT product_gid FROM {$this->_name} WHERE check_product = " . $condition['check_product'];
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['product_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.product_gid IN (' . trim($idString, ',') .')');
        	
        }

        if (null != @$condition['user_id']) {
            $select->where('c.user_id = ?', $condition['user_id']);
        }

        if (null != @$condition['tinnhanh']) {
            $select->where('c.tinnhanh = ?', $condition['tinnhanh']);
        }
        
        return $this->fetchAll($select)->toArray();
    }

    public function getAllProductEnabled($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
            ->join(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
            ->join(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
            ->join(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
            ->where('c.check_product=2')
            ->order($order)
            ->limit($count, $offset);
        /**
         * Conditions
         */

        if (null != @$condition['tinnhanh']) {
            $select->where('c.tinnhanh = ?', $condition['tinnhanh']);
        }

        return $this->fetchAll($select)->toArray();
    }

	public function getAlltinlienquan($gid,$id)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.product_category_gid = ?', $gid)
                ->where('c.product_id <> ?', $id)
                ->order('c.product_id Desc')
                ->limit(10);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    
	public function getAllProductNew()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.created_date DESC")
                ->limit(10);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }

    public function getProductByAlias($alias)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
            ->joinLeft(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
            ->joinLeft(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
            ->joinLeft(array('cs' => $this->_prefix . 'properties_category'), 'c.supply_case = cs.properties_category_gid', array('csname' => 'name'))
            ->joinLeft(array('cst' => $this->_prefix . 'properties_category'), 'c.supply_tine = cst.properties_category_gid', array('cstname' => 'name'))
            ->joinLeft(array('tl' => $this->_prefix . 'properties_category'), 'c.trong_luong_gid = tl.properties_category_gid', array('tlname' => 'name'))
            ->joinLeft(array('dg' => $this->_prefix . 'properties_category'), 'c.donggoi = dg.properties_category_gid', array('dgname' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name', 'name_company'=>'name_company'))
            ->joinLeft(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('c.alias=?', $alias);
        /**
         * Conditions
         */

        return $this->fetchAll($select)->toArray();
    }

    public function getProductRelatedByUserID($user_id, $product_category_gid = null, $count = null, $offset = null) {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('c.user_id=?', $user_id)
            ->limit($count, $offset);

        /**
         * Conditions
         */

        if(@$product_category_gid != null) {
            $select->where('c.product_category_gid=?', $product_category_gid);
        }

        return $this->fetchAll($select)->toArray();
    }

    public function getProductRelated($product_category_gid, $product_gid, $user_id = null, $count = 0) {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
            ->join(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
            ->join(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name', 'name_company'=>'name_company', 'user_alias' => 'alias'))
            ->join(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('c.product_category_gid=?', $product_category_gid)
            ->where('c.product_gid <>?', $product_gid);

        if($count != 0) {
            $select->limit($count);
        }
        /**
         * Conditions
         */

        if(@$user_id != null) {
            $select->where('c.user_id=?', $user_id);
        }
        return $this->fetchAll($select)->toArray();
    }

 	public function getAllProductByArrCat($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
	            ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
	            ->join(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
	            ->join(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
	            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
	            ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
	            ->join(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
	            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
	            ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
	            ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
	            ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
        if (null != @$condition['keyword']) {
            $sql = "SELECT product_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('alias LIKE ?', "%{$condition['keyword']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['product_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.product_gid IN (' . trim($idString, ',') .')');
        }
    	if (null != @$condition['name']) {
            $sql = "SELECT product_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('alias LIKE ?', "%{$condition['name']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['product_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.product_gid IN (' . trim($idString, ',') .')');
        }
        if (null != @$condition['product_category_gid']) {
        	
        	$sql = "SELECT gid_string FROM {$this->_prefix}" . 'product_category' . " WHERE " . $this->getAdapter()->quoteInto('product_category_gid = ? ', $condition['product_category_gid']);
        	$ids = @reset($this->_db->fetchAll($sql));
        	if (null == $ids) {
        	    $ids = '0';
        	}
            /**
             * Add to select object
             */
            $select->where('c.product_category_gid IN (' . trim($ids['gid_string'], ',') .')');
        	
        }
        
    	if (null != @$condition['genabled']) {
        	
        	$sql = "SELECT product_gid FROM {$this->_name} WHERE genabled = " . $condition['genabled'];
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['product_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('c.product_gid IN (' . trim($idString, ',') .')');
        	
        }

        if(null != @$condition['str_category']){
        	$select->where('c.product_category_gid IN (' . trim($condition['str_category'], ',') .')');
        }

        if(null != @$condition['xuat_xu']){
            $select->where('c.xuat_xu=?', $condition['xuat_xu']);
        }
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllProductNewByCatGid($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.created_date DESC")
                ->limit(4);
         $select->where('cc.parent_id = ? or cc.parent_id IS NULL and c.product_category_gid=?' , $gid , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllProductByCatGid($arrgid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.product_category_gid IN(?)', $arrgid)
                ->limit(2)
                ->order("c.created_date DESC");
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    
	public function getAllProductNewByCatGidIndex($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.genabled=?', 1)
                ->order("c.created_date DESC")
                ->limit(5);
         $select->where('c.product_category_gid=?' , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllProductNewByCatGidCateLimit($gid , $limit)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.genabled=?', 1)
                ->order("c.created_date DESC")
                ->limit($limit);
         $select->where('c.product_category_gid=?' , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllProductNewByCatGidDetail($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.created_date DESC");
         $select->where('cc.parent_id = ? or cc.parent_id IS NULL and c.product_category_gid=?' , $gid , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    public function getAllProductNewByCatGidCon($gid)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.created_date DESC");
         $select->where('c.product_category_gid=?' , $gid);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getProductHotOne()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.like DESC")
                ->limit(1);
        /**
         * Conditions
         */
        
        return @reset($this->fetchAll($select)->toArray());
    }
	public function getProductEvent()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.product_category_gid=?', '116')
                ->where('c.genabled=?', '1')
                ->order("c.publish_up_date DESC");
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllProductHot()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.like DESC")
                ->limit(5);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllProductView()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.count_view DESC")
                ->order(" c.publish_up_date DESC")
                ->limit(4);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllProductComment()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.comment DESC")
                ->order("c.publish_up_date DESC")
                ->limit(4);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
public function getAllProductLike()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("c.like DESC")
                ->order("c.publish_up_date DESC")
                ->limit(4);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    
	public function getAllProductTinNhanh()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.tinnhanh=?', '1')
                ->order("c.publish_up_date DESC")
                ->limit(4);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
    
    
	public function getAllEnabledProductByCategory( $catGid, $condition = array(), $order = null, $count = null, $offset = null)
    {
    	
    	$objCat = new Models_ProductCategory();
    	
    	$cat = @reset($objCat->getByColumns(array('product_category_gid=?' => $catGid))->toArray());
    	
    	$gidStr = @trim($cat['gid_string'].',0',','); 
    	/**
    	 * Get all enabled categories
    	 */
    	$select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_prefix . 'product_category'), array('product_category_gid'))
                ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
                ->where('cc.product_category_gid IN (' . $gidStr .')');
                
        $cats   = $this->fetchAll($select)->toArray();
        $gidStr = '';
        foreach ($cats as $cat) {
        	$gidStr .= $cat['product_category_gid'] . ',';
        }
        $gidStr = @trim($gidStr.'0',',');
    	
        /**
         * Get all enabled products in enabled categories
         */
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->where('product_category_gid IN (' . $gidStr .')')
                ->order($order)
                ->limit($count, $offset);

         /**
          * Condition
          */       
         if (null != @$condition['exclude_product_gids']) {
         	$gidStr = trim($condition['exclude_product_gids'].',0',',');
         	$select->where('product_gid NOT IN (' . $gidStr .')');
         }
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledProductsearch( $search, $order = null, $count = null, $offset = null)
    {
    	
    	
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->order($order)
                ->limit($count, $offset);
         
        $idString = '';

        $sql = "SELECT product_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('title LIKE ?', "%{$search}%");
        $ids = $this->_db->fetchAll($sql);
    	
		foreach ($ids as $row) {
     		$idString .= $row['product_gid'] . ',';
   		}
   		
   		if($idString != ""){
   		}else{
   			$sql = "SELECT product_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('full_text LIKE ?', "%{$search}%") ;
	   		$ids = $this->_db->fetchAll($sql);
			foreach ($ids as $row) {
	     		$idString .= $row['product_gid'] . ',';
	   		}
   		}
    	
    	
   		
   		$search = $this->convert_vi_to_en($search);
   		$search = str_replace(" ", "-" , $search);
   		
   		if($idString != ""){
   		}else{
   			$sql = "SELECT product_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('alias LIKE ?', "%{$search}%");
	   		$ids = $this->_db->fetchAll($sql);
	    	
			foreach ($ids as $row) {
	     		$idString .= $row['product_gid'] . ',';
	   		}
   		}
    	
       
        $select->where('product_gid IN (' . trim($idString, ',') .')');
        $select->where('product_category_gid !=?', '116');
        $select->where('product_category_gid !=?', '118');
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledProductTag( $search, $order = null, $count = null, $offset = null)
    {
    	
    	
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->order($order)
                ->limit($count, $offset);
         
         $idString = '';
         $sql = "SELECT product_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('tag LIKE ?', "%{$search}%");
         $ids = $this->_db->fetchAll($sql);
         if (empty($ids)) {
         	return array();
            }
            foreach ($ids as $row) {
                $idString .= $row['product_gid'] . ',';
            }
        
       
        $select->where('product_gid IN (' . trim($idString, ',') .')');
        $select->where('product_category_gid !=?', '116');
        $select->where('product_category_gid !=?', '118');
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledProductTagAdmin( $search, $id_s = null)
    {
    	
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->order("created_date DESC");
         
         $idString = '';
         $sql = "SELECT product_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('tag LIKE ?', "%{$search}%" )."";
         $ids = $this->_db->fetchAll($sql);
         if (empty($ids)) {
         	return array();
            }
            foreach ($ids as $row) {
                $idString .= $row['product_gid'] . ',';
            }
        
       	if(trim($id_s, ',') != ""){
       		$select->where('product_gid NOT IN (' . trim($id_s, ',') .')');
       	}
        $select->where('product_gid IN (' . trim($idString, ',') .')');
        
        $select->where('lang_id = 2');
        $select->where('product_category_gid !=?', '116');
        $select->where('product_category_gid !=?', '118');
        
		
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledProductByidUser( $user, $condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
            ->join(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
            ->join(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
            ->join(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
            ->where('c.check_product=2')
            ->where('c.user_id = ?' , $user)
            ->order($order)
            ->limit($count, $offset);

        if(null != @$condition['tinnhanh']) {
            $select->where('tinnhanh=?', $condition['tinnhanh']);
        }

    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledProductByidUserLike( $user, $condition = array(), $order = null, $count = null, $offset = null)
    {
    	
    	$objCat = new Models_ProductCategory();
    	
                
        
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->order($order)
                ->limit($count, $offset);
        $select->where('user_like LIKE ?', ',' . $user .',');

    	return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledProductByCategory2( $catGid, $condition = array(), $order = null, $count = null, $offset = null)
    {
    	
    	$objCat = new Models_ProductCategory();
    	
    	$cat = @reset($objCat->getByColumns(array('product_category_gid=?' => $catGid))->toArray());
    	
    	$gidStr = @trim($cat['gid_string'].',0',','); 
    	/**
    	 * Get all enabled categories
    	 */
    	$select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_prefix . 'product_category'), array('product_category_gid'))
                ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
                ->where('cc.product_category_gid IN (' . $gidStr .')');
                    
        $cats   = $this->fetchAll($select)->toArray();
        $gidStr = '';
        foreach ($cats as $cat) {
        	$gidStr .= $cat['product_category_gid'] . ',';
        }
        $gidStr = @trim($gidStr.'0',',');
    	
        /**
         * Get all enabled products in enabled categories
         */
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1')
                ->where('product_category_gid IN (' . $gidStr .')')
                ->order($order)
                ->limit($count, $offset);

         /**
          * Condition
          */       
         if (null != @$condition['exclude_product_gids']) {
         	$gidStr = trim($condition['exclude_product_gids'].',0',',');
         	$select->where('product_gid NOT IN (' . $gidStr .')');
         }
               
    	return $this->fetchAll($select)->toArray();
    }
    public function getLatestProductByCategory( $catGid )
    {
    	$allProduct = $this->getAllEnabledProductByCategory($catGid, array(), array('sorting ASC','product_gid DESC','product_id DESC'),1,0);
    	return @reset($allProduct);
    }
    public function convert_vi_to_en($str) {
	    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
	    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
	    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
	    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
	    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
	    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
	    $str = preg_replace("/(đ)/", 'd', $str);
	    $str = preg_replace("/(D)/", 'd', $str);
	    $str = preg_replace("/(L)/", 'l', $str);
	    $str = preg_replace("/(K)/", 'k', $str);
	    $str = preg_replace("/(Q)/", 'q', $str);
	    $str = preg_replace("/(R)/", 'r', $str);
	    $str = preg_replace("/(T)/", 't', $str);
	    $str = preg_replace("/(N)/", 'n', $str);
	    $str = preg_replace("/(C)/", 'c', $str);
	    $str = preg_replace("/(B)/", 'b', $str);
	    $str = preg_replace("/(M)/", 'm', $str);
	    
	    $str = preg_replace("/(O)/", 'o', $str);
	    $str = preg_replace("/(P)/", 'p', $str);
	    $str = preg_replace("/(S)/", 's', $str);
	    $str = preg_replace("/(G)/", 'g', $str);
	    $str = preg_replace("/(H)/", 'h', $str);
	    $str = preg_replace("/(V)/", 'v', $str);
	    $str = preg_replace("/(X)/", 'x', $str);
	    $str = preg_replace("/(R)/", 'r', $str);
	    $str = preg_replace("/(E)/", 'e', $str);
	    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'a', $str);
	    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'e', $str);
	    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'i', $str);
	    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'o', $str);
	    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'u', $str);
	    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'y', $str);
	    $str = preg_replace("/(Đ)/", 'd', $str);
	    //$str = str_replace(" ", "-", str_replace("&*#39;","",$str));
	 
	    return $str;
	 
	}
    public function increaseSorting($startPos = 1, $num = 1)
    {
        $sql = "UPDATE {$this->_name} SET sorting = sorting + " . intval($num) . " WHERE sorting >= " . intval($startPos);
        
        $this->_db->query($sql);
    }
    
    /**
     * Get product by gid
     * 
     * @param int $gid
     * @return Zend_Db_Table_Row
     */
    public function getProductByGid($gid)
    {
        $this->setAllLanguages(false);
        $select = $this->select()
        ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
        ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
            ->joinLeft(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
            ->joinLeft(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
            ->joinLeft(array('cs' => $this->_prefix . 'properties_category'), 'c.supply_case = cs.properties_category_gid', array('csname' => 'name'))
            ->joinLeft(array('cst' => $this->_prefix . 'properties_category'), 'c.supply_tine = cst.properties_category_gid', array('cstname' => 'name'))
            ->joinLeft(array('tl' => $this->_prefix . 'properties_category'), 'c.trong_luong_gid = tl.properties_category_gid', array('tlname' => 'name'))
            ->joinLeft(array('dg' => $this->_prefix . 'properties_category'), 'c.donggoi = dg.properties_category_gid', array('dgname' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name', 'name_company'=>'name_company'))
            ->joinLeft(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('cs.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('cst.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('product_gid=?', $gid);
                
        return $this->fetchRow($select);
    }
//    public function getProductByUrl($url, $langId)
//    {
//        $url = $this->getAdapter()->quote($url);
//        $langId = intval($langId);
//        $time = time();
//        
//        $query = "
//                 SELECT *
//                 FROM ( SELECT * 
//                        FROM {$this->_prefix}product_lang 
//                        WHERE lang_id = {$langId} AND enabled = 1 AND url = {$url} 
//                       ) AS sl
//                 JOIN ( SELECT product_id, product_category_gid, enabled AS senabled, publish_up_date, publish_down_date, sorting AS ssorting, created_date, layout,image
//                        FROM {$this->_prefix}product 
//                        WHERE enabled = 1 AND publish_up_date <= {$time} AND (publish_down_date = 0 OR publish_down_date > {$time} )
//                       ) AS s
//                 ON s.product_id = sl.product_id
//                 LIMIT 0,1
//        ";
////        echo $query;die;
//        $result =$this->_db->fetchRow($query);
//         $array = explode(" ", $result['title']);
//         
//        foreach ($array as $item){
//           $result['titleeach'][] = substr($item,0,1); 
//           $result['titleeach'][] = substr($item,1,strlen($item)-1); 
//        }
//        return $result;
//    }

    public function getAllProductByEstoreCategory($gid, $count = null, $offset = null)

    {
            
            
            
            $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
            ->join(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
            ->join(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
            ->join(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
            ->where('c.estore_category_gid=?', $gid)
            ->where('c.check_product=2')
            ->limit($count, $offset);

        /**
         * Conditions
         */

        return $this->fetchAll($select)->toArray();
    }

    public function getAllProductSearch($search, $user_id = null, $count = null, $offset = null)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
            ->join(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
            ->join(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
            ->joinLeft(array('pu' => $this->_prefix . 'user_profile'), 'u.user_id = pu.user_id AND pu.lang_id = '.Nine_Language::getCurrentLangId(), array('padd' => 'add', 'pcompany_name' => 'company_name', 'pnote' => 'note', 'pabout' => 'about', 'pproduct_chinh' => 'product_chinh', 'pproduct_khac' => 'product_khac'))
            ->join(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
            ->where('c.check_product=2')
            ->where('c.title LIKE "%' . $search . '%"')
            ->limit($count, $offset);

        /**
         * Conditions
         */

        if(null != $user_id) {
            $select->where('c.user_id=?', $user_id);
        }

        return $this->fetchAll($select)->toArray();
    }

    public function getAllProductByTpp($tpp, $count=null, $offset=null)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('c' => $this->_name))
            ->join(array('cc' => $this->_prefix . 'product_category'), 'c.product_category_gid = cc.product_category_gid', array('cname' => 'name','product_deleteable' => 'product_deleteable'))
            ->join(array('p' => $this->_prefix . 'properties_category'), 'c.so_luong_gid = p.properties_category_gid', array('pname' => 'name'))
            ->join(array('p2' => $this->_prefix . 'properties_category'), 'c.unit = p2.properties_category_gid', array('pname2' => 'name'))
            ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','full_name'=>'full_name'))
            ->join(array('ct' => $this->_prefix . 'country_category'), 'c.xuat_xu = ct.country_category_gid', array('ctname' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('p2.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('ct.lang_id=?', Nine_Language::getCurrentLangId())
            ->where('cc.enabled = 1 AND cc.genabled = 1 AND cc.parent_genabled = 1')
            ->where('c.xuat_xu=?', $tpp)
            ->where('c.check_product=2')
            ->limit($count, $offset);

        /**
         * Conditions
         */

        return $this->fetchAll($select)->toArray();
    }
}