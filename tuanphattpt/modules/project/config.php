<?php
return array(
    'permissionRules' => array(
        'see_project' => array(//External permission
            'See all articles',//Full control name
            '9_lang',//table
            'lang_id', //Check permisison column
            'name'),//Display column at permission interface
        'new_project' => array(//External permission
            'Create new article',//Full control name
            '9_lang',//table
            'lang_id', //Check permisison column
            'name'),//Display column at permission inteface
        'edit_project' => array(//External permission
            'Edit existed articles',//Full control name
            '9_lang',//table
            'lang_id', //Check permisison column
            'name'),//Display column at permission inteface

        'genabled_project' => 'Genabled existed articles',
        'delete_project' => 'Delete existed articles'
    ),

);
