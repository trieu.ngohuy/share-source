<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/user/models/User.php';

require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/content/models/Content.php';

class access_ActiveController extends Nine_Controller_Action {

    /**
     * The default action - show the access page
     */
    public function indexAction() {
        $this->setLayout('access');
        $params = $this->_request->getParams();
        $code = $params['code'];


        $objUser = new Models_User();
        $user = $objUser->getByColumns(array(
                    "active_code" => $code
                ))->toArray();

        //echo "<pre />";print_r($user);die;
        if (count($user) > 0 && $code != '') {
            $user = @reset($user);
            $username = $user['username'];
            $objUser->updateActive($username);
            $message = array(
                'success' => true,
                'message' => Nine_Language::translate('Chúc mừng bạn đã kích hoạt tài khoản thành công!')
            );
            $this->view->accessMessage = $message;
        } else {
            $message = array(
                'success' => false,
                'message' => Nine_Language::translate('Mã kích hoạt không tồn tại.')
            );
            $this->view->accessMessage = $message;
        }
    }

}
