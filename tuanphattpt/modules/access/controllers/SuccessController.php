<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/user/models/User.php';
include_once 'modules/mail/models/Mail.php';
include_once 'modules/mail/models/phpmailer.php';
include_once 'modules/mail/models/smtp.php';

require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/content/models/Content.php';

require_once 'Zend/Config.php';
require_once 'Zend/Config/Writer/Array.php';

class access_SuccessController extends Nine_Controller_Action {

    /**
     * The default action - show the access page
     */
    public function indexAction() {
        //SET LAYOUTS
        $this->setLayout('access');
        $objProduct = new Models_Product ();
        $objProductCategory = new Models_ProductCategory();
        $objContent = new Models_Content ();

        $message = $this->session->message;
        if (isset($_SESSION['message']) && $_SESSION['message'] != '') {
            $message = $_SESSION['message'];
            $_SESSION['message'] = '';
        }
        $this->session->message = '';
        $this->view->message = $message;

        $objProduct = new Models_Product ();
        $objProductCategory = new Models_ProductCategory();
        $objContent = new Models_Content ();
        $allFlasher = $objProduct->getAllProductTinNhanh();
        foreach ($allFlasher as &$content) {
            $content ['title'] = Nine_Function::subStringAtBlank(trim(strip_tags($content ['title'])), 40);
            $content ['intro_text'] = Nine_Function::subStringAtBlank(trim(strip_tags($content ['full_text'])), 120);
            if ($content ['alias'] == "") {
                $content ['alias'] = $objContent->convert_vi_to_en($content ['alias']);
                $content ['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", trim($content ['alias'])));
            }
            $content ['url'] = Nine_Route::_("product/index/detail/gid/{$content['product_gid']}", array('alias' => $content ['alias']));
            $tmp = explode('||', $content ['images']);
            $content ['main_image'] = Nine_Function::getThumbImage(@$tmp [0], 182, 109, false, false);
        }
        $this->view->allFlasher = $allFlasher;
    }

}
