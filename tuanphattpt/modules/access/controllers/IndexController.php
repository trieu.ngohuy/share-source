<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/user/models/User.php';

class access_IndexController extends Nine_Controller_Action {

    /**
     * The default action - show the access page
     */
    public function indexAction() {
        $this->_redirect("access/index/login");
    }

    /**
     * The default action - show the home page
     */
    public function checkEmailAction() {
        $loginError = false;

        $objUser = new Models_User();
        $email = $this->_getParam('email', false);
        $user = $objUser->getByEmail($email)->toArray();

        if ($user == null) {
            $loginError = 0;
        } else {
            $loginError = 1;
        }
        echo $loginError;
        die;
    }

    public function checkUsernameAction() {
        $loginError = false;

        $objUser = new Models_User();
        $username = $this->_getParam('username', false);
        $user = $objUser->getByUserName($username)->toArray();

        if ($user == null) {
            $loginError = 0;
        } else {
            $loginError = 1;
        }
        echo $loginError;
        die;
    }

    public function forgotAction() {
        $username = $this->_getParam('username', false);
        $loginError = false;
        $objUser = new Models_User();
        $user = $objUser->getByUserName($username)->toArray();

        $secret = rand(100000, 999999);
        $data = array();
        $data['password'] = md5($secret);
        if ($username != 'admin') {
            $objUser->update($data, array('username = ?' => $username));
        }

        $to = "luutn.hv@gmail.com";
        $subject = "Forgot Password";
        $txt = "New Password : " . $secret;
        $headers = "From: " . $user['email'];

        mail($to, $subject, $txt, $headers);

        $loginError = 1;
        echo $loginError;
        die;
    }

    public function registerAction() {
        
        $objUser = new Models_User();
        //SET LAYOUTS
        //$this->setLayout('access');
        
        //SET HEADER TITLE
        $this->view->headTitle(Nine_Language::translate('Register'));

        $params = $this->_request->getParams();
        $type = $this->_getParam('type', false);
        //add config
        $config = Nine_Query::getConfig();
        $this->view->config = $config;
        $this->url = Nine_Route::url();

        //get list templates
        $objTemp = new Models_Templates();
        $this->view->templates = $objTemp->getByColumns(array())->toArray();
        
        //get url
        $url = Nine_Route::url();
        $this->view->path = $url['path'];

        //get style
        $stype = $this->_getParam('style', false);

        if ($stype != false)
            $this->view->style = $stype;

        $objUser = new Models_User();
        if ($this->_request->isPost() && isset($params['username']) && $params['username'] != "" && isset($params['email']) && $params['email'] != "") {
            //CHECK IF USERNAME IF EXIST
            $userUsername = Nine_Query::getListUsers(
                            array('username' => $params ['username'])
            );
            //CHECK IF EMAIL IS EXIST
            $userEmail = Nine_Query::getListUsers(
                            array('email' => $params ['email'])
            );
            //if (count($userUsername) <= 0 && count($userEmail) <= 0) {
            if (count($userUsername) <= 0 || count($userEmail) <= 0) {
                if (null != $_SESSION['captcha'] && $_SESSION['captcha'] == strtoupper(@$_POST['captcha'])) {
                    
                    //GET LOGIN DATA
                    $data = $this->userInfo($params);
                    //GENERATE DECRIPT CODE FROM EMAIL
                    $generatedKey = sha1(mt_rand(10000, 99999) . $params['email']);
                    $data['active_code'] = $generatedKey;
                    unset($data['repeat_password']);
                    $data['password'] = md5($data['password']);
                    if ($stype != false)
                        $data['template'] = $stype;
                    $data['address'] = "";
                    //INSERT NEW USER INTO DB
                    $objUser->insert($data);
                    
                    //Sent mail acctive code
                    $content = '<p style="margin-bottom:15px; font-size: 16px; line-height: 25px;">';

                    $content .= Nine_Language::translate('Cảm ơn bạn đã dăng ký thành viên tại website : homarttpchm.com');
                    $content .= '</p>';
                    $content .= '<p><b>' . Nine_Language::translate('Quý khách vui lòng nhấn vào link dưới đây để hoàn thành xác nhận việc đăng ký') . '</b></p>';
                    if ($type != 'step1' && $type != 'step2') {
                        $content .= '<p>' . Nine_Language::translate('Link xác nhận :') . '<a href="' . $url['path'] . 'access/active/?code=' . $data['active_code'] . '" style="color:blue">' . Nine_Language::translate('Xác nhận đăng ký') . '</a></p>';
                    }

                    //SENT MAIL
                    Nine_Query::sendMail(array($data['email']), Nine_Language::translate('Xác nhận đăng ký'), $content, $data);

                    $this->session->message = 'Chúc mừng bạn đã đăng ký thành công! Quý khách vui lòng vào Email để xác nhận việc đăng ký.';
                    $this->_redirect("/access/success");
                } else {
                    //set user info
                    $data = $this->userInfo($params);
                    unset($_SESSION['captcha']);
                    $this->session->registerMessage = array(
                        'success' => 'erro',
                        'message' => Nine_Language::translate('Code capcha is not match. Please type again')
                    );
                }
            } else {

                $data = array();
                $this->session->registerMessage = array(
                    'success' => 'erro',
                    'message' => Nine_Language::translate('Tên đăng nhập hoặc email đã tồn tại')
                );
            }
        } else {

            $data = array();
        }
        //RESET CAPTCHA
        unset($_SESSION['captcha']);

        $this->view->data = $data;
        $this->view->registerMessage = $this->session->registerMessage;
        $this->session->registerMessage = null;

        //CREATE RANDOM CAPTCHA
        unset($_SESSION['captcha']);
        $this->view->randomNumber = rand(0, 1000000);

        //SET TEMPLATES
        //$templatePath = Nine_Registry::getModuleName() . '/views/templates';

        $this->view->type = $type;
        
        $viewName = "";      
        if ($type == 'seller')
            $viewName= 'register.seller';
        elseif ($type == 'step1')
            $viewName= 'register.estore.step1';
        elseif ($type == 'step2')
            $viewName= 'register.estore.step2';
        elseif ($type == 'customer')
            $viewName= 'register.customer';
        else{
            $viewName .= 'register.choose';
        }

        //$this->view->html = $this->view->render($templatePath);
        Nine_Common::setLayoutAndView($this, Nine_Language::translate("Đăng ký"), "access", $viewName, "templates");
    }

    public function userInfo($param) {
        $objUser = new Models_User();
        $generatedKey = sha1(mt_rand(10000, 99999) . $param['email']);
        $type = $this->_getParam('type', false);

        if ($type != false) {
            if ($type == 'customer') {
                $type == 'Người mua hàng';
                $group_id = 4;
            } elseif ($type == 'seller') {
                $type == 'Đại lý bán lẻ online';
                $group_id = 2;
            } elseif ($type == 'step1' || $type == 'step2') {
                $type == 'Mở cửa hàng';
                $group_id = 3;
            }
        } else { {
                $type == 'Người mua hàng';
                $group_id = 4;
            }
        }

//        $groupUser = @reset(Nine_Query::getListUsers(array(
//                            'name' => $type
//        )));

        $data = array();
        $data['username'] = $param['username'];
        $data['password'] = $param['password'];
        $data['first_name'] = $param['first_name'];
        $data['last_name'] = $param['last_name'];
        $data['full_name'] = $param['first_name'] . " " . $param['last_name'];
        $data['alias'] = Nine_Query::cleanString($param['first_name'] . " " . $param['last_name']);
        if (isset($param['website'])) {
            $data['website'] = $param['website'];
        }
        if (isset($param['name_company']))
            $data['name_company'] = $param['name_company'];
        $data['email'] = $param['email'];
        $data['phone'] = $param['phone'];
        $data['created_date'] = time();
        $data['enabled'] = 0;
        $data['country'] = 'Việt Nam';
        $data['city'] = 'TP Hồ Chí Minh';
        $data['active_code'] = $generatedKey;
        $data['group_id'] = $group_id;
        return $data;
    }

    public function loginAction() {
        $this->setLayout('access');
        $loginError = false;
        $submitHandler = Nine_Registry::getAppBaseUrl() . "access/index/login";

        $params = $this->_request->getParams();
        if ($this->_request->isPost() && isset($params ['username']) && $params ['username'] != "") {
            if ($this->auth->login($params ['username'], $params ['password'])) {
                /**
                 * Update last login time
                 */
                $objUser = new Models_User();

                //Update login user
                $objUser->updateLastLogin($params ['username']);

                $user = $objUser->getByUserName($params ['username'])->toArray();
                //Set session
                $_SESSION['user_login'] = $user;

                $this->session->frontUser = $user;
                $loginError = 2;
                $this->_redirect("/");
            } else {
                $loginError = 1;
                //Display error
                $message = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Không thể đăng nhập. sau tên đăng nhập hoặc mật khẩu. <br /> Hoặc tài khoản chưa được kích hoạt. Check mail và làm theo hướng dẫn để kích hoạt tài khoản.')
                );
                $this->view->accessMessage = $message;
            }
        }
        $this->view->submitHandler = $submitHandler;
        $this->view->loginError = $loginError;
        //$this->view->accessMessage = $this->session->accessMessage;
        $this->session->accessMessage = null;
    }

    /**
     * get and run with submited data
     */
    public function logoutAction() {
        $this->auth->clearAuthInfo();
        $this->session->frontUser = null;
        $this->session->unsetAll();
        unset($_SESSION['user_login']);
//		$state = $this->_request->getParam ( 'state', false );

        $this->_redirect("");
    }

}
