<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/user/models/User.php';

class access_AdminController extends Nine_Controller_Action {
	/**
	 * The default action - show the access page
	 */
	public function indexAction() {
	    $this->_redirect("access/admin/login");
	}
	
	/**
	 * The default action - show the home page
	 */
	public function loginAction()
	{
		$this->view->headTitle(Nine_Language::translate("Login to Ninesual Idea Control Panel"));
		$this->setLayout('default', 'default');
		
		$loginError = false;
		$submitHandler = Nine_Registry::getAppBaseUrl() . "access/admin/login";
		
		$params = $this->_request->getParams ();
		if ($this->_request->isPost () && isset($params ['username']) && $params ['username'] != "") {			
			if ($this->auth->login( $params ['username'], $params ['password'] )) {
                /**
                 * Update last login time
                 */
			    $objUser = new Models_User();
			    //Update login user
			    $objUser->updateLastLogin($params ['username']);

			    $user = $objUser->getByUserName($params ['username'])->toArray();
			    //Set session
			    $_SESSION['user_login'] = $user;
			    
			    $this->session->frontUser =  $user;
				$loginError = 2;
				echo $loginError;die;
			} else {
				$loginError = 1;
				echo $loginError;die;
			}
		}
		$this->view->submitHandler = $submitHandler;
		$this->view->loginError = $loginError;
		$this->view->accessMessage = $this->session->accessMessage;
		$this->session->accessMessage = null;
	}
	/**
	 * get and run with submited data
	 */
	public function logoutAction() 
	{
		$this->auth->logout ();
		unset($_SESSION['user_login']);
		$this->_redirect ("");
	}
}