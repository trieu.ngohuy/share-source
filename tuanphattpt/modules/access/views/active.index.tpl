<br /><br />
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Thông báo!!!</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3 col-md-3">

                            {{if $accessMessage.success  == 'true'}}
                            <img src="https://cdn2.iconfinder.com/data/icons/user-management/512/user_growth-512.png" width="100" />
                            {{else}}					
                            <img src="http://www.freeiconspng.com/uploads/error-icon-3.png" width="100" />
                            {{/if}}
                        </div>
                        <div class="col-sm-9 col-md-9">
                            <br />
                            {{if $accessMessage.success  == 'true'}}
                            <h3>{{l}}Tài khoản kích hoạt thành công{{/l}}</h3>

                            <p>{{$accessMessage.message}} {{l}}Vui lòng {{/l}} <a class="" href="{{$BASE_URL}}login.html"> {{l}}Đăng nhập{{/l}}</a> &nbsp; &nbsp; &nbsp;</p>			
                            {{else}}					
                            <h3>{{l}}Tài khoản kích hoạt không thành công{{/l}}</h3>

                            <p>{{$accessMessage.message}} &nbsp; &nbsp; &nbsp;</p>			
                            {{/if}}
                        </div>
                    </div>
                </div>
            </div>
        </div>		
        <div class="col-md-3"></div>
    </div>
</div>