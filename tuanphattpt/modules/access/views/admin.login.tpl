<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		
		{{$headLink}}
		{{$headStyle}}
		{{$headTitle}}
		<link rel="shortcut icon" href="{{$LAYOUT_HELPER_URL}}admin/assets/ico/favicon.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/bootstrap.css" />
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/ace-fonts.css" />

		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace-extra.js"></script>

	</head>
	
	<body class="login-layout light-login" >
		
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<i class="ace-icon fa fa-leaf green"></i>
									<span class="red">HOMARTTPHCM</span>
									<span class="white" id="id-text2">APPLICATION</span>
								</h1>
								<h4 class="blue" id="id-company-text">&copy; GROUP TPP</h4>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-coffee green"></i>
												Please Enter Your Information
											</h4>

											<div class="space-6"></div>
											<div class="alert alert-danger hidden" id="erro">
												<button data-dismiss="alert" class="close" type="button">
													<i class="ace-icon fa fa-times"></i>
												</button>
	
												<strong>
													<i class="ace-icon fa fa-times"></i>
													Oh snap!
												</strong>
	
												<p class="erro"></p>
											</div>
											<form id="form-signin" class="form-signin" action="{{$submitHandler}}" method="post" name="login">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input  type="text" class="form-control" name="username" id="inputEmail1" placeholder="{{l}}Username{{/l}}" required data-fv-notempty-message="{{l}}The username is required{{/l}}">
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" id="inputPassword"  name="password" placeholder="{{l}}Password{{/l}}" required data-fv-notempty-message="{{l}}The password is required{{/l}}">
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">

														<button type="submit"  id=" sign-in" class="width-40 pull-right btn btn-sm btn-primary">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">{{l}}Sign in{{/l}}</span>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>

										</div><!-- /.widget-main -->

									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->


							</div><!-- /.position-relative -->

						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->
					
		<script type="text/javascript">
			window.jQuery || document.write("<script src='{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/bootstrap.js"></script>


		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery-ui.custom.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.ui.touch-punch.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.easypiechart.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.sparkline.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/flot/jquery.flot.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/flot/jquery.flot.pie.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/flot/jquery.flot.resize.js"></script>

		<!-- ace scripts -->
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.scroller.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.colorpicker.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.fileinput.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.typeahead.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.wysiwyg.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.spinner.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.treeview.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.wizard.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.aside.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.ajax-content.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.touch-drag.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.sidebar.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.submenu-hover.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.widget-box.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.settings.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.settings-rtl.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.settings-skin.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.searchbox-autocomplete.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>

		<!-- inline scripts related to this page -->

		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin//docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.onpage-help.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.onpage-help.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin//docs/assets/js/rainbow.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin//docs/assets/js/language/generic.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin//docs/assets/js/language/html.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin//docs/assets/js/language/css.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin//docs/assets/js/language/javascript.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.min.js"></script>
		<script src="{{php}} echo SOCKET_IO; {{/php}}js/moment.min.js"></script>
		<script src="{{php}} echo SOCKET_IO; {{/php}}socket.io/socket.io.js"></script>
		<script src="{{php}} echo SOCKET_IO; {{/php}}js/style.js"></script>
		{{php}}
		$user_id = isset($_SESSION['user_login']) ? $_SESSION['user_login']['user_id'] : 0;
		$user_name = isset($_SESSION['user_login']) ? $_SESSION['user_login']['username'] : null;
		$img = isset($_SESSION['user_login']['image']) && $_SESSION['user_login']['image'] !== '' ? BASEURL.$_SESSION['user_login']['image'] : 'http://localhost:8080/img/unnamed.jpg';
		{{/php}}
		<script type="text/javascript">
			/*$(document).ready(function () {
				var us = '{{php}} echo $user_id ;{{/php}}';
				var un = '{{php}} echo $user_name ;{{/php}}';
				var img = '{{php}} echo $img ;{{/php}}';
				var baseUrl = location.protocol + '//' + location.host ;
				LiveChat.init({
					user_id:us,
					name:un,
					img:img,
					currentUrl:baseUrl
				});
			});*/
		</script>
		<script type="text/javascript">
			jQuery(function($) {
				$("#form-signin").submit(function(event){
					event.preventDefault();
					//LiveChat.adminLoginCheck(this,function (bool) {
						//if(bool){
							var main=$("#main");
							//scroll to top
							main.animate({
								scrollTop: 0
							}, 500);
							main.addClass("slideDown");
							// send username and password to php check login
							$.ajax({
								url: "{{$APP_BASE_URL}}access/admin/login", data: $("#form-signin").serialize(), type: "POST", dataType: 'json',
								success: function (e) {
									setTimeout(function () { main.removeClass("slideDown") }, !e.status ? 500:3000);
									if (e == 1) {
										$('#erro').removeClass('hidden');
										$('.erro').html('Check Username or Password again !!');
										return false;
									}
									setTimeout(function () { $("#loading-top span").text("Yes, account is access...") }, 500);
									setTimeout(function () { $("#loading-top span").text("Redirect to account page...")  }, 1500);
									setTimeout( "window.location.href='{{$APP_BASE_URL}}default/admin/index'", 300 );
								}
							});
						//}else{
							//alert('Admin Đang Đăng nhập.');
						//}
					//});
				});
				$(document).on('click', '.toolbar a[data-target]', function(e) {
					e.preventDefault();
					var target = $(this).data('target');
					$('.widget-box.visible').removeClass('visible');//hide others
					$(target).addClass('visible');//show target
				});
			});
			//you don't need this, just used for changing background
			jQuery(function($) {
				$('#btn-login-dark').on('click', function(e) {
					$('body').attr('class', 'login-layout');
					$('#id-text2').attr('class', 'white');
					$('#id-company-text').attr('class', 'blue');

					e.preventDefault();
				});
				$('#btn-login-light').on('click', function(e) {
					$('body').attr('class', 'login-layout light-login');
					$('#id-text2').attr('class', 'grey');
					$('#id-company-text').attr('class', 'blue');

					e.preventDefault();
				});
				$('#btn-login-blur').on('click', function(e) {
					$('body').attr('class', 'login-layout blur-login');
					$('#id-text2').attr('class', 'white');
					$('#id-company-text').attr('class', 'light-blue');

					e.preventDefault();
				});

			});
		</script>
	<style>
		.section{
			display: none;
		}
	</style>
	</body>
</html>
