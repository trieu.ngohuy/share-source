<div class="container">
    <div class="row">				
        <div class="category-col-left product-list col-md-12">
            <br /><br />
            <div class="page-title text-center">
                <h1>{{l}}Register{{/l}}</h1>
            </div>    
            <hr>
            {{if $message != ''}}
            <div class="alert alert-success col-sm-12">
                <button class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>
                <div class="helping-text wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="0.8s"><strong>{{l}}{{$message}}{{/l}}</strong></div>
            </div>
            {{/if}}			

        </div>

    </div>
</div>



