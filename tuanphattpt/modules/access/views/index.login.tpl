
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}HOME{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{l}}LOGIN{{/l}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-sm-9">
                <div class="box-authentication">
                    <h2 style="text-align: center; margin-bottom: 20px;">{{l}}Login{{/l}}</h2>
                    {{if $accessMessage|@count > 0}}
                    <div class="alert alert-warning">
                        <button class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>
                        {{$accessMessage.message}}.
                    </div>
                    {{/if}}
                    <form class="form-horizontal" data-toggle="validator" role="form"  method="post" action="{{$submitHandler}}">

                        <label class="text-right col-sm-5">{{l}}Tên Đăng Nhập{{/l}}</label>
                        <input class="form-control" type="text" value="" placeholder="Nhập tên đăng nhập" name="username" required>
                        <br class="cb">
                        <label class="text-right col-sm-5">{{l}}Mật Khẩu{{/l}}</label>
                        <input class="form-control" type="password" value="" placeholder="Nhập mật khẩu" name="password" required>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-7" style="text-align: center;">
                                <button class="button col-md-4" type="submit"><i class="fa fa-lock"></i> {{l}}Đăng nhập{{/l}}</button>
                                <p class="col-md-8" style="margin-top: 24px;"><a href="#">{{l}}Quên mật khẩu?{{/l}}</a></p>
                            </div>
                        </div>
                    </form>
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-4"><i class="fa fa-custom fa-user" style="margin-top: 2px;margin-right: 3px;"></i><a href="{{$BASE_URL}}access/index/register/type/customer">{{l}}Đăng ký người mua hàng{{/l}}</a></div>
                        <div class="col-md-5"><i class="fa fa-custom fa-shopping-cart" style="margin-top: 2px;margin-right: 3px;"></i><a href="{{$BASE_URL}}access/index/register/type/seller">{{l}}Đăng ký Đại lý bán lẻ online{{/l}}</a></div>
                        <div class="col-md-3"><i class="fa fa-custom fa-hotel" style="margin-top: 2px;margin-right: 3px;"></i><a href="{{$BASE_URL}}access/index/register/type/step1">{{l}}Mở Gian hàng{{/l}}</a></div>
                    </div>
                    <!--div class="row" style="margin-top: 15px;">
                        <p class="col-md-3" style="margin-top: 11px;">{{l}}Đăng nhập với: {{/l}}</p>
                        <div class="row col-md-4">
                            <a href="#"><img src="http://www.freeiconspng.com/uploads/fb-icon-11.png" width="40"/></a>
                            <a href="#"><img src="http://dpi.wi.gov/sites/default/files/imce/math/images/googleplus-logos-02.png" width="40"/></a>
                        </div>
                    </div-->
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <!-- ./row-->
    </div>
</div>
<style>
    #home-slider{
        display:none;
    }
    .ads{
        display: none;
    }
    .vertical-menu-content{
        display: none !important;
    }
</style>