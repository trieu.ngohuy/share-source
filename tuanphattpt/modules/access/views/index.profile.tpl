<div class="container">
			<div class="row">
				<div class="category-label col-md-12 col-sm-12 col-xs-12">
					{{$name_cate}}
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12 category-col-right col-md-push-9">
					<div class="recommend-box col-md-12 col-sm-12">
						<div class="recommend-label col-md-12 col-sm-12">
							{{l}}GỢI Ý CHO BẠN{{/l}}
						</div>
						<div class="recommend-content col-md-12 col-sm-12">
						
							{{foreach from=$allFlasher item=item key=key}}
								<div class="hot-news-item col-md-12 col-sm-6">
									<img src="{{$BASE_URL}}{{$item.main_image}}">
									<br class="cb">
									<a href="{{$item.url}}">{{$item.intro_text}}</a>
								</div>
							{{/foreach}}
							
						</div>
					</div>
				</div>
				<div class="category-col-left product-list col-md-9 col-sm-12 col-xs-12 col-md-pull-3">
					<div class="page-title text-center">
                        	<h1>{{l}}Thông Tin Tài Khoản{{/l}}</h1>
                        </div>
		                        
		               {{if $erro|@count > 0}}
		                        <div class="alert {{if $erro.erro == 1}}alert-danger{{else}}alert-success {{/if}} col-sm-12">
		                        	<button class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>
										{{$erro.mess}}.
								</div>
	                        {{/if}}
                        <form class="form-horizontal" data-toggle="validator" role="form"  method="post" action="" enctype="multipart/form-data">
	                        
	
	                        <div class="">
	                        	<p class="text-right col-sm-5">{{l}}Full Name{{/l}}</p>
	                            <div class="form-group text-left col-sm-4">
									<input class="form-control" type="text"  placeholder="Nhập tên đầy đủ" name="data[full_name]" value="{{$loggedUser.full_name}}"  >
								</div>
								<br class="cb">
								<p class="text-right col-sm-5">{{l}}Email{{/l}}</p>
	                            <div class="form-group text-left col-sm-4">
									<input class="form-control" type="text"  placeholder="Nhập email" name="data[email]" value="{{$loggedUser.email}}" >
								</div>
								<br class="cb">
								<p class="text-right col-sm-5">{{l}}Address{{/l}}</p>
	                            <div class="form-group text-left col-sm-4">
									<input class="form-control" type="text"  placeholder="Nhập địa chỉ" name="data[address]" value="{{$loggedUser.address}}" >
								</div>
								<br class="cb">
								<p class="text-right col-sm-5">{{l}}Phone{{/l}}</p>
	                            <div class="form-group text-left col-sm-4">
									<input class="form-control" type="text"  placeholder="Nhập số điện thoại" name="data[phone]" value="{{$loggedUser.phone}}" >
								</div>
								<br class="cb">
								<p class="text-right col-sm-5">{{l}}Mật Khẩu Mới{{/l}}</p>
	                            <div class="form-group text-left col-sm-4">
									<input class="form-control" type="password"  placeholder="Nhập mật khẩu mới" name="data[password]" >
								</div>
								<br class="cb">
								<p class="text-right col-sm-5">{{l}}Nhập Lại Mật Khẩu{{/l}}</p>
	                            <div class="form-group text-left col-sm-4">
									<input class="form-control" type="password"  placeholder="Nhập lại mật khẩu" name="data[repeat_password]" >
								</div>
								<br class="cb">
	                        </div>
	                        <div class="text-center">
	                        
	                            <button class="btn btn-danger btn-md" type="submit">{{l}}Cập Nhập{{/l}}</button>
	                            
	                        </div>
	                        
	                        
                        </form>				
					
				</div>
				
			</div>
		</div>




