<br /><br />
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Chọn loại user!!!</div>
                <div class="panel-body">
                    <p>{{l}}Bạn muốn tham gia với vai trò: {{/l}}</p>
                    <hr>
                    <div class="row">
                        <div class="col-md-4" style="text-align: center;">
                            <a href="{{$path}}/access/index/register/type/customer" class="btn btn-primary">{{l}}Người dùng{{/l}}</a>
                        </div>
                        <div class="col-md-4" style="text-align: center;">
                            <a href="{{$path}}/access/index/register/type/step1" class="btn btn-success">{{l}}Bán hàng online{{/l}}</a>
                        </div>
                        <div class="col-md-4" style="text-align: center;">
                            <a href="{{$path}}/access/index/register/type/seller" class="btn btn-info">{{l}}Mở cửa hàng{{/l}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>		
        <div class="col-md-3"></div>
    </div>
</div>