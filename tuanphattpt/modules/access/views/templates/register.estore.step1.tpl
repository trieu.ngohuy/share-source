<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}HOME{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{l}}REGISTER{{/l}}</span>
        </div>
        <!-- ./breadcrumb -->
        <div class="panel panel-default">
            <div class="panel-heading">{{l}}Mở cửa hàng{{/l}}</div>
            <div class="panel-body">
                <h4>{{l}}Chọn template của riêng bạn: {{/l}}</h4>
                <hr>
                <form class="form-horizontal" data-toggle="validator" role="form"  method="post" action="{{$BASE_URL}}access/index/register/type/step2">
                    <div class="row">
                        {{foreach from=$templates item=item key=key}}
                        <div class="col-xs-6 col-sm3 col-md-3 col-lg-3">
                            <img src="{{$path}}{{$item.image}}" />
                            <hr>
                            <div>
                                <label>
                                <input type="radio" value="{{$item.templates_id}}" name="style" class="style" {{if $key == 0}}checked=""{{/if}}>
                                {{$item.name}}
                            </label>
                            <a href="#"><img src="https://cdn2.iconfinder.com/data/icons/circle-icons-1/64/eye-128.png" width="20"/></a>
                            <img src="https://cdn1.iconfinder.com/data/icons/basic-ui-elements-color/700/09_search-128.png" width="20" />
                            </div>
                        </div>
                        {{/foreach}}
                    </div><br>
                    <div class="al-c">
                        <button type="submit" class="btn btn-primary open-store">Mở shop ngay</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- ./row-->
    </div>
</div>