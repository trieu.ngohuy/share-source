<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}HOME{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{l}}REGISTER{{/l}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12 step-2" id="center_column">

                <div class="row step-2-form">
                    <div class="col-sm-12">                        
                        <h2>ĐĂNG KÝ GIAN HÀNG MIỄN PHÍ</h2>
                        <hr>
                    </div>
                    <form class="form-horizontal" data-toggle="validator" role="form"  method="post" action="{{$BASE_URL}}access/index/register/type/{{$type}}/style/{{$style}}">
                        <div class="col-sm-7">
                            <div class="box-authentication">                            
                                {{if $registerMessage|@count > 0}}
                                <div class="alert alert-danger col-sm-12">
                                    <button class="close" data-dismiss="alert">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    {{$registerMessage.message}}.
                                    <br style="cb">
                                </div>

                                {{/if}}
                                {{if $errors|@count > 0}}
                                <div class="alert alert-danger col-sm-12">
                                    <button class="close" data-dismiss="alert">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    {{foreach from=$errors item=item key=key}}
                                    -{{$item}}.<br>
                                    {{/foreach}}
                                    <br style="cb">

                                </div>
                                <br style="cb">
                                {{/if}}


                                <div class="">
                                    <label class="text-right col-sm-5">{{l}}Tên Đăng Nhập{{/l}}</label>
                                    <input class="form-control" type="text" placeholder="{{l}}Nhập tên đăng nhập{{/l}}" value="{{$data.username}}" name="username" required>
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Mật Khẩu{{/l}}</label>
                                    <input class="form-control" type="password" minlength="6" placeholder="{{l}}Nhập mật khẩu{{/l}}" name="password" required>
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Nhập lại Mật Khẩu{{/l}}</label>
                                    <input class="form-control" type="password" minlength="6" placeholder="{{l}}Nhập lại mật khẩu{{/l}}" name="re-password" required>
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Email{{/l}}</label>
                                    <input class="form-control" type="text" placeholder="{{l}}Nhập email{{/l}}" value="{{$data.email}}" name="email" data-error="{{l}}Bruh, that email address is invalid{{/l}}" required>
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Phone{{/l}}</label>
                                    <input class="form-control" type="text" placeholder="{{l}}Nhập phone{{/l}}" value="{{$data.phone}}" name="phone" required>
                                    
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Tên{{/l}}</label>
                                    <input class="form-control" type="text" placeholder="{{l}}Tên{{/l}}" value="{{$data.first_name}}" name="first_name" required>
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Họ và tên lót{{/l}}</label>
                                    <input class="form-control" type="text" placeholder="{{l}}Họ và tên lót{{/l}}" value="{{$data.last_name}}" name="last_name" required>
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Website{{/l}}</label>
                                    <input class="form-control" type="text" placeholder="{{l}}Website{{/l}}" value="{{$data.website}}" name="website" required>                                    
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Tên công ty{{/l}}</label>
                                    <input class="form-control" type="text" placeholder="{{l}}Tên công ty{{/l}}" value="{{$data.name_company}}" name="name_company" required>
                                    <br class="cb">
                                    <div class="col-sm-12 mar-t-20">
                                        <label class="text-right col-sm-5">{{l}}Capcha{{/l}}</label>
                                        <input class="form-control" type="text"  name="captcha" placeholder="{{l}}Vui lòng nhập mã capcha bên dưới{{/l}}" required data-fv-notempty-message="{{l}}The captcha{{/l}}">
                                        <br class="cb">
                                        <label class="text-right col-sm-5"></label>
                                        <div class="form-group text-left col-sm-6">
                                            <img src="{{$LAYOUT_HELPER_URL}}front/captcha/create_image.php?r={{$randomNumber}}" style="padding-top:4px;" />
                                        </div>
                                    </div>
                                    <br class="cb">
                                </div>
                                <button class="button" type="submit"><i class="fa fa-lock"></i> {{l}}Đăng Ký{{/l}}</button>	

                            </div>
                        </div>
                        <div class="col-sm-5">
                            <h3>{{l}}Chính sách đối với thành viên{{/l}}</h3>
                            <div class="box-authentication  policy-div" >
                                {{$config.policy}}
                            </div>
                            <div class="col-sm-12">
                                <label><input type="checkbox" value="yes" name="policy" required="">{{l}}Tôi đã đọc và đồng ý với những quy định trên{{/l}}</label>
                            </div>
                        </div>
                    </form>	
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>

    </div>
</div>