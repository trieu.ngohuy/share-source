<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}HOME{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{l}}REGISTER{{/l}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <div class="row">                 
                    <form class="form-horizontal" data-toggle="validator" role="form"  method="post" action="{{$BASE_URL}}access/index/register/type/customer">
                        <div class="col-sm-7">
                            <div class="box-authentication">
                                <h2>{{l}}Đăng ký người mua hàng{{/l}}</h2>
                                <br>
                                {{if $registerMessage|@count > 0}}
                                <div class="alert alert-danger col-sm-12">
                                    <button class="close" data-dismiss="alert">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    {{$registerMessage.message}}.
                                    <br style="cb">
                                </div>

                                {{/if}}
                                {{if $errors|@count > 0}}
                                <div class="alert alert-danger col-sm-12">
                                    <button class="close" data-dismiss="alert">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    {{foreach from=$errors item=item key=key}}
                                    -{{$item}}.<br>
                                    {{/foreach}}
                                    <br style="cb">

                                </div>
                                <br style="cb">
                                {{/if}}


                                <div class="">
                                    <label class="text-right col-sm-5">{{l}}Tên{{/l}}</label>
                                    <input class="form-control" type="text" placeholder="{{l}}Tên{{/l}}" value="{{$data.first_name}}" name="first_name" required>
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Họ và tên lót{{/l}}</label>
                                    <input class="form-control" type="text" placeholder="{{l}}Họ và tên lót{{/l}}" value="{{$data.last_name}}" name="last_name" required>
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Tên Đăng Nhập{{/l}}</label>
                                    <input class="form-control" type="text" placeholder="{{l}}Nhập tên đăng nhập{{/l}}" value="{{$data.username}}" name="username" required>
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Mật Khẩu{{/l}}</label>
                                    <input class="form-control" type="password" placeholder="{{l}}Nhập mật khẩu{{/l}}" name="password" minlength="6" required>
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Nhập lại Mật Khẩu{{/l}}</label>
                                    <input class="form-control" type="password" placeholder="{{l}}Nhập lại mật khẩu{{/l}}" name="re-password" minlength="6" required>
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Email{{/l}}</label>
                                    <input class="form-control" type="text" placeholder="{{l}}Nhập email{{/l}}" value="{{$data.email}}" name="email" data-error="{{l}}Bruh, that email address is invalid{{/l}}" required>
                                    <br class="cb">
                                    <label class="text-right col-sm-5">{{l}}Phone{{/l}}</label>
                                    <input class="form-control" type="text" placeholder="{{l}}Nhập phone{{/l}}" value="{{$data.phone}}" name="phone" required>
                                    <br class="cb">
                                    <!--div class="col-sm-12">
                                        <label class="text-right col-sm-5">{{l}}Tỉnh / Thành{{/l}}</label>
                                        <div class="row col-sm-7">
                                            <select class="input-large form-control" name="city">             
                                                <option value="--" {{if $data.city =="--"}}selected="selected"{{/if}}>--</option>
                                                {{foreach from=$places.provinces item=item}}
                                                <option value="{{$item}}" {{if $data.city ==$item}}selected="selected"{{/if}}>{{$item}}</option>
                                                {{/foreach}}
                                            </select>
                                        </div>
                                    </div-->
                                    <!--div class="col-sm-12 mar-t-20">
                                        <label class="text-right col-sm-5">{{l}}Quốc gia{{/l}}</label>
                                        <div class="row col-sm-7">
                                            <select class="input-large form-control" name="country">
                                                <option value="--" {{if $data.country =="--"}}selected="selected"{{/if}}>--</option>
                                                {{foreach from=$places.countries item=item}}
                                                <option value="{{$item}}" {{if $data.country ==$item}}selected="selected"{{/if}}>{{$item}}</option>
                                                {{/foreach}}
                                            </select>
                                        </div>
                                    </div-->
                                    <br class="cb">
                                    <div class="col-sm-12 mar-t-20">
                                        <label class="text-right col-sm-5">{{l}}Capcha{{/l}}</label>
                                        <input class="form-control" type="text"  name="captcha" placeholder="{{l}}Vui lòng nhập mã capcha bên dưới{{/l}}" required data-fv-notempty-message="{{l}}The captcha{{/l}}">
                                        <br class="cb">
                                        <label class="text-right col-sm-5"></label>
                                        <div class="form-group text-left col-sm-6">
                                            <img src="{{$LAYOUT_HELPER_URL}}front/captcha/create_image.php?r={{$randomNumber}}" style="padding-top:4px;" />
                                        </div>
                                    </div>
                                    <br class="cb">

                                </div>
                                <button class="button" type="submit"><i class="fa fa-lock"></i> {{l}}Đăng Ký{{/l}}</button>	

                            </div>
                        </div>
                        <div class="col-sm-5">
                            <h3>{{l}}Chính sách đối với thành viên{{/l}}</h3>
                            <div class="box-authentication  policy-div" >
                                {{$config.policy}}
                            </div>
                            <div class="col-sm-12">
                                <label><input type="checkbox" value="yes" name="policy" required="">{{l}}Tôi đã đọc và đồng ý với những quy định trên{{/l}}</label>
                            </div>
                        </div>
                    </form>	
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>