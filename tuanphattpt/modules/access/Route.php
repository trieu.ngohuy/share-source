<?php

class Route_Access {

    /**
     * Build friendly URL
     * @param array $linkArr 0 => <module>, 1 => controller, 2 => action, 3...n param/value
     * @param array $params param => value
     * 
     * @return string friendly URL
     */
    public function build($linkArr, $params = array()) {

        $result = implode('/', $linkArr);

        /**
         * Link is detail
         */
        if ('logout' == @$linkArr[2]) {
            /**
             * Structure: pages/<id>/<alias>.html
             */
            $result = "user/";
            if (null != $params['alias']) {
                $result .= urlencode($params['alias']);
            }
        }
        if ('register' == @$linkArr[2]) {
            /**
             * Structure: pages/<id>/<alias>.html
             */
            $result = "user/";
            if (null != $params['alias']) {
                $result .= urlencode($params['alias']);
            }
        }
        return $result;
    }

    /**
     * Parse friendly URL
     */
    public function parse() {
        $router = Nine_Controller_Front::getInstance()->getRouter();


        $route1 = new Zend_Controller_Router_Route_Regex(
                'access/login', array(
            'module' => 'access',
            'controller' => 'index',
            'action' => 'login'
                )
        );
        $router->addRoute('access1', $route1);

        $route2 = new Zend_Controller_Router_Route_Regex(
                'access/logout', array(
            'module' => 'access',
            'controller' => 'index',
            'action' => 'logout'
                )
        );
        $router->addRoute('access2', $route2);
        $route3 = new Zend_Controller_Router_Route_Regex(
                'register.html', array(
            'module' => 'access',
            'controller' => 'index',
            'action' => 'register'
                )
        );
        $router->addRoute('access3', $route3);
    }

}
