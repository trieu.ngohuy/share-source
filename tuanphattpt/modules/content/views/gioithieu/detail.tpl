<div class="container">
			<div class="row">
				<div class="news-label col-md-12 col-sm-12">
					{{$detail.title}}
				</div>
				<div class="col-md-9 col-sm-12 col-xs-12">
					<div class="news-detail col-md-12 col-sm-12 col-xs-12">
						
						{{$detail.full_text}}
					</div>
				</div>
				
				
				<div class="hot-news col-md-3 col-sm-12 col-xs-12">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="hot-news-label col-md-12 col-sm-12">
							{{l}}TIN TỨC NỔI BẬT{{/l}}
						</div>
						<div class="hot-news-content col-md-12 col-sm-12">
							{{foreach from=$allFlasher item=item key=key}}
								<div class="hot-news-item col-md-12 col-sm-6">
									<img src="{{$BASE_URL}}{{$item.main_image}}">
									<br class="cb">
									<a href="{{$item.url}}">{{$item.title}}</a>
								</div>
							{{/foreach}}
						</div>
					</div>
				</div>
				
				
			</div>
		</div>