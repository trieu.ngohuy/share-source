<div class="page-header">
    <div class="container">
        <h1>Chứng chỉ</h1>
    </div>
</div>
<div class="container zoom-gallery">
    <div class="row">
        {{foreach from=$objDetail.arrImages item=item}}
        <a class="col-md-3" href="{{$item}}">
            <img src="{{$item}}">
        </a>
        {{/foreach}}
    </div>
</div>