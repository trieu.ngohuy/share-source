<div class="page-header">
    <div class="container">
        <h1>{{$objDetail.title}}</h1>
    </div>
</div>
<div class="container small-content">
    <div class="row justify-content-lg-center">
        <div class="content col-lg-8">
            {{$objDetail.full_text}}
        </div>
    </div>
</div>