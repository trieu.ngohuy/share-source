<div class="page-header">
    <div class="container">
        <h1>{{$objCat.name}}</h1>
    </div>
</div>
<div class="container">
    <div class="row justify-content-lg-center">
        <div class="col-lg-12">
            {{foreach from=$arrContents item=item}}
            <article class="mx-2 py-3 post-3082 post type-post status-publish format-standard has-post-thumbnail hentry category-kien-thuc-chieu-sang">
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-lg-4"> 
                            <a href="{{$item.url}}" class="featured-image pb-2 pr-lg-3 pb-lg-0 text-center" style="display:block;"> 
                                <img src="{{$item.main_image}}" alt="{{$item.title}}"> 
                            </a>
                        </div>
                        <div
                            class="col-lg-8">
                            <header>
                                <h2 class="entry-title h5">
                                    <a href="{{$item.url}}">{{$item.title}}</a>
                                </h2>
                                <div class="metadata mb-2">
                                    <time class="updated"><img src="https://image.flaticon.com/icons/svg/1142/1142015.svg" width="15" style="margin-top: -5px"/> {{$item.cat}}</time> | 
                                    <time class="updated"><img src="https://www.flaticon.com/premium-icon/icons/svg/640/640224.svg" width="15" style="margin-top: -5px" /> {{$item.created_date}}</time>
                                </div>
                            </header>
                            <div class="entry-summary">
                                <p>{{$item.intro_text}} <a href="{{$item.url}}">{{l}}Đọc tiếp{{/l}}</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            {{/foreach}}
        </div>
        <!--div class="col-lg-3 d-none d-lg-block">
            <section class="widget categories-2 widget_categories">
                <h3>{{l}}Chuyên mục{{/l}}</h3>
                <ul>
                    {{foreach from=$arrCats item=item}}
                    <li class="cat-item cat-item-23">
                        <a href="{{$item.url}}">{{$item.name}}</a>
                    </li>                    
                    {{/foreach}}
                </ul>
            </section>
        </div-->
    </div>
</div>
{{if $countAllPages > 1}}
<div class="text-center">
    <nav>
        <ul class="pagination justify-content-center">
            <li class="disabled page-item">
                <span class="page-link"><span aria-hidden="true">{{l}}Trang{{/l}} {{$currentPage}} / {{$countAllPages}}</span></span>
            </li>
            {{if $first}}
            <li class="page-item">
                <a class="page-link" href="?page=1" aria-label="First">
                    «<span class="hidden-xs"> {{l}}First{{/l}}</span>
                </a>
            </li>
            {{/if}}

            {{if $prevPage}}
            <li class="page-item">
                <a class="page-link" href="?page={{$prevPage}}" aria-label="Previous">
                    ‹<span class="hidden-xs"> {{l}}Previous{{/l}}</span>
                </a>
            </li>
            {{/if}}

            {{foreach from=$prevPages item=item}}
            <li class='page-item'><a class='page-link' href='?page={{$item}}'>{{$item}}</a></li>
            {{/foreach}}

            <li class="page-item active"><span class='page-link'>{{$currentPage}} <span class="sr-only">(current)</span></span>
            </li>
            
            {{foreach from=$nextPages item=item}}
            <li class='page-item'><a class='page-link' href='?page={{$item}}'>{{$item}}</a></li>
            {{/foreach}}
            
            {{if $nextPage}}
            <li class='page-item'><a class='page-link' href="?page={{$nextPage}}" aria-label='Next'><span class='hidden-xs'>{{l}}Next{{/l}} </span>&rsaquo;</a></li>
            {{/if}}
            
            {{if $last}}
            <li class='page-item'><a class='page-link' href='?page={{$countAllPages}}' aria-label='Last'><span class='hidden-xs'>{{l}}Last{{/l}} </span>&raquo;</a></li>
            {{/if}}
        </ul>
    </nav>
</div>
{{/if}}