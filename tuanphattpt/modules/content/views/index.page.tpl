<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span>{{$page.title}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- Blog category -->
                {{sticker name=front_default_category}}
                <!-- ./blog category  -->
                     {{foreach from=$adv item=item}}
					    <div class="col-xs-12 col-sm-12 col-md-12 m10t qc" style="padding: 0px">
					        <a href="{{$item.link}}"><img alt="Banner right 1" src="{{$BASE_URL}}{{$item.images}}"></a>
					    </div>
					{{/foreach}}
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <h1 class="page-heading">
                    <span class="page-heading-title2">{{$page.title}}</span>
                </h1>
                <article class="entry-detail">
                    <div class="entry-meta-data">
                        <span class="date"><i class="fa fa-calendar"></i> {{$page.created_date|date_format:"%d-%m-%Y %H:%M:%S"}}</span>
                    </div>
                    <div class="content-text clearfix">
                        {{$page.full_text}}
                    </div>

                </article>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>