


<div class="page-header">
    <h1>
        {{l}}Content{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}List All Content{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">

                {{if $allContent|@count <= 0}}
                <div class="alert alert-info">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{l}}No content with above conditions.{{/l}}
                </div>
                {{/if}}

                {{if $contentMessage|@count > 0 && $contentMessage.success == true}}
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$categoryMessage.message}}.
                </div>
                {{/if}}

                {{if $contentMessage|@count > 0 && $contentMessage.success == false}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$contentMessage.message}}.
                </div>
                {{/if}}


                <table id="simple-table" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                    <form id="top-search" name="search" method="post" action="">



                        <th class="center">
                            <button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
                        </th>
                        <th class="center">
                            <input class="check-all" type="checkbox" />
                        </th>
                        <th>{{l}}Category{{/l}}</th>
                        <th>{{l}}User Upload{{/l}}
                            <input  type="text" name="condition[username]" id="username" value="{{$condition.username}}"" placeholder="{{l}}Find Username{{/l}}" class="form-control" />
                        </th>
                        <th>{{l}}Created{{/l}}</th>
                        <th class="center">
                            {{l}}Flashes{{/l}}
                        </th>
                        <th class="center">
                            {{l}}Sort{{/l}}
                            {{if $fullPermisison}}
                            <a href="javascript:document.sortForm.submit();"><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/save_16.png" alt="Save sorting"></a>
                                {{/if}}
                        </th>
                        <th colspan="5">
                            <div class="col-sm-12">
                                {{l}}Title{{/l}}
                            </div>
                            <div class="col-sm-4">
                                <input  type="text" name="condition[keyword]" id="username" value="{{$condition.keyword}}"" placeholder="{{l}}Find Title{{/l}}" class="form-control" />
                            </div>
                            <div class="col-sm-4">
                                <select class=" form-control" name="condition[content_category_gid]" onchange="this.form.submit();">
                                    <option value="">{{l}}All categories{{/l}}</option>
                                    {{foreach from=$allCats item=item}}
                                    <option value="{{$item.content_category_gid}}" {{if $condition.content_category_gid== $item.content_category_gid}}selected="selected"{{/if}}>{{$item.name}}</option>
                                    {{/foreach}}
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <select class=" form-control" name="condition[genabled]" onchange="this.form.submit();">
                                    <option value="">{{l}}All Published{{/l}}</option>
                                    <option value="1" {{if $condition.genabled == 1}}selected="selected"{{/if}}>{{l}}Published{{/l}}</option>
                                    <option value="0" {{if $condition.genabled != "" && $condition.genabled == 0}}selected="selected"{{/if}}>{{l}}No Published{{/l}}</option>
                                </select>
                            </div>
                        </th>
                    </form>
                    </tr>
                    </thead>

                    <tbody>
                        {{if $allContent|@count > 0}}
                    <form action="" method="post" name="sortForm">
                        {{foreach from=$allContent item=item name=content key=key}}
                        <tr>
                            <td class="center">{{$key+1}}</td>
                            <td class="center">{{if 1 == $item.content_deleteable}}<input type="checkbox" value="{{$item.content_gid}}" name="allContent" class="allContent"/>{{else}}<img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/unselect.png" alt="Do not delete, enable, disable" />{{/if}}</td>
                            <td  width="10%">
                                {{$item.cname}}
                                {{if $item.content_category_gid == 116}}
                                <a class="button" href="{{$APP_BASE_URL}}content/admin/manager-event/content_gid/{{$item.content_gid}}" style="margin-top: 5px; width: 100%">Manager</a>
                                <a class="button" href="{{$APP_BASE_URL}}content/admin/add-event/content_gid/{{$item.content_gid}}" style="margin-top: 5px; width: 100%">New</a>
                                <a class="button" href="{{$APP_BASE_URL}}content/admin/remove-event/content_gid/{{$item.content_gid}}" style="margin-top: 5px; width: 100%">Delete</a>
                                {{/if}}
                            </td>
                            <td>
                                {{$item.uname}}

                            </td>
                            <td>{{$item.created_date}}</td>
                            <td class="center">
                                {{if $item.tinnhanh == '1' }}
                                <a href="{{$APP_BASE_URL}}content/admin/distinnhanh-content/gid/{{$item.content_gid}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png"></a>
                                    {{else}}
                                <a href="{{$APP_BASE_URL}}content/admin/tinnhanh-content/gid/{{$item.content_gid}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png"></a>
                                    {{/if}}
                            </td>
                            <td class="center">
                                {{if $fullPermisison}}
                                <input name="data[{{$item.content_gid}}]" value="{{$item.sorting}}" size="3" style="text-align: center;"></input>
                                {{else}}
                                {{$item.sorting}}
                                {{/if}}
                            </td>
                            <td colspan="5" style="padding:0px;">
                                <!-- All languages -->
                                <table style="border-collapse: separate;margin-bottom: 0px" class="table table-bordered table-striped">
                                    <tbody id="table{{$item.content_id}}">
                                        <tr>
                                            <td width="39%">--</td>
                                            <td width="5%">{{$item.hit}}</td>
                                            <td width="5%">
                                                {{if $item.genabled == '1' }}
                                                {{if $CheckGenalbel}}
                                                <a href="{{$APP_BASE_URL}}content/admin/disable-content/gid/{{$item.content_gid}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png"></a>
                                                    {{else}}
                                                <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png">
                                                {{/if}}
                                                {{else}}
                                                {{if $CheckGenalbel}}
                                                <a href="{{$APP_BASE_URL}}content/admin/enable-content/gid/{{$item.content_gid}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png"></a>
                                                    {{else}}
                                                <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png">
                                                {{/if}}
                                                {{/if}}
                                            </td>
                                            <td width="5%"  class="center">
                                                {{if $EditPermisision}}
                                                <span class="tooltip-area">
                                                    <a href="{{$APP_BASE_URL}}content/admin/edit-content/gid/{{$item.content_gid}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                                </span>
                                                {{else}}
                                                --
                                                {{/if}}
                                            </td>
                                            <td width=5%">
                                                {{$item.content_gid}}
                                            </td>
                                        </tr>

                                        {{foreach from=$item.langs item=item2}}
                                        <tr>
                                            <td><image style="vertical-align:middle;" src="{{$BASE_URL}}{{$item2.lang_image}}"> {{$item2.title}}</td>
                                            <td>{{$item2.hit}}</td>
                                            <td>

                                                {{assign var='langId' value=$item2.lang_id}}

                                                {{if $item.genabled == '1'}}

                                                <!-- FULL PERMISSION -->
                                                {{p name=edit_content module=content expandId=$langId}}
                                                {{if $item2.enabled == '1'}}
                                                {{if  $CheckGenalbel}}
                                                <a href="{{$APP_BASE_URL}}content/admin/disable-content/gid/{{$item.content_gid}}/lid/{{$item2.lang_id}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png"></a>
                                                    {{else}}
                                                <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png">
                                                {{/if}}
                                                {{else}}
                                                {{if  $CheckGenalbel}}
                                                <a href="{{$APP_BASE_URL}}content/admin/enable-content/gid/{{$item.content_gid}}/lid/{{$item2.lang_id}}" ><img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png"></a>
                                                    {{else}}
                                                <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png">
                                                {{/if}}
                                                {{/if}}
                                                {{/p}}
                                                <!-- DON'T HAVE PERMISSION -->
                                                {{np name=edit_content module=content expandId=$langId}}
                                                {{if $item2.enabled == '1'}}
                                                <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/visible16x16.png">
                                                {{else}}
                                                <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png">
                                                {{/if}}
                                                {{/np}}
                                                {{else}}
                                                <img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/invisible16x16.png">
                                                {{/if}}
                                            </td>
                                            <td  class="center">
                                                {{p name=edit_content module=content expandId=$langId}}
                                                <span class="tooltip-area">
                                                    <a href="{{$APP_BASE_URL}}content/admin/edit-content/gid/{{$item.content_gid}}/lid/{{$item2.lang_id}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                                </span>
                                                {{/p}}
                                                {{np name=edit_content module=content expandId=$langId}}
                                                --
                                                {{/np}}
                                            </td>
                                            <td>
                                                {{$item2.content_id}}
                                            </td>
                                        </tr>
                                        {{/foreach}}

                                    </tbody>
                                </table>
                            </td>

                        </tr>
                        {{/foreach}}
                    </form>
                    {{/if}}
                    </tbody>
                </table>


            </div><!-- /.span -->
        </div><!-- /.row -->


        <div class="col-lg-12">
            <div class="form-group col-lg-4">
                <label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
                <div class="col-lg-6">
                    <select id="action" class="form-control" >
                        <option value=";">{{l}}Choose an action...{{/l}}</option>
                        {{p name=delete_content module=content}}
                        <option value="deleteContent();">{{l}}Delete{{/l}}</option>
                        {{/p}}
                        {{if $CheckGenalbel}}
                        <option value="enableContent();">{{l}}Enable{{/l}}</option>
                        <option value="disableContent();">{{l}}Disable{{/l}}</option>
                        {{/if}}
                    </select>
                </div>
                <div class="col-lg-6">
                    <a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
                </div>
            </div>
            <div class="form-group col-lg-2">
                <label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
                <div class="col-lg-12">
                    <form class="search" name="search" method="post" action="">
                        <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
                            <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
                            <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
                            <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
                            <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
                            <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
                        </select>
                    </form>
                </div>
            </div>
            {{if $countAllPages > 1}}
            <div class="col-lg-6 pagination">
                {{if $first}}
                <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
                {{/if}}
                {{if $prevPage}}
                <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
                {{/if}}

                {{foreach from=$prevPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>

                {{foreach from=$nextPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                {{if $nextPage}}
                <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
                {{/if}}
                {{if $last}}
                <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
                {{/if}}

            </div>
            {{/if}}
        </div>
    </div>


</div>
</div>

<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        $('.close').click(function () {
            $(this).parent().hide("slow");
        });
        $('.check-all').click(function () {
            if (this.checked) { // check select status
                $('.allContent').each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.allContent').each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });
    });

    function applySelected()
    {
        var task = document.getElementById('action').value;
        eval(task);
    }
    function enableContent()
    {
        var all = document.getElementsByName('allContent');
        var tmp = '';
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
            }
        }
        if ('' == tmp) {
            alert('Please choose an content');
        }
        window.location.href = '{{$APP_BASE_URL}}content/admin/enable-content/gid/' + tmp;
    }

    function disableContent()
    {
        var all = document.getElementsByName('allContent');
        var tmp = '';
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
            }
        }
        if ('' == tmp) {
            alert('Please choose an content');
        }
        window.location.href = '{{$APP_BASE_URL}}content/admin/disable-content/gid/' + tmp;
    }

    function deleteContent()
    {
        var all = document.getElementsByName('allContent');
        var tmp = '';
        var count = 0;
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
                count++;
            }
        }
        if ('' == tmp) {
            alert('Please choose an content');
            return;
        } else {
            result = confirm('Are you sure you want to delete ' + count + ' content(s)?');
            if (false == result) {
                return;
            }
        }
        window.location.href = '{{$APP_BASE_URL}}content/admin/delete-content/gid/' + tmp;
    }


    function deleteAContent(id)
    {
        result = confirm('Are you sure you want to delete this content?');
        if (false == result) {
            return;
        }
        window.location.href = '{{$APP_BASE_URL}}content/admin/delete-content/gid/' + id;
    }
</script>