<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}HOME{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{$detail.title}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
            
            	<div class="block left-module">
                    <p class="title_block">{{l}}Tin Nổi Bật{{/l}}</p>
                    <div class="block_content">
                        <ul class="products-block">
                        	{{foreach from=$allFlasher item=item key=key}}
								<li style="padding: 10px 0px;">
	                                <div class="products-block-left" style="float: left;">
	                                    <a href="{{$item.url}}">
	                                        <img src="{{$BASE_URL}}{{$item.main_image}}" alt="{{$item.title}}">
	                                    </a>
	                                </div>
	                                <div class="products-block-right"  style="min-height: 45px;">
	                                    <p class="product-name">
	                                        <a href="{{$item.url}}">{{$item.title}}</a>
	                                    </p>
	                                </div>
	                            </li>
							{{/foreach}}
                            
                        </ul>
                    </div>
                </div>
                
                <div class="col-left-slide left-module">
                    <ul class="owl-carousel owl-style2" data-loop="true" data-nav = "false" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-items="1" data-autoplay="true">
                        <li><a href="#"><img src="{{$LAYOUT_HELPER_URL}}front/assets/data/slide-left.jpg" alt="slide-left"></a></li>
                        <li><a href="#"><img src="{{$LAYOUT_HELPER_URL}}front/assets/data/slide-left2.jpg" alt="slide-left"></a></li>
                        <li><a href="#"><img src="{{$LAYOUT_HELPER_URL}}front/assets/data/slide-left3.png" alt="slide-left"></a></li>
                    </ul>

                </div>
                
                
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- view-product-list-->
                <h1 class="page-heading">
                    <span class="page-heading-title2">{{$detail.title}}</span>
                </h1>
				<article class="entry-detail">
						<div class="content-text clearfix">
							{{$detail.full_text}}
							
							<p style="text-transform:uppercase ;font-weight: bold;font-size: 14px;color: #000;text-align: right;width: 100%;float: left;">{{l}}Nguồn{{/l}} : {{$detail.nguon}}</p>
						</div>
				</article>
				<div class="single-box">
                    <h2>{{l}}Tin cùng chuyên mục{{/l}}</h2>
                    <ul class="related-posts owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":2},"1000":{"items":3}}'>
	               		{{foreach from=$tinlienquan item=item key=key}}
		               		<li class="post-item">
	                            <article class="entry">
	                                <div class="entry-thumb image-hover2">
	                                    <a href="{{$item.url}}">
	                                        <img src="{{$BASE_URL}}{{$item.main_image}}" alt="Blog">
	                                    </a>
	                                </div>
	                                <div class="entry-ci">
	                                    <h3 class="entry-title"><a href="{{$item.url}}">{{$item.title}}</a></h3>
	                                </div>
	                            </article>
	                        </li>
                        {{/foreach}}
	               	</ul>
                </div>
					
                
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>