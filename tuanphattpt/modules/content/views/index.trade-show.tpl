<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{$BASE_URL}}" title="Return to Home">{{l}}Home{{/l}}</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">{{l}}Trade Show{{/l}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- Category -->
                {{sticker name=front_default_category}}
                <!-- /Category -->

                <!-- Popular Posts -->
                {{sticker name=front_link_website}}
                <!-- ./Popular Posts -->

                <!-- Banner -->
                {{foreach from=$adv item=item}}
					    <div class="col-xs-12 col-sm-12 col-md-12 m10t qc" style="padding: 0px">
					        <a href="{{$item.link}}"><img alt="Banner right 1" src="{{$BASE_URL}}{{$item.images}}"></a>
					    </div>
					{{/foreach}}
                <!-- ./Banner -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <h2 class="page-heading">
                    <span class="page-heading-title2">{{l}}Trade Show{{/l}}</span>
                </h2>
                <div class="sortPagiBar clearfix">
                    <span class="page-noite">Showing {{$firstItemInPage}} to {{$lastItemInPage}} of {{$allItems}} ({{$countAllPages}} {{l}}Pages{{/l}})</span>
                    {{if $countAllPages > 1}}
                    <div class="bottom-pagination">
                        <nav>
                            <ul class="pagination">
                                {{if $prevPage}}
                                <li>
                                    <a href="?page={{$prevPage}}" aria-label="Prev">
                                        <span aria-hidden="true">&laquo; Prev</span>
                                    </a>
                                </li>
                                {{/if}}

                                {{foreach from=$prevPages item=item}}
                                <li><a href="?page={{$item}}">{{$item}}</a></li>
                                {{/foreach}}

                                <li class="active"><a>{{$currentPage}}</a></li>

                                {{foreach from=$nextPages item=item}}
                                <li><a href="?page={{$item}}"">{{$item}}</a></li>
                                {{/foreach}}

                                {{if $nextPage}}
                                <li>
                                    <a href="?page={{$nextPage}}" aria-label="Next">
                                        <span aria-hidden="true">Next &raquo;</span>
                                    </a>
                                </li>
                                {{/if}}
                            </ul>
                        </nav>
                    </div>
                    {{/if}}
                </div>
                <ul class="blog-posts">
                    {{foreach from=$allTradeShow item=item}}
                    <li class="post-item">
                        <article class="entry">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="entry-thumb image-hover2">
                                        <a href="{{$BASE_URL}}trade-show/{{$item.alias}}">
                                            <img src="{{$BASE_URL}}{{$item.images}}" alt="Blog">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="entry-ci">
                                        <h3 class="entry-title"><a href="{{$BASE_URL}}trade-show/{{$item.alias}}">{{$item.title}}</a></h3>
<!--                                        <div class="entry-meta-data">-->
<!--                                            <span class="date"><i class="fa fa-calendar"></i> {{$item.created_date|date_format:"%d-%m-%Y %H:%M:%S"}}</span>-->
<!--                                        </div>-->
                                        <div class="entry-excerpt">
                                            <p style="margin: 10px 0px; padding: 0px; color: rgb(102, 102, 102); font-family: Tahoma, Arial, sans-serif; font-size: 12.8px; line-height: 19.2px; text-align: justify;">
												<strong>{{l}}Thời gian{{/l}}:</strong>&nbsp;<span style="color: rgb(0, 128, 128);"><strong>{{$item.time_start}}</strong></span><br>
												<strong>{{l}}Địa điểm{{/l}}:</strong>&nbsp;{{$item.add}}
											</p>
                                        </div>
                                        <div class="entry-more">
                                            <a href="{{$BASE_URL}}trade-show/{{$item.alias}}">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </li>
                    {{/foreach}}
                </ul>
                <div class="sortPagiBar clearfix">
                    {{if $countAllPages > 1}}
                    <div class="bottom-pagination">
                        <nav>
                            <ul class="pagination">
                                {{if $prevPage}}
                                <li>
                                    <a href="?page={{$prevPage}}" aria-label="Prev">
                                        <span aria-hidden="true">&laquo; Prev</span>
                                    </a>
                                </li>
                                {{/if}}

                                {{foreach from=$prevPages item=item}}
                                <li><a href="?page={{$item}}">{{$item}}</a></li>
                                {{/foreach}}

                                <li class="active"><a>{{$currentPage}}</a></li>

                                {{foreach from=$nextPages item=item}}
                                <li><a href="?page={{$item}}"">{{$item}}</a></li>
                                {{/foreach}}

                                {{if $nextPage}}
                                <li>
                                    <a href="?page={{$nextPage}}" aria-label="Next">
                                        <span aria-hidden="true">Next &raquo;</span>
                                    </a>
                                </li>
                                {{/if}}
                            </ul>
                        </nav>
                    </div>
                    {{/if}}
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>



<!---->