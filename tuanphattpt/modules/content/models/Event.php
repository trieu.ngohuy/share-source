<?php
/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2009 visualidea.org
 * @license    http://license.visualidea.org
 * @version    v 1.0 2009-04-15
 */
require_once 'Nine/Model.php';

class Models_Event extends Nine_Model
{ 
   
    public function __construct($config = array())
    {
        $this->_name = $this->_prefix . 'event';
        return parent::__construct($config); 
    }

    public function getAllEventByContentGid($gid , $condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('cc' => $this->_prefix . 'content_category'), 'c.content_category_gid = cc.content_category_gid', array('cname' => 'name','content_deleteable' => 'content_deleteable'))
                ->join(array('l' => $this->_prefix . 'lang'), 'c.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->where('c.content_gid = ?', $gid)
                ->order($order)
                ->limit($count, $offset);
       
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllEnabledContent( $gid, $order = null, $count = null, $offset = null)
    {
    	
    	
    	$select = $this->select()
                ->setIntegrityCheck(false)
                ->where('genabled = 1')
                ->order($order)
                ->limit($count, $offset);
         
        $select->where('content_gid_event = ?', $gid);
        
         /**
          * Condition
          */       
               
    	return $this->fetchAll($select)->toArray();
    }
	
}