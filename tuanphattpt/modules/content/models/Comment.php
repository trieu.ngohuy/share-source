<?php
/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2009 visualidea.org
 * @license    http://license.visualidea.org
 * @version    v 1.0 2009-04-15
 */
require_once 'Nine/Model.php';
require_once 'modules/content/models/ContentCategory.php';
require_once 'modules/content/models/Content.php';

class Models_Comment extends Nine_Model
{ 
    /**
     * The primary key column or columns.
     * A compound key should be declared as an array.
     * You may declare a single-column primary key
     * as a string.
     *
     * @var mixed
     */
    protected $_primary = 'comment_id';
    /**
     * The field name what we use to group all language rows together
     * 
     * @var string
     */
   
    public function __construct($config = array())
    {
        $this->_name = $this->_prefix . 'comment';
        return parent::__construct($config); 
    }

    /**
     * Get all content with conditions
     * 
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
//    public function getAllContentsWithDefaultLang($condition = array(), $order = null, $count = null, $offset = null)
//    {
//        $select = $this->select()
//                ->setIntegrityCheck(false)
//                ->from(array('s' => $this->_name), array('content_id', 'senabled' => 'enabled', 'publish_up_date' => 'publish_up_date', 'publish_down_date' => 'publish_down_date' , 'ssorting' => 'sorting', 'created_date' => 'created_date'))
//                ->join(array('sl' => $this->_prefix . 'content_lang'), 's.content_id = sl.content_id')
//                ->join(array('sc' => $this->_prefix . 'content_category'), 's.content_category_gid = sc.content_category_gid', array('cname' => 'name'))
//                ->order($order)
//                ->limit($count, $offset);
//        /**
//         * Conditions
//         */
//        if (null != @$condition['keyword']) {
//            $select->where($this->getAdapter()->quoteInto('sl.title LIKE ?', "%{$condition['keyword']}%"));
//        }
//        if (null != @$condition['content_category_gid']) {
//            $select->where("s.content_category_gid = ?", $condition['content_category_gid']);
//        }
//        if (null != @$condition['lang_id']) {
//            $select->where("sl.lang_id = ?", $condition['lang_id']);
//        }
//        
//        return $this->fetchAll($select)->toArray();
//    }

    /**
     * Get all content with conditions
     * 
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
    public function getAllComment($gid, $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','image' =>'image'))
                ->where('c.content_gid = ?', $gid)
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllCommentAdmin($condition , $order = null, $count = null, $offset = null )
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => $this->_name))
                ->join(array('u' => $this->_prefix . 'user'), 'c.user_id = u.user_id', array('uname' => 'username','image' =>'image'))
                ->where('c.genable = ?', $condition['genable'])
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
        
        return $this->fetchAll($select)->toArray();
    }
}