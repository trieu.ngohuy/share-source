<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/content/models/Content.php';
require_once 'modules/content/models/ContentCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/user/models/User.php';
require_once 'modules/mail/models/Mail.php';
require_once 'modules/mail/models/MailStore.php';
require_once 'modules/message/models/MessageCategory.php';

class content_EstoreController extends Nine_Controller_Action {

    protected $_content_category_gid_news = 1;
    protected $_content_category_gid_promotion = 9;

    /*     * ************************************************************************************************
     *                                         NEWS's FUNCTIONS
     * ************************************************************************************************ */

    public function manageNewsAction() {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        $this->setLayout('estore');

        $objContent = new Models_Content();
        $objLang = new Models_Lang();
        $objCat = new Models_ContentCategory();

        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_content', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $this->view->headTitle(Nine_Language::translate('manage Content '));


        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page", false);
        $displayNum = $this->_getParam('displayNum', false);
        if ($currentPage == false) {
            $currentPage = 1;
            $this->session->contentDisplayNum = null;
            $this->session->contentCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id => $value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objContent->update(array('sorting' => $value), array('content_gid=?' => $id));
            $this->session->contentMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("News Order is edited successfully")
            );
        }

        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->contentDisplayNum;
        } else {
            $this->session->contentDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->contentCondition;
        } else {
            $this->session->contentCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        if (@$condition['keyword'] != '') {
            $condition['keyword'] = $objContent->convert_vi_to_en($condition['keyword']);
            $condition['keyword'] = preg_replace("/( )/", '-', $condition['keyword']);
        }

        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_content', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        /**
         * Condition by User Id
         */
        $condition['user_id'] = $user['user_id'];



        /**
         * Get all contents
         */
        $numRowPerPage = $numRowPerPage;
        $allContent = $objContent->getAllContent($condition, array('sorting ASC', 'content_gid DESC', 'content_id ASC'), $numRowPerPage, ($currentPage - 1) * $numRowPerPage
        );
//        echo '<pre>';print_r($allContent);die;
        /**
         * Count all contents
         */
        $count = count($objContent->getallContent($condition));
        /**
         * Format
         */
        $tmp = array();
        $tmp2 = false;
        $tmpGid = @$allContent[0]['content_gid'];
        foreach ($allContent as $index => $content) {
            /**
             * Change date
             */
            if (0 != $content['created_date']) {
                $content['created_date'] = date($config['dateFormat'], $content['created_date']);
            } else {
                $content['created_date'] = '';
            }
            if (0 != $content['publish_up_date']) {
                $content['publish_up_date'] = date($config['dateFormat'], $content['publish_up_date']);
            } else {
                $content['publish_up_date'] = '';
            }
            if (0 != $content['publish_down_date']) {
                $content['publish_down_date'] = date($config['dateFormat'], $content['publish_down_date']);
            } else {
                $content['publish_down_date'] = '';
            }
            if ($tmpGid != $content['content_gid']) {
                $tmp[] = $tmp2;
                $tmp2 = false;
                $tmpGid = $content['content_gid'];
            }
            if (false === $tmp2) {
                $tmp2 = $content;
                $tmp2['hit'] = 0;
            }
            $tmp2['hit'] += $content['hit'];
            $tmp2['langs'][] = $content;
            /**
             * Final element
             */
            if ($index == count($allContent) - 1) {
                $tmp[] = $tmp2;
            }
        }
        $allContent = $tmp;
        $export = $this->_getParam('export', false);
        if ($export != false) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $style = "style = 'border: 1px solid'";

            if (Nine_Language::getCurrentLangId() == 1) {

                $textHeader = array(
                    "<b>No</b>",
                    "<b>Category Content</b>",
                    "<b>Estore Upload</b>",
                    "<b>Created Date</b>",
                    "<b>Title</b>"
                );
            } else {

                $textHeader = array(
                    mb_convert_encoding("<b>S? Th? T?</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Danh M?c B�i Vi?t</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Gian H�ng Upload</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Ng�y Kh?i T?o</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Ti�u �?</b>", 'HTML-ENTITIES', 'UTF-8')
                );
            }

            $header = "<tr>";
            foreach ($textHeader as $text) {
                $header .= "<td $style>" . utf8_encode($text) . "</td>";
            }
            $header .= "</tr>";

            $content = '';
            $no = 1;

            foreach ($allContent as $item) {
                $content .= "<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['cname'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['uname'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['created_date'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['title'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "</tr>";
                $no++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-content-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();
        }
        /**
         * Set values for tempalte
         */
//        echo print_r($condition); die;
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allContent = $allContent;
        $this->view->contentMessage = $this->session->contentMessage;
        $this->session->contentMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_content');
        $this->view->EditPermisision = $this->checkPermission('edit_content');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_content');
        $this->view->DeletePermisision = $this->checkPermission('delete_content');
        $this->view->allLangs = $allLangs;

        $this->view->menu = array(
            0 => 'news',
            1 => 'manager-news'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-newspaper-o',
                'url' => Nine_Registry::getBaseUrl() . 'estore/manage-news/' . $user['alias'] . '.html',
                'name' => Nine_Language::translate('Manager News')
            )
        );

        $this->view->alias = $user['alias'] . '.html';
        $this->view->username = $user['alias'];
    }

    public function newNewsAction() {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        $this->setLayout('estore');

        $objCat = new Models_ContentCategory();
        $objLang = new Models_Lang();
        $objContent = new Models_Content();
        $id = $this->_getParam('id', false);

        if ($id != false) {
            $id = explode("_", $id);
            if (count($id) != 2) {
                $this->_redirect('estore/manage-news/' . $user['alias'] . '.html');
            }
        }
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_content', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data = $this->_getParam('data', false);

        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();

        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_content', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new content
             */
            $newContent = $data;
            $newContent['created_date'] = time();
            if (null == $newContent['sorting']) {
                unset($newContent['sorting']);
            }
            if (false == $this->checkPermission('new_content', null, '*')) {
                $newContent['genabled'] = 0;
                $newContent['publish_up_date'] = null;
                $newContent['publish_down_date'] = null;
                $newContent['sorting'] = 1;
            }

            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newContent[$lang['lang_id']]['intro_text'], $newContent[$lang['lang_id']]['full_text']) = Nine_Function::splitTextWithReadmore($newContent[$lang['lang_id']]['full_text']);
                if ($newContent[$lang['lang_id']]['alias'] == "") {
                    $newContent[$lang['lang_id']]['alias'] = $objContent->convert_vi_to_en($newContent[$lang['lang_id']]['title']);
                    $newContent[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", $newContent[$lang['lang_id']]['alias']));
                }
            }

            /**
             * Remove empty images
             */
            if (is_array($newContent['images'])) {
                foreach ($newContent['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newContent['images'][$index]);
                    } else {
                        $newContent['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newContent['images'] = implode('||', $newContent['images']);
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > $newContent['sorting']) {
                    $newContent['sorting'] = 1;
                }
                $objContent->increaseSorting($newContent['sorting'], 1);

                if ($id != false) {
                    $newContent['user_id'] = $id[1];
                } else {
                    $newContent['user_id'] = Nine_Registry::getLoggedInUserId();
                }


                $newContent['content_category_gid'] = $this->_content_category_gid_news;
                $objContent->insert($newContent);
                /**
                 * Message
                 */
                $this->session->contentMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Content is created successfully.')
                );
//                echo "<pre>HERE";die;
                $this->_redirect('estore/manage-news/' . $user['alias'] . '.html');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now: ' . $e));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $this->view->id = $id;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Content'));
        $this->view->fullPermisison = $this->checkPermission('new_content', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_content');
        $this->view->menu = array(
            0 => 'news',
            1 => 'new-news'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-newspaper-o',
                'url' => Nine_Registry::getBaseUrl() . 'estore/manage-news/' . $user['alias'] . '.html',
                'name' => Nine_Language::translate('Manager News')
            ),
            1 => array(
                'icon' => 'fa-plus',
                'url' => '',
                'name' => Nine_Language::translate('New News')
            )
        );

        $this->view->alias = $user['alias'] . '.html';
    }

    public function editNewsAction() {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        $this->setLayout('estore');

        $objContent = new Models_Content();
        $objCat = new Models_ContentCategory();
        $objLang = new Models_Lang();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_content', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        if (false == $gid) {
            $this->_redirect('estore/manage-news/' . $user['alias'] . '.html');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_content', null, '*')) || (false != $lid && false == $this->checkPermission('edit_content', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        

        $data = $this->_getParam('data', false);

        /**
         * Get all content languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        
        /**
         * Check permisison for each language
         */
//        foreach ($allLangs as $index => $lang) {
//            if (false == $this->checkPermission('new_content', null, $lang['lang_id'])) {
//                /**
//                 * Clear data
//                 */
//                unset($data[$lang['lang_id']]);
//                /**
//                 * Disappear this language
//                 */
//                unset($allLangs[$index]);
//            }
//        }
        $allContentLangs = $objContent->setAllLanguages(true)->getByColumns(array('content_gid=?' => $gid, 'user_id=?' => $user['user_id']))->toArray();
        /**
         * Check permisison for each language
         */
//        foreach ($allLangs as $lang) {
//            if (false == $this->checkPermission('edit_content', null, $lang['lang_id'])) {
//                /**
//                 * Disappear this language
//                 */
//                if (isset($index) && isset($allLangs[$index])) {
//                    unset($allLangs[$index]);
//                }
//                unset($allContentLangs[$lang['lang_id']]);
//                unset($data[$lang['lang_id']]);
//            }
//        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new content
             */
            $newContent = $data;
            if (null == $newContent['sorting']) {
                unset($newContent['sorting']);
            }

            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                //list($newContent[$lang['lang_id']]['intro_text'], $newContent[$lang['lang_id']]['full_text'])= Nine_Function::splitTextWithReadmore($newContent[$lang['lang_id']]['full_text']);
                if ($newContent[$lang['lang_id']]['alias'] == "") {
                    $newContent[$lang['lang_id']]['alias'] = $objContent->convert_vi_to_en($newContent[$lang['lang_id']]['title']);
                    $newContent[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", $newContent[$lang['lang_id']]['alias']));
                }
            }
            /**
             * Remove empty images
             */
            if (is_array($newContent['images'])) {
                foreach ($newContent['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newContent['images'][$index]);
                    } else {
                        $newContent['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newContent['images'] = implode('||', $newContent['images']);
            try {
                /**
                 * Update
                 */
                $objContent->update($newContent, array('content_gid=?' => $gid));
                /**
                 * Message
                 */
                $this->session->contentMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('News is updated successfully.')
                );

                $this->_redirect('estore/manage-news/' . $user['alias'] . '.html');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allContentLangs);

            if (false == $data) {
                $this->session->contentMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("News doesn't exist.")
                );
                $this->_redirect('estore/manage-news/' . $user['alias'] . '.html');
            }
            /**
             * Change date
             */
            if (0 != $data['publish_up_date']) {
                $data['publish_up_date'] = date('d/m/Y', $data['publish_up_date']);
            } else {
                $data['publish_up_date'] = '';
            }
            if (0 != $data['publish_down_date']) {
                $data['publish_down_date'] = date('d/m/Y', $data['publish_down_date']);
            } else {
                $data['publish_down_date'] = '';
            }
            /**
             * Format image
             */
            $data['images'] = explode('||', $data['images']);
            if (!is_array($data['images'])) {
                $data['images'] = array();
            }
            $data['images'] = array_pad($data['images'], 50, null);
            /**
             * Get all lang contents
             */
            foreach ($allContentLangs as $content) {
                /**
                 * Rebuild readmore DIV
                 */
                $content['full_text'] = Nine_Function::combineTextWithReadmore($content['intro_text'], $content['full_text']);
                $data[$content['lang_id']] = $content;
            }

            /**
             * Add deleteable field
             */
            if (null != @$data['content_category_gid']) {
                $cat = @reset($objCat->getByColumns(array('content_category_gid' => $data['content_category_gid']))->toArray());
                $data['content_deleteable'] = @$cat['content_deleteable'];
            }
        }
        if (null == $data['time_start']) {
            
        } else {
            $data['time_start'] = date("d-m-Y", $data['time_start']);
        }
        /**
         * Prepare for template
         */
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Content'));
        $this->view->fullPermisison = $this->checkPermission('edit_content', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_content');
        $this->view->menu = array(
            0 => 'news',
            1 => 'manager-news'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-newspaper-o',
                'url' => Nine_Registry::getBaseUrl() . 'estore/manage-news/' . $user['alias'] . '.html',
                'name' => Nine_Language::translate('Manager News')
            ),
            1 => array(
                'icon' => 'fa-pencil',
                'url' => '',
                'name' => Nine_Language::translate('Edit News')
            )
        );

        $this->view->alias = $user['alias'] . '.html';
    }

    public function deleteNewsAction() {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $objContent = new Models_Content();
        $objCat = new Models_ContentCategory();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_content')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid = $this->_getParam('gid', false);

        if (false == $gid) {
            $this->_redirect('estore/manage-news/' . $user['alias'] . '.html');
        }

        $gids = explode('_', trim($gid, '_'));

        try {
            foreach ($gids as $gid) {

                $content = @reset($objContent->getByColumns(array('content_gid=?' => $gid))->toArray());
                $cat = @reset($objCat->getByColumns(array('content_category_gid=?' => $content['content_category_gid']))->toArray());
                if (0 == $cat['content_deleteable']) {
                    $this->session->contentMessage = array(
                        'success' => false,
                        'message' => Nine_Language::translate('Can NOT delete content (' . $gid . '). Please try again')
                    );
                    $this->_redirect('estore/manage-news/' . $user['alias'] . '.html');
                } else {
                    $objContent->delete(array('content_gid=?' => $gid, 'user_id=?' => $user['user_id']));
                }
            }
            $this->session->contentMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Content is deleted successfully')
            );
        } catch (Exception $e) {
            $this->session->contentMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT delete this content. Please try again')
            );
        }
        $this->_redirect('estore/manage-news/' . $user['alias'] . '.html');
    }

    /*     * ************************************************************************************************
     *                                         GENERAL FUNCTIONS
     * ************************************************************************************************ */

    public function tinnhanhContentAction() {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $objContent = new Models_Content();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);

        if (false == $gid) {
            $this->_redirect('estore/' . $user['alias'] . '.html');
        }

        $gids = explode('_', trim($gid, '_'));

        /**
         * Change general status
         * Check full permission
         */
        if (false == $this->checkPermission('edit_content', null, '*')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        try {
            foreach ($gids as $gid) {
                $objContent->update(array('tinnhanh' => 1), array('content_gid=?' => $gid, 'user_id=?' => $user['user_id']));
            }
            $this->session->contentMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Content is Flashes successfully')
            );
        } catch (Exception $e) {
            $this->session->contentMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT activate this content. Please try again')
            );
        }

        $estorePage = $objContent->getEstoreContentPage($gid);
        $this->_redirect('estore/' . $estorePage . '/' . $user['alias'] . '.html');
    }

    public function distinnhanhContentAction() {

        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $objContent = new Models_Content();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);

        if (false == $gid) {
            $this->_redirect('estore/' . $user['alias'] . '.html');
        }

        $gids = explode('_', trim($gid, '_'));

        /**
         * Change general status
         * Check full permission
         */
        if (false == $this->checkPermission('edit_content', null, '*')) {
            $this->_forwardToNoPermissionPage();
            return;
        }
        try {
            foreach ($gids as $gid) {
                $objContent->update(array('tinnhanh' => 0), array('content_gid=?' => $gid, 'user_id=?' => $user['user_id']));
            }
            $this->session->contentMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Content is Dis Flashes successfully')
            );
        } catch (Exception $e) {
            $this->session->contentMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT activate this content. Please try again')
            );
        }

        $estorePage = $objContent->getEstoreContentPage($gid);
        $this->_redirect('estore/' . $estorePage . '/' . $user['alias'] . '.html');
    }

    public function disableContentAction() {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $objContent = new Models_Content();

        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);

        if (false == $gid) {
            $this->_redirect('estore/' . $user['alias'] . '.html');
        }

        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_content', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objContent->update(array('genabled' => 0), array('content_gid=?' => $gid, 'user_id=?' => $user['user_id']));
                    $news = $objContent->getContentByGid($gid)->toArray();
                }
                $this->session->contentMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Content is disable successfully')
                );
            } catch (Exception $e) {
                $this->session->contentMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT deactive this content. Please try again')
                );
            }
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_content', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objContent->update(array('enabled' => 0), array('content_gid=?' => $gid, 'lang_id=?' => $lid, 'user_id=?' => $user['user_id']));
                }
                $this->session->contentMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Content is disable successfully')
                );
            } catch (Exception $e) {
                $this->session->contentMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT deactive this content. Please try again')
                );
            }
        }

        $this->_redirect('estore/manage-news/' . $user['alias'] . '.html');
    }

    public function enableContentAction() {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $objContent = new Models_Content();
        $objUser = new Models_User();
        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);

        if (false == $gid) {
            $this->_redirect('estore/' . $user['alias'] . '.html');
        }

        $gids = explode('_', trim($gid, '_'));
        if (false == $lid) {
            /**
             * Change general status
             * Check full permission
             */
            if (false == $this->checkPermission('edit_content', null, '*')) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objContent->update(array('genabled' => 1), array('content_gid=?' => $gid, 'user_id=?' => $user['user_id']));
                    $news = $objContent->getContentByGid($gid)->toArray();
                }
                $this->session->contentMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Content is enable successfully')
                );
            } catch (Exception $e) {
                $this->session->contentMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT activate this content. Please try again')
                );
            }
        } else {
            /**
             * Check permission on each language
             */
            if (false == $this->checkPermission('edit_content', null, $lid)) {
                $this->_forwardToNoPermissionPage();
                return;
            }
            try {
                foreach ($gids as $gid) {
                    $objContent->update(array('enabled' => 1), array('content_gid=?' => $gid, 'lang_id=?' => $lid, 'user_id=?' => $user['user_id']));
                }
                $this->session->contentMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Content is enable successfully')
                );
            } catch (Exception $e) {
                $this->session->contentMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Can NOT activate this content. Please try again')
                );
            }
        }
        $this->_redirect('estore/manage-news/' . $user['alias'] . '.html');
    }

    /*     * ************************************************************************************************
     *                                         PROMOTION's FUNCTIONS
     * ************************************************************************************************ */

    public function managePromotionAction() {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        $this->setLayout('estore');

        $objContent = new Models_Content();
        $objLang = new Models_Lang();
        $objCat = new Models_ContentCategory();

        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_content', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $this->view->headTitle(Nine_Language::translate('manage Content '));


        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page", false);
        $displayNum = $this->_getParam('displayNum', false);
        if ($currentPage == false) {
            $currentPage = 1;
            $this->session->contentDisplayNum = null;
            $this->session->contentCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id => $value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objContent->update(array('sorting' => $value), array('content_gid=?' => $id));
            $this->session->contentMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("News Order is edited successfully")
            );
        }

        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->contentDisplayNum;
        } else {
            $this->session->contentDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->contentCondition;
        } else {
            $this->session->contentCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        if (@$condition['keyword'] != '') {
            $condition['keyword'] = $objContent->convert_vi_to_en($condition['keyword']);
            $condition['keyword'] = preg_replace("/( )/", '-', $condition['keyword']);
        }

        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_content', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        /**
         * Condition by User Id
         */
        $condition['user_id'] = $user['user_id'];

        /**
         * Condition News
         */
        $condition['content_category_gid'] = $this->_content_category_gid_promotion;

        /**
         * Get all contents
         */
        $numRowPerPage = $numRowPerPage;
        $allContent = $objContent->getAllContent($condition, array('sorting ASC', 'content_gid DESC', 'content_id ASC'), $numRowPerPage, ($currentPage - 1) * $numRowPerPage
        );
//        echo '<pre>';print_r($allContent);die;
        /**
         * Count all contents
         */
        $count = count($objContent->getallContent($condition));
        /**
         * Format
         */
        $tmp = array();
        $tmp2 = false;
        $tmpGid = @$allContent[0]['content_gid'];
        foreach ($allContent as $index => $content) {
            /**
             * Change date
             */
            if (0 != $content['created_date']) {
                $content['created_date'] = date($config['dateFormat'], $content['created_date']);
            } else {
                $content['created_date'] = '';
            }
            if (0 != $content['publish_up_date']) {
                $content['publish_up_date'] = date($config['dateFormat'], $content['publish_up_date']);
            } else {
                $content['publish_up_date'] = '';
            }
            if (0 != $content['publish_down_date']) {
                $content['publish_down_date'] = date($config['dateFormat'], $content['publish_down_date']);
            } else {
                $content['publish_down_date'] = '';
            }
            if ($tmpGid != $content['content_gid']) {
                $tmp[] = $tmp2;
                $tmp2 = false;
                $tmpGid = $content['content_gid'];
            }
            if (false === $tmp2) {
                $tmp2 = $content;
                $tmp2['hit'] = 0;
            }
            $tmp2['hit'] += $content['hit'];
            $tmp2['langs'][] = $content;
            /**
             * Final element
             */
            if ($index == count($allContent) - 1) {
                $tmp[] = $tmp2;
            }
        }
        $allContent = $tmp;
        $export = $this->_getParam('export', false);
        if ($export != false) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $style = "style = 'border: 1px solid'";

            if (Nine_Language::getCurrentLangId() == 1) {

                $textHeader = array(
                    "<b>No</b>",
                    "<b>Category Content</b>",
                    "<b>Estore Upload</b>",
                    "<b>Created Date</b>",
                    "<b>Title</b>"
                );
            } else {

                $textHeader = array(
                    mb_convert_encoding("<b>S? Th? T?</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Danh M?c B�i Vi?t</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Gian H�ng Upload</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Ng�y Kh?i T?o</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Ti�u �?</b>", 'HTML-ENTITIES', 'UTF-8')
                );
            }

            $header = "<tr>";
            foreach ($textHeader as $text) {
                $header .= "<td $style>" . utf8_encode($text) . "</td>";
            }
            $header .= "</tr>";

            $content = '';
            $no = 1;

            foreach ($allContent as $item) {
                $content .= "<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['cname'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['uname'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['created_date'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['title'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "</tr>";
                $no++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-content-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();
        }
        /**
         * Set values for tempalte
         */
//        echo print_r($condition); die;
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allContent = $allContent;
        $this->view->contentMessage = $this->session->contentMessage;
        $this->session->contentMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_content');
        $this->view->EditPermisision = $this->checkPermission('edit_content');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_content');
        $this->view->DeletePermisision = $this->checkPermission('delete_content');
        $this->view->allLangs = $allLangs;

        $this->view->menu = array(
            0 => 'promotion',
            1 => 'manager-promotion'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-newspaper-o',
                'url' => Nine_Registry::getBaseUrl() . 'estore/manage-promotion/' . $user['alias'] . '.html',
                'name' => Nine_Language::translate('Manager Promotion')
            )
        );

        $this->view->alias = $user['alias'] . '.html';
        $this->view->username = $user['alias'];
    }

    public function newPromotionAction() {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        $this->setLayout('estore');

        $objCat = new Models_ContentCategory();
        $objLang = new Models_Lang();
        $objContent = new Models_Content();
        $id = $this->_getParam('id', false);


        if ($id != false) {
            $id = explode("_", $id);
            if (count($id) != 2) {
                $this->_redirect('estore/manage-promotion/' . $user['alias'] . '.html');
            }
        }
        /**
         * Check permission
         */
        if (false == $this->checkPermission('new_content', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data = $this->_getParam('data', false);

        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('new_content', null, $lang['lang_id'])) {
                /**
                 * Clear data
                 */
                unset($data[$lang['lang_id']]);
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new content
             */
            $newContent = $data;
            $newContent['created_date'] = time();
            /**
             * Change format date
             */
            if (null == $newContent['time_start']) {
                unset($newContent['time_start']);
            } else {
                $tmp = explode('-', $newContent['time_start']);
                $newContent['time_start'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (null == $newContent['publish_up_date']) {
                unset($newContent['publish_up_date']);
            } else {
                $tmp = explode('/', $newContent['publish_up_date']);
                $newContent['publish_up_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (null == $newContent['publish_down_date']) {
                unset($newContent['publish_down_date']);
            } else {
                $tmp = explode('/', $newContent['publish_down_date']);
                $newContent['publish_down_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            /**
             * Sorting
             */
            if (null == $newContent['sorting']) {
                unset($newContent['sorting']);
            }
            if (false == $this->checkPermission('new_content', null, '*')) {
                $newContent['genabled'] = 0;
                $newContent['publish_up_date'] = null;
                $newContent['publish_down_date'] = null;
                $newContent['sorting'] = 1;
            }

            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newContent[$lang['lang_id']]['intro_text'], $newContent[$lang['lang_id']]['full_text']) = Nine_Function::splitTextWithReadmore($newContent[$lang['lang_id']]['full_text']);
                if ($newContent[$lang['lang_id']]['alias'] == "") {
                    $newContent[$lang['lang_id']]['alias'] = $objContent->convert_vi_to_en($newContent[$lang['lang_id']]['title']);
                    $newContent[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", $newContent[$lang['lang_id']]['alias']));
                }
            }

            /**
             * Remove empty images
             */
            if (is_array($newContent['images'])) {
                foreach ($newContent['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newContent['images'][$index]);
                    } else {
                        $newContent['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newContent['images'] = implode('||', $newContent['images']);
            try {
                /**
                 * Increase all current sortings
                 */
                if (1 > $newContent['sorting']) {
                    $newContent['sorting'] = 1;
                }
                $objContent->increaseSorting($newContent['sorting'], 1);

                if ($id != false) {
                    $newContent['user_id'] = $id[1];
                } else {
                    $newContent['user_id'] = Nine_Registry::getLoggedInUserId();
                }

                if ($newContent['publish_up_date'] == '') {
                    $newContent['publish_up_date'] = date();
                }

                $newContent['content_category_gid'] = $this->_content_category_gid_promotion;
                $objContent->insert($newContent);


                $objUser = new Models_User();
                $allNewlater = array();
                foreach ($user['follow'] as $index => $item) {
                    if (trim($item) == '') {
                        unset($user['follow'][$index]);
                    } else {
                        $user_to = current($objUser->getByColumns(array("user_id = ?" => $item))->toArray());

                        $newMail = array();
                        $subject = $newContent['title'];
                        $newMail['body'] = $newContent['full_text'];
                        $objMail = new Models_Mail();
                        $objMail->sendHtmlMail('newsletter', $newMail, $user_to['email'], $subject);

                        $userSession = Nine_Registry::getLoggedInUser();
                        $newCategory = array();
                        $newCategory['parent_id'] = 0;
                        $newCategory['type'] = 3;
                        $newCategory['description'] = $newContent['full_text'];
                        $newCategory['created_date'] = time();
                        $newCategory[1]['description'] = $newContent['full_text'];
                        $newCategory[2]['description'] = $newContent['full_text'];
                        $newCategory['user_from_id'] = $item;
                        $newCategory['user_to_id'] = Nine_Registry::getLoggedInUserId();
                        $newCategory['user_view'] = 0;
                        $newCategory['admin_view'] = 0;

                        $objCategoryMess = new Models_MessageCategory();
                        $objCategoryMess->insert($newCategory);
                    }
                }



                /**
                 * Message
                 */
                $this->session->contentMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Content is created successfully.')
                );
//                echo "<pre>HERE";die;
                $this->_redirect('estore/manage-promotion/' . $user['alias'] . '.html');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            $data = array('sorting' => 1);
        }
        /**
         * Prepare for template
         */
        $this->view->id = $id;
        $this->view->allLangs = $allLangs;
        $this->view->errors = $errors;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('New Content'));
        $this->view->fullPermisison = $this->checkPermission('new_content', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_content');
        $this->view->menu = array(
            0 => 'promotion',
            1 => 'new-promotion'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-newspaper-o',
                'url' => Nine_Registry::getBaseUrl() . 'estore/manage-promotion/' . $user['alias'] . '.html',
                'name' => Nine_Language::translate('Manager Promotion')
            ),
            1 => array(
                'icon' => 'fa-plus',
                'url' => '',
                'name' => Nine_Language::translate('New Promotion')
            )
        );

        $this->view->alias = $user['alias'] . '.html';
    }

    public function editPromotionAction() {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        $this->setLayout('estore');

        $objContent = new Models_Content();
        $objCat = new Models_ContentCategory();
        $objLang = new Models_Lang();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_content', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        if (false == $gid) {
            $this->_redirect('estore/manage-promotion/' . $user['alias'] . '.html');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_content', null, '*')) || (false != $lid && false == $this->checkPermission('edit_content', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data = $this->_getParam('data', false);

        /**
         * Get all content languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allContentLangs = $objContent->setAllLanguages(true)->getByColumns(array('content_gid=?' => $gid, 'user_id=?' => $user['user_id']))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_content', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allContentLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new content
             */
            $newContent = $data;
            /**
             * Change format date
             */
            if (null == $newContent['time_start']) {
                unset($newContent['time_start']);
            } else {
                $tmp = explode('-', $newContent['time_start']);
                $newContent['time_start'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (null != $newContent['publish_up_date']) {
                $tmp = explode('/', $newContent['publish_up_date']);
                $newContent['publish_up_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (null != $newContent['publish_down_date']) {
                $tmp = explode('/', $newContent['publish_down_date']);
                $newContent['publish_down_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            /**
             * Sorting
             */
            if (null == $newContent['sorting']) {
                unset($newContent['sorting']);
            }
            if (false == $this->checkPermission('new_content', null, '*')) {
                unset($newContent['genabled']);
                unset($newContent['publish_up_date']);
                unset($newContent['publish_down_date']);
                unset($newContent['sorting']);
            }

            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newContent[$lang['lang_id']]['intro_text'], $newContent[$lang['lang_id']]['full_text']) = Nine_Function::splitTextWithReadmore($newContent[$lang['lang_id']]['full_text']);
                if ($newContent[$lang['lang_id']]['alias'] == "") {
                    $newContent[$lang['lang_id']]['alias'] = $objContent->convert_vi_to_en($newContent[$lang['lang_id']]['title']);
                    $newContent[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", $newContent[$lang['lang_id']]['alias']));
                }
            }
            /**
             * Remove empty images
             */
            if (is_array($newContent['images'])) {
                foreach ($newContent['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newContent['images'][$index]);
                    } else {
                        $newContent['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newContent['images'] = implode('||', $newContent['images']);
            try {
                /**
                 * Update
                 */
                $objContent->update($newContent, array('content_gid=?' => $gid));
                /**
                 * Message
                 */
                $this->session->contentMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('News is updated successfully.')
                );

                $this->_redirect('estore/manage-promotion/' . $user['alias'] . '.html');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allContentLangs);

            if (false == $data) {
                $this->session->contentMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("News doesn't exist.")
                );
                $this->_redirect('estore/manage-promotion/' . $user['alias'] . '.html');
            }
            /**
             * Change date
             */
            if (0 != $data['publish_up_date']) {
                $data['publish_up_date'] = date('d/m/Y', $data['publish_up_date']);
            } else {
                $data['publish_up_date'] = '';
            }
            if (0 != $data['publish_down_date']) {
                $data['publish_down_date'] = date('d/m/Y', $data['publish_down_date']);
            } else {
                $data['publish_down_date'] = '';
            }
            /**
             * Format image
             */
            $data['images'] = explode('||', $data['images']);
            if (!is_array($data['images'])) {
                $data['images'] = array();
            }
            $data['images'] = array_pad($data['images'], 50, null);
            /**
             * Get all lang contents
             */
            foreach ($allContentLangs as $content) {
                /**
                 * Rebuild readmore DIV
                 */
                $content['full_text'] = Nine_Function::combineTextWithReadmore($content['intro_text'], $content['full_text']);
                $data[$content['lang_id']] = $content;
            }

            /**
             * Add deleteable field
             */
            if (null != @$data['content_category_gid']) {
                $cat = @reset($objCat->getByColumns(array('content_category_gid' => $data['content_category_gid']))->toArray());
                $data['content_deleteable'] = @$cat['content_deleteable'];
            }
        }
        if (null == $data['time_start']) {
            
        } else {
            $data['time_start'] = date("d-m-Y", $data['time_start']);
        }
        /**
         * Prepare for template
         */
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Promotion'));
        $this->view->fullPermisison = $this->checkPermission('edit_content', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_content');
        $this->view->menu = array(
            0 => 'promotion',
            1 => 'manager-promotion'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-newspaper-o',
                'url' => Nine_Registry::getBaseUrl() . 'estore/manage-promotion/' . $user['alias'] . '.html',
                'name' => Nine_Language::translate('Manager Promotion')
            ),
            1 => array(
                'icon' => 'fa-pencil',
                'url' => '',
                'name' => Nine_Language::translate('Edit Promotion')
            )
        );

        $this->view->alias = $user['alias'] . '.html';
    }

    public function deletePromotionAction() {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }

        $objContent = new Models_Content();
        $objCat = new Models_ContentCategory();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('delete_content')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid = $this->_getParam('gid', false);

        if (false == $gid) {
            $this->_redirect('estore/manage-promotion/' . $user['alias'] . '.html');
        }

        $gids = explode('_', trim($gid, '_'));

        try {
            foreach ($gids as $gid) {

                $content = @reset($objContent->getByColumns(array('content_gid=?' => $gid))->toArray());
                $cat = @reset($objCat->getByColumns(array('content_category_gid=?' => $content['content_category_gid']))->toArray());
                if (0 == $cat['content_deleteable']) {
                    $this->session->contentMessage = array(
                        'success' => false,
                        'message' => Nine_Language::translate('Can NOT delete content (' . $gid . '). Please try again')
                    );
                    $this->_redirect('estore/manage-promotion/' . $user['alias'] . '.html');
                } else {
                    $objContent->delete(array('content_gid=?' => $gid, 'user_id=?' => $user['user_id']));
                }
            }
            $this->session->contentMessage = array(
                'success' => true,
                'message' => Nine_Language::translate('Content is deleted successfully')
            );
        } catch (Exception $e) {
            $this->session->contentMessage = array(
                'success' => false,
                'message' => Nine_Language::translate('Can NOT delete this content. Please try again')
            );
        }
        $this->_redirect('estore/manage-promotion/' . $user['alias'] . '.html');
    }

    /*     * ************************************************************************************************
     *                                         ESTORE PAGE's FUNCTIONS
     * ************************************************************************************************ */

    public function manageEstorePageAction() {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        $this->setLayout('estore');

        $objContent = new Models_Content();
        $objLang = new Models_Lang();
        $objCat = new Models_ContentCategory();

        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_content', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $this->view->headTitle(Nine_Language::translate('Manage Estore Page '));


        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page", false);
        $displayNum = $this->_getParam('displayNum', false);
        if ($currentPage == false) {
            $currentPage = 1;
            $this->session->contentDisplayNum = null;
            $this->session->contentCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id => $value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objContent->update(array('sorting' => $value), array('content_gid=?' => $id));
            $this->session->contentMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("News Order is edited successfully")
            );
        }

        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->contentDisplayNum;
        } else {
            $this->session->contentDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->contentCondition;
        } else {
            $this->session->contentCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }
        if (@$condition['keyword'] != '') {
            $condition['keyword'] = $objContent->convert_vi_to_en($condition['keyword']);
            $condition['keyword'] = preg_replace("/( )/", '-', $condition['keyword']);
        }

        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_content', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }

        /**
         * Condition by User Id
         */
        $condition['user_id'] = $user['user_id'];

        /**
         * Condition News
         */
        $condition['content_category_gid_estore'] = '5,25';

        /**
         * Get all contents
         */
        $numRowPerPage = $numRowPerPage;
        $allContent = $objContent->getAllContent($condition, array('sorting ASC', 'content_gid DESC', 'content_id ASC'), $numRowPerPage, ($currentPage - 1) * $numRowPerPage
        );
//        echo '<pre>';print_r($allContent);die;
        /**
         * Count all contents
         */
        $count = count($objContent->getallContent($condition));
        /**
         * Format
         */
        $tmp = array();
        $tmp2 = false;
        $tmpGid = @$allContent[0]['content_gid'];
        foreach ($allContent as $index => $content) {
            /**
             * Change date
             */
            if (0 != $content['created_date']) {
                $content['created_date'] = date($config['dateFormat'], $content['created_date']);
            } else {
                $content['created_date'] = '';
            }
            if (0 != $content['publish_up_date']) {
                $content['publish_up_date'] = date($config['dateFormat'], $content['publish_up_date']);
            } else {
                $content['publish_up_date'] = '';
            }
            if (0 != $content['publish_down_date']) {
                $content['publish_down_date'] = date($config['dateFormat'], $content['publish_down_date']);
            } else {
                $content['publish_down_date'] = '';
            }
            if ($tmpGid != $content['content_gid']) {
                $tmp[] = $tmp2;
                $tmp2 = false;
                $tmpGid = $content['content_gid'];
            }
            if (false === $tmp2) {
                $tmp2 = $content;
                $tmp2['hit'] = 0;
            }
            $tmp2['hit'] += $content['hit'];
            $tmp2['langs'][] = $content;
            /**
             * Final element
             */
            if ($index == count($allContent) - 1) {
                $tmp[] = $tmp2;
            }
        }
        $allContent = $tmp;
        $export = $this->_getParam('export', false);
        if ($export != false) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $style = "style = 'border: 1px solid'";

            if (Nine_Language::getCurrentLangId() == 1) {

                $textHeader = array(
                    "<b>No</b>",
                    "<b>Category Content</b>",
                    "<b>Estore Upload</b>",
                    "<b>Created Date</b>",
                    "<b>Title</b>"
                );
            } else {

                $textHeader = array(
                    mb_convert_encoding("<b>S? Th? T?</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Danh M?c B�i Vi?t</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Gian H�ng Upload</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Ng�y Kh?i T?o</b>", 'HTML-ENTITIES', 'UTF-8'),
                    mb_convert_encoding("<b>Ti�u �?</b>", 'HTML-ENTITIES', 'UTF-8')
                );
            }

            $header = "<tr>";
            foreach ($textHeader as $text) {
                $header .= "<td $style>" . utf8_encode($text) . "</td>";
            }
            $header .= "</tr>";

            $content = '';
            $no = 1;

            foreach ($allContent as $item) {
                $content .= "<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['cname'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['uname'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['created_date'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['title'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "</tr>";
                $no++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-content-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();
        }
        /**
         * Set values for tempalte
         */
//        echo print_r($condition); die;
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allContent = $allContent;
        $this->view->contentMessage = $this->session->contentMessage;
        $this->session->contentMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage;
        $this->view->fullPermisison = $this->checkPermission('see_content');
        $this->view->EditPermisision = $this->checkPermission('edit_content');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_content');
        $this->view->DeletePermisision = $this->checkPermission('delete_content');
        $this->view->allLangs = $allLangs;

        $this->view->menu = array(
            0 => 'estore-page',
            1 => 'manager-estore-page'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-newspaper-o',
                'url' => Nine_Registry::getBaseUrl() . 'estore/manage-estore-page/' . $user['alias'] . '.html',
                'name' => Nine_Language::translate('Manager Estore Page')
            )
        );

        $this->view->alias = $user['alias'] . '.html';
        $this->view->username = $user['alias'];
    }

    public function editEstorePageAction() {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
            return false;
        }
        $this->setLayout('estore');

        $objContent = new Models_Content();
        $objCat = new Models_ContentCategory();
        $objLang = new Models_Lang();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_content', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid = $this->_getParam('gid', false);
        $lid = $this->_getParam('lid', false);
        if (false == $gid) {
            $this->_redirect('estore/manage-estore-page/' . $user['alias'] . '.html');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_content', null, '*')) || (false != $lid && false == $this->checkPermission('edit_content', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data = $this->_getParam('data', false);

        /**
         * Get all content languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allContentLangs = $objContent->setAllLanguages(true)->getByColumns(array('content_gid=?' => $gid, 'user_id=?' => $user['user_id']))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_content', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allContentLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }

        $errors = array();
        if (false !== $data) {
            /**
             * Insert new content
             */
            $newContent = $data;
            /**
             * Change format date
             */
            if (null == $newContent['time_start']) {
                unset($newContent['time_start']);
            } else {
                $tmp = explode('-', $newContent['time_start']);
                $newContent['time_start'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (null != $newContent['publish_up_date']) {
                $tmp = explode('/', $newContent['publish_up_date']);
                $newContent['publish_up_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            if (null != $newContent['publish_down_date']) {
                $tmp = explode('/', $newContent['publish_down_date']);
                $newContent['publish_down_date'] = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
            }
            /**
             * Sorting
             */
            if (null == $newContent['sorting']) {
                unset($newContent['sorting']);
            }
            if (false == $this->checkPermission('new_content', null, '*')) {
                unset($newContent['genabled']);
                unset($newContent['publish_up_date']);
                unset($newContent['publish_down_date']);
                unset($newContent['sorting']);
            }

            /**
             * Slipt intro_text & full_text
             */
            foreach ($allLangs as $index => $lang) {
                list($newContent[$lang['lang_id']]['intro_text'], $newContent[$lang['lang_id']]['full_text']) = Nine_Function::splitTextWithReadmore($newContent[$lang['lang_id']]['full_text']);
                if ($newContent[$lang['lang_id']]['alias'] == "") {
                    $newContent[$lang['lang_id']]['alias'] = $objContent->convert_vi_to_en($newContent[$lang['lang_id']]['title']);
                    $newContent[$lang['lang_id']]['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", $newContent[$lang['lang_id']]['alias']));
                }
            }
            /**
             * Remove empty images
             */
            if (is_array($newContent['images'])) {
                foreach ($newContent['images'] as $index => $image) {
                    if (null == $image) {
                        unset($newContent['images'][$index]);
                    } else {
                        $newContent['images'][$index] = Nine_Function::getImagePath($image);
                    }
                }
            }
            $newContent['images'] = implode('||', $newContent['images']);
            try {
                /**
                 * Update
                 */
                $objContent->update($newContent, array('content_gid=?' => $gid));
                /**
                 * Message
                 */
                $this->session->contentMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate('News is updated successfully.')
                );

                $this->_redirect('estore/manage-estore-page/' . $user['alias'] . '.html');
            } catch (Exception $e) {
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allContentLangs);

            if (false == $data) {
                $this->session->contentMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("News doesn't exist.")
                );
                $this->_redirect('estore/manage-estore-page/' . $user['alias'] . '.html');
            }
            /**
             * Change date
             */
            if (0 != $data['publish_up_date']) {
                $data['publish_up_date'] = date('d/m/Y', $data['publish_up_date']);
            } else {
                $data['publish_up_date'] = '';
            }
            if (0 != $data['publish_down_date']) {
                $data['publish_down_date'] = date('d/m/Y', $data['publish_down_date']);
            } else {
                $data['publish_down_date'] = '';
            }
            /**
             * Format image
             */
            $data['images'] = explode('||', $data['images']);
            if (!is_array($data['images'])) {
                $data['images'] = array();
            }
            $data['images'] = array_pad($data['images'], 50, null);
            /**
             * Get all lang contents
             */
            foreach ($allContentLangs as $content) {
                /**
                 * Rebuild readmore DIV
                 */
                $content['full_text'] = Nine_Function::combineTextWithReadmore($content['intro_text'], $content['full_text']);
                $data[$content['lang_id']] = $content;
            }

            /**
             * Add deleteable field
             */
            if (null != @$data['content_category_gid']) {
                $cat = @reset($objCat->getByColumns(array('content_category_gid' => $data['content_category_gid']))->toArray());
                $data['content_deleteable'] = @$cat['content_deleteable'];
            }
        }
        if (null == $data['time_start']) {
            
        } else {
            $data['time_start'] = date("d-m-Y", $data['time_start']);
        }
        /**
         * Prepare for template
         */
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Estore Page'));
        $this->view->fullPermisison = $this->checkPermission('edit_content', null, '*');
        $this->view->CheckGenalbel = $this->checkPermission('genabled_content');
        $this->view->menu = array(
            0 => 'estore-page',
            1 => 'manager-estore-page'
        );
        $this->view->breadcrumb = array(
            0 => array(
                'icon' => 'fa-newspaper-o',
                'url' => Nine_Registry::getBaseUrl() . 'estore/manage-estore-page/' . $user['alias'] . '.html',
                'name' => Nine_Language::translate('Manager Estore Page')
            ),
            1 => array(
                'icon' => 'fa-pencil',
                'url' => '',
                'name' => Nine_Language::translate('Edit Estore Page')
            )
        );

        $this->view->alias = $user['alias'] . '.html';
    }

}
