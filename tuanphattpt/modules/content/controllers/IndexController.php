<?php

require_once 'modules/content/models/Content.php';
require_once 'modules/content/models/ContentCategory.php';
require_once 'modules/user/models/User.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/product/models/EstoreCategory.php';
require_once 'modules/banner/models/Banner.php';

class content_IndexController extends Nine_Controller_Action {

    private function _redirectToNotFoundPage() {
        $this->_redirect("");
    }

    public function indexAction() {
        $objContent = new Models_Content();
        $objUser = new Models_User();
        $objEstoreCategory = new Models_EstoreCategory();
        $objContentCategory = new Models_ContentCategory ();
        $config = Nine_Registry::getConfig();
        /**
         * Get category
         */
        $categoryGid = $this->_getParam('gid', false);
        if (false == $categoryGid) {
            $this->_redirectToNotFoundPage();
        }
        $numRowPerPage = 10;
        $currentPage = $this->_getParam("page", 1);

        $this->setLayout('front-default-category');
        $templatePath = Nine_Registry::getModuleName() . '/views/default';

        $allNews = $objContent->getAllEnabledContentByCategory($categoryGid, array(), array('sorting ASC', 'content_id DESC'), $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
        foreach ($allNews as &$item) {
            $user = $objUser->getUser($item['user_id']);
            $item['author'] = $user['username'];
            $item['author_alias'] = $user['alias'];
            $item['intro_text'] = Nine_Function::subStringAtBlank(trim(strip_tags($item ['full_text'])), 150);
            $tmp = explode('||', $item ['images']);

            $item ['images'] = Nine_Function::getThumbImage(@$tmp[0], 375, 200, false, false);
            unset($item);
        }
        /**
         * Count all category
         */
        $count = count($objContent->getAllEnabledContentByCategory($categoryGid, array()));
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $objBanner = new Models_Banner();
        $this->view->adv = $objBanner->getAllBanner(array('user_id' => 1, 'banner_category_gid' => 141, 'page' => 4, 'location' => 1), 'sorting');
        $this->view->pageTitle = Nine_Language::translate('Tin tức');
        $this->view->allNews = $allNews;
        $this->view->allItems = $count;
        $cat = @reset($objContentCategory->getByColumns(array('content_category_gid=?' => $categoryGid))->toArray());
        $cat['url'] = Nine_Route::_("content/index/index/cid/{$cat['content_category_gid']}", array('alias' => $cat['alias']));
        $this->view->headTitle($cat ['name']);
        $this->view->name_cate = $cat ['name'];
        $this->view->description = $cat ['description'];
        $this->view->cat = $cat;
        $this->view->firstItemInPage = ($currentPage - 1) * $numRowPerPage + 1;
        $this->view->lastItemInPage = ($currentPage - 1) * $numRowPerPage + count($objContent->getAllEnabledContentByCategory($categoryGid, array()));


        $templatePath .= '/index.' . Nine_Constant::VIEW_SUFFIX;
        $this->view->html = $this->view->render($templatePath);
    }

    /*
     * Content categories
     */

    public function newsAction() {
        try {
            //Get alias
            $alias = $this->_getParam('alias', false);
            $objCat = array();
            if ($alias !== false) {
                //Get paging
                $page = $this->_getParam('page', false);
                $itemPerPage = 10;
                $currentPage = 1;
                if ($page !== false) {
                    $currentPage = $page;
                }
                //Get cat
                $objCat = @reset(Nine_Read::GetListData('Models_ContentCategory', array(
                                    'alias' => $alias
                )));
                $this->view->objCat = $objCat;

                //Get list contents
                $arrContents = Nine_Read::GetListData('Models_Content', array(
                            'content_category_gid' => $objCat['content_category_gid']
                                ), array(), 'detail', $itemPerPage, ($currentPage - 1) * $itemPerPage);
                foreach ($arrContents as &$value) {
                    $tmpCat = @reset(Nine_Read::GetListData('Models_ContentCategory', array(
                        'content_category_gid' => $value['content_category_gid']
                    )));
                    $value['cat'] = $tmpCat['name'];
                }
                unset($value);
                $this->view->arrContents = $arrContents;

                $arrAllContents = Nine_Read::GetListData('Models_Content', array(
                            'content_category_gid' => $objCat['content_category_gid']
                                ), array(), 'detail');

                $this->setPagination($itemPerPage, $currentPage, count($arrAllContents));

                //Get list cats
                $arrCats = Nine_Read::GetListData('Models_ContentCategory', array(
                            'content_category_gid !=?' => $objCat['content_category_gid']
                                ), array(), 'index');
                $this->view->arrCats = $arrCats;


                //Paging info
                $this->view->arrAllContents = count($arrAllContents);
                $this->view->currentPage = $currentPage;
                $this->view->from = $currentPage > 1 ? $itemPerPage * $currentPage - 9 : 1;
                $this->view->to = count($arrAllContents) > $itemPerPage ? $itemPerPage * $currentPage : count($arrAllContents);
            }
            /*
             * Set layout file and template
             */
            Nine_Common::setLayoutAndView($this, $objCat['name'], 'blog', 'index');
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /*
     * Article detail action
     */
    public function newsDetailAction() {
        try {
            //Get alias
            $alias = $this->_getParam('alias', false);
            $objDetail = array();
            $viewFile = 'detail';
            $layout = 'blog-detail';
            if ($alias !== false) {
                
                //Get cat
                $objDetail = @reset(Nine_Read::GetListData('Models_Content', array(
                                    'alias' => $alias
                )));
                $this->view->objDetail = $objDetail;
                //Set view file
                if($objDetail['type'] === '1'){
                    $viewFile = 'recluirse';
                }
                if($objDetail['type'] === '2'){
                    $viewFile = 'certificate';
                    $layout = 'certificate';
                }
                if($objDetail['type'] === '3'){
                    $viewFile = 'why';
                }
                if($objDetail['type'] === '4'){
                    $viewFile = 'progress';
                }
            }
            /*
             * Set layout file and template
             */
            Nine_Common::setLayoutAndView($this, $objDetail['title'], $layout, $viewFile);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /*
     * Get list content in library
     */

    private function GetLibrary($alias, $numRowPerPage, $currentPage) {
        /*
         * Cat detail
         */
        $cat = @reset(Nine_Query::getListContentCategory(array(
            'alias' => $alias
        )));
        $this->view->cat = $cat;

        /*
         * Get list contents
         */
        $allCat = Nine_Query::getListContentCategory(array());


        $listId = Nine_Query::getListContentCategoryId($allCat, $cat['content_category_gid']);

        $allContents = Nine_Query::getListContents(array(
            'content_category_gid IN (?)' => explode(",", $listId)
        ), null, $numRowPerPage, ($currentPage - 1));


        //$allContents = Nine_Query::getListContentsByCatAlias($cat['alias'], $cat['alias'], $numRowPerPage, ($currentPage - 1));

        $allData = Nine_Query::getListContents(array(
            'content_category_gid IN (?)' => explode(",", $listId)
        ));
        /*
         * Get url
         */
        $url = Nine_Route::url();
        /*
         * Get breadcrumbs
         */
        $this->view->breadCrumbs = Nine_Common::getBreadCumbs(-1, 'content', array(
            'name' => $cat['name'],
            'url' => $url['path'] . 'news/' . $alias
        ));
        return array(
            'allContents' => $allContents,
            'allData' => $allData,
            'cat' => $cat
        );
    }

    public function tradeShowAction() {
        $objContent = new Models_Content();
        $objUser = new Models_User();

        $this->setLayout('front-default-category');

        /* Set Pagination */
        $numRowPerPage = 10;
        $currentPage = $this->_getParam("page", false);
        if ($currentPage == false) {
            $currentPage = 1;
        }
        $condition = array(
            'content_category_gid' => 35
        );
        $allTradeShow = $objContent->getAllEnableContent($condition, 'created_date DESC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage);
        foreach ($allTradeShow as &$item) {
            $user = $objUser->getUser($item['user_id']);
            $item['author'] = $user['name_company'];
            $item['author_alias'] = $user['alias'];
            $item['intro_text'] = Nine_Function::subStringAtBlank(trim(strip_tags($item ['full_text'])), 250);
            unset($item);
        }
        /**
         * Count all category
         */
        $objBanner = new Models_Banner();
        $this->view->adv = $objBanner->getAllBanner(array('user_id' => 1, 'banner_category_gid' => 141, 'page' => 4, 'location' => 1), 'sorting');
        $count = count($objContent->getAllEnableContent($condition));
        $this->setPagination($numRowPerPage, $currentPage, $count);

        $this->view->pageTitle = Nine_Language::translate('Trade Show');
        $this->view->allTradeShow = $allTradeShow;
        $this->view->allItems = $count;
        $this->view->firstItemInPage = ($currentPage - 1) * $numRowPerPage + 1;
        $this->view->lastItemInPage = ($currentPage - 1) * $numRowPerPage + count($objContent->getAllEnableContent($condition, 'created_date DESC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage));
    }

    public function tradeShowDetailAction() {
        $objContent = new Models_Content();
        $objUser = new Models_User();

        $this->setLayout('front-default-category');

        $alias = $this->_getParam('alias', false);
        $allContentLangs = current($objContent->setAllLanguages(true)->getByColumns(array('alias = ?' => $alias))->toArray());
        $tradeShow = @$objContent->getContentByGid($allContentLangs['content_gid'])->toArray();
        $user = $objUser->getByUserId($tradeShow['user_id']);
        $newTradeShow = $objContent->getAllEnableContent(array('content_category_gid' => 35), 'created_date DESC', 10);
        $objBanner = new Models_Banner();
        $this->view->adv = $objBanner->getAllBanner(array('user_id' => 1, 'banner_category_gid' => 141, 'page' => 4, 'location' => 1), 'sorting');
        $this->view->pageTitle = $tradeShow['title'];
        $this->view->tradeShow = $tradeShow;
        $this->view->newTradeShow = $newTradeShow;
        $this->view->user = $user;
    }

    public function pageAction() {
        $objContent = new Models_Content();

        $this->setLayout('front-default-category');

        $alias = $this->_getParam('alias', false);
        $allContentLangs = current($objContent->setAllLanguages(true)->getByColumns(array('alias = ?' => $alias))->toArray());
        $page = @$objContent->getContentByGid($allContentLangs['content_gid'])->toArray();

        $page['full_text'] = $page['full_text'] . $page['intro_text'];
        $this->view->pageTitle = $page['title'];
        $this->view->page = $page;

        $objBanner = new Models_Banner();
        $this->view->adv = $objBanner->getAllBanner(array('user_id' => 1, 'banner_category_gid' => 141, 'page' => 4, 'location' => 1), 'sorting');
    }

    public function promotionAction() {
        $objContent = new Models_Content();
        $objUser = new Models_User();
        $objEstoreCategory = new Models_EstoreCategory();

        $aliasEstore = $this->_getParam('alias-estore', false);

        $estore = $objUser->getByAlias($aliasEstore);
        $estoreCategory = $objEstoreCategory->getAllCategoryEnableNoParentByUserId($estore['user_id']);

        $numRowPerPage = 15;
        $currentPage = $this->_getParam("page", false);
        if ($currentPage == false) {
            $currentPage = 1;
        }

        $condition = array(
            'content_category_gid' => 27,
            'user_id' => $estore['user_id']
        );
        $allPromotion = $objContent->getAllEnableContent($condition, 'created_date DESC', $numRowPerPage, ($currentPage - 1) * $numRowPerPage);

        $this->setLayout('front-estore-' . $estore['template']);
        $templatePath = Nine_Registry::getModuleName() . '/views/estore-' . $estore['template'];

        /**
         * Count all category
         */
        $count = count($objContent->getAllEnableContent($condition));
        $this->setPagination($numRowPerPage, $currentPage, $count);

        $this->view->estore = $estore;
        $this->view->estoreCategory = $estoreCategory;
        $this->view->allPromotion = $allPromotion;
        $objBanner = new Models_Banner();
        $this->view->adv = $objBanner->getAllBanner(array('user_id' => 1, 'banner_category_gid' => 141, 'page' => 4, 'location' => 1), 'sorting');
        $templatePath .= '/promotion.' . Nine_Constant::VIEW_SUFFIX;
        $this->view->html = $this->view->render($templatePath);
    }

    public function promotionDetailAction() {
        $objContent = new Models_Content();
        $objUser = new Models_User();
        $objEstoreCategory = new Models_EstoreCategory();

        $aliasEstore = $this->_getParam('alias-estore', false);

        $estore = $objUser->getByAlias($aliasEstore);
        $estoreCategory = $objEstoreCategory->getAllCategoryEnableNoParentByUserId($estore['user_id']);

        $alias = $this->_getParam('alias-promotion', false);
        $allContentLangs = current($objContent->setAllLanguages(true)->getByColumns(array('alias = ?' => $alias))->toArray());
        $promotion = @$objContent->getContentByGid($allContentLangs['content_gid'])->toArray();

        $this->setLayout('front-estore-' . $estore['template']);
        $templatePath = Nine_Registry::getModuleName() . '/views/estore-' . $estore['template'];

        $this->view->estore = $estore;
        $this->view->promotion = $promotion;
        $this->view->estoreCategory = $estoreCategory;
        $objBanner = new Models_Banner();
        $this->view->adv = $objBanner->getAllBanner(array('user_id' => 1, 'banner_category_gid' => 141, 'page' => 4, 'location' => 1), 'sorting');
        $templatePath .= '/promotion-detail.' . Nine_Constant::VIEW_SUFFIX;
        $this->view->html = $this->view->render($templatePath);
    }

    public function aboutUsAction() {
        $objContent = new Models_Content();
        $objUser = new Models_User();
        $objEstoreCategory = new Models_EstoreCategory();

        $aliasEstore = $this->_getParam('alias-estore', false);

        $estore = $objUser->getByAlias($aliasEstore);
        $estoreCategory = $objEstoreCategory->getAllCategoryEnableNoParentByUserId($estore['user_id']);

        if (Nine_Language::getCurrentLangId() == '2') {
            $alias = 'gioi-thieu';
        } else {
            $alias = 'about-us';
        }
        $objBanner = new Models_Banner();
        $this->view->adv = $objBanner->getAllBanner(array('user_id' => $estore['user_id'], 'banner_category_gid' => 141, 'page' => 4, 'location' => $estore['template'] + 1), 'sorting');
        $about = $objContent->getContentByAlias($alias, $estore['user_id']);
        $this->setLayout('front-estore-' . $estore['template']);
        $templatePath = Nine_Registry::getModuleName() . '/views/estore-' . $estore['template'];

        $this->view->estore = $estore;
        $this->view->about = $about;
        $this->view->estoreCategory = $estoreCategory;

        $templatePath .= '/about-us.' . Nine_Constant::VIEW_SUFFIX;
        $this->view->html = $this->view->render($templatePath);
    }

    public function serviceAction() {
        $objContent = new Models_Content();
        $objUser = new Models_User();
        $objEstoreCategory = new Models_EstoreCategory();

        $aliasEstore = $this->_getParam('alias-estore', false);

        $estore = $objUser->getByAlias($aliasEstore);
        $estoreCategory = $objEstoreCategory->getAllCategoryEnableNoParentByUserId($estore['user_id']);

        if (Nine_Language::getCurrentLangId() == '2') {
            $alias = 'dich-vu';
        } else {
            $alias = 'service';
        }
        $objBanner = new Models_Banner();
        $this->view->adv = $objBanner->getAllBanner(array('user_id' => $estore['user_id'], 'banner_category_gid' => 141, 'page' => 4, 'location' => $estore['template'] + 1), 'sorting');
        $service = $objContent->getContentByAlias($alias, $estore['user_id']);
        $this->setLayout('front-estore-' . $estore['template']);
        $templatePath = Nine_Registry::getModuleName() . '/views/estore-' . $estore['template'];

        $this->view->estore = $estore;
        $this->view->service = $service;
        $this->view->estoreCategory = $estoreCategory;

        $templatePath .= '/service.' . Nine_Constant::VIEW_SUFFIX;
        $this->view->html = $this->view->render($templatePath);
    }

}
