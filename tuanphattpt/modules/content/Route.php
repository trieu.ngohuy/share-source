<?php

class Route_Content
{
    /**
     * Build friendly URL
     * @param array $linkArr 0 => <module>, 1 => controller, 2 => action, 3...n param/value
     * @param array $params param => value
     *
     * @return string friendly URL
     */
    public function build($linkArr, $params = array())
    {
        $result = implode('/', $linkArr);
        if ('detail' == @$linkArr[2]) {
            if(Nine_Language::getCurrentLangCode() == 'vi'){
                $result = "chi-tiet-tin-tuc/";
            }else{
                $result = "new-detail/";
            }
            if (null != $params['alias']) {
                $result .= urlencode($params['alias']);
            }

        } else if ('news' == @$linkArr[2] || 'index' == @$linkArr[2]) {
            /**
             * Check current language
             */
            if(Nine_Language::getCurrentLangCode() == 'vi'){
                $result = "danh-muc-tin-tuc/";
            }else{
                $result = "news/";
            }
            
            if (null != @$params['alias']) {
                $result .= urlencode($params['alias']);
            }

        }
        return $result;
    }

    /**
     * Parse friendly URL
     */
    public function parse()
    {
        $router = Nine_Controller_Front::getInstance()->getRouter();
        // $route = new Zend_Controller_Router_Route_Regex(
        //     'news-page/(.*)',
        //     array(
        //         'module' => 'content',
        //         'controller' => 'index',
        //         'action' => 'index'
        //     ),
        //     array(1 => 'alias')
        // );
        // $router->addRoute('content', $route);

        /*MAIN ROUTE*/
//        $routeCategory = new Zend_Controller_Router_Route_Regex(
//            'news-page/(.*)',
//            array(
//                'module' => 'content',
//                'controller' => 'index',
//                'action' => 'news'
//            ),
//            array(
//                1 => 'alias'
//            )
//        );
//        $router->addRoute('routeCategory', $routeCategory);

        $routeEStoreCategory = new Zend_Controller_Router_Route_Regex(
            '(.*)/news',
            array(
                'module' => 'content',
                'controller' => 'index',
                'action' => 'news'
            ),
            array(
                1 => 'alias-estore',
                2 => 'alias-news'
            )
        );
        $router->addRoute('routeEStoreCategory', $routeEStoreCategory);


        /*
         * News detail (En)
         */
        $contentDetailEn = new Zend_Controller_Router_Route_Regex(
            'new-detail/(.*)',
            array(
                'module' => 'content',
                'controller' => 'index',
                'action' => 'news-detail'
            ),
            array(1 => 'alias')
        );
        $router->addRoute('contentDetailEn', $contentDetailEn);
        /*
         * News detail (Vi)
         */
        $contentDetailVi = new Zend_Controller_Router_Route_Regex(
            'chi-tiet-tin-tuc/(.*)',
            array(
                'module' => 'content',
                'controller' => 'index',
                'action' => 'news-detail'
            ),
            array(1 => 'alias')
        );
        $router->addRoute('contentDetailVi', $contentDetailVi);

        $route2 = new Zend_Controller_Router_Route_Regex(
            'estore/manage-news/(.*).html',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'manage-news'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('content2', $route2);

        $route4 = new Zend_Controller_Router_Route_Regex(
            'estore/edit-news/(.*)/([0-9]+)',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'edit-news'
            ),
            array(
                1 => 'alias',
                2 => 'gid'
            )
        );
        $router->addRoute('content4', $route4);

        $route5 = new Zend_Controller_Router_Route_Regex(
            'estore/edit-news/(.*)/([0-9]+)/([0-9]+)',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'edit-news'
            ),
            array(
                1 => 'alias',
                2 => 'gid',
                3 => 'lid'
            )
        );
        $router->addRoute('content5', $route5);

        $route6 = new Zend_Controller_Router_Route_Regex(
            'estore/new-news/(.*).html',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'new-news'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('content6', $route6);

        $route7 = new Zend_Controller_Router_Route_Regex(
            'estore/enable-hot-content/([0-9]+)/(.*).html',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'tinnhanh-content'
            ),
            array(
                1 => 'gid',
                2 => 'alias'
            )
        );
        $router->addRoute('content7', $route7);

        $route8 = new Zend_Controller_Router_Route_Regex(
            'estore/disable-hot-content/([0-9]+)/(.*).html',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'distinnhanh-content'
            ),
            array(
                1 => 'gid',
                2 => 'alias'
            )
        );
        $router->addRoute('content8', $route8);

        $route9 = new Zend_Controller_Router_Route_Regex(
            'estore/disable-content/(.*)/(.*)',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'disable-content'
            ),
            array(
                1 => 'alias',
                2 => 'gid'
            )
        );
        $router->addRoute('content9', $route9);

        $route10 = new Zend_Controller_Router_Route_Regex(
            'estore/disable-content/(.*)/(.*)/([0-9]+)',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'disable-content'
            ),
            array(
                1 => 'alias',
                2 => 'gid',
                3 => 'lid'
            )
        );
        $router->addRoute('content10', $route10);

        $route11 = new Zend_Controller_Router_Route_Regex(
            'estore/enable-content/(.*)/(.*)',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'enable-content'
            ),
            array(
                1 => 'alias',
                2 => 'gid'
            )
        );
        $router->addRoute('content11', $route11);

        $route12 = new Zend_Controller_Router_Route_Regex(
            'estore/enable-content/(.*)/(.*)/([0-9]+)',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'enable-content'
            ),
            array(
                1 => 'alias',
                2 => 'gid',
                3 => 'lid'
            )
        );
        $router->addRoute('content12', $route12);

        $route13 = new Zend_Controller_Router_Route_Regex(
            'estore/delete-news/(.*)/(.*)',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'delete-news'
            ),
            array(
                1 => 'alias',
                2 => 'gid',
            )
        );
        $router->addRoute('content13', $route13);

        $route14 = new Zend_Controller_Router_Route_Regex(
            'estore/manage-promotion/(.*).html',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'manage-promotion'
            ),
            array(
                1 => 'alias',
            )
        );
        $router->addRoute('content14', $route14);

        $route15 = new Zend_Controller_Router_Route_Regex(
            'estore/new-promotion/(.*).html',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'new-promotion'
            ),
            array(
                1 => 'alias',
            )
        );
        $router->addRoute('content15', $route15);

        $route16 = new Zend_Controller_Router_Route_Regex(
            'estore/edit-promotion/(.*)/([0-9]+)',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'edit-promotion'
            ),
            array(
                1 => 'alias',
                2 => 'gid'
            )
        );
        $router->addRoute('content16', $route16);

        $route17 = new Zend_Controller_Router_Route_Regex(
            'estore/edit-promotion/(.*)/([0-9]+)/([0-9]+)',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'edit-promotion'
            ),
            array(
                1 => 'alias',
                2 => 'gid',
                3 => 'lid'
            )
        );
        $router->addRoute('content17', $route17);

        $route18 = new Zend_Controller_Router_Route_Regex(
            'estore/delete-promotion/(.*)/(.*)',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'delete-promotion'
            ),
            array(
                1 => 'alias',
                2 => 'gid',
            )
        );
        $router->addRoute('content18', $route18);

        $route19 = new Zend_Controller_Router_Route_Regex(
            'estore/manage-estore-page/(.*).html',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'manage-estore-page'
            ),
            array(
                1 => 'alias',
            )
        );
        $router->addRoute('content19', $route19);

        $route20 = new Zend_Controller_Router_Route_Regex(
            'estore/edit-estore-page/(.*)/([0-9]+)',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'edit-estore-page'
            ),
            array(
                1 => 'alias',
                2 => 'gid',
            )
        );
        $router->addRoute('content20', $route20);

        $route21 = new Zend_Controller_Router_Route_Regex(
            'estore/edit-estore-page/(.*)/([0-9]+)/([0-9]+)',
            array(
                'module' => 'content',
                'controller' => 'estore',
                'action' => 'edit-estore-page'
            ),
            array(
                1 => 'alias',
                2 => 'gid',
                3 => 'lid'
            )
        );
        $router->addRoute('content21', $route21);

        $route22 = new Zend_Controller_Router_Route_Regex(
            'news',
            array(
                'module' => 'content',
                'controller' => 'index',
                'action' => 'news'
            )
        );
        $router->addRoute('content22', $route22);

        /*
         * Content category (En)
         */
        $contentCatEn = new Zend_Controller_Router_Route_Regex(
            'news/(.*)',
            array(
                'module' => 'content',
                'controller' => 'index',
                'action' => 'news'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('contentCatEn', $contentCatEn);
        /*
         * Content category (En)
         */
        $contentCatVi = new Zend_Controller_Router_Route_Regex(
            'danh-muc-tin-tuc/(.*)',
            array(
                'module' => 'content',
                'controller' => 'index',
                'action' => 'news'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('contentCatVi', $contentCatVi);

        $route24 = new Zend_Controller_Router_Route_Regex(
            'trade-show',
            array(
                'module' => 'content',
                'controller' => 'index',
                'action' => 'trade-show'
            )
        );
        $router->addRoute('content24', $route24);

        $route25 = new Zend_Controller_Router_Route_Regex(
            'trade-show/(.*)',
            array(
                'module' => 'content',
                'controller' => 'index',
                'action' => 'trade-show-detail'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('content25', $route25);

        $route26 = new Zend_Controller_Router_Route_Regex(
            'page/(.*)',
            array(
                'module' => 'content',
                'controller' => 'index',
                'action' => 'page'
            ),
            array(
                1 => 'alias'
            )
        );
        $router->addRoute('content26', $route26);


        $route28 = new Zend_Controller_Router_Route_Regex(
            '(.*)/news/(.*)',
            array(
                'module' => 'content',
                'controller' => 'index',
                'action' => 'news-detail'
            ),
            array(
                1 => 'alias-estore',
                2 => 'alias-news'
            )
        );
        $router->addRoute('content28', $route28);

        $route29 = new Zend_Controller_Router_Route_Regex(
            '(.*)/promotion',
            array(
                'module' => 'content',
                'controller' => 'index',
                'action' => 'promotion'
            ),
            array(
                1 => 'alias-estore'
            )
        );
        $router->addRoute('content29', $route29);

        $route30 = new Zend_Controller_Router_Route_Regex(
            '(.*)/promotion/(.*)',
            array(
                'module' => 'content',
                'controller' => 'index',
                'action' => 'promotion-detail'
            ),
            array(
                1 => 'alias-estore',
                2 => 'alias-promotion'
            )
        );
        $router->addRoute('content30', $route30);

//        $route31 = new Zend_Controller_Router_Route_Regex(
//            '(.*)/about-us',
//            array(
//                'module' => 'content',
//                'controller' => 'index',
//                'action' => 'about-us'
//            ),
//            array(
//                1 => 'alias-estore'
//            )
//        );
//        $router->addRoute('content31', $route31);

        $route32 = new Zend_Controller_Router_Route_Regex(
            '(.*)/service',
            array(
                'module' => 'content',
                'controller' => 'index',
                'action' => 'service'
            ),
            array(
                1 => 'alias-estore'
            )
        );
        $router->addRoute('content32', $route32);
    }
}