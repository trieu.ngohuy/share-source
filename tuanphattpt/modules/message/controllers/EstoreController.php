<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/content/models/Content.php';
require_once 'modules/message/models/MessageCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/user/models/User.php';

class message_EstoreController extends Nine_Controller_Action
{
    public function manageMessageAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $type = $this->_getParam('type', null);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
        }

        $this->setLayout('estore');

        $objLang    = new Models_Lang();
        $objCategory     = new Models_MessageCategory();


        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_message', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $this->view->headTitle(Nine_Language::translate('manage Category '));


        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);

        if($currentPage == false){
            $currentPage = 1;
            $this->session->categoryDisplayNum = null;
            $this->session->categoryCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objCategory->update(array('sorting' => $value), array('message_category_gid=?' => $id));
            $this->session->categoryMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("Edit sort numbers successfully")
            );
        }

        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->categoryDisplayNum;
        } else {
            $this->session->categoryDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->categoryCondition;
        } else {
            $this->session->categoryCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }

        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_message', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        $condition['user_to_id'] = $user['user_id'];
        $condition['user_from_id'] = $user['user_id'];
        $condition['parent_id'] = 0;
        $condition['type'] = 1;
        /**
         * Get all categorys
         */
        $allCategories  = $objCategory->setAllLanguages(true)->getAllCategories($condition, array('message_category_gid DESC'),
            $numRowPerPage,
            ($currentPage - 1) * $numRowPerPage
        );
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Count all categorys
         */
        $count = count($objCategory->setAllLanguages(true)->getAllCategories($condition));
        /**
         * Format
         */

        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allCategories[0]['message_category_gid'];
        foreach ($allCategories as $index=>$category) {
            /**
             * Change date
             */
        	if( ($category['user_from_id'] == Nine_Registry::getLoggedInUserId() && $category['user_view'] == 0 ) || ($category['user_to_id'] == Nine_Registry::getLoggedInUserId() && $category['admin_view'] == 0 ) ){
            	$category['view'] = 0;
            }else{
            	$category['view'] = 1;
            }
            if (0 != $category['created_date']) {
                $category['created_date'] = date($config['dateFormat'], $category['created_date']);
            } else {
                $category['created_date'] = '';
            }
            if ($tmpGid != $category['message_category_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $category['message_category_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $category;
            }
            $tmp2['langs'][]  = $category;
            /**
             * Final element
             */
            if ($index == count($allCategories) - 1) {
                $tmp[] = $tmp2;
            }

        }

        $allCategories = $tmp;
        $export = $this->_getParam('export', false);
        if($export != false){
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $style = "style = 'border: 1px solid'";

            if(Nine_Language::getCurrentLangId() == 1){
                $textHeader = array(
                    "<b>No</b>",
                    "<b>Country</b>",
                    "<b>User From</b>",
                    "<b>User To</b>",
                    "<b>Email</b>",
                    "<b>Create</b>",
                    "<b>Title</b>"
                );
            }else{
                $textHeader = array(
                    mb_convert_encoding("<b>S? Th? T?</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Qu?c Gia</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ng�?i G?i</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Gian H�ng Nh?n</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Email</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ng�y T?o</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ti�u �?</b>",'HTML-ENTITIES','UTF-8'),
                );
            }

            $header = "<tr>";
            foreach ($textHeader as $text) {
                $header .= "<td $style>" . utf8_encode($text) . "</td>";
            }
            $header .= "</tr>";

            $content = '';
            $no = 1;

            foreach ($allCategories as $item) {
                $content .="<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['quocgia'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['username_from'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['username_to'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['email'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['name'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .="</tr>";
                $no++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-message-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();

        }
//        echo print_r($allCategories);die;
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCategories = $allCategories;
        $this->view->categoryMessage = $this->session->categoryMessage;
        $this->session->categoryMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage/2;
        $this->view->fullPermisison = $this->checkPermission('edit_message');
        $this->view->allLangs = $allLangs;

        $this->view->menu = array(
            0=>'estore-message',
            1=>'manager-message',
            2=>'manager-message'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-folder',
                'url'	=>	Nine_Registry::getBaseUrl().'estore/manage-message/' . $user['alias'] . '.html',
                'name'	=>	Nine_Language::translate('Manager Category Message')
            )

        );

        $allCats = $objCategory->buildTree($objCategory->getAllCategoriesParentNull());
        $this->view->allCats = $allCats;
        $this->view->alias = $user['alias'] . '.html';
        $this->view->username = $user['alias'];
    }

    public function editMessageAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
        }

        $this->setLayout('estore');

        $objCategory     = new Models_MessageCategory();
        $objLang    = new Models_Lang();
        $objContent = new Models_Content();
        $objUser = new Models_User();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_message', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false);
        if (false == $gid) {
            $this->_redirect('estore/manage-message/' . $user['alias'] . '.html');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_message', null, '*'))
            ||  (false != $lid && false == $this->checkPermission('edit_message', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data   = $this->_getParam('data', false);


        /**
         * Get all category languages
         */

        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allCategoryLangs = $objCategory->setAllLanguages(true)->getByColumns(array('message_category_gid=?' => $gid))->toArray();
//        echo "<pre>";print_r($allCategoryLangs);die;
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_message', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allCategoryLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }

        $errors = array();
        if (false !== $data) {

            $description = $this->_getParam('description', false);
//            var_dump($description);die;
            /**
             * Insert new category
             */
            $newCategory = $data;

            try {
				
                	$enable = $data['enabled'];
                if (null != $description) {
                    $userSession = Nine_Registry::getLoggedInUser();
                    $newCategory = $data;
                    $newCategory['parent_id'] = $gid;
                    $newCategory['description'] = $description;
                    $newCategory['created_date'] = time();
                    $newCategory[1]['description'] = $description;
                    $newCategory[2]['description'] = $description;

                    if(isset($_FILES['images']) && $_FILES['images']['tmp_name'] != '') {
                        $fileName = basename($_FILES['images']['name']);
                        $fileTmp = $_FILES['images']['tmp_name'];
                        $uploadPath = 'media/userfiles/user/' . $userSession['user_id'] . '/';
                        $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                        $uploadFile = $uploadDir . $fileName;

                        $ext_allow = array (
                            'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX'
                        );
                        $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
                        if(in_array($ext, $ext_allow)) {
                            move_uploaded_file($fileTmp, $uploadFile);
                            $newCategory['images'] = $uploadPath . $fileName;
                        }
                    }
					
					
					
            
               	 	
                    $objCategory->insert($newCategory);
                    $this->session->contactMessage = array(
                        'success' => true,
                        'message' => Nine_Language::translate('Contact is created successfully.')
                    );
                } else {
                    /**
                     * Message
                     */
                    $this->session->contactMessage = array(
                        'success' => true,
                        'message' => Nine_Language::translate('Not yet reply.')
                    );
                }
                $updated_date = time();
           	 $data = current($allCategoryLangs);
	                if($data['user_from_id'] == Nine_Registry::getLoggedInUserId()){
	                	
	                	$newCategory['user_from_id'] = Nine_Registry::getLoggedInUserId();
                    	$newCategory['type_contact'] = 1;
	                	$objCategory->update(array('updated_date' => $updated_date,'admin_view'=> 0,'enabled'=>$enable), array('message_category_gid =?' => $gid));
		            }else{
                    	$newCategory['type_contact'] = 2;
		            	$newCategory['user_to_id'] = Nine_Registry::getLoggedInUserId();
		            	$objCategory->update(array('updated_date' => $updated_date,'user_view'=> 0,'enabled'=>$enable), array('message_category_gid =?' => $gid));
		            }
				
                $this->_redirect('estore/manage-message/' . $user['alias'] . '.html');
            } catch (Exception $e) {
                echo '<pre>';
                echo print_r($e);
                echo '<pre>';
                die;
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allCategoryLangs);
            if (false == $data) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("Category doesn't exist.")
                );
                $this->_redirect('estore/manage-message/' . $user['alias'] . '.html');
            }
            /**
             * Get all lang categorys
             */
            foreach ($allCategoryLangs as $category) {
                $data[$category['lang_id']] = $category;
            }

            /**
             * Get all child category
             */
            if($data['user_from_id'] == Nine_Registry::getLoggedInUserId()){
            	$objCategory->update(array('user_view' => 1), array('message_category_gid =?' => $gid));
            }else{
            	$objCategory->update(array('admin_view' => 1), array('message_category_gid =?' => $gid));
            }
            
        }
        $user_to = $objUser->getByUserId($data['user_to_id']);
        try {
        	$user_form = $objUser->getByUserId($data['user_from_id']);
        	
        } catch (Exception $e) {
        	echo '<pre>';
        	echo print_r($e);
        	echo '</pre>';die;
        }
        $data['user_to'] = $user_to;

        /**
         * Remove it and its childs from category list
         */
        $oldData = @reset($allCategoryLangs);
        $condition['parent_id'] = $gid;
        $allMessage = $objCategory->getAllMessage($condition);
        /**
         * Prepare for template
         */

        $this->view->allCats = $allMessage;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->user_form = $user_form;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Category'));
        $this->view->fullPermisison = $this->checkPermission('edit_message', null, '*');
        $this->view->menu = array(
            0=>'estore-message',
            1=>'manager-message',
            2=>'manager-message'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-folder',
                'url'	=>	Nine_Registry::getBaseUrl().'estore/manage-message/' . $user['alias'] . '.html',
                'name'	=>	Nine_Language::translate('Manager Message')
            ),
            1=>array(
                'icon' 	=> 	'fa-pencil',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('Edit Message')
            )

        );

        $this->view->alias = $user['alias'] . '.html';
    }
    
    
    
    
    
    
    
public function manageMessageEstoreAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $type = $this->_getParam('type', null);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
        }

        $this->setLayout('estore');

        $objLang    = new Models_Lang();
        $objCategory     = new Models_MessageCategory();


        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_message', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $this->view->headTitle(Nine_Language::translate('manage Category '));


        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);

        if($currentPage == false){
            $currentPage = 1;
            $this->session->categoryDisplayNum = null;
            $this->session->categoryCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objCategory->update(array('sorting' => $value), array('message_category_gid=?' => $id));
            $this->session->categoryMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("Edit sort numbers successfully")
            );
        }

        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->categoryDisplayNum;
        } else {
            $this->session->categoryDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->categoryCondition;
        } else {
            $this->session->categoryCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }

        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_message', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        $condition['user_to_id'] = $user['user_id'];
        $condition['user_from_id'] = $user['user_id'];
        $condition['parent_id'] = 0;
        $condition['type'] = 2;
        /**
         * Get all categorys
         */
        $allCategories  = $objCategory->setAllLanguages(true)->getAllCategories($condition, array('message_category_gid DESC'),
            $numRowPerPage,
            ($currentPage - 1) * $numRowPerPage
        );
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Count all categorys
         */
        $count = count($objCategory->setAllLanguages(true)->getAllCategories($condition));
        /**
         * Format
         */

        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allCategories[0]['message_category_gid'];
        foreach ($allCategories as $index=>$category) {
            /**
             * Change date
             */
        	if( ($category['user_from_id'] == Nine_Registry::getLoggedInUserId() && $category['user_view'] == 0 ) || ($category['user_to_id'] == Nine_Registry::getLoggedInUserId() && $category['admin_view'] == 0 ) ){
            	$category['view'] = 0;
            }else{
            	$category['view'] = 1;
            }
            if (0 != $category['created_date']) {
                $category['created_date'] = date($config['dateFormat'], $category['created_date']);
            } else {
                $category['created_date'] = '';
            }
            if ($tmpGid != $category['message_category_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $category['message_category_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $category;
            }
            $tmp2['langs'][]  = $category;
            /**
             * Final element
             */
            if ($index == count($allCategories) - 1) {
                $tmp[] = $tmp2;
            }

        }

        $allCategories = $tmp;
        $export = $this->_getParam('export', false);
        if($export != false){
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $style = "style = 'border: 1px solid'";

            if(Nine_Language::getCurrentLangId() == 1){
                $textHeader = array(
                    "<b>No</b>",
                    "<b>Country</b>",
                    "<b>User From</b>",
                    "<b>User To</b>",
                    "<b>Email</b>",
                    "<b>Create</b>",
                    "<b>Title</b>"
                );
            }else{
                $textHeader = array(
                    mb_convert_encoding("<b>S? Th? T?</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Qu?c Gia</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ng�?i G?i</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Gian H�ng Nh?n</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Email</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ng�y T?o</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ti�u �?</b>",'HTML-ENTITIES','UTF-8'),
                );
            }

            $header = "<tr>";
            foreach ($textHeader as $text) {
                $header .= "<td $style>" . utf8_encode($text) . "</td>";
            }
            $header .= "</tr>";

            $content = '';
            $no = 1;

            foreach ($allCategories as $item) {
                $content .="<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['quocgia'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['username_from'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['username_to'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['email'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['name'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .="</tr>";
                $no++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-message-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();

        }
//        echo print_r($allCategories);die;
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCategories = $allCategories;
        $this->view->categoryMessage = $this->session->categoryMessage;
        $this->session->categoryMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage/2;
        $this->view->fullPermisison = $this->checkPermission('edit_message');
        $this->view->allLangs = $allLangs;

        $this->view->menu = array(
            0=>'estore-message',
            1=>'manager-message-hoihang',
            2=>'manager-message'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-folder',
                'url'	=>	Nine_Registry::getBaseUrl().'estore/manage-message-estore/' . $user['alias'] . '.html',
                'name'	=>	Nine_Language::translate('Manager Category Message')
            )

        );

        $allCats = $objCategory->buildTree($objCategory->getAllCategoriesParentNull());
        $this->view->allCats = $allCats;
        $this->view->alias = $user['alias'] . '.html';
        $this->view->username = $user['alias'];
    }

    public function editMessageEstoreAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
        }

        $this->setLayout('estore');

        $objCategory     = new Models_MessageCategory();
        $objLang    = new Models_Lang();
        $objContent = new Models_Content();
        $objUser = new Models_User();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_message', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false);
        if (false == $gid) {
            $this->_redirect('estore/manage-message-estore/' . $user['alias'] . '.html');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_message', null, '*'))
            ||  (false != $lid && false == $this->checkPermission('edit_message', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data   = $this->_getParam('data', false);


        /**
         * Get all category languages
         */

        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allCategoryLangs = $objCategory->setAllLanguages(true)->getByColumns(array('message_category_gid=?' => $gid))->toArray();
//        echo "<pre>";print_r($allCategoryLangs);die;
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_message', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allCategoryLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }

        $errors = array();
        if (false !== $data) {

            $description = $this->_getParam('description', false);
//            var_dump($description);die;
            /**
             * Insert new category
             */
            $newCategory = $data;

            try {
				
                	
                $enable = $data['enabled'];
                if (null != $description) {
                    $userSession = Nine_Registry::getLoggedInUser();
                    $newCategory = $data;
                    $newCategory['parent_id'] = $gid;
                    $newCategory['type'] = 2;
                    $newCategory['description'] = $description;
                    $newCategory['created_date'] = time();
                    $newCategory[1]['description'] = $description;
                    $newCategory[2]['description'] = $description;

                    if(isset($_FILES['images']) && $_FILES['images']['tmp_name'] != '') {
                        $fileName = basename($_FILES['images']['name']);
                        $fileTmp = $_FILES['images']['tmp_name'];
                        $uploadPath = 'media/userfiles/user/' . $userSession['user_id'] . '/';
                        $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                        $uploadFile = $uploadDir . $fileName;

                        $ext_allow = array (
                            'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX'
                        );
                        $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
                        if(in_array($ext, $ext_allow)) {
                            move_uploaded_file($fileTmp, $uploadFile);
                            $newCategory['images'] = $uploadPath . $fileName;
                        }
                    }
					
	                
                    $objCategory->insert($newCategory);
                    
                    $this->session->contactMessage = array(
                        'success' => true,
                        'message' => Nine_Language::translate('Contact is created successfully.')
                    );
                } else {
                    /**
                     * Message
                     */
                    $this->session->contactMessage = array(
                        'success' => true,
                        'message' => Nine_Language::translate('Not yet reply.')
                    );
                }
                $updated_date = time();
            		$data = current($allCategoryLangs);
	                if($data['user_from_id'] == Nine_Registry::getLoggedInUserId()){
	                	
	                	$newCategory['user_from_id'] = Nine_Registry::getLoggedInUserId();
                    	$newCategory['type_contact'] = 1;
	                	$objCategory->update(array('updated_date' => $updated_date,'admin_view'=> 0,'enabled'=>$enable), array('message_category_gid =?' => $gid));
		            }else{
                    	$newCategory['type_contact'] = 2;
		            	$newCategory['user_to_id'] = Nine_Registry::getLoggedInUserId();
		            	$objCategory->update(array('updated_date' => $updated_date,'user_view'=> 0,'enabled'=>$enable), array('message_category_gid =?' => $gid));
		            }
                $this->_redirect('estore/manage-message-estore/' . $user['alias'] . '.html');
            } catch (Exception $e) {
                echo '<pre>';
                echo print_r($e);
                echo '<pre>';
                die;
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allCategoryLangs);
            if (false == $data) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("Category doesn't exist.")
                );
                $this->_redirect('estore/manage-message-estore/' . $user['alias'] . '.html');
            }
            /**
             * Get all lang categorys
             */
            foreach ($allCategoryLangs as $category) {
                $data[$category['lang_id']] = $category;
            }

            /**
             * Get all child category
             */
            if($data['user_from_id'] == Nine_Registry::getLoggedInUserId()){
            	$objCategory->update(array('user_view' => 1), array('message_category_gid =?' => $gid));
            }else{
            	$objCategory->update(array('admin_view' => 1), array('message_category_gid =?' => $gid));
            }
            
        }
        $user_to = $objUser->getByUserId($data['user_to_id']);
        try {
        	$user_form = $objUser->getByUserId($data['user_from_id']);
        	
        } catch (Exception $e) {
        	echo '<pre>';
        	echo print_r($e);
        	echo '</pre>';die;
        }
        $data['user_to'] = $user_to;

        /**
         * Remove it and its childs from category list
         */
        $oldData = @reset($allCategoryLangs);
        $condition['parent_id'] = $gid;
        $allMessage = $objCategory->getAllMessage($condition);
        /**
         * Prepare for template
         */

        $this->view->allCats = $allMessage;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->user_form = $user_form;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Category'));
        $this->view->fullPermisison = $this->checkPermission('edit_message', null, '*');
        $this->view->menu = array(
            0=>'estore-message',
            1=>'manager-message-hoihang',
            2=>'manager-message-hoihang'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-folder',
                'url'	=>	Nine_Registry::getBaseUrl().'estore/manage-message-estore/' . $user['alias'] . '.html',
                'name'	=>	Nine_Language::translate('Manager Message')
            ),
            1=>array(
                'icon' 	=> 	'fa-pencil',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('Edit Message')
            )

        );

        $this->view->alias = $user['alias'] . '.html';
    }
    
    
    
    
    
public function manageMessageSaleAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $type = $this->_getParam('type', null);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
        }

        $this->setLayout('estore');

        $objLang    = new Models_Lang();
        $objCategory     = new Models_MessageCategory();


        /**
         * Check permission
         */
        if (false == $this->checkPermission('see_message', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $this->view->headTitle(Nine_Language::translate('manage Category '));


        $config = Nine_Registry::getConfig();
        $numRowPerPage = Nine_Registry::getConfig("defaultNumberRowPerPage");
        $currentPage = $this->_getParam("page",false);
        $displayNum = $this->_getParam('displayNum', false);

        if($currentPage == false){
            $currentPage = 1;
            $this->session->categoryDisplayNum = null;
            $this->session->categoryCondition = null;
        }
        /**
         * Update sorting
         */
        $data = $this->_getParam('data', array());
        foreach ($data as $id=>$value) {
            $value = intval($value);
            if (1 > $value) {
                continue;
            }
            $objCategory->update(array('sorting' => $value), array('message_category_gid=?' => $id));
            $this->session->categoryMessage = array(
                'success' => true,
                'message' => Nine_Language::translate("Edit sort numbers successfully")
            );
        }

        /**
         * Get number of items per page
         */
        if (false === $displayNum) {
            $displayNum = $this->session->categoryDisplayNum;
        } else {
            $this->session->categoryDisplayNum = $displayNum;
        }
        if (null != $displayNum) {
            $numRowPerPage = $displayNum;
        }
        /**
         * Get condition
         */
        $condition = $this->_getParam('condition', false);
        if (false === $condition) {
            $condition = $this->session->categoryCondition;
        } else {
            $this->session->categoryCondition = $condition;
            $currentPage = 1;
        }
        if (false == $condition) {
            $condition = array();
        }

        /**
         * Get all display languages
         */
        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $index => $lang) {
            if (false == $this->checkPermission('see_message', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
            }
        }
        $condition['user_to_id'] = $user['user_id'];
        $condition['user_from_id'] = $user['user_id'];
        $condition['parent_id'] = 0;
        $condition['type'] = 3;
        /**
         * Get all categorys
         */
        $allCategories  = $objCategory->setAllLanguages(true)->getAllCategories($condition, array('message_category_gid DESC'),
            $numRowPerPage,
            ($currentPage - 1) * $numRowPerPage
        );
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Count all categorys
         */
        $count = count($objCategory->setAllLanguages(true)->getAllCategories($condition));
        /**
         * Format
         */

        $tmp    = array();
        $tmp2   = false;
        $tmpGid = @$allCategories[0]['message_category_gid'];
        foreach ($allCategories as $index=>$category) {
            /**
             * Change date
             */
        	if( ($category['user_from_id'] == Nine_Registry::getLoggedInUserId() && $category['user_view'] == 0 ) || ($category['user_to_id'] == Nine_Registry::getLoggedInUserId() && $category['admin_view'] == 0 ) ){
            	$category['view'] = 0;
            }else{
            	$category['view'] = 1;
            }
            if (0 != $category['created_date']) {
                $category['created_date'] = date($config['dateFormat'], $category['created_date']);
            } else {
                $category['created_date'] = '';
            }
            if ($tmpGid != $category['message_category_gid']) {
                $tmp[]  = $tmp2;
                $tmp2   = false;
                $tmpGid = $category['message_category_gid'];
            }
            if (false === $tmp2) {
                $tmp2        = $category;
            }
            $tmp2['langs'][]  = $category;
            /**
             * Final element
             */
            if ($index == count($allCategories) - 1) {
                $tmp[] = $tmp2;
            }

        }

        $allCategories = $tmp;
        $export = $this->_getParam('export', false);
        if($export != false){
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $style = "style = 'border: 1px solid'";

            if(Nine_Language::getCurrentLangId() == 1){
                $textHeader = array(
                    "<b>No</b>",
                    "<b>Country</b>",
                    "<b>User From</b>",
                    "<b>User To</b>",
                    "<b>Email</b>",
                    "<b>Create</b>",
                    "<b>Title</b>"
                );
            }else{
                $textHeader = array(
                    mb_convert_encoding("<b>S? Th? T?</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Qu?c Gia</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ng�?i G?i</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Gian H�ng Nh?n</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Email</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ng�y T?o</b>",'HTML-ENTITIES','UTF-8'),
                    mb_convert_encoding("<b>Ti�u �?</b>",'HTML-ENTITIES','UTF-8'),
                );
            }

            $header = "<tr>";
            foreach ($textHeader as $text) {
                $header .= "<td $style>" . utf8_encode($text) . "</td>";
            }
            $header .= "</tr>";

            $content = '';
            $no = 1;

            foreach ($allCategories as $item) {
                $content .="<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['quocgia'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['username_from'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['username_to'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item['email'],'HTML-ENTITIES','UTF-8')  . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['created_date'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .= "<td $style>" .  mb_convert_encoding($item['name'],'HTML-ENTITIES','UTF-8') . "</td>";
                $content .="</tr>";
                $no++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-message-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();

        }
//        echo print_r($allCategories);die;
//        echo '<pre>';print_r($allCategories);die;
        /**
         * Set values for tempalte
         */
        $this->setPagination($numRowPerPage, $currentPage, $count);
        $this->view->allCategories = $allCategories;
        $this->view->categoryMessage = $this->session->categoryMessage;
        $this->session->categoryMessage = null;
        $this->view->condition = $condition;
        $this->view->displayNum = $numRowPerPage/2;
        $this->view->fullPermisison = $this->checkPermission('edit_message');
        $this->view->allLangs = $allLangs;

        $this->view->menu = array(
            0=>'estore-message',
            1=>'manager-message-khuyenmai',
            2=>'manager-message'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-folder',
                'url'	=>	Nine_Registry::getBaseUrl().'estore/manage-message-sale/' . $user['alias'] . '.html',
                'name'	=>	Nine_Language::translate('Manager Category Message')
            )

        );

        $allCats = $objCategory->buildTree($objCategory->getAllCategoriesParentNull());
        $this->view->allCats = $allCats;
        $this->view->alias = $user['alias'] . '.html';
        $this->view->username = $user['alias'];
    }

    public function editMessageSaleAction()
    {
        $url = $this->_request->getParams();
        $objUser = new Models_User();
        $user = $objUser->getByAlias($url['alias']);
        $userSession = Nine_Registry::getLoggedInUser();
        $userMessage = null;
        if ($user == null || $user['alias'] != $userSession['alias']) {
            $this->_redirect('/');
        }

        $this->setLayout('estore');

        $objCategory     = new Models_MessageCategory();
        $objLang    = new Models_Lang();
        $objContent = new Models_Content();
        $objUser = new Models_User();
        /**
         * Check permission
         */
        if (false == $this->checkPermission('edit_message', null, '?')) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $gid     = $this->_getParam('gid', false);
        $lid    = $this->_getParam('lid', false);
        if (false == $gid) {
            $this->_redirect('estore/manage-message-sale/' . $user['alias'] . '.html');
        }
        /**
         * Check permission
         */
        if ((false == $lid && false == $this->checkPermission('edit_message', null, '*'))
            ||  (false != $lid && false == $this->checkPermission('edit_message', null, $lid))) {
            $this->_forwardToNoPermissionPage();
            return;
        }

        $data   = $this->_getParam('data', false);


        /**
         * Get all category languages
         */

        $allLangs = $objLang->getAll(array('sorting ASC'))->toArray();
        $allCategoryLangs = $objCategory->setAllLanguages(true)->getByColumns(array('message_category_gid=?' => $gid))->toArray();
//        echo "<pre>";print_r($allCategoryLangs);die;
        /**
         * Check permisison for each language
         */
        foreach ($allLangs as $lang) {
            if (false == $this->checkPermission('edit_message', null, $lang['lang_id'])) {
                /**
                 * Disappear this language
                 */
                unset($allLangs[$index]);
                unset($allCategoryLangs[$lang['lang_id']]);
                unset($data[$lang['lang_id']]);
            }
        }

        $errors = array();
        if (false !== $data) {

            $description = $this->_getParam('description', false);
//            var_dump($description);die;
            /**
             * Insert new category
             */
            $newCategory = $data;

            try {

                if (null != $description) {
                    $userSession = Nine_Registry::getLoggedInUser();
                    $newCategory = $data;
                    $newCategory['parent_id'] = $gid;
                    $newCategory['type_contact'] = 2;
                    $newCategory['type'] = 3;
                    $newCategory['description'] = $description;
                    $newCategory['created_date'] = time();
                    $newCategory[1]['description'] = $description;
                    $newCategory[2]['description'] = $description;

                    if(isset($_FILES['images']) && $_FILES['images']['tmp_name'] != '') {
                        $fileName = basename($_FILES['images']['name']);
                        $fileTmp = $_FILES['images']['tmp_name'];
                        $uploadPath = 'media/userfiles/user/' . $userSession['user_id'] . '/';
                        $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
                        $uploadFile = $uploadDir . $fileName;

                        $ext_allow = array (
                            'doc', 'docx', 'pdf', 'xls', 'xlsx', 'DOC', 'DOCX', 'PDF', 'XLS', 'XLSX'
                        );
                        $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);
                        if(in_array($ext, $ext_allow)) {
                            move_uploaded_file($fileTmp, $uploadFile);
                            $newCategory['images'] = $uploadPath . $fileName;
                        }
                    }

                    $objCategory->insert($newCategory);
                    $this->session->contactMessage = array(
                        'success' => true,
                        'message' => Nine_Language::translate('Contact is created successfully.')
                    );
                } else {
                    /**
                     * Message
                     */
                    $this->session->contactMessage = array(
                        'success' => true,
                        'message' => Nine_Language::translate('Not yet reply.')
                    );
                }
                $updated_date = time();
                $objCategory->update(array('updated_date' => $updated_date,'user_view'=> 0), array('message_category_gid =?' => $gid));
				
                $this->_redirect('estore/manage-message-sale/' . $user['alias'] . '.html');
            } catch (Exception $e) {
                echo '<pre>';
                echo print_r($e);
                echo '<pre>';
                die;
                $errors = array('main' => Nine_Language::translate('Can not insert into database now'));
            }
        } else {
            /**
             * Get old data
             */
            $data = @reset($allCategoryLangs);
            if (false == $data) {
                $this->session->categoryMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("Category doesn't exist.")
                );
                $this->_redirect('estore/manage-message-sale/' . $user['alias'] . '.html');
            }
            /**
             * Get all lang categorys
             */
            foreach ($allCategoryLangs as $category) {
                $data[$category['lang_id']] = $category;
            }

            /**
             * Get all child category
             */
            if($data['user_from_id'] == Nine_Registry::getLoggedInUserId()){
            	$objCategory->update(array('user_view' => 1), array('message_category_gid =?' => $gid));
            }else{
            	$objCategory->update(array('admin_view' => 1), array('message_category_gid =?' => $gid));
            }
            
        }
        $user_to = $objUser->getByUserId($data['user_to_id']);
        try {
        	$user_form = $objUser->getByUserId($data['user_from_id']);
        	
        } catch (Exception $e) {
        	echo '<pre>';
        	echo print_r($e);
        	echo '</pre>';die;
        }
        $data['user_to'] = $user_to;

        /**
         * Remove it and its childs from category list
         */
        $oldData = @reset($allCategoryLangs);
        $condition['parent_id'] = $gid;
        $allMessage = $objCategory->getAllMessage($condition);
        /**
         * Prepare for template
         */

        $this->view->allCats = $allMessage;
        $this->view->allLangs = $allLangs;
        $this->view->datepickerFormat = Nine_Registry::getConfig('datepickerFormat');
        $this->view->lid = $lid;
        $this->view->user_form = $user_form;
        $this->view->errors = $errors;
        $this->view->data = $data;
        $this->view->headTitle(Nine_Language::translate('Edit Category'));
        $this->view->fullPermisison = $this->checkPermission('edit_message', null, '*');
        $this->view->menu = array(
            0=>'estore-message',
            1=>'manager-message-khuyenmai',
            2=>'manager-message'
        );
        $this->view->breadcrumb = array(
            0=>array(
                'icon' 	=> 	'fa-folder',
                'url'	=>	Nine_Registry::getBaseUrl().'estore/manage-message-sale/' . $user['alias'] . '.html',
                'name'	=>	Nine_Language::translate('Manager Message')
            ),
            1=>array(
                'icon' 	=> 	'fa-pencil',
                'url'	=>	'',
                'name'	=>	Nine_Language::translate('Edit Message')
            )

        );

        $this->view->alias = $user['alias'] . '.html';
    }
}