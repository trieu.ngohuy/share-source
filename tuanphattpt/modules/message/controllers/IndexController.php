<?php

require_once 'libs/Nine/Controller/Action.php';
require_once 'modules/content/models/Content.php';
require_once 'modules/message/models/MessageCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/user/models/User.php';
require_once 'modules/product/models/EstoreCategory.php';

class message_IndexController extends Nine_Controller_Action
{
    public function contactUsAction()
    {
        $objUser = new Models_User();
        $objEstoreCategory = new Models_EstoreCategory();
        $objCategoryMessage = new Models_MessageCategory();

        $aliasEstore = $this->_getParam('alias-estore', false);
        $estore = $objUser->getByAlias($aliasEstore);
        $estoreCategory = $objEstoreCategory->getAllCategoryEnableNoParentByUserId($estore['user_id']);

        $this->setLayout('front-estore-' . $estore['template']);
        $templatePath  = Nine_Registry::getModuleName() . '/views/estore-' . $estore['template'];
    	$user = Nine_Registry::getLoggedInUser();
        if($user != ''){
	        if($user['group_id'] == 4){
	        	$this->view->lock = 1;
	        }
        }else{
        	$this->view->lock = 1;
        }
        $data = $this->_getParam('data', false);
        if(false != $data) {
            $newContact = $data;
            $newContact['created_date'] = time();
            $newContact['type'] = 1;
            $newContact['type_contact'] = 1;
            $newContact['user_to_id'] = $estore['user_id'];
            $newContact['parent_id'] = 0;
            $newContact['enabled'] = 0;
	        $newContact['user_from_id'] = Nine_Registry::getLoggedInUserId();

            try {
                $objCategoryMessage->insert($newContact);
                $this->view->message = array(
                    'success' => true,
                    'message' => Nine_Language::translate('Gửi liên hệ thành công')
                );
            } catch(Exception $e) {
                $this->view->message = array(
                    'success' => false,
                    'message' => Nine_Language::translate('Gửi liên hệ thất bại')
                );
            }
            unset($data);
        }
        $this->view->pageTitle = Nine_Language::translate('Contact Estore');
        $this->view->estore = $estore;
        $this->view->estoreCategory = $estoreCategory;
        $templatePath .= '/contact.' . Nine_Constant::VIEW_SUFFIX;
        $this->view->html = $this->view->render( $templatePath );
    }
}