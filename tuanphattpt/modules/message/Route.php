<?php

class Route_Message
{
    public function parse()
    {
        $router = Nine_Controller_Front::getInstance()->getRouter();
        $route = new Zend_Controller_Router_Route_Regex(
            'estore/manage-message/(.*).html',
            array(
                'module' => 'message',
                'controller' => 'estore',
                'action' => 'manage-message'
            ),
            array(1 => 'alias')
        );
        $router->addRoute('message', $route);

        $route1 = new Zend_Controller_Router_Route_Regex(
            'estore/edit-message/(.*)/([0-9]+)',
            array(
                'module' => 'message',
                'controller' => 'estore',
                'action' => 'edit-message'
            ),
            array(
                1 => 'alias',
                2 => 'gid'
            )
        );
        $router->addRoute('message1', $route1);

        $route2 = new Zend_Controller_Router_Route_Regex(
            '(.*)/contact-us',
            array(
                'module' => 'message',
                'controller' => 'index',
                'action' => 'contact-us'
            ),
            array(
                1 => 'alias-estore',
            )
        );
        $router->addRoute('message2', $route2);
        
        $route3 = new Zend_Controller_Router_Route_Regex(
            'estore/manage-message-estore/(.*).html',
            array(
                'module' => 'message',
                'controller' => 'estore',
                'action' => 'manage-message-estore'
            ),
            array(1 => 'alias')
        );
        $router->addRoute('message3', $route3);

        $route4 = new Zend_Controller_Router_Route_Regex(
            'estore/edit-message-estore/(.*)/([0-9]+)',
            array(
                'module' => 'message',
                'controller' => 'estore',
                'action' => 'edit-message-estore'
            ),
            array(
                1 => 'alias',
                2 => 'gid'
            )
        );
        $router->addRoute('message4', $route4);
        
        $route5 = new Zend_Controller_Router_Route_Regex(
            'estore/manage-message-sale/(.*).html',
            array(
                'module' => 'message',
                'controller' => 'estore',
                'action' => 'manage-message-sale'
            ),
            array(1 => 'alias')
        );
        $router->addRoute('message5', $route5);

        $route6 = new Zend_Controller_Router_Route_Regex(
            'estore/edit-message-sale/(.*)/([0-9]+)',
            array(
                'module' => 'message',
                'controller' => 'estore',
                'action' => 'edit-message-sale'
            ),
            array(
                1 => 'alias',
                2 => 'gid'
            )
        );
        $router->addRoute('message6', $route6);
    }
}