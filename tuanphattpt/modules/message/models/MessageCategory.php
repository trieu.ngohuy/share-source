<?php

require_once 'Nine/Model.php';
class Models_MessageCategory extends Nine_Model
{ 
    /**
     * The primary key column or columns.
     * A compound key should be declared as an array.
     * You may declare a single-column primary key
     * as a string.
     *
     * @var mixed
     */
    protected $_primary = 'message_category_id';
     /**
     * The field name what we use to group all language rows together
     * 
     * @var string
     */
    protected $_groupField = 'message_category_gid';
    /**
     * Let system know this is miltilingual table or not.
     * If this table has multilingual fields, Zend_Db_Table_Select object
     * will be inserted language condition automatically.
     * 
     * @var array
     */
    protected $_multilingualFields = array('name', 'enabled', 'alias', 'description','name_nhan');
    
    /**
     * Constructor.
     *
     * Supported params for $config are:
     * - db              = user-supplied instance of database connector,
     *                     or key name of registry instance.
     * - name            = table name.
     * - primary         = string or array of primary key(s).
     * - rowClass        = row class name.
     * - rowsetClass     = rowset class name.
     * - referenceMap    = array structure to declare relationship
     *                     to parent tables.
     * - dependentTables = array of child tables.
     * - metadataCache   = cache for information from adapter describeTable().
     *
     * @param  mixed $config Array of user-specified config options, or just the Db Adapter.
     * @return void
     */
    public function __construct($config = array())
    {
        $this->_name = $this->_prefix . 'message_category';
        return parent::__construct($config); 
    }

    /**
     * Get all categories with conditions
     * 
     * @param $condition
     * @param $order
     * @param $count
     * @param $offset
     */
    public function getAllCategories($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_name))
                ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->join(array('u' => $this->_prefix . 'user'), 'cc.user_to_id = u.user_id', array('username_to' => 'username','id_to' => 'user_id'))
                ->joinLeft(array('u2' => $this->_prefix . 'user'), 'cc.user_from_id = u2.user_id', array('username_from' => 'username'))
//                ->join(array('u2' => $this->_prefix . 'user'), 'cc.user_from_id = u2.user_id', array('username_from' => 'username','id_from' => 'user_id'))
                ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.message_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
                ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order($order)
                ->limit($count, $offset);
        /**
         * Conditions
         */
    	if (null != @$condition['username']) {
            
            $select->where($this->getAdapter()->quoteInto('u.username LIKE ?', "%{$condition['username']}%" ));
        }
        if (null != @$condition['keyword']) {
            $sql = "SELECT message_category_gid FROM {$this->_name} WHERE " . $this->getAdapter()->quoteInto('name LIKE ?', "%{$condition['keyword']}%");
            $ids = $this->_db->fetchAll($sql);
            if (empty($ids)) {
                return array();
            }
            $idString = '';
            foreach ($ids as $row) {
                $idString .= $row['message_category_gid'] . ',';
            }
            /**
             * Add to select object
             */
            $select->where('cc.message_category_gid IN (' . trim($idString, ',') .')');
        }

        if(null != @$condition['user_to_id'] && null == @$condition['user_from_id']) {
            $select->where('cc.user_to_id=?', $condition['user_to_id']);
        }
    	if(null !== @$condition['user_view']) {
            $select->where('cc.user_view=?', $condition['user_view']);
        }
    	if(null !== @$condition['admin_view']) {
            $select->where('cc.admin_view=?', $condition['admin_view']);
        }
    	if(null != @$condition['user_from_id'] && null == @$condition['user_to_id']) {
            $select->where('cc.user_from_id=?', $condition['user_from_id']);
        }
    	if(null != @$condition['user_from_id'] && null != @$condition['user_to_id']) {
            $select->where('cc.user_from_id= '.$condition['user_from_id'].' or cc.user_to_id='.$condition['user_to_id']);
        }
        if(null !== @$condition['parent_id']) {
            $select->where('cc.parent_id=?', $condition['parent_id']);
        }
    	if(false != @$condition['type']) {
            $select->where('cc.type=?', $condition['type']);
        }
        
        return $this->fetchAll($select)->toArray();
    }
 	public function getAllCategoriesParentNull()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_name))
                ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.message_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
				->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
                ->order("sorting");
        /**
         * Conditions
         */
        
    	 $select->where('cc.parent_id IS NULL');
        
        return $this->fetchAll($select)->toArray();
    }
	public function getAllCategoriesOnparentkey($key)
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('cc' => $this->_name))
                ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
                ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.message_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'));
        /**
         * Conditions
         */
        $select->where('cc.parent_id = ?',$key);
        
        return $this->fetchAll($select)->toArray();
    }
    public function getAllEnabledCategory()
    {
    	$select = $this->select()
                ->where('enabled = 1 AND genabled = 1 AND parent_genabled = 1');
                    
        
		return $this->fetchAll($select)->toArray();       
    }
    
    
    
	public function increaseSorting($startPos = 1, $num = 1)
    {
        $sql = "UPDATE {$this->_name} SET sorting = sorting + " . intval($num) . " WHERE sorting >= " . intval($startPos);
        
        $this->_db->query($sql);
    }
    
    
    public function updateGidString($gid, $gidString) 
    {
    	/**
    	 * Get current update node
    	 */
    	if (null == $gid) {
    		return;
    	}
    	else {
    		$category = @reset($this->getByColumns(array('message_category_gid = ?'	=>	$gid))->toArray());
    		$this->update(array('gid_string'	=> $this->concatGidString($category['gid_string'],$gidString)),array('message_category_gid = ?' => $gid));
    		return $this->updateGidString($category['parent_id'],$gidString);
    	}
    }
    
    public function deleteGidString($gid, $gidString)
    {
    	/**
    	 * Get current update node
    	 */
    	if (null == $gid){
    		return;
    	}
    	else {
    		$category = @reset($this->getByColumns(array('message_category_gid = ?'	=>	$gid))->toArray());
    		$this->update(array('gid_string' => $this->removeGidString($category['gid_string'],$gidString)), array('message_category_gid = ?' => $gid));
    		return $this->deleteGidString($category['parent_id'], $gidString);
    	}
    }
    
    
    public function concatGidString($oldGidStr, $concatGidStr) 
    {
    	if(null == $oldGidStr){
    		return $concatGidStr;
    	}
    	
    	$oldGidString = explode(',', trim($oldGidStr,','));
    	$concatGidString = explode(',', trim($concatGidStr,','));
    	
    	foreach($concatGidString as $item){
    		if (false == in_array($item, $oldGidString)){
    			$oldGidString[] = $item;
    		}
    	}
    	return implode(',', $oldGidString);
    }
    
    private function removeGidString($oldGidStr, $removedGidStr)
    {
    	if(null == $oldGidStr){
    		return null;
    	}
    	
    	$oldGidString = explode(',', trim($oldGidStr,','));
    	$removedGidString = explode(',', trim($removedGidStr,','));
    	
//    	echo "<pre>";print_r($removedGidString);die; 
    	foreach($oldGidString as $key => $item){
    		if (false != in_array($item, $removedGidString)){
    			unset($oldGidString[$key]);
    		}
    	}
//    	echo "<pre>";print_r($oldGidString);die; 
    	return implode(',', $oldGidString);
    }

    public function getAllMessage($condition = array(), $order = null, $count = null, $offset = null)
    {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('cc' => $this->_name))
            ->join(array('l' => $this->_prefix . 'lang'), 'cc.lang_id = l.lang_id', array('lname' => 'name', 'lang_image'))
            ->joinLeft(array('cc2' => $this->_name), 'cc.parent_id = cc2.message_category_gid AND cc2.lang_id='. Nine_Language::getCurrentLangId(), array('parent' => 'name'))
            ->where('cc.lang_id=?', Nine_Language::getCurrentLangId())
            ->order($order)
            ->limit($count, $offset);
        /**
         * Conditions
         */

        if (null != @$condition['parent_id']) {

            $select->where('cc.parent_id = '.$condition['parent_id'].' OR cc.message_category_gid = '.$condition['parent_id']);
        }

        return $this->fetchAll($select)->toArray();
    }
}