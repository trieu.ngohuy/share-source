<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/ckfinder.js"></script>
<script src="{{$LAYOUT_HELPER_URL}}admin/js/jquery.slug.js"></script>

<script type="text/javascript">
    //<![CDATA[

    jQuery(document).ready(function (){
        CKFinder.setupCKEditor( null, '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/' );
        jQuery( "#images" ).sortable();
        jQuery( "#images" ).disableSelection();
        //Make slug
        {{foreach from=$allLangs item=item name=langDiv}}
        jQuery('#name{{$smarty.foreach.langDiv.iteration}}').makeSlug({
            slug: jQuery('#alias{{$smarty.foreach.langDiv.iteration}}')
        });
        {{/foreach}}
        //Display images
        jQuery(".input_image[value!='']").parent().find('div').each( function (index, element){
            jQuery(this).toggle();
        });
    });
    var imgId;
    function chooseImage(id)
    {
        imgId = id;
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();
        finder.basePath = '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/'; // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.selectActionFunction = setFileField;
        finder.popup();
    }
    // This is a sample function which is called when a file is selected in CKFinder.
    function setFileField( fileUrl )
    {
        document.getElementById( 'chooseImage_img' + imgId ).src = fileUrl;
        document.getElementById( 'chooseImage_input' + imgId).value = fileUrl;
        document.getElementById( 'chooseImage_div' + imgId).style.display = '';
        document.getElementById( 'chooseImage_noImage_div' + imgId ).style.display = 'none';
    }
    function clearImage(imgId)
    {
        document.getElementById( 'chooseImage_img' + imgId ).src = '';
        document.getElementById( 'chooseImage_input' + imgId ).value = '';
        document.getElementById( 'chooseImage_div' + imgId).style.display = 'none';
        document.getElementById( 'chooseImage_noImage_div' + imgId).style.display = '';
    }

    function addMoreImg()
    {
        jQuery("ul#images > li.hidden").filter(":first").removeClass('hidden');
    }

    //]]>
</script>
<style type="text/css">
    #images { list-style-type: none; margin: 0; padding: 0;}
    #images li { margin: 10px; float: left; text-align: center;  height: 180px;}
</style>


<div class="page-header">
    <h1>
        {{l}}Message Category{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}Edit Message Category{{/l}}
        </small>
    </h1>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">
                {{if $errors|@count > 0}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{if $errors.main}}
                    </strong> {{$errors.main}}
                    {{else}}
                    {{l}}Please check following information again{{/l}}
                    {{/if}}
                </div>
                {{/if}}

                <div class="col-sm-12 widget-container-col ui-sortable">
                    <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                        <div class="col-md-3" >
                            <div class="col-md-12 col-sm-6 widget-container-col">
                                <div class="widget-box">
                                    <div class="widget-header">
                                        <h5 class="widget-title">{{l}}Ticket{{/l}}</h5>
                                    </div>
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div class="form-group has-info">
                                                <label class="col-md-4"  style="color: #669fc7">{{l}}Status{{/l}}</label>
                                                <div class="col-md-8 p70">
                                                    <select name="data[enabled]" class="form-control">
						                                        <option value="0" {{if $data.enabled == 0}}selected="selected"{{/if}}>{{l}}Chờ xử lý{{/l}}</option>
						                                        <option value="1" {{if $data.enabled == 1}}selected="selected"{{/if}}>{{l}}Đang xử lý {{/l}}</option>
						                                        <option value="2" {{if $data.enabled == 2}}selected="selected"{{/if}}>{{l}}Đã xử lý{{/l}}</option>
						                                        <option value="3" {{if $data.enabled == 3}}selected="selected"{{/if}}>{{l}}Đã đóng{{/l}}</option>
													</select>
                                                </div>
                                            </div>
                                            <div class="form-group has-info">
                                                <label class="col-md-4"  style="color: #669fc7">{{l}}Estore{{/l}}</label>
                                                <div class="col-md-8 p70">
                                                    {{$data.user_to.full_name}}
                                                </div>
                                            </div>
                                            <div class="form-group has-info">
                                                <label class="col-md-4"  style="color: #669fc7">{{l}}Created At{{/l}}</label>
                                                <div class="col-md-8 p70">
                                                    {{$data.created_date|date_format:"%d-%m-%Y %H:%M:%S"}}
                                                </div>
                                            </div>
                                            <div class="form-group has-info">
                                                <label class="col-md-4" style="color: #669fc7">{{l}}Updated At{{/l}}</label>
                                                <div class="col-md-8 p70">
                                                    {{$data.updated_date|date_format:"%d-%m-%Y %H:%M:%S"}}
                                                </div>
                                            </div>
                                            <div class="form-group has-info">
                                                <label class="col-md-4" style="color: #669fc7">{{l}}Ticket ID{{/l}}</label>
                                                <div class="col-md-8 p70">
                                                    CT-{{$data.message_category_gid}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-6 widget-container-col">
                                <div class="widget-box">
                                    <div class="widget-header">
                                        <h5 class="widget-title">{{l}}User Details{{/l}}</h5>
                                    </div>
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div class="form-group has-info">
                                                <label class="col-md-4" style="color: #669fc7">{{l}}Người Gửi{{/l}}</label>
                                                <div class="col-md-8 p70">
                                                    {{$data.name_sender}}
                                                </div>
                                            </div>
                                            <div class="form-group has-info">
                                                <label class="col-md-4" style="color: #669fc7">{{l}}Email{{/l}}</label>
                                                <div class="col-md-8 p70">
                                                    {{$data.email}}
                                                </div>
                                            </div>
                                            <div class="form-group has-info">
                                                <label class="col-md-4" style="color: #669fc7">{{l}}Phone{{/l}}</label>
                                                <div class="col-md-8 p70">
                                                    {{$data.phone}}
                                                </div>
                                            </div>
                                            <div class="form-group has-info">
                                                <label class="col-md-4" style="color: #669fc7">{{l}}Company Name{{/l}}</label>
                                                <div class="col-md-8 p70">
                                                    {{$data.name_company}}
                                                </div>
                                            </div>
                                            <div class="form-group has-info">
                                                <label class="col-md-4" style="color: #669fc7">{{l}}Quốc Gia{{/l}}</label>
                                                <div class="col-md-8 p70">
                                                    <img src="{{$BASE_URL}}{{$user_form.ctimage}}" style="width: 17px;margin: -3px 5px 0px 0px;">{{$user_form.ctname}}
                                                </div>
                                            </div>
                                            <div class="form-group has-info">
                                                <label class="col-md-4" style="color: #669fc7">{{l}}Company Address{{/l}}</label>
                                                <div class="col-md-8 p70">
                                                    {{$data.add_company}}
                                                </div>
                                            </div>
                                          
                                            <div class="form-group has-info">
												<label class="col-md-4" style="color: #669fc7">{{l}}Business Type{{/l}}</label>
												<div class="col-md-8 p70">
													{{$user_form.pproduct_chinh}}
												</div>
											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-12 col-sm-6 widget-container-col">
                                <div class="widget-box">
                                    <div class="widget-header">
                                        <h5 class="widget-title">{{$data.name}}</h5>
                                    </div>
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div class="timeline-container">
                                                <div class="timeline-items">
                                                    {{foreach from=$allCats item=item}}
                                                    {{if $item.type_contact!= 2}}
                                                    <div class="timeline-item clearfix">
                                                        <div class="timeline-info">
                                                            <i class="timeline-indicator ace-icon fa fa-comment btn btn-grey no-hover"></i>
                                                        </div>
                                                        <div class="widget-box widget-color-grey">
                                                            <div class="widget-header widget-header-small">
                                                                <h5 class="widget-title smaller">{{l}}Người gửi{{/l}} : {{$data.name_sender}}</h5>
																			<span class="widget-toolbar no-border">
																				<i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                                {{$item.created_date|date_format:"%d-%m-%Y %H:%M"}}
																			</span>
                                                            </div>

                                                            <div class="widget-body">
                                                                <div class="widget-main">
                                                                    {{$item.description}}
                                                                    {{if $item.images != null}}
                                                                    <a href="{{$BASE_URL}}{{$item.images}}">{{l}}Download Attached File{{/l}}</a>
                                                                    {{/if}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{else}}
                                                    <div class="timeline-item clearfix">
                                                        <div class="timeline-info">
                                                            <i class="timeline-indicator ace-icon fa fa-comment btn btn-primary no-hover"></i>
                                                        </div>
                                                        <div class="widget-box widget-color-blue2">
                                                            <div class="widget-header widget-header-small">
                                                                <h5 class="widget-title smaller">{{l}}Reply{{/l}} : {{$data.user_to.full_name}}</h5>
																			<span class="widget-toolbar no-border">
																				<i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                                {{$item.created_date|date_format:"%d-%m-%Y %H:%M"}}
																			</span>
                                                            </div>

                                                            <div class="widget-body">
                                                                <div class="widget-main">
                                                                    {{$item.description}}
                                                                    {{if $item.images != null}}
                                                                    <a href="{{$BASE_URL}}{{$item.images}}">{{l}}Download Attached File{{/l}}</a>
                                                                    {{/if}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{/if}}
                                                    {{/foreach}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-6 widget-container-col">
                                <div class="col-md-12">
                                    <div class="form-group has-info">
                                        <textarea style="float:left;" class="text-input textarea ckeditor"  name="description" rows="10" cols="40"></textarea>
                                    </div>
                                    <div class="form-group has-info">
                                        <label class="control-label col-md-2">{{l}}Attach File{{/l}}</label>
                                        <div class="col-md-10">
                                            <input type="file" id="id-input-file-2" class="form-control" name="images" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-offset-8 col-md-4">
                                        <button class="btn btn-info" type="submit" style="float: right;">
                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                            {{l}}Send{{/l}}
                                        </button>

<!--                                        <button type="button" class="btn btn-warning" onclick="window.location.href='{{$APP_BASE_URL}}message/admin/close-contact/gid/{{$data.message_category_gid}}'">-->
<!--                                            <i class="ace-icon fa fa-close bigger-110"></i>-->
<!--                                            {{l}}Close Contact{{/l}}-->
<!--                                        </button>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.span -->
        </div><!-- /.row -->
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {
        $('#id-input-file-1 , #id-input-file-2').ace_file_input({
            no_file:'No File ...',
            btn_choose:'Choose',
            btn_change:'Change',
            droppable:false,
            onchange:null,
            thumbnail:false //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            //blacklist:'exe|php'
            //onchange:''
            //
        });
    });
</script>

			