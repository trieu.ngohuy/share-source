<!-- Main Container Starts -->
<div id="main-container" class="container">
    <!-- Breadcrumb Starts -->
    <ol class="breadcrumb">
        <li><a href="{{$BASE_URL}}{{$estore.alias}}.htm">{{l}}Home{{/l}}</a></li>
        <li class="active">{{l}}Contact Us{{/l}}</li>
    </ol>
    <!-- Breadcrumb Ends -->
    <!-- Main Heading Starts -->
    <h2 class="main-heading text-center">
        {{l}}Contact Us{{/l}}
    </h2>
    <!-- Main Heading Ends -->
    <!-- Starts -->
    <div class="row">
        <!-- Contact Details Starts -->
        <div class="col-sm-4">
            <div class="panel panel-smart">
                <div class="panel-heading">
                    <h3 class="panel-title">{{l}}Contact Details{{/l}}</h3>
                </div>
                <div class="panel-body">
                    <ul class="list-unstyled contact-details">
                        <li class="clearfix">
                            <i class="fa fa-home pull-left"></i>
									<span class="pull-left">
										{{$estore.name_company}} <br />
										
										{{if $lock != 1}}
                                        	{{$estore.add}}
                                        {{else}}
                                        	***********
                                        {{/if}}
									</span>
                        </li>
                        <li class="clearfix">
                            <i class="fa fa-phone pull-left"></i>
									<span class="pull-left">
										
										{{if $lock != 1}}
                                        	{{$estore.phone}}
                                        {{else}}
                                        	***********
                                        {{/if}}
									</span>
                        </li>
                        <li class="clearfix">
                            <i class="fa fa-envelope-o pull-left"></i>
									<span class="pull-left">
										
										{{if $lock != 1}}
                                        	{{$estore.email_company}}
                                        {{else}}
                                        	***********
                                        {{/if}}
									</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Contact Details Ends -->
        <!-- Contact Form Starts -->
        <div class="col-sm-8">
            <div class="panel panel-smart">
                <div class="panel-heading">
                    <h3 class="panel-title">{{l}}Contact Form{{/l}}</h3>
                </div>
                {{if isset($message)}}
                    {{if $message.success == true}}
                        <div class="alert alert-success" role="alert">
                            {{$message.message}}
                        </div>
                    {{else}}
                        <div class="alert alert-warning" role="alert">
                            {{$message.message}}
                        </div>
                    {{/if}}
                {{/if}}
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">
                                {{l}}Name{{/l}}
                            </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="data[name_sender]" id="name_sender" placeholder="Name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">
                                {{l}}Email{{/l}}
                            </label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" name="data[email]" id="email" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label" required>
                                {{l}}Phone{{/l}}
                            </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="data[phone]" id="phone" placeholder="Phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">
                                {{l}}Company Name{{/l}}
                            </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="data[name_company]" id="name_company" placeholder="Company Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">
                                {{l}}Company Adress{{/l}}
                            </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="data[add_company]" id="add_company" placeholder="Company Address">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="subject" class="col-sm-2 control-label">
                                {{l}}Subject{{/l}}
                            </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="data[name]" id="name" placeholder="Subject" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="col-sm-2 control-label">
                                {{l}}Message{{/l}}
                            </label>
                            <div class="col-sm-10">
                                <textarea name="data[description]" id="description" class="form-control" rows="5" placeholder="Message" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-warning text-uppercase">
                                    {{l}}Submit{{/l}}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Contact Form Ends -->
    </div>
    <!-- Ends -->
</div>
<!-- Main Container Ends -->