


<div class="page-header">
    <h1>
        {{l}}Message{{/l}}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{l}}List All Message{{/l}}
        </small>
    </h1>
</div>
<a class="btn btn-lg btn-success" href="?export=true&page=1">
    <i class="ace-icon fa fa-file-excel-o"></i>
    Export
</a>
<br class="cb">
<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row">
            <div class="col-xs-12">
                {{if $allCategories|@count <= 0}}
                <div class="alert alert-info">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{l}}No category with above conditions.{{/l}}
                </div>
                {{/if}}

                {{if $categoryMessage|@count > 0 && $categoryMessage.success == true}}
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$categoryMessage.message}}.
                </div>
                {{/if}}

                {{if $categoryMessage|@count > 0 && $categoryMessage.success == false}}
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    {{$categoryMessage.message}}.
                </div>
                {{/if}}

                <div  id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">{{l}}User Info{{/l}}</h4>
                            </div>
                            <div class="modal-body" id="load-info-user">

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{l}}Close{{/l}}</button>
                            </div>
                        </div>

                    </div>
                </div>
                <table id="simple-table" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <form id="top-search" name="search" method="post" action="">
                            <th class="center">
                                <button class="btn btn-circle" type="submit"><i class="fa fa-search"></i></button>
                            </th>
                            <th class="center">
                                <input class="check-all" type="checkbox" />
                            </th>
                            <th>{{l}}User To{{/l}}
                                <input  type="text" name="condition[username]" id="username" value="{{$condition.username}}"" placeholder="{{l}}Find Username{{/l}}" class="form-control" />
                            </th>
                            <th>{{l}}User From{{/l}}
                            </th>
                            <th>
                            	{{l}}Company Send Contact{{/l}}
                            </th>
                            <th>{{l}}Email{{/l}}</th>
                            <th>{{l}}Tình Trạng{{/l}}</th>
                            <th>{{l}}Created{{/l}}</th>
                            <th colspan="4" class="col-lg-4 center">{{l}}Detail{{/l}}
                                <input  type="text" name="condition[keyword]" id="username" value="{{$condition.keyword}}"" placeholder="{{l}}Find Name{{/l}}" class="form-control" />
                            </th>
                        </form>
                    </tr>
                    </thead>

                    <tbody>
                    {{if $allCategories|@count > 0}}
                    <form action="" method="post" name="sortForm">
                        {{foreach from=$allCategories item=item name=category key=key}}
                        <tr {{if $item.view == 0}} style="color: #337ab7" {{/if}}>
                            <td class="center">{{$key+1}}</td>
                            <td class="center">{{if 1 == $item.message_deleteable}}<input type="checkbox" value="{{$item.message_category_gid}}" name="allCategories" class="allCategories"/>{{else}}<img src="{{$LAYOUT_HELPER_URL}}admin/images/icons/unselect.png" alt="Do not delete, enable, disable" />{{/if}}</td>
                            <td>
                                {{$item.username_to}}
                            </td>
                            <td>
                                {{$item.username_from}}
                            </td>
                            <td>

                                <a class="" data-toggle="modal" data-target="#myModal" href="#" onclick="loadUser('{{$item.id_to}}');">
                                    {{$item.name_company}}
                                </a>

                            </td>
                            <td>{{$item.email}}</td>
                            <td>
                            	{{if $item.enabled == '0'}}
		                            {{l}}Chờ xử lý{{/l}}
		                        {{elseif $item.enabled == '1'}}
		                            {{l}}Đang xử lý{{/l}}
		                        {{elseif $item.enabled == '2'}}
		                            {{l}}Đã xử lý{{/l}}
		                        {{elseif $item.enabled == '3'}}
		                            {{l}}Đã đóng{{/l}}
		                        {{/if}}
                            </td>
                            <td>{{$item.created_date}}</td>

                            <td colspan="5" style="padding:0px;">
                                <!-- All languages -->
                                <table style="border-collapse: separate;margin-bottom: 0px;" class="table table-bordered table-striped">
                                    <tbody id="table{{$item.message_category_id}}">

                                    {{foreach from=$item.langs item=item2}}
                                    <tr>
                                        <td>{{$item2.name}}</td>

                                        <td class="center" style="width: 50px">

                                            {{p name=edit_message module=message expandId=$langId}}
                                            <span class="tooltip-area">
                                                <a href="{{$APP_BASE_URL}}estore/edit-message-estore/{{$username}}/{{$item.message_category_gid}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                            </span>
                                            {{/p}}
                                            {{np name=edit_message module=message expandId=$langId}}
                                            --
                                            {{/np}}
                                        </td>
                                        <td style="width: 50px">
                                            {{$item2.message_category_id}}
                                        </td>
                                    </tr>
                                    {{/foreach}}

                                    </tbody>
                                </table>
                            </td>

                        </tr>
                        {{/foreach}}
                    </form>
                    {{/if}}
                    </tbody>
                </table>


            </div><!-- /.span -->
        </div><!-- /.row -->


        <div class="col-lg-12">
            <div class="form-group col-lg-4">
                <label class="control-label col-lg-12">{{l}}Action{{/l}}</label>
                <div class="col-lg-6">
                    <select id="action" class="form-control" >
                        <option value=";">{{l}}Choose an action...{{/l}}</option>
                        {{p name=delete_category module=message}}
                        <option value="deleteCategory();">{{l}}Delete{{/l}}</option>
                        {{/p}}
                        <option value="enableCategory();">{{l}}Enable{{/l}}</option>
                        <option value="disableCategory();">{{l}}Disable{{/l}}</option>
                    </select>
                </div>
                <div class="col-lg-6">
                    <a href="javascript:applySelected();" class="form-control btn btn-theme col-lg-4">{{l}}Apply to selected{{/l}}</a>
                </div>
            </div>
            <div class="form-group col-lg-2">
                <label class="control-label col-lg-12">{{l}}Display Num{{/l}}</label>
                <div class="col-lg-12">
                    <form class="search" name="search" method="post" action="">
                        <select name="displayNum" onchange="this.parentNode.submit();"  class="form-control" >
                            <option value="10" {{if $displayNum == 10}} selected="selected" {{/if}}>10</option>
                            <option value="20" {{if $displayNum == 20}} selected="selected" {{/if}}>20</option>
                            <option value="50" {{if $displayNum == 50}} selected="selected" {{/if}}>50</option>
                            <option value="100" {{if $displayNum == 100}} selected="selected" {{/if}}>100</option>
                            <option value="1000000000" {{if $displayNum >= 1000000000}} selected="selected" {{/if}}>{{l}}All{{/l}}</option>
                        </select>
                    </form>
                </div>
            </div>
            {{if $countAllPages > 1 && $type == ''}}
            <div class="col-lg-6 pagination">
                {{if $first}}
                <a href="?page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
                {{/if}}
                {{if $prevPage}}
                <a href="?page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
                {{/if}}

                {{foreach from=$prevPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>

                {{foreach from=$nextPages item=item}}
                <a href="?page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                {{if $nextPage}}
                <a href="?page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
                {{/if}}
                {{if $last}}
                <a href="?page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
                {{/if}}

            </div>
            {{elseif $countAllPages > 1 && $type != ''}}
            	<div class="col-lg-6 pagination">
                {{if $first}}
                <a href="&page=1"  class="btn btn-success" title="{{l}}First Page{{/l}}">&laquo; {{l}}First{{/l}}</a>
                {{/if}}
                {{if $prevPage}}
                <a href="&page={{$prevPage}}"  class="btn btn-success" title="{{l}}Previous Page{{/l}}">&laquo;</a>
                {{/if}}

                {{foreach from=$prevPages item=item}}
                <a href="&page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                <a href="#"  class="btn btn-info active" title="{{$currentPage}}">{{$currentPage}}</a>

                {{foreach from=$nextPages item=item}}
                <a href="&page={{$item}}"  class="btn btn-success" title="{{$item}}">{{$item}}</a>
                {{/foreach}}

                {{if $nextPage}}
                <a href="&page={{$nextPage}}"  class="btn btn-success" title="{{l}}Next Page{{/l}}">&raquo;</a>
                {{/if}}
                {{if $last}}
                <a href="&page={{$countAllPages}}"  class="btn btn-success" title="{{l}}Last Page{{/l}}">{{l}}Last{{/l}} &raquo;</a>
                {{/if}}

            </div>
            {{/if}}
        </div>
    </div>


</div>
</div>

<script language="javascript" type="text/javascript">
    $(document).ready(function(){
        $('.close').click(function(){
            $(this).parent().hide( "slow");
        });
        $('.check-all').click(function(){
            if(this.checked) { // check select status
                $('.allCategories').each(function() { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"
                });
            }else{
                $('.allCategories').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"
                });
            }
        });
    });
    function loadUser(id){
        $.ajax({
            type:'post',
            url:'{{$BASE_URL}}admin/user/admin/get-info',
            data:{id: id},
            success:function(data){
                $('#load-info-user').html(data);
            }
        });
    }
    function applySelected()
    {
        var task = document.getElementById('action').value;
        eval(task);
    }
    function enableCategory()
    {
        var all = document.getElementsByName('allCategories');
        var tmp = '';
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
            }
        }
        if ('' == tmp) {
            alert('Please choose an category');
        }
        window.location.href = '{{$APP_BASE_URL}}message/admin/enable-category/gid/' + tmp;
    }

    function disableCategory()
    {
        var all = document.getElementsByName('allCategories');
        var tmp = '';
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
            }
        }
        if ('' == tmp) {
            alert('Please choose an category');
        }
        window.location.href = '{{$APP_BASE_URL}}message/admin/disable-category/gid/' + tmp;
    }

    function deleteCategory()
    {
        var all = document.getElementsByName('allCategories');
        var tmp = '';
        var count = 0;
        for (var i = 0; i < all.length; i++) {
            if (all[i].checked) {
                tmp = tmp + '_' + all[i].value;
                count++;
            }
        }
        if ('' == tmp) {
            alert('Please choose an category');
            return;
        } else {
            result = confirm('Are you sure you want to delete ' + count + ' category(s)?');
            if (false == result) {
                return;
            }
        }
        window.location.href = '{{$APP_BASE_URL}}message/admin/delete-category/gid/' + tmp;
    }


    function deleteACategory(id)
    {
        result = confirm('Are you sure you want to delete this category?');
        if (false == result) {
            return;
        }
        window.location.href = '{{$APP_BASE_URL}}message/admin/delete-category/gid/' + id;
    }
</script>