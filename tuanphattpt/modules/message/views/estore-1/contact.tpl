<!--Content Block-->
<section class="content-wrapper">
    <div class="content-container container">
        <div class="col-left">
            <div class="block compare-block">
                <div class="block-title">{{l}}CONTACT INFO{{/l}}</div>
                <ul class="list-unstyled contact-details">
                    <li class="clearfix">
                        
									<span class="pull-left">
										<i class="fa fa-home pull-left" style="margin: 3px 5px 0px 0px;"></i>
										<strong>{{$estore.name_company}}</strong> <br />
										{{if $lock != 1}}
                                        	{{$estore.add}}
                                        {{else}}
                                        	***********
                                        {{/if}}
									</span>
                    </li>
                    <li class="clearfix">
                        			<i class="fa fa-phone pull-left"  style="margin: 3px 5px 0px 0px;"></i>
									<span class="pull-left">
										{{if $lock != 1}}
                                        	{{$estore.phone}}
                                        {{else}}
                                        	***********
                                        {{/if}}
										
									</span>
                    </li>
                    <li class="clearfix">
                        <i class="fa fa-envelope-o pull-left"   style="margin: 3px 5px 0px 0px;"></i>
									<span class="pull-left">
										{{if $lock != 1}}
                                        	{{$estore.email_company}}
                                        {{else}}
                                        	***********
                                        {{/if}}
										
									</span>
                    </li>
                </ul>
            </div>
        </div>
        <div  class="col-main">
            {{if isset($message)}}
                {{if $message.success == true}}
                    <div class="alert alert-success" role="alert">
                        {{$message.message}}
                    </div>
                {{else}}
                    <div class="alert alert-warning" role="alert">
                        {{$message.message}}
                    </div>
                {{/if}}
            {{/if}}
            <h1 class="page-title">{{l}}Contact Us{{/l}}</h1>

            <form role="form" method="post">
            <div class="contact-form-container">
                <div  class="form-title">{{l}}Contact Information{{/l}}</div>
                <ul class="form-fields">
                    <li class="full-row">
                        <label>{{l}}Name{{/l}}<em>*</em></label>
                        <input type="text" required name="data[name_sender]" id="name_sender" />
                    </li>
                    <li class="full-row">
                        <label>{{l}}Email{{/l}}<em>*</em></label>
                        <input type="email" required name="data[email]" id="email" />
                    </li>
                    <li class="full-row">
                        <label>{{l}}Phone{{/l}}</label>
                        <input type="text" name="data[phone]" id="phone" />
                    </li>
                    <li class="full-row">
                        <label>{{l}}Company Name{{/l}}</label>
                        <input type="text" name="data[name_company]" id="name_company" />
                    </li>
                    <li class="full-row">
                        <label>{{l}}Company Address{{/l}}</label>
                        <input type="text" name="data[add_company]" id="add_company" />
                    </li>
                    <li class="full-row">
                        <label>{{l}}Subject{{/l}}<em>*</em></label>
                        <input type="text" required name="data[name]" id="name" />
                    </li>
                    <li class="full-row">
                        <label>{{l}}Messsage{{/l}}<em>*</em></label>
                        <textarea required name="data[description]" id="description"></textarea>
                    </li>
                </ul>
                <div class="button-set">
                    <p class="required">* {{l}}Required Fields{{/l}}</p>
                    <button type="submit" class="form-button"><span>{{l}}Submit{{/l}}</span></button>
                </div>
            </div>
            </form>
        </div>

    </div>
</section>