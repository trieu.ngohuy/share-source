<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="secondary_page_wrapper">

    <div class="container">

        <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

        <ul class="breadcrumbs">

            <li><a href="{{$BASE_URL}}{{$estore.alias}}.htm">{{l}}Home{{/l}}</a></li>
            <li>{{l}}Contact Us{{/l}}</li>

        </ul>

        <div class="row">

            <main class="col-md-12 col-sm-12">
                <h2 class="page_title">{{l}}Contact Us{{/l}}</h2>
                <section class="section_offset">
                    <div class="col-md-4">

                        <p class="form_caption">
                            {{$estore.name_company}}
                        </p>
                        <ul class="c_info_list">
                            <li class="c_info_location">{{if $lock != 1}}
                                        	{{$estore.add}}
                                        {{else}}
                                        	***********
                                        {{/if}}</li>
                            <li class="c_info_phone">{{if $lock != 1}}
                                        	{{$estore.phone}}
                                        {{else}}
                                        	***********
                                        {{/if}}</li>
                            <li class="c_info_mail"><a href="mailto:{{$estore.email_company}}">{{if $lock != 1}}
                                        	{{$estore.email_company}}
                                        {{else}}
                                        	***********
                                        {{/if}}</a></li>
                        </ul>

                    </div><!--/ [col]-->
                    <div class="col-md-8">
                        <h3>{{l}}Contact Form{{/l}}</h3>
                        {{if isset($message)}}
                            {{if $message.success == true}}
                                <div class="alert alert-success" role="alert">
                                    {{$message.message}}
                                </div>
                            {{else}}
                                <div class="alert alert-warning" role="alert">
                                    {{$message.message}}
                                </div>
                             {{/if}}
                        {{/if}}
                        <form id="contact_form" method="post">
                        <div class="theme_box">
                            <!-- - - - - - - - - - - - - - Contact form - - - - - - - - - - - - - - - - -->
                                <ul>
                                    <li class="row">
                                        <div class="col-sm-12">
                                            <label for="cf_name" class="required">{{l}}Name{{/l}}</label>
                                            <input type="text" required name="data[name_sender]" id="name_sender" title="Name">
                                        </div><!--/ [col]-->
                                    </li><!--/ .row -->
                                    <li class="row">
                                        <div class="col-sm-6">
                                            <label for="cf_email" class="required">{{l}}Email Address{{/l}}</label>
                                            <input type="email" required name="data[email]" id="email" title="Email">
                                        </div>
                                        <div class="col-xs-6">
                                            <label for="cf_order_number">{{l}}Phone{{/l}}</label>
                                            <input type="text" name="data[phone]" id="phone" title="Phone">
                                        </div><!--/ [col]-->
                                    </li><!--/ .row -->
                                    <li class="row">
                                        <div class="col-xs-12">
                                            <label for="cf_order_number">{{l}}Companny Name{{/l}}</label>
                                            <input type="text" name="data[name_company]" id="name_company" title="Company Name">
                                        </div><!--/ [col]-->
                                    </li><!--/ .row -->
                                    <li class="row">
                                        <div class="col-xs-12">
                                            <label for="cf_order_number">{{l}}Company Address{{/l}}</label>
                                            <input type="text" name="data[add_company]" id="add_company" title="Company Address">
                                        </div><!--/ [col]-->
                                    </li><!--/ .row -->
                                    <li class="row">
                                        <div class="col-xs-12">
                                            <label for="data[name]" class="required">{{l}}Subject{{/l}}</label>
                                            <input type="text" required name="data[name]" id="name" title="Subject">
                                        </div><!--/ [col]-->
                                    </li><!--/ .row -->
                                    <li class="row">
                                        <div class="col-xs-12">
                                            <label for="cf_message" class="required">{{l}}Message{{/l}}</label>
                                            <textarea name="data[description]" id="description" required title="Message" rows="6"></textarea>
                                        </div><!--/ [col]-->
                                    </li><!--/ .row -->
                                </ul>
                            <!-- - - - - - - - - - - - - - End of contact form - - - - - - - - - - - - - - - - -->
                        </div><!--/ .theme_box -->
                        <footer class="bottom_box on_the_sides">
                            <div class="left_side">
                                <button class="button_dark_grey middle_btn" type="submit" form="contact_form">{{l}}Submit{{/l}}</button>
                            </div>
                            <div class="right_side">
                                <p class="prompt">{{l}}Required Fields{{/l}}</p>
                            </div>
                        </footer>
                        </form><!--/ .contactform -->
                    </div>


                </section>
            </main><!--/ [col]-->

        </div><!--/ .row-->

    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->