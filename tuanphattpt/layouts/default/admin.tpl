<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		
		{{$headLink}}
		{{$headStyle}}
		{{$headTitle}}
		<link rel="icon" href="{{$LAYOUT_HELPER_URL}}front/images/favicon_2.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/bootstrap.css" />
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/css/font-awesome-4.6.3/css/font-awesome.min.css" />
		
		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/jquery-ui.custom.css" />
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/chosen.css" />
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/datepicker.css" />
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/bootstrap-datetimepicker.css" />
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/colorpicker.css" />
		
		
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/bootstrap-duallistbox.css" />

		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/select2.css" />
		
		
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/ace-fonts.css" />

		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace-extra.js"></script>
		<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.js"></script>
        <link rel="shortcut icon" type="image/png" href="{{$LAYOUT_HELPER_URL}}front/assets/icon.ico"/>

		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/bootstrap.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}libs/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}libs/bootstrap-multiselect/bootstrap-multiselect.css" />

	</head>
	
	<body class="no-skin">
		{{sticker name=admin_header}}
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>
			
			{{sticker name=admin_menu}}
			
			
			<div class="main-content">
				<div class="main-content-inner">
				
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="{{$BASE_URL}}admin">{{l}}Home{{/l}}</a>
							</li>
							{{foreach from=$breadcrumb item=item}}
								<li class="active">
									<i class="ace-icon fa {{$item.icon}}"></i>
									<a href="{{$item.url}}">
										{{$item.name}}
									</a>
								</li>
							{{/foreach}}
						</ul>

					</div>
					
					<div class="page-content">
						{{$content}}
					</div>
				</div>
			</div>
			{{sticker name=admin_footer}}
		</div>
		
		<script type="text/javascript">
			window.jQuery || document.write("<script src='{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>


		<!-- page specific plugin scripts -->
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.bootstrap-duallistbox.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.raty.js"></script>

		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/select2.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/typeahead.jquery.js"></script>
		
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/chosen.jquery.js"></script>
		
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery-ui.custom.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.ui.touch-punch.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.easypiechart.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/jquery.sparkline.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/flot/jquery.flot.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/flot/jquery.flot.pie.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/flot/jquery.flot.resize.js"></script>
		
		
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/date-time/bootstrap-timepicker.js"></script>

		<!-- ace scripts -->
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.scroller.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.colorpicker.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.fileinput.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.typeahead.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.wysiwyg.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.spinner.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.treeview.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.wizard.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.aside.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.ajax-content.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.touch-drag.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.sidebar.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.submenu-hover.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.widget-box.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.settings.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.settings-rtl.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.settings-skin.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.searchbox-autocomplete.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var demo1 = $('select[id="duallist_cate"]').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">{{l}}Filtered{{/l}}</span>'});
				var demo2 = $('select[name="data[category_id][]"]').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">{{l}}Filtered{{/l}}</span>'});
				var demo3 = $('select[name="data[color][]"]').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">{{l}}Filtered{{/l}}</span>'});
				var demo4 = $('select[name="data[thanhtoan][]"]').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">{{l}}Filtered{{/l}}</span>'});

				var container1 = demo1.bootstrapDualListbox('getContainer');
				container1.find('.btn').addClass('btn-white btn-info btn-bold');

				var container2 = demo2.bootstrapDualListbox('getContainer');
				container2.find('.btn').addClass('btn-white btn-info btn-bold');

				var container3 = demo3.bootstrapDualListbox('getContainer');
				container3.find('.btn').addClass('btn-white btn-info btn-bold');

				var container4 = demo4.bootstrapDualListbox('getContainer');
				container4.find('.btn').addClass('btn-white btn-info btn-bold');

				$(document).one('ajaxloadstart.page', function(e) {
					$('[class*=select2]').remove();
					$('select[name="data[category_id][]"]').bootstrapDualListbox('destroy');
					$('.rating').raty('destroy');
				});

				// $('.multiselect').multiselect({
				// 	 enableFiltering: true,
				// 	 buttonClass: 'btn btn-white btn-primary',
				// 	 templates: {
				// 		button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
				// 		ul: '<ul class="multiselect-container dropdown-menu"></ul>',
				// 		filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
				// 		filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
				// 		li: '<li><a href="javascript:void(0);"><label></label></a></li>',
				// 		divider: '<li class="multiselect-item divider"></li>',
				// 		liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
				// 	 }
				// 	});

				if(!ace.vars['touch']) {
					$('.chosen-select').chosen({allow_single_deselect:true});
					//resize the chosen on window resize

					$(window)
					.off('resize.chosen')
					.on('resize.chosen', function() {
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					}).trigger('resize.chosen');
					//resize chosen on sidebar collapse/expand
					$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
						if(event_name != 'sidebar_collapsed') return;
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					});


					$('#chosen-multiple-style .btn').on('click', function(e){
						var target = $(this).find('input[type=radio]');
						var which = parseInt(target.val());
						if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
						 else $('#form-field-select-4').removeClass('tag-input-style');
					});
				}
				$('.input-daterange').datepicker({autoclose:true});
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('.easy-pie-chart.percentage').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
					var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
					var size = parseInt($(this).data('size')) || 50;
					$(this).easyPieChart({
						barColor: barColor,
						trackColor: trackColor,
						scaleColor: false,
						lineCap: 'butt',
						lineWidth: parseInt(size/10),
						animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
						size: size
					});
				})

				$('.sparkline').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
					$(this).sparkline('html',
									 {
										tagValuesAttribute:'data-values',
										type: 'bar',
										barColor: barColor ,
										chartRangeMin:$(this).data('min') || 0
									 });
				});





				//Android's default browser somehow is confused when tapping on label which will lead to dragging the task
				//so disable dragging when clicking on label
				var agent = navigator.userAgent.toLowerCase();
				if("ontouchstart" in document && /applewebkit/.test(agent) && /android/.test(agent))
				  $('#tasks').on('touchstart', function(e){
					var li = $(e.target).closest('#tasks li');
					if(li.length == 0)return;
					var label = li.find('label.inline').get(0);
					if(label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation() ;
				});

				$('#tasks').sortable({
					opacity:0.8,
					revert:true,
					forceHelperSize:true,
					placeholder: 'draggable-placeholder',
					forcePlaceholderSize:true,
					tolerance:'pointer',
					stop: function( event, ui ) {
						//just for Chrome!!!! so that dropdowns on items don't appear below other items after being moved
						$(ui.item).css('z-index', 'auto');
					}
					}
				);
				$('#tasks').disableSelection();
				$('#tasks input:checkbox').removeAttr('checked').on('click', function(){
					if(this.checked) $(this).closest('li').addClass('selected');
					else $(this).closest('li').removeClass('selected');
				});


				//show the dropdowns on top or bottom depending on window height and menu position
				$('#task-tab .dropdown-hover').on('mouseenter', function(e) {
					var offset = $(this).offset();

					var $w = $(window)
					if (offset.top > $w.scrollTop() + $w.innerHeight() - 100)
						$(this).addClass('dropup');
					else $(this).removeClass('dropup');
				});



			})
		</script>

		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin/assets/css/ace.onpage-help.css" />
		<link rel="stylesheet" href="{{$LAYOUT_HELPER_URL}}admin//docs/assets/js/themes/sunburst.css" />

		<script type="text/javascript"> ace.vars['base'] = '..'; </script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/elements.onpage-help.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin/assets/js/ace/ace.onpage-help.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin//docs/assets/js/rainbow.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin//docs/assets/js/language/generic.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin//docs/assets/js/language/html.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin//docs/assets/js/language/css.js"></script>
		<script src="{{$LAYOUT_HELPER_URL}}admin//docs/assets/js/language/javascript.js"></script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-72841960-4', 'auto');
		  ga('send', 'pageview');
		
		</script>
	</body>
</html>
