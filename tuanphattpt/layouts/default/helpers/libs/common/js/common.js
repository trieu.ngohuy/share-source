/*===========================================================
 ZolaWeb Group Ecommerce JS
 Author: ZolaWeb group
 License: © 2018 Copyright: ZolaWeb Group. All rights reserved
 ============================================================*/

/*
 Expire cookie day length
 */
var $_cookie_expire_length = 7;
/*
 Cart cookie name
 */
var $_cookie_cart = 'cart';
/*
 Cookie message
 */
var $_cookie_message = [];

/*==============================================================
 READY
 ================================================================*/
$(document).ready(function () {
    //Check if cookie enable
    //CheckCookie();
});
/*==============================================================
 END
 ================================================================*/

/*==============================================================
 CART
 ================================================================*/

/*
 Get cart
 */
function GetCart() {
    //Get cookie
    var cart = GetCookie($_cookie_cart);
    if (cart.status === false) {
        return;
    }
    //Format cart
}

/*==============================================================
 END
 ================================================================*/

/*==============================================================
 COOKIE
 ================================================================*/

/*
 Check if cookie enabled
 */
function CheckCookie() {
    $.cookie('Cookie_Test', true, {expires: 1});
    if (typeof $.cookie(name) === 'undefined') {
        return {
            status: false,
            message: $_cookie_message['cookie-disabled']
        }
    } else {
        return {
            status: true
        };
    }
}

/*
 Set cookies
 */
function SaveCookie(name, value) {
    //Check cookie
    var tmp = CheckCookie();
    if (tmp.status === false) {
        return tmp;
    }
    //Save cookie
    $.cookie(name, JSON.stringify(value), {expires: $_cookie_expire_length});

}

/*
 Get cookie
 */
function GetCookie(name) {
    //Check cookie
    var tmp = CheckCookie();
    if (tmp.status === false) {
        return tmp;
    }
    //Get cookie
    if (typeof $.cookie(name) === 'undefined') {
        return {
            status: false,
            message: $_cookie_message['cookie-valid'],
        };
    } else {
        return {
            status: true,
            val: $.parseJSON($.cookie(name))
        };
    }
}

/*==============================================================
 END
 ================================================================*/
/*
 * Handle ajax
 */
function HandleAjaxCall(url, input, message, debug_mode) {
    jQuery.ajax({
        type: "POST",
        url: url,
        data: input,
        success: function (data) {
            data = jQuery.parseJSON(data);
            if (data.status) {
                swal(message.success.title, message.success.content, "success");
            } else {
                var content = message.error.content;
                if (debug_mode) {
                    content = message.error.content +
                            '<hr>' + 
                            '<p><b>Error detail: </b></p>' +                            
                            '<p>Content: ' + data.message + '</p>';
                }
                swal(message.error.title, content, "error");
            }
            //Handle after call ajax
            if (typeof HandleAjaxResponse === 'function') {
                HandleAjaxResponse(data);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var content = message.error.content;
            if (debug_mode) {
                var content = message.error.content +
                        '<hr>' + 
                        '<p><b>Error detail: </b></p>' +
                        '<p>Status: ' + textStatus + '</p>' +
                        '<p>Error: ' + errorThrown + '</p>' +
                        '<p>Content: ' + XMLHttpRequest.responseText + '</p>';
            }
            swal(message.error.title, content, "error");
        }
    });
}
/*
 * Format price
 */
/*
 * Format price
 * @param {type} value
 * @returns {undefined}
 */
function FormatPrice(value) {
    var formatedPrice = "";
    //Convert to string
    value = value.toString();
    //Loop each char of value
    var count = 1;
    for (var i = value.length - 1; i >= 0; i--) {
        //Add ',' between each 3 number
        if (count >= 3 && i > 0 && count % 3 === 0) {
            formatedPrice += value[i] + ",";
        } else {
            formatedPrice += value[i];
        }
        count++;
    }
    //Rever formated price then return
    return formatedPrice.split('').reverse().join('');
}