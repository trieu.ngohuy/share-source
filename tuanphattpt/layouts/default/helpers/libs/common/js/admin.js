/*
 Zola Web Group - Admin common javascript
 Author: Zola Web Group
 Copyright © 2018 Zola Web Group. All rights reserved.
 */
/*
 * Current image selected id
 * @type type
 */
var arrAttr = {
    arrImg: {
      activeIndex: 0,
      activeImg: 0,
      arrDetail: []
    },
    source: '',
    destination: '',
    mode: 0 /*new*/
};

/*==========
 * Admin manager
 ==========*/
/*
 * Admin ready
 */
function AdminReady() {
    /*
     Select all item in tables
     */
    $('.check-all').click(function () {
        if (this.checked) { // check select status
            $('.allParams').each(function () { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"
            });
        } else {
            $('.allParams').each(function () { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"
            });
        }
    });
}
/*
 Run action
 */
function RunAction() {
    var task = document.getElementById('action').value;
    eval(task);
}

/*
 Delete data
 */
function DeleteData(url) {
    var all = document.getElementsByName('allItems');
    var tmp = '';
    var count = 0;
    for (var i = 0; i < all.length; i++) {
        if (all[i].checked) {
            tmp = tmp + '_' + all[i].value;
            count++;
        }
    }
    if ('' === tmp) {
        alert('Please choose an at least 1');
        return;
    } else {
        result = confirm('Are you sure you want to delete ' + count + ' item(s)?');
        if (false === result) {
            return;
        }
    }
    window.location.href = url + tmp;
}

/*==========
 * Admin modify
 ==========*/
/*
 * Set up slug
 * @param {type} arrLangs
 * @returns {undefined}
 */
function SetupSlug(arrLangs){
    //Set slug to from source to destination
    for (var i = 1; i <= arrLangs.length; i++) {
        $('#' + arrAttr.source + i + '').makeSlug({
            slug: jQuery('#' + arrAttr.destination + i + '')
        });
    }
}
/*
 * Modify ready
 */
function CkfinderInit(layout_url) {
    //Init ckfinder
    CKFinder.setupCKEditor(null, layout_url);

    //Ul images config
    $("#ulImages").sortable();
    $("#ulImages").disableSelection();
    
    //Add style to list images
    for (var i = 0; i < arrAttr.arrImg.arrDetail.length; i++) {
        $("#"+arrAttr.arrImg.arrDetail[i]).sortable();
        $("#"+arrAttr.arrImg.arrDetail[i]).disableSelection();
    
        $('#'+arrAttr.arrImg.arrDetail[i]).css({
            'list-style-type': 'none',
            'margin': '0',
            'padding': '0'
        });
        $('#'+arrAttr.arrImg.arrDetail[i]+' li').css({
            'margin': '10px',
            'float': 'left',
            'text-align': 'center',
            'height': '180px'
        });
    }

}
/*
 * Open image manager window
 */
function ImageManagerWindow(activeImg, indexImg) {
    arrAttr.arrImg.activeImg = arrAttr.arrImg.arrDetail[activeImg];
    arrAttr.arrImg.activeIndex = indexImg;
    // You can use the "CKFinder" class to render CKFinder in a page:
    var finder = new CKFinder();
    finder.basePath = '{{$LAYOUT_HELPER_URL}}admin/js/ckfinder/'; // The path for the installation of CKFinder (default = "/ckfinder/").
    finder.selectActionFunction = SetImage;
    finder.popup();
}

/*
 * This is a sample function which is called when a file is selected in CKFinder.
 */
function SetImage(fileUrl) {
    document.getElementById('imgDisplay_'+arrAttr.arrImg.activeImg+'_' + arrAttr.arrImg.activeIndex).src = fileUrl;
    document.getElementById('inpSrc_'+arrAttr.arrImg.activeImg+'_' + arrAttr.arrImg.activeIndex).value = fileUrl;
    document.getElementById('divWrap_'+arrAttr.arrImg.activeImg+'_' + arrAttr.arrImg.activeIndex).style.display = '';
    document.getElementById('divNoImageWrap_'+arrAttr.arrImg.activeImg+'_' + arrAttr.arrImg.activeIndex).style.display = 'none';
}
/*
 * Clear selected image
 */
function ClearImage(activeImg, indexImg) {
    arrAttr.arrImg.activeImg = arrAttr.arrImg.arrDetail[activeImg];
    arrAttr.arrImg.activeIndex = indexImg;
    
    document.getElementById('imgDisplay_' + arrAttr.arrImg.activeImg+'_' + arrAttr.arrImg.activeIndex).src = '';
    document.getElementById('inpSrc_' + arrAttr.arrImg.activeImg+'_' + arrAttr.arrImg.activeIndex).value = '';
    document.getElementById('divWrap_' + arrAttr.arrImg.activeImg+'_' + arrAttr.arrImg.activeIndex).style.display = 'none';
    document.getElementById('divNoImageWrap_' + arrAttr.arrImg.activeImg+'_' + arrAttr.arrImg.activeIndex).style.display = '';
}

/*
 * Add image block
 */
function AddImageBlock(activeImg) {
    //Set active img
    arrAttr.arrImg.activeImg = arrAttr.arrImg.arrDetail[activeImg];
    var imgVal = arrAttr.arrImg.arrDetail[activeImg];
    //Count li
    var max = $('#' + imgVal).length + 1;
    //li html
    var html = '<li class="ui-sortable-handle">' +
            '<input name="data[' + imgVal + '][]" class="input_image" id="inpSrc_'+imgVal+'_'+max+'" type="hidden" value="">' +
            '<div id="divWrap_'+imgVal+'_'+max+'" style="display: none;">' +
            '    <img id="imgDisplay_'+imgVal+'_'+max+'" style="max-width: 150px; max-height:150px; border:dashed thin;" src="">' +
            '</div>' +
            '<div id="divNoImageWrap_'+imgVal+'_'+max+'" style="width: 150px; border: thin dashed; text-align: center; padding:70px 0px;">' +
            '    No image' +
            '</div>' +
            '<br>' +
            '<a href="javascript:ImageManagerWindow('+imgVal+','+max+');">[Choose image]</a> |' +
            '<a href="javascript:ClearImage('+imgVal+','+max+');">[Delete]</a>' +
            '</li>';
    jQuery("ul#"+imgVal).append(html);
}