/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* global arrData */
var selectedIndex = 1;
(function ($) {
    $(document).ready(function ($) {

        //First establist table
        EstablishTable();

        //Input data change
        $('.inpData').on('keyup', function () {
            EstablishTable();
        });
        //Custom combobox onchange
        $(document).on({
            change: function () {
                EstablishTable();
            }
        }, '#comCustom');
    });
})(jQuery);

/*
 Fill table data
 */
function EstablishTable() {
    //Get inputs
    var input = {
        doroi: parseInt($('#txtDoroi').val() === '' ? 0 : $('#txtDoroi').val()),
        dai: parseInt($('#txtDai').val() === '' ? 0 : $('#txtDai').val()),
        rong: parseInt($('#txtRong').val() === '' ? 0 : $('#txtRong').val()),
        cao: parseInt($('#txtCao').val() === '' ? 0 : $('#txtCao').val()),
        sogio: parseInt($('#txtSogio').val() === '' ? 0 : $('#txtSogio').val())
    };

    //Filter data
    var arrTmp = arrData.filter(x => parseInt(x.cao) >= input.cao);

    //Fill data
    FillData(input, arrTmp.slice(0, 3));
    
    //Set selected index
    $('#comCustom :nth-child('+selectedIndex+')').prop('selected', true);
}
/*
 Params select
 */
function ParamsSelect() {
    html = '<select data-v-6010a6d2="" class="custom-select mb-2 mr-sm-2 mb-sm-0" id="comCustom" style="height: 28px; padding: 0px; padding-left: 10px;">';
    for (var i = 0; i < arrData.length; i++) {
        html += '<option data-v-6010a6d2="" value="' + i + '">' + arrData[i]['code'] + '</option>';
    }
    html += '</select>';
    return html;
}
/*
 Calculating number of led need to used
 */
function Calculating(input, obj) {
    //Calculating dien tich
    var dientich = input.dai * input.rong;
    //Tính tông lumen cần dùng
    var lumen = input.doroi * dientich;
    //Tính tổng công suất
    var congsuat = lumen / 100;
    //Tính số lượng đền cần dùng
    var soluong = congsuat / obj.congsuat;
    var tmp = soluong + '';
    if (tmp.indexOf("e") >= 0) {
        return toFixed(soluong);
    } else {
        return parseInt(soluong);
    }
}
/*
 Display params detail
 */
function ParamsDetail(data, input, type) {

    var html = '';
    var val = '';

    //Fill default column
    for (var i = 0; i < data.length; i++) {

        var tmp = data[i];
        //code
        if (type === 'code') {
            val = tmp.code;
        }
        //congsuat
        if (type === 'congsuat') {
            val = tmp.congsuat;
        }
        //quangthong
        if (type === 'quangthong') {
            val = tmp.quangthong;
        }
        //soluong
        if (type === 'soluong') {
            val = Calculating(input, tmp);
        }
        //tuoitho
        if (type === 'tuoitho') {
            var sogio = parseInt(tmp.sogio);
            if (input.sogio === 0) {
                val = 0;
            } else {
                val = sogio / input.sogio;
            }
            val = parseInt(val / 365);
            val = val + ' ' + msgYear;
        }

        html += '<td data-v-6010a6d2=""><input data-v-6010a6d2="" type="text" disabled="disabled" value="' + val + '"></td>';
    }
    //Get custom selected item
    if ($('#comCustom').length > 0) {
        var obj = arrData[parseInt($("#comCustom option:selected").val())];
        selectedIndex = parseInt($("#comCustom option:selected").val()) + 1;
    } else {
        var obj = arrData[0];
    }

    //Fill custom column
    //congsuat
    if (type === 'congsuat') {
        val = obj.congsuat;
    }
    //quangthong
    if (type === 'quangthong') {
        val = obj.quangthong;
    }
    //soluong
    if (type === 'soluong') {
        val = Calculating(input, obj);
    }
    //tuoitho
    if (type === 'tuoitho') {
        var sogio = parseInt(obj.sogio);
        if (input.sogio === 0) {
            val = 0;
        } else {
            val = sogio / input.sogio;
        }
        val = parseInt(val / 365);
        val = val + ' ' + msgYear;
    }

    //Return
    return {
        html: html,
        val: val
    };
}
/*
 Fill table html
 */
function FillData(input, data) {

    //Table html
    var html = '<tr data-v-6010a6d2="">' +
            '                    <td data-v-6010a6d2=""></td>';
    if (data.length > 0) {
        html += '                    <td data-v-6010a6d2="" colspan="3" style="background-color: rgb(92, 184, 95); color: rgb(255, 255, 255); text-transform: uppercase; font-weight: bold;">' + msgKhuyendung + '</td>';
    }

    html += '                    <td data-v-6010a6d2="" style="background-color: rgb(40, 108, 218); color: rgb(255, 255, 255); text-transform: uppercase; font-weight: bold;">' + msgCustom + '</td>' +
            '                </tr>' +
            '                <tr data-v-6010a6d2="">' +
            '                    <td data-v-6010a6d2="">' + msgCode + '</td>';
    var tmp = ParamsDetail(data, input, 'code');
    html += tmp.html;

    html += '                    <td data-v-6010a6d2="">'
            + ParamsSelect() +
            '                    </td>' +
            '                </tr>' +
            '                <tr data-v-6010a6d2="">' +
            '                    <td data-v-6010a6d2="">' + msgCongsuat + '</td>';
    tmp = ParamsDetail(data, input, 'congsuat');
    html += tmp.html;

    html += '                    <td data-v-6010a6d2=""><input value="' + tmp.val + '" id="txtCustomCongSuat" data-v-6010a6d2="" type="number" disabled="disabled"></td>' +
            '                </tr>' +
            '                <tr data-v-6010a6d2="">' +
            '                    <td data-v-6010a6d2="">' + msgQuangthong + '</td>';
    tmp = ParamsDetail(data, input, 'quangthong');
    html += tmp.html;

    html += '                    <td data-v-6010a6d2=""><input value="' + tmp.val + '" id="txtCustomQuangThong" data-v-6010a6d2="" type="number" disabled="disabled"></td>' +
            '                </tr>' +
            '                <tr data-v-6010a6d2="">' +
            '                    <td data-v-6010a6d2="">' + msgSoluong + '</td>';
    tmp = ParamsDetail(data, input, 'soluong');
    html += tmp.html;

    html += '                    <td data-v-6010a6d2=""><input value="' + tmp.val + '" id="txtCustomSoLuong" data-v-6010a6d2="" type="number" disabled="disabled"></td>' +
            '                </tr>' +
            '                <tr data-v-6010a6d2="">' +
            '                    <td data-v-6010a6d2="">' + msgTuoitho + '</td>';
    tmp = ParamsDetail(data, input, 'tuoitho');
    html += tmp.html;

    html += '                    <td data-v-6010a6d2=""><input value="' + tmp.val + '" id="txtCustomTuoiTho" data-v-6010a6d2="" type="text" disabled="disabled"></td>' +
            '                </tr>';

    //Fill data to table
    $('#tableData').html(html);
}
/*
 Convert big number include e
 */
function toFixed(x) {
    if (Math.abs(x) < 1.0) {
        var e = parseInt(x.toString().split('e-')[1]);
        if (e) {
            x *= Math.pow(10, e - 1);
            x = '0.' + (new Array(e)).join('0') + x.toString().substring(2);
        }
    } else {
        var e = parseInt(x.toString().split('+')[1]);
        if (e > 20) {
            e -= 20;
            x /= Math.pow(10, e);
            x += (new Array(e + 1)).join('0');
        }
    }
    return x;
}