<!doctype html>
<html lang="vi">

<head>
    <script type="text/javascript">
        window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var o=t[n]={exports:{}};e[n][0].call(o.exports,function(t){var o=e[n][1][t];return r(o||t)},o,o.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({1:[function(e,t,n){function r(){}function o(e,t,n){return function(){return i(e,[f.now()].concat(u(arguments)),t?null:this,n),t?void 0:this}}var i=e("handle"),a=e(2),u=e(3),c=e("ee").get("tracer"),f=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,t){s[t]=o(d+t,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),t.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,t){var n={},r=this,o="function"==typeof t;return i(l+"tracer",[f.now(),e,n],r),function(){if(c.emit((o?"":"no-")+"fn-start",[f.now(),r,o],n),o)try{return t.apply(this,arguments)}catch(e){throw c.emit("fn-err",[arguments,this,e],n),e}finally{c.emit("fn-end",[f.now()],n)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,t){m[t]=o(l+t)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,f.now()])}},{}],2:[function(e,t,n){function r(e,t){var n=[],r="",i=0;for(r in e)o.call(e,r)&&(n[i]=t(r,e[r]),i+=1);return n}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],3:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,o=n-t||0,i=Array(o<0?0:o);++r<o;)i[r]=e[t+r];return i}t.exports=r},{}],4:[function(e,t,n){t.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,t,n){function r(){}function o(e){function t(e){return e&&e instanceof r?e:e?c(e,u,i):i()}function n(n,r,o,i){if(!d.aborted||i){e&&e(n,r,o);for(var a=t(o),u=m(n),c=u.length,f=0;f<c;f++)u[f].apply(a,r);var p=s[y[n]];return p&&p.push([b,n,r,a]),a}}function l(e,t){v[e]=m(e).concat(t)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(n)}function g(e,t){f(e,function(e,n){t=t||"feature",y[n]=t,t in s||(s[t]=[])})}var v={},y={},b={on:l,emit:n,get:w,listeners:m,context:t,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",c=e("gos"),f=e(2),s={},p={},d=t.exports=o();d.backlog=s},{}],gos:[function(e,t,n){function r(e,t,n){if(o.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[t]=r,r}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){o.buffer([e],r),o.emit(e,t,n)}var o=e("ee").get("handle");t.exports=r,r.ee=o},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!x++){var e=h.info=NREUM.info,t=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&t))return s.abort();f(y,function(t,n){e[t]||(e[t]=n)}),c("mark",["onload",a()+h.offset],null,"api");var n=d.createElement("script");n.src="https://"+e.agent,t.parentNode.insertBefore(n,t)}}function o(){"complete"===d.readyState&&i()}function i(){c("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),c=e("handle"),f=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,SI:p.setImmediate,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1071.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=t.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),c("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);
    </script>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" media="all" href="{{$LAYOUT_HELPER_URL}}front/css/autoptimize_7c0.css" rel="stylesheet" />
    <link type="text/css" media="only screen and (max-width: 768px)" href="{{$LAYOUT_HELPER_URL}}front/css/autoptimize_c1b.css" rel="stylesheet" />
    {{$headTitle}}
    <meta name="robots" content="noydir" />
    <meta name="description" content="Đèn LED nhà xưởng TUẤN PHÁT • Công suất từ 40w đến 250w • Chip LED Nichia (Nhật Bản) với hiệu suất phát quang cao nhất thế giới • Bảo hành 3 năm 1 đối 1 • Hỗ trợ mượn đèn để so sánh và dùng thử • Đặc biệt tư vấn và thiết kế chiếu sáng miễn phí."
    />
    <meta property="og:image" content="/wp-content/uploads/2017/04/favicon.png" />
    <meta property="og:image:width" content="270" />
    <meta property="og:image:height" content="270" />
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Đèn LED Nhà Xưởng &#8211; TUẤN PHÁT" />
    <meta property="og:description" content="Đèn LED nhà xưởng TUẤN PHÁT • Công suất từ 40w đến 250w • Chip LED Nichia (Nhật Bản) với hiệu suất phát quang cao nhất thế giới • Bảo hành 3 năm 1 đối 1 • Hỗ trợ mượn đèn để so sánh và dùng thử • Đặc biệt tư vấn và thiết kế chiếu sáng miễn phí."
    />
    <meta property="og:url" content="/cat/den-led-nha-xuong/" />
    <meta property="og:site_name" content="TUẤN PHÁT" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="Đèn LED Nhà Xưởng &#8211; TUẤN PHÁT" />
    <meta name="twitter:description" content="Đèn LED nhà xưởng TUẤN PHÁT • Công suất từ 40w đến 250w • Chip LED Nichia (Nhật Bản) với hiệu suất phát quang cao nhất thế giới • Bảo hành 3 năm 1 đối 1 • Hỗ trợ mượn đèn để so sánh và dùng thử • Đặc biệt tư vấn và thiết kế chiếu sáng miễn phí."
    />
    <meta name="twitter:image" content="/wp-content/uploads/2017/04/favicon.png" />
    <meta name="twitter:image:width" content="270" />
    <meta name="twitter:image:height" content="270" />
    <link rel="canonical" href="/cat/den-led-nha-xuong/" />
    <link rel="next" href="https:///cat/den-led-nha-xuong/page/2/" />
    <link rel='dns-prefetch' href='//www.potech.com.vn' />
    <link rel='dns-prefetch' href='//www.google.com' />
    <link rel='dns-prefetch' href='//code.jquery.com' /> <noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
    <link rel="icon" href="/wp-content/uploads/2017/04/favicon-60x60.png" sizes="32x32" />
    <link rel="icon" href="/wp-content/uploads/2017/04/favicon.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="/wp-content/uploads/2017/04/favicon.png" />
    <meta name="msapplication-TileImage" content="/wp-content/uploads/2017/04/favicon.png" />

    <!--Link to font awesome css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--Custom script-->
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
</head>

<body class="archive tax-product_cat term-den-led-nha-xuong term-9 woocommerce woocommerce-page woocommerce-no-js sidebar-primary"> <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMSXHR2"
                                                                                                                                                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<div id="wptime-plugin-preloader"></div>
<!--[if IE]><div class="alert alert-warning"> You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</div> <![endif]-->
{{sticker name=front_header}}
<div class="wrap" role="document">
    {{$content}}
</div>
{{sticker name=front_footer}}
<div class="ctaMobileV2 toggleCTA animated"> <a class="ctaMobileV2Wrap"> <i class="fa fa-phone" aria-hidden="true"></i> </a></div>

<script type="application/ld+json">
        {"@context":"https:\/\/schema.org\/","@graph":[{"@context":"https:\/\/schema.org\/","@type":"BreadcrumbList","itemListElement":[{"@type":"ListItem","position":"1","item":{"name":"Trang ch\u1ee7","@id":"https:\/\/www.potech.com.vn"}},{"@type":"ListItem","position":"2","item":{"name":"S\u1ea3n ph\u1ea9m","@id":"https:\/\/www.potech.com.vn\/san-pham\/"}},{"@type":"ListItem","position":"3","item":{"name":"\u0110\u00e8n LED nh\u00e0 x\u01b0\u1edfng"}}]},{"@context":"https:\/\/schema.org\/","@graph":[{"@type":"Product","@id":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-low-bay\/","name":"\u0110\u00e8n LED Low Bay","url":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-low-bay\/"},{"@type":"Product","@id":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-250w-rsh\/","name":"\u0110\u00e8n LED nh\u00e0 x\u01b0\u1edfng 250w (RSH)","url":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-250w-rsh\/"},{"@type":"Product","@id":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-150w-rsh\/","name":"\u0110\u00e8n LED nh\u00e0 x\u01b0\u1edfng 150w (RSH)","url":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-150w-rsh\/"},{"@type":"Product","@id":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-100w-rsh\/","name":"\u0110\u00e8n LED nh\u00e0 x\u01b0\u1edfng 100w (RSH)","url":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-100w-rsh\/"},{"@type":"Product","@id":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-80w-rsh\/","name":"\u0110\u00e8n LED nh\u00e0 x\u01b0\u1edfng 80w (RSH)","url":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-80w-rsh\/"},{"@type":"Product","@id":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-60w-rsh\/","name":"\u0110\u00e8n LED nh\u00e0 x\u01b0\u1edfng 60w (RSH)","url":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-60w-rsh\/"},{"@type":"Product","@id":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-40w-rsh\/","name":"\u0110\u00e8n LED nh\u00e0 x\u01b0\u1edfng 40w (RSH)","url":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-40w-rsh\/"},{"@type":"Product","@id":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-ufo\/","name":"\u0110\u00e8n LED UFO","url":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-ufo\/"},{"@type":"Product","@id":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-high-bay-smd\/","name":"\u0110\u00e8n LED High Bay SMD","url":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-high-bay-smd\/"},{"@type":"Product","@id":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-250w\/","name":"\u0110\u00e8n LED nh\u00e0 x\u01b0\u1edfng 250W","url":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-250w\/"},{"@type":"Product","@id":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-200w\/","name":"\u0110\u00e8n LED nh\u00e0 x\u01b0\u1edfng 200W","url":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-200w\/"},{"@type":"Product","@id":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-150w\/","name":"\u0110\u00e8n LED nh\u00e0 x\u01b0\u1edfng 150W","url":"https:\/\/www.potech.com.vn\/san-pham\/den-led-nha-xuong\/den-led-nha-xuong-150w\/"}]}]}
    </script>
<script type="text/javascript">
    var c = document.body.className;
    c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
    document.body.className = c;
</script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
    (window.jQuery && jQuery.noConflict()) || document.write('<script src="wp-includes/js/jquery/jquery.js"><\/script>')
</script>
<script type='text/javascript'>
    var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
</script>
<script type='text/javascript'>
    var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_9b87401fd56e9a5234353a80222e3206","fragment_name":"wc_fragments_9b87401fd56e9a5234353a80222e3206"};
</script>
<script type="text/javascript">
    var renderInvisibleReCaptcha = function() {

        for (var i = 0; i < document.forms.length; ++i) {
            var form = document.forms[i];
            var holder = form.querySelector(".inv-recaptcha-holder");

            if (null === holder) continue;
            holder.innerHTML = "";

            (function(frm){
                var cf7SubmitElm = frm.querySelector(".wpcf7-submit");
                var holderId = grecaptcha.render(holder,{
                    "sitekey": "6Lc_pigUAAAAABURbr91dvrrwSYPX2HiGZTW3GxN", "size": "invisible", "badge" : "bottomright",
                    "callback" : function (recaptchaToken) {
                        if((null !== cf7SubmitElm) && (typeof jQuery != "undefined")){jQuery(frm).submit();grecaptcha.reset(holderId);return;}
                            HTMLFormElement.prototype.submit.call(frm);
                    },
                    "expired-callback" : function(){grecaptcha.reset(holderId);}
                });

                if(null !== cf7SubmitElm && (typeof jQuery != "undefined") ){
                    jQuery(cf7SubmitElm).off("click").on("click", function(clickEvt){
                        clickEvt.preventDefault();
                        grecaptcha.execute(holderId);
                    });
                }
                else
                {
                    frm.onsubmit = function (evt){evt.preventDefault();grecaptcha.execute(holderId);};
                }


            })(form);
        }
    };
</script>
<script async defer src="https://www.google.com/recaptcha/api.js?onload=renderInvisibleReCaptcha&#038;render=explicit"></script>
<script type="text/javascript" defer src="{{$LAYOUT_HELPER_URL}}front/js/autoptimize_903.js"></script>
<script type="text/javascript">
    window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"641d8326cc","applicationID":"52293986","transactionName":"NQBQYRYAWRBRVUwPDQxKZ0cNTl4NVFNASBIKFQ==","queueTime":0,"applicationTime":1310,"atts":"GUdTF14aSh4=","errorBeacon":"bam.nr-data.net","agent":""}
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5b22322770971d3d70af71fb/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>

</html>
<!--
Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/

Page Caching using disk: enhanced
Application Monitoring using New Relic

Served from: potech.com.vn @ 2018-06-14 08:39:41 by W3 Total Cache
-->