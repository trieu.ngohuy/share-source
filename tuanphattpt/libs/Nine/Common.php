<?php

/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2011 9fw.org
 * @license    http://license.9fw.org
 * @version    v 1.0 2009-04-15
 * 
 */
require_once 'Zend/Config.php';
require_once 'Zend/Config/Writer/Array.php';
require_once 'modules/content/models/Content.php';
require_once 'modules/content/models/ContentCategory.php';
require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/user/models/User.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/templates/models/Templates.php';
include_once 'modules/mail/models/phpmailer.php';
include_once 'modules/mail/models/smtp.php';

class Nine_Common {

    protected function __construct() {
        
    }

    /*
     * Write log
     */

    function WriteLog($content) {
        echo '<script>console.log("' . $content . '")</script>';
        //error_log( print_r($allCats, TRUE) );
    }

    /*
     * Image update data progressing
     */

    function UpdateImage($image) {
        $target_dir = 'media/userfiles/user/';
        if ($image["name"] != '') {
            $target_dir = $target_dir . time() . '-' . basename($image["name"]);
            $uploadOk = 1;


            // Check file size
            if ($image["size"] > 20000000) {
                $errors['erro'] = 1;
                $errors['mess'] = Nine_Language::translate('File Lớn Hơn 20Mb.');
                $uploadOk = 0;
            }

            // Only GIF files allowed
            if ($image["type"] != "image/gif" && $image["type"] != "image/jpg" && $image["type"] != "image/png" && $image["type"] != "image/jpeg") {
                $errors['erro'] = 1;
                $errors['mess'] = Nine_Language::translate('Chỉ nhận file png / jpg / gif / jpeg .');
                $uploadOk = 0;
            }

            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk != 0) {
                if (move_uploaded_file($image["tmp_name"], $target_dir)) {

                    $errors['erro'] = 0;
                    $errors['mess'] = Nine_Language::translate('Update hình ảnh thành công!!');
                    $errors['image'] = $target_dir;
                } else {
                    //die;
                    $errors['erro'] = 1;
                    $errors['mess'] = Nine_Language::translate('Sever Not Upload File . ');
                    $errors['image'] = '';
                }
            } else {
                $errors['image'] = '';
            }
            return $errors;
        }
    }

    /*
     * Get setting contacs
     */

    public static function GetSettingContacts($view, $contact) {
        $contact = explode('||', $contact);
        $arrContacts = array();
        for ($i = 0; $i < count($contact); $i++) {
            $tmp = explode('::', $contact[$i]);
            $arrContacts[$tmp[0]] = array(
                'type' => $tmp[1],
                'val' => $tmp[2]
            );
        }
        $view->arrContacts = $arrContacts;
    }

    //Check file is exist on server
    public static function checkRemoteFile($url) {
        if (@getimagesize($url)) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Set layout and view 
     * $view - view object
     * $layout - layout name
     * $viewFolder - folder contain views file
     * $viewName - view name
     */

    public static function setLayoutAndView($obj, $title = "", $layout, $viewName = 'index', $viewFolder = 'default') {
        /*
         * Set title
         */
        $obj->view->headTitle($title);
        $obj->setLayout($layout);
        $templatePath = Nine_Registry::getModuleName() . '/views/' . $viewFolder;
        $templatePath .= '/' . $viewName . '.' . Nine_Constant::VIEW_SUFFIX;
        $obj->view->html = $obj->view->render($templatePath);
    }

    /*
     * Set layout
     */

    public static function SetViews($obj, $path, $fileName) {
        $templatePath = Nine_Registry::getModuleName() . '/views' . $path;
        $templatePath .= '/' . $fileName . '.' . Nine_Constant::VIEW_SUFFIX;
        $obj->view->html = $obj->view->render($templatePath);
    }

    //FUNCTION
    //Convert vn to en
    //PARAMETERS:
    //pa1: str
    public static function convert_vi_to_en($str) {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        // $str = str_replace(" ", "-", str_replace("&*#39;","",$str));
        return $str;
    }

    // Not found page
    public static function NotFoundPage($gid) {
        
    }

    // set layouts and views base on user_id
    public static function setLayoutAndViews($arr) {
        $estorePath = 'default';
        $estoreHome = 'front';
        //Get params and set variables
        if (isset($arr['_request'])) {
            //Get parameters
            $params = $arr['_request']->getParams();
            if (isset($arr ['user_id']) && $arr ['user_id'] != null)
                $arr ['user_id'] = $params['user_id'];
            if (isset($arr ['alias']) && $arr ['alias'] != null)
                $arr ['alias'] = $params['alias'];
        }
        if ((isset($arr ['user_id']) && $arr ['user_id'] != null && $arr ['user_id'] != 0) || (isset($arr ['alias']) && $arr ['alias'] != null && $arr ['alias'] != "")) {

            // get estore
            if ($arr ['user_id'] != null && $arr ['user_id'] != 0)
                $estoreUser = @reset(Nine_Query::getListUsers(array(
                                    'user_id' => $arr ['user_id']
                )));

            elseif ($arr ['alias'] != null && $arr ['alias'] != "")
                $estoreUser = @reset(Nine_Query::getListUsers(array(
                                    'alias' => $arr ['alias']
                )));

            if ($estoreUser ['group_id'] == 3) {
                // get template
                $objTemp = new Models_Templates ();
                $estoreTemplate = @reset($objTemp->getByColumns(array(
                                    'templates_id' => $estoreUser ['template']
                                ))->toArray());
                // set path
                $estorePath = $estoreTemplate ['path'];
                $estorePath = 'estore-3';
                $estoreHome = 'helpers/themes/' . $estoreTemplate ['path'] . '/home';
                $estoreHome = 'helpers/themes/estore-3/home';
            }
        }
        // set layout
        Nine_Controller_Action::setLayout($estoreHome);
        $templatePath = Nine_Registry::getModuleName() . '/views/' . $estorePath;
        $templatePath .= '/' . $arr ['viewsName'] . '.' . Nine_Constant::VIEW_SUFFIX;
        $arr ['view']->html = $arr ['view']->render($templatePath);
    }

    /*
     * Recursion reverse array data
     */

    public static function RecursionReverse($listData, $keyId, $strKey) {
        if (isset($listId)) {
            $listId .= "";
        } else {
            $listId = "";
        }
        foreach ($listData as $value) {

            if ($value [$strKey] == $keyId && $keyId != 0) {
                $listId .= "," . $value ['parent_id'];
                $listId .= Nine_Common::RecursionReverse($listData, $value ['parent_id'], $strKey);
            }
        }
        return $listId;
    }

    /*
     * Get list id from parent to all children
     */

    public static function GetReverseId($arrData, $gid) {
        if (isset($listId)) {
            $listId .= " ";
        } else {
            $listId = "";
        }
        foreach ($arrData as $value) {
            if ($value ['parent_id'] == $gid) {
                $listId .= "," . $value ['product_category_gid'];
                $tmp = Nine_Query::getListProductCategroryId($arrData, $value ['product_category_gid']);
                $listId .= ", " . $tmp['arr'];
            }
        }
        $str = substr($listId, 1);
        return array(
            'str' => substr($listId, 1),
            'arr' => explode(",", substr($str, 0, strlen($str) - 2))
        );
    }

    // breadcrumbs
    public static function getBreadCumbs($gid, $type, $detail, $extrasCat = null) {
        /*
         * Init Array
         */
        $breadCrubms = array();
        /*
         * Add home item
         */
        $breadCrubms ['home'] = array(
            'name' => Nine_Language::translate("Home"),
            'url' => Nine_Route::_("default/index", "")
        );
        /*
         * Add url
         */
        $url = Nine_Route::url();
        /*
         * Check case
         */
        switch ($type) {
            case 'product' :
                /*
                 * Get all cat
                 */
                $allCat = Nine_Query::getListProductCategory(array());
                $listId = Nine_Common::RecursionReverse($allCat, $gid, 'product_category_gid');
                /*
                 * Get list product categories
                 */
                $breadCrubms['data'] = Nine_Query::getListProductCategory(array(
                            'product_category_gid IN(?)' => explode(",", $listId)
                ));
                break;
            case 'content' :
                /*
                 * Get all cat
                 */
                $allCat = Nine_Query::getListContentCategory(array());
                $listId = Nine_Common::RecursionReverse($allCat, $gid, 'content_category_gid');
                /*
                 * Get list product categories
                 */
                $breadCrubms['data'] = Nine_Query::getListContentCategory(array(
                            'content_category_gid IN(?)' => explode(",", $listId)
                ));
                break;
            default :
                break;
        }
        /*
         * Extras cat
         */
        if ($extrasCat != null) {
            $breadCrubms['data'][] = $extrasCat;
        }
        /*
         * Add detail item
         */
        $breadCrubms['detail'] = $detail;
        /*
         * return breadcrumbs
         */
        return $breadCrubms;
    }

    /*
     * Get logged user
     */

    public static function getLoggedUser($obj, $force = false, $view = null) {
        $loggedUser = null;
        if (isset($_SESSION ['user_login'])) {
            $loggedUser = $_SESSION ['user_login'];
            /*
             * List delivery add
             */
            $loggedUser['add'] = explode('||', $loggedUser['add']);
            /*
             * Set layout put
             */
            if ($view != null) {
                $view->loggedUser = $loggedUser;
            }
        } else {
            $loggedUser = '';
            /*
             * Go to login page
             */

            if ($force == true) {
                //$obj->_redirect('access/login');
            }
        }
        return $loggedUser;
    }

    // make up alias
    public static function makeUpAlias($_) {
        return str_replace(" ", "-", str_replace("&*#39;", "", trim($_)));
    }

    // makeup price
    public static function makeUpPrice($_) {
        return strrev((string) preg_replace('/(\d{3})(?=\d)(?!\d*\.)/', '$1,', strrev($_)));
    }

    public static function cleanString($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    // check upload image
    public static function uploadImage($image) {
        $upload = array();
        $target_dir = 'media/userfiles/user/';
        $target_dir = $target_dir . basename($image ["name"]);
        // Check if file already exists
        if (file_exists($target_dir . $image ["name"])) {
            $upload ['erro'] = 0;
            $upload ['mess'] = Nine_Language::translate('Tên File Đã Tồn Tại');
            $upload ['image'] = "";
        }
        // Check file size
        if ($image ["size"] > 20000000) {
            $upload ['erro'] = 0;
            $upload ['mess'] = Nine_Language::translate('File Lớn Hơn 20Mb.');
            $upload ['image'] = "";
        }
        // Only GIF files allowed
        if ($image ["type"] != "image/gif" && $image ["type"] != "image/jpg" && $image ["type"] != "image/png" && $image ["type"] != "image/jpeg") {
            $upload ['erro'] = 0;
            $upload ['mess'] = Nine_Language::translate('Chỉ nhận file png / jpg / gif / jpeg .');
            $upload ['image'] = "";
        }
        if (isset($upload ['erro'])) {
            $upload ['erro'] = 0;
            $upload ['mess'] = Nine_Language::translate('Image can\' be uploaded.');
            $upload ['image'] = "";
        } else {
            if (move_uploaded_file($image ["tmp_name"], $target_dir)) {
                $upload ['erro'] = 1;
                $upload ['image'] = $target_dir;
            } else {
                die();
                $upload ['erro'] = 1;
                $upload ['mess'] = Nine_Language::translate('Sever Not Upload File . ');
                $upload ['image'] = "";
            }
        }
        return $upload;
    }

    // get estore menu
    public static function getEstoreMenu($alias) {
        $url = Nine_Route::url();
        $estoreMenu = array();
        $estoreMenu ['home'] = array(
            "name" => Nine_Language::translate("Home"),
            "url" => $url ['path'] . $alias . ".htm"
        );
        $estoreMenu ['contact'] = array(
            "name" => Nine_Language::translate("Contact"),
            "url" => $url ['path'] . 'estore/contact-us/' . $alias . ".html"
        );
        $estoreMenu ['editInfo'] = array(
            "name" => Nine_Language::translate("Edit info"),
            "url" => $url ['path'] . 'estore/edit-info/' . $alias . ".html"
        );
        $estoreMenu ['resetPass'] = array(
            "name" => Nine_Language::translate("Edit info"),
            "url" => $url ['path'] . 'estore/change-password' . $alias . ".html"
        );

        $estoreMenu ['setting'] = array(
            "name" => Nine_Language::translate("Edit info"),
            "url" => $url ['path'] . 'estore/setting/' . $alias . ".html"
        );
        $estoreMenu ['search'] = array(
            "name" => Nine_Language::translate("Search"),
            "url" => $url ['path'] . 'estore/search/' . $alias . ".html"
        );

        $estoreMenu ['cart'] = array(
            "client" => array(
                "name" => Nine_Language::translate("Cart"),
                "url" => $url ['path'] . "cart/" . $alias . ".html"
            ),
            "admin" => array(
                "name" => Nine_Language::translate("Manager Cart"),
                "url" => $url ['path'] . "estore/manage-cart/" . $alias . ".html"
            )
        );
        $estoreMenu ['user'] = array(
            "login" => array(
                "name" => Nine_Language::translate("Login"),
                "url" => $url ['path'] . "estore/login/" . $alias . ".htm"
            ),
            "register" => array(
                "name" => Nine_Language::translate("Register"),
                "url" => $url ['path'] . "estore/register/" . $alias . ".html"
            ),
            "profile" => array(
                "name" => Nine_Language::translate("Profile"),
                "url" => $url ['path'] . "estore/profile/" . $alias . ".html"
            )
        );
        $estoreMenu ['products'] = array(
            "managerProducts" => array(
                "name" => Nine_Language::translate("Manager Products"),
                "url" => $url ['path'] . "estore/manage-products/" . $alias . ".html"
            ),
            "managerCategorys" => array(
                "name" => Nine_Language::translate("Manager Products"),
                "url" => $url ['path'] . "estore/manage-product-category/" . $alias . ".html"
            )
        );

        return $estoreMenu;
    }

    public static function sendMail($listEmail, $content) {
        $objUser = new Models_User ();
        // get admin email
        $admin = $objUser->getByUserName('admin');
        $adminemail = $admin ['email'];
        // get config
        $config = Nine_Query::getConfig();
        $data_send = array(
            'name_to' => $content ['name'],
            'email_to' => array_merge(array(
                $adminemail,
                $config ['Email'],
                $listEmail
            )),
            'title_email' => $content ['title'],
            'name_from' => $config ['WebsiteName'],
            'content' => $content
        );
        $this->sendmail_smtp($data_send);
    }

    public static function sendmail_smtp($data) {
        // Mail
        define('MAIL_PROTOCOL', 'smtp');
        define('MAIL_HOST', 'ssl://smtp.googlemail.com');
        define('MAIL_PORT', '465');
        define('MAIL_USER', 'luutn.it@gmail.com');
        define('MAIL_PASS', 'Linh2204');
        define('MAIL_SEC', 'ssl');
        define('MAIL_FROM', 'no.reply.goods@gmail.com');

        $mail = new PHPMailer ();
        $mail->IsSMTP();
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPAuth = TRUE;
        $mail->SMTPSecure = MAIL_SEC;
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASS;
        $mail->CharSet = "UTF-8";
        $mail->WordWrap = 50;

        $mail->IsHTML(TRUE);
        $mail->From = MAIL_USER;
        $mail->FromName = $data ['name_from'];
        $to = $data ['email_to'];
        $name = $data ['name_to'];
        if (is_array($to)) {
            foreach ($to as $key => $sto) {
                $mail->AddAddress($sto, '');
            }
        } else {
            $mail->AddAddress($to, $name);
        }

        $mail->AddReplyTo(MAIL_USER, "Goods");
        $mail->Subject = $data ['title_email'];
        $mail->Body = $data ['content'];
        $mail->AltBody = $data ['content'];
        if (!$mail->Send()) {
            return false;
        } else {
            return true;
        }
    }

    public static function getCart($session, $view = null) {
        /*
         * Get list cart
         */
        $allCard = $session->cart;
        /*
         * Check if cart is empty
         */
        if (count($allCard) <= 0) {
            $allCard = array(
                'cartDetail' => array(),
                'total_price' => 0
            );
        }
        /*
         * Set to view
         */
        if ($view != null) {
            /*
             * Set output view
             */
            $view->allCard = $allCard;
        }
        /*
         * Return list cart
         */
        return $session->cart;

//        $cart = array();
//        // get cart
//        $allCard = $session->cart;
//        if ($allCard == '') {
//            $allCard = array();
//        }
//        $total_price = 0;
//        $objProductDetail = new Models_Product ();
//        foreach ($allCard as &$item) {
//            $item ['product'] = @reset(Nine_Query::getListProducts(array(
//                                'product_gid' => $item ['product_gid']
//            )));
//            $temp_price = str_replace(",", "", $item ['product'] ['price']);
//            $temp_price = str_replace(".", "", $temp_price);
//            $total_price += ($temp_price * $item ['mount']);
//            $item ['product'] ['total_price'] = str_replace(".", ",", $objProductDetail->makeUpPrice($temp_price * $item ['mount']));
//            $item ['product'] ['tprice_prase'] = str_replace(".", ",", $objProductDetail->makeUpPrice($item ['product'] ['price']));
//        }
//        unset($item);
//        $total_price = str_replace(".", ",", Nine_Query::makeUpPrice($total_price));
//        $cart ['allCart'] = $allCard;
//        $cart ['total_price'] = $total_price;
//        return $cart;
    }

    // get translate menu
    public static function getTranslateMenu($arr) {
        $translateText = array();
        foreach ($arr as $key => $value) {
            $translateText [$key] ['url'] = Nine_Route::_($value ['url'], array(
                        'alias' => $value ['alias']
            ));
            $translateText [$key] ['name'] = Nine_Language::translate($value ['name']);
        }
        return $translateText;
    }

    /**
     * Singleton instance
     *
     * @return Nine_Language
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self ();
        }

        return self::$_instance;
    }

    // export excel
    public static function exportExcel($export) {
        if ($export != false) {
            $objType = new Models_Type ();

            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $style = "style = 'border: 1px solid;text-align:left'";
            $style1 = "style = 'border: 1px solid;font-weight: bold;'";

            $allProductType = $objType->getAllTypes();

            $header = "";
            $header .= "<tr>";
            $header .= "<td $style1>CÔNG TY TNHH SX - TM LẬP THÀNH PHÁT QUỐC TẾ</td>";
            $header .= "</tr>";
            $header .= "<tr>";
            $header .= "<td $style>40/1 Đường 18, tổ 24, KP 2, P. Thạnh Mỹ Lợi, Q2, TP.HCM</td>";
            $header .= "</tr>";
            $header .= "<tr>";
            $header .= "<td $style>Điện thoại: 08.3743.8480 - Fax: 08.3743.7719</td>";
            $header .= "</tr>";
            $header .= "<tr>";
            $header .= "<td $style1>No</td>";
            $header .= "<td $style1>Mã Sản Phẩm</td>";
            $header .= "<td $style1>Tên Sản Phẩm</td>";
            $header .= "<td $style1>Kiểu</td>";
            $header .= "<td $style1>Số Lượng</td>";
            $header .= "<td $style1>Giá</td>";
            $header .= "</tr>";
            $content = '';
            $no = 1;

            foreach ($allProductType as $item) {
                $content .= "<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item ['product_gid'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item ['title'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item ['type'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item ['mount'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($objProduct->makeUpPrice($item ['price']), 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "</tr>";
                $no ++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-product-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();
        }
    }

    // get list country and provines
    public static function getPlaces() {
        $places = array();
        $places ['countries'] = array(
            "Afghanistan",
            "Albania",
            "Algeria",
            "American Samoa",
            "Andorra",
            "Angola",
            "Anguilla",
            "Antarctica",
            "Antigua and Barbuda",
            "Argentina",
            "Armenia",
            "Aruba",
            "Australia",
            "Austria",
            "Azerbaijan",
            "Bahamas",
            "Bahrain",
            "Bangladesh",
            "Barbados",
            "Belarus",
            "Belgium",
            "Belize",
            "Benin",
            "Bermuda",
            "Bhutan",
            "Bolivia",
            "Bosnia and Herzegowina",
            "Botswana",
            "Bouvet Island",
            "Brazil",
            "British Indian Ocean Territory",
            "Brunei Darussalam",
            "Bulgaria",
            "Burkina Faso",
            "Burundi",
            "Cambodia",
            "Cameroon",
            "Canada",
            "Cape Verde",
            "Cayman Islands",
            "Central African Republic",
            "Chad",
            "Chile",
            "China",
            "Christmas Island",
            "Cocos (Keeling) Islands",
            "Colombia",
            "Comoros",
            "Congo",
            "Congo, the Democratic Republic of the",
            "Cook Islands",
            "Costa Rica",
            "Cote d'Ivoire",
            "Croatia (Hrvatska)",
            "Cuba",
            "Cyprus",
            "Czech Republic",
            "Denmark",
            "Djibouti",
            "Dominica",
            "Dominican Republic",
            "East Timor",
            "Ecuador",
            "Egypt",
            "El Salvador",
            "Equatorial Guinea",
            "Eritrea",
            "Estonia",
            "Ethiopia",
            "Falkland Islands (Malvinas)",
            "Faroe Islands",
            "Fiji",
            "Finland",
            "France",
            "France Metropolitan",
            "French Guiana",
            "French Polynesia",
            "French Southern Territories",
            "Gabon",
            "Gambia",
            "Georgia",
            "Germany",
            "Ghana",
            "Gibraltar",
            "Greece",
            "Greenland",
            "Grenada",
            "Guadeloupe",
            "Guam",
            "Guatemala",
            "Guinea",
            "Guinea-Bissau",
            "Guyana",
            "Haiti",
            "Heard and Mc Donald Islands",
            "Holy See (Vatican City State)",
            "Honduras",
            "Hong Kong",
            "Hungary",
            "Iceland",
            "India",
            "Indonesia",
            "Iran (Islamic Republic of)",
            "Iraq",
            "Ireland",
            "Israel",
            "Italy",
            "Jamaica",
            "Japan",
            "Jordan",
            "Kazakhstan",
            "Kenya",
            "Kiribati",
            "Korea, Democratic People's Republic of",
            "Korea, Republic of",
            "Kuwait",
            "Kyrgyzstan",
            "Lao, People's Democratic Republic",
            "Latvia",
            "Lebanon",
            "Lesotho",
            "Liberia",
            "Libyan Arab Jamahiriya",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Macau",
            "Macedonia, The Former Yugoslav Republic of",
            "Madagascar",
            "Malawi",
            "Malaysia",
            "Maldives",
            "Mali",
            "Malta",
            "Marshall Islands",
            "Martinique",
            "Mauritania",
            "Mauritius",
            "Mayotte",
            "Mexico",
            "Micronesia, Federated States of",
            "Moldova, Republic of",
            "Monaco",
            "Mongolia",
            "Montserrat",
            "Morocco",
            "Mozambique",
            "Myanmar",
            "Namibia",
            "Nauru",
            "Nepal",
            "Netherlands",
            "Netherlands Antilles",
            "New Caledonia",
            "New Zealand",
            "Nicaragua",
            "Niger",
            "Nigeria",
            "Niue",
            "Norfolk Island",
            "Northern Mariana Islands",
            "Norway",
            "Oman",
            "Pakistan",
            "Palau",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Philippines",
            "Pitcairn",
            "Poland",
            "Portugal",
            "Puerto Rico",
            "Qatar",
            "Reunion",
            "Romania",
            "Russian Federation",
            "Rwanda",
            "Saint Kitts and Nevis",
            "Saint Lucia",
            "Saint Vincent and the Grenadines",
            "Samoa",
            "San Marino",
            "Sao Tome and Principe",
            "Saudi Arabia",
            "Senegal",
            "Seychelles",
            "Sierra Leone",
            "Singapore",
            "Slovakia (Slovak Republic)",
            "Slovenia",
            "Solomon Islands",
            "Somalia",
            "South Africa",
            "South Georgia and the South Sandwich Islands",
            "Spain",
            "Sri Lanka",
            "St. Helena",
            "St. Pierre and Miquelon",
            "Sudan",
            "Suriname",
            "Svalbard and Jan Mayen Islands",
            "Swaziland",
            "Sweden",
            "Switzerland",
            "Syrian Arab Republic",
            "Taiwan, Province of China",
            "Tajikistan",
            "Tanzania, United Republic of",
            "Thailand",
            "Togo",
            "Tokelau",
            "Tonga",
            "Trinidad and Tobago",
            "Tunisia",
            "Turkey",
            "Turkmenistan",
            "Turks and Caicos Islands",
            "Tuvalu",
            "Uganda",
            "Ukraine",
            "United Arab Emirates",
            "United Kingdom",
            "United States",
            "United States Minor Outlying Islands",
            "Uruguay",
            "Uzbekistan",
            "Vanuatu",
            "Venezuela",
            "Vietnam",
            "Virgin Islands (British)",
            "Virgin Islands (U.S.)",
            "Wallis and Futuna Islands",
            "Western Sahara",
            "Yemen",
            "Yugoslavia",
            "Zambia",
            "Zimbabwe"
        );
        $places ['provinces'] = array(
            "Hà Nội",
            "Huế",
            "Đà nẵng",
            "Hồ chí minh"
        );
        return $places;
    }

    // get config
    public static function getConfig($view = null) {
        require_once 'Zend/Config.php';
        // list
        $oldSettings = include 'setting.php';
        $url = Nine_Route::url();
        $oldSettings ['Logo'] = $url ['path'] . $oldSettings ['Logo'];
        /*
         * Set layout variables
         */
        if($view !== null){
            $view->config = $oldSettings;
        }
        
        /*
         * Return data
         */
        return $oldSettings;
    }

    public static function setConfig($arr) {
        /**
         * Save config
         */
        try {
            $oldConfig = Nine_Query::getConfig();
            $writer = new Zend_Config_Writer_Array ();
            $writer->setExclusiveLock(true);
            $writer->write('setting.php', new Zend_Config(array_merge($oldConfig, $arr)));
            $writer->setExclusiveLock(false);

            return true;
        } catch (Exception $e) {
            return $e;
        }
    }

    // get params
    public static function getParams($arr) {
        $arr = array();
        foreach ($variable as $value) {
            $arr [$value] = Nine_Query::_getParam($value, false);
        }

        return $arr;
    }

    public static function getListProductCategroryId($listData, $parentId) {
        if (isset($listId)) {
            $listId .= "";
        } else {
            $listId = "";
        }
        foreach ($listData as $value) {
            if ($value ['parent_id'] == $parentId) {
                $listId .= "," . $value ['product_category_id'];
                $listId .= Nine_Query::getListProductCategroryId($listData, $value ['product_category_id']);
            }
        }
        return substr($listId, 1);
    }

    /*
     * Format array as tree format
     */

    public static function FomatTree($listData, $parentId = null, $character = null, $attr) {
        if ($parentId == null) {
            $parentId = 0;
        }
        if ($character == null) {
            $character = "";
        }
        if (isset($outputArray)) {
            // $outputArray[] .= $outputArray;
        } else {
            $outputArray = array();
        }

        foreach ($listData as $value) {
            if ($value['parent_id'] == $parentId) {
                if (isset($value [$attr])) {
                    $temp = $attr;
                } elseif (isset($value [$attr])) {
                    $temp = 'content_category_gid';
                } else {
                    $temp = 'estore_category_gid';
                }
                $outputArray[] = array(
                    'name' => $character . " " . $value ['name'],
                    $attr => $value [$attr],
                    'alias' => $value ['alias'],
                );
                $outputArray = array_merge($outputArray, Nine_Common::FomatTree($listData, $value [$attr], $character . "--", $attr));
            }
        }
        return $outputArray;
    }

    /*
     * Format array as tree
     */

    public static function FomatArrayAsTree($listData, $parentId = null, $character = null, $attr, $isMerge = false) {
        if ($parentId == null) {
            $parentId = 0;
        }

        if (isset($outputArray)) {
            // $outputArray[] .= $outputArray;
        } else {
            $outputArray = array();
        }

        foreach ($listData as $value) {
            if ($value['parent_id'] == $parentId) {

                if ($isMerge) {
                    $value['name'] = ($character === null ? '' : $character) . " " . $value ['name'];
                    $value['detail'] = Nine_Common::FomatArrayAsTree($listData, $value [$attr], ($character === null ? null : $character . "--"), $attr, $isMerge);
                    $outputArray[] = $value;
                } else {
                    $value['name'] = ($character === null ? '' : $character) . " " . $value ['name'];
                    $outputArray[] = $value;
                    $outputArray = array_merge($outputArray, Nine_Common::FomatArrayAsTree($listData, $value [$attr], ($character === null ? '' : $character . "--"), $attr, $isMerge));
                }

                //print_r($value);
            }
        }
        return $outputArray;
    }

    public static function getListContentCategoryId($listData, $parentId) {
        if (isset($listId)) {
            $listId .= "";
        } else {
            $listId = "";
        }
        foreach ($listData as $value) {
            if ($value ['parent_id'] == $parentId) {
                $listId .= "," . $value ['content_category_id'];
                $listId .= Nine_Query::getListContentCategoryId($listData, $value ['content_category_id']);
            }
        }
        return substr($listId, 1);
    }

    public static function formatData($allData, $objData) {
        try {
            // get url
            $url = Nine_Route::url();
            foreach ($allData as &$content) {
                if ($content ['alias'] == "") {
                    $content ['alias'] = $objData->convert_vi_to_en($content ['alias']);
                    $content ['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", trim($content ['alias'])));
                }
                $gid = 0;
                $tempPath = 'index';
                $mainPath = 'content';
                if (isset($content ['product_category_gid'])) {
                    $mainPath = 'product';
                    if (isset($content ['product_gid'])) {
                        $gid = $content ['product_gid'];
                        $tempPath = 'detail';
                    } else {
                        $gid = $content ['product_category_gid'];
                    }
                } elseif (isset($content ['content_category_gid'])) {
                    if (isset($content ['content_gid'])) {
                        $gid = $content ['content_gid'];
                        $tempPath = 'detail';
                    } else {
                        $gid = $content ['content_category_gid'];
                    }
                }

                if (isset($content ['price'])) {
                    $content ['price'] = Nine_Query::makeUpPrice($content ['price']);
                }
                if (isset($content ['price_sale'])) {
                    $content ['price_sale'] = Nine_Query::makeUpPrice($content ['price_sale']);
                }
                $content ['url'] = Nine_Route::_("{$mainPath}/index/{$tempPath}", array(
                            'alias' => $content ['alias']
                ));

                if (isset($content ['price']) && isset($content ['price_sale'])) {
                    $content ['price'] = str_replace('.', ',', Nine_Query::makeUpPrice($content ['price']));
                    $content ['price_sale'] = str_replace('.', ',', Nine_Query::makeUpPrice($content ['price_sale']));
                }

                if (isset($content ['images'])) {
                    $tmp = explode('||', $content ['images']);
                    $content ['main_image'] = $url ['path'] . $tmp [0];
                }
                if (isset($content ['icon'])) {
                    $content ['icon'] = $url ['path'] . $content ['icon'];
                }
                $content ['images'] = explode('||', $content ['images']);
                foreach ($content ['images'] as $value) {
                    $value = $url ['path'] . $value;
                }
                unset($value);
                $content ['created_date'] = date('d-m-Y', $content ['created_date']);
            }
            unset($content);
        } catch (Exception $e) {
            
        }
        return $allData;
    }

    // get list content category
    public static function getListContents($arr, $sort = NULL, $count = NULL, $offset = NULL) {
        $objData = new Models_Content ();
        if ($sort == null)
            $sort = array(
                'sorting ASC'
            );
        $data = $objData->getByColumns($arr, $sort, $count, $offset)->toArray();
        $data = Nine_Query::formatData($data, $objData);
        return $data;
    }

    public static function getListProducts($arr, $sort = NULL, $count = NULL, $offset = NULL) {
        $objData = new Models_Product ();


        $arr['lang_id'] = Nine_Language::getCurrentLangId();

        if ($sort == null)
            $sort = array(
                'sorting ASC'
            );

        $data = $objData->getByColumns($arr, $sort, $count, $offset)->toArray();
        $data = Nine_Query::formatData($data, $objData);
        return $data;
    }

    public static function initHTML($arr) {
        // get path
        $url = Nine_Route::url();
        // get config
        $config = Nine_Query::getConfig();
        if (isset($config [$arr ['key']]))
            $data = $config [$arr ['key']];
        else
            $data = '';
        $html = array();
        $html ['value'] = $data;
        // name, type, editor
        if ($arr ['type'] == 'input') {
            $html ['html'] = '<div class="form-group">
                                            <label class="control-label col-md-3">' . $arr ['name'] . '</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[' . $arr ['key'] . ']"  value="' . $data . '" >
                                            </div>
                                        </div>';
        } elseif ($arr ['type'] == 'image') {
            $html ['html'] = '';
        }
        if ($arr ['type'] == 'textarea') {
            $html ['html'] = '<div class="form-group">
                                            <label class="control-label col-md-3">' . $arr ['name'] . '</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control textarea " name="data[' . $arr ['key'] . ']" >' . $data . '</textarea>
                                            </div>
                                        </div>';
        } elseif ($arr ['type'] == 'editor') {
            $html ['html'] = '';
        } elseif ($arr ['type'] == 'checkbox') {
            $html ['html'] = '';
        } elseif ($arr ['type'] == 'radiobutton') {
            $html ['html'] = '';
        } elseif ($arr ['type'] == 'upload') {
            if ($data == '' || $data != $url ['path'])
                $display_data = "display: none;";
            else
                $display_data = "";
            if ($data != '' || $data == $url ['path'])
                $display_none = "display: none;";
            else
                $display_none = "";
            $html ['html'] = '<div class="form-group">
                                        <label class="control-label col-md-3">' . $arr ['name'] . '</label>
                                        <div class="col-md-9">
                                            <input style="display: none;" type="file" name="' . $arr ['key'] . '" id="ip_' . $arr ['key'] . '">
                                            <input type="hidden" value="' . $data . '" name="data[' . $arr ['key'] . ']">
                                            <div class="form-group ">
                                                <div id="' . $arr ['key'] . '" class="col-md-3 select_' . $arr ['key'] . '" style="' . $display_data . ';">
                                                    <img src="' . $data . '" id="display_' . $arr ['key'] . '" style="max-width: 100%; max-height:80px; border:dashed blue thin;"/>
                                                </div>
                                                <div id="no_' . $arr ['key'] . '" class="col-md-3 select_' . $arr ['key'] . '" style="' . $display_none . 'width: 10%; border: thin blue dashed; text-align: center; padding:38px 0px;">
                                                    ' . $arr ['name'] . '
                                                </div>
                                            </div>
                                        </div>
                                    </div>' . "<script>jQuery(document).ready(function () {
                                        $('.select_" . $arr ['key'] . "').click(function (event) {
                                            $('#ip_" . $arr ['key'] . "').click();
                                        });

                                        $('#ip_" . $arr ['key'] . "').change(function (event) {
                                            var tmppath = URL.createObjectURL(event.target.files[0]);
                                            $('#display_" . $arr ['key'] . "').attr('src', URL.createObjectURL(event.target.files[0]));
                                            $('#no_" . $arr ['key'] . "').css('display', 'none');
                                            $('#" . $arr ['key'] . "').css('display', 'block');
                                        });

                                    });</script>";
        } elseif ($arr ['type'] == 'mul_upload') {
            $html ['html'] = "";
        }
        return $html;
    }

    // get list content category
    public static function getListContentCategory($arr, $sort = null) {
        $objCat = new Models_ContentCategory ();
        if ($sort == null)
            $sort = array(
                'sorting ASC'
            );
        $data = $objCat->getByColumns($arr, $sort)->toArray();
        $objData = new Models_Content ();
        $data = Nine_Query::formatData($data, $objData);
        return $data;
    }

    // get list content category
    public static function getListProductCategory($arr, $sort = null) {
        $objCat = new Models_ProductCategory ();
        if ($sort == null) {
            $sort = array(
                'sorting ASC'
            );
        }
        $data = $objCat->getByColumns($arr, $sort)->toArray();
        $objData = new Models_Product ();
        $data = Nine_Query::formatData($data, $objData);
        return $data;
    }

    // get list users
    public static function getListUsers($arr) {
        $objUser = new Models_User ();
        $data = $objUser->getByColumns($arr, array(
                    'user_id ASC'
                ))->toArray();
        $url = Nine_Route::url();
        foreach ($data as &$value) {
            $value ['topsuplier'] = $url ['path'] . 'estore/topsuplier/' . $value ['alias'] . '.html';
            $value ['image'] = $url ['path'] . "/" . $value ['image'];
            $value ['logo'] = $url ['path'] . "/" . $value ['logo'];
            $value ['icon'] = $url ['path'] . "/" . $value ['icon'];
        }
        unset($value);
        return $data;
    }

    // get list users
    public static function getListGroupUsers($arr) {
        $objUser = new Models_Group ();
        $data = $objUser->getByColumns($arr, array(
                    'sorting ASC'
                ))->toArray();
        return $data;
    }

    /*
     * Upload file
     */

    public static function UploadFile($objUpload) {
        $filePath = '';
        //Get url
        $url = Nine_Route::url();
        print_r($objUpload);
        if (isset($objUpload) && $objUpload['tmp_name'] != '') {

            $fileName = basename($objUpload['name']);
            $fileTmp = $objUpload['tmp_name'];
            $uploadPath = 'media/userfiles/files/';
            $ext_allow = array(
                'pdf', 'PDF'
            );

            //Check if localhost
            if (strpos($url['path'], 'localhost')) {
                $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/tuanphat/' . $uploadPath;
            } else {
                $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $uploadPath;
            }

            //Rename file name
            $arrFileName = explode('.', $fileName);
            $fileName = $arrFileName[0] . '_' . time() . '.' . $arrFileName[1];

            $uploadFile = $uploadDir . $fileName;
            $ext = pathinfo($uploadFile, PATHINFO_EXTENSION);

            //Check insupport format
            if (in_array($ext, $ext_allow) === false) {
                return array(
                    'status' => false,
                    'message' => Nine_Language::translate('Chỉ được phép nhập file PDF.')
                );
            }

            //Upload file
            move_uploaded_file($fileTmp, $uploadFile);
            $filePath = $uploadPath . $fileName;
        }
        return $filePath;
    }

}
