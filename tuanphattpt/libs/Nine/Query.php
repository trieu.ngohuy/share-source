<?php

/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2011 9fw.org
 * @license    http://license.9fw.org
 * @version    v 1.0 2009-04-15
 * 
 */
require_once 'Zend/Config.php';
require_once 'Zend/Config/Writer/Array.php';
require_once 'modules/content/models/Content.php';
require_once 'modules/content/models/ContentCategory.php';
require_once 'modules/product/models/Product.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/topsuplier/models/TopsuplierCategory.php';
require_once 'modules/user/models/User.php';
require_once 'modules/user/models/Group.php';
require_once 'modules/templates/models/Templates.php';
include_once 'modules/mail/models/phpmailer.php';
include_once 'modules/mail/models/smtp.php';

class Nine_Query {

    protected function __construct() {
        
    }

    //FUNCTION
    //Check file is exist on server
    public static function checkRemoteFile($url) {
        if (@getimagesize($url)) {
            return true;
        } else {
            return false;
        }
    }

    //FUNCTION
    //Convert vn to en
    //PARAMETERS:
    //pa1: str
    public static function convert_vi_to_en($str) {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        // $str = str_replace(" ", "-", str_replace("&*#39;","",$str));
        return $str;
    }

    //Check if username exist
    //DES: Check if username exist
    //pa1: username
    //RETURN: boolean
    function CheckUserName($username) {
        
    }

    // Not found page
    public static function NotFoundPage($gid) {
        
    }

    // set layouts and views base on user_id
    public static function setLayoutAndViews($arr) {
        $estorePath = 'default';
        $estoreHome = 'front';
        //Get params and set variables
        if (isset($arr['_request'])) {
            //Get parameters
            $params = $arr['_request']->getParams();
            if (isset($arr ['user_id']) && $arr ['user_id'] != null)
                $arr ['user_id'] = $params['user_id'];
            if (isset($arr ['alias']) && $arr ['alias'] != null)
                $arr ['alias'] = $params['alias'];
        }
        if ((isset($arr ['user_id']) && $arr ['user_id'] != null && $arr ['user_id'] != 0) || (isset($arr ['alias']) && $arr ['alias'] != null && $arr ['alias'] != "")) {

            // get estore
            if ($arr ['user_id'] != null && $arr ['user_id'] != 0)
                $estoreUser = @reset(Nine_Query::getListUsers(array(
                                    'user_id' => $arr ['user_id']
                )));

            elseif ($arr ['alias'] != null && $arr ['alias'] != "")
                $estoreUser = @reset(Nine_Query::getListUsers(array(
                                    'alias' => $arr ['alias']
                )));

            if ($estoreUser ['group_id'] == 3) {
                // get template
                $objTemp = new Models_Templates ();
                $estoreTemplate = @reset($objTemp->getByColumns(array(
                                    'templates_id' => $estoreUser ['template']
                                ))->toArray());
                // set path
                $estorePath = $estoreTemplate ['path'];
                $estorePath = 'estore-3';
                $estoreHome = 'helpers/themes/' . $estoreTemplate ['path'] . '/home';
                $estoreHome = 'helpers/themes/estore-3/home';
            }
        }
        // set layout
        Nine_Controller_Action::setLayout($estoreHome);
        $templatePath = Nine_Registry::getModuleName() . '/views/' . $estorePath;
        $templatePath .= '/' . $arr ['viewsName'] . '.' . Nine_Constant::VIEW_SUFFIX;
        $arr ['view']->html = $arr ['view']->render($templatePath);
    }

    // breadcrumbs
    public static function getBreadCumbs($gid, $type) {
        $breadCrubms = array();
        $breadCrubms ['home'] = array(
            'name' => Nine_Language::translate("Home"),
            'url' => Nine_Route::_("default/index", "")
        );
        $url = Nine_Route::url();
        switch ($type) {
            case 'product' :
                // GET PRODUCT DETAIL
                $detail = @reset(Nine_Query::getListProducts(array(
                                    'product_gid' => $gid
                                        ), null));
                $breadCrubms ['detail'] = array(
                    'name' => $detail ['title'],
                    'url' => $detail ['url']
                );
                // GET CATEGORY PARENT
                $category = @reset(Nine_Query::getListProductCategory(array(
                                    'product_category_gid' => $detail ['product_category_gid']
                                        ), null));
                $breadCrubms ['cat'] = array(
                    'name' => $category ['name'],
                    'url' => $category ['url']
                );

                break;
            case 'product-category' :
                if ($gid == 0) {

                    // GET PRODUCT DETAIL					
                    $breadCrubms ['detail'] = array(
                        'name' => 'Danh sách sản phẩm',
                        'url' => $url['path'] . 'category/danh-sach-san-pham'
                    );
                    // GET PRODUCT CATEGORY					
                    // $breadCrubms ['cat'] = array (
                    // 		'name' => 'Danh sách sản phẩm',
                    // 		'url' =>  $url['path'] . 'category/danh-sach-san-pham'
                    // );
                } elseif ($gid == 1) {
                    // GET PRODUCT DETAIL					
                    $breadCrubms ['detail'] = array(
                        'name' => 'Sản phẩm bán chạy',
                        'url' => $url['path'] . 'category/best-sale'
                    );
                    // GET PRODUCT CATEGORY					
                    // $breadCrubms ['cat'] = array (
                    // 		'name' => 'Sản phẩm bán chạy',
                    // 		'url' =>  $url['path'] . 'category/best-sale'
                    // );
                } elseif ($gid == 2) {
                    // GET PRODUCT DETAIL					
                    $breadCrubms ['detail'] = array(
                        'name' => 'Sản phẩm hot',
                        'url' => $url['path'] . 'category/hot-product'
                    );
                    // // GET PRODUCT CATEGORY					
                    // $breadCrubms ['cat'] = array (
                    // 		'name' => 'Sản phẩm hot',
                    // 		'url' =>  $url['path'] . 'category/hot-product'
                    // );
                } else {
                    // GET PRODUCT DETAIL
                    $detail = @reset(Nine_Query::getListProductCategory(array(
                                        'product_category_gid' => $gid
                                            ), null));

                    //Get all product categories
                    $allCat = Nine_Query::getListProductCategory(array());
                    //Get list id
                    $listId = Nine_Query::getListProductCategroryReverseId($allCat, $detail ['parent_id']);

                    $listId = explode(",", substr($listId, 0, strlen($listId) - 2));


                    $breadCrubms = array();

                    foreach ($listId as $value) {
                        if ($value != " " && $value != $detail ['product_category_gid']) {
                            $category = @reset(Nine_Query::getListProductCategory(array(
                                                'product_category_gid' => $value
                            )));
                            $breadCrubms[] = array(
                                'name' => $category['name'],
                                'url' => $url['path'] . 'category/' . $category['alias']
                            );
                        }
                    }

                    // print_r($breadCrubms);
                    // foreach ($breadCrubms as $key => $value) {
                    // 	echo $value['name'];
                    // }

                    $breadCrubms[] = array(
                        'name' => $detail['name'],
                        'url' => $detail['url']
                    );
                }

                break;
            case 'content' :
                // GET CONTENTS DETAIL
                $detail = @reset(Nine_Query::getListContents(array(
                                    'content_gid' => $gid
                                        ), null));
                $breadCrubms ['detail'] = array(
                    'name' => $detail ['title'],
                    'url' => $detail ['url']
                );
                // GET CATEGORY PARENT
                $category = @reset(Nine_Query::getListContentCategory(array(
                                    'content_category_gid' => $detail ['content_category_gid']
                                        ), null));
                $breadCrubms ['cat'] = array(
                    'name' => $category ['name'],
                    'url' => $category ['url']
                );
                break;
            case 'content-category' :
                if ($gid == 0) {
                    // GET CONTENT DETAIL
                    $breadCrubms ['detail'] = array(
                        'name' => '',
                        'url' => ''
                    );
                    // GET PRODUCT CONTENT
                    $breadCrubms ['cat'] = array(
                        'name' => 'Danh sách tin tức',
                        'url' => $url['path'] . 'news-page/danh-sach-tin-tuc'
                    );
                } else {
                    // GET CONTENT DETAIL
                    $detail = @reset(Nine_Query::getListContentCategory(array(
                                        'content_category_gid' => $gid
                                            ), null));
                    $breadCrubms ['detail'] = array(
                        'name' => $detail ['name'],
                        'url' => $detail ['url']
                    );
                    // GET PRODUCT CONTENT
                    $category = @reset(Nine_Query::getListContentCategory(array(
                                        'parent_id' => $detail ['content_category_gid']
                                            ), null));
                    $breadCrubms ['cat'] = array(
                        'name' => $category ['name'],
                        'url' => $category ['url']
                    );
                }
                break;
            default :
                break;
        }
        return $breadCrubms;
    }

    // get logged user
    public static function getLoggedUser() {
        $loggedUser = array();
        if (isset($_SESSION ['user_login'])) {
            $loggedUser = $_SESSION ['user_login'];
            if ($loggedUser != '') {
                $loggedUser ['redic'] = Nine_Query::makeUpPrice($loggedUser ['redic']);
            }
            return $loggedUser;
        } else {
            // get addmin information
            $admin = @reset(Nine_Query::getListUsers(array()));
        }

        return $loggedUser;
    }

    // make up alias
    public static function makeUpAlias($_) {
        return str_replace(" ", "-", str_replace("&*#39;", "", trim($_)));
    }

    // makeup price
    public static function makeUpPrice($_) {
        return strrev((string) preg_replace('/(\d{3})(?=\d)(?!\d*\.)/', '$1,', strrev($_)));
    }

    public static function cleanString($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    // check upload image
    public static function uploadImage($image) {
        $upload = array();
        $target_dir = 'media/userfiles/user/';
        $target_dir = $target_dir . basename($image ["name"]);
        // Check if file already exists
        if (file_exists($target_dir . $image ["name"])) {
            $upload ['erro'] = 0;
            $upload ['mess'] = Nine_Language::translate('Tên File Đã Tồn Tại');
            $upload ['image'] = "";
        }
        // Check file size
        if ($image ["size"] > 20000000) {
            $upload ['erro'] = 0;
            $upload ['mess'] = Nine_Language::translate('File Lớn Hơn 20Mb.');
            $upload ['image'] = "";
        }
        // Only GIF files allowed
        if ($image ["type"] != "image/gif" && $image ["type"] != "image/jpg" && $image ["type"] != "image/png" && $image ["type"] != "image/jpeg") {
            $upload ['erro'] = 0;
            $upload ['mess'] = Nine_Language::translate('Chỉ nhận file png / jpg / gif / jpeg .');
            $upload ['image'] = "";
        }
        if (isset($upload ['erro'])) {
            $upload ['erro'] = 0;
            $upload ['mess'] = Nine_Language::translate('Image can\' be uploaded.');
            $upload ['image'] = "";
        } else {
            if (move_uploaded_file($image ["tmp_name"], $target_dir)) {
                $upload ['erro'] = 1;
                $upload ['image'] = $target_dir;
            } else {
                die();
                $upload ['erro'] = 1;
                $upload ['mess'] = Nine_Language::translate('Sever Not Upload File . ');
                $upload ['image'] = "";
            }
        }
        return $upload;
    }

    // get estore menu
    public static function getEstoreMenu($alias) {
        $url = Nine_Route::url();
        $estoreMenu = array();
        $estoreMenu ['home'] = array(
            "name" => Nine_Language::translate("Home"),
            "url" => $url ['path'] . $alias . ".htm"
        );
        $estoreMenu ['contact'] = array(
            "name" => Nine_Language::translate("Contact"),
            "url" => $url ['path'] . 'estore/contact-us/' . $alias . ".html"
        );
        $estoreMenu ['editInfo'] = array(
            "name" => Nine_Language::translate("Edit info"),
            "url" => $url ['path'] . 'estore/edit-info/' . $alias . ".html"
        );
        $estoreMenu ['resetPass'] = array(
            "name" => Nine_Language::translate("Edit info"),
            "url" => $url ['path'] . 'estore/change-password' . $alias . ".html"
        );

        $estoreMenu ['setting'] = array(
            "name" => Nine_Language::translate("Edit info"),
            "url" => $url ['path'] . 'estore/setting/' . $alias . ".html"
        );
        $estoreMenu ['search'] = array(
            "name" => Nine_Language::translate("Search"),
            "url" => $url ['path'] . 'estore/search/' . $alias . ".html"
        );

        $estoreMenu ['cart'] = array(
            "client" => array(
                "name" => Nine_Language::translate("Cart"),
                "url" => $url ['path'] . "cart/" . $alias . ".html"
            ),
            "admin" => array(
                "name" => Nine_Language::translate("Manager Cart"),
                "url" => $url ['path'] . "estore/manage-cart/" . $alias . ".html"
            )
        );
        $estoreMenu ['user'] = array(
            "login" => array(
                "name" => Nine_Language::translate("Login"),
                "url" => $url ['path'] . "estore/login/" . $alias . ".htm"
            ),
            "register" => array(
                "name" => Nine_Language::translate("Register"),
                "url" => $url ['path'] . "estore/register/" . $alias . ".html"
            ),
            "profile" => array(
                "name" => Nine_Language::translate("Profile"),
                "url" => $url ['path'] . "estore/profile/" . $alias . ".html"
            )
        );
        $estoreMenu ['products'] = array(
            "managerProducts" => array(
                "name" => Nine_Language::translate("Manager Products"),
                "url" => $url ['path'] . "estore/manage-products/" . $alias . ".html"
            ),
            "managerCategorys" => array(
                "name" => Nine_Language::translate("Manager Products"),
                "url" => $url ['path'] . "estore/manage-product-category/" . $alias . ".html"
            )
        );

        return $estoreMenu;
    }

    public static function sendMail($listEmail, $title, $content, $data = null) {
        $objUser = new Models_User ();
        // get admin email
        $admin = @reset($objUser->getByUserName('admin'));
        $adminemail = $admin ['email'];
        // get config
        $config = Nine_Query::getConfig();

        $data_send = array(
            'name_to' => $data == null || isset($data ['full_name']) ? $config['WebsiteName'] : $data ['full_name'],
            'email_to' => array_merge(
                    array(
                $adminemail,
                $config ['Email']
                    ), $listEmail
            ),
            'title_email' => $title,
            'name_from' => $config ['WebsiteName'],
            'content' => $content
        );
        //SEND MAIL
        Nine_Query::sendmail_smtp($data_send);
    }

    public static function sendmail_smtp($data) {
        $oldSettings = include 'setting.php';
        $config = $oldSettings;
        $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
        $mail->IsSMTP(); // telling the class to use SMTP
        try {
            //echo $config['Email_Username'] . '/' . $config['Email_Password'];
            //$mail->Host       = "mail.gmail.com"; // SMTP server
            //$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
            $mail->SMTPAuth = true;                  // enable SMTP authentication
            $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
            $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
            $mail->Port = 465;   // set the SMTP port for the GMAIL server
            $mail->SMTPKeepAlive = true;
            $mail->Mailer = "smtp";
            $mail->Username = $config['Email_Username'];  // GMAIL username
            $mail->Password = $config['Email_Password'];            // GMAIL password
            $to = $data['email_to'];
            $name = $data['name_to'];
            if (is_array($to)) {
                foreach ($to as $key => $sto) {
                    $mail->AddAddress($sto, '');
                }
            } else {
                $mail->AddAddress($to, $name);
            }
            $mail->CharSet = "UTF-8";
            $mail->WordWrap = 50;
            //$mail->AddAddress('trieu.ngohuy@gmail.com', 'abc');

            $mail->SetFrom($config['Email_Username'], $data['name_from']);
            $mail->Subject = $data['title_email'];
            $mail->AltBody = $data['content']; // optional - MsgHTML will create an alternate automatically
            $mail->MsgHTML($data['content']);
            $mail->Send();
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }

    /* FUNCTION - SET CART */

    public static function setCart($data, $session, $view) {
        if ($data != false) {

            //Get cart	
            //$session->cart = array();
            $allCard = $session->cart;
            if ($allCard == '') {
                $allCard = array();
            }

            //Check if product has exist in array
            $count = false;

            if (count($allCard) > 0) {
                foreach ($allCard['cartDetail'] as $item) {
                    if ($item ['product']['product_gid'] == $data ['product_gid']) {
                        $count = true;
                        break;
                    }
                }
            }
            //If product has exist in cart
            if ($count == true) {
                $view->cartMessage = array(
                    'success' => false,
                    'message' => Nine_Language::translate("Sản Phẩm Đã Tồn Tại Trong Giỏ Hàng.")
                );
            } else {
                $temp = array();
                $cart = array();

                //GET PRODUCT DETAIL
                $temp ['product'] = @reset(Nine_Query::getListProducts(array(
                                    'product_gid' => $data ['product_gid']
                )));

                //Update mount
                $temp ['mount'] = $data['mount'];

                /* GET PRICE OR PRICE_SALE */
                $temp_price = 0;
                if ($temp['product']['price_sale'] != '')
                    $temp_price = $temp['product']['price_sale'];
                else
                    $temp_price = $temp['product']['price'];

                //CONVERT PRICE TO INT
                $temp_price = str_replace(",", "", $temp_price);
                $temp_price = str_replace(".", "", $temp_price);

                //CALCULATING PRICE
                $temp ['product'] ['total_price'] = str_replace(".", ",", Nine_Query::makeUpPrice($temp_price * $data ['mount']));
                $temp ['product'] ['tprice_prase'] = str_replace(".", ",", Nine_Query::makeUpPrice($temp_price));
                //Update cart		
                $allCard ['cartDetail'][] = $temp;

                //calcualting cart
                Nine_Query::CalCart($allCard, $session);

                //Set message
                $view->cartMessage = array(
                    'success' => true,
                    'message' => Nine_Language::translate("Đã Thêm Sản Phẩm Vào Giỏ Hàng.")
                );
            }
        }
    }

    /*
     * SUMMARY: Calculating cart	
     */

    public static function CalCart($allCard, $session) {
        $cart = array();
        //Calculating total price
        $total_price = 0;
        foreach ($allCard['cartDetail'] as &$item) {

            /* GET PRICE OR PRICE_SALE */
            $temp_price = 0;
            if ($item['product']['price_sale'] != '')
                $temp_price = $item['product']['price_sale'];
            else
                $temp_price = $item['product']['price'];

            //CONVERT PRICE TO INT
            $temp_price = str_replace(",", "", $temp_price);
            $temp_price = str_replace(".", "", $temp_price);

            //CALCULATING PRICE
            $total_price += ($temp_price * $item ['mount']);
            //CALCULATING PRICE
            $item ['product'] ['total_price'] = str_replace(".", ",", Nine_Query::makeUpPrice($temp_price * $item ['mount']));
        }
        unset($item);
        //REMAKE TOTAL PRICE
        $total_price = str_replace(".", ",", Nine_Query::makeUpPrice($total_price));
        $cart ['cartDetail'] = $allCard['cartDetail'];
        $cart ['total_price'] = $total_price;

        //Update session
        $session->cart = $cart;
    }

    /* FUNCTION - GET CART */

    public static function getCart($session, $view = null) {
        
        /*
         * Get list cart
         */
        $allCard = $session->cart;
        /*
         * Check if cart is empty
         */
        if (count($allCard) <= 0) {
            $allCard = array(
                'cartDetail' => array(),
                'total_price' => 0
            );
        }
        /*
         * Set to view
         */
        if($view != null){
            $view->allCard = $allCard;
        }
        /*
         * Return list cart
         */
        return $session->cart;
        // $cart = array ();
        // // get cart
        // $allCard = $session->cart;			
        // //$session->cart = '';
        // if ($allCard == '') {
        // 	$allCard = array ();
        // }
        // $total_price = 0;
        // $objProductDetail = new Models_Product ();
        // if(count($allCard) > 0 ){
        // 	foreach ( $allCard as &$item ) {
        // 		//GET PRODUCT DETAIL
        // 		$item ['product'] = @reset ( Nine_Query::getListProducts ( array (
        // 				'product_gid' => $item ['product_gid'] 
        // 		) ) );
        // 		/*GET PRICE OR PRICE_SALE*/
        // 		$temp_price = 0;
        //            if($item['product']['price_sale'] != '')
        //                $temp_price = $item['product']['price_sale'];
        //            else $temp_price = $item['product']['price'];
        //           	//CONVERT PRICE TO INT
        // 		$temp_price = str_replace ( ",", "", $temp_price );
        // 		$temp_price = str_replace ( ".", "", $temp_price );
        // 		//CALCULATING PRICE
        // 		$total_price += ($temp_price * $item ['mount']);
        // 		$item ['product'] ['total_price'] = str_replace ( ".", ",", Nine_Query::makeUpPrice ( $temp_price * $item ['mount'] ) );
        // 		$item ['product'] ['tprice_prase'] = str_replace ( ".", ",", Nine_Query::makeUpPrice ( $temp_price ) );
        // 	}
        // 	unset ( $item );	
        // }
        // //REMAKE TOTAL PRICE
        // $total_price = str_replace ( ".", ",", Nine_Query::makeUpPrice ( $total_price ) );
        // $cart ['cartDetail'] = $allCard;
        // $cart ['total_price'] = $total_price;
        // return $cart;
    }

    // get translate menu
    public static function getTranslateMenu($arr) {
        $translateText = array();
        foreach ($arr as $key => $value) {
            $translateText [$key] ['url'] = Nine_Route::_($value ['url'], array(
                        'alias' => $value ['alias']
            ));
            $translateText [$key] ['name'] = Nine_Language::translate($value ['name']);
        }
        return $translateText;
    }

    /**
     * Singleton instance
     *
     * @return Nine_Language
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self ();
        }

        return self::$_instance;
    }

    // export excel
    public static function exportExcel($export) {
        if ($export != false) {
            $objType = new Models_Type ();

            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $style = "style = 'border: 1px solid;text-align:left'";
            $style1 = "style = 'border: 1px solid;font-weight: bold;'";

            $allProductType = $objType->getAllTypes();

            $header = "";
            $header .= "<tr>";
            $header .= "<td $style1>CÔNG TY TNHH SX - TM LẬP THÀNH PHÁT QUỐC TẾ</td>";
            $header .= "</tr>";
            $header .= "<tr>";
            $header .= "<td $style>40/1 Đường 18, tổ 24, KP 2, P. Thạnh Mỹ Lợi, Q2, TP.HCM</td>";
            $header .= "</tr>";
            $header .= "<tr>";
            $header .= "<td $style>Điện thoại: 08.3743.8480 - Fax: 08.3743.7719</td>";
            $header .= "</tr>";
            $header .= "<tr>";
            $header .= "<td $style1>No</td>";
            $header .= "<td $style1>Mã Sản Phẩm</td>";
            $header .= "<td $style1>Tên Sản Phẩm</td>";
            $header .= "<td $style1>Kiểu</td>";
            $header .= "<td $style1>Số Lượng</td>";
            $header .= "<td $style1>Giá</td>";
            $header .= "</tr>";
            $content = '';
            $no = 1;

            foreach ($allProductType as $item) {
                $content .= "<tr>";
                $content .= "<td $style>" . $no . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item ['product_gid'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item ['title'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item ['type'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($item ['mount'], 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "<td $style>" . mb_convert_encoding($objProduct->makeUpPrice($item ['price']), 'HTML-ENTITIES', 'UTF-8') . "</td>";
                $content .= "</tr>";
                $no ++;
            }

            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("content-disposition: attachment;filename=thong-ke-product-" . date('d-m-Y H:i:s') . ".xls");

            $xlsTbl = $header;
            $xlsTbl .= $content;

            echo "<table style='font-family: arial, verdana, sans-serif;' >$xlsTbl</table>";
            exit();
        }
    }

    // get list country and provines
    public static function getPlaces() {
        $places = array();
        $places ['countries'] = array(
            "Afghanistan",
            "Albania",
            "Algeria",
            "American Samoa",
            "Andorra",
            "Angola",
            "Anguilla",
            "Antarctica",
            "Antigua and Barbuda",
            "Argentina",
            "Armenia",
            "Aruba",
            "Australia",
            "Austria",
            "Azerbaijan",
            "Bahamas",
            "Bahrain",
            "Bangladesh",
            "Barbados",
            "Belarus",
            "Belgium",
            "Belize",
            "Benin",
            "Bermuda",
            "Bhutan",
            "Bolivia",
            "Bosnia and Herzegowina",
            "Botswana",
            "Bouvet Island",
            "Brazil",
            "British Indian Ocean Territory",
            "Brunei Darussalam",
            "Bulgaria",
            "Burkina Faso",
            "Burundi",
            "Cambodia",
            "Cameroon",
            "Canada",
            "Cape Verde",
            "Cayman Islands",
            "Central African Republic",
            "Chad",
            "Chile",
            "China",
            "Christmas Island",
            "Cocos (Keeling) Islands",
            "Colombia",
            "Comoros",
            "Congo",
            "Congo, the Democratic Republic of the",
            "Cook Islands",
            "Costa Rica",
            "Cote d'Ivoire",
            "Croatia (Hrvatska)",
            "Cuba",
            "Cyprus",
            "Czech Republic",
            "Denmark",
            "Djibouti",
            "Dominica",
            "Dominican Republic",
            "East Timor",
            "Ecuador",
            "Egypt",
            "El Salvador",
            "Equatorial Guinea",
            "Eritrea",
            "Estonia",
            "Ethiopia",
            "Falkland Islands (Malvinas)",
            "Faroe Islands",
            "Fiji",
            "Finland",
            "France",
            "France Metropolitan",
            "French Guiana",
            "French Polynesia",
            "French Southern Territories",
            "Gabon",
            "Gambia",
            "Georgia",
            "Germany",
            "Ghana",
            "Gibraltar",
            "Greece",
            "Greenland",
            "Grenada",
            "Guadeloupe",
            "Guam",
            "Guatemala",
            "Guinea",
            "Guinea-Bissau",
            "Guyana",
            "Haiti",
            "Heard and Mc Donald Islands",
            "Holy See (Vatican City State)",
            "Honduras",
            "Hong Kong",
            "Hungary",
            "Iceland",
            "India",
            "Indonesia",
            "Iran (Islamic Republic of)",
            "Iraq",
            "Ireland",
            "Israel",
            "Italy",
            "Jamaica",
            "Japan",
            "Jordan",
            "Kazakhstan",
            "Kenya",
            "Kiribati",
            "Korea, Democratic People's Republic of",
            "Korea, Republic of",
            "Kuwait",
            "Kyrgyzstan",
            "Lao, People's Democratic Republic",
            "Latvia",
            "Lebanon",
            "Lesotho",
            "Liberia",
            "Libyan Arab Jamahiriya",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Macau",
            "Macedonia, The Former Yugoslav Republic of",
            "Madagascar",
            "Malawi",
            "Malaysia",
            "Maldives",
            "Mali",
            "Malta",
            "Marshall Islands",
            "Martinique",
            "Mauritania",
            "Mauritius",
            "Mayotte",
            "Mexico",
            "Micronesia, Federated States of",
            "Moldova, Republic of",
            "Monaco",
            "Mongolia",
            "Montserrat",
            "Morocco",
            "Mozambique",
            "Myanmar",
            "Namibia",
            "Nauru",
            "Nepal",
            "Netherlands",
            "Netherlands Antilles",
            "New Caledonia",
            "New Zealand",
            "Nicaragua",
            "Niger",
            "Nigeria",
            "Niue",
            "Norfolk Island",
            "Northern Mariana Islands",
            "Norway",
            "Oman",
            "Pakistan",
            "Palau",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Philippines",
            "Pitcairn",
            "Poland",
            "Portugal",
            "Puerto Rico",
            "Qatar",
            "Reunion",
            "Romania",
            "Russian Federation",
            "Rwanda",
            "Saint Kitts and Nevis",
            "Saint Lucia",
            "Saint Vincent and the Grenadines",
            "Samoa",
            "San Marino",
            "Sao Tome and Principe",
            "Saudi Arabia",
            "Senegal",
            "Seychelles",
            "Sierra Leone",
            "Singapore",
            "Slovakia (Slovak Republic)",
            "Slovenia",
            "Solomon Islands",
            "Somalia",
            "South Africa",
            "South Georgia and the South Sandwich Islands",
            "Spain",
            "Sri Lanka",
            "St. Helena",
            "St. Pierre and Miquelon",
            "Sudan",
            "Suriname",
            "Svalbard and Jan Mayen Islands",
            "Swaziland",
            "Sweden",
            "Switzerland",
            "Syrian Arab Republic",
            "Taiwan, Province of China",
            "Tajikistan",
            "Tanzania, United Republic of",
            "Thailand",
            "Togo",
            "Tokelau",
            "Tonga",
            "Trinidad and Tobago",
            "Tunisia",
            "Turkey",
            "Turkmenistan",
            "Turks and Caicos Islands",
            "Tuvalu",
            "Uganda",
            "Ukraine",
            "United Arab Emirates",
            "United Kingdom",
            "United States",
            "United States Minor Outlying Islands",
            "Uruguay",
            "Uzbekistan",
            "Vanuatu",
            "Venezuela",
            "Vietnam",
            "Virgin Islands (British)",
            "Virgin Islands (U.S.)",
            "Wallis and Futuna Islands",
            "Western Sahara",
            "Yemen",
            "Yugoslavia",
            "Zambia",
            "Zimbabwe"
        );
        $places ['provinces'] = array(
            "Hà Nội",
            "Huế",
            "Đà nẵng",
            "Hồ chí minh"
        );
        return $places;
    }

    // get config
    public static function getConfig() {
        require_once 'Zend/Config.php';
        // list
        $oldSettings = include 'setting.php';
        $url = Nine_Route::url();
        $oldSettings ['Logo'] = $url ['path'] . $oldSettings ['Logo'];
        return $oldSettings;
    }

    public static function setConfig($arr) {
        /**
         * Save config
         */
        try {
            $oldConfig = Nine_Query::getConfig();
            $writer = new Zend_Config_Writer_Array ();
            $writer->setExclusiveLock(true);
            $writer->write('setting.php', new Zend_Config(array_merge($oldConfig, $arr)));
            $writer->setExclusiveLock(false);

            return true;
        } catch (Exception $e) {
            return $e;
        }
    }

    // get params
    public static function getParams($arr) {
        $arr = array();
        foreach ($arr as $value) {
            $arr [$value] = Nine_Query::_getParam($value, false);
        }

        return $arr;
    }

    public static function getListProductCategroryReverseId($listData, $product_category_gid) {
        if (isset($listId)) {
            $listId .= " ";
        } else {
            $listId = "";
        }
        foreach ($listData as $value) {
            if ($value ['product_category_gid'] == $product_category_gid) {                               
                if($value ['parent_id'] != 0 && $value ['parent_id'] != ""){
                    $listId .= "," . $value ['parent_id'];
                    $listId .= ", " . Nine_Query::getListProductCategroryReverseId($listData, $value ['parent_id']);    
                }                
            }
        }
        return substr($listId, 1);
    }

    public static function getListProductCategroryId($listData, $parentId) {
        if (isset($listId)) {
            $listId .= " ";
        } else {
            $listId = "";
        }
        foreach ($listData as $value) {
            if ($value ['parent_id'] == $parentId) {
                $listId .= "," . $value ['product_category_id'];

                $listId .= ", " . Nine_Query::getListProductCategroryId($listData, $value ['product_category_id']);
            }
        }
        return substr($listId, 1);
    }

    //Init estore combobox
    public static function buildTreeEstore($listData, $parentId = null, $character = null) {
        if ($parentId == null) {
            $parentId = array();
        }
        if ($character == null) {
            $character = "";
        }
        if (isset($outputArray)) {
            // $outputArray[] .= $outputArray;
        } else {
            $outputArray = array();
        }

        foreach ($listData as $value) {

            if (is_array($parentId)) {
                $outputArray[] = array(
                    'name' => $character . " " . $value ['name'],
                    'estore_category_gid' => $value ['estore_category_gid']
                );
                if (array_key_exists($value['parent_id'], $parentId)) {
                    $outputArray = array_merge($outputArray, Nine_Query::buildTree($listData, $value ['parent_id'], $character . "--"));
                }
            } else {

                if ($value['parent_id'] == $parentId) {
                    $outputArray[] = array(
                        'name' => $character . " " . $value ['name'],
                        'estore_category_gid' => $value ['estore_category_gid']
                    );
                    $outputArray = array_merge($outputArray, Nine_Query::buildTree($listData, $value ['parent_id'], $character . "--"));
                }
            }
        }
        return $outputArray;
    }

    public static function buildTree($listData, $parentId = null, $character = null) {
        if ($parentId == null) {
            $parentId = 0;
        }
        if ($character == null) {
            $character = "";
        }
        if (isset($outputArray)) {
            // $outputArray[] .= $outputArray;
        } else {
            $outputArray = array();
        }

        foreach ($listData as $value) {
            if ($value['parent_id'] == $parentId) {
                if (isset($value ['product_category_gid'])) {
                    $temp = 'product_category_gid';
                } elseif (isset($value ['content_category_gid'])) {
                    $temp = 'content_category_gid';
                } else {
                    $temp = 'estore_category_gid';
                }
                $outputArray[] = array(
                    'name' => $character . " " . $value ['name'],
                    $temp => $value [$temp]
                );
                $outputArray = array_merge($outputArray, Nine_Query::buildTree($listData, $value [$temp], $character . "--"));
            }
        }
        return $outputArray;
    }

    public static function getListContentCategoryId($listData, $parentId) {
        if (isset($listId)) {
            $listId .= "";
        } else {
            $listId = "";
        }
        foreach ($listData as $value) {
            if ($value ['parent_id'] == $parentId) {
                $listId .= "," . $value ['content_category_id'];
                $listId .= Nine_Query::getListContentCategoryId($listData, $value ['content_category_id']);
            }
        }
        return substr($listId, 1);
    }

    public static function formatData($allData, $objData) {
        try {
            // get url
            $url = Nine_Route::url();
            foreach ($allData as &$content) {
                if ($content ['alias'] == "") {
                    $content ['alias'] = Nine_Query::convert_vi_to_en($content ['alias']);
                    $content ['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", trim($content ['alias'])));
                }
//                if (isset($content['title'])) {
//                    $content['title'] = substr($content['title'], 0, 43);
//                }

                $gid = 0;
                $tempPath = 'index';
                $mainPath = 'content';
                if (isset($content ['product_category_gid'])) {
                    $mainPath = 'product';
                    if (isset($content ['product_gid'])) {
                        $gid = $content ['product_gid'];
                        $tempPath = 'detail';
                    } else {
                        $gid = $content ['product_category_gid'];
                    }
                } elseif (isset($content ['content_category_gid'])) {
                    if (isset($content ['content_gid'])) {
                        $gid = $content ['content_gid'];
                        $tempPath = 'detail';
                    } else {
                        $gid = $content ['content_category_gid'];
                    }
                } elseif (isset($content ['estore_category_gid'])) {
                    $gid = $content ['estore_category_gid'];
                }

                if (isset($content ['price'])) {
                    $content ['price'] = Nine_Query::makeUpPrice($content ['price']);
                }
                if (isset($content ['price_sale'])) {
                    $content ['price_sale'] = Nine_Query::makeUpPrice($content ['price_sale']);
                }
                $content ['url'] = Nine_Route::_("{$mainPath}/index/{$tempPath}", array(
                            'alias' => $content ['alias']
                ));

                if (isset($content ['price']) && isset($content ['price_sale'])) {
                    $content ['price'] = str_replace('.', ',', Nine_Query::makeUpPrice($content ['price']));
                    $content ['price_sale'] = str_replace('.', ',', Nine_Query::makeUpPrice($content ['price_sale']));
                }

                if (isset($content ['images'])) {
                    $tmp = explode('||', $content ['images']);
                    $content ['main_image'] = $url ['path'] . $tmp [0];
                }
                if (isset($content ['icon'])) {
                    $content ['icon'] = $url ['path'] . $content ['icon'];
                }
                $content ['images'] = explode('||', $content ['images']);
                foreach ($content ['images'] as $value) {
                    $value = $url ['path'] . $value;
                }
                unset($value);
                $content ['created_date'] = date('d-m-Y', $content ['created_date']);
            }
            unset($content);
        } catch (Exception $e) {
            
        }
        return $allData;
    }

    // get list content category
    public static function getListContents($arr, $sort = NULL, $count = NULL, $offset = NULL) {
        $objData = new Models_Content ();
        /*
         * Add lang condition
         */
        $arr['lang_id'] = Nine_Language::getCurrentLangId();
        /*
         * Sort
         */
        if ($sort == null)
            $sort = array(
                'sorting ASC'
            );
        $data = $objData->getByColumns($arr, $sort, $count, $offset)->toArray();
        $data = Nine_Query::formatData($data, $objData);
        return $data;
    }

    public static function getListProducts($arr, $sort = NULL, $count = NULL, $offset = NULL) {
        $objData = new Models_Product ();


        $arr['lang_id'] = Nine_Language::getCurrentLangId();

        if ($sort == null)
            $sort = array(
                'sorting ASC'
            );

        $data = $objData->getByColumns($arr, $sort, $count, $offset)->toArray();
        $data = Nine_Query::formatData($data, $objData);
        return $data;
    }

    public static function initHTML($arr) {
        // get path
        $url = Nine_Route::url();
        // get config
        $config = Nine_Query::getConfig();
        if (isset($config [$arr ['key']]))
            $data = $config [$arr ['key']];
        else
            $data = '';
        $html = array();
        $html ['value'] = $data;
        // name, type, editor
        if ($arr ['type'] == 'input') {
            $html ['html'] = '<div class="form-group">
                                            <label class="control-label col-md-3">' . $arr ['name'] . '</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="data[' . $arr ['key'] . ']"  value="' . $data . '" >
                                            </div>
                                        </div>';
        } elseif ($arr ['type'] == 'image') {
            $html ['html'] = '';
        }
        if ($arr ['type'] == 'textarea') {
            $html ['html'] = '<div class="form-group">
                                            <label class="control-label col-md-3">' . $arr ['name'] . '</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control textarea " name="data[' . $arr ['key'] . ']" >' . $data . '</textarea>
                                            </div>
                                        </div>';
        } elseif ($arr ['type'] == 'editor') {
            $html ['html'] = '';
        } elseif ($arr ['type'] == 'checkbox') {
            $html ['html'] = '';
        } elseif ($arr ['type'] == 'radiobutton') {
            $html ['html'] = '';
        } elseif ($arr ['type'] == 'upload') {
            if ($data == '' || $data != $url ['path'])
                $display_data = "display: none;";
            else
                $display_data = "";
            if ($data != '' || $data == $url ['path'])
                $display_none = "display: none;";
            else
                $display_none = "";
            $html ['html'] = '<div class="form-group">
                                        <label class="control-label col-md-3">' . $arr ['name'] . '</label>
                                        <div class="col-md-9">
                                            <input style="display: none;" type="file" name="' . $arr ['key'] . '" id="ip_' . $arr ['key'] . '">
                                            <input type="hidden" value="' . $data . '" name="data[' . $arr ['key'] . ']">
                                            <div class="form-group ">
                                                <div id="' . $arr ['key'] . '" class="col-md-3 select_' . $arr ['key'] . '" style="' . $display_data . ';">
                                                    <img src="' . $data . '" id="display_' . $arr ['key'] . '" style="max-width: 100%; max-height:80px; border:dashed blue thin;"/>
                                                </div>
                                                <div id="no_' . $arr ['key'] . '" class="col-md-3 select_' . $arr ['key'] . '" style="' . $display_none . 'width: 10%; border: thin blue dashed; text-align: center; padding:38px 0px;">
                                                    ' . $arr ['name'] . '
                                                </div>
                                            </div>
                                        </div>
                                    </div>' . "<script>jQuery(document).ready(function () {
                                        $('.select_" . $arr ['key'] . "').click(function (event) {
                                            $('#ip_" . $arr ['key'] . "').click();
                                        });

                                        $('#ip_" . $arr ['key'] . "').change(function (event) {
                                            var tmppath = URL.createObjectURL(event.target.files[0]);
                                            $('#display_" . $arr ['key'] . "').attr('src', URL.createObjectURL(event.target.files[0]));
                                            $('#no_" . $arr ['key'] . "').css('display', 'none');
                                            $('#" . $arr ['key'] . "').css('display', 'block');
                                        });

                                    });</script>";
        } elseif ($arr ['type'] == 'mul_upload') {
            $html ['html'] = "";
        }
        return $html;
    }

    // get list content category
    public static function getListContentCategory($arr, $sort = null) {
        $objCat = new Models_ContentCategory ();
        /*
         * Add conditional language
         */
        $arr['lang_id'] = Nine_Language::getCurrentLangId();
        /*
         * Add sort
         */
        if ($sort == null)
            $sort = array(
                'sorting ASC'
            );
        $data = $objCat->getByColumns($arr, $sort)->toArray();
        $objData = new Models_Content ();
        $data = Nine_Query::formatData($data, $objData);
        return $data;
    }

    // get list content category
    public static function getListProductCategory($arr, $sort = NULL, $count = NULL, $offset = NULL) {
        $objCat = new Models_ProductCategory ();
        /*
         * Add lang search conditional
         */
        $arr['lang_id'] = Nine_Language::getCurrentLangId();
        /*
         * Add sorting
         */
        if ($sort == null) {
            $sort = array(
                'sorting ASC'
            );
        }
        $data = $objCat->getByColumns($arr, $sort, $count, $offset)->toArray();
        $objData = new Models_Product ();
        $data = Nine_Query::formatData($data, $objData);
        return $data;
    }

    //get list product category
    public static function getListEstoreCategory($arr, $sort = null) {
        $objCat = new Models_EstoreCategory ();
        if ($sort == null) {
            $sort = array(
                'sorting ASC'
            );
        }
        $data = $objCat->getByColumns($arr, $sort)->toArray();

        $data = Nine_Query::formatData($data, null);
        return $data;
    }

    // get list users
    public static function getListUsers($arr) {
        $objUser = new Models_User ();
        $data = $objUser->getByColumns($arr, array(
                    'user_id ASC'
                ))->toArray();
        $url = Nine_Route::url();
        foreach ($data as &$value) {
            if(isset($value ['topsuplier']))
                $value ['topsuplier'] = $url ['path'] . 'estore/topsuplier/' . $value ['alias'] . '.html';
            if(isset($value ['image']))
                $value ['image'] = $url ['path'] . "/" . $value ['image'];
            if(isset($value ['logo']))
                $value ['logo'] = $url ['path'] . "/" . $value ['logo'];
            if(isset($value ['icon']))
                $value ['icon'] = $url ['path'] . "/" . $value ['icon'];
        }
        unset($value);
        return $data;
    }

    // get list users
    public static function getListGroupUsers($arr) {
        $objUser = new Models_Group ();
        $data = $objUser->getByColumns($arr, array(
                    'sorting ASC'
                ))->toArray();
        return $data;
    }

    /*
     * Get list content by category alias
     */

    public static function getListContentsByCatAlias($en_alias, $vi_alias, $count = NULL, $offset = NULL) {
        //Get cat
        if (Nine_Language::getCurrentLangCode() == 'vi') {
            $cat = Nine_Query::getListContentCategory(array(
                        'alias' => $vi_alias
            ));
        } else {
            $cat = Nine_Query::getListContentCategory(array(
                        'alias' => $en_alias
            ));
        }
        //Get list contents
        if (count($cat) > 0) {
            $cat = @reset($cat);
            /*
             * Get all content category
             */
            $allData = Nine_Query::getListContentCategory(array(
                        'lang_id' => Nine_Language::getCurrentLangId()
            ));
            /*
             * Get list sub categories
             */
            $listId = Nine_Query::getListContentCategoryId($allData, $cat['content_category_gid']);

            if ($listId != '') {
                $listId = "(" . $listId . "," . $cat['content_category_gid'] . ")";
            } else {
                $listId = "(" + $cat['content_category_gid'] + ")";
            }
            $listId = explode(",", $listId);

            /*
             * get list contents
             */
            return Nine_Query::getListContents(array(
                        'content_category_gid IN(?)' => $listId
                            ), null, $count, $offset);
        } else {
            return array();
        }
    }

    /*
     * Get list products by category alias
     */

    public static function getListProductsByCatAlias($en_alias, $vi_alias, $count = NULL, $offset = NULL) {
        //Get cat
        if (Nine_Language::getCurrentLangCode() == 'vi') {
            $cat = Nine_Query::getListProductCategory(array(
                        'alias' => $vi_alias
            ));
        } else {
            $cat = Nine_Query::getListProductCategory(array(
                        'alias' => $en_alias
            ));
        }
        //Get list contents
        if (count($cat) > 0) {
            $cat = @reset($cat);
            /*
             * Get all content category
             */
            $allData = Nine_Query::getListProductCategory(array(
                        'lang_id' => Nine_Language::getCurrentLangId()
            ));
            /*
             * Get list sub categories
             */
            $listId = Nine_Query::getListProductCategoryId($allData, $cat['product_category_gid']);

            if ($listId != '') {
                $listId = "(" . $listId . ", " . $cat['product_category_gid'] . ")";
            } else {
                $listId = "(" + $cat['product_category_gid'] + ")";
            }
            $listId = explode(',', $listId);
            /*
             * get list contents
             */
            return Nine_Query::getListProducts(array(
                        'product_category_gid IN(?)' => $listId
                            ), null, $count, $offset);
        } else {
            return array();
        }
    }

    /*
     * Get list products by category alias
     */

    public static function getListProductsByCatId($Id, $count = NULL, $offset = NULL) {
        /*
         * Get all content category
         */
        $allData = Nine_Query::getListProductCategory(array(
                    'lang_id' => Nine_Language::getCurrentLangId()
        ));
        /*
         * Get list sub categories
         */
        $listId = Nine_Query::getListProductCategroryId($allData, $Id);

        if ($listId != '') {
            $listId = "(" . $listId . ", " . $Id . ")";
        } else {
            $listId = "(" + $Id + ")";
        }
        $listId = explode(',', $listId);
        
        /*
         * get list contents
         */
        return Nine_Query::getListProducts(array(
                    'product_category_gid IN(?)' => $listId,
                    'check_product' => 2
                        ), null, $count, $offset);
    }

    /*
     * Get list suplier
     */

    public static function getListSuplier($obj) {
        $objTopSuplies = new Models_TopsuplierCategory();
        $listTopSuplier = $objTopSuplies->getByColumns(array())->toArray();
        $url = Nine_Route::url();
        /*
         * Get main image
         */
        foreach ($listTopSuplier as &$value) {
            $tmp = explode('||', $value ['images']);
            $value ['main_image'] = $url ['path'] . $tmp [0];
        }
        unset($value);
        /*
         * Set view
         */
        $obj->listTopSuplier = $listTopSuplier;

        /*
         * return
         */
        return $listTopSuplier;

    }

}
