<?php

/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2011 9fw.org
 * @license    http://license.9fw.org
 * @version    v 1.0 2009-04-15
 * 
 */
require_once 'modules/payment/models/PaymentCategory.php';
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/project/models/Project.php';
require_once 'modules/design/models/Design.php';
require_once 'modules/design/models/DesignRegister.php';
require_once 'modules/contact/models/Contact.php';
require_once 'modules/design/models/Params.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/menu/models/Menu.php';
require_once 'Nine/Registry.php';


class Nine_Read {
    
    /*
     * Get list module data
     */

    public static function GetListData($module, $condition = array(), $sort = array(), $type = 'index', $count = null, $offset = null) {
        try {
            /*
             * Init class
             */
            $objClass = new $module();
            //get list data
            $data = $objClass->getByColumns($condition, $sort, $count, $offset)->toArray();

            //format list data
            $data = Nine_Read::FormatData($data, $module, $type);
            //Return data
            return $data;
        } catch (Exception $exc) {
            echo "Error: " . $exc->getMessage();
            return array();
        }
    }
    /*
     * Get url of current model
     */
    public static function url($module, $type, $alias){
        //Get modules name
        $module = substr($module, 7, strlen($module));
        $module = strtolower($module);
        $index = strpos($module, 'category');
        if($index !== false){
            $module = substr($module, 0, $index);
        }        
        //return
        return Nine_Route::_($module."/index/" . $type, array(
            'alias' => $alias
        ));
    }
    /*
     * Format data
     */

    public static function FormatData($data, $module, $type) {
        try {
            //Get type
            if($type === null){
                $type = 'index';
            }
            // get url
            $url = Nine_Route::url();
            foreach ($data as &$content) {
                //Alias
                if ($content ['alias'] == "") {
                    $content ['alias'] = Nine_Common::convert_vi_to_en($content ['alias']);
                    $content ['alias'] = str_replace(" ", "-", str_replace("&*#39;", "", trim($content ['alias'])));
                }

                //Main image
                if (isset($content ['images'])) {
                    $tmp = explode('||', $content ['images']);
                    $content ['main_image'] = $url ['path'] . $tmp [0];
                    //Create list images
                    $arrImg = explode('||', $content['images']);
                    foreach ($arrImg as &$item) {
                        $item = $url['path'] . $item;
                    }
                    $content['arrImages'] = $arrImg;
                }

                //Files
                if (isset($content ['file'])) {
                    $content['file'] = $url['path'] . $content['file'];
                }

                //Icon
                if (isset($content ['icon'])) {
                    $content ['icon'] = $url ['path'] . $content ['icon'];
                }
                //Set up price
                if (isset($content ['price'])) {
                    $content ['price'] = Nine_Query::makeUpPrice($content ['price']);
                }
                if (isset($content ['price_sale'])) {
                    $content ['price_sale'] = Nine_Query::makeUpPrice($content ['price_sale']);
                }
                //Url
                $content ['url'] = Nine_Read::url($module, $type, $content['alias']);

                //Created date
                if(isset($content ['created_date'])){
                    $content ['created_date'] = date('d-m-Y', $content ['created_date']);
                }

            }
            unset($content);
            return $data;
        } catch (Exception $exc) {
            echo "Error: " . $exc->getMessage();
            //Go to error page   
            return array();
        }
    }

}
