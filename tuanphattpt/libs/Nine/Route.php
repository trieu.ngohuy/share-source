<?php

/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2011 9fw.org
 * @license    http://license.9fw.org
 * @version    v 1.0 2009-04-15
 * 
 */
require_once 'Nine/Registry.php';

class Nine_Route {

    //get url
    public static function url() {
        if (isset($_SERVER['HTTPS'])) {
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        } else {
            $protocol = 'http';
        }
        $current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (strpos($current_url, 'localhost'))
            $arr['path'] = $protocol . "://" . $_SERVER['HTTP_HOST'] . '/tuanphat'. "/";
        else
            $arr['path'] = $protocol . "://" . $_SERVER['HTTP_HOST'] ."/";
        $arr['current_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        return $arr;
    }

    //get sessions
    public static function getSession($name) {
        $defaultSession = new Zend_Session_Namespace('Default');
        $session = $defaultSession->defaultSession;
        if ($session != '' || $session != null) {
            foreach ($session as $key => $value) {
                if ($key == $name)
                    return $value;
            }
        }
        return '';
    }

    //set secssion
    public static function setSession($arr) {
        $defaultSession = new Zend_Session_Namespace('Default');
        $session = $defaultSession->defaultSession;
        foreach ($arr as $key => $value) {
            $session[$key] = $value;
        }
        $defaultSession->defaultSession = $session;
    }

    /**
     * Build friendly link of module
     * 
     * @param string $link The link
     * @return string
     */
    public static function _($link, $params = array()) {
        /**
         * Active friendly URL?
         */
        $fUrlConfig = Nine_Registry::getConfig('seachEngineFriendly');

        if (false == $fUrlConfig['active']) {
            return $link;
        }

        $link = rtrim($link, '/') . '/';
        if (false !== stripos($link, 'http://') || false !== stripos($link, 'https://')) {
            /**
             * Link contain http/https
             */
            return $link;
        } else {
            if ('/' == @$link{0}) {
                /**
                 * Link from root folder
                 * Type: /<aplicationBaseUrl>/<module>/<controller>/<action>/<param1>/<value1>/...
                 */
                return $link;
            } else {
                /**
                 * Quick call link of module
                 * Type: <module>/<controller>/<action>/<param1>/<value1>/...
                 */
                $linkArr = explode('/', trim($link, '/'));
                $moduleName = @$linkArr[0];
                $routePath = "modules/{$moduleName}/Route.php";

                if (null != $moduleName && is_readable($routePath)) {
                    /**
                     * Call rout calculating from moudle
                     */
                    require_once "{$routePath}";
                    $className = 'Route_' . ucfirst($moduleName);
                    $objRoute = new $className;
                    //Nine_Registry::getAppBaseUrl() . $objRoute->build($linkArr, $params) . '.' . $fUrlConfig['suffix'];
                    return Nine_Registry::getAppBaseUrl() . $objRoute->build($linkArr, $params) ;
                }
                return Nine_Registry::getAppBaseUrl() . $link;
            }
        }
    }

}
