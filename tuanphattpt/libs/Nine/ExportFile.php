<?php

/**
 * LICENSE
 * 
 * [license information]
 * 
 * @category   Nine
 * @copyright  Copyright (c) 2011 9fw.org
 * @license    http://license.9fw.org
 * @version    v 1.0 2009-04-15
 * 
 */
class Nine_ExportFile {
    /*
     * Excel version
     */

    static public $ExcelVersion = 'Excel2007';
    /*
     * File extinction
     */
    static public $PdfFileExtinction = '.pdf';
    static public $ExcelFileExtinction = '.xlsx';
    /*
     * Path
     */
    static public $path = '/tuanphat/media/files/';
    /*
     * Template file
     */
    static public $template_1 = 'Export_Template_1';
    static public $template_2 = 'Export_Template_2';

    /*
     * Contructor
     */

    protected function __construct() {
        
    }

    /*
     * Fomart price
     */

    static function ConvertStringToPrice($value) {
        $formatedPrice = "";
        //Convert to string
        $value = (string) $value;
        //Loop each char of value
        $count = 1;
        for ($i = strlen($value) - 1; $i >= 0; $i--) {
            //Add ',' between each 3 number
            if ($count >= 3 && $i > 0 && $count % 3 === 0) {
                $formatedPrice .= $value[$i] . ",";
            } else {
                $formatedPrice .= $value[$i];
            }
            $count++;
        }
        //Rever formated price then return
        $formatedPrice = strrev($formatedPrice);
        return $formatedPrice;
    }

    /*
     * Generate file name
     */

    static function GenerateFileName($prefix = null) {
        //Get config
        $config = Nine_Common::getConfig();
        //Get current date
        $date = new DateTime();
        $date = date_format($date, 'YmdHis');
        //return
        return 'TUANPHAT_' . ($prefix === null ? '' : $prefix) . '_' . $date;
    }

    /*
     * Open excel file
     */

    static function OpenExcelFile($fileName) {
        $excelObj = PHPExcel_IOFactory::createReader(Nine_ExportFile::$ExcelVersion);
        $url = Nine_Route::url();
        $excelObj = $excelObj->load($_SERVER['DOCUMENT_ROOT'] . Nine_ExportFile::$path . $fileName);
        $excelObj->setActiveSheetIndex(0);
        return $excelObj;
    }

    /*
     * Save excel file
     */

    static function SaveExcelFile($excelObj, $fileUrl) {
        $objWriter = \PHPExcel_IOFactory::createWriter($excelObj, Nine_ExportFile::$ExcelVersion);
        $objWriter->save($fileUrl);
    }

    /*
     * Export Excel
     */

    public static function ExportExcelFile($data) {
        try {
            //Import library
            require_once('libs/PHPExcel/Classes/PHPExcel.php');
            require_once('libs/PHPExcel/Classes/PHPExcel/IOFactory.php');

            //Get file name and file url
            $fileName = Nine_ExportFile::GenerateFileName();

            $fileName = $fileName . Nine_ExportFile::$ExcelFileExtinction;

            //File url
            $fileUrl = $_SERVER['DOCUMENT_ROOT'] . Nine_ExportFile::$path . 'export/' . $fileName;

            //Get template name
            $template_file_name = Nine_ExportFile::$template_1;
            if ($data['file_index'] === 2) {
                $template_file_name = Nine_ExportFile::$template_2;
            }
            //Open template file            
            $excelObj = Nine_ExportFile::OpenExcelFile($template_file_name . Nine_ExportFile::$ExcelFileExtinction);

            // Get active sheet obj
            $activeSheet = $excelObj->getActiveSheet();

            //Write content
            if ($data['file_index'] === 1) {
                Nine_ExportFile::WriteExcelTable1Content($activeSheet, $data);
            } else {
                Nine_ExportFile::WriteExcelTable2Content($activeSheet, $data);
            }
            //Save file
            Nine_ExportFile::SaveExcelFile($excelObj, $fileUrl);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /*
     * Write table 1 content
     */

    static function WriteExcelTable1Content($activeSheet, $data) {
        //Get config
        $config = Nine_Common::getConfig();
        //Write header
        $activeSheet
                ->setCellValue('B1', $config['WebsiteName'])
                ->setCellValue('F2', $config['add'])
                ->setCellValue('F3', $config['office'])
                ->setCellValue('F4', $config['hotline'])
                ->setCellValue('F5', $config['Email'])
                ->setCellValue('F6', $config['MST'])
                ->setCellValue('F7', $config['website']);
        //Write content
        //Input 1
        $activeSheet
                ->setCellValue('AE15', $data['input']['val1']['val1'])
                ->setCellValue('AK15', $data['input']['val1']['val2']);
        //Input 2
        $activeSheet
                ->setCellValue('AE17', $data['input']['val2']['val1'])
                ->setCellValue('AK17', $data['input']['val2']['val2']);
        //Result 1
        $activeSheet
                ->setCellValue('AE19', Nine_ExportFile::ConvertStringToPrice($data['result']['val1']['val1']))
                ->setCellValue('AK19', Nine_ExportFile::ConvertStringToPrice($data['result']['val1']['val2']));
        //Result 2
        $activeSheet
                ->setCellValue('AE21', $data['result']['val2']['val1'])
                ->setCellValue('AK21', $data['result']['val2']['val2']);
        //Input 3
        $activeSheet
                ->setCellValue('AE23', $data['input']['val3']['val1'])
                ->setCellValue('AK23', $data['input']['val3']['val2']);
        //Input 4
        $activeSheet
                ->setCellValue('AE24', $data['input']['val4']['val1'])
                ->setCellValue('AK24', $data['input']['val4']['val2']);
        //Input 5
        $activeSheet
                ->setCellValue('AE25', $data['input']['val5']['val1'])
                ->setCellValue('AK25', $data['input']['val5']['val2']);
        //Result 3
        $activeSheet
                ->setCellValue('AE26', $data['result']['val3']['val1'])
                ->setCellValue('AK26', $data['result']['val3']['val2']);
        //Result 4
        $activeSheet
                ->setCellValue('AE27', Nine_ExportFile::ConvertStringToPrice($data['result']['val4']['val1']))
                ->setCellValue('AK27', Nine_ExportFile::ConvertStringToPrice($data['result']['val4']['val2']));
        //Input 6
        $activeSheet
                ->setCellValue('AE28', $data['input']['val6']['val1'])
                ->setCellValue('AK28', $data['input']['val6']['val2']);

        //Result 5
        $activeSheet
                ->setCellValue('AE32', Nine_ExportFile::ConvertStringToPrice($data['result']['val5']['val1']))
                ->setCellValue('AK32', Nine_ExportFile::ConvertStringToPrice($data['result']['val5']['val2']));
        //Input 7
        $activeSheet
                ->setCellValue('AE35', Nine_ExportFile::ConvertStringToPrice($data['input']['val7']['val1']))
                ->setCellValue('AK35', Nine_ExportFile::ConvertStringToPrice($data['input']['val7']['val2']));
        //Result 6
        $activeSheet
                ->setCellValue('AE36', Nine_ExportFile::ConvertStringToPrice($data['result']['val6']['val1']))
                ->setCellValue('AK36', Nine_ExportFile::ConvertStringToPrice($data['result']['val6']['val2']));
        //Result 7
        $activeSheet
                ->setCellValue('AE37', Nine_ExportFile::ConvertStringToPrice($data['result']['val7']['val1']))
                ->setCellValue('AK37', Nine_ExportFile::ConvertStringToPrice($data['result']['val7']['val2']));
        //Result 8
        $activeSheet->setCellValue('AK38', Nine_ExportFile::ConvertStringToPrice($data['result']['val8']['val2']));
        //Result 9
        $activeSheet->setCellValue('AK43', Nine_ExportFile::ConvertStringToPrice($data['result']['val9']['val2']));
        //Result 11
        $activeSheet->setCellValue('AK51', Nine_ExportFile::ConvertStringToPrice($data['result']['val11']['val2']));
        //Result 12
        $activeSheet->setCellValue('AK56', $data['result']['val12']['val2'] . '%');
        //Result 13
        $activeSheet->setCellValue('AK59', $data['result']['val13']['val2']);
        //Result 14
        $activeSheet->setCellValue('AK60', $data['result']['val14']['val2']);
        //Result 15
        $activeSheet->setCellValue('AK61', $data['result']['val15']['val2'] . '%');
    }

    /*
     * Write table 2 content
     */

    static function WriteExcelTable2Content($activeSheet, $data) {
        //Get config
        $config = Nine_Common::getConfig();
        //Write header
        $activeSheet
                ->setCellValue('B1', $config['WebsiteName'])
                ->setCellValue('F2', $config['add'])
                ->setCellValue('F3', $config['office'])
                ->setCellValue('F4', $config['hotline'])
                ->setCellValue('F5', $config['Email'])
                ->setCellValue('F6', $config['MST'])
                ->setCellValue('F7', $config['website']);
        //Write content
        //Input 1
        $activeSheet
                ->setCellValue('AE15', $data['input']['val1']['val1'])
                ->setCellValue('AK15', $data['input']['val1']['val2']);
        //Input 2
        $activeSheet
                ->setCellValue('AE17', $data['input']['val2']['val1'])
                ->setCellValue('AK17', $data['input']['val2']['val2']);
        //Result 1
        $activeSheet
                ->setCellValue('AE19', Nine_ExportFile::ConvertStringToPrice($data['result']['val1']['val1']))
                ->setCellValue('AK19', Nine_ExportFile::ConvertStringToPrice($data['result']['val1']['val2']));
        //Result 2
        $activeSheet
                ->setCellValue('AE21', $data['result']['val2']['val1'])
                ->setCellValue('AK21', $data['result']['val2']['val2']);
        //Input 3
        $activeSheet
                ->setCellValue('AE23', $data['input']['val3']['val1'])
                ->setCellValue('AK23', $data['input']['val3']['val2']);
        //Input 4
        $activeSheet
                ->setCellValue('AE24', $data['input']['val4']['val1'])
                ->setCellValue('AK24', $data['input']['val4']['val2']);
        //Input 5
        $activeSheet
                ->setCellValue('AE25', $data['input']['val5']['val1'])
                ->setCellValue('AK25', $data['input']['val5']['val2']);
        //Result 3
        $activeSheet
                ->setCellValue('AE26', $data['result']['val3']['val1'])
                ->setCellValue('AK26', $data['result']['val3']['val2']);
        //Result 5
        $activeSheet
                ->setCellValue('AE28', Nine_ExportFile::ConvertStringToPrice($data['result']['val5']['val1']))
                ->setCellValue('AK28', Nine_ExportFile::ConvertStringToPrice($data['result']['val5']['val2']));
        //Input 6
        $activeSheet
                ->setCellValue('AE29', Nine_ExportFile::ConvertStringToPrice($data['input']['val6']['val1']))
                ->setCellValue('AK29', Nine_ExportFile::ConvertStringToPrice($data['input']['val6']['val2']));

        //Result 6
        $activeSheet
                ->setCellValue('AE33', Nine_ExportFile::ConvertStringToPrice($data['result']['val6']['val1']))
                ->setCellValue('AK33', Nine_ExportFile::ConvertStringToPrice($data['result']['val6']['val2']));
        //Input 7
        $activeSheet
                ->setCellValue('AE36', Nine_ExportFile::ConvertStringToPrice($data['input']['val7']['val1']))
                ->setCellValue('AK36', Nine_ExportFile::ConvertStringToPrice($data['input']['val7']['val2']));
        //Result 7
        $activeSheet
                ->setCellValue('AE37', Nine_ExportFile::ConvertStringToPrice($data['result']['val7']['val1']))
                ->setCellValue('AK37', Nine_ExportFile::ConvertStringToPrice($data['result']['val7']['val2']));
        //Result 8
        $activeSheet
                ->setCellValue('AE38', Nine_ExportFile::ConvertStringToPrice($data['result']['val8']['val1']))
                ->setCellValue('AK38', Nine_ExportFile::ConvertStringToPrice($data['result']['val8']['val2']));
        //Result 9
        $activeSheet->setCellValue('AK39', Nine_ExportFile::ConvertStringToPrice($data['result']['val9']['val2']));
        //Result 10
        $activeSheet->setCellValue('AK43', Nine_ExportFile::ConvertStringToPrice($data['result']['val10']['val2']));
        //Result 11
        $activeSheet->setCellValue('AK47', Nine_ExportFile::ConvertStringToPrice($data['result']['val11']['val2']));
        //Result 12
        $activeSheet->setCellValue('AK51', Nine_ExportFile::ConvertStringToPrice($data['result']['val12']['val2']));
        //Result 13
        $activeSheet->setCellValue('AK56', $data['result']['val13']['val2'] . '%');
        //Result 14
        $activeSheet->setCellValue('AK59', $data['result']['val14']['val2']);
        //Result 15
        $activeSheet->setCellValue('AK60', $data['result']['val15']['val2']);
        //Result 16
        $activeSheet->setCellValue('AK61', $data['result']['val16']['val2'] . '%');
    }

    /*
     * Open excel file
     */

    static function OpenPdfFile($mpdf, $fileName, $mode, $left, $top, $width) {
        //Load template
        $mpdf->SetImportUse();
        // Add First page
        $mpdf->AddPage($mode);
        //$mpdf->setFooter('{PAGENO}');
        $pagecount = $mpdf->SetSourceFile($_SERVER['DOCUMENT_ROOT'] . Nine_ExportFile::$path . $fileName . Nine_ExportFile::$PdfFileExtinction);
        $tplId = $mpdf->ImportPage($pagecount);
        $mpdf->UseTemplate($tplId, $left, $top, $width);
        return $mpdf;
    }

    /*
     * Export Pdf
     */

    public static function ExportPdfFile($data) {
        try {
            //Import library
            require_once('libs/mpdf/mpdf.php');

            //Get file name and file url
            $fileName = Nine_ExportFile::GenerateFileName();

            $fileName = $fileName . Nine_ExportFile::$PdfFileExtinction;

            //File url
            $fileUrl = $_SERVER['DOCUMENT_ROOT'] . Nine_ExportFile::$path . 'export/' . $fileName;

            //Get template name
            $template_file_name = Nine_ExportFile::$template_1;
            if ($data['file_index'] === 'table_2') {
                $template_file_name = Nine_ExportFile::$template_2;
            }
            //Create object pdf
            $mpdf = new mPDF();
            //echo $url['path'] . $_SERVER['DOCUMENT_ROOT'] . Nine_ExportFile::$path . $template_file_name . Nine_ExportFile::$PdfFileExtinction;
            //Load template
            $mpdf = Nine_ExportFile::OpenPdfFile($mpdf, $template_file_name, 'P', 2.5, 1.5, 207);

            //Get config
            $config = Nine_Common::getConfig();

            //Add css
            $stylesheet = file_get_contents($_SERVER['DOCUMENT_ROOT'] . Nine_ExportFile::$path . 'style.css'); // external css
            $mpdf->WriteHTML($stylesheet, 1);

            //Write content
            if ($data['file_index'] === 'table_1') {
                Nine_ExportFile::WritePdfTable1Content($mpdf, $data, $config);
            } else {
                Nine_ExportFile::WritePdfTable2Content($mpdf, $data, $config);
            }

            //Write config file_url
            $writer = new Zend_Config_Writer_Array();
            $writer->setExclusiveLock(true);
            $config['file_url'] = $fileUrl;
            $config['file_name'] = $fileName;
            $oldSettings = include 'setting.php';
            $config['Logo'] = $oldSettings['Logo'];
            $writer->write('setting.php', new Zend_Config($config));
            $writer->setExclusiveLock(false);

            //Save pdf file
            ob_end_clean();
            $mpdf->Output($fileUrl);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /*
     * Write pdf table 1 content
     */

    static function WritePdfTable1Content($mpdf, $data, $config) {

        //Write header
        $header = '<div id="divHeader">
                    <h3 class="font-bold">' . $config['WebsiteName'] . '</h3>
                    <p >' . $config['Add'] . '</p>
                    <p >' . $config['Office'] . '</p>
                    <p >' . $config['hotline'] . '</p>
                    <p >' . $config['Email'] . '</p>
                    <p >' . $config['MST'] . '</p>
                    <p >' . $config['Website'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($header, 10, 10, 100, 100, 'auto');
//        //Write content
//        //Input 1
        $html = '<div class="div-block">
                    <p >' . $data['input']['val1']['val1'] . '</p><p class="val2">' . $data['input']['val1']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 63, 900, 100, 'auto');
//        //Input 2
        $html = '<div class="div-block">
                    <p >' . $data['input']['val2']['val1'] . '</p><p class="val2">' . $data['input']['val2']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 70, 900, 100, 'auto');
//        //Result 1
        $html = '<div class="div-block">
                    <p >' . Nine_ExportFile::ConvertStringToPrice($data['result']['val1']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val1']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 77, 900, 100, 'auto');
//        //Result 2
        $html = '<div class="div-block">
                    <p >' . $data['result']['val2']['val1'] . '</p><p class="val2">' . $data['result']['val2']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 84, 900, 100, 'auto');
//        //Input 3
        $html = '<div class="div-block">
                    <p >' . Nine_ExportFile::ConvertStringToPrice($data['input']['val3']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['input']['val3']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 89, 900, 100, 'auto');
//        //Input 4
        $html = '<div class="div-block">
                    <p >' . $data['input']['val4']['val1'] . '</p><p class="val2">' . $data['input']['val4']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 92.5, 900, 100, 'auto');
//        //Input 5
        $html = '<div class="div-block">
                    <p >' . $data['input']['val5']['val1'] . '</p><p class="val2">' . $data['input']['val5']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 96, 900, 100, 'auto');
//        //Result 3
        $html = '<div class="div-block">
                    <p >' . $data['result']['val3']['val1'] . '</p><p class="val2">' . $data['result']['val3']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 99, 900, 100, 'auto');
//        //Result 4
        $html = '<div class="div-block">
                    <p >' . Nine_ExportFile::ConvertStringToPrice($data['result']['val4']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val4']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 102, 900, 100, 'auto');
//        //Input 6
        $html = '<div class="div-block">
                    <p >' . Nine_ExportFile::ConvertStringToPrice($data['input']['val6']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['input']['val6']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 106, 900, 100, 'auto');
//
//        //Result 5
        $html = '<div class="div-block">
                    <p >' . Nine_ExportFile::ConvertStringToPrice($data['result']['val5']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val5']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 122, 900, 100, 'auto');
//        //Input 7
        $html = '<div class="div-block">
                    <p >' . Nine_ExportFile::ConvertStringToPrice($data['input']['val7']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['input']['val7']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 129, 900, 100, 'auto');
//        //Result 6
        $html = '<div class="div-block">
                    <p>' . Nine_ExportFile::ConvertStringToPrice($data['result']['val6']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val6']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 132.5, 900, 100, 'auto');
//        //Result 7
        $html = '<div class="div-block">
                    <p >' . Nine_ExportFile::ConvertStringToPrice($data['result']['val7']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val7']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 136, 900, 100, 'auto');
//        //Result 8
        $html = '<div class="div-block">
                    <p >&emsp;</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val8']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 147, 900, 100, 'auto');
//        //Result 9
        $html = '<div class="div-block">
                    <p >&emsp;</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val9']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 162, 900, 100, 'auto');
//        //Result 10
        $html = '<div class="div-block">
                    <p >&emsp;</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val10']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 175, 900, 100, 'auto');
//        //Result 11
        $html = '<div class="div-block">
                    <p >&emsp;</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val11']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 190, 900, 100, 'auto');
//        //Result 12
        $html = '<div class="div-block">
                    <p >&emsp;</p><p class="val2">' . $data['result']['val12']['val2'] . '%' . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 203, 900, 100, 'auto');
//        //Result 13
        $html = '<div class="div-block">
                    <p >&emsp;</p><p class="val2">' . $data['result']['val13']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 210, 900, 100, 'auto');
//        //Result 14
        $html = '<div class="div-block">
                    <p >&emsp;</p><p class="val2">' . $data['result']['val14']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 213, 900, 100, 'auto');
//        //Result 15
       $html = '<div class="div-block">
                    <p >&emsp;</p><p class="val2">' . $data['result']['val15']['val2'] . '%' . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 217, 900, 100, 'auto');
    }

    /*
     * Write pdf table 2 content
     */

    static function WritePdfTable2Content($mpdf, $data, $config) {

        //Write header
        $header = '<div id="divHeader">
                    <h3 class="font-bold">' . $config['WebsiteName'] . '</h3>
                    <p >' . $config['Add'] . '</p>
                    <p >' . $config['Office'] . '</p>
                    <p >' . $config['hotline'] . '</p>
                    <p >' . $config['Email'] . '</p>
                    <p >' . $config['MST'] . '</p>
                    <p >' . $config['Website'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($header, 10, 10, 100, 100, 'auto');
//        //Write content
//        //Input 1
        $html = '<div class="div-block div-block2">
                    <p >' . $data['input']['val1']['val1'] . '</p><p class="val2">' . $data['input']['val1']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 62, 900, 100, 'auto');
//        //Input 2
        $html = '<div class="div-block div-block2">
                    <p >' . $data['input']['val2']['val1'] . '</p><p class="val2">' . $data['input']['val2']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 69, 900, 100, 'auto');
//        //Result 1
        $html = '<div class="div-block div-block2">
                    <p >' . Nine_ExportFile::ConvertStringToPrice($data['result']['val1']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val1']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 76, 900, 100, 'auto');
//        //Result 2
        $html = '<div class="div-block div-block2">
                    <p >' . $data['result']['val2']['val1'] . '</p><p class="val2">' . $data['result']['val2']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 83, 900, 100, 'auto');
//        //Input 3
        $html = '<div class="div-block div-block2">
                    <p >' . $data['input']['val3']['val1'] . '</p><p class="val2">' . $data['input']['val3']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 88, 900, 100, 'auto');
//        //Input 4
        $html = '<div class="div-block div-block2">
                    <p >' . $data['input']['val4']['val1'] . '</p><p class="val2">' . $data['input']['val4']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 91.5, 900, 100, 'auto');
//        //Input 5
        $html = '<div class="div-block div-block2">
                    <p >' . $data['input']['val5']['val1'] . '</p><p class="val2">' . $data['input']['val5']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 95, 900, 100, 'auto');
//        //Result 3
        $html = '<div class="div-block div-block2">
                    <p >' . $data['result']['val3']['val1'] . '</p><p class="val2">' . $data['result']['val3']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 98, 900, 100, 'auto');
//        //Result 5
        $html = '<div class="div-block div-block2">
                    <p >' . Nine_ExportFile::ConvertStringToPrice($data['result']['val5']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val5']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 104.5, 900, 100, 'auto');
//        //Input 6
        $html = '<div class="div-block div-block2">
                    <p >' . Nine_ExportFile::ConvertStringToPrice($data['input']['val6']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['input']['val6']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 108, 900, 100, 'auto');
//
//        //Result 6
        $html = '<div class="div-block div-block2">
                    <p >' . Nine_ExportFile::ConvertStringToPrice($data['result']['val6']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val6']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 125, 900, 100, 'auto');
//        //Input 7
        $html = '<div class="div-block div-block2">
                    <p >' . Nine_ExportFile::ConvertStringToPrice($data['input']['val7']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['input']['val7']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 131, 900, 100, 'auto');
//        //Result 7
        $html = '<div class="div-block div-block2">
                    <p>' . Nine_ExportFile::ConvertStringToPrice($data['result']['val7']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val7']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 134.5, 900, 100, 'auto');
//        //Result 8
        $html = '<div class="div-block div-block2">
                    <p >' . Nine_ExportFile::ConvertStringToPrice($data['result']['val8']['val1']) . '</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val8']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 138, 900, 100, 'auto');
//        //Result 9
        $html = '<div class="div-block div-block2">
                    <p >&emsp;</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val9']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 146, 900, 100, 'auto');
//        //Result 10
        $html = '<div class="div-block div-block2">
                    <p >&emsp;</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val10']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 160, 900, 100, 'auto');
//        //Result 11
        $html = '<div class="div-block div-block2">
                    <p >&emsp;</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val11']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 173, 900, 100, 'auto');
//        //Result 12
        $html = '<div class="div-block div-block2">
                    <p >&emsp;</p><p class="val2">' . Nine_ExportFile::ConvertStringToPrice($data['result']['val12']['val2']) . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 188, 900, 100, 'auto');
//        //Result 13
        $html = '<div class="div-block div-block2">
                    <p >&emsp;</p><p class="val2">' . $data['result']['val13']['val2'] . '%' . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 201, 900, 100, 'auto');
//        //Result 14
        $html = '<div class="div-block div-block2">
                    <p >&emsp;</p><p class="val2">' . $data['result']['val14']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 207, 900, 100, 'auto');
//        //Result 15
        $html = '<div class="div-block div-block2">
                    <p >&emsp;</p><p class="val2">' . $data['result']['val15']['val2'] . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 210, 900, 100, 'auto');
//        //Result 16
       $html = '<div class="div-block div-block2">
                    <p >&emsp;</p><p class="val2">' . $data['result']['val16']['val2'] . '%' . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($html, 20, 214, 900, 100, 'auto');
    }
}
