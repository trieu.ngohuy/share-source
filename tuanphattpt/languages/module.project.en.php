<?php
return array (
  'Dự án gần đây' => 'Recent Projects',
  'First' => 'First',
  'Previous' => 'Previous',
  'Next' => 'Next',
  'Last' => 'Last',
  'Dự án' => 'Projects',
  'Trang' => 'Page',
  'Xem mô tả chi tiết' => 'See detailed description',
  'Liên hệ với TUẤN PHÁT' => 'Contact',
  'Mô tả chi tiết' => 'Detailed description',
  'Sản phẩm sử dụng (Click để xem thông số kỹ thuật)' => 'Products used (Click to see specifications)',
  'CHI TIẾT' => 'DETAIL',
);
