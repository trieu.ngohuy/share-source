<?php

return array(
    'Đọc tiếp' => 'Read more',
    'Chuyên mục' => 'Category',
    'First' => 'First',
    'Previous' => 'Previous',
    'Next' => 'Next',
    'Last' => 'Last',
);
