<?php
return array (
  'Sản phẩm' => 'Products',
  'Hiển thị' => 'Display',
  'trong' => 'in',
  'kết quả' => 'results',
  'Thứ tự theo sản phẩm mới' => 'Order by new product',
  'Thứ tự theo mức độ phổ biến' => 'Order by popularity',
  'Thứ tự theo giá: thấp đến cao' => 'Order by price: low to high',
  'Thứ tự theo giá: cao xuống thấp' => 'Order by price: high to low',
  'Mã SP' => 'Code',
  'Liên hệ' => 'Call',
  'Đọc tiếp' => 'Continue reading',
  'Khách hàng của TUẤN PHÁT' => 'Customers of TUAN PHAT',
  'Danh mục' => 'Category',
  'Mã' => 'Code',
  'Xem thông số' => 'See specifications',
  'Tư vấn &amp; đặt mua' => 'Call and ordered',
  'Hỗ trợ trực tuyến' => 'Online support',
  'Live Chat' => 'Live Chat',
  'Thông số' => 'Parameter',
  'Lắp đặt' => 'Installation',
  'Bảo hành' => 'Guarantee',
  'Tài liệu' => 'Document',
  'Sản phẩm cùng loại' => 'Products of the same type',
  'Xem thông số' => 'See specifications',
  'Mô Tả' => 'Description',
  'Chọn đèn' => 'Select the lamp',
  'Cấu tạo' => 'Structure',
  'Dự án' => 'Projects',
);
