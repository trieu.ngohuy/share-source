﻿<?php

$arr = array(
    'database' => array(
        'adapter' => 'Pdo_Mysql',
        'params' => array(
            'host' => 'localhost',
            'username' => 'root',
            'password' => '',
            'dbname' => 'admin_tuanphat',
            'prefix' => '9_',
            'profiler' => false //if false, system is quick
        )
    ),
    'viewConfig' => array(
        'compile_check' => true,
        'debugging' => false,
        'template_dir' => 'modules/',
        'compile_dir' => 'tmp/compile/',
        'cache_dir' => 'tmp/cache/',
        'plugins_dir' => 'libs/Smarty/plugins/',
        'cache_modified_check' => true,
        'caching' => false,
        'cache_lifetime' => 50//120					
    ),
    'layoutConfig' => array(
    ),
    'adminMail' => array(
        'auth' => 'login',
        'username' => '',
        'password' => '',
        'ssl' => 'ssl',
        'port' => 465,
        'mailServer' => 'smtp.gmail.com'
    ),
    'fromMail' => 'info@bsvina.com',
    'requiredModule' => array(
        'default' => 'default',
        'user' => 'user',
        'access' => 'access',
        'error' => 'error',
        'design' => 'design',
        'content' => 'content',
        'feedback' => 'feedback',
        'templates' => 'templates',
        'list' => 'list',
        'product' => 'product',
        'project' => 'project',
        'hs' => 'hs',
        'properties' => 'properties',
        'country' => 'country',
        'payment' => 'payment',
        'package' => 'package',
        'banner' => 'banner',
        'message' => 'message',
        'contact' => 'contact',
        'buying' => 'buying',
        'website' => 'website',
        'topsuplier' => 'topsuplier',
        'cart' => 'cart',
        'provinces' => 'provinces',
        'newlater' => 'newlater',
        'menu' => 'menu',
    ),
    'seachEngineFriendly' => array(
        'active' => true,
        'rewrite' => true,
        'suffix' => 'html'
    ),
    'log' => array(
        'active' => false
    ),
    'defaultLangCode' => 'vi',
    'defaultApp' => 'front',
    'layoutCollection' => 'default',
    'defaultLayout' => 'front',
    'defaultModule' => 'default',
    'defaultController' => 'index',
    'defaultAction' => 'index',
    'currentMode' => Nine_Constant::DEVELOP_MODE,
    'usingMultiAuth' => true,
    'forwardToDefaultAppWhenNotFoundAppName' => true, //or NULL when change to NOT FOUND page.
    //If true, default application name can be missed
    'defaultNumberRowPerPage' => 12,
    'usingOneLanguage' => true,
    'dateFormat' => 'd/m/Y',
    'datepickerFormat' => array(
        'js' => 'dd/mm/yy', #Datepicker format
        'php' => 'd/m/Y', #Used for calculation to Unix time
        'display' => 'dd/mm/yyyy' #Display for user
    ),
    'useAdminFullControlSystem' => true,
    'forgotPasswordExpiredTime' => 86400,
    'defaultThumbnailImageSize' => '100x100',
    'websiteName' => 'Tuấn Phát',
    'liveSite' => '',
    'metaKeywords' => 'tpp',
    'metaDescription' => 'tpp'
);

return $arr;
