<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use DateTime;

require_once(ROOT . DS . 'vendor' . DS . 'mpdf' . DS . 'mpdf.php');
require_once(ROOT . DS . 'vendor' . DS . 'PHPExcel' . DS . 'Classes' . DS . 'PHPExcel.php');
require_once(ROOT . DS . 'vendor' . DS . 'PHPExcel' . DS . 'Classes' . DS . 'PHPExcel' . DS . 'IOFactory.php');

/**
 * Description of PdfController
 *
 * @author h_trieu
 */
class PdfController extends AppController
{
    /*
     * Excel version
     */

    public $ExcelVersion = 'Excel2007';
    /*
     * Excel extinction
     */

    public $PdfFileExtinction = '.pdf';
    public $ExcelFileExtinction = '.xlsx';
    /*
     * Path
     */
    public $path = ROOT . DS . 'Files' . DS;
    /*
     * pdf folder
     */
    public $roomFolder = "Room_Bill";
    public $reportFolder = "Report";
    /*
     * Prefix
     */
    public $prefixRoomBill = 'HoaDonPhong';
    public $prefixReport = 'BaoCao_';
    /*
     * File template
     */
    public $templateRoomCheckOut = 'Room_Bill_Template';
    public $templateReport = 'Report_Template';

    public function initialize()
    {
        parent::initialize();
        // Set the layout
        $this->viewBuilder()->setLayout('empty');
        //Load models
        $this->loadModel('BookingRoom');
        $this->loadModel('Rooms');
        $this->loadModel('Services');
    }

    /*
     * Open excel file
     */

    function OpenTemplateFile($mpdf, $fileName, $mode, $left, $top, $width)
    {
        //Load template
        $mpdf->SetImportUse();
        // Add First page
        $mpdf->AddPage($mode);
        $mpdf->setFooter('{PAGENO}');
        $pagecount = $mpdf->SetSourceFile($this->path . DS . $fileName . $this->PdfFileExtinction);
        $tplId = $mpdf->ImportPage($pagecount);
        $mpdf->UseTemplate($tplId, $left, $top, $width);
        return $mpdf;
    }

    /*
         * Open excel file
         */

    function OpenExcelFile($fileName)
    {
        $excelObj = \PHPExcel_IOFactory::createReader($this->ExcelVersion);
        $excelObj = $excelObj->load($this->path . $fileName);
        $excelObj->setActiveSheetIndex(0);
        return $excelObj;
    }

    /*
     * Generate file name
     */

    function GenerateFileName($mode, $data)
    {
        //Get current date
        $date = new DateTime();
        $date = date_format($date, 'YmdHis');
        $tmp = '';
        //Establish middle name string
        if ($mode === $this->prefixRoomBill) {
            //Get room object
            $room = $this->Rooms->find('all', array('conditions' => array('RoRoomId' => $data['BrRoomId'])));
            $room = $room->toArray();
            $tmp = $this->vn_to_en($data['BrCustomerName']) . '_' . $room[0]['RoAlias'];
        } else {
            $tmp = $data['fromDate'] . '_' . $data['toDate'];
        }
        //Return
        //return $mode . '_' . $tmp . '_' . $date . $this->PdfFileExtinction;
        return $mode . '_' . $tmp . '_' . $date;
    }

    /*
     * Export room bill
     */

    public function ExportRoomBill()
    {

        try {
            //Get post data
            $data = $this->request->getData();

            //Get booking info
            $bookingObj = $this->BookingRoom->find('all', array('conditions' => array(
                'BrBookingId' => $data['id']
            )));
            $bookingObj = $bookingObj->toArray();

            //Create object pdf
            $mpdf = new \mPDF();

            //Load template
            $mpdf = $this->OpenTemplateFile($mpdf, $this->templateRoomCheckOut, 'P', -5, 0, 220);

            //Write bill data
            $this->RoomBillHtml($mpdf, $bookingObj[0]);

            //Set output pdf file
            ob_end_clean();

            $fileName = $this->GenerateFileName($this->prefixRoomBill, $bookingObj[0]) . $this->PdfFileExtinction;
            $fileUrl = $this->path . DS . $this->roomFolder . DS . $fileName;
            $mpdf->Output($fileUrl);

            //Disable render layout
            $this->layout = false;
            $this->autoRender = false;
            $this->response->type('json');
            $json = json_encode(array(
                'success' => true,
                'file_name' => $fileName
            ));
            //Return json
            $this->response->body($json);
            return $this->response;
            //$mpdf->Output();
        } catch (Exception $exc) {
            $this->ExceptionHandle();
        }
    }

    /*
     * Establish room bill html
     */

    function RoomBillHtml($mpdf, $data)
    {

        //Add css
        $stylesheet = file_get_contents($this->path . 'report.css'); // external css
        $mpdf->WriteHTML($stylesheet, 1);

        //Logo
        $logo = $this->GetConfigData('logo') === '' || $this->GetConfigData('logo') === null ? 'no_image.png' : $this->GetConfigData('logo');
        $logo = $this->upload_path . DS . 'images' . DS . $logo;
        $mpdf->Image($logo, 25, 8, 50, 30, 'jpg', '', true, false);
        //Hotel name
        $name = '<h2 style="font-family: arial; font-weight: 100;">' . $this->GetConfigData('hotel-name') . '</h2>';
        $mpdf->WriteFixedPosHTML($name, 84, 8, 100, 100, 'auto');
        //Hotel infomation
        $header = '<div style="font-family: arial; font-size: 14px; ">
                    <p style="line-height: 25px">' . $this->GetConfigData('address') . '</p>
                    <p style="line-height: 25px">' . $this->GetConfigData('phone') . '</p>
                    <p>' . $this->GetConfigData('email') . '</p>
                    </div>';
        $mpdf->WriteFixedPosHTML($header, 110, 15, 100, 100, 'auto');
        //Current date
        $date = new DateTime();
        $date = date_format($date, $this->dateFromat);
        $mpdf->WriteFixedPosHTML($date, 57, 50.5, 100, 100, 'auto');
        //Booking id
        $mpdf->WriteFixedPosHTML($data['BrBookingId'] . ' ', 175, 50.5, 100, 100, 'auto');
        //Room info
        $roomData = $this->Rooms->find('all', array(
            'order' => array('RoCreatedDate ASC'),
            'condition' => array('RoRoomId' => $data['BrRoomId'])
        ));
        $roomData = $roomData->toArray();
        $roomData = $roomData[0];
        $customerEmail = $data['BrCustomerEmail'] === '' || $data['BrCustomerEmail'] === null ? '.' : $data['BrCustomerEmail'];
        $customerPhone = $data['BrCustomerPhone'] === '' || $data['BrCustomerPhone'] === null ? '.' : $data['BrCustomerPhone'];
        $roomInfo = '<div style="font-family: arial; font-size: 14px; ">
                    <p style="line-height: 25px">' . $data['BrCustomerName'] . '</p>
                    <p style="line-height: 25px">' . $customerEmail . '</p>
                    <p style="line-height: 25px">' . $customerPhone . '</p>
                    <p >' . $roomData['RoName'] . '</p>
                    </div>';
        $mpdf->WriteFixedPosHTML($roomInfo, 50, 54, 100, 100, 'auto');
        $roomInfo = '<div style="font-family: arial; font-size: 14px; ">
                    <p style="line-height: 25px">' . $data['BrFromDate'] . '</p>
                    <p style="line-height: 25px">' . $data['BrToDate'] . '</p>
                    <p style="line-height: 25px">' . $this->CountDaysBetweenTwoDates($data['BrFromDate'], $data['BrToDate']) . '</p>
                    <p >' . $this->loginInfo['UsFullName'] . '</p>
                    </div>';
        $mpdf->WriteFixedPosHTML($roomInfo, 150, 54, 100, 100, 'auto');

        //Calculating total room fee
        $totalRoomFee = $this->CalculatingRoomFee($data);

        //Write total money block
        $html = '<div id="divTotalMoney">
                    <div class="wrap">
                        <p class="content">Tiền phòng: </p>
                        <p class="fee">' . $totalRoomFee['RoomFee'] . '</p>
                    </div>';
        if ($totalRoomFee['TransferMessage'] !== '') {
            $html .= '<div class="wrap sub-message-wrap">
                                    <p class="content sub-message">' . $totalRoomFee['TransferMessage'] . '</p>
                                </div>';
            $html .= '<div class="wrap">
                                    <p class="content">Tiền chuyển phòng: </p>
                                    <p class="fee">' . $totalRoomFee['TransferRoomFee'] . '</p>
                                </div>';
        }
        $checkDisplay = substr($totalRoomFee['ServicesFee'], 0, strlen($totalRoomFee['ServicesFee']) - 4);
        if ((int)$checkDisplay !== 0) {
            $html .= '<div class="wrap">
                                    <p class="content">Tiền dịch vụ: </p>
                                    <p class="fee">' . $totalRoomFee['ServicesFee'] . '</p>
                                </div>';
        }
        if ($totalRoomFee['ServicesMessage'] !== '') {
            $html .= '<div class="wrap sub-message-wrap">
                                    <p class="content sub-message">' . $totalRoomFee['ServicesMessage'] . '</p>
                                </div>';
        }
        $checkDisplay = substr($totalRoomFee['DepositFee'], 0, strlen($totalRoomFee['DepositFee']) - 4);
        if ((int)$checkDisplay !== 0) {
            $html .= '<div class="wrap">
                                    <p class="content">Tiền đặt cọc: </p>
                                    <p class="fee">' . $totalRoomFee['DepositFee'] . '</p>
                                </div>';
        }
        $checkDisplay = substr($totalRoomFee['ModeFee'], 0, strlen($totalRoomFee['ModeFee']) - 4);
        if ((int)$checkDisplay !== 0) {
            $html .= '<div class="wrap">
                                            <p class="content">Tiền nhận phòng sớm / trả phòng trễ: </p>
                                            <p class="fee">' . $totalRoomFee['ModeFee'] . '</p>
                                    </div>';
            $html .= '<div class="wrap sub-message-wrap">
                                        <p class="content sub-message">' . $totalRoomFee['ModeMessage'] . '</p>
                                    </div>';
        }
        $checkDisplay = substr($totalRoomFee['ExtrasAldultFee'], 0, strlen($totalRoomFee['ExtrasAldultFee']) - 4);
        if ((int)$checkDisplay !== 0) {
            $html .= '<div class="wrap">
                                        <p class="content">Tiền thêm người: </p>
                                        <p class="fee">' . $totalRoomFee['ExtrasAldultFee'] . '</p>
                                    </div>';
            $html .= '<div class="wrap sub-message-wrap">
                                    <p class="content sub-message">' . $totalRoomFee['ExtrasAldultMessage'] . '</p>
                                </div>';
        }
        if ($totalRoomFee['VATMessage'] !== '') {
            $html .= '<div class="wrap total-money-wrap">
                                        <p class="content vat-wrap"> ' . $totalRoomFee['VATMessage'] . '</p>
                                        <h2 class="fee font-bold">Tổng tiền = ' . $totalRoomFee['TotalFee'] . '</h2>
                                    </div>';
        } else {
            $html .= '<div class="wrap total-money-wrap">
                                        <h2 class="fee font-bold" style="width: 100%">Tổng tiền = ' . $totalRoomFee['TotalFee'] . '</h2>
                                    </div>';
        }


        $html .= '</div>';

        //Write signature
        $html .= '<div class="wrap" id="divSignature">
                    <p class="content"><b>Thu Ngân</b></p>
                    <p class="fee"><b>Khách hàng</b></p>
                </div>';
        $mpdf->WriteHTML($html, 2);
    }

    /*
     * Calculating room fee
     */

    function CalculatingRoomFee($bookingObj)
    {
        $feeObj = array();
        $totalFee = 0;
        $isTransferRoom = $bookingObj['BrTransferRoomId'] !== '' && $bookingObj['BrTransferRoomId'] !== null ? true : false;

        //Get room data
        $roomObj = $this->GetSingleRow('Rooms', array('RoRoomId' => $bookingObj['BrRoomId']), array('RoCreatedDate ASC'));
        $transferRoomObj = $this->GetSingleRow('Rooms', array('RoRoomId' => $bookingObj['BrTransferRoomId']), array('RoCreatedDate ASC'));

        //Room fee
        if ($isTransferRoom) {
            $nightCount = $this->CountDaysBetweenTwoDates($bookingObj['BrTransferDate'], $bookingObj['BrToDate']);
            //Add transfer message
            $feeObj['TransferMessage'] = '- Chuyển sang từ phòng ' . $transferRoomObj['RoName'] . ' từ ngày ' . $bookingObj['BrTransferDate'] . ' .';
        } else {
            $nightCount = $this->CountDaysBetweenTwoDates($bookingObj['BrFromDate'], $bookingObj['BrToDate']);
            $feeObj['TransferMessage'] = '';
        }
        $roomFee = $nightCount * $roomObj['RoPrice'];

        //Add to return object
        $feeObj['RoomFee'] = $this->ConvertStringToPrice($roomFee) . ' ' . $this->currency;
        //Update total fee
        $totalFee += $roomFee;

        //Transfer room fee
        $transferRoomFee = 0;
        if ($isTransferRoom) {
            $nightCount = $this->CountDaysBetweenTwoDates($bookingObj['BrFromDate'], $bookingObj['BrTransferDate']);
            $transferRoomFee = $nightCount * $transferRoomObj['RoPrice'];
        }

        //Add to return object
        $feeObj['TransferRoomFee'] = $this->ConvertStringToPrice($transferRoomFee) . ' ' . $this->currency;
        //Update total fee
        $totalFee += $transferRoomFee;

        //Services fee
        $servicesFee = 0;
        $servicesMessage = '';
        if ($bookingObj['BrServices'] !== '' && $bookingObj['BrServices'] !== null) {
            //Convert services to arr
            $arrServices = explode('||', $bookingObj['BrServices']);
            //Loop
            for ($i = 0; $i < count($arrServices); $i++) {
                //Convert string to arr
                $arrTmp = explode(':', $arrServices[$i]);
                //Get services info
                $servicesObj = $this->GetSingleRow('Services', array('SrServiceId' => (int)$arrTmp[0]), array('SrCreatedDate ASC'));
                //Update total services fee
                $servicesFee += (int)$arrTmp[1] * $servicesObj['SrPrice'];
                //Update message
                $servicesMessage .= '- ' . $servicesObj['SrName'] . ': ' . $this->ConvertStringToPrice($servicesObj['SrPrice']) . ' ' . $this->currency . ', <br>';
            }
        }

        //Add to return object
        $feeObj['ServicesFee'] = $this->ConvertStringToPrice($servicesFee) . ' ' . $this->currency;
        $feeObj['ServicesMessage'] = substr($servicesMessage, 0, strlen($servicesMessage) - 6);
        //Update total fee
        $totalFee += $servicesFee;

        //Mode fee
        $mode = $bookingObj['BrMode'];
        $modeFee = 0;
        $modeMessage = "";
        $arrMode = explode('||', $mode);
        for ($i = 0; $i < count($arrMode); $i++) {
            if ($arrMode[$i] === '0') {
                //Reset sub title
                $modeMessage = '- Không có, <br>';
            } else if ($arrMode[$i] === '2') {
                $modeFee += $roomObj['RoPrice'] / 2;
                //Add to sub title
                $modeMessage = $modeMessage . '- Trả phòng muộn từ 12h10 - 16h: ' . $this->ConvertStringToPrice($roomObj['RoPrice'] / 2) . $this->currency . ', <br>';
            } else if ($arrMode[$i] === '3') {
                $modeFee += $roomObj['RoPrice'];
                //Add to sub title
                $modeMessage = $modeMessage . '- Trả phòng muộn sau 16h: ' . $this->ConvertStringToPrice($roomObj['RoPrice']) . $this->currency . ', <br>';
            } else {
                $tmpGetRoomEarly = explode(':', $arrMode[$i]);
                //Get early hour price
                $tmpSearch = $this->FindHourPrice($tmpGetRoomEarly[1]);
                $modeFee += $tmpSearch;
                //Add to sub title
                $modeMessage = $modeMessage . '- Nhân phòng sớm ' . $tmpGetRoomEarly[1] . 'h: ' . $this->ConvertStringToPrice($tmpSearch) . $this->currency . ', <br>';
            }
        }

        //Add to return object
        $feeObj['ModeFee'] = $this->ConvertStringToPrice($modeFee) . ' ' . $this->currency;
        $feeObj['ModeMessage'] = substr($modeMessage, 0, strlen($modeMessage) - 6);
        //Update total fee
        $totalFee += $modeFee;

        //Deposit
        $feeObj['DepositFee'] = $this->ConvertStringToPrice($bookingObj['BrDeposit']) . ' ' . $this->currency;
        $totalFee -= $bookingObj['BrDeposit'];

        //Extras people
        $aldultPrice = $this->GetConfigData('aldult');
        $aldultFee = 0;
        if ($bookingObj['BrAldult'] > $roomObj['RoMaxAldult']) {
            $extrasAldult = $bookingObj['BrAldult'] - $roomObj['RoMaxAldult'];
            $aldultFee = $extrasAldult * (int)$aldultPrice;

            $feeObj['ExtrasAldultMessage'] = '- Người lớn: +' . $extrasAldult . ' người';
        } else {
            $feeObj['ExtrasAldultMessage'] = '- Không thêm người';
        }

        //Add to return object
        $feeObj['ExtrasAldultFee'] = $this->ConvertStringToPrice($aldultFee) . ' ' . $this->currency;
        //Update total fee
        $totalFee += $aldultFee;

        //Vat
        $vat = (int)$this->GetConfigData('vat');
        $vatFee = 0;
        if ($bookingObj['BrIsVAT'] === 1) {
            $vatFee = ($totalFee * $vat) / 100;
            $feeObj['VATMessage'] = 'VAT(' . $vat . '): ' . $this->ConvertStringToPrice($vatFee) . ' ' . $this->currency;
        } else {
            $feeObj['VATMessage'] = '';
        }
        //Update total fee
        $totalFee += $vatFee;
        $feeObj['TotalFee'] = $this->ConvertStringToPrice($totalFee) . ' ' . $this->currency;

        return $feeObj;
    }

    /*
     * Find hour price
     */

    function FindHourPrice($hour)
    {
        //get data
        $roomEarlyData = $this->GetConfigData('get-room-early');
        //Convert to array
        $roomEarlyData = explode('||', $roomEarlyData);
        //Loop
        for ($i = 0; $i < count($roomEarlyData); $i++) {
            $tmp = explode(':', $roomEarlyData[$i]);
            if ($hour === $tmp[0]) {
                return (int)$tmp[1];
            }
        }
        return 0;
    }

    /*
     * View pdf file
     */

    public function ViewPdf($fileName)
    {
        if (strpos($fileName, $this->prefixRoomBill) !== false) {
            $this->response->file($this->path . $this->roomFolder . DS . $fileName);
        } else {
            $this->response->file($this->path . $this->reportFolder . DS . $fileName);
        }

        $this->response->header('Content-Disposition', 'inline');
        return $this->response;
    }

    /*
         * Save excel file
         */

    function SaveExcelFile($excelObj, $fileUrl)
    {
        $objWriter = \PHPExcel_IOFactory::createWriter($excelObj, $this->ExcelVersion);
        $objWriter->save($fileUrl);
    }

    /*
     * Export excel
     */
    public function ExportExcel($fromDate, $toDate, $fileName)
    {
        //Begin rows index
        $templateBeginRowIndex = 11;
        $templateRowsCount = 9;
        $beginRowIndex = 24;
        $maxColumn = 41;
        $templateSumBeginRowIndex = 21;
        $templateSumRowsCount = 2;
        $templateAllRowsCount = 13;

        //Get export data
        $arrBooking = $this->GetAll('BookingRoom', array(), array('BrCreatedDate ASC'));
        $arrRoom = $this->GetAll('Rooms', array(), array('RoCreatedDate ASC'));

        //Get input data
        $data = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
        );

        //Get file name
        //$fileName = $this->GetConfigData('export-file-name');
        $fileUrl = $this->path . DS . $this->reportFolder . DS . $fileName;

        //Sort data
        $arrData = $this->SortBooking($arrBooking, $data['fromDate'], $data['toDate']);
        
        //Open template file
        $excelObj = $this->OpenExcelFile('Report_Template_1' . $this->ExcelFileExtinction);

        // Set active sheet
        $excelObj->setActiveSheetIndex(0);

        // Get active sheet obj
        $activeSheet = $excelObj->getActiveSheet();
        
        //Header
        $activeSheet
            ->setCellValue('I1', $this->GetConfigData('hotel-name'))
            ->setCellValue('L2', $this->GetConfigData('address'))
            ->setCellValue('L3', $this->GetConfigData('phone'))
            ->setCellValue('L4', $this->GetConfigData('email'));

        //Export date
        $date = new DateTime();
        $date = date_format($date, $this->dateFromat);
        $activeSheet
            ->setCellValue('G6', $data['fromDate'] . ' - ' . $data['toDate'])
            ->setCellValue('G7', $date)
            ->setCellValue('AO7', $this->loginInfo['UsFullName']);
        
        //Booking data
        $this->WriteBookingData($activeSheet, $templateBeginRowIndex, $templateRowsCount, $templateSumBeginRowIndex, $templateSumRowsCount, $maxColumn, $beginRowIndex, $arrData, $arrRoom);
         
        //Remove template rows
        for($i = 1 ; $i < $templateAllRowsCount ; $i++){
            $activeSheet->removeRow($templateBeginRowIndex, 1);
        }

        $activeSheet->getRowDimension($templateBeginRowIndex)->setVisible(false);

        //$activeSheet
        //    ->setCellValue('A1', 'EDITED Last Name')
        //    ->setCellValue('B1', 'EDITED First Name')
        //    ->setCellValue('C1', 'EDITED Age')
        //    ->setCellValue('D1', 'EDITED Sex')
        //    ->setCellValue('E1', 'EDITED Location');

        //Copy rows and format
        //$this->CopyRows($activeSheet, 11, 12, 1, 40);

        //Set rows height
        //$activeSheet->getRowDimension('11')->setRowHeight(300);

        //Set column height
        //$excelObj->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

        //Delete template rows
        //$activeSheet->removeRow(11, 1);

        //Set style
        //$styleArray = array(
        //    'font'  => array(
        //        'bold'  => true,
        //        'color' => array('rgb' => 'FF0000'),
        //        'size'  => 15,
        //        'name'  => 'Verdana'
        //    ));

        //$activeSheet->getCell('A1')->setValue('Some text');
        //$activeSheet->getStyle('A1')->applyFromArray($styleArray);
         
        //Save file
        $this->SaveExcelFile($excelObj, $fileUrl);
         
    }

    //Write report data table
    function WriteBookingData($activeSheet, $templateBeginRowIndex, $templateRowsCount, $templateSumBeginRowIndex, $templateSumRowsCount, $maxColumn, $beginRowIndex, $arrBooking, $arrRoom)
    {
        $totalMoney = 0;
        $row = $beginRowIndex;
        $listStyleCharacter = '• ';
        
        for ($i = 0; $i < count($arrBooking); $i++) {
            
            //Current booking row
            $tmpRow = $row;

            //Get current booking data
            $tmp = $arrBooking[$i];

            //Get room data
            $roomObj = $this->FindInArray($arrRoom, 'RoRoomId', $tmp['BrRoomId']);
            
            //Calculating total money
            $totalRoomFee = $this->CalculatingRoomFee($tmp);
            

            //Copy new rows
            $this->CopyRows($activeSheet, $templateBeginRowIndex, $row, $templateRowsCount, $maxColumn);
            
            //Write transfer room Fee
            $checkDisplay = substr($totalRoomFee['TransferRoomFee'], 0, strlen($totalRoomFee['TransferRoomFee']) - 4);
            if ((int)$checkDisplay !== 0) {
                $activeSheet->setCellValue('AD' . $row, $listStyleCharacter . 'Tiền chuyển phòng: ' . $totalRoomFee['TransferRoomFee']);
                $row++;
            } else {
                $activeSheet->removeRow($row, 1);
            }
            
            //Write room Fee
            $activeSheet->setCellValue('AD' . $row, $listStyleCharacter . 'Tiền phòng: ' . $totalRoomFee['RoomFee']);
            $row++;

            //Write services Fee
            $checkDisplay = substr($totalRoomFee['ServicesFee'], 0, strlen($totalRoomFee['ServicesFee']) - 4);
            if ((int)$checkDisplay !== 0) {
                $activeSheet->setCellValue('AD' . $row, $listStyleCharacter . 'Tiền dịch vụ: ' . $totalRoomFee['ServicesFee']);
                $row++;
            } else {
                $activeSheet->removeRow($row, 1);
            }

            //Write deposit Fee
            $checkDisplay = substr($totalRoomFee['DepositFee'], 0, strlen($totalRoomFee['DepositFee']) - 4);
            if ((int)$checkDisplay !== 0) {
                $activeSheet->setCellValue('AD' . $row, $listStyleCharacter . 'Tiền đặt cọc: ' . $totalRoomFee['DepositFee']);
                $row++;
            } else {
                $activeSheet->removeRow($row, 1);
            }

            //Write early/late Fee
            $checkDisplay = substr($totalRoomFee['ModeFee'], 0, strlen($totalRoomFee['ModeFee']) - 4);
            if ((int)$checkDisplay !== 0) {
                $activeSheet->setCellValue('AD' . $row, $listStyleCharacter . 'Tiền nhận phòng sớm / trả phòng muộn: ' . $totalRoomFee['ModeFee']);
                $row++;
            } else {
                $activeSheet->removeRow($row, 1);
            }

            //Write more people
            $checkDisplay = substr($totalRoomFee['ExtrasAldultFee'], 0, strlen($totalRoomFee['ExtrasAldultFee']) - 4);
            if ((int)$checkDisplay !== 0) {
                $activeSheet->setCellValue('AD' . $row, $listStyleCharacter . 'Tiền thêm người: ' . $totalRoomFee['ExtrasAldultFee']);
                $row++;
            } else {
                $activeSheet->removeRow($row, 1);
            }

            //Write vat Fee
            $row++;
            if ($totalRoomFee['VATMessage'] !== '') {
                $activeSheet->setCellValue('AD' . $row, $totalRoomFee['VATMessage']);
                $row++;
            } else {
                $activeSheet->removeRow($row, 1);
            }
            //Write total Fee
            $activeSheet->setCellValue('AD' . $row, $totalRoomFee['TotalFee']);

            //Write booking info
            $activeSheet
                ->setCellValue('B' . $tmpRow, ($i + 1))
                ->setCellValue('D' . $tmpRow, $tmp['BrCustomerName'])
                ->setCellValue('J' . $tmpRow, $roomObj['RoName'])
                ->setCellValue('O' . $tmpRow, $this->ConvertStringToPrice($roomObj['RoPrice']) . ' ' . $this->currency)
                ->setCellValue('T' . $tmpRow, $tmp['BrFromDate'])
                ->setCellValue('Y' . $tmpRow, $tmp['BrToDate']);

            //Update total money
            $tmpTotalMoney = substr($totalRoomFee['TotalFee'], 0, strlen($totalRoomFee['TotalFee']) - 4);
            $tmpTotalMoney = str_replace(',', '', $tmpTotalMoney);
            $totalMoney += (int)$tmpTotalMoney;

            //Increase current row index
            $row++;

        }
        
        //Write sum total price
        $this->CopyRows($activeSheet, $templateSumBeginRowIndex, $row, $templateSumRowsCount, $maxColumn);
        $activeSheet->setCellValue('AO' . ($row + 1), 'TỔNG DOANH THU: ' . $this->ConvertStringToPrice($totalMoney) . ' ' . $this->currency);
       
    }

    /*
         * Copy rows and format
         */
    function CopyRows($sheet, $srcRow, $dstRow, $height, $width)
    {
        for ($row = 0; $row < $height; $row++) {
            for ($col = 0; $col < $width; $col++) {
                $cell = $sheet->getCellByColumnAndRow($col, $srcRow + $row);
                $style = $sheet->getStyleByColumnAndRow($col, $srcRow + $row);
                $dstCell = \PHPExcel_Cell::stringFromColumnIndex($col) . (string)($dstRow + $row);
                $sheet->setCellValue($dstCell, $cell->getValue());
                $sheet->duplicateStyle($style, $dstCell);
            }

            $h = $sheet->getRowDimension($srcRow + $row)->getRowHeight();
            $sheet->getRowDimension($dstRow + $row)->setRowHeight($h);
        }

        foreach ($sheet->getMergeCells() as $mergeCell) {
            $mc = explode(":", $mergeCell);
            $col_s = preg_replace("/[0-9]*/", "", $mc[0]);
            $col_e = preg_replace("/[0-9]*/", "", $mc[1]);
            $row_s = ((int)preg_replace("/[A-Z]*/", "", $mc[0])) - $srcRow;
            $row_e = ((int)preg_replace("/[A-Z]*/", "", $mc[1])) - $srcRow;

            if (0 <= $row_s && $row_s < $height) {
                $merge = $col_s . (string)($dstRow + $row_s) . ":" . $col_e . (string)($dstRow + $row_e);
                $sheet->mergeCells($merge);
            }
        }
    }

    /*
     * Export report
     */

    public function ExportReport()
    {

        try {
            //Get booking info
            $arrBooking = $this->GetAll('BookingRoom', array(), array('BrCreatedDate ASC'));
            $arrRoom = $this->GetAll('Rooms', array(), array('RoCreatedDate ASC'));

            //Get post data
            $data = $this->request->getData();

            //Sort data
            $arrData = $this->SortBooking($arrBooking, $data['fromDate'], $data['toDate']);

            //Get file name and file url
            $fileName = $this->GenerateFileName($this->prefixReport, array(
                'fromDate' => $data['fromDate'],
                'toDate' => $data['toDate']
            ));


            //Export excel
            if ($data['type'] === 'excel') {
                $fileName = $fileName . $this->ExcelFileExtinction;
//                //Update config
//                $this->SaveConfigData('ExportFromDate', $data['fromDate']);
//                $this->SaveConfigData('ExportToDate', $data['toDate']);
//                $this->SaveConfigData('ExportFileName', $fileName);                
                $this->ExportExcel($data['fromDate'], $data['toDate'], $fileName);                
            } else {
                //Create object pdf
                $mpdf = new \mPDF();

                //Load template
                $mpdf = $this->OpenTemplateFile($mpdf, $this->templateReport, 'L', 3.5, 5, 294);

                //Write bill data
                $this->ReportHtml($mpdf, $arrData, $arrRoom, $data);

                //Set output pdf file
                ob_end_clean();

                $fileName = $fileName . $this->PdfFileExtinction;
                $fileUrl = $this->path . DS . $this->reportFolder . DS . $fileName;
                $mpdf->Output($fileUrl);

            }

            //Disable render layout
            $this->layout = false;
            $this->autoRender = false;
            $this->response->type('json');
            $json = json_encode(array(
                'success' => true,
                'file_name' => $fileName
            ));
            //Return json
            $this->response->body($json);
            return $this->response;

            //$mpdf->Output();
        } catch (Exception $exc) {
            $this->ExceptionHandle();
        }
    }

    /*
     * Sort booking data fromDate and toDate
     */

    function SortBooking($arrBooking, $fromDate, $toDate)
    {
        $arrSort = array();
        for ($i = 0; $i < count($arrBooking); $i++) {
            $tmp = $arrBooking[$i];
            if (($this->CompareTwoDates($tmp['BrToDate'], $fromDate) === 1 || $this->CompareTwoDates($tmp['BrToDate'], $fromDate) === 0) &&
                ($this->CompareTwoDates($tmp['BrToDate'], $toDate) === 2 || $this->CompareTwoDates($tmp['BrToDate'], $toDate) === 1)) {
                array_push($arrSort, $tmp);
            }
        }
        return $arrSort;
    }

    /*
     * Establish room bill html
     */

    function ReportHtml($mpdf, $arrBooking, $arrRoom, $inp)
    {

        //Add css
        $stylesheet = file_get_contents($this->path . 'report.css'); // external css
        $mpdf->WriteHTML($stylesheet, 1);

        //Logo
        $logo = $this->GetConfigData('logo') === '' || $this->GetConfigData('logo') === null ? 'no_image.png' : $this->GetConfigData('logo');
        $logo = $this->upload_path . DS . 'images' . DS . $logo;
        $mpdf->Image($logo, 16, 12, 40, 18, 'jpg', '', true, false);

        //Hotel infomation
        $header = '<div id="divHotelInfo">
                    <h2 class="font-bold">' . $this->GetConfigData('hotel-name') . '</h2>
                    <p >' . $this->GetConfigData('address') . '</p>
                    <p >' . $this->GetConfigData('phone') . '</p>
                    <p >' . $this->GetConfigData('email') . '</p>
                   </div>';
        $mpdf->WriteFixedPosHTML($header, 61, 8, 100, 100, 'visible');

        //Report mode
        $exportDate = '<div class="font-bold report-title">
            <h2>HÓA ĐƠN BÁO CÁO KINH DOANH</h2>
            </div>';
        $mpdf->WriteFixedPosHTML($exportDate, 165, 16.5, 200, 100, 'auto');

        //Export rangge date
        $date = new DateTime();
        $date = date_format($date, $this->dateFromat);
        $exportDate = '<div id="divDate">
            <p>' . $inp['fromDate'] . ' - ' . $inp['toDate'] . '</p>
            <p>' . $date . '</p>
            </div>';
        $mpdf->WriteFixedPosHTML($exportDate, 45, 33, 100, 100, 'auto');

        //Exporter
        $exportDate = '<div>
            <p>' . $this->loginInfo['UsFullName'] . '</p>
            </div>';
        $mpdf->WriteFixedPosHTML($exportDate, 270, 37, 100, 100, 'auto');

        //Write data table
        $this->WriteDataTable($mpdf, $arrBooking, $arrRoom);
    }

    //Write report data table
    function WriteDataTable($mpdf, $arrBooking, $arrRoom)
    {
        $totalMoney = 0;
        $html = '<table id="tableData">
        <thead>
        <tr class="tr-header">
            <th class="id-column">#</th>
            <th>Tên Khách Hàng</th>
            <th>Tên Phòng</th>
            <th>Giá Phòng</th>
            <th>Ngày Đến</th>
            <th>Ngày Đi</th>
            <th class="th-total-money">Tổng Tiền</th>
        </tr>
        </thead>
        <tbody>';

        for ($i = 0; $i < count($arrBooking); $i++) {

            $tmp = $arrBooking[$i];

            //Get room data

            $roomObj = $this->FindInArray($arrRoom, 'RoRoomId', $tmp['BrRoomId']);

            //Calculating total money
            $totalRoomFee = $this->CalculatingRoomFee($tmp);

            //Update html
            $html .= '<tr>
                <td class="id-column">' . ($i + 1) . '</td>
                <td>' . $tmp['BrCustomerName'] . '</td>
                <td>' . $roomObj['RoName'] . '</td>
                <td>' . $this->ConvertStringToPrice($roomObj['RoPrice']) . ' ' . $this->currency . '</td>
                <td>' . $tmp['BrFromDate'] . '</td>
                <td>' . $tmp['BrToDate'] . '</td>
                <td class="th-total-money td-total-money">
                    <ul>';

            $checkDisplay = substr($totalRoomFee['TransferRoomFee'], 0, strlen($totalRoomFee['TransferRoomFee']) - 4);
            if ((int)$checkDisplay !== 0) {
                $html .= '<li>Tiền chuyển phòng: ' . $totalRoomFee['TransferRoomFee'] . '</li>';
            }

            $html .= '<li>Tiền phòng:' . $totalRoomFee['RoomFee'] . '</li>';

            $checkDisplay = substr($totalRoomFee['ServicesFee'], 0, strlen($totalRoomFee['ServicesFee']) - 4);
            if ((int)$checkDisplay !== 0) {
                $html .= '<li>Tiền dịch vụ: ' . $totalRoomFee['ServicesFee'] . '</li>';
            }

            $checkDisplay = substr($totalRoomFee['DepositFee'], 0, strlen($totalRoomFee['DepositFee']) - 4);
            if ((int)$checkDisplay !== 0) {
                $html .= '<li>Tiền đặt cọc: ' . $totalRoomFee['DepositFee'] . '</li>';
            }

            $checkDisplay = substr($totalRoomFee['ModeFee'], 0, strlen($totalRoomFee['ModeFee']) - 4);
            if ((int)$checkDisplay !== 0) {
                $html .= '<li>Tiền nhận phòng sớm / trả phòng muộn: ' . $totalRoomFee['ModeFee'] . '</li>';
            }

            $checkDisplay = substr($totalRoomFee['ExtrasAldultFee'], 0, strlen($totalRoomFee['ExtrasAldultFee']) - 4);
            if ((int)$checkDisplay !== 0) {
                $html .= '<li>Tiền thêm người: ' . $totalRoomFee['ExtrasAldultFee'] . '</li>';
            }
            $html .= '</ul>
                    <hr>
                    <p class="total-money-label"><b>Tổng tiền: </b></p>
                    <p class="vat-wrap">' . $totalRoomFee['VATMessage'] . '</p>
                    <h1>' . $totalRoomFee['TotalFee'] . '</h1>
                </td>
            </tr>';
            //Update total money
            $tmpTotalMoney = substr($totalRoomFee['TotalFee'], 0, strlen($totalRoomFee['TotalFee']) - 4);
            $tmpTotalMoney = str_replace(',', '', $tmpTotalMoney);
            $totalMoney += (int)$tmpTotalMoney;
        }

        $html .= '</tbody>
    </table>';
        //Write total money
        $html .= '
        <h1 id="totalMoneyWrap">
        TỔNG DOANH THU: ' . $this->ConvertStringToPrice($totalMoney) . ' ' . $this->currency .
            '</h1>';
        if ($mpdf !== null) {
            $mpdf->WriteHTML($html, 2);
        } else {
            return $html;
        }

    }


}
