<?php

/*
 * Common header sticker
 */

class common_headerSticker extends Nine_Sticker {

    public function run() {
        try {
            /*
             * Get config
             */
            $this->view->config = Nine_Query::getConfig();
            /*
             * Get list language
             */
            $this->view->allLang = Nine_Language::getAllLanguages();
            /*
             * Get current lang
             */
            $this->view->currentLang = Nine_Language::getCurrentLang();
            /*
             * Get article about us
             */
            // if (Nine_Language::getCurrentLangCode() == 'en') {
            //     $this->view->aboutUs = @reset(Nine_Query::getListContents(array(
            //                         'alias' => 'about-us'
            //     )));
            // } else {
            //     $this->view->aboutUs = @reset(Nine_Query::getListContents(array(
            //                         'alias' => 'gioi-thieu'
            //     )));
            // }
            $this->view->aboutUs = @reset(Nine_Query::getListContents(array(
                                    'isSpecialPost' => 1
                )));
            /*
             * Get article support
             */
            // if (Nine_Language::getCurrentLangCode() == 'en') {
            //     $this->view->support = @reset(Nine_Query::getListContents(array(
            //                         'alias' => 'support'
            //     )));
            // } else {
            //     $this->view->support = @reset(Nine_Query::getListContents(array(
            //                         'alias' => 'ho-tro'
            //     )));
            // }
            $this->view->support = @reset(Nine_Query::getListContents(array(
                                    'isSpecialPost' => 2
                )));
            /*
             * Get logged user
             */
            $loggedUser = Nine_Common::getLoggedUser($this, true);
            $this->view->loggedUser = $loggedUser;
            /*
             * Get all parent product category
             */
            $this->view->comSearchAllCategory = Nine_Query::getListProductCategory(array(
                        'parent_id' => 0
            ));
            /*
             * Get cart
             */
            $allCard = Nine_Query::getCart($this->session);
            //$this->session->cart = array();
            if (count($allCard) > 0) {
                $this->view->allCard = $allCard;
            } else {
                $this->view->allCard = array(
                    'cartDetail' => array(),
                    'total_price' => 0
                );
            }
            /*
             * get header menu
             */
            $this->getHeaderMenu();
            /*
             * Get main menu
             */
            $this->GetMainMenu();
            /*
             * News menu item
             */
            if (Nine_Language::getCurrentLangCode() == 'vi') {
                $newsMenu = @reset(Nine_Query::getListContentCategory(array(
                                    'alias' => 'tin-tuc'
                )));
            } else {
                $newsMenu = @reset(Nine_Query::getListContentCategory(array(
                                    'alias' => 'news-info'
                )));
            }
            if (count($newsMenu) <= 0) {
                $url = Nine_Route::url();
                $this->view->newsMenu = array(
                    'name' => Nine_Language::translate('Tin tức'),
                    'url' => $url['path'] . 'news/danh-sach-tin-tuc',
                    'subCat' => array()
                );
            } else {
                /*
                 * get sub car
                 */
                $newsMenu['subCat'] = Nine_Query::getListContentCategory(array(
                            'parent_id' => $newsMenu['content_category_gid']
                ));
                $this->view->newsMenu = $newsMenu;
            }
            /*
             * Get library menu item
             */
            $url = Nine_Route::url();
            $this->view->libraryMenu = array(
                'name' => Nine_Language::translate('Thư viện'),
                'url' => '#',
                'subCat' => array(
                    @reset(Nine_Query::getListContentCategory(array(
                                'alias' => Nine_Language::getCurrentLangCode() == 'vi' ? 'hinh-anh' : 'images'
                    ))),
                    @reset(Nine_Query::getListContentCategory(array(
                                'alias' => 'videos'
                    ))),
                )
            );
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /*
     * Get header menu
     */

    public function getHeaderMenu() {
        /*
         * Get list home category
         */
        $allCat = Nine_Query::getListProductCategory(array(
                    'is_display' => 1,
                    'parent_id' => 0
        ));
        /*
         * Add sub cat
         */
        foreach ($allCat as &$value) {
            /*
             * Add sub cat
             */
            $value['subCat'] = Nine_Query::getListProductCategory(array(
                        'parent_id' => $value['product_category_gid']
            ));
            /*
             * Add second level cat
             */
            foreach ($value['subCat'] as &$content) {
                $content['subCat'] = Nine_Query::getListProductCategory(array(
                            'parent_id' => $content['product_category_gid']
                ));
            }
            unset($content);
            /*
             * Add list products
             */
            $value['products'] = Nine_Query::getListProductsByCatId($value['product_category_gid']);
        }
        unset($value);
        
        $this->view->allCat = $allCat;
        /*
         * Get contact route url
         */
        $this->view->Contact = Nine_Route::_("contact/contac-us");
    }
    /*
     * Get main menu
     */
    public function GetMainMenu(){
        $url = Nine_Route::url();
        $allCat = array(
            array(
                'name' => Nine_Language::translate("Sản phẩm hot"),
                'url' => $url['path'].'category/' . (Nine_Language::getCurrentLangCode() == 'vi' ? 'san-pham-hot' : 'hot-product')
            ),
            array(
                'name' => Nine_Language::translate("Sản phẩm sale"),
                'url' => $url['path'].'category/' . (Nine_Language::getCurrentLangCode() == 'vi' ? 'san-pham-sale' : 'sale-product')
            ),
            array(
                'name' => Nine_Language::translate("Sản phẩm special"),
                'url' => $url['path'].'category/' . (Nine_Language::getCurrentLangCode() == 'vi' ? 'san-pham-special' : 'special-product')
            )
        );
        $this->view->mainMenu = $allCat;
    }

}
