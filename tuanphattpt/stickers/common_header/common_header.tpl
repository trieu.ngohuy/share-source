<div id="header" class="header">
    <div class="top-header">
        <div class="container">            
            <div class="language ">
                <div class="dropdown">
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                        <img alt="{{$currentLang.name}}" src="{{$currentLang.lang_image}}" />{{$currentLang.name}}

                    </a>
                    <ul class="dropdown-menu" role="menu">
                        {{foreach from=$allLang item=item}}
                        <li><a href="{{$BASE_URL}}{{$item.lang_code}}">
                                <img alt="{{$item.name}}" src="{{$item.lang_image}}" />{{$item.name}}
                            </a></li>
                            {{/foreach}}
                    </ul>
                </div>
            </div>
            <div class="top-bar-social">
                <a href="{{$config.yahoo}}"><i class="fa fa-yahoo"></i></a>
                <a href="{{$config.skype}}"><i class="fa fa-skype"></i></a>
                <a href="{{$config.youtube}}"><i class="fa fa-youtube"></i></a>
                <a href="{{$config.google}}"><i class="fa fa-google-plus"></i></a>
            </div>
            <div class="clearfix header-clearfix" style="display: none;"></div>
            <div class="support-link dropdown">
                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="submenu-hover">{{l}}About Us{{/l}}</a>
                <ul class="support-submenu dropdown-menu mega_dropdown">
                    <li><a href="{{if $aboutUs.url == ''}}#{{else}}{{$aboutUs.url}}{{/if}}">{{if $aboutUs.title == ''}}{{l}}About Us{{/l}}{{else}}{{$aboutUs.title}}{{/if}}</a></li>
                    <li><a href="{{if $support.url == ''}}#{{else}}{{$support.url}}{{/if}}">{{if $support.title == ''}}{{l}}Support{{/l}}{{else}}{{$support.title}}{{/if}}</a></li>
                </ul>
            </div>

            {{if $loggedUser == ''}}
            <div id="user-info-top" class="user-info pull-right">
                <div class="dropdown">
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span>{{l}}Tài khoản của tôi{{/l}}</span></a>
                    <ul class="dropdown-menu mega_dropdown" role="menu">
                        <li><a href="{{$BASE_URL}}access/login">{{l}}Đăng nhập{{/l}}</a></li>
                        <li><a href="{{$BASE_URL}}access/index/register/type/choose">{{l}}Đăng ký{{/l}}</a></li>
                    </ul>
                </div>
            </div>
            {{else}}
            <div id="user-info-top" class="user-info pull-right">
                <!--Profile menu-->
                <div class="dropdown">
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                        <span>{{l}}Xin chào{{/l}}, {{$loggedUser.full_name}}</span>
                    </a>
                    <ul class="dropdown-menu mega_dropdown" role="menu">
                        <li><a href="{{$BASE_URL}}user/edit-info"
                               title="">{{l}}Chỉnh sửa thông tin{{/l}}</a></li>
                        <li><a href="{{$BASE_URL}}user/change-password"
                               title="">{{l}}Đổi mật khẩu{{/l}}</a></li>

                        <li><a href="{{$BASE_URL}}user/upgrade" title="">{{l}}Upgrade
                                Account{{/l}}</a></li>
                        <li><a href="{{$BASE_URL}}user/manager-upgrade" title="">{{l}}Payment History{{/l}}</a></li>
                        <li><a href="{{$BASE_URL}}access/logout" title="">{{l}}Đăng xuất{{/l}}</a></li>
                    </ul>
                </div>
            </div>
            <!--Estore menu-->
            <div id="user-info-top1" class="user-info pull-right">
                <div class="dropdown">
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                        <span>{{l}}Cửa hàng{{/l}}</span>
                    </a>
                    <ul class="dropdown-menu mega_dropdown" role="menu">
                        {{if $loggedUser.group_id == 3 }}
                        <li><a target=""
                               href="{{$BASE_URL}}{{$loggedUser.alias}}.htm" title="">
                                {{l}}Your Store{{/l}} </a></li>                        
                        <li><a target="" href="{{$BASE_URL}}estore/manage-message-sale/{{$loggedUser.alias}}.html" title="">{{l}}Tin khuyến
                                mãi{{/l}}</a></li>
                        <li><a target="" href="{{$BASE_URL}}estore/{{$loggedUser.alias}}.html" title="">{{l}}Chat
                                manager{{/l}}</a></li>
                        <li><a target=""
                               href="{{$BASE_URL}}estore/manage-message/{{$loggedUser.alias}}.html"
                               title="">{{l}}Contact Center{{/l}}</a></li>                       
                        <li><a target="" href="{{$BASE_URL}}estore/manage-message-estore/{{$loggedUser.alias}}.html" title="">{{l}}Thư hỏi
                                hàng mới{{/l}}</a></li>
                        <li><a target="_blank"
                               href="{{$BASE_URL}}estore/manage-cart/{{$loggedUser.alias}}.html"
                               title="">{{l}}Báo giá mới{{/l}}</a></li>
                        <li><a target=""
                               href="{{$BASE_URL}}estore/manage-product/{{$loggedUser.alias}}.html"
                               title="">{{l}}Product Managerment{{/l}}</a></li>
                        <li><a target=""
                               href="{{$BASE_URL}}estore/edit-Info/{{$loggedUser.alias}}.html"
                               title="">{{l}}Setting{{/l}}</a></li> 
                        <li><a href="{{$BASE_URL}}user/buying-request-reply" title="">{{l}}Buying
                                Request Reply{{/l}}</a></li>
                        <li><a href="{{$BASE_URL}}user/contact" title="">{{l}}Contact
                                Mainpage{{/l}}</a></li>
                                {{else}}
                        <li><a target="" href="{{$BASE_URL}}user/cart-manager" title="">{{l}}Car Manager{{/l}}</a></li> 
                        <li><a href="{{$BASE_URL}}user/buying-request-reply" title="">{{l}}Buying Request Reply{{/l}}</a></li>
                        <li><a href="{{$BASE_URL}}user/contact" title="">{{l}}Contact Mainpage{{/l}}</a></li>
                            {{/if}}  
                    </ul>
                </div>
            </div>                      
            {{/if}}

        </div>
    </div>
    <!--/.top-header -->
    <!-- MAIN HEADER -->
    <div class="container main-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 logo">
                <a href="{{$BASE_URL}}"><img alt="{{$config.WebsiteName}}" src="{{$config.Logo}}" /></a>
            </div>
            <div class="col-xs-7 col-sm-8 col-md-7 header-search-box">
                <form class="form-inline" action="{{$BASE_URL}}product/search">
                    <div class="form-group form-category">
                        <select class="select-category" name="cat">
                            <option value="-1">{{l}}Tất cả{{/l}}</option>
                            {{foreach from=$comSearchAllCategory item=item}}
                            <option value="{{$item.product_category_id}}">{{$item.name}}</option>
                            {{/foreach}}
                        </select>
                    </div>
                    <div class="form-group input-serach">
                        <input type="text"  placeholder="{{l}}Từ khóa...{{/l}}" name="key">
                    </div>
                    <button type="submit" class="pull-right btn-search"></button>
                </form>
            </div>
            <div class="col-xs-5 col-sm-3 col-md-2 group-button-header">
                <div class="btn-cart" id="cart-block">
                    <a title="My cart" href="{{$BASE_URL}}cart">{{l}}Giỏ hàng{{/l}}</a>
                    <span class="notify notify-right">{{$allCard.cartDetail|@count}}</span>
                    <div class="cart-block">
                        <div class="cart-block-content">
                            <h5 class="cart-title">{{$allCard.cartDetail|@count}} {{l}}sản phẩm trong giỏ hàng{{/l}}</h5>
                            <div class="cart-block-list">
                                <ul>
                                    {{foreach from=$allCard.cartDetail item=cartItem}}
                                    <li class="product-info">
                                        <div class="p-left">
                                            <a href="{{$cartItem.product.url}}">
                                                <img class="img-responsive" src="{{$cartItem.product.main_image}}" alt="{{$cartItem.product.title}}">
                                            </a>
                                        </div>
                                        <div class="p-right">
                                            <p class="p-name">{{$cartItem.product.title}}</p>
                                            <p class="p-rice">{{$cartItem.product.total_price}} {{$config.currency}}</p>
                                            <p>Qty: {{$cartItem.mount}}</p>
                                        </div>
                                    </li>
                                    {{/foreach}}
                                </ul>
                            </div>
                            <div class="toal-cart">
                                <span>{{l}}Tổng tiền{{/l}}</span>
                                <span class="toal-price pull-right">{{$allCard.total_price}} {{$config.currency}}</span>
                            </div>
                            <div class="cart-buttons">
                                <a href="{{$BASE_URL}}cart" class="btn-check-out">{{l}}Tới giỏ hàng{{/l}}</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- END MANIN HEADER -->
    <div id="nav-top-menu" class="nav-top-menu">
        <div class="container">
            <div class="row">
                <div class="col-sm-3" id="box-vertical-megamenus">
                    <div class="box-vertical-megamenus">
                        <h4 class="title">
                            <span class="btn-open-mobile"><i class="fa fa-bars"></i></span>
                        </h4>
                        <div class="vertical-menu-content is-home">
                            <ul class="vertical-menu-list">
                                {{assign var='count' value='1'}}
                                {{foreach from=$allCat item=leftMenuItem}}
                                <li {{if $count > 10}}class='cat-link-orther'{{/if}}>
                                    <a {{if $leftMenuItem.subCat|@count > 0}}class="parent"{{/if}} 
                                                                             href="{{$leftMenuItem.url}}">
                                        <i class="fa {{if $leftMenuItem.faicon == ''}}fa-cart-plus{{else}}{{$leftMenuItem.faicon}}{{/if}}" style="margin-top: 13px;"></i>
                                        {{$leftMenuItem.name}}
                                    </a>
                                    {{if $leftMenuItem.subCat|@count > 0}}
                                    <div class="vertical-dropdown-menu">
                                        <div class="vertical-groups col-sm-12">
                                            {{foreach from=$leftMenuItem.subCat item=leftSubMenuItem}}
                                            <div class="mega-group col-sm-4">
                                                <h4 class="mega-group-header"><span>{{$leftSubMenuItem.name}}</span></h4>
                                                <ul class="group-link-default">
                                                    {{foreach from=$leftSubMenuItem.subCat item=secondSubMenuItem}}
                                                    <li><a href="{{$secondSubMenuItem.url}}">{{$secondSubMenuItem.name}}</a></li>
                                                        {{/foreach}}
                                                </ul>
                                            </div>
                                            {{/foreach}}
                                        </div>
                                    </div>
                                    {{/if}}
                                </li>
                                {{assign var='count' value=$count+1}}
                                {{/foreach}}
                            </ul>
                            <div class="all-category"><span class="open-cate">{{l}}Xem tất cả{{/l}}</span></div>
                        </div>
                    </div>
                </div>
                <div id="main-menu" class="col-sm-9 main-menu">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <a class="navbar-brand" href="#">MENU</a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li class='dropdown {{if $headerItem == 'home' || $headerItem == ''}}active{{/if}}'>
                                        <a href="{{$BASE_URL}}">{{l}}Trang chủ{{/l}}</a>
                                        <ul class="dropdown-menu container-fluid">
                                            <li class="block-container">
                                                <ul class="block">

                                                    <li class="link_container">
                                                        <a href="{{$aboutUs.url}}">{{l}} Giới thiệu {{/l}}</a>
                                                    </li>
                                                    <li class="link_container">
                                                        <a href="{{$support.url}}">{{l}} Hỗ trợ khách hàng {{/l}}</a>
                                                    </li>
                                                    <li class="link_container home-menu-li">
                                                        <a class="home-menu-item" href="{{$newsMenu.url}}">{{$newsMenu.name}}</a>
                                                        {{if $newsMenu.subCat|@count > 0}}
                                                        <ul class="home-second-submenu dropdown-menu container-fluid">
                                                            <li class="block-container">
                                                                <ul class="block">
                                                                    {{foreach from=$newsMenu.subCat item=subCatItem}}
                                                                    <li class="link_container"><a href="{{$subCatItem.url}}">{{$subCatItem.name}}</a></li>
                                                                        {{/foreach}}
                                                                </ul>
                                                            </li>
                                                        </ul> 
                                                        {{/if}}
                                                    </li>
                                                    <li class="link_container home-menu-li">
                                                        <a class="home-menu-item" href="{{$libraryMenu.url}}">{{$libraryMenu.name}}</a>
                                                        {{if $libraryMenu.subCat|@count > 0}}
                                                        <ul class="home-second-submenu dropdown-menu container-fluid">
                                                            <li class="block-container">
                                                                <ul class="block">
                                                                    {{foreach from=$libraryMenu.subCat item=subCatItem}}
                                                                    <li class="link_container"><a href="{{$subCatItem.url}}">{{$subCatItem.name}}</a></li>
                                                                        {{/foreach}}
                                                                </ul>
                                                            </li>
                                                        </ul> 
                                                        {{/if}}
                                                    </li>

                                                </ul>
                                            </li>
                                        </ul> 
                                    </li>

                                    {{foreach from=$mainMenu item=catItem}}
                                    <li class="{{if $headerItem == '{{$catItem.alias}}'}}active{{/if}}">
                                        <a href="{{$catItem.url}}">{{$catItem.name}}</a>
                                    </li>
                                    {{/foreach}}
                                    <!--
                                    <li class='{{if $newsMenu.subCat|@count > 0}}dropdown{{/if}} {{if $headerItem == 'tin-tuc'}}active{{/if}}'>
                                        <a href="{{$newsMenu.url}}">{{$newsMenu.name}}</a>
                                    {{if $newsMenu.subCat|@count > 0}}
                                    <ul class="dropdown-menu container-fluid">
                                        <li class="block-container">
                                            <ul class="block">
                                    {{foreach from=$newsMenu.subCat item=subCatItem}}
                                    <li class="link_container"><a href="{{$subCatItem.url}}">{{$subCatItem.name}}</a></li>
                                    {{/foreach}}
                            </ul>
                        </li>
                    </ul> 
                                    {{/if}}
                                </li>
                                
                                <li class='{{if $libraryMenu.subCat|@count > 0}}dropdown{{/if}} 
                                    {{if $headerItem == 'libary' || $headerItem == 'images' || $headerItem == 'videos'}}active{{/if}}'>
                                    <a href="{{$libraryMenu.url}}">{{$libraryMenu.name}}</a>
                                    {{if $libraryMenu.subCat|@count > 0}}
                                    <ul class="dropdown-menu container-fluid">
                                        <li class="block-container">
                                            <ul class="block">
                                    {{foreach from=$libraryMenu.subCat item=subCatItem}}
                                    <li class="link_container"><a href="{{$subCatItem.url}}">{{$subCatItem.name}}</a></li>
                                    {{/foreach}}
                            </ul>
                        </li>
                    </ul> 
                                    {{/if}}
                                </li>-->
                                    <!--Contact-->
                                    <li class='{{if $headerItem == 'contact'}}active{{/if}}'>
                                        <a href="{{$Contact}}">{{l}}Liên hệ{{/l}}</a>
                                    </li>
                                </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </nav>
                </div>
            </div>
            <!-- userinfo on top-->
            <div id="form-search-opntop">
            </div>
            <!-- userinfo on top-->
            <div id="user-info-opntop">
            </div>
            <!-- CART ICON ON MMENU -->
            <div id="shopping-cart-box-ontop">
                <a href="{{$BASE_URL}}cart"><i class="fa fa-shopping-cart"></i></a>
                <div class="shopping-cart-box-ontop-content"></div>
            </div>
        </div>
    </div>
</div>
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<a href="tel:+{{$config.phone_call}}" class="phone_ico" title="Phone" style="display: block;">
    <img src="{{$LAYOUT_HELPER_URL}}front/assets/images/phone_icon.PNG" width="50px" height="50px"/>
</a>
<script type="text/javascript">
    $(function () {
        setInterval(function () {
            $('.phone_ico').fadeOut(1000).fadeIn(1000).fadeOut(1000).fadeIn(1000).fadeOut(1000);
        }, 0);
    });
</script>