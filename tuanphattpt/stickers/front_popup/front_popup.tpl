<div id="divPopup">    
    <div id="divWrap" class="tabbable tabs-left">
        <img id="imgClose" src="{{$LAYOUT_HELPER_URL}}front/images/close.png" alt="Close" width="30"/>
        <ul class="nav nav-tabs">
            <li >
                <a href="#tab1" data-toggle="tab" aria-expanded="true" class="active show">
                    {{l}}Bảng 1{{/l}}
                </a>
            </li>
            <li >
                <a href="#tab2" data-toggle="tab" aria-expanded="false">
                    {{l}}Bảng 2{{/l}}
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="tab1" class="tab-pane active">
                <h3 class="title">{{l}}BẢNG NHẬP THÔNG SỐ{{/l}}</h3>
                <p class="title">{{l}}BẢNG SO SÁNH TIẾT KIỆM ĐÈN LED T8/T5 & ĐÈN HUỲNH QUANG T8{{/l}}</p>
                <hr>
                <table id="tbInput" class="tb-data">
                    <tbody>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}SẢN PHẨM{{/l}}</td>
                            <td class="td-header">{{l}}LED TPT LAMP{{/l}}</td>
                            <td class="td-header">{{l}}T8 FLUORESCENT{{/l}}</td>
                            <td class="td-header">{{l}}ĐƠN VỊ{{/l}}</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}SỐ NGÀY HOẠT ĐỘNG/NĂM{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp11" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp12" min="0" value="0"/></td>
                            <td class="td-last">{{l}}NGÀY/NĂM{{/l}}</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}SỐ GIỜ HOẠT ĐỘNG/NGÀY{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp21" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp22" min="0" value="0"/></td>
                            <td class="td-last">{{l}}GiỜ/NGÀY{{/l}}</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}SỐ BỘ ĐÈN ĐANG SỬ DỤNG{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp31" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp32" min="0" value="0"/></td>
                            <td class="td-last">{{l}}CÁI{{/l}}</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}CÔNG SUẤT BÓNG ĐƠN CHUYỂN ĐỔI{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp41" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp42" min="0" value="0"/></td>
                            <td class="td-last">W</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}TỔN THẤT BALLAST{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp51" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp52" min="0" value="0"/></td>
                            <td class="td-last">W</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}CHI PHÍ TIỀN ĐIỆN/1KWH TÍNH TRUNG BÌNH{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp61" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp62" min="0" value="0"/></td>
                            <td class="td-last">VND/KWH</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}GIÁ ĐẦU TƯ BAN ĐẦU/BỘ{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp71" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp72" min="0" value="0"/></td>
                            <td class="td-last">{{l}}VND/BỘ{{/l}}</td>
                        </tr>
                    </tbody>
                </table>
                <div id="divButtons">
                    <button class="btn btn-info btn-calculation" type="submit" data-tab="#tab1">
                        <i class="ace-icon fa fa-check bigger-110"></i>{{l}}Tính toán kết quả{{/l}}</button>
                    <button class="btn btn-success btn-export" type="submit" data-tab="#tab1">
                        <i class="ace-icon fa fa-check bigger-110"></i>{{l}}Xuất file PDF{{/l}}</button>
                </div>
                <br>
                <h3 class="title">{{l}}BẢNG HIỆN KẾT QUẢ{{/l}}</h3>
                <hr>
                <table id="tbResult" class="tb-data">
                    <tbody>
                        <tr>
                            <td class="td-header td-first" colspan="3">
                                {{l}}TỔNG MỨC TIẾT KIỆM/NĂM
                                (TIÊU HAO ĐIỆN NĂNG ĐÈN T8 HIỆN HỮU + CHI PHÍ THAY THẾ BẢO TRÌ HÀNG NĂM T8 HIỆN HỮU - TIÊU HAO ĐIỆN NĂNG ĐÈN LED T8 TPT + CHI PHÍ TIÊU HAO GIẢM NHIỆT ĐÈN LED T8){{/l}}
                            </td>
                            <td></td>
                            <td class="td-price td-center" id="inp82">0</td>
                            <td class="td-last td-center">VND</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3" rowspan="3">
                                {{l}}MỨC LỢI NHUẬN{{/l}}
                            </td>
                            <td class="td-header">
                                {{l}}Năm thứ 1
                                (TỔNG MỨC TIẾT KIỆM HẰNG NĂM - TỔNG ĐẦU TƯ BAN ĐẦU){{/l}}
                            </td>
                            <td class="td-price td-center" id="inp92">0</td>
                            <td class="td-last td-center">VND</td>
                        </tr>
                        <tr>
                            <td class="td-header">
                                {{l}}Năm thứ 2
                                (TỔNG MỨC TIẾT KIỆM HÀNG NĂM + LỢI NHUẬN NĂM THỨ 1){{/l}}
                            </td>
                            <td class="td-price td-center" id="inp102">0</td>
                            <td class="td-last td-center">VND</td>
                        </tr>
                        <tr>
                            <td class="td-header">
                                {{l}}Năm thứ 3
                                (TỔNG MỨC TIẾT KIỆM HÀNG NĂM + LỢI NHUẬN NĂM THỨ 1 + NĂM THỨ 2){{/l}}
                            </td>
                            <td class="td-price td-center" id="inp112">0</td>
                            <td class="td-last td-center">VND</td>
                        </tr>
                        <tr>
                            <td class="td-header" colspan="3">
                                
                            </td>
                            <td class="td-header">{{l}}Thời gian hoàn vốn{{/l}}</td>
                            <td class="td-price td-center" id="inp142"> 0 </td>
                            <td class="td-last td-center">{{l}}Tháng/Month{{/l}}</td>
                        </tr>
                        <tr>
                            <td class="td-header" colspan="3">
                                
                            </td>
                            <td class="td-header">{{l}}Tiết kiệm{{/l}}</td>
                            <td class="td-price td-center" id="inp152">0%</td>
                            <td class="td-last td-center"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="tab2" class="tab-pane">
                <h3 class="title">{{l}}BẢNG NHẬP THÔNG SỐ{{/l}}</h3>
                <p class="title">{{l}}BẢNG SO SÁNH TIẾT KIỆM ĐÈN LED CAO ÁP & ĐÈN CAO ÁP{{/l}}</p>
                <hr>
                <table id="tbInput" class="tb-data">
                    <tbody>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}SẢN PHẨM{{/l}}</td>
                            <td class="td-header">{{l}}LED HIGHBAY TPT{{/l}}</td>
                            <td class="td-header">{{l}}CAO ÁP-SODIUM/METAL{{/l}}</td>
                            <td class="td-header">{{l}}ĐƠN VỊ{{/l}}</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}SỐ NGÀY HOẠT ĐỘNG/NĂM{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp11" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp12" min="0" value="0"/></td>
                            <td class="td-last">{{l}}NGÀY/NĂM{{/l}}</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}SỐ GIỜ HOẠT ĐỘNG/NGÀY{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp21" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp22" min="0" value="0"/></td>
                            <td class="td-last">{{l}}GiỜ/NGÀY{{/l}}</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}SỐ BỘ ĐÈN ĐANG SỬ DỤNG{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp31" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp32" min="0" value="0"/></td>
                            <td class="td-last">{{l}}CÁI{{/l}}</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}CÔNG SUẤT BÓNG ĐƠN CHUYỂN ĐỔI{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp41" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp42" min="0" value="0"/></td>
                            <td class="td-last">W</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}TỔN THẤT BALLAST{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp51" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp52" min="0" value="0"/></td>
                            <td class="td-last">W</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}CHI PHÍ TIỀN ĐIỆN/1KWH TÍNH TRUNG BÌNH{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp61" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp62" min="0" value="0"/></td>
                            <td class="td-last">VND/KWH</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3">{{l}}GIÁ ĐẦU TƯ BAN ĐẦU/BỘ{{/l}}</td>
                            <td><input class="form-control" type="number" id="inp71" min="0" value="0"/></td>
                            <td><input class="form-control" type="number" id="inp72" min="0" value="0"/></td>
                            <td class="td-last">{{l}}VND/BỘ{{/l}}</td>
                        </tr>
                    </tbody>
                </table>
                <div id="divButtons">
                    <button class="btn btn-info btn-calculation" type="submit" data-tab="#tab2">
                        <i class="ace-icon fa fa-check bigger-110"></i>{{l}}Tính toán kết quả{{/l}}</button>
                    <button class="btn btn-success btn-export" type="submit" data-tab="#tab2">
                        <i class="ace-icon fa fa-check bigger-110"></i>{{l}}Xuất file PDF{{/l}}</button>
                </div>
                <br>
                <h3 class="title">{{l}}BẢNG HIỆN KẾT QUẢ{{/l}}</h3>
                <hr>
                <table id="tbResult" class="tb-data">
                    <tbody>
                        <tr>
                            <td class="td-header td-first" colspan="3">
                                {{l}}TỔNG MỨC TIẾT KIỆM/NĂM
                                (TIÊU HAO ĐIỆN NĂNG ĐÈN T8 HIỆN HỮU + CHI PHÍ THAY THẾ BẢO TRÌ HÀNG NĂM T8 HIỆN HỮU - TIÊU HAO ĐIỆN NĂNG ĐÈN LED T8 TPT + CHI PHÍ TIÊU HAO GIẢM NHIỆT ĐÈN LED T8){{/l}}
                            </td>
                            <td></td>
                            <td class="td-price td-center" id="inp92">0</td>
                            <td class="td-last td-center">VND</td>
                        </tr>
                        <tr>
                            <td class="td-header td-first" colspan="3" rowspan="3">
                                {{l}}MỨC LỢI NHUẬN{{/l}}
                            </td>
                            <td class="td-header">
                                {{l}}Năm thứ 1
                                (TỔNG MỨC TIẾT KIỆM HẰNG NĂM - TỔNG ĐẦU TƯ BAN ĐẦU){{/l}}
                            </td>
                            <td class="td-price td-center" id="inp102">0</td>
                            <td class="td-last td-center">VND</td>
                        </tr>
                        <tr>
                            <td class="td-header">
                                {{l}}Năm thứ 2
                                (TỔNG MỨC TIẾT KIỆM HÀNG NĂM + LỢI NHUẬN NĂM THỨ 1){{/l}}
                            </td>
                            <td class="td-price td-center" id="inp112">0</td>
                            <td class="td-last td-center">VND</td>
                        </tr>
                        <tr>
                            <td class="td-header">
                                {{l}}Năm thứ 3
                                (TỔNG MỨC TIẾT KIỆM HÀNG NĂM + LỢI NHUẬN NĂM THỨ 1 + NĂM THỨ 2){{/l}}
                            </td>
                            <td class="td-price td-center" id="inp122">0</td>
                            <td class="td-last td-center">VND</td>
                        </tr>
                        <tr>
                            <td class="td-header" colspan="3">
                                
                            </td>
                            <td class="td-header">{{l}}Thời gian hoàn vốn{{/l}}</td>
                            <td class="td-price td-center" id="inp152"> 0 </td>
                            <td class="td-last td-center">{{l}}Tháng/Month{{/l}}</td>
                        </tr>
                        <tr>
                            <td class="td-header" colspan="3">
                                
                            </td>
                            <td class="td-header">{{l}}Tiết kiệm{{/l}}</td>
                            <td class="td-price td-center" id="inp162">0%</td>
                            <td class="td-last td-center"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
    {{if isset($hidePopup) === true}}
        //First init
        Init();
    {{/if}}
        //Jquery event
        JqueryEvents();
        
        //Calculating button click
        jQuery('.btn-calculation').click(function () {
            //Fill data
            FillResultTable(jQuery(this).attr('data-tab'));
        });
        //Export pdf button click
        jQuery('.btn-export').click(function () {
            //Get tab
            var tab = jQuery(this).attr('data-tab');
            //Fill data
            var data = FillResultTable(tab);
            //Set file index
            if (tab === '#tab1') {
                data['file_index'] = 'table_1';
            } else {
                data['file_index'] = 'table_2';
            }
            //Export pdf
            ExportPdf(data);
        });
    });
    /*
     * Export pdf
     */
    function ExportPdf(data) {
        jQuery.ajax({
            type: 'post',
            url: '{{$APP_BASE_URL}}default/index/export-pdf',
            data: {
                data: data
            },
            success: function (response) {
                //Check if success
                if (response === "﻿1") {
                    console.log('Successfull export pdf');
                    //Openpdf file
                    window.open('{{$BASE_URL}}default/index/view-pdf');
                } else {
                    alert('Xuất file PDF thất bại!');
                }
            },
            error: function (xhr, status, error) {
                console.log('Export pdf failure: ' + error.message);
            }
        });
    }
    /*
     * Jquery function
     */
    function JqueryEvents() {
        //Tab click
        jQuery('#divPopup .nav-tabs a').click(function (e) {
            //Get href
            var href = jQuery(this).attr('href');
            //Remove active class
            jQuery('#divPopup .nav-tabs a').removeClass('active show');
            jQuery('#divPopup .tab-pane').removeClass('active');
            //Add active class
            jQuery(this).addClass('active show');
            jQuery(href).addClass('active');
            //Scroll to top
            e.preventDefault()
        });
        //Close pơpup screen
        jQuery('#imgClose').click(function () {
            jQuery('#divPopup').fadeOut('slow');
        });
    }
    /*
     * First init popup
     */
    function Init() {
        //Popup screen appear
        if (jQuery(window).width() >= 760) {
            jQuery('#divPopup').fadeIn('fast');
        } else {
            jQuery('#divPopup').fadeOut('fast');
        }
        //Windows resize event
        jQuery(window).resize(function () {
            if (jQuery(window).width() >= 760) {
                jQuery('#divPopup').fadeIn('fast');
            } else {
                jQuery('#divPopup').fadeOut('fast');
            }
        });

        //Set popup height
        jQuery('#divPopup').css('height', jQuery(document).height() + 'px');
        //Body scroll event
        jQuery(document).on('scroll touchmove mousewheel', function (e) {
            if (jQuery("#divPopup").is(":visible") === false) {
                return;
            }
            //Get curret top scroll position
            var top = jQuery(this).scrollTop() + 50;
            //Get popup bottom position
            var bottom = jQuery('#divPopup #divWrap').position().top + jQuery('#divPopup #divWrap').height();

            if (top >= bottom - 439) {
                jQuery(document).scrollTop(bottom - 439);
            }
        });
    }
    /*
     * Get input data
     */
    function GetData(table) {
        return {
            val1: {
                val1: parseFloat(jQuery(table + ' #tbInput #inp11').val()),
                val2: parseFloat(jQuery(table + ' #tbInput #inp12').val())
            },
            val2: {
                val1: parseFloat(jQuery(table + ' #tbInput #inp21').val()),
                val2: parseFloat(jQuery(table + ' #tbInput #inp22').val())
            },
            val3: {
                val1: parseFloat(jQuery(table + ' #tbInput #inp31').val()),
                val2: parseFloat(jQuery(table + ' #tbInput #inp32').val())
            },
            val4: {
                val1: parseFloat(jQuery(table + ' #tbInput #inp41').val()),
                val2: parseFloat(jQuery(table + ' #tbInput #inp42').val())
            },
            val5: {
                val1: parseFloat(jQuery(table + ' #tbInput #inp51').val()),
                val2: parseFloat(jQuery(table + ' #tbInput #inp52').val())
            },
            val6: {
                val1: parseFloat(jQuery(table + ' #tbInput #inp61').val()),
                val2: parseFloat(jQuery(table + ' #tbInput #inp62').val())
            },
            val7: {
                val1: parseFloat(jQuery(table + ' #tbInput #inp71').val()),
                val2: parseFloat(jQuery(table + ' #tbInput #inp72').val())
            }
        };
    }
    /*
     * Calculating table 1
     */
    function CalculatingTable1(data) {
        var result = {};
        //0
        result['val0'] = {
            val1: 50000,
            val2: 6000
        };
        //1
        result['val1'] = {
            val1: parseInt((data.val1.val1 * data.val2.val1).toFixed(2)),
            val2: parseInt((data.val1.val1 * data.val2.val1).toFixed(2))
        };
        //2
        result['val2'] = {
            val1: parseFloat((result.val0.val1 / (result.val1.val1 === 0 ? 1 : result.val1.val1)).toFixed(1)),
            val2: parseFloat((result.val0.val2 / (result.val1.val2 === 0 ? 1 : result.val1.val2)).toFixed(1))
        };
        //3
        result['val3'] = {
            val1: parseInt((data.val4.val1 + data.val5.val1).toFixed(2)),
            val2: parseInt((data.val4.val2 + data.val5.val2).toFixed(2))
        };
        //4
        result['val4'] = {
            val1: parseInt((result.val3.val1 * data.val3.val1 * result.val1.val1 / 1000).toFixed(2)),
            val2: parseInt((result.val3.val2 * data.val3.val2 * result.val1.val2 / 1000).toFixed(2))
        };
        //5
        result['val5'] = {
            val1: parseInt((data.val7.val1 * data.val3.val1 * 2 / 100).toFixed(2)),
            val2: parseInt((data.val3.val1 * 25000).toFixed(2))
        };
        //6
        result['val6'] = {
            val1: parseInt((data.val7.val1 * data.val3.val1).toFixed(2)),
            val2: parseInt((data.val7.val2 * data.val3.val2).toFixed(2))
        };
        //7
        result['val7'] = {
            val1: parseInt((result.val4.val1 * data.val6.val1).toFixed(2)),
            val2: parseInt((result.val4.val2 * data.val6.val2).toFixed(2))
        };
        //8
        result['val8'] = {
            val1: 0,
            val2: parseInt(((result.val7.val2 + result.val6.val2 + result.val5.val2) - (result.val7.val1 + result.val6.val1 + result.val5.val1)).toFixed(2))
        };
        //9
        result['val9'] = {
            val1: 0,
            val2: result.val8.val2
        };
        //10
        result['val10'] = {
            val1: 0,
            val2: parseInt((result.val9.val2 + (result.val7.val2 - result.val7.val1) + (result.val5.val2 - result.val5.val1)).toFixed(2))
        };
        //11
        result['val11'] = {
            val1: 0,
            val2: parseInt((result.val10.val2 + (result.val7.val2 - result.val7.val1) + (result.val5.val2 - result.val5.val1)).toFixed(2))
        };
        //12
        result['val12'] = {
            val1: 0,
            val2: parseInt((result.val8.val2 / (result.val6.val1 === 0 ? 1 : result.val6.val1)) * 100)
        };
        //13
        result['val13'] = {
            val1: 0,
            val2: parseInt(((result.val6.val1 - result.val6.val2) / (result.val8.val2 === 0 ? 1 : result.val8.val2)).toFixed(2))
        }
        //14
        result['val14'] = {
            val1: 0,
            val2: parseInt((12 / (result.val13.val2 === 0 ? 1 : result.val13.val2)).toFixed(2))
        }
        //15
        result['val15'] = {
            val1: 0,
            val2: parseInt((result.val8.val2 * 100 / (result.val7.val2 === 0 ? 1 : result.val7.val2)).toFixed(2))
        }
        return result;
    }
    /*
     * Calculating table 2
     */
    function CalculatingTable2(data) {
        var result = {};
        //0
        result['val0'] = {
            val1: 50000,
            val2: 10000
        };
        //1
        result['val1'] = {
            val1: parseInt((data.val1.val1 * data.val2.val1).toFixed(2)),
            val2: parseInt((data.val1.val1 * data.val2.val1).toFixed(2))
        };
        //2
        result['val2'] = {
            val1: parseFloat((result.val0.val1 / (result.val1.val1 === 0 ? 1 : result.val1.val1)).toFixed(1)),
            val2: parseFloat((result.val0.val2 / (result.val1.val2 === 0 ? 1 : result.val1.val2)).toFixed(1))
        };
        //3
        result['val3'] = {
            val1: parseInt((data.val4.val1 + data.val5.val1).toFixed(2)),
            val2: parseInt((data.val4.val2 + data.val5.val2).toFixed(2))
        };
        //4
        result['val4'] = {
            val1: '80 - 90',
            val2: '250 - 300'
        };
        //5
        result['val5'] = {
            val1: parseInt((result.val3.val1 * data.val3.val1 * result.val1.val1 / 1000).toFixed(2)),
            val2: parseInt((result.val3.val2 * data.val3.val2 * result.val1.val2 / 1000).toFixed(2))
        };
        //7
        result['val7'] = {
            val1: parseInt((data.val7.val1 * data.val3.val1).toFixed(2)),
            val2: parseInt((data.val7.val2 * data.val3.val2).toFixed(2))
        };
        //6
        result['val6'] = {
            val1: parseInt((result.val7.val1 * 3 / 100).toFixed(2)),
            val2: parseInt((data.val3.val2 * 200000).toFixed(2))
        };

        //8
        result['val8'] = {
            val1: parseInt((result.val5.val1 * data.val6.val1).toFixed(2)),
            val2: parseInt((result.val5.val2 * data.val6.val2).toFixed(2))
        };
        //9
        result['val9'] = {
            val1: 0,
            val2: parseInt(((result.val8.val2 + result.val7.val2 + result.val6.val2) - (result.val8.val1 + result.val7.val1 + result.val6.val1)).toFixed(2))
        };
        //10
        result['val10'] = {
            val1: 0,
            val2: result.val9.val2
        };
        //11
        result['val11'] = {
            val1: 0,
            val2: parseInt((result.val10.val2 + (result.val8.val2 - result.val8.val1) + (result.val6.val2 - result.val6.val1)).toFixed(2))
        };
        //12
        result['val12'] = {
            val1: 0,
            val2: parseInt((result.val11.val2 + (result.val8.val2 - result.val8.val1) + (result.val6.val2 - result.val6.val1)).toFixed(2))
        };
        //13
        result['val13'] = {
            val1: 0,
            val2: parseInt((result.val9.val2 * 100 / (result.val7.val1 === 0 ? 1 : result.val7.val1)).toFixed(2))
        };
        //14
        result['val14'] = {
            val1: 0,
            val2: parseFloat((result.val9.val2 / ((result.val8.val2 - result.val8.val1) === 0 ? 1 : (result.val8.val2 - result.val8.val1))).toFixed(2))
        }
        //15
        result['val15'] = {
            val1: 0,
            val2: parseFloat((12 * result.val14.val2).toFixed(2))
        }
        //16
        result['val16'] = {
            val1: 0,
            val2: parseInt((result.val9.val2 * 100 / (result.val8.val2 === 0 ? 1 : result.val8.val2)).toFixed(2))
        }
        return result;
    }
    /*
     * Fill table 1
     */
    function FillTable1(result) {
        jQuery('#tab1 #tbResult #inp82').text(FormatPrice(result.val8.val2));
        jQuery('#tab1 #tbResult #inp92').text(FormatPrice(result.val9.val2));
        jQuery('#tab1 #tbResult #inp102').text(FormatPrice(result.val10.val2));
        jQuery('#tab1 #tbResult #inp112').text(FormatPrice(result.val11.val2));

        jQuery('#tab1 #tbResult #inp142').text(result.val14.val2);
        jQuery('#tab1 #tbResult #inp152').text(result.val15.val2 + '%');
    }
    /*
     * Fill table 2
     */
    function FillTable2(result) {
        jQuery('#tab2 #tbResult #inp92').text(FormatPrice(result.val9.val2));
        jQuery('#tab2 #tbResult #inp102').text(FormatPrice(result.val10.val2));
        jQuery('#tab2 #tbResult #inp112').text(FormatPrice(result.val11.val2));
        jQuery('#tab2 #tbResult #inp122').text(FormatPrice(result.val12.val2));

        jQuery('#tab2 #tbResult #inp152').text(result.val15.val2);
        jQuery('#tab2 #tbResult #inp162').text(result.val16.val2 + '%');
    }
    /*
     * Display to result table
     */
    function FillResultTable(table) {
        //Get data
        var data = GetData(table);

        //Fill data
        if (table === '#tab1') {
            //Calculating
            var result = CalculatingTable1(data);
            FillTable1(result);
        } else {
            //Calculating
            var result = CalculatingTable2(data);
            FillTable2(result);
        }
        //Return
        return {
            input: data,
            result: result
        }
    }
    /*
     * Format price
     * @param {type} value
     * @returns {undefined}
     */
    function FormatPrice(value) {
        var formatedPrice = "";
        //Convert to string
        value = value.toString();
        //Loop each char of value
        var count = 1;
        for (var i = value.length - 1; i >= 0; i--) {
            //Add ',' between each 3 number
            if (count >= 3 && i > 0 && count % 3 === 0) {
                formatedPrice += value[i] + ",";
            } else {
                formatedPrice += value[i];
            }
            count++;
        }
        //Rever formated price then return
        return formatedPrice.split('').reverse().join('');
    }
</script>