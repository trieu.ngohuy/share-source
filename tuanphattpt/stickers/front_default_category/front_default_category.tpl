<!-- Blog category -->
<div class="block left-module">
    <p class="title_block">{{l}}Categories{{/l}}</p>
    <div class="block_content">
        <!-- layered -->
        <div class="layered layered-category">
            <div class="layered-content">
                <ul class="tree-menu">
                    {{foreach from=$category item=item}}
                    <li><span></span><a href="{{$BASE_URL}}category/{{$item.alias}}">{{$item.name}}</a></li>
                    {{/foreach}}
                </ul>
            </div>
        </div>
        <!-- ./layered -->
    </div>
</div>
<!-- ./blog category  -->