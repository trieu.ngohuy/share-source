<?php
require_once 'modules/product/models/ProductCategory.php';
require_once 'Zend/Config.php';
require_once 'Zend/Config/Writer/Array.php';

class front_default_categorySticker extends Nine_Sticker
{
    public function run()
    {
        $objProductCategory = new Models_ProductCategory();
        $category = $objProductCategory->getAllCategoriesParentNull();
        $this->view->category = $category;
        
    }
}