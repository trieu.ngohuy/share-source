<div class="product-container">
    <div class="left-block">
        <a href="{{$item.url}}">
            <img class="img-responsive" alt="product" src="{{$item.main_image}}" />
        </a>
        <div class="quick-view">
            <a title="Add to my wishlist" class="heart" href="#"></a>
            <a title="Add to compare" class="compare" href="#"></a>
            <a title="Quick view" class="search" href="#"></a>
        </div>
        <div class="add-to-cart">
            <a title="Add to Cart" href="#add">Add to Cart</a>
        </div>
    </div>
    <div class="right-block">
        <h5 class="product-name"><a href="{{$item.url}}">{{$item.title}}</a></h5>
        
        <div class="content_price">
            <span class="price product-price">
                {{if $item.price_sale != ''}}
                {{$item.price_sale}} {{$config.currency}}
                {{elseif $item.price != ''}}
                {{$item.price}} {{$config.currency}}
                {{else}}
                {{l}}Liên hệ{{/l}}
                {{/if}}
            </span>

            {{if $item.price_sale != ''}}
            <span class="price old-price">
                {{if $item.price != ''}}
                {{$item.price}} {{$config.currency}}
                {{else}}
                {{l}}Liên hệ{{/l}}
                {{/if}}
            </span>
            {{/if}}

        </div>
    </div>
</div>