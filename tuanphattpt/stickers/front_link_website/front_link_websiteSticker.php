<?php
require_once 'modules/website/models/WebsiteCategory.php';
require_once 'Zend/Config.php';
require_once 'Zend/Config/Writer/Array.php';
class front_link_websiteSticker extends Nine_Sticker
{
    public function run()
    {
        $objWebsite = new Models_WebsiteCategory();
        $websiteLink = $objWebsite->getAllEnabledCategory();

        $this->view->websiteLink = $websiteLink;
    }
}