			<div class="footer">
				<div class="footer-inner">
					<!-- #section:basics/footer -->
					<div class="footer-content">
						<span class="bigger-120" style="margin-top: 20px">
							{{l}}Copyright &copy; 2015 NOCNDEAL - Member of Minh Pham Bros Corp. All rights reserved. {{/l}}
			                <br/>
			                {{l}}The No. 1 "Quality" B2B E-Marketplace in Vietnam.{{/l}}
						</span>
						<br />
						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="callTo://luunguyen_it">
								<i class="ace-icon fa fa-skype light-blue bigger-150"></i>
							</a>

							<a href="https://www.facebook.com/luutn.hv">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

						</span>
					</div>

					<!-- /section:basics/footer -->
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>