<div class="column col-xs-12 col-sm-3" id="left_column">
    <!-- block category -->
    <div class="block left-module">
        <p class="title_block">{{l}}Danh mục{{/l}}</p>
        <div class="block_content">
            <!-- layered -->
            <div class="layered layered-category">
                <div class="layered-content">
                    <ul class="tree-menu">
                        {{foreach from=$childCategory item=leftCatItem}}
                        <li>
                            <span></span><a href="{{$leftCatItem.url}}">{{$leftCatItem.name}}</a>
                            {{if $leftCatItem|@count > 0 }}
                            <ul>
                                {{foreach from=$leftCatItem.subCat key=leftSubCatKey item=leftSubCatItem}}
                                <li><span></span>
                                    <a href="{{$leftSubCatItem.url}}">
                                        {{if $leftSubCatKey == $leftCatItem.subCat|@count}}<span></span>{{/if}}
                                        {{$leftSubCatItem.name}}
                                    </a>
                                </li>
                                {{/foreach}}
                            </ul>
                            {{/if}}
                        </li>
                        {{/foreach}}
                    </ul>
                </div>
            </div>
            <!-- ./layered -->
        </div>
    </div>
    <!-- ./block category  -->
    <!-- SPECIAL -->
    {{if $leftPanelSpecial != ''}}
    <!-- block best sellers -->
    <div class="block left-module">
        <p class="title_block">{{l}}Sản phẩm đặc biệt{{/l}}</p>
        <div class="block_content">
            <div class="owl-carousel owl-best-sell" data-loop="true" data-nav = "false" data-margin = "0" data-autoplayTimeout="1000" data-autoplay="true" data-autoplayHoverPause = "true" data-items="1">
                <ul class="products-block best-sell">
                    {{assign var='count' value='0'}}
                    {{foreach from=$leftPanelSpecial item=leftPanelSpecialItem}}
                    {{if $count <= 3}}
                    <li>
                        <div class="products-block-left">
                            <a href="{{$leftPanelSpecialItem.url}}">
                                <img src="{{$leftPanelSpecialItem.main_image}}" alt="SPECIAL PRODUCTS">
                            </a>
                        </div>
                        <div class="products-block-right">
                            <p class="product-name">
                                <a href="{{$leftPanelSpecialItem.url}}">{{$leftPanelSpecialItem.title}}</a>
                            </p>
                            <p class="product-price">
                                {{if $leftPanelSpecialItem.price_sale !=''}}
                                {{$leftPanelSpecialItem.price}} {{$config.currency}}
                                {{else}}
                                {{$leftPanelSpecialItem.price}} {{$config.currency}}
                                {{/if}}
                            </p>
                            <p class="product-star">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </p>
                        </div>
                    </li>
                    {{/if}}
                    {{assign var='count' value=$count+1}}
                    {{/foreach}}
                </ul>
                <ul class="products-block best-sell">
                    {{assign var='count' value='0'}}
                    {{foreach from=$leftPanelSpecial item=leftPanelSpecialItem}}
                    {{if $count > 3}}
                    <li>
                        <div class="products-block-left">
                            <a href="{{$leftPanelSpecialItem.url}}">
                                <img src="{{$leftPanelSpecialItem.main_image}}" alt="SPECIAL PRODUCTS">
                            </a>
                        </div>
                        <div class="products-block-right">
                            <p class="product-name">
                                <a href="{{$leftPanelSpecialItem.url}}">{{$leftPanelSpecialItem.title}}</a>
                            </p>
                            <p class="product-price">
                                {{if $leftPanelSpecialItem.price_sale !=''}}
                                {{$leftPanelSpecialItem.price}} {{$config.currency}}
                                {{else}}
                                {{$leftPanelSpecialItem.price}} {{$config.currency}}
                                {{/if}}
                            </p>
                            <p class="product-star">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </p>
                        </div>
                    </li>
                    {{/if}}
                    {{assign var='count' value=$count+1}}
                    {{/foreach}}
                </ul>
            </div>
        </div>
    </div>
    <!-- ./block best sellers  -->
    {{/if}}                    
    <!-- ./SPECIAL -->
</div>