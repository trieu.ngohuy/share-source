<div class="column col-xs-12 col-sm-3" id="left_column">
    <!-- block category -->
    <div class="block left-module">
        <p class="title_block">{{l}}Danh mục{{/l}}</p>
        <div class="block_content">
            <!-- layered -->
            <div class="layered layered-category">
                <div class="layered-content">
                    <ul class="tree-menu">
                        {{foreach from=$childCategory item=leftCatItem}}
                        <li>
                            <span></span><a href="{{$leftCatItem.url}}">{{$leftCatItem.name}}</a>
                            {{if $leftCatItem|@count > 0 }}
                            <ul>
                                {{foreach from=$leftCatItem.subCat key=leftSubCatKey item=leftSubCatItem}}
                                <li><span></span>
                                    <a href="{{$leftSubCatItem.url}}">
                                        {{if $leftSubCatKey == $leftCatItem.subCat|@count}}<span></span>{{/if}}
                                        {{$leftSubCatItem.name}}
                                    </a>
                                </li>
                                {{/foreach}}
                            </ul>
                            {{/if}}
                        </li>
                        {{/foreach}}
                    </ul>
                </div>
            </div>
            <!-- ./layered -->
        </div>
    </div>
    <!-- ./block category  -->
    {{if $rootCat.images|@Count > 0}}
    <br />
    <div class="block left-module">
        <div class="block_content">
            <!-- layered -->
            <div class="layered layered-category">
                <div class="clearfix"></div>

                {{assign var='count' value='0'}}
                {{foreach from=$rootCat.images item=catImgItem key=catImgKey}}
                {{if $catImgItem != ""}}
                <div id="CatSlideshow">
                    <div>
                        <img src="{{$catImgItem}}">
                    </div>
                </div>
                {{assign var='count' value=$count+1}}
                {{/if}}
                {{/foreach}}


                {{if $count > 0}}
                <style>
                    #CatSlideshow { 
                        margin: 10px auto; 
                        position: relative; 
                        width: 100%; 
                        height: 100%;
                        padding: 10px; 
                        box-shadow: 0 0 10px rgba(0,0,0,0.4); 
                    }

                    #CatSlideshow > div { 
                        /*position: absolute; */
                        top: 10px; 
                        left: 10px; 
                        right: 10px; 
                        bottom: 10px; 
                    }
                    #CatSlideshow > div img{ 
                        height: auto;
                    }
                </style>
                {{/if}}
            </div>
            <!-- ./layered -->
        </div>
    </div>
    {{sticker name=category_slide}}
    {{/if}}
    <!-- SPECIAL -->
    {{if $leftPanelSpecial != ''}}
    <br />
    <!-- block best sellers -->
    <div class="block left-module left-special-product">
        <p class="title_block">{{l}}Sản phẩm đặc biệt{{/l}}</p>
        <div class="block_content left-panel-special">
            {{assign var='count' value='1'}}
            {{foreach from=$leftPanelSpecial item=leftPanelSpecialItem}}
            {{if $count <= 10}}
            <div class="product-container">
                <div class="left-block">
                    <a href="{{$leftPanelSpecialItem.url}}">
                        <img class="img-responsive" alt="product" src="{{$leftPanelSpecialItem.main_image}}">
                    </a>
                </div>
                <div class="right-block">
                    <br>
                    <h5 class="product-name">
                        <a href="{{$leftPanelSpecialItem.url}}">
                            {{$leftPanelSpecialItem.title}}
                        </a>
                    </h5>

                    <div class="content_price">
                        <span class="price product-price">
                            {{if $leftPanelSpecialItem.price_sale != ''}}
                            {{$leftPanelSpecialItem.price_sale}}{{$config.currency}}
                            {{else}}
                            {{$leftPanelSpecialItem.price}}{{$config.currency}}
                            {{/if}}
                        </span>
                        {{if $leftPanelSpecialItem.price_sale != ''}}
                        <span class="price old-price">
                            {{$leftPanelSpecialItem.price}}{{$config.currency}}
                        </span>
                        {{/if}}

                    </div>
                </div>
            </div>
            <hr>
            {{/if}}
            {{assign var='count' value=$count+1}}
            {{/foreach}}

        </div>
    </div>
    <!-- ./block best sellers  -->
    {{/if}}                    
    <!-- ./SPECIAL -->
    
</div>