<?php
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/product/models/Product.php';
require_once 'modules/contact/models/Contact.php';
require_once 'modules/message/models/MessageCategory.php';
require_once 'modules/buying/models/Buying.php';
require_once 'modules/buying/models/BuyingCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'Zend/Config.php';
require_once 'Zend/Config/Writer/Array.php';
require_once 'modules/content/models/Content.php';
require_once 'modules/content/models/ContentCategory.php';
require_once 'modules/bank/models/Bank.php';
require_once 'modules/bank/models/BankCategory.php';

class front_default_headerSticker extends Nine_Sticker
{
	public function run()
	{
		$objLang  = new Models_Lang();
		$allLangs = $objLang->getByColumns(array('enabled = ?' => 1), array('sorting ASC'))->toArray();
        $this->view->allLangs = $allLangs;
        $this->view->langCode = Nine_Registry::getLangCode();
		$objProductCategory = new Models_ProductCategory();
		$allCategory = $objProductCategory->getAllCategoriesParentNull();
		$objLang  = new Models_Lang();
		$objContact = new Models_Contact();
		$objMessage = new Models_MessageCategory();
		
		foreach($allCategory as &$item){
			$item['cate'] = $objProductCategory->getAllCategoriesOnparentkey($item['product_category_gid']);
			foreach($item['cate'] as &$item1){
				$item1['cate'] = $objProductCategory->getAllCategoriesOnparentkey($item1['product_category_gid'],3);
			}
			unset($item1);
		}
		unset($item);
		if($this->auth->isLogin()) {
			$user = Nine_Registry::getLoggedInUser();

			if($user['group_id'] == 3) {
				$condition = array(
					'enabled = ?' => 0,
					'parent_id = ?' => 0,
					'user_to_id = ?' => Nine_Registry::getLoggedInUserId()
				);
				$allNewMessage = $objMessage->getByColumns($condition)->toArray();
				if($allNewMessage != null) {
					$hasNewMessage = true;
				}
				$this->view->hasNewMessage = $hasNewMessage;
				$this->view->countNewMessage = count($allNewMessage);
			}


			$condition = array(
				'enabled = ?' => 1,
				'parent_id = ?' => 0,
				'user_id = ?' => Nine_Registry::getLoggedInUserId()
			);
			$objCategory     = new Models_MessageCategory();
			$condition = array();
			$condition['user_from_id'] = Nine_Registry::getLoggedInUserId();
	        $condition['parent_id'] = 0;
	        $condition['user_view'] = 0;
	        $condition['type'] = 1;
	        $allCategories  = $objCategory->setAllLanguages(true)->getAllCategories($condition);
	        
	        $count = count($allCategories);
	        $condition = array();
			$condition['user_to_id'] = Nine_Registry::getLoggedInUserId();
	        $condition['parent_id'] = 0;
	        $condition['admin_view'] = 0;
	        $condition['type'] = 1;
	        $allCategories  = $objCategory->setAllLanguages(true)->getAllCategories($condition);
	        $count += count($allCategories);
	        
	        $this->view->countNewContactEstore = $count;
	        
	        $condition = array();
			$condition['user_from_id'] = Nine_Registry::getLoggedInUserId();
	        $condition['parent_id'] = 0;
	        $condition['user_view'] = 0;
	        $condition['type'] = 2;
	        $allCategories  = $objCategory->setAllLanguages(true)->getAllCategories($condition);
	        
	        $count = count($allCategories);
	        
	        $condition = array();
			$condition['user_to_id'] = Nine_Registry::getLoggedInUserId();
	        $condition['parent_id'] = 0;
	        $condition['admin_view'] = 0;
	        $condition['type'] = 2;
	        $allCategories  = $objCategory->setAllLanguages(true)->getAllCategories($condition);
	        $count += count($allCategories);
	        
	        $this->view->countNewContactEstoreHoiHang = $count;
	        $condition = array();
			$condition['user_from_id'] = Nine_Registry::getLoggedInUserId();
	        $condition['parent_id'] = 0;
	        $condition['user_view'] = 0;
	        $condition['type'] = 3;
	        $allCategories  = $objCategory->setAllLanguages(true)->getAllCategories($condition);
	        
	        $count = count($allCategories);
	        
	        $condition = array();
			$condition['user_to_id'] = Nine_Registry::getLoggedInUserId();
	        $condition['parent_id'] = 0;
	        $condition['admin_view'] = 0;
	        $condition['type'] = 3;
	        $allCategories  = $objCategory->setAllLanguages(true)->getAllCategories($condition);
	        $count += count($allCategories);
	        
	        $this->view->countNewContactEstoreKhuyenMai = $count;
	        
	        $objCategory     = new Models_BuyingCategory();
	        $condition = array();
	        $condition['user_id'] = Nine_Registry::getLoggedInUserId();
	        $condition['view'] = 1;
	        $allCategories  = $objCategory->getAllEnabledCategory($condition);
	        $this->view->countNewBuying = count($allCategories);
	        
			$this->view->userLogin = $user;
		}

		$this->view->allCategory = $allCategory;
		$this->view->isLogin = $this->auth->isLogin();
		$objContent = new Models_Content ();
		
		$banle = $objContent->getAllEnabledContentByCategory(37);
		foreach ( $banle as &$content ) {
			$content ['title'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $content ['title'] ) ), 30 );
			$content ['intro_text'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $content ['full_text'] ) ), 100 );
			if($content ['alias'] == ""){
				$content ['alias'] = $objContent->convert_vi_to_en($content ['alias']);
				$content ['alias'] = str_replace(" ", "-", str_replace("&*#39;","",trim($content ['alias'])));
			}
			$content ['url'] = Nine_Route::_ ( "content/index/detail/gid/{$content['content_gid']}", array ('alias' => $content ['alias'] ) );
			$tmp = explode ( '||', $content ['images'] );
			$content ['main_image'] = Nine_Function::getThumbImage ( @$tmp [0], 96, 96 , false , true );
		}
		unset ( $content );
		$this->view->banle = $banle;
		
		$objCategoryBank     = new Models_BankCategory();
		$allCategories  = $objCategoryBank->getallCategories();
		$this->view->allCategoriesBank = $allCategories;
		
		$objContentcat = new Models_ContentCategory();
		$mnbv = $objContentcat->getAllCategoriesOnparentkey(39);
		try {
			foreach ($mnbv as &$item){
				$item['parent_cate'] = $objContentcat->getAllCategoriesOnparentkey($item['content_category_gid']);
				if($item['alias'] == ""){
					$item['alias'] = $objContent->convert_vi_to_en($item['name']);
					$item['alias'] = str_replace(" ", "-", str_replace("&*#39;","",trim($item['alias'])));
				}
				$item['url'] = Nine_Route::_("content/index/index/cid/{$item['content_category_gid']}",array ('alias' => $item['alias'] ) );
				foreach ($item['parent_cate'] as &$item_){
					$item_['parent_cate'] = $objContentcat->getAllCategoriesOnparentkey($item_['content_category_gid']);
					if($item_['alias'] == ""){
						$item_['alias'] = $objContent->convert_vi_to_en($item_['name']);
						$item_['alias'] = str_replace(" ", "-", str_replace("&*#39;","",trim($item_['alias'])));
					}
					$item_['url'] = Nine_Route::_("content/index/index/cid/{$item_['content_category_gid']}",array ('alias' => $item_['alias'] ) );
					
					foreach ($item_['parent_cate'] as &$item__){
						if($item__['alias'] == ""){
							$item__['alias'] = $objContent->convert_vi_to_en($item__['name']);
							$item__['alias'] = str_replace(" ", "-", str_replace("&*#39;","",trim($item__['alias'])));
						}
						$item__['url'] = Nine_Route::_("content/index/index/cid/{$item__['content_category_gid']}",array ('alias' => $item__['alias'] ) );
					}
					unset($item__);
				}
				unset($item_);
			}
		} catch (Exception $e) {
			
		}
		unset($item);
		$this->view->mnbv = $mnbv;
	}
}