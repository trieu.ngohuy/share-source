<div class="product-container">
    <div class="left-block">
        <a href="{{$productItem.url}}">
            <img class="img-responsive" alt="{{$productItem.title}}" src="{{$productItem.main_image}}" />
        </a>
        <div class="quick-view">
            <a title="Quick view" class="search" href="#"></a>
        </div>
        <div class="add-to-cart">
            <a title="Add to Cart" href="#add">{{l}}Thêm vào giỏ hàng{{/l}}</a>
        </div>
    </div>
    <div class="right-block">
        <h5 class="product-name"><a href="{{$productItem.url}}">{{$productItem.title}}</a></h5>
        
        <div class="content_price">
            <span class="price product-price">
                {{if $productItem.price_sale != ''}}
                {{$productItem.price_sale}}{{$config.currency}}
                {{else}}
                {{$productItem.price}}{{$config.currency}}
                {{/if}}
            </span>
            {{if $productItem.price_sale != ''}}
            <span class="price old-price">{{$productItem.price}} {{$config.currency}}</span>
            {{/if}}
        </div>
        <div class="info-orther">
            <div class="product-desc">
                {{$productItem.intro_text}}
            </div>
        </div>
    </div>
</div>