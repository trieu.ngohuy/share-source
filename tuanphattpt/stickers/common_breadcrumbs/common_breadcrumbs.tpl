<div class="breadcrumb clearfix">
    <a class="home" href="{{$breadCrumbs.home.url}}" title="{{$breadCrumbs.home.name}}">{{$breadCrumbs.home.name}}</a>
    <span class="navigation-pipe">&nbsp;</span>
    {{foreach from=$breadCrumbs.data item=breadCrumbsItem}}
    <a class="home" href="{{$breadCrumbsItem.url}}" title="{{$breadCrumbsItem.name}}">{{$breadCrumbsItem.name}}</a>
    <span class="navigation-pipe">&nbsp;</span>
    {{/foreach}}
    
    <span class="navigation_page">{{$breadCrumbs.detail.name}}</span>
</div>