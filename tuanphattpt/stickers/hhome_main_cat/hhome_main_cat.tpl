<div class="page-top">
    <div class="container">
        <!-- block-popular-cat -->
        <div class="row">
            {{foreach from=$productCatHomeDisplay item=topCatItem}}
            <div class="col-sm-4">
                <div class="block-popular-cat">
                    <div class="parent-categories">{{$topCatItem.name}}</div>
                    <div class="block-popular-inner">
                        <div class="image banner-boder-zoom2">
                            <a href="{{$topCatItem.url}}">
                                <img src="{{$topCatItem.main_image}}" alt="{{$topCatItem.name}}">
                            </a>
                        </div>
                        <div class="sub-categories">
                            <ul>
                                {{foreach from=$topCatItem.subCat item=subCatItem}}
                                <li><a href="{{$subCatItem.url}}">{{$subCatItem.name}}</a></li>
                                    {{/foreach}}
                            </ul>
                            <a href="{{$topCatItem.url}}" class="more">{{l}}Xem...{{/l}}</a>
                        </div>
                    </div>
                </div>
            </div>
            {{/foreach}}
        </div>
        <!-- ./block-popular-cat -->
        <!-- Baner bottom -->
        <div class="row banner-bottom">
            {{foreach from=$listAds key=adsKey item=adsItem}}
            <div class="col-sm-6 item-{{if $adsKey == 0}}left{{else}}right{{/if}}">
                <div class="banner-boder-zoom2">
                    <a href="{{$adsItem.url}}"">
                        <img alt="{{$adsItem.title}}" class="img-responsive" src="{{$adsItem.main_image}}" />
                    </a>
                </div>
            </div>
            {{/foreach}}
        </div>
        <!-- end banner bottom -->
    </div>
</div>