<?php

class hhome_main_catSticker extends Nine_Sticker {

    public function run() {
        //Get top list category
        $topCat = Nine_Query::getListProductCategory(array(
                    'is_ads' => 1
        ));
        //Add sub cate
        foreach ($topCat as &$value) {
            //Get list sub cats
            $value['subCat'] = Nine_Query::getListProductCategory(array(
                        'parent_id' => $value['product_category_gid']
                            ), null, 3, null);
            //cut cat name
            foreach ($value['subCat'] as &$content) {
                $content['name'] = mb_substr($content['name'], 0, 14);
            }
            unset($content);
        }
        unset($value);
        $this->view->topCat = $topCat;
        /*
         * Get list ads
         */
        $listAds = Nine_Query::getListContentsByCatAlias('ads', 'ads', 2, null);
        $this->view->listAds = $listAds;
    }

}
