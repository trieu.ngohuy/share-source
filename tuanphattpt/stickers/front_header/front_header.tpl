<header class="banner">
    <div class="contact-bar d-none d-md-block">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="social-bar">
                        <a href="{{$config.facebook}}" target="_blank" rel="nofollow"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                        <a href="{{$config.google}}" target="_blank" rel="nofollow"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
                        <a href="{{$config.youtube}}" target="_blank" rel="nofollow"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="float-r" style="font-size: 14px;">
                        <span class="btn-design">
                            <a href="{{$design}}">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{l}}Thiết kế chiếu sáng{{/l}}
                            </a>
                        </span>
                        <span class="btn-calculating">
                            <a href="{{$calculation}}">
                                <i class="fa fa-calculator" aria-hidden="true"></i>  {{l}}Tính toán & chọn đèn nhà xưởng{{/l}}
                            </a>
                        </span>
                        <span class="livechat">
                            <a href="javascript:void(Tawk_API.toggle())">
                                <i class="fa fa-user" aria-hidden="true"></i> Live Chat
                            </a>
                        </span> 
                        <span class="email">
                            <a href="mailto:{{$config.Email}}">
                                <i class="fa fa-envelope" aria-hidden="true"></i> {{$config.Email}}
                            </a>
                        </span>
                        <span class="hotline"><a class="toggleCTA"><i class="fa fa-phone-square"
                                                                      aria-hidden="true"></i> {{$config.hotline}}</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main_nav">
        <div class="container">
            <nav class="navbar nav-primary navbar-expand-md navbar-light">
                <a class="navbar-brand" href="{{$homepage}}"
                   style="background-image: url({{$config.Logo}});">
                    <h1>{{$config.WebsiteName}}</h1>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs4navbar"
                        aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation"><span
                        class="navbar-toggler-icon"></span></button>
                <div id="bs4navbar"
                     class="collapse navbar-collapse justify-content-end">
                    <ul id="menu-menu-1" class="navbar-nav">

                        {{foreach from=$arrMenus item=item}}
                        <li id="menu-item-1740" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1740 nav-item 
                            {{if $item.detail|@count > 0}}dropdown{{/if}}">
                            {{if $item.detail|@count > 0}}
                            <a href="{{$item.url}}" class="nav-link dropdown-toggle"
                               data-toggle="dropdown">{{$item.name}}</a>
                            <div class="dropdown-menu">
                                {{foreach from=$item.detail item=item2}}
                                <a href="{{$item2.url}}" class="dropdown-item">{{$item2.name}}</a>
                                {{/foreach}}
                            </div>
                            {{else}}
                            <a href="{{$item.url}}" class="nav-link">{{$item.name}}</a>
                            {{/if}}

                        </li>
                        {{/foreach}}

                    </ul>
                </div>
                <aside data-wg-notranslate="" class="wg-drop country-selector closed" onclick="openClose(this);" style="z-index: 9;">
                    {{foreach from=$lang.langs item=item}}
                    {{if $item.lang_code === $lang.active}}
                    <div data-wg-notranslate="" class="wgcurrent wg-li wg-flags {{$item.lang_code}}"><a
                            href="javascript:void(0);">{{$item.alias}}</a>
                    </div>
                    {{/if}}
                    {{/foreach}}

                    <ul>
                        {{foreach from=$lang.langs item=item}}
                        {{if $item.lang_code !== $lang.active}}
                        <li class="wg-li wg-flags {{$item.lang_code}}">
                            <a data-wg-notranslate="" href="{{$url.path}}{{$item.lang_code}}">{{$item.alias}}</a>
                        </li>
                        {{/if}}
                        {{/foreach}}

                    </ul>
                </aside>
            </nav>
        </div>
    </div>
</header>
<style type="text/css">
    #menu-menu-1 a{
        font-weight: 600;
        font-size: 15px;
    }
</style>