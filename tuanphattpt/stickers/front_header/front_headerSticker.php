<?php

class front_headerSticker extends Nine_Sticker {

    /**
     * @throws Exception
     */
    public function run() {
        //Get menu
        $arrMenus = Nine_Read::GetListData('Models_Menu', array(), array('sorting ASC'));

        //Create url
        foreach ($arrMenus as &$value) {
            if ($value['link'] !== '') {
                $value['url'] = $value['link'];
            } else {
                
                $value['url'] = Nine_Route::_($value['modules'] . '/' . $value['controller'] . '/' . $value['function'], array(
                            'alias' => $value['alias']
                ));
//                if ($value['function'] === 'category-detail') {
//                    $value['url'] = Nine_Route::_($value['modules'] . '/' . $value['controller'] . '/' . $value['function'], array(
//                                'alias' => $value['alias']
//                    ));
//                }
            }
        }
        unset($value);
        
        $arrMenus = Nine_Common::FomatArrayAsTree($arrMenus, 0, null, 'menu_gid', true);
        $this->view->arrMenus = $arrMenus;

        //Get calculation and design url
        $this->view->calculation = Nine_Route::_('design/index/calculation');
        $this->view->design = Nine_Route::_('design/index/index');

        //Get config
        $this->view->config = Nine_Common::getConfig($this->view);

        //Get url
        $this->view->url = Nine_Route::url();

        //Get list language
        $lang = Nine_Language::getAllLanguages();
        $this->view->lang = array(
            'langs' => Nine_Language::getAllLanguages(),
            'active' => Nine_Language::getCurrentLangCode()
        );

        //Home page url
        $this->view->homepage = Nine_Route::_("default/index/index");
        $url = Nine_Route::url();
        $this->view->homepage = $url['path'];
    }

}
