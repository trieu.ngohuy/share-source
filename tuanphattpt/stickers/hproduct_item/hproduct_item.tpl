<div class="right-block">
    <h5 class="product-name"><a href="{{$productItem.url}}">{{$productItem.title}}</a></h5>
    <div class="content_price">
        <span class="price product-price">
            {{if $productItem.price_sale !=''}}
                {{$productItem.price_sale}} {{$config.currency}}
            {{elseif $productItem.price != ''}}
                {{$productItem.price}} {{$config.currency}}
            {{else}}
            {{l}}Liên hệ{{/l}}
            {{/if}}
        </span>
    </div>
</div>
<div class="left-block">
    <a href="{{$productItem.url}}">
        <img class="img-responsive" alt="product" src="{{$productItem.main_image}}" />
    </a>
    <!--div class="price-percent-reduction2">-30% OFF</div-->
    <div class="quick-view">
        <a title="Quick view" class="search" href="{{$productItem.url}}"></a>
    </div>
    <div class="add-to-cart">
        <a title="Add to Cart" href="{{$productItem.url}}">{{l}}Xem thêm{{/l}}</a>
    </div>
</div>