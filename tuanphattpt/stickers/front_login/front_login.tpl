<div class="modal" id="loginPopup">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closeLogin" data-dismiss="modal">x</button>
                <h3>Login to HOMARTTPHCM</h3>
            </div>
            <div class="modal-body">
                <form role="form" id="form-login" method="POST" action="{{$BASE_URL}}access/login">
                    <div class="form-group">
                        <label for="inputEmail1">{{l}}Username{{/l}}</label>
                        <input type="text" class="form-control" name="username" id="inputEmail1" placeholder="{{l}}Username{{/l}}">
                    </div>
                    <div class="form-group">
                        <label for="inputPassword">{{l}}Password{{/l}}</label>
                        <input type="password" class="form-control" name="password" id="inputPassword" placeholder="{{l}}Password{{/l}}">
                    </div>
                    <button id="checkSubmit" type="submit" class="btn btn-default">{{l}}Submit{{/l}}</button>
                </form>
            </div>
            <div class="modal-footer">
                New To C&D.com?
                <a href="{{$BASE_URL}}user/register" class="btn btn-primary">Register</a>
            </div>
        </div>
    </div>
</div>