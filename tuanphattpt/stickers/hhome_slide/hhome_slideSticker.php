<?php

class hhome_slideSticker extends Nine_Sticker {

    public function run() {
        /*
         * Get list banner
         */
        $this->view->banner = Nine_Query::getListContentsByCatAlias('banner', 'banner');
        
        /*
         * Get list subbanner
         */
        $this->view->subBanner = Nine_Query::getListContentsByCatAlias('sub-banner', 'sub-banner');
        
        //Get list "Cam ket"
        if (Nine_Language::getCurrentLangCode() == 'vi') {
            $camketCat = Nine_Query::getListContentCategory(array(
                        'alias' => 'cam-ket'
            ));
        } else {
            $camketCat = Nine_Query::getListContentCategory(array(
                        'alias' => 'commitment'
            ));
        }

        if (count($camketCat) > 0) {
            $camketCat = @reset($camketCat);
            $listContent = Nine_Query::getListContents(array(
                        'user_id' => 1,
                        'content_category_gid' => $camketCat['content_category_gid']
            ));
            //Get main image
            $url = Nine_Route::url();
            foreach ($listContent as &$value) {
                //$tmp = explode ( '||', $value ['images'] );
                //Get first image
                //$value ['main_image'] = $url ['path'] . $value [0];
                if (count($value['images']) >= 2) {
                    $value ['second_image'] = $url ['path'] . $value['images'] [1];
                } else {
                    $value ['second_image'] = $url ['path'] . $value['images'] [0];
                }
            }
            unset($value);
            $this->view->listCamket = $listContent;
        } else {
            $this->view->listCamket = array();
        }
    }

}
