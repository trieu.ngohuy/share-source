<footer id="footer">
    <div class="container">
        <!-- introduce-box -->
        <div id="introduce-box" class="row">
            <div class="col-md-3">
                <div id="address-box">
                    <a href="{{$BASE_URL}}">
                        <img src="{{$url.path}}{{$config.FooterLogo}}" alt="{{$config.WebsiteName}}" />
                    </a>
                    <div id="address-list">
                        <h3 class="pd-b-10">{{$config.WebsiteName}}</h3>
                        <div class="tit-name">Address:</div>
                        <div class="tit-contain">{{$config.Add}}</div>
                        <div class="tit-name">Phone:</div>
                        <div class="tit-contain">{{$config.hotline}}</div>
                        <div class="tit-name">Email:</div>
                        <div class="tit-contain">{{$config.Email}}</div>
                    </div>
                </div> 
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="introduce-title">{{$footerMenuCompany.name}}</div>
                        <ul id="introduce-company"  class="introduce-list">
                            {{foreach from=$footerMenuCompany.detail item=companyContensItem}}
                            <li><a href="{{$companyContensItem.url}}">{{$companyContensItem.title}}</a></li>
                            {{/foreach}}
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="introduce-title">{{$footerMenuMyAccount.name}}</div>
                        <ul id = "introduce-Account" class="introduce-list">
                            {{foreach from=$footerMenuMyAccount.detail item=accountContensItem}}
                            <li><a href="{{$accountContensItem.url}}">{{$accountContensItem.title}}</a></li>
                            {{/foreach}}
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="introduce-title">{{$footerMenuSupport.name}}</div>
                        <ul id = "introduce-support"  class="introduce-list">
                            {{foreach from=$footerMenuSupport.detail item=supportContensItem}}
                            <li><a href="{{$supportContensItem.url}}">{{$supportContensItem.title}}</a></li>
                            {{/foreach}}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div id="contact-box">
                    <div class="introduce-title">{{l}}Theo dõi{{/l}}</div>
                    <div class="input-group" id="mail-box">
                        <input type="text" placeholder="Your Email Address"/>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">{{l}}OK{{/l}}</button>
                        </span>
                    </div><!-- /input-group -->
                    <div class="introduce-title">{{l}}Mạng xã hội{{/l}}</div>
                    <div class="social-link">
                        <a href="{{$config.facebook}}"><i class="fa fa-facebook"></i></a>
                        <a href="{{$config.youtube}}"><i class="fa fa-youtube"></i></a>
                        <a href="{{$config.skype}}"><i class="fa fa-skype"></i></a>
                        <a href="{{$config.yahoo}}"><i class="fa fa-yahoo"></i></a>
                        <a href="{{$config.google}}"><i class="fa fa-google-plus"></i></a>
                    </div>
                </div>

            </div>
        </div><!-- /#introduce-box -->

        <!-- #trademark-box -->
        <div id="trademark-box" class="row">
            <div class="col-sm-12">
                <ul id="trademark-list">
                    <li id="payment-methods">{{l}}Phương thức thanh toán{{/l}}</li>
                    {{foreach from=$listPayment item=paymentItem}}
                    <li>
                        <a href="{{$paymentItem.url}}">
                            <img src="{{$paymentItem.main_image}}"  alt="{{$paymentItem.title}}"/>
                        </a>
                    </li>
                    {{/foreach}}
                    
                </ul> 
            </div>
        </div> 
        <!-- /#trademark-box -->

        <!-- #trademark-text-box -->
        <div id="trademark-text-box" class="row">
            {{foreach from=$allCat item=catItem}}
            <div class="col-sm-12">
                <ul id="trademark-search-list" class="trademark-list">
                    <li class="trademark-text-tit">{{$catItem.name}}</li>
                    {{foreach from=$catItem.subCat item=detailItem}}
                    <li><a href="{{$detailItem.url}}" >{{$detailItem.name}}</a></li>
                    {{/foreach}}
                </ul>
            </div>
            {{/foreach}}
        </div><!-- /#trademark-text-box -->
        <div id="footer-menu-box">
            <p class="text-center">{{$config.status_footer}}</p>
        </div><!-- /#footer-menu-box -->
    </div> 
</footer>
