<?php
/*
 * Common footer sticker
 */
class common_footerSticker extends Nine_Sticker {

    /*
     * Run
     */
    public function run() {
        try{
            /*
             * Get config
             */
            Nine_Common::getConfig($this->view);
            /*
             * Get url 
             */
            $this->view->url = Nine_Route::url();
            /*
             * Get list menu
             */
            $this->GetFooterData();
            /*
             * Get payment
             */
            $this->view->listPayment = Nine_Read::GetListData('Models_PaymentCategory', array(), array());
           
            //print_r(Nine_Read::GetListData('Models_PaymentCategory', array(), array()));
        } catch (Exception $e) {
            
        }        
    }
    /*
     * Get list menu
     */
    public function GetFooterData() {
        /*
         * Get company
         */
        $this->view->footerMenuCompany = $this->GetCatAndListContents('company', 'cong-ty', 5);
        /*
         * Get my account
         */
        $this->view->footerMenuMyAccount = $this->GetCatAndListContents('my-account', 'tai-khoan', 5);
        /*
         * Get support
         */
        $this->view->footerMenuSupport = $this->GetCatAndListContents('support', 'ho-tro', 5);
        /*
         * Get all cát
         */
        $allCat = Nine_Read::GetListData("Models_ProductCategory", array(
            'parent_id' => 0
        ), array());
        /*
         * Add sub cats
         */
        foreach ($allCat as &$value) {
            $value['subCat'] = Nine_Read::GetListData("Models_ProductCategory", array(
                'parent_id' => $value['product_category_gid']
            ), array());
        }
        unset($value);
        $this->view->allCat = $allCat;
    }
    /*
     * Get cat and list cat contents
     */
    public function GetCatAndListContents($en_alias, $vi_alias, $count) {
        /*
         * Get cat detail
         */        
        $cat = Nine_Query::getListContentCategory(array(
            'alias' => Nine_Language::getCurrentLangCode() == 'vi' ? $vi_alias : $en_alias
        ));
        if(count($cat) >0){
            $cat = @reset($cat);
            /*
             * Get list contents
             */
            $cat['detail'] = Nine_Query::getListContents(array(
                'content_category_gid' => $cat['content_category_gid']
            ), array(), $count, null);
            return $cat;
        }else{
            return array();
        }
    }

}
