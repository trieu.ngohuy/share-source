<div class="clearfix"></div>

{{assign var='count' value='0'}}
{{foreach from=$CatSlide item=catImgItem key=catImgKey}}
{{if $catImgItem != ""}}
<div id="CatSlideshow">
    <div>
        <img src="{{$catImgItem}}">
    </div>
</div>
{{assign var='count' value=$count+1}}
{{/if}}
{{/foreach}}


{{if $count > 0}}
<style>
    #CatSlideshow { 
        margin: 10px auto; 
        position: relative; 
        width: 100%; 
        height: 100%;
        padding: 10px; 
        margin-left: 18px;
        box-shadow: 0 0 10px rgba(0,0,0,0.4); 
    }

    #CatSlideshow > div { 
        /*position: absolute; */
        top: 10px; 
        left: 10px; 
        right: 10px; 
        bottom: 10px; 
    }
    #CatSlideshow > div img{ 
        height: auto;
    }
</style>
{{/if}}