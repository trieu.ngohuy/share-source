<div class="footer">
    <div class="footer-inner">
        <!-- #section:basics/footer -->
        <div class="footer-content">
            <span class="bigger-120" style="margin-top: 20px">
                {{l}}Copyright &copy; 2017 HOMARTTPHCM. All rights reserved. {{/l}}
                <br/>
                {{l}}HOMART TPHCM{{/l}}
            </span>
        </div>

        <!-- /section:basics/footer -->
    </div>
</div>

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>