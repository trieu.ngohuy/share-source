<section id="slider">
	<div class="slider-grid">
		{{foreach from=$allBanner item=item key=key}}
			{{if $key%2 !=0}}
                <div class="col-md-12 col-sm-12 item">
                      <img src="{{$BASE_URL}}{{$item.main_image}}" alt="gallery">
                    <div class="text right">
                    	<h2 class="wow fadeInRight" data-wow-duration="1s" data-wow-delay="1s">{{$item.title}}</span></h2>
                    </div>
                </div>
        	{{else}}
        		<div class="col-md-12 col-sm-12 item">
                      <img src="{{$BASE_URL}}{{$item.main_image}}" alt="gallery">
                    <div class="text left">
                    	<h2 class="wow fadeInRight" data-wow-duration="1s" data-wow-delay="1s">{{$item.title}}</span></h2>
                    </div>
                </div>
        	{{/if}}
    	{{/foreach}}   
	</div>
				
</section>