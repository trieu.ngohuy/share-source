<section id="contact">
		<div class="container">
    	<div class="row">
                <h2 class="heading wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">{{l}}Contact US{{/l}}</h2>
                <div class="separator_wrapper wow bounceIn" data-wow-duration="0.7s" data-wow-delay="0.7s">
                        <div class="separator_first_circle">
                        <img src="{{$LAYOUT_HELPER_URL}}front/assets/images/green-flower.png" alt="green flower">
                        </div>
                    </div>
                <p class="helping-text wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="0.8s">{{l}}Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer egestas, lorem ut suscipit facilisis, nisi neque commodo quam, non pretium dui arcu varius neque.{{/l}}</p>
                <div class="col-md-12">
                	<form class="form-horizontal contact-form" id="contact-form" method="post">
                    <div class="row">
                    	<div class="col-md-6 wow fadeInLeft"  data-wow-duration="0.9s" data-wow-delay="0.9s">
                        	<div class="form-group">
                                <div class="col-sm-12">
                                  <input type="text" class="form-control" name="name" placeholder="{{l}}Your name{{/l}}">
                                </div>
                              </div>
  							<div class="form-group">
                                <div class="col-sm-12">
                                  <input type="email" class="form-control" name="email" placeholder="{{l}}Your email address{{/l}}">
                                </div>
                              </div>
  							<div class="form-group">
                                <div class="col-sm-12">
                                  <input type="text" class="form-control" placeholder="{{l}}Your phone number{{/l}}">
                                </div>
                              </div>
                        </div>
                    	<div class="col-md-6 wow fadeInRight" data-wow-duration="0.9s" data-wow-delay="0.9s">
                        	<div class="form-group">
                            <div class="col-sm-12">
                              <textarea class="form-control" placeholder="{{l}}Your message{{/l}}" name="message" rows="3"></textarea>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 text-center">
                          <button type="submit" class="btn main-btn hvr-sweep-to-right"><i class="fa fa-angle-right"></i> Send Message</button>
                        </div>
                      </div>
                    </form>
                </div>
       </div>
       </div>
<div class="locations">
	<div class="container">
    	<div class="row">
    	
        	<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
            	<h4>{{l}}Los Angeles{{/l}}</h4>
                <ul class="list-unstyled">
                  <li>{{l}}Angel Street 146, B16{{/l}}</li>
                  <li>{{l}}(058) 569 3668{{/l}}</li>
                  <li><a href="{{l}}href1{{/l}}">{{l}}la@ygritte.com1{{/l}}</a></li>
                </ul>
            </div>
            
            <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
            	<h4>{{l}}Los Angeles{{/l}}</h4>
                <ul class="list-unstyled">
                  <li>{{l}}Angel Street 146, B16{{/l}}</li>
                  <li>{{l}}(058) 569 3668{{/l}}</li>
                  <li><a href="{{l}}href2{{/l}}">{{l}}la@ygritte.com1{{/l}}</a></li>
                </ul>
            </div>
            
            <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
            	<h4>{{l}}Los Angeles{{/l}}</h4>
                <ul class="list-unstyled">
                  <li>{{l}}Angel Street 146, B16{{/l}}</li>
                  <li>{{l}}(058) 569 3668{{/l}}</li>
                  <li><a href="{{l}}href3{{/l}}">{{l}}la@ygritte.com1{{/l}}</a></li>
                </ul>
            </div>
            
        </div>
		</div>
        </div>
        
       </section>  
<section class="map">
 <div id="contact_map"></div>
</section>      