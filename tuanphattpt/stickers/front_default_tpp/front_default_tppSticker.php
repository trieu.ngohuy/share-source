<?php
require_once 'Zend/Config.php';
require_once 'Zend/Config/Writer/Array.php';
require_once 'modules/country/models/CountryCategory.php';

class front_default_tppSticker extends Nine_Sticker
{
    public function run()
    {
        $objCountry = new Models_CountryCategory();

        $country = $objCountry->getAllCategoriesParentNull();

        $this->view->country = $country;
    }
}