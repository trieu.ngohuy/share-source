<div id="trademark-box" class=" linkbanner">
    <div class="col-sm-12" style="padding: 0;">
        <ul id="trademark-list" class="bg_while m0">
            <li id="most-product">{{l}}TPP Most Interested Products{{/l}}</li>
            {{foreach from=$country item=item}}
            <li  class="col-xs-4 col-sm-4 col-md-4">
                <a href="{{$BASE_URL}}tpp/{{$item.alias}}"><img src="{{$BASE_URL}}{{$item.images}}"  alt="ups"  width="24px"/> <span>{{$item.code}}</span></a>
            </li>
            {{/foreach}}
        </ul>
    </div>
</div>
