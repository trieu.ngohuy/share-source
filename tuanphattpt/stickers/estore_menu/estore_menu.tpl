


<!-- h-sidebar	-->
<div id="sidebar" class="sidebar responsive h-sidebar sidebar-fixed">

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            {{foreach from=$allLangs item=item}}
            <a href="{{$BASE_URL}}{{$item.lang_code}}/estore/{{$alias}}"><img src="{{$BASE_URL}}{{$item.lang_image}}" alt=""></a>
            {{/foreach}}

        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div>

    <ul class="nav nav-list">

        <li class="{{if $menu[0] == 'home'}}active open{{/if}} hover">
            <a href="{{$APP_BASE_URL}}estore/{{$alias}}">
                <i class="menu-icon fa fa-dashboard"></i>
                <span class="menu-text"> {{l}}Control Panel{{/l}} </span>
            </a>

            <b class="arrow"></b>
        </li>


        <li class="{{if $menu[0] == 'profile'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-fax"></i>
                <span class="menu-text"> {{l}}Profile{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'edit-profile'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/edit-info/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Edit Info{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'change-password'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/change-password/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Change Password{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

            </ul>
        </li>


<!--        <li class="{{if $menu[0] == 'buying'}}active open{{/if}} hover">-->
<!--            <a href="#" class="dropdown-toggle">-->
<!--                <i class="menu-icon fa fa-fax"></i>-->
<!--                <span class="menu-text"> {{l}}Buying Request{{/l}} </span>-->
<!---->
<!--                <b class="arrow fa fa-angle-down"></b>-->
<!--            </a>-->
<!---->
<!--            <b class="arrow"></b>-->
<!---->
<!--            <ul class="submenu">-->
<!--                <li class="{{if $menu[1] == 'new-category-buying'}}active{{/if}} hover">-->
<!--                    <a href="{{$APP_BASE_URL}}estore/new-buying-request/{{$alias}}">-->
<!--                        <i class="menu-icon fa fa-caret-right"></i>-->
<!--                        {{l}}Buying Cùng Danh Mục{{/l}}-->
<!--                    </a>-->
<!---->
<!--                    <b class="arrow"></b>-->
<!--                </li>-->
<!--                <li class="{{if $menu[1] == 'manager-category-buying'}}active{{/if}} hover">-->
<!--                    <a href="{{$APP_BASE_URL}}estore/manage-buying-request/{{$alias}}">-->
<!--                        <i class="menu-icon fa fa-caret-right"></i>-->
<!--                        {{l}}Manage Buying Request{{/l}}-->
<!--                    </a>-->
<!---->
<!--                    <b class="arrow"></b>-->
<!--                </li>-->

<!--            </ul>-->
<!--        </li>-->
		<li class="{{if $menu[0] == 'banner'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-fax"></i>
                <span class="menu-text"> {{l}}Banner{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'manager-banner'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/manage-banner/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Banner{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'new-banner'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/new-banner/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Banner{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

            </ul>
        </li>
        <li class="{{if $menu[0] == 'product'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-fax"></i>
                <span class="menu-text"> {{l}}Product{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'manager-product'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/manage-product/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Product{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'new-product'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/new-product/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Product{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-category'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/manage-category/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Category{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'new-category'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/new-category/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Category{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="{{if $menu[0] == 'news'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-fax"></i>
                <span class="menu-text"> {{l}}News{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'manager-news'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/manage-news/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage News{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'new-news'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/new-news/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New News{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

            </ul>
        </li>

        <li class="{{if $menu[0] == 'promotion'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-fax"></i>
                <span class="menu-text"> {{l}}Promotion{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'manager-promotion'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/manage-promotion/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Promotion{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'new-promotion'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/new-promotion/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Promotion{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

            </ul>
        </li>

        <li class="{{if $menu[0] == 'estore-page'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-fax"></i>
                <span class="menu-text"> {{l}}Estore Page{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'manager-estore-page'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/manage-estore-page/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Estore Page{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

            </ul>
        </li>

        <li class="{{if $menu[0] == 'estore-message'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-fax"></i>
                <span class="menu-text"> {{l}}Message{{/l}} </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'manager-message'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}estore/manage-message/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Contact Center{{/l}} 
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="{{if $menu[1] == 'manager-message-khuyenmai'}}active{{/if}} hover">
                    <a href="{{$BASE_URL}}estore/manage-message-sale/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Tin Khuyến Mãi{{/l}} 
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="{{if $menu[1] == 'manager-message-hoihang'}}active{{/if}} hover">
                    <a href="{{$BASE_URL}}estore/manage-message-estore/{{$alias}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Thư Hỏi Hàng{{/l}} 
                    </a>

                    <b class="arrow"></b>
                </li>

            </ul>
        </li>
        <li class="{{if $menu[0] == 'newlater'}}active open{{/if}} hover">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-fax"></i>
				<span class="menu-text">{{l}}Newlater{{/l}}</span>

				<b class="arrow fa fa-angle-down"></b>
			</a>

			<b class="arrow"></b>

			<ul class="submenu">
				<li class="{{if $menu[1] == 'manager-newlater'}}active{{/if}} hover">
					<a href="{{$APP_BASE_URL}}estore/manage-newslate/{{$alias}}">
						<i class="menu-icon fa fa-caret-right"></i>
						{{l}}Manage Mail Newlater{{/l}}
					</a>

					<b class="arrow"></b>
				</li>
			</ul>
		</li>
		

    </ul><!-- /.nav-list -->


    <!-- #section:basics/sidebar.layout.minimize -->

    <!-- /section:basics/sidebar.layout.minimize -->
    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>
</div>






