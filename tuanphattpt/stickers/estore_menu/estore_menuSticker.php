<?php
require_once 'modules/language/models/Lang.php';
require_once 'modules/message/models/MessageCategory.php';
class estore_menuSticker extends Nine_Sticker
{
    public function run()
    {
        $objLang  = new Models_Lang();
        $objMessage = new Models_MessageCategory();

        $allLangs = $objLang->getByColumns(array('enabled = ?' => 1), array('sorting ASC'))->toArray();

        $condition = array(
            'enabled = ?' => 0,
            'parent_id = ?' => 0,
            'user_to_id = ?' => Nine_Registry::getLoggedInUserId()
        );
        $allNewMessage = $objMessage->getByColumns($condition)->toArray();
        if($allNewMessage != null) {
            $hasNewMessage = true;
        }

        $this->view->hasNewMessage = $hasNewMessage;
        $this->view->countNewMessage = count($allNewMessage);
        $this->view->allLangs = $allLangs;
        $this->view->loggedUser= Nine_Registry::getLoggedInUser()->toArray();
        
        $objCategory     = new Models_MessageCategory();
		$condition = array();
		$condition['user_to_id'] = Nine_Registry::getLoggedInUserId();
        $condition['parent_id'] = 0;
        $condition['admin_view'] = 0;
        $allCategories  = $objCategory->setAllLanguages(true)->getAllCategories($condition);
        
        $this->view->countNewContactEstore = count($allCategories);
    }
}