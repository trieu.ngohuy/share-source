<!-- box product -->
<div class="page-product-box">
    <h3 class="heading">{{l}}Related Products{{/l}}</h3>
    <ul class="product-list owl-carousel row" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":3},"1400":{"items":4}}'>
        {{foreach from=$product_slide item=item}}        
        <li>
            <div class="product-container">
                <div class="left-block">
                    <a href="{{$item.url}}">
                        <img class="img-responsive" alt="product" src="{{$item.main_image}}" />
                    </a>
                    <div class="quick-view">
                        <a title="Add to my wishlist" class="heart" href="#"></a>
                        <a title="Add to compare" class="compare" href="#"></a>
                        <a title="Quick view" class="search" href="#"></a>
                    </div>
                    <div class="add-to-cart">
                        <a title="Add to Cart" href="#add">Add to Cart</a>
                    </div>
                </div>
                <div class="right-block">
                    <h5 class="product-name"><a href="#">{{$item.title}}</a></h5>
                    <div class="content_price">
                        <span class="price product-price">
                            {{if $item.price_sale != ''}}
                            {{$item.price_sale}}{{$config.currency}}
                            {{elseif $item.price != ''}}
                            {{$item.price}}{{$config.currency}}
                            {{else}}
                            {{l}}Liên hệ{{/l}}
                            {{/if}}</span>
                        <span class="price old-price">
                            {{if $item.price_sale != '' && $item.price != ''}}
                            {{$item.price}} {{$config.currency}}
                            {{else}}
                            {{l}}Liên hệ{{/l}}
                            {{/if}}
                        </span>
                    </div>
                </div>
            </div>
        </li>
        {{/foreach}}
    </ul>
</div>
<!-- ./box product -->