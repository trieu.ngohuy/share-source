<div class="sortPagiBar">
    {{if $countAllPages >0}}
    <span class="page-noite">{{l}}Hiển thị trang{{/l}} {{$currentPage}} {{l}}trong{{/l}} ({{$countAllPages}} {{l}}trang{{/l}})</span>
    <div class="bottom-pagination">
        <nav>
            <ul class="pagination">
                {{if $currentPage > 1}}
                <li>
                    <a href="?page={{if ($currentPage-1) >= $prevPages.0}}{{$currentPage-1}}{{else}}#{{/if}}" aria-label="Next">
                        <span aria-hidden="true">{{l}}Trang trước{{/l}} &raquo;</span>
                    </a>
                </li>
                {{/if}}
                
                {{foreach from=$prevPages item=item}}
                <li><a href="?page={{$item}}">{{$item}}</a></li>
                {{/foreach}}
                
                <li class="active"><a href="#">{{$currentPage}}</a></li>
                
                {{foreach from=$nextPages item=item}}
                <li><a href="?page={{$item}}">{{$item}}</a></li>
                {{/foreach}}
                {{if $currentPage < $countAllPages}}
                <li>
                    <a href="?page={{if ($currentPage+1) <= $countAllPages}}{{$currentPage+1}}{{else}}#{{/if}}" aria-label="Next">
                        <span aria-hidden="true">{{l}}Trang tiếp{{/l}} &raquo;</span>
                    </a>
                </li>
                {{/if}}
            </ul>
        </nav>
    </div>
    {{/if}}
</div>