<!-- Footer -->
<footer id="footer">
    <div class="container">
        <!-- introduce-box -->
        <div id="introduce-box" class="row">
            <div class="col-md-4">
                <div id="address-box">
                    <a href="#"><img src="{{$LAYOUT_HELPER_URL}}front/assets/images/logo_nocndeal.png" alt="logo" /></a>
                    <div id="address-list">
                        <div class="tit-name">{{l}}Address:{{/l}}</div>
                        <div class="tit-contain">{{l}}Floor 2.1, 132-134 Dien Bien Phu, Dakao Ward, Ho Chi Minh City, Vietnam{{/l}}</div>
                        <div class="tit-name">{{l}}Phone:{{/l}}</div>
                        <div class="tit-contain">{{l}}(+84) 08 3820.1385{{/l}}</div>
                        <div class="tit-name">{{l}}Email:{{/l}}</div>
                        <div class="tit-contain">{{l}}support@C&D.com{{/l}}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="introduce-title">{{l}}COMPANY{{/l}}</div>
                        <ul id="introduce-company"  class="introduce-list">
                            <li><a href="{{$BASE_URL}}page/vision-mission">{{l}}Vision & Mission{{/l}}</a></li>
                            <li><a href="{{$BASE_URL}}page/messages-from-ceo">{{l}}Messages from CEO{{/l}}</a></li>
                            <li><a href="{{$BASE_URL}}page/introduction">{{l}}Introduction{{/l}}</a></li>
                            <li><a href="{{$BASE_URL}}sitemap">{{l}}Sitemap{{/l}}</a></li>
                            <li><a href="{{$BASE_URL}}page/advertise-with-us">{{l}}Advertise with Us{{/l}}</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="introduce-title">{{l}}INFORMATION{{/l}}</div>
                        <ul id = "introduce-Account" class="introduce-list">
                            <li><a href="{{$BASE_URL}}page/term-of-use">{{l}}Term of Use{{/l}}</a></li>
                            <li><a href="{{$BASE_URL}}page/privacy-policy">{{l}}Privacy Policy{{/l}}</a></li>
                            <li><a href="{{$BASE_URL}}page/quality-standards">{{l}}Quality Standards{{/l}}</a></li>
                            <li><a href="{{$BASE_URL}}page/quality-standards-vietnam">{{l}}Quality Standards Vietnam{{/l}}</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="introduce-title">{{l}}HELP CENTER{{/l}}</div>
                        <ul id = "introduce-support"  class="introduce-list">
                            <li><a href="{{$BASE_URL}}page/faqs">{{l}}FAQs{{/l}}</a></li>
                            <li><a href="{{$BASE_URL}}page/learning-center">{{l}}Learning Center{{/l}}</a></li>
                            <li><a href="{{$BASE_URL}}contact-us">{{l}}Contact Us{{/l}}</a></li>
                            <li><a href="{{$BASE_URL}}page/payment-instructions">{{l}}Payment Instructions{{/l}}</a></li>
                            <li><a href="{{$BASE_URL}}page/post-buying-request-rule">{{l}}Post Buying Request Rule{{/l}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div id="contact-box">
                    <div class="introduce-title">{{l}}Let's Socialize{{/l}}</div>
                    <div class="social-link">
                        <a href="https://www.facebook.com/C&D.tpp"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.youtube.com/channel/UC-T6CLuRYf8pRs-UOsu4ssQ"><i class="fa fa-youtube" style="background: red;"></i></a>
                        <a href="https://www.facebook.com/C&D.tpp"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.facebook.com/C&D.tpp"><i class="fa fa-google-plus"></i></a>
                    </div>
                </div>
            </div>
        </div><!-- /#introduce-box -->

        <!-- #trademark-box -->


        <!-- #trademark-text-box -->

        <div id="footer-menu-box">

            <p class="text-center">{{l}}Copyright 2016 C&D. All rights reserved.{{/l}}</p>
            <p class="text-center">{{l}}CÂU LẠC BỘ DOANH NHÂN{{/l}}</p>
        </div><!-- /#footer-menu-box -->
    </div>
</footer>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/57e331ff0814cc34e16e9647/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
