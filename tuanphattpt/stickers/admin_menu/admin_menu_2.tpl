<!-- h-sidebar	-->
<div id="sidebar" class="sidebar responsive h-sidebar sidebar-fixed">

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            {{foreach from=$allLangs item=item}}
            <a href="{{$BASE_URL}}admin/{{$item.lang_code}}"><img src="{{$BASE_URL}}{{$item.lang_image}}" alt=""></a>
                {{/foreach}}

        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div>

    <ul class="nav nav-list">

        <li class="{{if $menu[0] == 'home'}}active open{{/if}} hover">
            <a href="{{$APP_BASE_URL}}">
                <i class="menu-icon fa fa-dashboard"></i>
                <span class="menu-text"> {{l}}Control Panel{{/l}} </span>
            </a>

            <b class="arrow"></b>
        </li>



        <li class="{{if $menu[0] == 'setting'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-cog"></i>
                <span class="menu-text">
                    {{l}}Setting{{/l}}
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="{{if $menu[0] == 'language'}}active {{/if}} hover">
                    <a href="{{$APP_BASE_URL}}language/translation/manage">
                        <i class="menu-icon fa fa-language"></i>
                        <span class="menu-text"> {{l}}Translations{{/l}} </span>
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="{{if $menu[1] == 'topsuplier'}}active {{/if}} hover">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-caret-right"></i>

                        {{l}}Top Suplier{{/l}}
                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="{{if $menu[2] == 'new-topsuplier'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}topsuplier/admin/new-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}New Topsuplier{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="{{if $menu[2] == 'manager-topsuplier'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}topsuplier/admin/manage-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}Manage Topsuplier{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>


                    </ul>
                </li>

                <li class="{{if $menu[1] == 'package'}}active {{/if}} hover">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-caret-right"></i>

                        {{l}}Member Package{{/l}}
                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="{{if $menu[2] == 'new-package'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}package/admin/new-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}New Package{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="{{if $menu[2] == 'manager-package'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}package/admin/manage-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}Manage Package{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>


                    </ul>
                </li>

                <li class="{{if $menu[1] == 'payment'}}active {{/if}} hover">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-caret-right"></i>

                        {{l}}Payment Method{{/l}}
                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="{{if $menu[2] == 'new-payment'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}payment/admin/new-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}New Payment{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="{{if $menu[2] == 'manager-payment'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}payment/admin/manage-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}Manage Payment{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>


                    </ul>
                </li>
                <li class="{{if $menu[1] == 'provinces'}}active {{/if}} hover">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-caret-right"></i>

                        {{l}}Manager Provinces{{/l}}
                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="{{if $menu[2] == 'new-provinces'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}provinces/admin/new-provinces">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}New Provinces{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="{{if $menu[2] == 'manager-provinces'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}provinces/admin/manage-provinces">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}Manage Provinces{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>


                    </ul>
                </li>
                <li class="{{if $menu[1] == 'website'}}active {{/if}} hover">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-caret-right"></i>

                        {{l}}Link Website{{/l}}
                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="{{if $menu[2] == 'new-website'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}website/admin/new-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}New Link Website{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="{{if $menu[2] == 'manager-website'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}website/admin/manage-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}Manage Link Website{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>


                    </ul>
                </li>

                <li class="{{if $menu[1] == 'country'}}active {{/if}} hover">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-caret-right"></i>

                        {{l}}Country & City{{/l}}
                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="{{if $menu[2] == 'new-country'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}country/admin/new-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}New Country & City{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="{{if $menu[2] == 'manager-country'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}country/admin/manage-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}Manage Country & City{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>


                    </ul>
                </li>


                <li class="{{if $menu[1] == 'properties'}}active {{/if}} hover">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-caret-right"></i>

                        {{l}}Gennaral Select{{/l}}
                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="{{if $menu[2] == 'new-properties'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}properties/admin/new-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}New Properties{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="{{if $menu[2] == 'manager-properties'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}properties/admin/manage-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}Manage Properties{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>


                    </ul>
                </li>

                <li class="{{if $menu[1] == 'info'}}active {{/if}} hover">
                    <a href="{{$APP_BASE_URL}}default/admin/setting">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{l}}Info Company{{/l}} </span>
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'code'}}active open{{/if}} hover">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{l}}HS Code{{/l}} </span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">

                        <li class="{{if $menu[2] == 'new-hscode'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}hs/admin/new-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}New HS Code{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="{{if $menu[2] == 'manager-hscode'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}hs/admin/manage-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}Manage HS Code{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>

                    </ul>
                </li>

            </ul>
        </li>

        <li class="{{if $menu[0] == 'banner'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-picture-o"></i>
                <span class="menu-text"> {{l}}Banner{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'new-banner'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}banner/admin/new-banner">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Banner{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-banner'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}banner/admin/manage-banner">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Banner{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'new-category-banner'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}banner/admin/new-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Category Banner{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-category-banner'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}banner/admin/manage-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Categories Banner{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>
        <li class="{{if $menu[0] == 'estore'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-compress"></i>
                <span class="menu-text"> {{l}}Estore{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'message'}}active {{/if}} hover">
                    <a href="{{$APP_BASE_URL}}message/admin/manage-category" >
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Message Contact{{/l}}
                    </a>

                </li>


            </ul>
        </li>
        <li class="{{if $menu[0] == 'contact'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-compress"></i>
                <span class="menu-text"> {{l}}Contact Mainpage{{/l}} </span>
                {{if $hasNewContact == true}}
                <div style="background-image: url('{{$LAYOUT_HELPER_URL}}admin/images/icons/new_contact.png'); background-size: 30px 30px; width: 30px; height: 30px; position: absolute; top: 0px; right: 0px; max-width: 30px; text-align: center; padding-top: 3px; color: #fff">
                    {{$countNewContact}}
                </div>
                {{/if}}

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'new-contact'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}contact/admin/new-contact">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Contact{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-contact'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}contact/admin/manage-contact">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Contact{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'new-category-contact'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}contact/admin/new-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Support{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-category-contact'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}contact/admin/manage-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Support{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>


        <li class="{{if $menu[0] == 'buying'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-fax"></i>
                <span class="menu-text"> {{l}}Buying Request{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">


                <li class="{{if $menu[1] == 'manager-category-buying'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}buying/admin/manage-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Buying{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="{{if $menu[1] == 'new-category-buying'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}buying/admin/new-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Buying{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>


        <li class="{{if $menu[0] == 'user'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-users"></i>
                <span class="menu-text"> {{l}}Users & Groups{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'new-user'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}user/admin/new-user">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Users{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-user'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}user/admin/manage-user">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Users{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-group'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}user/admin/manage-group">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Groups{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-permission'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}permission/admin/manager">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Permissions{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>

        <li class="{{if $menu[0] == 'content'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-newspaper-o"></i>
                <span class="menu-text"> {{l}}Content & Category{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'new-content'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}content/admin/new-content">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Content{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-content'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}content/admin/manage-content">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Content{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'new-category-content'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}content/admin/new-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Category{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-category-content'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}content/admin/manage-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Categories{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>
<!--					<li class="{{if $menu[0] == 'bank'}}active open{{/if}} hover">-->
        <!--						<a href="#" class="dropdown-toggle">-->
        <!--							<i class="menu-icon fa fa-newspaper-o"></i>-->
        <!--							<span class="menu-text"> {{l}}Kết Nối tài Chính & Category{{/l}} </span>-->
        <!---->
        <!--							<b class="arrow fa fa-angle-down"></b>-->
        <!--						</a>-->
        <!---->
        <!--						<b class="arrow"></b>-->
        <!---->
        <!--						<ul class="submenu">-->
        <!--						-->
        <!--							<li class="{{if $menu[1] == 'new-bank'}}active{{/if}} hover">-->
        <!--								<a href="{{$APP_BASE_URL}}bank/admin/new-bank">-->
        <!--									<i class="menu-icon fa fa-caret-right"></i>-->
        <!--									{{l}}New Kết Nối Tài Chính{{/l}}-->
        <!--								</a>-->
        <!---->
        <!--								<b class="arrow"></b>-->
        <!--							</li>-->
        <!--							-->
        <!--							<li class="{{if $menu[1] == 'manager-bank'}}active{{/if}} hover">-->
        <!--								<a href="{{$APP_BASE_URL}}bank/admin/manage-bank">-->
        <!--									<i class="menu-icon fa fa-caret-right"></i>-->
        <!--									{{l}}Manage Kết Nối Tài Chính{{/l}}-->
        <!--								</a>-->
        <!---->
        <!--								<b class="arrow"></b>-->
        <!--							</li>-->
        <!--							-->
        <!--							<li class="{{if $menu[1] == 'new-category-bank'}}active{{/if}} hover">-->
        <!--								<a href="{{$APP_BASE_URL}}bank/admin/new-category">-->
        <!--									<i class="menu-icon fa fa-caret-right"></i>-->
        <!--									{{l}}New Category Bank{{/l}}-->
        <!--								</a>-->
        <!---->
        <!--								<b class="arrow"></b>-->
        <!--							</li>-->
        <!--							-->
        <!--							<li class="{{if $menu[1] == 'manager-category-bank'}}active{{/if}} hover">-->
        <!--								<a href="{{$APP_BASE_URL}}bank/admin/manage-category">-->
        <!--									<i class="menu-icon fa fa-caret-right"></i>-->
        <!--									{{l}}Manage Categories{{/l}}-->
        <!--								</a>-->
        <!---->
        <!--								<b class="arrow"></b>-->
        <!--							</li>-->
        <!---->
        <!--							-->
        <!--						</ul>-->
        <!--					</li>-->


        <li class="{{if $menu[0] == 'product'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-product-hunt"></i>
                <span class="menu-text"> {{l}}Product & Category{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'new-product'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}product/admin/new-product">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Product{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-product'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}product/admin/manage-product">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Product{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'new-category-product'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}product/admin/new-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Category Product{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-category-product'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}product/admin/manage-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Categories Product{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>



<!--					<li class="{{if $menu[0] == 'newlater'}}active open{{/if}} hover">-->
        <!--						<a href="#" class="dropdown-toggle">-->
        <!--							<i class="menu-icon fa fa-fax"></i>-->
        <!--							<span class="menu-text">{{l}}Newlater{{/l}}</span>-->
        <!---->
        <!--							<b class="arrow fa fa-angle-down"></b>-->
        <!--						</a>-->
        <!---->
        <!--						<b class="arrow"></b>-->
        <!---->
        <!--						<ul class="submenu">-->
        <!--							<li class="{{if $menu[1] == 'manager-newlater'}}active{{/if}} hover">-->
        <!--								<a href="{{$APP_BASE_URL}}newlater/admin/manage-category">-->
        <!--									<i class="menu-icon fa fa-caret-right"></i>-->
        <!--									{{l}}Manage Mail Newlater{{/l}}-->
        <!--								</a>-->
        <!---->
        <!--								<b class="arrow"></b>-->
        <!--							</li>-->
        <!--						</ul>-->
        <!--					</li>-->



<!--					<li class="{{if $menu[0] == 'commercial'}}active open{{/if}} hover">-->
        <!--						<a href="#" class="dropdown-toggle">-->
        <!--							<i class="menu-icon fa fa-fax"></i>-->
        <!--							<span class="menu-text"> {{l}}Commercial{{/l}} </span>-->
        <!---->
        <!--							<b class="arrow fa fa-angle-down"></b>-->
        <!--						</a>-->
        <!---->
        <!--						<b class="arrow"></b>-->
        <!---->
        <!--						<ul class="submenu">-->
        <!---->
        <!--							<li class="{{if $menu[1] == 'manager-commercial'}}active{{/if}} hover">-->
        <!--								<a href="{{$APP_BASE_URL}}commercial/admin/manage-commercial">-->
        <!--									<i class="menu-icon fa fa-caret-right"></i>-->
        <!--									{{l}}Manage Commercial{{/l}}-->
        <!--								</a>-->
        <!---->
        <!--								<b class="arrow"></b>-->
        <!--							</li>-->
        <!--							<li class="{{if $menu[1] == 'new-commercial'}}active{{/if}} hover">-->
        <!--								<a href="{{$APP_BASE_URL}}commercial/admin/new-commercial">-->
        <!--									<i class="menu-icon fa fa-caret-right"></i>-->
        <!--									{{l}}New Commercial{{/l}}-->
        <!--								</a>-->
        <!---->
        <!--								<b class="arrow"></b>-->
        <!--							</li>-->
        <!---->
        <!---->
        <!--						</ul>-->
        <!--					</li>-->
        <!--<li class="{{if $menu[0] == 'payment'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-fax"></i>
                <span class="menu-text"> {{l}}Payment{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'manager-payment'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}user/admin/manage-payment">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Payment{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>-->
        <li class="{{if $menu[0] == 'provinces'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-car"></i>
                <span class="menu-text"> {{l}}Provinces{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'manager-provinces'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}provinces/admin/manage-provinces">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Provinces{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="{{if $menu[1] == 'new-provinces'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}provinces/admin/new-provinces">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Provinces{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>
        <!--Cart Manager-->
        <li class="{{if $menu[0] == 'cart-manager'}}active open{{/if}} hover">
            <a href="{{$APP_BASE_URL}}cart/admin/manage-category" class="dropdown-toggle">
                <i class="menu-icon fa fa-product-hunt"></i>
                <span class="menu-text"> {{l}}Manager Cart{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
        </li>


    </ul><!-- /.nav-list -->


    <!-- #section:basics/sidebar.layout.minimize -->

    <!-- /section:basics/sidebar.layout.minimize -->
    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>
</div>






