<?php
require_once 'modules/language/models/Lang.php';
require_once 'modules/contact/models/Contact.php';
class admin_menuSticker extends Nine_Sticker
{
	public function run()
	{
		$objLang  = new Models_Lang();
		$objContact = new Models_Contact();

		$allLangs = $objLang->getByColumns(array('enabled = ?' => 1), array('sorting ASC'))->toArray();
		$allNewContact = $objContact->getByColumns(array())->toArray();

		$hasNewContact = false;
		if($allNewContact != null) {
			$hasNewContact = true;
		}

        $this->view->allLangs = $allLangs;
		$this->view->hasNewContact = $hasNewContact;
		$this->view->countNewContact = count($allNewContact);
	    $this->view->loggedUser= Nine_Registry::getLoggedInUser()->toArray();
	}
}