<!-- h-sidebar	-->
<div id="sidebar" class="sidebar responsive h-sidebar sidebar-fixed">

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            {{foreach from=$allLangs item=item}}
            <a href="{{$BASE_URL}}admin/{{$item.lang_code}}"><img src="{{$BASE_URL}}{{$item.lang_image}}" alt=""></a>
                {{/foreach}}

        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div>

    <ul class="nav nav-list">

        <li class="{{if $menu[0] == 'home'}}active open{{/if}} hover">
            <a href="{{$APP_BASE_URL}}">
                <i class="menu-icon fa fa-dashboard"></i>
                <span class="menu-text"> {{l}}Control Panel{{/l}} </span>
            </a>

            <b class="arrow"></b>
        </li>



        <li class="{{if $menu[0] == 'setting'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-cog"></i>
                <span class="menu-text">
                    {{l}}Setting{{/l}}
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="{{if $menu[0] == 'language'}}active {{/if}} hover">
                    <a href="{{$APP_BASE_URL}}language/translation/manage">
                        <i class="menu-icon fa fa-language"></i>
                        <span class="menu-text"> {{l}}Translations{{/l}} </span>
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="{{if $menu[1] == 'topsuplier'}}active {{/if}} hover">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-caret-right"></i>

                        {{l}}Top Suplier{{/l}}
                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="{{if $menu[2] == 'new-topsuplier'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}topsuplier/admin/new-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}New Topsuplier{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="{{if $menu[2] == 'manager-topsuplier'}}active{{/if}} hover">
                            <a href="{{$APP_BASE_URL}}topsuplier/admin/manage-category">
                                <i class="menu-icon fa fa-caret-right"></i>
                                {{l}}Manage Topsuplier{{/l}}
                            </a>

                            <b class="arrow"></b>
                        </li>


                    </ul>
                </li>

                <li class="{{if $menu[1] == 'info'}}active {{/if}} hover">
                    <a href="{{$APP_BASE_URL}}default/admin/setting">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <span class="menu-text"> {{l}}Info Company{{/l}} </span>
                    </a>

                    <b class="arrow"></b>
                </li>

            </ul>
        </li>

        <li class="{{if $menu[0] == 'menu'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-header"></i>
                <span class="menu-text"> {{l}}Menu{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'new-menu'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}menu/admin/new-menu">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Menu{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-menu'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}menu/admin/manage-menu">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Menu{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>

        <li class="{{if $menu[0] == 'content'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-newspaper-o"></i>
                <span class="menu-text"> {{l}}Content & Category{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'new-content'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}content/admin/new-content">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Content{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-content'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}content/admin/manage-content">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Content{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'new-category-content'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}content/admin/new-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Category{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-category-content'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}content/admin/manage-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Categories{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>


        <li class="{{if $menu[0] == 'product'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-product-hunt"></i>
                <span class="menu-text"> {{l}}Product & Category{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'new-product'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}product/admin/new-product">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Product{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-product'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}product/admin/manage-product">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Product{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'new-category-product'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}product/admin/new-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}New Category Product{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-category-product'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}product/admin/manage-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Categories Product{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>

        <li class="{{if $menu[0] == 'project'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-file-zip-o"></i>
                <span class="menu-text"> {{l}}Project{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'new-project'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}project/admin/new-project">
                        <i class="menu-icon fa fa-caret-righ"></i>
                        {{l}}New Project{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-project'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}project/admin/manage-project">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Project{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>

        <li class="{{if $menu[0] == 'contact'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-phone"></i>
                <span class="menu-text"> {{l}}Contact{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'manager-contact'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}contact/admin/manage-contact">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Contact{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>
        
        <li class="{{if $menu[0] == 'newsletter'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-mail-forward"></i>
                <span class="menu-text"> {{l}}NewsLetter{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'manager-newsletter'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}newlater/admin/manage-category">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage NewsLetter{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>

        <li class="{{if $menu[0] == 'params'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-sort-numeric-asc"></i>
                <span class="menu-text"> {{l}}Params{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'new-params'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}design/admin/new-params">
                        <i class="menu-icon fa fa-caret-righ"></i>
                        {{l}}New Params{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-project'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}design/admin/manage-params">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Params{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>

        <li class="{{if $menu[0] == 'design'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-desktop"></i>
                <span class="menu-text"> {{l}}Design{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'new-design'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}design/admin/new-design">
                        <i class="menu-icon fa fa-caret-righ"></i>
                        {{l}}New Design{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{if $menu[1] == 'manager-design'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}design/admin/manage-design">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Design{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>

        <li class="{{if $menu[0] == 'design-register'}}active open{{/if}} hover">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-newspaper-o"></i>
                <span class="menu-text"> {{l}}Design Register{{/l}} </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="{{if $menu[1] == 'manager-design-register'}}active{{/if}} hover">
                    <a href="{{$APP_BASE_URL}}design/admin/manage-design-register">
                        <i class="menu-icon fa fa-caret-right"></i>
                        {{l}}Manage Design Register{{/l}}
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul>
        </li>

    </ul><!-- /.nav-list -->


    <!-- #section:basics/sidebar.layout.minimize -->

    <!-- /section:basics/sidebar.layout.minimize -->
    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>
</div>






