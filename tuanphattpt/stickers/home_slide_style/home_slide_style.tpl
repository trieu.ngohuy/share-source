<style>
    /** OPTION CATEGORY **/

    .option6 .category-featured-{{$allCatItem.product_category_gid}} .sub-category-list a:hover{
        color: {{$allCatItem.color}};
    }

    .option6 .category-featured-{{$allCatItem.product_category_gid}} .navbar-brand{
        background: {{$allCatItem.color}};
    }
    .option6 .category-featured-{{$allCatItem.product_category_gid}} .navbar-brand a:hover{
        color: #fff;
    }
    .option6 .category-featured-{{$allCatItem.product_category_gid}} .nav-menu .navbar-collapse {
        background: #fff;
        border-bottom: 2px solid {{$allCatItem.color}};
    }
    .option6 .category-featured-{{$allCatItem.product_category_gid}} .nav-menu .nav>li:hover a,
    .option6 .category-featured-{{$allCatItem.product_category_gid}} .nav-menu .nav>li.active a{
        color: {{$allCatItem.color}};
    }

    .option6 .category-featured-{{$allCatItem.product_category_gid}} .nav-menu .nav>li:hover a:after,
    .option6 .category-featured-{{$allCatItem.product_category_gid}} .nav-menu .nav>li.active a:after{
        color: {{$allCatItem.color}};
    }

    .option6 .category-featured-{{$allCatItem.product_category_gid}} .nav-menu .nav>li>a:before{
        background: {{$allCatItem.color}};
    }

    .option6 .category-featured-{{$allCatItem.product_category_gid}} .product-list li .add-to-cart {
        //background-color: #2a467f;
        //background: #2a467f;
        color: #2a467f;
    }
    .option6 .category-featured-{{$allCatItem.product_category_gid}} .product-list li .add-to-cart:hover {
        //background: {{$allCatItem.color}};
    }

    .option6 .category-featured-{{$allCatItem.product_category_gid}} .product-list li .quick-view a.search:hover,
    .option6 .category-featured-{{$allCatItem.product_category_gid}} .product-list li .quick-view a.compare:hover,
    .option6 .category-featured-{{$allCatItem.product_category_gid}} .product-list li .quick-view a.heart:hover{
        background-color: {{$allCatItem.color}};
        opacity: 0.9;
    }
</style>