<?php
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/product/models/Product.php';
require_once 'modules/content/models/Content.php';
require_once 'modules/content/models/ContentCategory.php';
require_once 'modules/language/models/Lang.php';
require_once 'modules/list/models/List.php';
require_once 'Zend/Config.php';
require_once 'Zend/Config/Writer/Array.php';
require_once 'modules/user/models/User.php';
class vendorSticker extends Nine_Sticker
{
    public function run()
    {
        $objContent = new Models_Content();
		$objContentcat = new Models_ContentCategory();
		$objUser  = new Models_User();
		$objProduct = new Models_Product();
		$allVendorBar = $objUser->getAllUsersWithGroup($condition);
		foreach ($allVendorBar as &$item){
			$item['url'] = Nine_Route::_("user/index/vendor",array ('alias' => $item['alias'] ) );
			
			$tmp = explode ( '||', $item ['image'] );
			$item ['main_image'] = Nine_Function::getThumbImage ( @$tmp[0], 170, 68 , false , false );
		}
		unset($item);
		$this->view->allVendorBar = $allVendorBar;
    }
}