<?php

require_once 'Zend/Config.php';
require_once 'Zend/Config/Writer/Array.php';
class front_hs_standardSticker extends Nine_Sticker
{
    public function run()
    {
        if($this->auth->isLogin()) {
            $this->view->userLogin = Nine_Registry::getLoggedInUser();
        }
        $this->view->isLogin = $this->auth->isLogin();
    }
}