{{if $isLogin == true}}
<p style="margin: 0"><a data-toggle="modal" href="#hsCode"><b style="    font-weight: bold;">{{l}}View HS Code{{/l}}</b></a> - <a data-toggle="modal" href="#hsCodeVN"><b style="    font-weight: bold;">{{l}}View HS Code Vietnam{{/l}}</b></a></p>
<p><a data-toggle="modal" href="#hsStandard"><b style="    font-weight: bold;">{{l}}View Quality Standards{{/l}}</b></a> - <a data-toggle="modal" href="#hsStandardVN"><b style="    font-weight: bold;">{{l}}View Quality Standards Vietnam{{/l}}</b></a></p>
{{else}}
<p><a data-toggle="modal" href="#loginPopup"><b style="    font-weight: bold;">{{l}}View HS Code{{/l}}</b></a> - <a data-toggle="modal" href="#loginPopup"><b style="    font-weight: bold;">{{l}}View HS Code Vietnam{{/l}}</b></a></p>
<p><a data-toggle="modal" href="#loginPopup"><b style="    font-weight: bold;">{{l}}View Quality Standards{{/l}}</b></a> - <a data-toggle="modal" href="#loginPopup"><b style="    font-weight: bold;">{{l}}View Quality Standards Vietnam{{/l}}</b></a></p>
{{/if}}
<!-- HS Code -->
<div class="modal fade" id="hsCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>{{l}}View HS Code{{/l}}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{$BASE_URL}}{{$product.icon}}" style="width: 100%">
                    </div>
                    <div class="col-md-8">
                        <p style="    margin: 0 0 5px !important;"><strong>{{l}}HS Code{{/l}} </strong> : <span style="color: #e60707">{{$hsCode.name}}</span></p>
                        <p style="    margin: 0 0 5px !important;"><strong>{{l}}HS Description{{/l}}</strong></p>
                        {{$hsCode.description}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- HS Code VN -->
<div class="modal fade" id="hsCodeVN" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>{{l}}View HS Code Vietnam{{/l}}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{$BASE_URL}}{{$product.icon}}" style="width: 100%">
                    </div>
                    <div class="col-md-8">
                        <p style="    margin: 0 0 5px !important;"><strong>{{l}}HS Code Vietname{{/l}}</strong> : <span style="color: #e60707">{{$hsCodeVN.name}}</span></p>
                        <p style="    margin: 0 0 5px !important;"><strong>{{l}}HS Description{{/l}}</strong></p>
                        {{$hsCodeVN.description}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- View Quatity Standard  -->
<div class="modal fade" id="hsStandard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>{{l}}View Quatity Standard{{/l}}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{$BASE_URL}}{{$product.icon}}" style="width: 100%">
                    </div>
                    <div class="col-md-8">
                        <p style="    margin: 0 0 5px !important;"><strong>{{l}}Quatity Standard{{/l}}</strong></p>
                        {{$product.tc_qt}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- View Quatity Standard Viet Nam -->
<div class="modal fade" id="hsStandardVN" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>{{l}}View Quatity Standard Vietnam{{/l}}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{$BASE_URL}}{{$product.icon}}" style="width: 100%">
                    </div>
                    <div class="col-md-8">
                        <p  style="    margin: 0 0 5px !important;"><strong>{{l}}Quatity Standard Vietnam{{/l}}</strong></p>
                        {{$product.tc_vn}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Upgrade Account -->
<div class="modal fade" id="#upgradeAccountMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">{{l}}Upgrade Account{{/l}}</h4>
            </div>
            <div class="modal-body">
                {{l}}
                This is advanced features only available to official members. Please upgrade to official member to see the quality standards
                {{/l}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">{{l}}Upgrade Account{{/l}}</button>
            </div>
        </div>
    </div>
</div>

