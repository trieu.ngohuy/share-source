<!-- HEADER -->
<div id="header" class="header">
	<div class="top-header">
		<div class="container">
			<div class="nav-top-links">
				<a class="first-item" href="#"><img alt="phone" src="{{$LAYOUT_HELPER_URL}}front/assets/images/phone.png" />CALL US 090 141 8787</a>
				<a href="#"><img alt="email" src="{{$LAYOUT_HELPER_URL}}front/assets/images/email.png" />Contact us today!</a>
			</div>


			<div class="language ">
				<div class="dropdown">
					<a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
						{{if $langCode == 'vi'}}
						<img alt="email" src="{{$LAYOUT_HELPER_URL}}front/assets/images/vi.png" />Vietnamese
						{{else}}
							<img alt="email" src="{{$LAYOUT_HELPER_URL}}front/assets/images/en.png" />English
						{{/if}}
					</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="?lang=en"><img alt="email" src="{{$LAYOUT_HELPER_URL}}front/assets/images/en.png" />English</a></li>
						<li><a href="?lang=vi"><img alt="email" src="{{$LAYOUT_HELPER_URL}}front/assets/images/vi.png" />Vietnamese</a></li>
					</ul>
				</div>
			</div>

			<div class="nav-top-links">
				{{if $isLogin == true}}
					<a href="{{$BASE_URL}}user/profile">
						Hi <span class="font_bold">{{$userLogin.last_name}} ! </span><img alt="email" src="{{$LAYOUT_HELPER_URL}}front/assets/images/user.png" />
						{{if $hasNewContact == true}}
							<span class="badge">{{$countNewContact}}</span>
						{{/if}}
					</a>
				{{/if}}
			</div>
			<div class="nav-top-links">
				{{if $isLogin == false}}
					<a data-toggle="modal" href="#loginPopup">{{l}}Sign in{{/l}}</a>
					{{sticker name=front_login}}
				{{else}}
					<a data-toggle="modal" href="{{$BASE_URL}}access/logout">{{l}}Sign out{{/l}}</a>
				{{/if}}
			</div>
			{{if $isLogin != false && $userLogin.group_id == 3}}
			<div class="language ">
				<div class="dropdown">
					<a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
						{{l}}My C&D{{/l}}
					</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{$BASE_URL}}{{$userLogin.alias}}.htm" target="_blank">{{l}}Your STORE{{/l}}</a></li>
						<li><a href="{{$BASE_URL}}estore/manage-message-sale/{{$userLogin.alias}}.html">{{l}}Tin Khuyến Mãi Mới{{/l}}
							{{if $countNewContactEstoreKhuyenMai != 0}}
							<span class="badge">{{$countNewContactEstoreKhuyenMai}}</span>
							{{/if}}
						</a></li>
<!--						<li><a href="{{$BASE_URL}}estore/{{$userLogin.alias}}.html">{{l}}New Chat{{/l}}</a></li>-->
						<li><a href="{{$BASE_URL}}estore/{{$userLogin.alias}}.html">{{l}}Chat Management{{/l}}</a></li>
						<li>
							<a href="{{$BASE_URL}}estore/manage-message/{{$userLogin.alias}}.html">
								{{l}}Contact Center{{/l}}
								{{if $countNewContactEstore != 0}}
								<span class="badge">{{$countNewContactEstore}}</span>
								{{/if}}
							</a>
						</li>
						<li><a href="{{$BASE_URL}}estore/manage-message-estore/{{$userLogin.alias}}.html">{{l}}Thư Hỏi Hàng Mới{{/l}}
							{{if $countNewContactEstoreHoiHang != 0}}
							<span class="badge">{{$countNewContactEstoreHoiHang}}</span>
							{{/if}}
						</a></li>
						<li><a href="{{$BASE_URL}}user/buying-request">{{l}}Báo Giá Mới{{/l}}
							{{if $countNewBuying != 0}}
							<span class="badge">{{$countNewBuying}}</span>
							{{/if}}
						</a></li>
						<li><a href="{{$BASE_URL}}estore/manage-product/{{$userLogin.alias}}.html">{{l}}Product Management{{/l}}</a></li>
						<li><a href="{{$BASE_URL}}estore/edit-info/{{$userLogin.alias}}.html">{{l}}Setting{{/l}}</a></li>
					</ul>
				</div>
			</div>
			{{/if}}
			<div class="support-link">
				<a href="{{$BASE_URL}}contact-us">{{l}}Contact{{/l}}</a>
				<a href="{{$BASE_URL}}trade-show">{{l}}Sự Kiện{{/l}}</a>
			</div>

		</div>
	</div>
	<!--/.top-header -->
	<!-- MAIN HEADER -->
	<div class="container main-header">
		<div class="row">
			<div class="col-xs-12 col-sm-3 logo">
				<a href="{{$BASE_URL}}" ><img alt="C&D" src="{{$LAYOUT_HELPER_URL}}front/assets/images/logo_nocndeal.png" /></a>
			</div>
			<div class="col-xs-7 col-sm-7 header-search-box">
				<form class="form-inline" method="get" action="{{$BASE_URL}}search">
					<div class="form-group form-category">
						<select class="select-category" name="search">
							<option value="product">{{l}}Products{{/l}}</option>
							<option value="seller">{{l}}Doanh Nghiệp{{/l}}</option>
						</select>
					</div>
					<div class="form-group input-serach">
						<input type="text" name="key"  placeholder="Keyword here...">
					</div>
					<button type="submit" class="pull-right btn-search"></button>
				</form>
			</div>
			<div class="col-xs-5 col-sm-2">
				{{sticker name=vendor}}
			</div>
		</div>

	</div>
	<!-- END MANIN HEADER -->
	<div id="nav-top-menu" class="nav-top-menu">
		<div class="container">
				
				<div id="main-menu" class="col-sm-12 main-menu">
					<nav class="navbar navbar-default">
						<div class="container-fluid">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
									<i class="fa fa-bars"></i>
								</button>
								<a class="navbar-brand" href="#">MENU</a>
							</div>
							<div id="navbar" class="navbar-collapse collapse">
								<ul class="nav navbar-nav">
									{{foreach from=$mnbv item=item key=key}}
									
										<li class="{{if $cat.content_category_gid == $item.content_category_gid || $cat.parent_id == $item.content_category_gid}}current{{/if}} {{if $item.parent_cate|@count > 0}}dropdown{{/if}}"> <a {{if $item.parent_cate|@count > 0}}  class="dropdown-toggle" data-toggle="dropdown" {{else}}  href="{{$item.url}}" {{/if}}> {{$item.name}} </a>
								          	{{if $item.parent_cate|@count > 0}}
								          		<ul class="dropdown-menu container-fluid">
								          			<li class="block-container">
														<ul class="block">
								        					{{foreach from=$item.parent_cate item=item1 key=key1}}
								        						<li class="link_container"><a href="{{$item1.url}}">{{$item1.name}}</a></li>
							                                {{/foreach}}
					                                	</ul>
													</li>
								          		</ul>
								          	{{/if}}
								        </li> 
								    {{/foreach}}
									<li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown"> {{l}}SẢN PHẨM{{/l}} </a>
										<ul class="dropdown-menu container-fluid">
											<li class="block-container">
												<ul class="block">
													{{foreach from=$allCategory item=item key=key}}
														<li class="link_container"><a href="{{$BASE_URL}}category/{{$item.alias}}">{{$item.name}}</a></li>
								    				{{/foreach}}
								    			</ul>
											</li>
										</ul>
								    </li> 
								    <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown"> {{l}}DOANH NGHIỆP{{/l}} </a>
										<ul class="dropdown-menu container-fluid">
											<li class="block-container">
												<ul class="block">
													{{foreach from=$allCategory item=item key=key}}
														<li class="link_container"><a href="{{$BASE_URL}}category/{{$item.alias}}?lid=2">{{$item.name}}</a></li>
								    				{{/foreach}}
								    			</ul>
											</li>
										</ul>
								    </li> 
								    <li><a href="{{$BASE_URL}}trade-show">{{l}}SỰ KIỆN{{/l}}</a></li>
								    <li><a href="{{$BASE_URL}}contact-us">{{l}}LIÊN HỆ{{/l}}</a></li>
									
								</ul>
							</div><!--/.nav-collapse -->
						</div>
					</nav>
				</div>
			<!-- userinfo on top-->
			<div id="form-search-opntop">
			</div>
		</div>
	</div>
</div>
<!-- end header -->