<ul class="product-list owl-carousel" data-dots="false" data-loop="true"
data-nav="true" data-margin="30" data-autoplayTimeout="1000" data-autoplayHoverPause="true"
data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
	{{foreach from=$product_slide item=item}}
	<li>
		<div class="left-block">
			<a href="{{$item.url}}"> <img class="img-responsive" alt="product" src="{{$item.main_image}}"
				/> </a>
			{{ if $item.price_sale != '' }}
			<div class="group-price">			
			<span class="product-new">NEW</span> <span class="product-sale">Sale</span>				
			</div>
			{{/if}}
		</div>
		<div class="right-block">
			<h5 class="product-name"><a href="{{$item.url}}">{{$item.title}}</a></h5>
			<div class="content_price"> 
				<div class="content_price" {{if $item.pt_sale==''}} style="padding-left: 0px;" {{/if}} >
			      	<span class="price old-price">{{$item.price}}{{$config.price_unit}}</span>
			        <br /> 
			        <span class="price product-price">{{$item.price_sale}}{{$config.price_unit}}</span>
			        <a href="{{$item.url}}" class="product-more">
			        <img src="{{$LAYOUT_HELPER_URL}}front/assets/images/cartNumber.png"
			          style="width: 18px;float: left;"
			          /> {{if $item.so_luong == ''}}0
			          {{else}}{{$item.so_luong}}{{/if}}
			          </a> {{if $item.price_sale != ''}}
			        <style>
			          .product-sale-persent {
			            position: absolute;
			            left: -5px;
			            top: 4px;
			            color: orange;
			            font-size: 29px;
			          }
			        </style>
			        <p class="product-sale-persent"
			        {{if $item.pt_sale=='' }} style="display: none;"
			        {{/if}}>{{$item.pt_sale}}<span>%</span> </p>
			        {{/if}} 
		        </div>
			</div>			
		</div>
	</li> 
	{{/foreach}} 
</ul>