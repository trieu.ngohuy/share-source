<div class="breadcrumb clearfix">
	<a class="home" href="{{$breadCrumbs.home.url}}" title="Return to Home">{{$breadCrumbs.home.name}}</a>
	<span class="navigation-pipe">&nbsp;</span>
	{{foreach from=$breadCrumbs item=breadCrumbsItem}}
	<span class="navigation_page">
		<a href="{{$breadCrumbsItem.url}}">{{$breadCrumbsItemname}}</a>
	</span>
	<span class="navigation-pipe">&nbsp;</span>
	{{/foreach}}
	<span class="navigation_page">
		{{$breadCrumbs.detail.name}}
	</span>
</div>