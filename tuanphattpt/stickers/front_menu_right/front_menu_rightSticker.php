<?php
require_once 'modules/product/models/ProductCategory.php';
require_once 'modules/product/models/Product.php';
require_once 'modules/content/models/Content.php';
require_once 'modules/content/models/ContentCategory.php';
class front_menu_rightSticker extends Nine_Sticker
{
	public function run()
	{
		$objProduct = new Models_Product ();
		$objProductCategory = new Models_ProductCategory ();
		$objContent = new Models_Content ();
		
		$category = $objProductCategory->getAllCategoriesParentNull();
		try {
			foreach ($category as &$item){
				$item['parent_cate'] = $objProductCategory->getAllProductCatWithParent($item['product_category_gid']);
				if($item['alias'] == ""){
					$item['alias'] = $objContent->convert_vi_to_en($item['name']);
					$item['alias'] = str_replace(" ", "-", str_replace("&*#39;","",trim($item['alias'])));
				}
				$item['url'] = Nine_Route::_("product/index/index/cid/{$item['product_category_gid']}",array ('alias' => $item['alias'] ) );
				foreach ($item['parent_cate'] as &$item_){
					$item_['parent_cate'] = $objProductCategory->getAllProductCatWithParent($item_['product_category_gid']);
					if($item_['alias'] == ""){
						$item_['alias'] = $objContent->convert_vi_to_en($item_['name']);
						$item_['alias'] = str_replace(" ", "-", str_replace("&*#39;","",trim($item_['alias'])));
					}
					$item_['url'] = Nine_Route::_("product/index/index/cid/{$item_['product_category_gid']}",array ('alias' => $item_['alias'] ) );
					
					foreach ($item_['parent_cate'] as &$item__){
						if($item__['alias'] == ""){
							$item__['alias'] = $objContent->convert_vi_to_en($item__['name']);
							$item__['alias'] = str_replace(" ", "-", str_replace("&*#39;","",trim($item__['alias'])));
						}
						$item__['url'] = Nine_Route::_("product/index/index/cid/{$item__['product_category_gid']}",array ('alias' => $item__['alias'] ) );
					}
					unset($item__);
				}
				unset($item_);
			}
		} catch (Exception $e) {
			
		}
		unset($item);
		$this->view->category = $category;
		
		$tinnhanh = $objContent->getAllContentTinNhanh();
		foreach ( $tinnhanh as &$content ) {
			$content ['title'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $content ['title'] ) ), 30 );
			$content ['intro_text'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $content ['full_text'] ) ), 100 );
			if($content ['alias'] == ""){
				$content ['alias'] = $objContent->convert_vi_to_en($content ['alias']);
				$content ['alias'] = str_replace(" ", "-", str_replace("&*#39;","",trim($content ['alias'])));
			}
			$content ['url'] = Nine_Route::_ ( "content/index/detail/gid/{$content['content_gid']}", array ('alias' => $content ['alias'] ) );
			$tmp = explode ( '||', $content ['images'] );
			$content ['main_image'] = Nine_Function::getThumbImage ( @$tmp [0], 96, 96 , false , true );
		}
		unset ( $content );
		
		
		
		$allProduct = $objProduct->getAllProduct();
		foreach ( $allProduct as &$content ) {
			$content ['title'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $content ['title'] ) ), 30 );
			$content ['intro_text'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $content ['full_text'] ) ), 100 );
			if($content ['alias'] == ""){
				$content ['alias'] = $objContent->convert_vi_to_en($content ['alias']);
				$content ['alias'] = str_replace(" ", "-", str_replace("&*#39;","",trim($content ['alias'])));
			}
			$content ['url'] = Nine_Route::_ ( "product/index/detail/gid/{$content['product_gid']}", array ('alias' => $content ['alias'] ) );
			$tmp = explode ( '||', $content ['images'] );
			$content ['main_image'] = Nine_Function::getThumbImage ( @$tmp [0], 270, 270 , false , true );
		}
		unset ( $content );
		
		$this->view->tinnhanh_content = $tinnhanh;
		$this->view->allProduct = $allProduct;
		
	}
}