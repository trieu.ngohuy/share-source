<!-- Sidebar -->
				<aside class="sidebar col-lg-3 col-md-3 col-sm-3  col-lg-pull-9 col-md-pull-9 col-sm-pull-9">
					
					<!-- Categories -->
					<div class="row sidebar-box purple">
						
						<div class="col-lg-12 col-md-12 col-sm-12">
							
							<div class="sidebar-box-heading">
								<i class="icons icon-folder-open-empty"></i>
								<h4>{{l}}Categories{{/l}}</h4>
							</div>
							
							<div class="sidebar-box-content">
								<ul>
									{{foreach from=$category item=item}}
										
										<li><a href="{{$item.url}}">{{$item.name}}{{if $item.parent_cate|@count > 0}}<i class="icons icon-right-dir"></i>{{/if}}</a>
	                                    	{{if $item.parent_cate|@count > 0}}	
	                                    		<ul class="sidebar-dropdown">
	                                    		
		                                        	<li>
		                                            	<ul>
		                                            		{{foreach from=$item.parent_cate item=item2}}
																<li><a href="{{$item2.url}}">{{$item2.name}}</a></li>
															{{/foreach}}
		                                                </ul>
		                                            </li>
	                                       	 	</ul>
	                                       	 
		                                   	{{/if}}
	                                    </li>
										{{/foreach}}
								</ul>
							</div>
							
						</div>
							
					</div>
					<!-- /Categories -->
					
					
					
					<!-- Specials -->
					<div class="row products-row sidebar-box orange">
						 
						<div class="col-lg-12 col-md-12 col-sm-12">
							
							<!-- Carousel Heading -->
							<div class="carousel-heading no-margin">
								
								<h4><i class="icons icon-magic"></i> {{l}}Sản Phẩm{{/l}}</h4>
								<div class="carousel-arrows">
									<i class="icons icon-left-dir"></i>
									<i class="icons icon-right-dir"></i>
								</div>
								
							</div>
							<!-- /Carousel Heading -->
							
						</div>
						
						<!-- Carousel -->
						<div class="carousel owl-carousel-wrap col-lg-12 col-md-12 col-sm-12">
							
							<div class="owl-carousel" data-max-items="1">
									
								<!-- Slide -->
								{{foreach from=$allProduct item=item}}
								{{if $item.tinnhanh == 1}}
								<div>
									<!-- Carousel Item -->
									<div class="product">
										
										<div class="product-image">
											<img src="{{$BASE_URL}}{{$item.main_image}}" alt="Product1">
										</div>
										
										<div class="product-info">
											<h5><a href="products_page_v1.html">{{$item.title}}</a></h5>
											<span class="price">
											{{if $item.sale == 1}}
												{{$item.gia_sale}} {{$item.tiente}}
											{{else}}
												{{$item.price}} {{$item.tiente}}
											{{/if}}
											</span>
										</div>
										
										<div class="product-actions">
											<span class="add-to-cart">
												<span class="action-wrapper">
													<i class="icons icon-basket-2"></i>
													<a href="{{$item.url}}"><span class="action-name">{{l}}Xem Thêm{{/l}}</span></a>
												</span >
											</span>
										</div>
										
									</div>
									<!-- /Carousel Item -->
								</div>
								{{/if}}
								{{/foreach}}
								<!-- /Slide -->
								
								
								
							</div>
						
						</div>
						<!-- / Carousel -->
						
						
					</div>
					<!-- /Specials -->
					
					<!-- Bestsellers -->
					<div class="row sidebar-box red">
						
						<div class="col-lg-12 col-md-12 col-sm-12">
							
							<div class="sidebar-box-heading">
                            <i class="icons icon-award-2"></i>
								<h4>{{l}}Tin Nhanh{{/l}}</h4>
							</div>
							
							<div class="sidebar-box-content">
								<table class="bestsellers-table">
									{{foreach from=$tinnhanh_content item=item}}
									<tr>
										<td class="product-thumbnail"><a href="{{$item.url}}"><img src="{{$BASE_URL}}{{$item.main_image}}" alt="Product1"></a></td>
										<td class="product-info">
											<p><a href="{{$item.url}}">{{$item.title}}</a></p>
										</td>
									</tr>
									{{/foreach}}
									
								</table>
							</div>
							
						</div>
						
					</div>
					<!-- /Bestsellers -->
					
				</aside>
				<!-- /Sidebar -->
				
