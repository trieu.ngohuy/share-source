<?php
require_once 'Zend/Config.php';
require_once 'Zend/Config/Writer/Array.php';
require_once 'modules/commercial/models/Commercial.php';

class front_default_commercial_2Sticker extends Nine_Sticker
{
    public function run()
    {
        $objCommercial = new Models_Commercial();
        $commercial = $objCommercial->getAllEnabled('created_date DESC', 2);

        foreach ($commercial as $item) {
            $item['images'] = Nine_Function::getThumbImage ( @$item['images'], 270, 200 , false , false, true );
        }

        $this->view->commercial = $commercial;
    }
}