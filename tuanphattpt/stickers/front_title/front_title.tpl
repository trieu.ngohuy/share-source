<header class="ry-section ry-header-slider">
        <div class="ry-home-slider-wrapper ry-color-dark-1">
          <div class="ry-home-slider flexslider">
            <ul class="slides">
            
            	{{foreach from=$allBanner item=item key=key}}
					{{if $key%2 ==0}}
						<li class="ry-home-slider-item">
			                <img alt="" src="{{$BASE_URL}}{{$item.main_image}}" style="width: 100%;position: absolute;">
			                <div class="container-fullsize">
			                  <div class="ry-caption-wrapper ry-cell-vertical-wrapper">
			                    <div class="ry-cell-middle">
			                      <div class="ry-caption-inner row">
			                        <div class="col-xlg-12 col-lg-6">
			                          <h1 class="ry-caption ">{{$item.title}}</h1>
			                          <p class="ry-caption  hidden-xs" style="font-size: 18px;">{{$item.full_text}}</p><a href="#" class="ry-btn-1 ry-caption">{{l}}VIEW MORE{{/l}}</a>
			                        </div>
			                      </div>
			                    </div>
			                  </div>
			                </div>
			                
			              </li>
					{{else}}
						<li class="ry-home-slider-item">
			                <img alt="" src="{{$BASE_URL}}{{$item.main_image}}" style="width: 100%;position: absolute;">
			                <div class="container-fullsize">
			                  <div class="ry-caption-wrapper ry-cell-vertical-wrapper">
			                    <div class="ry-cell-middle">
			                      <div class="ry-caption-inner row">
			                        <div class="col-xlg-4 col-xlg-offset-8 col-lg-5 col-lg-offset-7">
			                          <h1 class="ry-caption ">{{$item.title}}</h1>
			                          <p class="ry-caption  hidden-xs" style="font-size: 18px;">{{$item.full_text}}</p><a href="#" class="ry-btn-1 ry-caption">{{l}}VIEW MORE{{/l}}</a>
			                        </div>
			                      </div>
			                    </div>
			                  </div>
			                </div>
			                
			                
			              </li>
			              
					
					{{/if}}
				{{/foreach}}
									
              
              
              
              
              
              
            </ul>
          </div>
        </div>
      </header>