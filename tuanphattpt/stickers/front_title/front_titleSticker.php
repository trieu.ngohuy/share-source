<?php

require_once 'modules/content/models/Content.php';
require_once 'modules/content/models/ContentCategory.php';

class front_titleSticker extends Nine_Sticker
{
	public function run()
	{
		$objContent = new Models_Content ();
		$objContentCategory = new Models_ContentCategory ();
		
		$allBanner = $objContent->getAllEnabledContentByCategory ( 1, array (), array ('sorting ASC', 'content_id DESC' ));
		
		foreach ( $allBanner as &$content ) {
			$content ['title'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $content ['title'] ) ), 30 );
			$content ['intro_text'] = Nine_Function::subStringAtBlank ( trim ( strip_tags ( $content ['full_text'] ) ), 60 );
			if($content ['alias'] == ""){
				$content ['alias'] = $objContent->convert_vi_to_en($content ['alias']);
				$content ['alias'] = str_replace(" ", "-", str_replace("&*#39;","",trim($content ['alias'])));
			}
			$content ['url'] = Nine_Route::_ ( "content/index/detail/gid/{$content['content_gid']}", array ('alias' => $content ['alias'] ) );
			$tmp = explode ( '||', $content ['images'] );
			$content ['main_image'] = Nine_Function::getThumbImage ( @$tmp [0], 1140, 522 ,false,true);
		}
		unset ( $content );
		/**
		 * Assign to view
		 */
		$this->view->allBanner = $allBanner;
//		echo '<pre>';
//            echo print_r($allBanner);
//            echo '</pre>';die;
		
	}
}