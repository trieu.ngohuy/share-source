<div class="ctaMobileModal animated">
    <div class="ctaMobileModalWrap"> <button type="button" class="close" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        <div class="ctaMobileModalHeader text-center">
            <h4 class="ctaMobileHeadline">{{l}}Hãy để chúng tôi giúp bạn bắt đầu với sản phẩm đèn LED{{/l}}</h4>
            <p>{{l}}Gọi ngay để được tư vấn miễn phí{{/l}}:</p>
        </div>
        <div class="ctaMobileModalBody">
            <div class="row">
                {{$config.Contacts}}
            </div>
        </div>
        <div class="ctaMobileModalFooter">
            <hr> 
            <img alt="Đảm bảo" class="cp-icon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAXCAMAAAA8w5+RAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABWVBMVEUAAADMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMwAAABh0liMAAAAcXRSTlMAJ6vo6rMzIaTxw0xGuvOvKku1yTgrwL1UA0m04+mnNy6f5+W8U/nBWAlRsv3TIwjPzVkTQtDEchQ5X8yVztiFv47GB7md7m02b1X1R5c6sDvKhIMb8l169Ot8LASC7WSj301gFnQgLZSZSIyNijHkMlLCBNYAAAABYktHRACIBR1IAAAACXBIWXMAAAuIAAALiAHljilJAAABBUlEQVQY02NggABGJmYWVjYGZMDOwcnFzcPLxy8AFxIU4hQWATFExTjFJSSBDClpGVk5eZi8gqKSsooqg5q6hiaySZJa2joMunoMaEDfgMHQCIlvbAwkTEwZzMwRYhaFlkDSypqB1QYhaGsHIu0dGByd4GIShc4gysWVgccNxHD3ABKFnmA5Fi8GSW8fIMO30I/BXxksFqCmz8AQyA9iBhUGF4aABZlMgYRPKJgdVhgOMTlCCkRGRoE50TFgKjYOTMUXIvunMAxCsyQgxFgTYaFZmAQTSy5MgTFTC9MgjPTCZISmjEJwWEUXBiMbn1mYBYyBwmzUQEwtzMmFehMJ5OXLFsDYAIBlJWU4O27rAAAAAElFTkSuQmCC">
            <span>{{l}}Tư vấn miễn phí 24/7.{{/l}}</span>
        </div>
    </div>
</div>
<section class="contact-info" id="lienhe">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{l}}Bạn quan tâm? Liên hệ ngay!{{/l}}</h3>
            </div>
            <div class="col-md-4 lets-talk">
                <p class="headline">{{l}}Trò chuyện với Chuyên Gia{{/l}}</p>
                <p>{{l}}Hãy để chúng tôi giúp đỡ bạn bắt đầu với sản phẩm đèn LED chiếu sáng.{{/l}}</p>
                <ul>
                    <li><a href="tel:+84912122016"><i class="fa fa-phone-square" aria-hidden="true"
                                                      style="font-size: 20px;"></i><strong>Hotline: {{$config.hotline}}</strong></a>
                    </li>
                    <li><a href="tel:+842837269399"><i class="fa fa-phone-square" aria-hidden="true"
                                                       style="font-size: 20px;"></i><strong>Phone: {{$config.phone_call}}</strong></a></li>
                    <li><a href="mailto:hieu.tu@tuanphat.com.vn"><i class="fa fa-envelope"
                                                                    aria-hidden="true"></i><strong>Email: {{$config.Email}}</strong></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-5 lets-talk" style="text-align: center;">
                {{if $currentLangCode === 'en'}}
                {{$config.contact_detail_en}}
                {{else}}
                {{$config.contact_detail}}
                {{/if}}
            </div>
            <style>
                .lets-talk h2{
                    color: #ffffff !important;
                }
            </style>
            <div class="col-md-3 lets-talk" style="border-left:1px solid #ffffff;">
                <p class="headline">{{l}}Đăng ký nhận tin tức{{/l}}</p>
                <div id="divNewsLetter">
                    <input type="email" id="txtEmail" placeholder="{{l}}Nhập email của bạn{{/l}}"> 
                    <a id="btnSubmitNewsLetter" href="#" class="cta">Ok</a>
                </div>
                <p style="margin-bottom: 5px;">{{l}}Mạng xã hội{{/l}}</p>
                <div id="divSocial">
                    <a class="_blank" href="{{$config.facebook}}" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a class="_blank" href="{{$config.youtube}}ECHodnMdyWuExw" target="_blank"><i class="fa fa-youtube"></i></a>
                    <a class="_blank" href="{{$config.google}}" target="_blank"><i class="fa fa-google-plus"></i></a>
                </div>
            </div>
        </div>
</section>
<footer class="content-info">
    <div class="container"></div>
    <div class="container copyright">
        <div class="row">
            <div class="col-md-12">
                {{$config.status_footer}}
                </p>
            </div>
        </div>
    </div>
</footer>

<link type="text/css" media="all" href="{{$LAYOUT_HELPER_URL}}front/css/custom.css" rel="stylesheet"/>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<!--Sweet alert-->
<link rel="stylesheet" type="text/css" href="{{$LAYOUT_HELPER_URL}}libs/sweetalert/sweetalert2.css"/>
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}libs/sweetalert/sweetalert2.js"></script>

<!--Common-->
<script type="text/javascript" src="{{$LAYOUT_HELPER_URL}}libs/common/js/common.js"></script>

<script>
    $(document).ready(function ($) {
        

        //Newsletter submit
        $('#btnSubmitNewsLetter').click(function () {
            //Check valid
            if ($('#txtEmail').val() === '') {
                swal("{{l}}Error{{/l}}", "{{l}}Please type email before submit.{{/l}}", "error");
                            } else {
                                //Submit
                                HandleAjaxCall("{{$BASE_URL}}newlater/admin/save", {data: {email: $('#txtEmail').val()}},
                                        {
                                            success: {
                                                title: '{{l}}Success{{/l}}',
                                                                                content: '{{l}}Submit news letter successful!{{/l}}'
                                                                                                            },
                                                                                                            error: {
                                                                                                                title: '{{l}}Error{{/l}}',
                                                                                                                                                content: '{{l}}Submit news letter failure!{{/l}}'
                                                                                                                                                                            }
                                                                                                                                                                        }, {{$arrConfig.debug_mode}});
                                                                                                                                                            }
                                                                                                                                                        });
                                                                                                                                                        $('head').append('<link rel="shortcut icon" href="/tuanphat/layouts/default/helpers/front/images/favicon.ico">');
                                                                                                                                                    });
</script>
<!--Popup-->
{{if isset($hidePopup) === true}}
{{sticker name=front_popup}}
{{/if}}