<?php

class front_footerSticker extends Nine_Sticker
{
	public function run()
    {
        $this->view->contact = Nine_Route::_('contact/index/index');
        //Get config
        $config = Nine_Common::getConfig($this->view);
        $this->view->config = $config;

        //Get current language code
        $this->view->currentLangCode = Nine_Language::getCurrentLangCode();
        //Get list contact
        //Nine_Common::GetSettingContacts($this->view, $config['Contacts']);
    }
}