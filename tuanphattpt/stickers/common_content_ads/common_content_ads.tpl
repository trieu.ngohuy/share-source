<div class="container ads">
    <div class="service ">
        {{foreach from=$listContentAds item=listContentAdsItem}}
        <div class="col-xs-6 col-sm-3 service-item">
            <div class="icon">
                <img alt="services" 
                     src="{{$listContentAdsItem.main_image}}">
            </div>
            <div class="info">
                <a href="{{$listContentAdsItem.url}}">
                    <h3>{{$listContentAdsItem.title}}</h3>
                </a> 
                <span>{{$listContentAdsItem.seo_title}}</span>
            </div>
        </div>
        {{/foreach}}
    </div>
</div>