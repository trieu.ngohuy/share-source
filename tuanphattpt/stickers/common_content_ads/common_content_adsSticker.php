<?php

class common_content_adsSticker extends Nine_Sticker
{
	public function run()
	{
		/*
                 * Get list ads contents
                 */
            $listContentAds = Nine_Query::getListContentsByCatAlias('home-ads', 'home-ads', 4, null);
            
            /*
             * Create intro text
             */
            foreach ($listContentAds as &$value) {
             $value['intro_text'] = mb_substr($value['full_text'], 0, 18);
            }
            unset($value);
            $this->view->listContentAds = $listContentAds;
	}
}