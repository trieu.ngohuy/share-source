<div class="panel panel-default">
    <div class="panel-heading">
        <ul class="row">
            <li class="{{if $menu=='profile'}}active{{/if}} col-xs-6 col-sm-2 col-md-2"><a href="{{$BASE_URL}}user/profile">{{l}}Overview{{/l}}</a></li>
            <li class="{{if $menu=='edit-info'}}active{{/if}} col-xs-6 col-sm-2 col-md-2"><a href="{{$BASE_URL}}user/edit-info">{{l}}Edit Info{{/l}}</a></li>
            <li class="{{if $menu=='cart-manager'}}active{{/if}} col-xs-6 col-sm-2 col-md-2"><a href="{{$BASE_URL}}user/cart-manager">{{l}}Cart manager{{/l}}</a></li>
            <li class="{{if $menu=='change-password'}}active{{/if}} col-xs-6 col-sm-2 col-md-2"><a href="{{$BASE_URL}}user/change-password">{{l}}Change Password{{/l}}</a></li>
            <li class="{{if $menu=='buying-request'}}active{{/if}} col-xs-6 col-sm-2 col-md-2"><a href="{{$BASE_URL}}user/buying-request">{{l}}Buying Request{{/l}}({{$countNewBuying}})</a></li>
            <li class="{{if $menu=='buying-request-reply'}}active{{/if}} col-xs-6 col-sm-2 col-md-2"><a href="{{$BASE_URL}}user/buying-request-reply">{{l}}Buying Request Reply{{/l}}</a></li>
            <li class="{{if $menu=='contact'}}active{{/if}} col-xs-6 col-sm-2 col-md-2"><a href="{{$BASE_URL}}user/contact">{{l}}Contact Mainpage {{/l}} ({{$countNewContact}})</a></li>
<!--                    <li {{if $menu=='contact-estore'}}class="active"{{/if}}><a href="{{$BASE_URL}}user/contact-estore">{{l}}Contact Estore{{/l}} ({{$countNewContactEstore}})</a></li>-->
            {{if $group_id != 2}}
            <li class="{{if $menu=='upgrade'}}active{{/if}} col-xs-6 col-sm-2 col-md-2"><a href="{{$BASE_URL}}user/upgrade">{{l}}Upgrade Account{{/l}}</a></li>
                {{else}}
            <li class="{{if $menu=='bank'}}active{{/if}} col-xs-6 col-sm-2 col-md-2"><a href="{{$BASE_URL}}bank/manager">{{l}}Kết Nối Tài Chính{{/l}} ({{$countconnect}})</a></li>
                {{/if}}
            <li class="{{if $menu=='manager-upgrade'}}active{{/if}} col-xs-6 col-sm-2 col-md-2"><a href="{{$BASE_URL}}user/manager-upgrade">{{l}}Payment history{{/l}}</a></li>
        </ul>
    </div>
</div>