<?php

require_once 'Zend/Config.php';
require_once 'Zend/Config/Writer/Array.php';
require_once 'modules/contact/models/Contact.php';
require_once 'modules/message/models/MessageCategory.php';
require_once 'modules/bank/models/ConnectBank.php';

require_once 'modules/buying/models/Buying.php';
require_once 'modules/buying/models/BuyingCategory.php';

class front_user_menuSticker extends Nine_Sticker
{
    public function run()
    {
    	$objContact = new Models_Contact();
		$allNewContact = $objContact->getByColumns(array('user_view = ?' => 0, 'parent_id = ?' => 0,'user_id = ?'=> Nine_Registry::getLoggedInUserId()))->toArray();
		$this->view->countNewContact = count($allNewContact);
		$user = Nine_Registry::getLoggedInUser();
		$this->view->group_id = $user['group_id'];
		$objCategory     = new Models_MessageCategory();
		$condition = array();
		$condition['user_from_id'] = Nine_Registry::getLoggedInUserId();
        $condition['parent_id'] = 0;
        $condition['user_view'] = 0;
        $allCategories  = $objCategory->setAllLanguages(true)->getAllCategories($condition);
        
        $this->view->countNewContactEstore = count($allCategories);
        
        $objBankConnect = new Models_ConnectBank();
        $allConnect = $objBankConnect->getByColumns(array('view = ?' => 0))->toArray();
        $this->view->countconnect = count($allConnect);
        
        $objCategory     = new Models_BuyingCategory();
        $condition = array();
        $condition['user_id'] = Nine_Registry::getLoggedInUserId();
        $condition['view'] = 1;
        $allCategories  = $objCategory->getAllEnabledCategory($condition);
        $this->view->countNewBuying = count($allCategories);
    }
}