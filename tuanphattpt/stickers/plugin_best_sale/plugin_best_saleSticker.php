<?php

class plugin_best_saleSticker extends Nine_Sticker {

    public function run() {
        //Get list best sale
        $listBestSale = Nine_Query::getListProducts(array(
                    'sale' => 1,
                    'lang_id' => Nine_Language::getCurrentLangId()
                        ), null, 5, null);
        //Edit name
        $limit = 30;
        foreach ($listBestSale as &$value) {
            if(strlen($value['title']) < $limit){
                $value['title'] = $value['title'].str_repeat(". ",$limit - strlen($value['title']));
            }else{
                $value['title'] = mb_substr($value['title'], 0, $limit)."...";
            }
        }
        unset($value);
        $this->view->listBestSale = $listBestSale;
        //get config
        $this->view->config = Nine_Query::getConfig();
    }

}
