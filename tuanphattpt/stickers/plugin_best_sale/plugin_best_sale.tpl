<!-- block best sellers -->
<div class="block left-module">
    <p class="title_block">{{l}}BEST SALE{{/l}}</p>
    <div class="block_content">
        <ul class="products-block">
            {{foreach from=$listBestSale item=item}}
            <li>
                <div class="products-block-left">
                    <a href="{{$item.url}}">
                        <img src="{{$item.main_image}}" alt="SPECIAL PRODUCTS">
                    </a>
                </div>
                <div class="products-block-right">
                    <p class="product-name">
                        <a href="{{$item.url}}">{{$item.title}}</a>
                    </p>
                    <p class="product-price">{{if $item.price_sale}}{{$item.price_sale}}{{else}}{{$item.price}}{{/if}} {{$config.currency}}</p>
                    
                </div>
            </li>
            {{/foreach}}
        </ul>
    </div>
</div>
<!-- ./block best sellers  -->