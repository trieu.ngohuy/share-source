
<!--				<div class="logo-area clearfix">-->
<!--						<a href="#" class="logo"></a>-->
<!--				</div>-->
<!--				-->
<!--				<div class="tools-bar">-->
<!--						<ul class="nav navbar-nav nav-main-xs">-->
<!--								<li><a href="#menu" class="icon-toolsbar" id="header_menu"><i class="fa fa-bars"></i></a></li>-->
<!--						</ul>-->
<!--						<ul class="nav navbar-nav navbar-right tooltip-area">-->
<!--								<li class="dropdown">-->
<!--										<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">-->
<!--											<em><strong>Hi</strong>, {{$loggedUser.username}} </em> <i class="dropdown-icon fa fa-angle-down"></i>-->
<!--										</a>-->
<!--										<ul class="dropdown-menu pull-right icon-right arrow">-->
<!--												<li class="divider"></li>-->
<!--												<li><a href="{{$BASE_URL}}admin/access/admin/logout"><i class="fa fa-sign-out"></i> {{l}}Signout{{/l}} </a></li>-->
<!--										</ul>-->
<!--								</li>-->
<!--								<li class="visible-lg">-->
<!--									<a href="#" class="h-seperate fullscreen" data-toggle="tooltip" title="Full Screen" data-container="body"  data-placement="left">-->
<!--										<i class="fa fa-expand"></i>-->
<!--									</a>-->
<!--								</li>-->
<!--						</ul>-->
<!--				</div>-->
				
				
				
				
				
				
				
				
				
				
				
		<div id="navbar" class="navbar navbar-default navbar-collapse h-navbar navbar-fixed-top"">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<!-- #section:basics/sidebar.mobile.toggle -->
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>
				<div class="navbar-header pull-left">
					<!-- #section:basics/navbar.layout.brand -->
					<a href="{{$BASE_URL}}admin" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i>
							{{l}}CMS MANAGER WEBSITE GROUP WEBSITE BUSSINESS{{/l}}
						</small>
					</a>

				</div>

				<div class="navbar-buttons navbar-header pull-right  collapse navbar-collapse" role="navigation">
					<ul class="nav ace-nav">

						<li class="light-blue user-min">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								{{if $loggedUser.image != null}}
									<img class="nav-user-photo" src="{{$BASE_URL}}{{$loggedUser.image}}" alt="{{$loggedUser.full_name}}" />
								{{else}}
									<img class="nav-user-photo" src="{{$LAYOUT_HELPER_URL}}admin/assets/avatars/user.jpg" alt="Jason's Photo" />
								{{/if}}

								<span class="user-info">
									<small>{{l}}Welcome{{/l}},</small>
									{{$loggedUser.username}}
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="{{$BASE_URL}}">
										<i class="ace-icon fa fa-eye"></i>
										{{l}}View Website{{/l}}
									</a>
								</li>


								<li class="divider"></li>

								<li>
									<a href="{{$BASE_URL}}admin/access/admin/logout">
										<i class="ace-icon fa fa-power-off"></i>
										{{l}}Logout{{/l}}
									</a>
								</li>
							</ul>
						</li>

						<!-- /section:basics/navbar.user_menu -->
					</ul>
				</div>

				<!-- /section:basics/navbar.dropdown -->
			</div><!-- /.navbar-container -->
		</div>