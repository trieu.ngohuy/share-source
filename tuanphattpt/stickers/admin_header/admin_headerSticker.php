<?php
require_once 'modules/language/models/Lang.php';
class admin_headerSticker extends Nine_Sticker
{
	public function run()
	{
	    $langCode = Nine_Registry::get('langCode');
	    
	    $this->view->user = Nine_Registry::getLoggedInUser()->toArray();
	    $this->view->loggedUser= Nine_Registry::getLoggedInUser()->toArray();
	    
	    $objLang  = new Models_Lang();
		$allLangs = $objLang->getByColumns(array('enabled = ?' => 1), array('sorting ASC'))->toArray();
        $this->view->allLangs = $allLangs;
	}
}