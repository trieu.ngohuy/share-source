/*
 Navicat MySQL Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 100121
 Source Host           : localhost:3306
 Source Schema         : lightnovelsynthetic

 Target Server Type    : MySQL
 Target Server Version : 100121
 File Encoding         : 65001

 Date: 17/12/2018 17:27:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for zl_banner
-- ----------------------------
DROP TABLE IF EXISTS `zl_banner`;
CREATE TABLE `zl_banner`  (
  `banner_id` int(11) NOT NULL,
  `banner_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `module` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `controller` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `function` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `params` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `link` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `genabled` tinyint(1) NOT NULL,
  `sorting` int(11) NOT NULL,
  `created_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`banner_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for zl_comment
-- ----------------------------
DROP TABLE IF EXISTS `zl_comment`;
CREATE TABLE `zl_comment`  (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_episodes_id` int(11) NOT NULL,
  `full_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`comment_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zl_comment
-- ----------------------------
INSERT INTO `zl_comment` VALUES (1, 5, 0, 1, 0, 'First comment 3', 0, 1544569200);
INSERT INTO `zl_comment` VALUES (2, 5, 1, 1, 0, 'Second comment', 0, 1544569200);
INSERT INTO `zl_comment` VALUES (3, 5, 1, 1, 0, 'Third comment 2', 0, 1544569200);
INSERT INTO `zl_comment` VALUES (5, 5, 0, 1, 4, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.', 0, 1544569200);
INSERT INTO `zl_comment` VALUES (7, 5, 0, 1, 0, ' this is new comment', 0, 1544569200);
INSERT INTO `zl_comment` VALUES (8, 5, 1, 1, 0, ' this new new reply comment', 0, 1544914800);
INSERT INTO `zl_comment` VALUES (9, 5, 1, 1, 0, ' This is second reply', 0, 1544914800);
INSERT INTO `zl_comment` VALUES (10, 5, 5, 1, 4, ' New reply', 0, 1544914800);
INSERT INTO `zl_comment` VALUES (11, 5, 0, 1, 0, ' Haha', 0, 1544914800);
INSERT INTO `zl_comment` VALUES (12, 0, 0, 1, 1, ' New comment', 0, 1544914800);

-- ----------------------------
-- Table structure for zl_content
-- ----------------------------
DROP TABLE IF EXISTS `zl_content`;
CREATE TABLE `zl_content`  (
  `content_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_gid` int(11) NOT NULL,
  `content_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title_en` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title_ja` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title_romaji` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `intro_text` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `full_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `list_content_category_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `list_project_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `images` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logo` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sorting` int(11) NOT NULL,
  `participator` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `article` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `language` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `progress` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `translator` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content_state_id` int(11) NULL DEFAULT NULL,
  `view` int(11) NULL DEFAULT 0,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `genabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` int(11) NOT NULL,
  `is_display_in_home` tinyint(1) NULL DEFAULT 0,
  `is_adult` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`content_id`) USING BTREE,
  UNIQUE INDEX `alias`(`alias`) USING BTREE,
  UNIQUE INDEX `title`(`title`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zl_content
-- ----------------------------
INSERT INTO `zl_content` VALUES (1, 0, 0, 0, '달빛 조각사 - LEGENDARY MOONLIGHT SCULPTOR', '1', '2', '3', 'legendary-moonlight-sculptor', '', '<p>b</p>\\n\\n<p>c</p>\\n', '1||5', '3', 'assets/upload/users/admin/Legendary_Moonlight1.jpg', 'assets/upload/users/admin/Legendary_Moonlight.jpg', 0, 'DEO CAN TEN', 'DEO CAN TEN', '남희성', 'Tiếng Hàn', '', 'Đéo Cần Tên (ĐCT)', '', 'Light Novel', 4, 141, 1, 1, 0, 0, 0);
INSERT INTO `zl_content` VALUES (5, 0, 0, 0, 'AKU NO SOSHIKI NO KYUUJINKOUKOKU', 'The Evil Organization’s Recruitment Ad', '', '', 'aku-no-soshiki-no-kyuujinkoukoku', '', '<p>Komori Neito quyết t&acirc;m rời bỏ lối sống l&agrave;m NEET của m&igrave;nh v&agrave; cố xin việc l&agrave;m.Cậu t&igrave;m thấy một mẩu quảng c&aacute;o tuyển dụng cho một tổ chức t&agrave; &aacute;c h&agrave;ng đầu, lu&ocirc;n chiến đấu chống lại c&aacute;c si&ecirc;u anh h&ugrave;ng. V&agrave; thế l&agrave; cậu quyết định đi phỏng vấn xin việc. Kh&ocirc;ng c&oacute; kỹ năng n&agrave;o kh&aacute;c ngo&agrave;i c&aacute;ch nh&igrave;n nhận đ&uacute;ng đắn về thế giới t&agrave;n nhẫn n&agrave;y. Komori trở th&agrave;nh một tay sai cấp thấp v&agrave; bắt đầu l&agrave;m việc theo c&aacute;ch ri&ecirc;ng của m&igrave;nh để thăng tiến trong tổ chức Metallica, tổ chức t&agrave; &aacute;c lớn nhất thế giới.Bản dịch thuộc về nh&oacute;m dịch Shinigami-TeamNghi&ecirc;m cấm sao ch&eacute;p dưới mọi h&igrave;nh thức</p>\\n\\n<p>hah</p>\\n', '2||5', '1||3', 'assets/upload/users/admin/aff4.png', 'assets/upload/users/admin/aff.png', 0, 'Ms Lich', 'Ms Lich', 'Kotatsu', 'Tiếng Anh', 'Bất ổn', '', '', 'Web Novel', 3, 56, 1, 1, 1544482800, 0, 1);
INSERT INTO `zl_content` VALUES (6, 0, 0, 0, '오크지만 찬양해! - VINH DANH LOÀI ORC!', '', NULL, NULL, '-vinh-danh-loai-orc-', '', 'Tại một thế giới nơi mà sức mạnh, quyền lực và tiền bạc trở thành những phương tiện chính để người ta theo đuổi.\\\\n\\\\nỞ một thế giới, bị sự tham lam và ích kỷ làm cho ô uế.\\\\n\\\\nTrong một thế giới nơi mà danh dự, công lý, niềm tin cùng các giá trị tốt đẹp khác đã đi vào quên lãng.\\\\n\\\\n------------------------\\\\n\\\\nMột chiến binh Orc xuất hiện, chứng tỏ điều ngược lại.', '2||5', '1||4', 'assets/upload/users/admin/xxlarge23.png', 'assets/upload/users/admin/xxlarge22.png', 0, 'Roan', 'Roan', 'N/A', 'Tiếng Hàn', '1-3 chapter/ngày. (Trừ Thứ Bảy và Chủ Nhật, vì không chắc sẽ ra)', 'Roan', '', 'Light Novel', 3, 0, 1, 1, 1544914800, 0, 0);

-- ----------------------------
-- Table structure for zl_content_category
-- ----------------------------
DROP TABLE IF EXISTS `zl_content_category`;
CREATE TABLE `zl_content_category`  (
  `content_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `intro_text` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `images` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logo` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `genableed` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`content_category_id`) USING BTREE,
  UNIQUE INDEX `alias`(`alias`) USING BTREE,
  UNIQUE INDEX `title`(`title`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zl_content_category
-- ----------------------------
INSERT INTO `zl_content_category` VALUES (1, 0, 0, 'Action', 'action', '', '', '', 0, 1, 1, '1');
INSERT INTO `zl_content_category` VALUES (2, 0, 0, 'Adult', 'adult', '', '', '', 0, 1, 1, '1544451128');
INSERT INTO `zl_content_category` VALUES (5, 0, 0, 'Adventure', 'adventure', '', '', '', 0, 1, 1, '1544451129');

-- ----------------------------
-- Table structure for zl_content_episodes
-- ----------------------------
DROP TABLE IF EXISTS `zl_content_episodes`;
CREATE TABLE `zl_content_episodes`  (
  `content_episodes_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_episodes_gid` int(11) NOT NULL,
  `content_season_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `intro_text` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `full_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `images` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logo` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `genableed` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`content_episodes_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zl_content_episodes
-- ----------------------------
INSERT INTO `zl_content_episodes` VALUES (1, 0, 6, 0, 'Chương 1', 'chuong-1', '', 'This is content', '', '', 0, 1, 1, 1);
INSERT INTO `zl_content_episodes` VALUES (2, 0, 4, 0, 'Chương 2', 'chuong-2-2', '', '', '', '', 0, 1, 1, 1544569200);
INSERT INTO `zl_content_episodes` VALUES (4, 0, 6, 0, 'Chương2', 'chuong-2-1', '', '<p>In theory, you regular express does work but the problem is that not all operating system and browsers send only \\\\n at the end of string. Many will also send a \\\\r.</p>\\n\\n<p>Try:</p>\\n\\n<p><strong>Edit:</strong>&nbsp;I&#39;ve simplified this one:</p>\\n\\n<pre>\\n<code>preg_replace(&quot;/(\\\\r?\\\\n){2,}/&quot;, &quot;\\\\n\\\\n&quot;, $text);</code></pre>\\n\\n<p><strong>Edit:</strong>&nbsp;And to address the problem of some sending \\\\r only:</p>\\n\\n<pre>\\n<code>preg_replace(&quot;/[\\\\r\\\\n]{2,}/&quot;, &quot;\\\\n\\\\n&quot;, $text);</code></pre>\\n\\n<p><strong>Update 1:</strong>&nbsp;Based on your update:</p>\\n\\n<pre>\\n<code>// Replace multiple (one ore more) line breaks with a single one.\\n$text = preg_replace(&quot;/[\\\\r\\\\n]+/&quot;, &quot;\\\\n&quot;, $text);\\n\\n$text = wordwrap($text,120, &#39;&lt;br/&gt;&#39;, true);\\n$text = nl2br($text);</code></pre>\\n', '', '', 0, 1, 1, 1544569200);

-- ----------------------------
-- Table structure for zl_content_season
-- ----------------------------
DROP TABLE IF EXISTS `zl_content_season`;
CREATE TABLE `zl_content_season`  (
  `content_season_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_season_gid` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `intro_text` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `full_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `images` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logo` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `genableed` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`content_season_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zl_content_season
-- ----------------------------
INSERT INTO `zl_content_season` VALUES (1, 0, 1, 0, 'Tập 1', 'tap-1', '', '', '', 'assets/upload/users/admin/avatar2.png', 0, 1, 1, 1);
INSERT INTO `zl_content_season` VALUES (4, 0, 1, 0, 'Tập 2', 'tap-2', '', '', '', 'assets/upload/users/admin/photo41.jpg', 0, 1, 1, 1544569200);
INSERT INTO `zl_content_season` VALUES (6, 0, 5, 0, 'Tập 1', 'tap-1', '', '', '', 'assets/upload/users/admin/avatar4.png', 0, 1, 1, 1544569200);

-- ----------------------------
-- Table structure for zl_content_state
-- ----------------------------
DROP TABLE IF EXISTS `zl_content_state`;
CREATE TABLE `zl_content_state`  (
  `content_state_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_state_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT 1,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `alias` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`content_state_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zl_content_state
-- ----------------------------
INSERT INTO `zl_content_state` VALUES (3, 0, 1, 'Đang tiến hành', 'dang-tien-hanh', 0, 1544482800);
INSERT INTO `zl_content_state` VALUES (4, 0, 1, 'Đã hoàn thành', 'da-hoan-thanh', 0, 1544482800);

-- ----------------------------
-- Table structure for zl_estore
-- ----------------------------
DROP TABLE IF EXISTS `zl_estore`;
CREATE TABLE `zl_estore`  (
  `estore_id` int(11) NOT NULL,
  `estore_gid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `alias` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logo` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `favicon` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `genabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`estore_id`) USING BTREE,
  UNIQUE INDEX `name_alias`(`name`, `alias`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for zl_lang
-- ----------------------------
DROP TABLE IF EXISTS `zl_lang`;
CREATE TABLE `zl_lang`  (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logo` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT 0,
  `created_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`lang_id`) USING BTREE,
  UNIQUE INDEX `code_title`(`code`, `title`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zl_lang
-- ----------------------------
INSERT INTO `zl_lang` VALUES (1, 'vi', 'Tiếng Việt', 'vi.png', 1, 1, '');
INSERT INTO `zl_lang` VALUES (2, 'en', 'English', 'en.png', 1, 0, '');

-- ----------------------------
-- Table structure for zl_menu
-- ----------------------------
DROP TABLE IF EXISTS `zl_menu`;
CREATE TABLE `zl_menu`  (
  `menu_id` int(11) NOT NULL,
  `menu_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `module` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `controller` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `function` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `params` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `link` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `genabled` tinyint(1) NOT NULL,
  `sorting` int(11) NOT NULL,
  `created_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for zl_product
-- ----------------------------
DROP TABLE IF EXISTS `zl_product`;
CREATE TABLE `zl_product`  (
  `product_id` int(11) NOT NULL,
  `product_gid` int(11) NOT NULL,
  `product_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `intro_text` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `full_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` int(11) NOT NULL,
  `is_sale` tinyint(1) NOT NULL,
  `price_sale` int(11) NOT NULL,
  `images` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logo` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `genableed` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`product_id`) USING BTREE,
  UNIQUE INDEX `alias`(`alias`) USING BTREE,
  UNIQUE INDEX `title`(`title`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for zl_product_category
-- ----------------------------
DROP TABLE IF EXISTS `zl_product_category`;
CREATE TABLE `zl_product_category`  (
  `product_category_id` int(11) NOT NULL,
  `product_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `intro_text` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `images` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logo` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `genableed` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`product_category_id`) USING BTREE,
  UNIQUE INDEX `alias`(`alias`) USING BTREE,
  UNIQUE INDEX `title`(`title`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for zl_project
-- ----------------------------
DROP TABLE IF EXISTS `zl_project`;
CREATE TABLE `zl_project`  (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_gid` int(11) NOT NULL,
  `parent_gid` int(11) NOT NULL DEFAULT 0,
  `lang_id` int(11) NOT NULL DEFAULT 1,
  `alias` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`project_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zl_project
-- ----------------------------
INSERT INTO `zl_project` VALUES (1, 1, 0, 1, 'tieu-thuyet', 'Tiêu thuyết', 0, 1);
INSERT INTO `zl_project` VALUES (3, 0, 0, 1, 'tieu-thuyet-tu-sang-tac', 'Tiểu thuyết tự sáng tác', 0, 2);
INSERT INTO `zl_project` VALUES (4, 0, 1, 1, 'truyen-tranh', 'Truyện tranh', 0, 3);

-- ----------------------------
-- Table structure for zl_setting
-- ----------------------------
DROP TABLE IF EXISTS `zl_setting`;
CREATE TABLE `zl_setting`  (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT 1,
  `user_id` int(11) NOT NULL DEFAULT 1,
  `level` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 - Nomal, 1 - Secure',
  `alias` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'md5 encrypt',
  `value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`setting_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zl_setting
-- ----------------------------
INSERT INTO `zl_setting` VALUES (1, 1, 1, 1, 0, 'website_title', 'Tiêu đề website ', 'Light Novel Synthetic', 1, 1);
INSERT INTO `zl_setting` VALUES (3, 3, 1, 1, 0, 'logo', 'Logo', 'assets/upload/users/admin/avatar6.png', 1, 1);
INSERT INTO `zl_setting` VALUES (5, 5, 1, 1, 0, 'favicon', 'Favicon', 'assets/upload/users/admin/avatar23.png', 1, 1);
INSERT INTO `zl_setting` VALUES (7, 0, 1, 1, 0, 'website_url', 'Website url', 'lightnovelsynthetic.com', 1, 1);
INSERT INTO `zl_setting` VALUES (8, 0, 1, 1, 0, 'email_name', 'Email Name', 'lightnovelsynthetic@gmail.com', 1, 1);
INSERT INTO `zl_setting` VALUES (9, 0, 1, 1, 0, 'email_password', 'Email Password', 'khanhtoan1999', 1, 1);
INSERT INTO `zl_setting` VALUES (10, 0, 1, 1, 0, 'footer_note', 'Footer Note', 'Light Novel Synthetic @ Coppyright 2018', 0, 0);

-- ----------------------------
-- Table structure for zl_template
-- ----------------------------
DROP TABLE IF EXISTS `zl_template`;
CREATE TABLE `zl_template`  (
  `template_id` int(11) NOT NULL,
  `template_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `alias` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `path` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sorting` int(11) NOT NULL,
  `logo` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `images` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `genabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`template_id`) USING BTREE,
  UNIQUE INDEX `name_alias`(`name`, `alias`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for zl_user
-- ----------------------------
DROP TABLE IF EXISTS `zl_user`;
CREATE TABLE `zl_user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1 - admin, 0 - user',
  `username` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'md5 encrypt',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `config` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `full_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logo` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `template` int(1) NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `active_code` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of zl_user
-- ----------------------------
INSERT INTO `zl_user` VALUES (1, 1, 'admin', '$2y$10$SIGvzrgzKh0h7bBSHMz7zeUm2iIvFkZVx3XMmzpFmvxMy13C7zvAq', 'zolawebgroup@gmail.com', NULL, NULL, 'website_address:a||website_title:Light Novel Synthetic||footer_note:a||email_name:lightnovelsynthetic@gmail.com||email_password:khanhtoan1999||logo:assets/upload/users/admin/avatar6.png||favicon:assets/upload/users/admin/avatar23.png', 'Admin', NULL, 0, 0, 1, '', 0);
INSERT INTO `zl_user` VALUES (14, 0, 'user22', '$2y$10$rq.Gv6gpXbfz8jcIAIWtVeugUI9W82mVd2660W3kSCR5KQgAZdI/C', 'user22@gmail.com', NULL, NULL, NULL, 'User 2', NULL, 0, 0, 1, '', 1544482800);

SET FOREIGN_KEY_CHECKS = 1;
