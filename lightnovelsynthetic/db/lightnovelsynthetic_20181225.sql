-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table lightnovelsynthetic.zl_banner
CREATE TABLE IF NOT EXISTS `zl_banner` (
  `banner_id` int(11) NOT NULL,
  `banner_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `module` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `function` varchar(50) NOT NULL,
  `params` varchar(50) NOT NULL,
  `link` text NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `genabled` tinyint(1) NOT NULL,
  `sorting` int(11) NOT NULL,
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`banner_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_banner: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `zl_banner` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_comment
CREATE TABLE IF NOT EXISTS `zl_comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_episodes_id` int(11) NOT NULL,
  `full_text` text NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`comment_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_comment: ~10 rows (approximately)
/*!40000 ALTER TABLE `zl_comment` DISABLE KEYS */;
REPLACE INTO `zl_comment` (`comment_id`, `content_id`, `parent_id`, `user_id`, `content_episodes_id`, `full_text`, `enabled`, `created_date`) VALUES
	(1, 5, 0, 1, 0, 'First comment 3', 0, 1544569200),
	(2, 5, 1, 1, 0, 'Second comment', 0, 1544569200),
	(3, 5, 1, 1, 0, 'Third comment 2', 0, 1544569200),
	(5, 5, 0, 1, 4, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.', 0, 1544569200),
	(7, 5, 0, 1, 0, ' this is new comment', 0, 1544569200),
	(8, 5, 1, 1, 0, ' this new new reply comment', 0, 1544914800),
	(9, 5, 1, 1, 0, ' This is second reply', 0, 1544914800),
	(10, 5, 5, 1, 4, ' New reply', 0, 1544914800),
	(11, 5, 0, 1, 0, ' Haha', 0, 1544914800),
	(12, 0, 0, 1, 1, ' New comment', 0, 1544914800);
/*!40000 ALTER TABLE `zl_comment` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_content
CREATE TABLE IF NOT EXISTS `zl_content` (
  `content_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_gid` int(11) NOT NULL,
  `content_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `title_en` varchar(200) DEFAULT NULL,
  `title_ja` varchar(200) DEFAULT NULL,
  `title_romaji` varchar(200) DEFAULT NULL,
  `alias` varchar(200) NOT NULL,
  `intro_text` varchar(100) NOT NULL,
  `full_text` longtext NOT NULL,
  `list_content_category_id` varchar(100) DEFAULT NULL,
  `list_project_id` varchar(100) DEFAULT NULL,
  `images` text NOT NULL,
  `logo` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `participator` varchar(100) DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `article` varchar(100) DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `progress` varchar(100) DEFAULT NULL,
  `translator` varchar(100) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `content_state_id` int(11) DEFAULT NULL,
  `view` int(11) DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `is_display_in_home` tinyint(1) DEFAULT '0',
  `is_adult` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`content_id`) USING BTREE,
  UNIQUE KEY `alias` (`alias`) USING BTREE,
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_content: ~3 rows (approximately)
/*!40000 ALTER TABLE `zl_content` DISABLE KEYS */;
REPLACE INTO `zl_content` (`content_id`, `content_gid`, `content_category_gid`, `lang_id`, `title`, `title_en`, `title_ja`, `title_romaji`, `alias`, `intro_text`, `full_text`, `list_content_category_id`, `list_project_id`, `images`, `logo`, `sorting`, `participator`, `author`, `article`, `language`, `progress`, `translator`, `note`, `type`, `content_state_id`, `view`, `enabled`, `genabled`, `created_date`, `is_display_in_home`, `is_adult`) VALUES
	(1, 0, 0, 0, '달빛 조각사 - LEGENDARY MOONLIGHT SCULPTOR', '', '', '', 'legendary-moonlight-sculptor', '', '<p>b</p>\\n\\n<p>c</p>\\n', '1||5', '3', 'assets/upload/users/admin/Legendary_Moonlight1.jpg', 'assets/upload/users/admin/Legendary_Moonlight.jpg', 0, 'DEO CAN TEN', 'DEO CAN TEN', '남희성', 'Tiếng Hàn', '', 'Đéo Cần Tên (ĐCT)', '', 'Light Novel', 4, 159, 1, 1, 0, 0, 1),
	(5, 0, 0, 0, 'AKU NO SOSHIKI NO KYUUJINKOUKOKU', 'The Evil Organization’s Recruitment Ad', '', '', 'aku-no-soshiki-no-kyuujinkoukoku', '', '<p>Komori Neito quyết t&acirc;m rời bỏ lối sống l&agrave;m NEET của m&igrave;nh v&agrave; cố xin việc l&agrave;m.Cậu t&igrave;m thấy một mẩu quảng c&aacute;o tuyển dụng cho một tổ chức t&agrave; &aacute;c h&agrave;ng đầu, lu&ocirc;n chiến đấu chống lại c&aacute;c si&ecirc;u anh h&ugrave;ng. V&agrave; thế l&agrave; cậu quyết định đi phỏng vấn xin việc. Kh&ocirc;ng c&oacute; kỹ năng n&agrave;o kh&aacute;c ngo&agrave;i c&aacute;ch nh&igrave;n nhận đ&uacute;ng đắn về thế giới t&agrave;n nhẫn n&agrave;y. Komori trở th&agrave;nh một tay sai cấp thấp v&agrave; bắt đầu l&agrave;m việc theo c&aacute;ch ri&ecirc;ng của m&igrave;nh để thăng tiến trong tổ chức Metallica, tổ chức t&agrave; &aacute;c lớn nhất thế giới.Bản dịch thuộc về nh&oacute;m dịch Shinigami-TeamNghi&ecirc;m cấm sao ch&eacute;p dưới mọi h&igrave;nh thức</p>\\n\\n<p>hah</p>\\n', '2||5', '1||3', 'assets/upload/users/admin/aff4.png', 'assets/upload/users/admin/aff.png', 0, 'Ms Lich', 'Ms Lich', 'Kotatsu', 'Tiếng Anh', 'Bất ổn', '', '', 'Web Novel', 3, 67, 1, 1, 1544482800, 0, 1),
	(6, 0, 0, 0, '오크지만 찬양해! - VINH DANH LOÀI ORC!', '', NULL, NULL, '-vinh-danh-loai-orc-', '', 'Tại một thế giới nơi mà sức mạnh, quyền lực và tiền bạc trở thành những phương tiện chính để người ta theo đuổi.\\\\n\\\\nỞ một thế giới, bị sự tham lam và ích kỷ làm cho ô uế.\\\\n\\\\nTrong một thế giới nơi mà danh dự, công lý, niềm tin cùng các giá trị tốt đẹp khác đã đi vào quên lãng.\\\\n\\\\n------------------------\\\\n\\\\nMột chiến binh Orc xuất hiện, chứng tỏ điều ngược lại.', '2||5', '1||4', 'assets/upload/users/admin/xxlarge23.png', 'assets/upload/users/admin/xxlarge22.png', 0, 'Roan', 'Roan', 'N/A', 'Tiếng Hàn', '1-3 chapter/ngày. (Trừ Thứ Bảy và Chủ Nhật, vì không chắc sẽ ra)', 'Roan', '', 'Light Novel', 3, 0, 1, 1, 1544914800, 0, 0);
/*!40000 ALTER TABLE `zl_content` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_content_category
CREATE TABLE IF NOT EXISTS `zl_content_category` (
  `content_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `intro_text` varchar(100) NOT NULL,
  `images` text NOT NULL,
  `logo` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genableed` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`content_category_id`) USING BTREE,
  UNIQUE KEY `alias` (`alias`) USING BTREE,
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_content_category: ~3 rows (approximately)
/*!40000 ALTER TABLE `zl_content_category` DISABLE KEYS */;
REPLACE INTO `zl_content_category` (`content_category_id`, `content_category_gid`, `lang_id`, `title`, `alias`, `intro_text`, `images`, `logo`, `sorting`, `enabled`, `genableed`, `created_date`) VALUES
	(1, 0, 0, 'Action', 'action', '', '', '', 0, 1, 1, '1'),
	(2, 0, 0, 'Adult', 'adult', '', '', '', 0, 1, 1, '1544451128'),
	(5, 0, 0, 'Adventure', 'adventure', '', '', '', 0, 1, 1, '1544451129');
/*!40000 ALTER TABLE `zl_content_category` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_content_episodes
CREATE TABLE IF NOT EXISTS `zl_content_episodes` (
  `content_episodes_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_episodes_gid` int(11) NOT NULL,
  `content_season_id` int(11) NOT NULL,
  `parent_gid` int(11) NOT NULL DEFAULT '0',
  `lang_id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `alias` text COLLATE utf8_unicode_ci NOT NULL,
  `intro_text` varchar(100) CHARACTER SET utf8 NOT NULL,
  `full_text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `footer_note` longtext COLLATE utf8_unicode_ci,
  `footer_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `footer_translator` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `footer_editor` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `images` text COLLATE utf8_unicode_ci NOT NULL,
  `logo` text COLLATE utf8_unicode_ci NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genableed` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`content_episodes_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_content_episodes: ~6 rows (approximately)
/*!40000 ALTER TABLE `zl_content_episodes` DISABLE KEYS */;
REPLACE INTO `zl_content_episodes` (`content_episodes_id`, `content_episodes_gid`, `content_season_id`, `parent_gid`, `lang_id`, `title`, `alias`, `intro_text`, `full_text`, `footer_note`, `footer_title`, `footer_translator`, `footer_editor`, `images`, `logo`, `sorting`, `enabled`, `genableed`, `created_date`) VALUES
	(1, 0, 6, 0, 0, 'Chương 1', 'chuong-1', '', '', NULL, 'a', 'b', 'c', '', '', 0, 1, 1, 1),
	(2, 0, 1, 0, 0, 'Chương 2', 'chuong-2-2', '', '<p>a</p>\\n\\n<p>b</p>\\n\\n<p>c</p>\\n', '<p>e</p>\\n\\n<p>f</p>\\n\\n<p>g</p>\\n', 'a', 'b', 'd', '', '', 0, 1, 1, 1544569200),
	(4, 0, 6, 1, 0, 'Chương2', 'chuong-2-1', '', '', '', '', '', '', '', '', 0, 1, 1, 1544569200),
	(5, 0, 4, 0, 0, 'Chapter 1', 'chapter-1', '', '<p>a</p>\\n', '<p>a</p>\\n', '', '', '', '', '', 0, 1, 1, 1545692400),
	(6, 0, 4, 0, 0, 'Chapter 2', 'chapter-2', '', '<p>b</p>\\n', '<p>b</p>\\n', '', '', '', '', '', 0, 1, 1, 1545692400),
	(7, 0, 6, 0, 0, 'Chapter 3', 'chapter-3', '', '<p>b</p>\\n', '<p>b</p>\\n', '', '', '', '', '', 0, 1, 1, 1545692400);
/*!40000 ALTER TABLE `zl_content_episodes` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_content_season
CREATE TABLE IF NOT EXISTS `zl_content_season` (
  `content_season_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_season_gid` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `alias` text NOT NULL,
  `intro_text` varchar(100) NOT NULL,
  `full_text` text NOT NULL,
  `images` text NOT NULL,
  `logo` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genableed` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`content_season_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_content_season: ~3 rows (approximately)
/*!40000 ALTER TABLE `zl_content_season` DISABLE KEYS */;
REPLACE INTO `zl_content_season` (`content_season_id`, `content_season_gid`, `content_id`, `lang_id`, `title`, `alias`, `intro_text`, `full_text`, `images`, `logo`, `sorting`, `enabled`, `genableed`, `created_date`) VALUES
	(1, 0, 1, 0, 'Tập 1', 'tap-1', '', '', '', 'assets/upload/users/admin/avatar2.png', 0, 1, 1, 1),
	(4, 0, 1, 0, 'Tập 2', 'tap-2', '', '', '', 'assets/upload/users/admin/photo41.jpg', 0, 1, 1, 1544569200),
	(6, 0, 5, 0, 'Tập 1', 'tap-1', '', '', '', 'assets/upload/users/admin/avatar4.png', 0, 1, 1, 1544569200);
/*!40000 ALTER TABLE `zl_content_season` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_content_state
CREATE TABLE IF NOT EXISTS `zl_content_state` (
  `content_state_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_state_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT '1',
  `title` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`content_state_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_content_state: ~2 rows (approximately)
/*!40000 ALTER TABLE `zl_content_state` DISABLE KEYS */;
REPLACE INTO `zl_content_state` (`content_state_id`, `content_state_gid`, `lang_id`, `title`, `alias`, `enabled`, `created_date`) VALUES
	(3, 0, 1, 'Đang tiến hành', 'dang-tien-hanh', 0, 1544482800),
	(4, 0, 1, 'Đã hoàn thành', 'da-hoan-thanh', 0, 1544482800);
/*!40000 ALTER TABLE `zl_content_state` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_estore
CREATE TABLE IF NOT EXISTS `zl_estore` (
  `estore_id` int(11) NOT NULL,
  `estore_gid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `logo` text NOT NULL,
  `favicon` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`estore_id`) USING BTREE,
  UNIQUE KEY `name_alias` (`name`,`alias`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_estore: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_estore` DISABLE KEYS */;
/*!40000 ALTER TABLE `zl_estore` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_lang
CREATE TABLE IF NOT EXISTS `zl_lang` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `logo` text NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`lang_id`) USING BTREE,
  UNIQUE KEY `code_title` (`code`,`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_lang: ~2 rows (approximately)
/*!40000 ALTER TABLE `zl_lang` DISABLE KEYS */;
REPLACE INTO `zl_lang` (`lang_id`, `code`, `title`, `logo`, `enabled`, `default`, `created_date`) VALUES
	(1, 'vi', 'Tiếng Việt', 'vi.png', 1, 1, ''),
	(2, 'en', 'English', 'en.png', 1, 0, '');
/*!40000 ALTER TABLE `zl_lang` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_menu
CREATE TABLE IF NOT EXISTS `zl_menu` (
  `menu_id` int(11) NOT NULL,
  `menu_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `module` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `function` varchar(50) NOT NULL,
  `params` varchar(50) NOT NULL,
  `link` text NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `genabled` tinyint(1) NOT NULL,
  `sorting` int(11) NOT NULL,
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_menu: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `zl_menu` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_product
CREATE TABLE IF NOT EXISTS `zl_product` (
  `product_id` int(11) NOT NULL,
  `product_gid` int(11) NOT NULL,
  `product_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `intro_text` varchar(100) NOT NULL,
  `full_text` text NOT NULL,
  `price` int(11) NOT NULL,
  `is_sale` tinyint(1) NOT NULL,
  `price_sale` int(11) NOT NULL,
  `images` text NOT NULL,
  `logo` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genableed` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`product_id`) USING BTREE,
  UNIQUE KEY `alias` (`alias`) USING BTREE,
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_product: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `zl_product` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_product_category
CREATE TABLE IF NOT EXISTS `zl_product_category` (
  `product_category_id` int(11) NOT NULL,
  `product_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `intro_text` varchar(100) NOT NULL,
  `images` text NOT NULL,
  `logo` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genableed` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`product_category_id`) USING BTREE,
  UNIQUE KEY `alias` (`alias`) USING BTREE,
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_product_category: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_product_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `zl_product_category` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_project
CREATE TABLE IF NOT EXISTS `zl_project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_gid` int(11) NOT NULL,
  `parent_gid` int(11) NOT NULL DEFAULT '0',
  `lang_id` int(11) NOT NULL DEFAULT '1',
  `alias` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`project_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_project: ~3 rows (approximately)
/*!40000 ALTER TABLE `zl_project` DISABLE KEYS */;
REPLACE INTO `zl_project` (`project_id`, `project_gid`, `parent_gid`, `lang_id`, `alias`, `title`, `enabled`, `created_date`) VALUES
	(1, 1, 0, 1, 'tieu-thuyet', 'Tiêu thuyết', 0, 1),
	(3, 0, 0, 1, 'tieu-thuyet-tu-sang-tac', 'Tiểu thuyết tự sáng tác', 0, 2),
	(4, 0, 1, 1, 'truyen-tranh', 'Truyện tranh', 0, 3);
/*!40000 ALTER TABLE `zl_project` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_setting
CREATE TABLE IF NOT EXISTS `zl_setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL DEFAULT '1',
  `level` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - Nomal, 1 - Secure',
  `alias` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL COMMENT 'md5 encrypt',
  `value` varchar(100) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`setting_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_setting: ~7 rows (approximately)
/*!40000 ALTER TABLE `zl_setting` DISABLE KEYS */;
REPLACE INTO `zl_setting` (`setting_id`, `setting_gid`, `lang_id`, `user_id`, `level`, `alias`, `title`, `value`, `enabled`, `created_date`) VALUES
	(1, 1, 1, 1, 0, 'website_title', 'Tiêu đề website ', 'Light Novel Synthetic', 1, 1),
	(3, 3, 1, 1, 0, 'logo', 'Logo', 'assets/upload/users/admin/avatar6.png', 1, 1),
	(5, 5, 1, 1, 0, 'favicon', 'Favicon', 'assets/upload/users/admin/avatar23.png', 1, 1),
	(7, 0, 1, 1, 0, 'website_url', 'Website url', 'lightnovelsynthetic.com', 1, 1),
	(8, 0, 1, 1, 0, 'email_name', 'Email Name', 'lightnovelsynthetic@gmail.com', 1, 1),
	(9, 0, 1, 1, 0, 'email_password', 'Email Password', 'khanhtoan1999', 1, 1),
	(10, 0, 1, 1, 0, 'footer_note', 'Footer Note', 'Light Novel Synthetic @ Coppyright 2018', 0, 0);
/*!40000 ALTER TABLE `zl_setting` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_template
CREATE TABLE IF NOT EXISTS `zl_template` (
  `template_id` int(11) NOT NULL,
  `template_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `path` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `logo` text NOT NULL,
  `images` text NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`template_id`) USING BTREE,
  UNIQUE KEY `name_alias` (`name`,`alias`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_template: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `zl_template` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_user
CREATE TABLE IF NOT EXISTS `zl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 - admin, 0 - user',
  `username` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL COMMENT 'md5 encrypt',
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` text,
  `config` text,
  `full_name` varchar(100) NOT NULL,
  `logo` text,
  `template` int(1) NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `active_code` text NOT NULL,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_user: ~2 rows (approximately)
/*!40000 ALTER TABLE `zl_user` DISABLE KEYS */;
REPLACE INTO `zl_user` (`user_id`, `type`, `username`, `password`, `email`, `phone`, `address`, `config`, `full_name`, `logo`, `template`, `sorting`, `enabled`, `active_code`, `created_date`) VALUES
	(1, 1, 'admin', '$2y$10$SIGvzrgzKh0h7bBSHMz7zeUm2iIvFkZVx3XMmzpFmvxMy13C7zvAq', 'zolawebgroup@gmail.com', NULL, NULL, 'website_address:a||website_title:Light Novel Synthetic||footer_note:a||email_name:lightnovelsynthetic@gmail.com||email_password:khanhtoan1999||logo:assets/upload/users/admin/avatar6.png||favicon:assets/upload/users/admin/avatar23.png', 'Admin', NULL, 0, 0, 1, '', 0),
	(14, 0, 'user22', '$2y$10$rq.Gv6gpXbfz8jcIAIWtVeugUI9W82mVd2660W3kSCR5KQgAZdI/C', 'user22@gmail.com', NULL, NULL, NULL, 'User 2', NULL, 0, 0, 1, '', 1544482800);
/*!40000 ALTER TABLE `zl_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
