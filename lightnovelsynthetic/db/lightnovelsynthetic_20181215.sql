-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5339
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for lightnovelsynthetic
CREATE DATABASE IF NOT EXISTS `lightnovelsynthetic` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `lightnovelsynthetic`;

-- Dumping structure for table lightnovelsynthetic.zl_banner
CREATE TABLE IF NOT EXISTS `zl_banner` (
  `banner_id` int(11) NOT NULL,
  `banner_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `module` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `function` varchar(50) NOT NULL,
  `params` varchar(50) NOT NULL,
  `link` text NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `genabled` tinyint(1) NOT NULL,
  `sorting` int(11) NOT NULL,
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`banner_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_banner: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `zl_banner` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_comment
CREATE TABLE IF NOT EXISTS `zl_comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_episodes_id` int(11) NOT NULL,
  `full_text` text NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`comment_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_comment: ~4 rows (approximately)
/*!40000 ALTER TABLE `zl_comment` DISABLE KEYS */;
REPLACE INTO `zl_comment` (`comment_id`, `content_id`, `parent_id`, `user_id`, `content_episodes_id`, `full_text`, `enabled`, `created_date`) VALUES
	(1, 1, 0, 1, 0, 'First comment 3', 0, 1),
	(2, 1, 5, 1, 0, 'Second comment', 0, 0),
	(3, 5, 5, 1, 0, 'Third comment 2', 0, 0),
	(5, 0, 0, 13, 4, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.', 0, 1544569200);
/*!40000 ALTER TABLE `zl_comment` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_content
CREATE TABLE IF NOT EXISTS `zl_content` (
  `content_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_gid` int(11) NOT NULL,
  `content_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `alias` varchar(50) NOT NULL,
  `intro_text` varchar(100) NOT NULL,
  `full_text` text NOT NULL,
  `list_content_category_id` varchar(100) DEFAULT NULL,
  `list_project_id` varchar(100) DEFAULT NULL,
  `images` text NOT NULL,
  `logo` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `participator` varchar(100) DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `article` varchar(100) DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `progress` varchar(100) DEFAULT NULL,
  `translator` varchar(100) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `content_state_id` int(11) DEFAULT NULL,
  `view` int(11) DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  `is_display_translator` tinyint(1) DEFAULT '1',
  `is_adult` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`content_id`) USING BTREE,
  UNIQUE KEY `alias` (`alias`) USING BTREE,
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_content: ~2 rows (approximately)
/*!40000 ALTER TABLE `zl_content` DISABLE KEYS */;
REPLACE INTO `zl_content` (`content_id`, `content_gid`, `content_category_gid`, `lang_id`, `title`, `title_en`, `alias`, `intro_text`, `full_text`, `list_content_category_id`, `list_project_id`, `images`, `logo`, `sorting`, `participator`, `author`, `article`, `language`, `progress`, `translator`, `note`, `type`, `content_state_id`, `view`, `enabled`, `genabled`, `created_date`, `is_display_translator`, `is_adult`) VALUES
	(1, 0, 0, 0, 'Truyện 1', 'Novel 1', 'truyen-1', '', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.', '1||2', '3', 'assets/upload/users/admin/photo42.jpg', 'assets/upload/users/admin/avatar2.png', 0, 'a', 'a', 'b', 'c', 'd', 'e', 'reg', 'demo', 1, 123, 1, 1, 0, 0, 0),
	(5, 0, 0, 0, 'Truyện 2', 'Novel 2', 'truyen-2', '', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\\nInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.\\nInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.', '2||5', '1||3', 'assets/upload/users/admin/avatar04.png', 'assets/upload/users/admin/avatar51.png', 0, 'b', 'b', 'd', 'e', 'f', 'g', 'efsafds sfd f 1', 'demo', 3, 0, 1, 1, 1544482800, 0, 0);
/*!40000 ALTER TABLE `zl_content` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_content_category
CREATE TABLE IF NOT EXISTS `zl_content_category` (
  `content_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `intro_text` varchar(100) NOT NULL,
  `images` text NOT NULL,
  `logo` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genableed` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`content_category_id`) USING BTREE,
  UNIQUE KEY `alias` (`alias`) USING BTREE,
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_content_category: ~3 rows (approximately)
/*!40000 ALTER TABLE `zl_content_category` DISABLE KEYS */;
REPLACE INTO `zl_content_category` (`content_category_id`, `content_category_gid`, `lang_id`, `title`, `alias`, `intro_text`, `images`, `logo`, `sorting`, `enabled`, `genableed`, `created_date`) VALUES
	(1, 0, 0, 'Action', 'action', '', '', '', 0, 1, 1, '1'),
	(2, 0, 0, 'Adult', 'adult', '', '', '', 0, 1, 1, '1544451128'),
	(5, 0, 0, 'Adventure', 'adventure', '', '', '', 0, 1, 1, '1544451129');
/*!40000 ALTER TABLE `zl_content_category` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_content_episodes
CREATE TABLE IF NOT EXISTS `zl_content_episodes` (
  `content_episodes_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_episodes_gid` int(11) NOT NULL,
  `content_season_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `intro_text` varchar(100) NOT NULL,
  `full_text` text NOT NULL,
  `images` text NOT NULL,
  `logo` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genableed` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`content_episodes_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_content_episodes: ~3 rows (approximately)
/*!40000 ALTER TABLE `zl_content_episodes` DISABLE KEYS */;
REPLACE INTO `zl_content_episodes` (`content_episodes_id`, `content_episodes_gid`, `content_season_id`, `lang_id`, `title`, `alias`, `intro_text`, `full_text`, `images`, `logo`, `sorting`, `enabled`, `genableed`, `created_date`) VALUES
	(1, 0, 6, 0, 'Chương 1', 'chuong-1', '', 'This is content', '', '', 0, 1, 1, 1),
	(2, 0, 4, 0, 'Chương 2', 'chuong-2', '', '', '', '', 0, 1, 1, 1544569200),
	(4, 0, 6, 0, 'Chương 2', 'chuong-2', '', '<p>Xin ch&agrave;o.</p>\\n\\n<p>T&ocirc;i t&ecirc;n l&agrave; Ng&ocirc; Huy Triều.</p>\\n\\n<p>Th&acirc;n &aacute;i.</p>\\n\\n<p>&nbsp;</p>\\n', '', '', 0, 1, 1, 1544569200);
/*!40000 ALTER TABLE `zl_content_episodes` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_content_season
CREATE TABLE IF NOT EXISTS `zl_content_season` (
  `content_season_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_season_gid` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `intro_text` varchar(100) NOT NULL,
  `full_text` text NOT NULL,
  `images` text NOT NULL,
  `logo` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genableed` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`content_season_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_content_season: ~3 rows (approximately)
/*!40000 ALTER TABLE `zl_content_season` DISABLE KEYS */;
REPLACE INTO `zl_content_season` (`content_season_id`, `content_season_gid`, `content_id`, `lang_id`, `title`, `alias`, `intro_text`, `full_text`, `images`, `logo`, `sorting`, `enabled`, `genableed`, `created_date`) VALUES
	(1, 0, 1, 0, 'Tập 1', 'tap-1', '', '', '', 'assets/upload/users/admin/avatar2.png', 0, 1, 1, 1),
	(4, 0, 1, 0, 'Tập 2', 'tap-2', '', '', '', 'assets/upload/users/admin/photo41.jpg', 0, 1, 1, 1544569200),
	(6, 0, 5, 0, 'Tập 1', 'tap-1', '', '', '', 'assets/upload/users/admin/avatar4.png', 0, 1, 1, 1544569200);
/*!40000 ALTER TABLE `zl_content_season` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_content_state
CREATE TABLE IF NOT EXISTS `zl_content_state` (
  `content_state_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_state_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT '1',
  `title` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`content_state_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_content_state: ~4 rows (approximately)
/*!40000 ALTER TABLE `zl_content_state` DISABLE KEYS */;
REPLACE INTO `zl_content_state` (`content_state_id`, `content_state_gid`, `lang_id`, `title`, `alias`, `enabled`, `created_date`) VALUES
	(1, 1, 1, 'Chưa tiến hành', 'chua-tien-hanh', 0, 1544482800),
	(3, 0, 1, 'Đang tiến hành', 'dang-tien-hanh', 0, 1544482800),
	(4, 0, 1, 'Đã tiến hành', 'da-tien-hanh', 0, 1544482800),
	(12, 0, 1, 'Tạm ngưng', 'tam-ngung', 0, 1544482800);
/*!40000 ALTER TABLE `zl_content_state` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_estore
CREATE TABLE IF NOT EXISTS `zl_estore` (
  `estore_id` int(11) NOT NULL,
  `estore_gid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `logo` text NOT NULL,
  `favicon` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`estore_id`) USING BTREE,
  UNIQUE KEY `name_alias` (`name`,`alias`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_estore: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_estore` DISABLE KEYS */;
/*!40000 ALTER TABLE `zl_estore` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_lang
CREATE TABLE IF NOT EXISTS `zl_lang` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `logo` text NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`lang_id`) USING BTREE,
  UNIQUE KEY `code_title` (`code`,`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_lang: ~2 rows (approximately)
/*!40000 ALTER TABLE `zl_lang` DISABLE KEYS */;
REPLACE INTO `zl_lang` (`lang_id`, `code`, `title`, `logo`, `enabled`, `default`, `created_date`) VALUES
	(1, 'vi', 'Tiếng Việt', 'vi.png', 1, 1, ''),
	(2, 'en', 'English', 'en.png', 1, 0, '');
/*!40000 ALTER TABLE `zl_lang` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_menu
CREATE TABLE IF NOT EXISTS `zl_menu` (
  `menu_id` int(11) NOT NULL,
  `menu_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `module` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `function` varchar(50) NOT NULL,
  `params` varchar(50) NOT NULL,
  `link` text NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `genabled` tinyint(1) NOT NULL,
  `sorting` int(11) NOT NULL,
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_menu: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `zl_menu` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_product
CREATE TABLE IF NOT EXISTS `zl_product` (
  `product_id` int(11) NOT NULL,
  `product_gid` int(11) NOT NULL,
  `product_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `intro_text` varchar(100) NOT NULL,
  `full_text` text NOT NULL,
  `price` int(11) NOT NULL,
  `is_sale` tinyint(1) NOT NULL,
  `price_sale` int(11) NOT NULL,
  `images` text NOT NULL,
  `logo` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genableed` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`product_id`) USING BTREE,
  UNIQUE KEY `alias` (`alias`) USING BTREE,
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_product: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `zl_product` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_product_category
CREATE TABLE IF NOT EXISTS `zl_product_category` (
  `product_category_id` int(11) NOT NULL,
  `product_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `intro_text` varchar(100) NOT NULL,
  `images` text NOT NULL,
  `logo` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genableed` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`product_category_id`) USING BTREE,
  UNIQUE KEY `alias` (`alias`) USING BTREE,
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_product_category: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_product_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `zl_product_category` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_project
CREATE TABLE IF NOT EXISTS `zl_project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_gid` int(11) NOT NULL,
  `parent_gid` int(11) NOT NULL DEFAULT '0',
  `lang_id` int(11) NOT NULL DEFAULT '1',
  `alias` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`project_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_project: ~3 rows (approximately)
/*!40000 ALTER TABLE `zl_project` DISABLE KEYS */;
REPLACE INTO `zl_project` (`project_id`, `project_gid`, `parent_gid`, `lang_id`, `alias`, `title`, `enabled`, `created_date`) VALUES
	(1, 1, 0, 1, 'demo-1', 'Demo 1', 0, 1),
	(3, 0, 0, 1, 'demo-2', 'Demo 2', 0, 2),
	(4, 0, 1, 1, 'demo-3', 'Demo 3', 0, 3);
/*!40000 ALTER TABLE `zl_project` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_setting
CREATE TABLE IF NOT EXISTS `zl_setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL DEFAULT '1',
  `level` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - Nomal, 1 - Secure',
  `alias` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL COMMENT 'md5 encrypt',
  `value` varchar(100) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`setting_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_setting: ~6 rows (approximately)
/*!40000 ALTER TABLE `zl_setting` DISABLE KEYS */;
REPLACE INTO `zl_setting` (`setting_id`, `setting_gid`, `lang_id`, `user_id`, `level`, `alias`, `title`, `value`, `enabled`, `created_date`) VALUES
	(1, 1, 1, 1, 0, 'website_title', 'Tiêu đề website ', 'Light Novel Synthetic', 1, 1),
	(3, 3, 1, 1, 0, 'logo', 'Logo', 'assets/upload/users/admin/avatar6.png', 1, 1),
	(5, 5, 1, 1, 0, 'favicon', 'Favicon', 'assets/upload/users/admin/avatar23.png', 1, 1),
	(7, 0, 1, 1, 0, 'website_url', 'Website url', 'lightnovelsynthetic.com', 1, 1),
	(8, 0, 1, 1, 0, 'email_name', 'Email Name', 'lightnovelsynthetic@gmail.com', 1, 1),
	(9, 0, 1, 1, 0, 'email_password', 'Email Password', 'khanhtoan1999', 1, 1),
	(10, 0, 1, 1, 0, 'footer_note', 'Footer Note', 'Light Novel Synthetic @ Coppyright 2018', 0, 0);
/*!40000 ALTER TABLE `zl_setting` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_template
CREATE TABLE IF NOT EXISTS `zl_template` (
  `template_id` int(11) NOT NULL,
  `template_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `path` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `logo` text NOT NULL,
  `images` text NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `genabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` varchar(100) NOT NULL,
  PRIMARY KEY (`template_id`) USING BTREE,
  UNIQUE KEY `name_alias` (`name`,`alias`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_template: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `zl_template` ENABLE KEYS */;

-- Dumping structure for table lightnovelsynthetic.zl_user
CREATE TABLE IF NOT EXISTS `zl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 - admin, 0 - user',
  `username` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL COMMENT 'md5 encrypt',
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` text,
  `config` text,
  `full_name` varchar(100) NOT NULL,
  `logo` text,
  `template` int(1) NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `active_code` text NOT NULL,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table lightnovelsynthetic.zl_user: ~3 rows (approximately)
/*!40000 ALTER TABLE `zl_user` DISABLE KEYS */;
REPLACE INTO `zl_user` (`user_id`, `type`, `username`, `password`, `email`, `phone`, `address`, `config`, `full_name`, `logo`, `template`, `sorting`, `enabled`, `active_code`, `created_date`) VALUES
	(1, 1, 'admin', '$2y$10$SIGvzrgzKh0h7bBSHMz7zeUm2iIvFkZVx3XMmzpFmvxMy13C7zvAq', 'zolawebgroup@gmail.com', NULL, NULL, 'website_address:a||website_title:Light Novel Synthetic||footer_note:a||email_name:lightnovelsynthetic@gmail.com||email_password:khanhtoan1999||logo:assets/upload/users/admin/avatar6.png||favicon:assets/upload/users/admin/avatar23.png', 'Admin', NULL, 0, 0, 1, '', 0),
	(13, 0, 'fgdfg', '$2y$10$2VipDalCbmPlv.V5AQMhruO/8ZoOEDpq6ARNTozrd/64d6CqnFWIe', 'feef@grge.rg', NULL, NULL, NULL, 'sd 1', 'user_logo.png', 0, 0, 0, '', 1544206326),
	(14, 0, 'user22', '$2y$10$rq.Gv6gpXbfz8jcIAIWtVeugUI9W82mVd2660W3kSCR5KQgAZdI/C', 'user22@gmail.com', NULL, NULL, NULL, 'User 2', NULL, 0, 0, 1, '', 1544482800),
	(18, 0, 'fewfewfewfwefwef', '$2y$10$Hgf9ulan9RRqC6j7Gcng1uEDpyYWMHFYi/qYjxAMNCs2kjCfSlZwm', 'ewfewfewf@fwefewf.ewfwef', NULL, NULL, NULL, 'efewfewfewfewfewf', 'user_logo.png', 0, 0, 0, '', 1544828400);
/*!40000 ALTER TABLE `zl_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
