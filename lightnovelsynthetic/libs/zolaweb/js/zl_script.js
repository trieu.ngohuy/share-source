/*
 * Zolaweb common script
 * Author: Zola Web Group
 * Create: 11/10/2018
 * Ver: 1.0
 */
/*
 * Ajax Handle
 */
function zl_ajax_handle(objInput) {
    if (objInput['type'] === undefined || objInput['url'] === undefined ||
            objInput['objData'] === undefined) {
        //Display error dialog (Handle later)
        alert('Input prameters invalid.');
    } else {
        $.ajax({
            type: objInput['type'],
            url: objInput['url'],
            data: objInput['objData'], 
            dataType: "html",
            success: function (objData) {
                if(typeof myfunc == 'RequestResponse'){
                    RequestResponse(objData, objInput['key']);
                }
            },
            error: function () {
                //Display error dialog (Handle later)
                alert('Ajax error.');
            }
        });
    }
}
/*Generate slug*/
function convert_to_slug(Text)
{
    Text = Text.toLowerCase();
    return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
        ;
}
//Convert vietnamese to english
function convert_vi_to_en(alias) {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    str = str.replace(/đ/g, 'd');
    str = str.replace(/\W+/g, ' ');
    str = str.replace(/\s/g, '-');
    return str;
}
//Check email format
function is_email(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}