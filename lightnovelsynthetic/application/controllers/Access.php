<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends CI_Controller {

    private $_view_folder = "access/";

    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();
        //Load encrypt model
        $this->load->helper('encrypt');
    }
    function load_global_data(&$arr_view_data){
        //Get login user
        $arr_login_user = $this->session->userdata(SS_LOGIN);
        $arr_view_data['login_data'] = $arr_login_user;
         //Load model
        $this->load->model('setting_model');
        $config_data = $this->setting_model->get_list_data();
        $arr_view_data['setting'] = $config_data;
        //Load state for header
        $this->load->model('state_model');
        $state = $this->state_model->get_list_data();
        $arr_view_data['state'] = $state;

        //Get list content for ranking
        $this->load->model('content_model');
        $content = $this->content_model->get_list_data();
        sort_array($content, 'view');
        $arr_view_data['global_content'] = $content;
    }
    /*
     * Admin logout
     */

    public function AdminLogout() {
        try {
            //Remove login data
            $this->session->set_userdata(SS_ADMIN_LOGIN, array());
            //Redirect to login page
            redirect(URL_ADMIN_LOGIN, 'refresh');
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    /*
     * client logout
     */

    public function ClientLogout() {
        try {
            //Remove login data
            $this->session->set_userdata(SS_LOGIN, array());
            //Redirect to login page
            redirect(URL_LOGIN, 'refresh');
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
     * Client login
     */

    public function ClientLogin() {
        try {
            $arr_view_data = array();

            //Redirect to homepage if already login
            if ($this->session->userdata(SS_LOGIN)) {
                redirect(URL_HOME, 'refresh');
            }

            //Form submitted
            if ($this->input->post('data')) {
                $arr_view_data = $this->Login($arr_view_data, SS_LOGIN, URL_HOME, 0);
            }
            $this->load_global_data($arr_view_data);
            $this->layout->load('access', 'access/frontend/login', $arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    /*
     * Admin Login
     */

    public function AdminLogin() {
        try {
            $arr_view_data = array();

            //Redirect to homepage if already login
            if ($this->session->userdata(SS_ADMIN_LOGIN)) {
                redirect(URL_ADMIN_SETTING, 'refresh');
            }

            //Form submitted
            if ($this->input->post('data')) {
                $arr_view_data = $this->Login($arr_view_data, SS_ADMIN_LOGIN, URL_ADMIN_SETTING, 1);
            }

            $this->layout->load('admin_access', 'access/backend/login', $arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    /*
     * Login function
     */

    private function Login($arr_view_data, $session, $redirect_url, $user_type) {

        //Get post data
        $arr_post_data = $this->input->post('data');

        //Load model
        $this->load->model('user_model');

        //Get user from username
        if ($user_type == 1) {
            $arr_data = $this->user_model->get_single_data(array(
                'username' => $arr_post_data['username'],
                'type' => $user_type
            ));
        } else {
            $arr_data = $this->user_model->get_single_data(array(
                'username' => $arr_post_data['username']
            ));
        }

        //Check if username exist
        if ($arr_data != false) {

            //Check if password is correct
            if (password_check(password_generator($arr_post_data['password']), $arr_data['password'])) {
                //Save session login user inf
                $this->session->set_userdata($session, $arr_data);
                //Redirect into setting page
                redirect($redirect_url, 'refresh');
            } else {
                $arr_view_data['result'] = array(
                    'status' => 0,
                    'message' => 'Mật khẩu không đúng.'
                );
            }
        } else {

            $arr_view_data['result'] = array(
                'status' => 0,
                'message' => 'Tên đăng nhập không đúng.'
            );
        }
        return $arr_view_data;
    }

    /*
     * Client register
     */

    public function ClientRegister() {
        try {
            $arr_view_data = array();
            $str_erorr_message = '';
            //$this->session->set_userdata(SS_ADMIN_LOGIN, array());
            //Redirect to homepage if already login
            if ($this->session->userdata(SS_LOGIN)) {
                redirect(URL_HOME, 'refresh');
            }

            //form submitted
            if ($this->input->post('data')) {

                //Get post data
                $arr_post_data = $this->input->post('data');

                //Assing submit data
                $arr_view_data['arr_post_data'] = $arr_post_data;

                //Load model
                $this->load->model('user_model');

                //Check if password is not match
                if ($arr_post_data['password'] != $arr_post_data['confirm_password']) {
                    $str_erorr_message .= '<li>Mật khẩu không khớp.</li>';
                }

                //Get user from username
                $arr_data = $this->user_model->get_single_data(array(
                    'username' => $arr_post_data['username']
                ));

                //Check if username exist
                if ($arr_data != false) {
                    $str_erorr_message .= '<li>Tên đăng nhập đã tồn tại.</li>';
                }

                //Get user from email
                $arr_data = $this->user_model->get_single_data(array(
                    'email' => $arr_post_data['email']
                ));

                //Check if email exist
                if ($arr_data != false) {
                    $str_erorr_message .= '<li>Email đã tồn tại.</li>';
                }

                //If there is no error then process register new user
                if ($str_erorr_message == '') {
                    $obj_insert = $this->user_model->insert_new_data(array(
                        'full_name' => $arr_post_data['full_name'],
                        'username' => $arr_post_data['username'],
                        'email' => $arr_post_data['email'],
                        'password' => password_encrypt(password_generator($arr_post_data['password'])),
                        'type' => 0,
                        'logo' => 'user_logo.png',
                        'created_date' => strtotime(date(DATE_FORMAT)),
                    ));
                    //Sent mail
                    $this->load->helper('mail');
                    send_mail($this, array(
                        'subject' => 'Có người đọc đăng ký mới.',
                        'content' => 'Có người đọc đăng ký mới, hãy vào admin để kích hoạt ngay.'
                    ));
                    //Show succeed message
                    $arr_view_data['result'] = array(
                        'status' => 1,
                        'message' => 'Đăng ký thành công. Hãy đợi ban quản trị duyệt đăng nhập nhé.'
                    );
                } else {
                    $arr_view_data['result'] = array(
                        'status' => 0,
                        'message' => 'Danh sách lỗi: <ul>' . $str_erorr_message . '</ul>'
                    );
                }
            } else {
                $arr_view_data['arr_post_data'] = array();
            }
            $this->load_global_data($arr_view_data);
            $this->layout->load('access', $this->_view_folder . 'frontend/register', $arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    /*
    * Client forget password
    */
    public function clientForgetPassword(){
        try {
            
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
}
