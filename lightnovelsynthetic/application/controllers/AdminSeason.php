<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminSeason extends ZLAdmin_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('season_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            //$this->arr_view_data = array();

            //Active menu
            $this->arr_view_data['active_menu'] = 'season';
            
            //Get list data
            $arr_data = $this->season_model->get_list_data();
            foreach ($arr_data as &$value) {
                $this->load->model('content_model');
                $tmp = $this->content_model->get_single_data(array(
                    'content_id' => $value['content_id']
                ));
                $value['content'] = $tmp['title'];
            }
            unset($value);

            //Set post data back to view
            $this->arr_view_data['data'] = $arr_data;

            //Get list category
            $this->load->model('content_model');
            $this->arr_view_data['content'] = $this->content_model->get_list_data();

            //Set layout
            $this->layout->load('admin', 'season/backend/manager', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    //Update/New data
    function execute_query() {
        //Get post data
        $content_season_id = $_POST['content_season_id'];
        $arr_query_data = array(
            'title' => $_POST['title'],
            'alias' => $_POST['alias'],
            'content_id' => $_POST['content_id']
        );
        
        //Upload images
        $upload_logo = '';
        if (isset($_FILES['logo']['name'])) {
            $upload_logo = $this->upload_images('logo');
        }
        if ($upload_logo !== false) {
            if ($upload_logo !== '') {
                $arr_query_data['logo'] = $upload_logo;
            }
            //Execute query
            if ($content_season_id == 0) {
                $arr_query_data['created_date'] = strtotime(date(DATE_FORMAT));
                //New
                $arr_data = $this->season_model->insert_new_data($arr_query_data);
            } else {
                //Update
                $arr_data = $this->season_model->update_existing_data(array(
                    'content_season_id' => $content_season_id
                        ), $arr_query_data);
            }
            echo '1';
        } else {
            echo '0';
        }
    }

    //Upload images
    function upload_images($name) {
        $is_upload_success = true;
        $path = '';
        //Get login user data
        $arr_login_user = $this->session->userdata(SS_ADMIN_LOGIN);

        $upload_logo = upload_images($this, $arr_login_user['username'], $name);
        if ($upload_logo['status'] == false) {
            $is_upload_success = false;
        } else {
            $path = USER_UPLOAD_PATH . $arr_login_user['username'] . '/' . $upload_logo['data']['upload_data']['file_name'];
        }

        if ($is_upload_success) {
            return $path;
        } else {
            return false;
        }
    }

    //Get data
    function get_data() {

        //Get list data
        $arr_data = $this->season_model->get_list_data();
        foreach ($arr_data as &$value) {
            $this->load->model('content_model');
            $tmp = $this->content_model->get_single_data(array(
                'content_id' => $value['content_id']
            ));
            $value['content'] = $tmp['title'];
        }
        echo json_encode($arr_data);
    }

    //Delete
    function delete() {
        //Get post data
        $content_season_id = $_POST['content_season_id'];

        //Get detail
        $arr_data = $this->season_model->get_single_data(array(
            'content_season_id' => $content_season_id
        ));
        
        //Execute query
        $this->season_model->delete_data(array(
            'content_season_id' => $content_season_id
        ));
        
        //Delete images
        delete_images($arr_data['logo']);
    }

}
