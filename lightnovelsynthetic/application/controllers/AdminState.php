<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminState extends ZLAdmin_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('state_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            //$this->arr_view_data = array();

            //Active menu
            $this->arr_view_data['active_menu'] = 'state';
            
            //Get list data
            $arr_data = $this->state_model->get_list_data();

            //Set post data back to view
            $this->arr_view_data['data'] = $arr_data;

            //Set layout
            $this->layout->load('admin', 'state/backend/manager', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    //Update/New data
    function execute_query() {
        //Get post data
        $content_state_id = $_POST['content_state_id'];
        $title = $_POST['title'];
        $alias = $_POST['alias'];

        //Execute query
        if ($content_state_id == 0) {
            //New
            $arr_data = $this->state_model->insert_new_data(array(
                'title' => $title,
                'alias' => $alias,
                'created_date' => strtotime(date(DATE_FORMAT))
            ));
        } else {
            //Update
            $arr_data = $this->state_model->update_existing_data(array(
                'content_state_id' => $content_state_id
                    ), array(
                'title' => $title,
                'alias' => $alias
            ));
        }
    }

    //Get data
    function get_data() {

        //Get list data
        $arr_data = $this->state_model->get_list_data();
        echo json_encode($arr_data);
    }

    //Delete
    function delete() {
        //Get post data
        $content_state_id = $_POST['content_state_id'];

        //Execute query
        $this->state_model->delete_data(array(
            'content_state_id' => $content_state_id
        ));
    }

}
