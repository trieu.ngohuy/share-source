<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminProject extends ZLAdmin_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('project_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            //$this->arr_view_data = array();

            //Active menu
            $this->arr_view_data['active_menu'] = 'project';
            
            //Get list data
            $arr_data = $this->project_model->get_list_data();
            foreach ($arr_data as &$value) {
                $tmp = $this->project_model->get_single_data(array('project_id' => $value['parent_gid']));
                $value['parent_title'] = $tmp['title'];
            }
            unset($value);

            //Set post data back to view
            $this->arr_view_data['data'] = $arr_data;

            //Set layout
            $this->layout->load('admin', 'project/backend/manager', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    //Update/New data
    function execute_query() {
        //Get post data
        $project_id = $_POST['project_id'];
        $title = $_POST['title'];
        $alias = $_POST['alias'];
        $parent_gid = $_POST['parent_gid'];

        //Execute query
        if ($project_id == 0) {
            //New
            $arr_data = $this->project_model->insert_new_data(array(
                'title' => $title,
                'alias' => $alias,
                'parent_gid' => $parent_gid,
                'created_date' => strtotime(date(DATE_FORMAT))
            ));
        } else {
            //Update
            $arr_data = $this->project_model->update_existing_data(array(
                'project_id' => $project_id
                    ), array(
                'title' => $title,
                'alias' => $alias,
                'parent_gid' => $parent_gid
            ));
        }
    }

    //Get data
    function get_data() {
        //Get list data
        $arr_data = $this->project_model->get_list_data();
        foreach ($arr_data as &$value) {
            $tmp = $this->project_model->get_single_data(array('project_id' => $value['parent_gid']));
            $value['parent_title'] = $tmp['title'];
        }
        unset($value);
        echo json_encode($arr_data);
    }

    //Delete
    function delete() {
        //Get post data
        $project_id = $_POST['project_id'];

        //Execute query
        $this->project_model->delete_data(array(
            'project_id' => $project_id
        ));
    }

}
