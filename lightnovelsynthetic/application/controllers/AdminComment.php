<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminComment extends ZLAdmin_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('comment_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            //$this->arr_view_data = array();

            //Active menu
            $this->arr_view_data['active_menu'] = 'comment';
            
            //Get list data
            $this->arr_view_data['data'] = $this->get_list_data();

            //Get list user
            $this->load->model('user_model');
            $this->arr_view_data['user'] = $this->user_model->get_list_data();

            //Set layout
            $this->layout->load('admin', 'comment/backend/manager', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    function get_list_data(){
        //Get list data
        $arr_data = $this->comment_model->get_list_data();
        $this->load->model('user_model');
        $this->load->model('content_model');
        $this->load->model('season_model');
        $this->load->model('episodes_model');
        foreach ($arr_data as &$value) {
            //Limit full text
            $value['full_text_cut'] = substr($value['full_text'], 0, 45) . '...';
            //Get user full name
            $tmp = $this->user_model->get_single_data(array(
                'user_id' => $value['user_id']
            ));
            $value['full_name'] = $tmp['full_name'];
            //Get parent comment
            if ($value['parent_id'] == 0) {
                $value['parent_comment'] = '---';
            } else {
                $tmp = $this->comment_model->get_single_data(array(
                    'comment_id' => $value['parent_id']
                ));
                $parent_full_text = substr($tmp['full_text'], 0, 10);
                $tmp = $this->user_model->get_single_data(array(
                    'user_id' => $tmp['user_id']
                ));
                $value['parent_comment'] = $tmp['full_name'] . ': ' . $parent_full_text . '...';
            }
            //Get content and content_episodes name
            if($value['content_episodes_id'] !== "0"){
                $tmp = $this->episodes_model->get_single_data(array(
                    'content_episodes_id' => $value['content_episodes_id']
                ));
                $value['episodes'] = $tmp['title'];
                $tmp = $this->season_model->get_single_data(array(
                    'content_season_id' => $tmp['content_season_id']
                ));
                $tmp = $this->content_model->get_single_data(array(
                    'content_id' => $tmp['content_id']
                ));
                $value['content'] = $tmp['title'];
            }else{
                $tmp = $this->content_model->get_single_data(array(
                    'content_id' => $value['content_id']
                ));
                $value['content'] = $tmp['title'];
                $value['episodes'] = '---';
            }
        }
        unset($value);
        return $arr_data;
    }

    //Update/New data
    function execute_query() {
        //Get post data
        $comment_id = $_POST['comment_id'];
        $full_text = $_POST['full_text'];

        //Execute query
        if ($comment_id == 0) {
            //New
            $arr_data = $this->comment_model->insert_new_data(array(
                'full_text' => $full_text,
                'created_date' => strtotime(date(DATE_FORMAT))
            ));
        } else {
            //Update
            $arr_data = $this->comment_model->update_existing_data(array(
                'comment_id' => $comment_id
                    ), array(
                'full_text' => $full_text
            ));
        }
    }

    //Get data
    function get_data() {
        echo json_encode($this->get_list_data());
    }

    //Delete
    function delete() {
        //Get post data
        $comment_id = $_POST['comment_id'];

        //Execute query
        $this->comment_model->delete_data(array(
            'comment_id' => $comment_id
        ));
    }

}
