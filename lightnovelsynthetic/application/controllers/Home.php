<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends ZLFront_Controller {

    /**
     * Index Page for this controller.
     */
    public function index() {

        //Website title
        $this->arr_view_data['website_title'] = 'Trang chủ';

        //get list contents
        $this->load->model('content_model');
        $this->load->model('season_model');
        $this->load->model('episodes_model');
        $arr_data = $this->episodes_model->get_list_data();
        sort_array($arr_data, 'created_date_number');
        foreach ($arr_data as $key => &$value) {
        	//Get season
        	$tmp = $this->season_model->get_single_data(array(
        		'content_season_id' => $value['content_season_id']
        	));
        	$value['season'] = $tmp['title'];
        	$value['season_url'] = $tmp['url'];
        	//Get content
        	$tmp = $this->content_model->get_single_data(array(
        		'content_id' => $tmp['content_id']
        	));
        	$value['content'] = $tmp['title'];
        	$value['content_logo'] = $tmp['logo'];
        	$value['content_url'] = $tmp['url'];
        }
        unset($value);
        $this->arr_view_data['data'] = $arr_data;
        //Category logo
        $this->arr_view_data['url'] = base_url() . URL_CATEGORY;

        $this->layout->load('front', 'home/index', $this->arr_view_data);
    }

}
