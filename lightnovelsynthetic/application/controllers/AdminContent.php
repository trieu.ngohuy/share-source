<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminContent extends ZLAdmin_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('content_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            //$this->arr_view_data = array();

            //Active menu
            $this->arr_view_data['active_menu'] = 'content';
            
            //Get list data
            $arr_data = $this->content_model->get_list_data();
            foreach ($arr_data as &$value) {
                $this->load->model('state_model');
                $tmp = $this->state_model->get_single_data(array(
                    'content_state_id' => $value['content_state_id']
                ));
                $value['content_state'] = $tmp['title'];
            }
            unset($value);

            //Set post data back to view
            $this->arr_view_data['data'] = $arr_data;

            //Get list category
            $this->load->model('category_model');
            $this->arr_view_data['category'] = $this->category_model->get_list_data();

            //Get list projects
            $this->load->model('project_model');
            $this->arr_view_data['project'] = $this->project_model->get_list_data();

            //Get list state
            $this->load->model('state_model');
            $this->arr_view_data['state'] = $this->state_model->get_list_data();

            //Set layout
            $this->layout->load('admin', 'content/backend/manager', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    //Update/New data
    function execute_query() {
        //Get post data
        $content_id = $_POST['content_id'];
        $arr_query_data = array(
            'title' => $_POST['title'],
            'alias' => $_POST['alias'],
            'list_content_category_id' => $_POST['list_content_category_id'],
            'list_project_id' => $_POST['list_project_id'],
            'content_state_id' => $_POST['content_state_id'],
            'participator' => $_POST['participator'],
            'author' => $_POST['author'],
            'article' => $_POST['article'],
            'language' => $_POST['language'],
            'progress' => $_POST['progress'],
            'translator' => $_POST['translator'],
            'note' => $_POST['note'],
            'title_en' => $_POST['title_en'],
            'title_ja' => $_POST['title_ja'],
            'title_romaji' => $_POST['title_romaji'],
            'type' => $_POST['type'],
            'full_text' => $_POST['full_text'],
            'is_adult' => $_POST['is_adult']
            //'is_display_translator' => $_POST['is_display_translator']
        );
        
        //Upload images
        $upload_logo = '';
        if (isset($_FILES['logo']['name'])) {
            $upload_logo = $this->upload_images('logo');
        }
        $upload_images= '';
        if (isset($_FILES['images']['name'])) {
            $upload_images = $this->upload_images('images');
        }
        if ($upload_logo !== false && $upload_images !== false) {
            if ($upload_logo !== '') {
                $arr_query_data['logo'] = $upload_logo;
            }
            if ($upload_images !== '') {
                $arr_query_data['images'] = $upload_images;
            }
            //Execute query
            if ($content_id == 0) {
                $arr_query_data['created_date'] = strtotime(date(DATE_FORMAT));
                //New
                $arr_data = $this->content_model->insert_new_data($arr_query_data);
            } else {
                //Update
                $arr_data = $this->content_model->update_existing_data(array(
                    'content_id' => $content_id
                        ), $arr_query_data);
            }
            echo '1';
        }else{
            echo '0';
        }
    }

    //Upload images
    function upload_images($name) {
        $is_upload_success = true;
        $path = '';
        //Get login user data
        $arr_login_user = $this->session->userdata(SS_ADMIN_LOGIN);

        $upload_logo = upload_images($this, $arr_login_user['username'], $name);
        if ($upload_logo['status'] === false) {
            //print_r($upload_logo['error']);
            $is_upload_success = false;
        } else {
            $path = USER_UPLOAD_PATH . $arr_login_user['username'] . '/' . $upload_logo['data']['upload_data']['file_name'];
        }

        if ($is_upload_success === true) {
            return $path;
        } else {
            return false;
        }
    }

    //Get data
    function get_data() {

        //Get list data
        $arr_data = $this->content_model->get_list_data();
        foreach ($arr_data as &$value) {
            $this->load->model('state_model');
            $tmp = $this->state_model->get_single_data(array(
                'content_state_id' => $value['content_state_id']
            ));
            $value['content_state'] = $tmp['title'];
        }
        echo json_encode($arr_data);
    }

    //Delete
    function delete() {
        //Get post data
        $content_id = $_POST['content_id'];

        //Get detail
        $arr_data = $this->content_model->get_single_data(array(
            'content_id' => $content_id
        ));
        
        //Execute query
        $this->content_model->delete_data(array(
            'content_id' => $content_id
        ));
        
        //Delete images and logo
        delete_images($arr_data['images']);
        delete_images($arr_data['logo']);
    }

}
