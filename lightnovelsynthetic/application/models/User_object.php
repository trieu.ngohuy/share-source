<?php

class User_object {

    private $user_id;
    private $username;
    private $password;
    private $email;
    private $phone;
    private $address;
    private $full_name;
    private $logo;
    private $template;
    private $sorting;
    private $enabled;
    private $active_code;
    private $created_date;
    private $url;

    /*
     * Get user name
     */

    function username() {
        return $this->username;
    }

    /*
     * Get url
     */

    function url() {
        return $this->url;
    }

    /*
     * Set url
     */

    function set_url($url) {
        $this->url = $url;
    }

}
