<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
/*
 * Admin route
 */
$route['admin.html'] = 'AdminSetting/Index';
$route['admin/login.html'] = 'Access/AdminLogin';
$route['admin/logout.html'] = 'Access/AdminLogout';
$route['admin/setting.html'] = 'AdminSetting/Index';
$route['admin/project.html'] = 'AdminProject/Index';
$route['admin/category.html'] = 'AdminCategory/Index';
$route['admin/content.html'] = 'AdminContent/Index';
$route['admin/season.html'] = 'AdminSeason/Index';
$route['admin/episodes.html'] = 'AdminEpisodes/Index';
$route['admin/comment.html'] = 'AdminComment/Index';
$route['admin/user.html'] = 'AdminUser/Index';
$route['admin/state.html'] = 'AdminState/Index';
/*
 * Client route
 */
$route['dang-nhap'] = 'access/clientlogin';
$route['dang-ky'] = 'access/clientregister';
$route['dang-xuat'] = 'access/clientlogout';
$route['cart.html'] = 'cart/index';
$route['du-an/(:any)'] = 'project/detail/$1';
$route['du-an'] = 'project/index';
$route['the-loai'] = 'category/index';
$route['the-loai/(:any)'] = 'category/detail/$1';
$route['tinh-trang/(:any)'] = 'state/index/$1';
$route['truyen/(:any)'] = 'content/index/$1';
$route['doc-truyen/(:any)'] = 'episodes/detail/$1';
$route['tim-kiem'] = 'content/search';
//Switch language
$route['switch-lang/(:any)'] = 'languageswitcher/switchlang/$1';
//Wildcards
$route['product/(:any)'] = 'catalog/product_lookup'; //any is anything user type
//Regular expressions
$route['products/([a-z]+)/([^/]+)'] = 'product/id/$1/gid/$2'; //$1 and $2 is variable user type in url. [a-z]+ - only string, [^/]+ - any value, [0-9]+ - only number
//Call back url
$route['products/([a-zA-Z]+)/edit/(\d+)'] = function ($product_type, $id)
{
        return 'catalog/product_edit/' . strtolower($product_type) . '/' . $id;
};