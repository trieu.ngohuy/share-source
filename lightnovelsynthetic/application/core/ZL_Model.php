<?php

class Zl_model extends CI_Model {

    private $_error_message = "Check Input parameters.";
    private $_join_prefix = "tbl_";
    private $_object;
    private $_table;

    /*
     * Constructor
     */

    function __construct($table = null, $object = null) {
        parent::__construct();
        if ($table == null || $object == null) {
            throw new Exception($this->_error_message);
        } else {
            //Prefix table name
            $this->_table = $this->db->dbprefix($table);
            //Protect identifiers
            $this->db->protect_identifiers($this->_table);
            //Set model object
            $this->_object = $object;
        }
    }

    /*
     * Format data
     */

    function format_data($arrData = array()) {
        foreach ($arrData as &$value) {
            //url
            $value['url'] = $this->routes($value);
            if(isset($value['created_date'])){
                $value['created_date_number'] = $value['created_date'];
                $value['created_date'] = date(DATE_FORMAT, $value['created_date']);    
            }
        }
        unset($value);
        return $arrData;
    }
    function format_data_object($objData) {
        $objData['url'] = $this->routes($objData);
        return $objData;
    }

    /*
     * Establish routes
     */

    function routes($objData) {
        //lang
        if ($this->_table == 'zl_lang') {
            return url_switch_lang() . $objData['code'];
        }
        //content
        if ($this->_table == 'zl_content') {
            return base_url() . URL_CONTENT . $objData['alias'];
        }
        //category
        if ($this->_table == 'zl_content_category') {
            return base_url() . URL_CATEGORY . $objData['alias'];
        }
        //episodes
        if ($this->_table == 'zl_content_episodes') {
            return base_url() . URL_EPISODES . $objData['alias'];
        }
        //state
        if ($this->_table == 'zl_content_state') {
            return base_url() . URL_STATE . $objData['alias'];
        }
        //project
        if ($this->_table == 'zl_project') {
            return base_url() . URL_PROJECT . $objData['alias'];
        }
    }

    /*
     * Escape data array
     */

    function EscapeData($arrData = null) {
        if ($arrData == null) {
            throw new Exception($this->_error_message);
        } else {
            foreach ($arrData as $key => &$value) {
                //Escape value
                if (is_array($value)) {
                    $value = $this->EscapeData($value);
                } else {
                    $value = $this->db->escape_str($value);
                }
                //Escape key
                $tmpKey = $this->db->escape_str($key);
                $arrData[$tmpKey] = $arrData[$key];
            }
            unset($value);
            return $arrData;
        }
    }

    /*
     * Default query condition
     */

    function check_query_condition($arrCondition = array()) {
        //Lang
        if (!array_key_exists('lang_id', $arrCondition)) {
            $arrCondition['lang_id'] = DEFAULT_LANG;
        }
        //Enabled
        if (!array_key_exists('enabled', $arrCondition)) {
            $arrCondition['enabled'] = 1;
        }
    }

    /*
     * Get single data
     */

    function get_single_data($arrCondition = array(), $arrJoin = null) {
        //Condition
        if (count($arrCondition) > 0) {
            //Escape queries
            $arrCondition = $this->EscapeData($arrCondition);
            $this->db->where($arrCondition);
        }
        //Join
        if ($arrJoin != null) {
            //Escape queries
            $arrJoin = $this->EscapeData($arrJoin);
            //Select from main table
            $this->db->from($this->_table . ' as ' . $this->_join_prefix . $this->_table);
            //Join
            foreach ($arrJoin as $value) {
                $this->db->join($value['strTable'], $value['strCondition']);
            }
        }
        //Query
        $objResult = $this->db->get($this->_table);
        //Handle errors
        if ($objResult) {
            //$objData = $objResult->custom_row_object(0, $this->_object);
            $objData = $objResult->row_array();
            if(count($objData) > 0){
                $objData = $this->format_data_object($objData);
            }            
            return count($objData) > 0 ? $objData : false;
        } else {
            throw new Exception($this->db->error());
        }
    }

    /*
     * Get list data
     */

    function get_list_data($select = null, $arrCondition = array(), $arrJoin = null, $count = null, $paging = null) {
        //Set setlect
        if($select != null){
            $this->db->select($select);
        }
        //Condition
        if (count($arrCondition) > 0) {
            //Escape queries
            $arrCondition = $this->EscapeData($arrCondition);
            $this->db->where($arrCondition);
        }
        //Join
        if ($arrJoin != null) {
            //Escape queries
            $arrJoin = $this->EscapeData($arrJoin);
            //Select from main table
            $this->db->from($this->_table . ' as ' . $this->_join_prefix . $this->_table);
            //Join
            foreach ($arrJoin as $key => $value) {
                $this->db->join($key, $value);
            }
        }
        //Limit
        if ($count != null && $paging != null) {
            $this->db->limit($this->db->escape_str($count), $this->db->escape_str($paging));
        }
        //Query
        $objResult = $this->db->get($this->_table);
        //Handle errors
        if ($objResult) {
            //$arrData = $objResult->custom_result_object($this->_object);
            $arrData = $objResult->result_array();
            $arrData = $this->format_data($arrData);
            return $arrData;
        } else {
            throw new Exception($this->db->error());
        }
    }

    /*
     * Insert new data
     */

    function insert_new_data($arrData = null) {
        if ($arrData == null) {
            throw new Exception($this->_error_message);
        } else {
            //Escape insert data
            $arrData = $this->EscapeData($arrData);
            //Insert data
            $objResult = $this->db->insert($this->_table, $arrData);
            //Handle errors
            if (!$objResult) {
                throw new Exception($this->db->error());
            }
        }
    }

    /*
     * Update existing data
     */

    function update_existing_data($arrCondition = null, $arrData = null) {
        if ($arrData == null || $arrCondition == null) {
            throw new Exception($this->_error_message);
        } else {
            //Escape insert data
            $arrCondition = $this->EscapeData($arrCondition);
            $arrData = $this->EscapeData($arrData);
            //Insert data
            $this->db->where($arrCondition);
            $objResult = $this->db->update($this->_table, $arrData);
            //Handle errors
            if (!$objResult) {
                throw new Exception($this->db->error());
            }
        }
    }

    /*
     * Update existing data
     */

    function delete_data($arrCondition = null) {
        if ($arrCondition == null) {
            throw new Exception($this->_error_message);
        } else {
            //Escape insert data
            $arrCondition = $this->EscapeData($arrCondition);
            //Insert data
            $this->db->where($arrCondition);
            $objResult = $this->db->delete($this->_table);
            //Handle errors
            if (!$objResult) {
                throw new Exception($this->db->error());
            }
        }
    }

}
