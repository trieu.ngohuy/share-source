<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quản lý bình luận
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <!-- <div class="box-header al-right">
                    <button type="button" id="btn_new" class="btn btn-primary">Thêm mới</button>
                </div> -->
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Truyện</th>
                                <th>Chương</th>
                                <th>Trả lời bình luận của</th>
                                <th>Người comment</th>
                                <th>Nội dung</th>
                                <th>Ngày comment</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($data as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $value['content']; ?></td>
                                    <td><?php echo $value['episodes']; ?></td>
                                    <td><?php echo $value['parent_comment']; ?></td>
                                    <td><?php echo $value['full_name']; ?></td>
                                    <td><?php echo $value['full_text_cut']; ?></td>
                                    <td><?php echo $value['created_date']; ?></td>
                                    <td>
                                        <a href="#" class="btn-edit" data-index="<?php echo $count - 1; ?>"><i class="fa fa-edit"></i> Sửa</a>
                                        <a href="#" class="btn-remove" data-index="<?php echo $count - 1; ?>"><i class="fa fa-remove"></i> Xóa</a>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Truyện</th>
                                <th>Chương</th>
                                <th>Trả lời bình luận của</th>
                                <th>Người comment</th>
                                <th>Nội dung</th>
                                <th>Ngày comment</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade" id="modal-data">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body">
                <!-- <div class="form-group">
                    <label>Trả lời comment</label>
                    <select id="com_parent" class="form-control select2"  data-placeholder="Chọn bình luận cha">
                        <option value="0">---</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Người trả lời</label>
                    <select id="com_user" class="form-control select2" data-placeholder="Chọn người trả lời"
                            style="width: 100%;">
                        <option value="">---</option>
                        <?php
                        foreach ($user as $value) {
                            echo '<option value="' . $value['user_id'] . '">' . $value['full_name'] . '</option>';
                        }
                        ?>
                    </select>
                </div> -->
                <div class="form-group">
                    <label>Nội dung</label>
                    <textarea class="form-control" rows="3" placeholder="Nhập nội dung" id="edi_fulltext"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="btn-save">Lưu</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Style -->
<style>
    .select2-container .select2-selection--single {
        height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 31px;
    }
</style>
<!-- page script -->
<script>
    //Convert php array to js array
<?php
$js_array = json_encode($data);
echo "var arr_data = " . $js_array . ";\n";
?>
    var mode = 'new';
    var comment_id = 0;
    $(function () {
        //Initialize datatable
        $('#example1').DataTable();
    });
    $(document).ready(function () {

        //Title on change
        $("#txt_title").on('input', function () {
            $("#txt_alias").val(convert_vi_to_en($(this).val()));
        });
        //Click new button
        $(document).on('click', '#btn_new', function (e) {
            //Update mode
            mode = 'new';
            //Set title
            $('.modal-title').html('Thêm mới');
            //Project id
            comment_id = 0;
            //Reset modal data
            $('#edi_fulltext').val('');
            //Add data into parent select
            $("#com_parent").find('option').not(':first').remove();
            for (var i = 0; i < arr_data.length; i++) {
                $("#com_parent").append(new Option(arr_data[i]['full_name'] + ': ' + arr_data[i]['full_text'].substring(0, 30)+'...', arr_data[i]['comment_id']));
            }
            $('#com_parent').val($('#com_parent option:eq(0)').val()).trigger('change');
            $("#com_user").val(null).trigger("change");
            
            //Enable combobox
            $('#com_parent').prop('disabled', false);
            $('#com_user').prop('disabled', false);
            //Open modal
            $("#modal-data").modal();
        });
        //Click remove button
        $(document).on('click', '.btn-remove', function (e) {
            if (confirm("Are you sure you want to delete this?")) {
                //Get id
                var index = $(this).attr('data-index');
                //Get edit data
                var obj_data = arr_data[index];
                //Project id
                comment_id = obj_data['comment_id'];
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url() ?>/AdminComment/delete",
                    data: {
                        'comment_id': comment_id
                    },
                    success: function (objData) {
                        //Refresh data
                        refresh_data();
                    },
                    error: function (jqXHR, exception) {
                        alert('Có lỗi. Thử lại sau.');
                    }
                });
            }
        });
        //Click edit button
        $(document).on('click', '.btn-edit', function (e) {
            //Update mode
            mode = 'edit';
            //Get id
            var index = $(this).attr('data-index');
            //Set title
            $('.modal-title').html('Chỉnh sửa');
            //Get edit data
            var obj_data = arr_data[index];
            //Project id
            comment_id = obj_data['comment_id'];
            //Fill modal data
            $('#edi_fulltext').val(obj_data['full_text']);
            //Add data to parent combobox
            $("#com_parent").find('option').not(':first').remove();
            for (var i = 0; i < arr_data.length; i++) {
                $("#com_parent").append(new Option(arr_data[i]['full_name'] + ': ' + arr_data[i]['full_text'].substring(0, 30)+'...', arr_data[i]['comment_id']));
            }
            if (parseInt(obj_data['parent_id']) === 0) {
                $('#com_parent').val($('#com_parent option:eq(0)').val()).trigger('change');
            } else {
                $("#com_parent").val(parseInt(obj_data['parent_id'])).trigger('change');
            }
            $("#com_user").val(parseInt(obj_data['user_id'])).trigger('change');
            //Disabled combobox
            $('#com_parent').prop('disabled', true);
            $('#com_user').prop('disabled', true);
            //Open modal
            $("#modal-data").modal();
        });

        //Modal save button click
        $('#btn-save').click(function () {

            //If add new then check if duplicate alias
            if (verify_data() === false) {
                return;
            }

            //Hide modal
            $('#modal-data').modal('toggle');
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url() ?>/AdminComment/execute_query",
                data: {
                    'comment_id': comment_id,
                    'full_text': $('#edi_fulltext').val()
                },
                success: function (objData) {
                    //Refresh data
                    refresh_data();
                },
                error: function (jqXHR, exception) {
                    alert('Có lỗi. Thử lại sau.');
                }
            });
        });
    });

    //Verify data
    function verify_data() {
        //Check empty input
        if ($('#edi_fulltext').val() === '') {
            alert('Nội dung comment không được rỗng!');
            return false;
        }
        if ($('#com_user').val() === '') {
            alert('Người trả lời không được rỗng!');
            return false;
        }
        //Check valid data
        //Can' be self reply
        if(comment_id !== 0 && comment_id === $('#com_parent').val()){
            alert('Không thể trả lời comment của chính mình!');
            return false;
        }
        //Parent can't reply child comment
        if(comment_id !== 0 && check_infinity_reply($('#com_parent').val())){
            alert('Bình luận cha không thể trả lời bình luận con hoặc bình luận con không thể trả lời bình luận cha!');
            return false;
        }
        //Child comment can't reply parent comment
    }
    function check_infinity_reply(reply_id){
        //Get current comment
        var result = $.grep(arr_data, function (e) {
            return e.comment_id === comment_id;
        });
        if(reply_id === result[0]['parent_id']){
            return false;
        }
        for(var i = 0 ; i < arr_data.length ; i++){
            if((arr_data[i]['comment_id'] === reply_id && arr_data[i]['parent_id'] === comment_id) ||
                    (arr_data[i]['comment_id'] === comment_id && arr_data[i]['parent_id'] === reply_id)){
                return true;
            }
        }
        return false;
    }
    /*
     * Refill datatable data
     */
    function refresh_data() {
        //Refresh data
        $.ajax({
            type: 'GET',
            url: "<?php echo base_url() ?>/AdminComment/get_data",
            success: function (data) {
                data = $.parseJSON(data);
                arr_data = data;
                //Fill datatable
                $('#example1 tbody').html('');
                var html = '';
                for (var i = 0; i < data.length; i++) {
                    html += '<tr>'
                            + '<td>' + (i + 1) + '</td>'
                            + '<td>' + data[i]['content'] + '</td>'
                            + '<td>' + data[i]['episodes'] + '</td>'
                            + '<td>' + data[i]['parent_comment'] + '</td>'
                            + '<td>' + data[i]['full_name'] + '</td>'
                            + '<td>' + data[i]['full_text_cut'] + '</td>'
                            + '<td>' + data[i]['created_date'] + '</td>'
                            + '<td>'
                            + '<a href="#" class="btn-edit" data-index="' + i + '"><i class="fa fa-edit"></i> Sửa</a>'
                            + ' <a href="#" class="btn-remove" data-index="' + i + '"><i class="fa fa-remove"></i> Xóa</a>'
                            + '</td>'
                            + '</tr>';
                }
                $('#example1 tbody').html(html);
                //Initialize datatable
                $('#example1').DataTable();

            },
            error: function () {
                alert('Có lỗi. Thử lại sau.');
            }
        });
    }
</script>