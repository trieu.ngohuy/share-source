<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quản lý truyện
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header al-right">
                    <button type="button" id="btn_new" class="btn btn-primary">Thêm mới</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tên truyện</th>
                                <th>Logo</th>
                                <th>Tác giả</th>
                                <th>Tình trạng</th>
                                <th>Lượt xem</th>
                                <th>Ngày tạo</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($data as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $value['title']; ?></td>
                                    <td><img src="<?php echo base_url().$value['logo']; ?>" width="50" /></td>
                                    <td><?php echo $value['author']; ?></td>
                                    <td><?php echo $value['content_state']; ?></td>
                                    <td><?php echo $value['view']; ?></td>
                                    <td><?php echo $value['created_date']; ?></td>
                                    <td>
                                        <a href="#" class="btn-edit" data-index="<?php echo $count - 1; ?>"><i class="fa fa-edit"></i> Sửa</a>
                                        <a href="#" class="btn-remove" data-index="<?php echo $count - 1; ?>"><i class="fa fa-remove"></i> Xóa</a>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Tên truyện</th>
                                <th>Logo</th>
                                <th>Tác giả</th>
                                <th>Tình trạng</th>
                                <th>Lượt xem</th>
                                <th>Ngày tạo</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade" id="modal-data">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Tên truyện</label>
                    <input id="txt_title" type="text" class="form-control" placeholder="Tên truyện">
                </div>
                <div class="form-group">
                    <label>Tên rút gọn</label>
                    <input id="txt_alias" type="text" class="form-control" placeholder="Tên rút gọn">
                    <p class="help-block">Tên rút gọn không được trùng.</p>
                </div>
                <div class="form-group pos-related">
                    <label for="exampleInputFile">Hình nền</label>
                    <input name="images" type="file" id="images">
                    <img class="upload_img" src="" width="50" id="img_images"/>
                    <p class="help-block">Tải lên hình nền cho truyện. Chỉ hổ trợ định dạng png/jpg.</p>
                </div>
                <div class="form-group pos-related">
                    <label for="exampleInputFile">Logo</label>
                    <input name="logo" type="file" id="logo">
                    <img class="upload_img" src="" width="50" id="img_logo"/>
                    <p class="help-block">Tải lên logo cho truyện. Chỉ hổ trợ định dạng png/jpg.</p>
                </div>
                <div class="form-group">
                    <label>Thể loại</label>
                    <select id="com_category" class="form-control select2" multiple="multiple" data-placeholder="Chọn thể loại"
                            style="width: 100%;">
                                <?php
                                foreach ($category as $value) {
                                    echo '<option value="' . $value['content_category_id'] . '">' . $value['title'] . '</option>';
                                }
                                ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Dự án</label>
                    <select id="com_project" class="form-control select2" multiple="multiple" data-placeholder="Chọn dự án"
                            style="width: 100%;">
                                <?php
                                foreach ($project as $value) {
                                    echo '<option value="' . $value['project_id'] . '">' . $value['title'] . '</option>';
                                }
                                ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Tình trạng</label>
                    <select id="com_state" class="form-control select2"data-placeholder="Chọn tình trạng"
                            style="width: 100%;">
                                <?php
                                foreach ($state as $value) {
                                    echo '<option value="' . $value['content_state_id'] . '">' . $value['title'] . '</option>';
                                }
                                ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Mô tả</label>
                    <textarea id="edi_fulltext" name="editor1" class="form-control" rows="10" placeholder="Mô tả"></textarea>
                </div>
                <div class="form-group">
                    <label>Tên tiếng anh</label>
                    <input id="txt_title_en" type="text" class="form-control" placeholder="Tên tiếng anh">
                </div>
                <div class="form-group">
                    <label>Tên tiếng nhật</label>
                    <input id="txt_title_ja" type="text" class="form-control" placeholder="Tên tiếng nhật">
                </div>
                <div class="form-group">
                    <label>Tên Romaji</label>
                    <input id="txt_title_romaji" type="text" class="form-control" placeholder="Tên romaji">
                </div>
                <div class="form-group">
                    <label>Tham gia</label>
                    <input id="txt_participator" type="text" class="form-control" placeholder="Tham gia">
                </div>
                <div class="form-group">
                    <label>Tác giả</label>
                    <input id="txt_author" type="text" class="form-control" placeholder="Tác giả">
                </div>
                <div class="form-group">
                    <label>Họa sỉ</label>
                    <input id="txt_article" type="text" class="form-control" placeholder="Họa sỉ">
                </div>
                <div class="form-group">
                    <label>Ngôn ngữ</label>
                    <input id="txt_language" type="text" class="form-control" placeholder="Ngôn ngữ">
                </div>
                <div class="form-group">
                    <label>Tiến trình</label>
                    <input id="txt_progress" type="text" class="form-control" placeholder="Tiến trình">
                </div>
                <div class="form-group">
                    <label>Loại truyện</label>
                    <input id="txt_type" type="text" class="form-control" placeholder="Loại truyện">
                </div>
                <div class="form-group">
                    <label>Ghi chú</label>
                    <input id="txt_note" class="form-control" placeholder="Ghi chú">
                </div>
                <div class="form-group">
                    <label>Nhóm dịch</label>
                    <input id="txt_translator" type="text" class="form-control" placeholder="Nhóm dịch">
                </div>
                <div class="form-group">
                    <label>Truyện chứa nội dung người lớn?</label>
                    <div class="radio">
                        <label>
                          <input type="radio" name="is_adult" id="red_adult_no" value="0" checked="">
                          Không
                        </label>
                        <label>
                          <input type="radio" name="is_adult" id="red_adult_yes" value="1">
                          Có
                        </label>
                      </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="btn-save">Lưu</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Style -->
<style>
    .select2-container .select2-selection--single {
        height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 31px;
    }
</style>
<!-- page script -->
<script>
    //Convert php array to js array
<?php
$js_array = json_encode($data);
echo "var arr_data = " . $js_array . ";\n";
?>
    var mode = 'new';
    var content_id = 0;
    $(function () {
        //Initialize datatable
        $('#example1').DataTable();
        //Initialize Select2 Elements
        $('.select2').select2()
    });
    $(document).ready(function () {
        //Init ckeditor
        CKEDITOR.replace('editor1', {
            toolbarGroups : [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                { name: 'forms', groups: [ 'forms' ] },
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                { name: 'links', groups: [ 'links' ] },
                { name: 'insert', groups: [ 'insert' ] },
                '/',
                { name: 'styles', groups: [ 'styles' ] },
                { name: 'colors', groups: [ 'colors' ] },
                { name: 'tools', groups: [ 'tools' ] },
                { name: 'others', groups: [ 'others' ] },
                { name: 'about', groups: [ 'about' ] }
            ],
            removePlugins: 'easyimage',
            removeButtons : 'Source,Save,NewPage,Preview,Templates,Scayt,Form,Checkbox,Radio,TextField,Textarea,Button,Select,ImageButton,HiddenField,CreateDiv,Language,Flash,Iframe,PageBreak,HorizontalRule,About,Print,Image,SpecialChar,EasyImage'
        });
        //Title on change
        $("#txt_title").on('input', function () {
            $("#txt_alias").val(convert_vi_to_en($(this).val()));
        });
        //Click new button
        $(document).on('click', '#btn_new', function (e) {
            //Update mode
            mode = 'new';
            //Set title
            $('.modal-title').html('Thêm mới');
            //Project id
            content_id = 0;
            //Reset modal data
            $("#txt_title").val('');
            $("#txt_alias").val('');
            $("#com_category").val(null).trigger("change");
            $("#com_project").val(null).trigger("change");
            $("#com_state").val(null).trigger("change");
            $('.upload_img').hide();
            $("#txt_participator").val('');
            $("#txt_author").val('');
            $("#txt_article").val('');
            $("#txt_language").val('');
            $("#txt_progress").val('');
            $("#txt_translator").val('');
            $("#txt_title_en").val('');
            $("#txt_title_ja").val('');
            $("#txt_title_romaji").val('');
            $("#txt_type").val('');
            $("#txt_note").val('');
            $("#logo").val('');
            CKEDITOR.instances['edi_fulltext'].setData('');
            $("#images").val('');
            //$("#chk_display_translator").prop('checked', true);
            $("input[name=is_adult][value='0']").prop("checked",true);
            //Open modal
            $("#modal-data").modal();
        });
        //Click remove button
        $(document).on('click', '.btn-remove', function (e) {
            if (confirm("Are you sure you want to delete this?")) {
                //Get id
                var index = $(this).attr('data-index');
                //Get edit data
                var obj_data = arr_data[index];
                //Project id
                content_id = obj_data['content_id'];
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url() ?>/AdminContent/delete",
                    data: {
                        'content_id': content_id
                    },
                    success: function (objData) {
                        //Refresh data
                        refresh_data();
                    },
                    error: function (jqXHR, exception) {
                        alert('Có lỗi. Thử lại sau.');
                    }
                });
            }
        });
        //Click edit button
        $(document).on('click', '.btn-edit', function (e) {
            //Update mode
            mode = 'edit';
            //Get id
            var index = $(this).attr('data-index');
            //Set title
            $('.modal-title').html('Chỉnh sửa');
            //Get edit data
            var obj_data = arr_data[index];
            //Project id
            content_id = obj_data['content_id'];
            //Fill modal data
            $("#txt_title").val(obj_data['title']);
            $("#txt_alias").val(obj_data['alias']);
            set_multiple_com('#com_category', obj_data['list_content_category_id']);
            set_multiple_com('#com_project', obj_data['list_project_id']);
            if (content_id === 0) {
                $('#com_state').val($('#com_state option:eq(0)').val()).trigger('change');
            } else {
                $("#com_state").val(parseInt(obj_data['content_state_id'])).trigger('change');
            }
            if (obj_data['logo'] !== '' && obj_data['logo'] !== null) {
                $('#img_logo').show();
                $('#img_logo').attr('src', '<?php echo base_url() ?>' + obj_data['logo']);
            }else{
                $('#img_logo').hide();
            }
            if (obj_data['images'] !== '' && obj_data['images'] !== null) {
                $('#img_images').show();
                $('#img_images').attr('src', '<?php echo base_url() ?>' + obj_data['images']);
            }else{
                $('#img_images').hide();
            }
            $("#txt_participator").val(obj_data['participator']);
            $("#txt_author").val(obj_data['author']);
            $("#txt_article").val(obj_data['article']);
            $("#txt_language").val(obj_data['language']);
            $("#txt_progress").val(obj_data['progress']);
            $("#txt_translator").val(obj_data['translator']);
            $("#txt_note").val(obj_data['note']);
            $("#txt_title_en").val(obj_data['title_en']);
            $("#txt_title_ja").val(obj_data['title_ja']);
            $("#txt_title_romaji").val(obj_data['title_romaji']);
            $("#txt_type").val(obj_data['type']);
            var full_text = obj_data['full_text'];
            full_text = full_text.replace(/\\n/g, "");
            CKEDITOR.instances['edi_fulltext'].setData(full_text);
            if(obj_data['is_display_translator'] === "0"){
                $("#chk_display_translator").prop('checked', false);
            }else{
                $("#chk_display_translator").prop('checked', true);
            }
            $("input[name=is_adult][value='"+obj_data['is_adult']+"']").prop("checked",true);
            
            //Open modal
            $("#modal-data").modal();
        });

        //Modal save button click
        $('#btn-save').click(function () {

            //If add new then check if duplicate alias
            if (verify_data() === false) {
                return;
            }

            //Get list content category id
            var list_content_category_id = get_multiple_select_val('#com_category');
            var list_project_id = get_multiple_select_val('#com_project');

            //Get submit data
            var fd = new FormData();
            var logo_upload = $('#logo')[0].files[0];
            var images_upload = $('#images')[0].files[0];
            fd.append('logo', logo_upload);
            fd.append('images', images_upload);
            fd.append('content_id', content_id);
            fd.append('title', $('#txt_title').val());
            fd.append('alias', $('#txt_alias').val());
            fd.append('list_content_category_id', list_content_category_id);
            fd.append('list_project_id', list_project_id);
            fd.append('content_state_id', $('#com_state').val());
            fd.append('participator', $('#txt_participator').val());
            fd.append('author', $('#txt_author').val());
            fd.append('article', $('#txt_article').val());
            fd.append('language', $('#txt_language').val());
            fd.append('progress', $('#txt_progress').val());
            fd.append('translator', $('#txt_translator').val());
            fd.append('note', $('#txt_note').val());
            fd.append('title_en', $('#txt_title_en').val());
            fd.append('title_ja', $('#txt_title_ja').val());
            fd.append('title_romaji', $('#txt_title_romaji').val());
            fd.append('type', $('#txt_type').val());
            fd.append('full_text', CKEDITOR.instances['edi_fulltext'].getData());
            fd.append('is_adult', $('input[name=is_adult]:checked').val());
            //fd.append('is_display_translator', $('#chk_display_translator').is(":checked"));

            //Hide modal
            $('#modal-data').modal('toggle');
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url() ?>/AdminContent/execute_query",
                data: fd,
                processData: false,
                contentType: false,
                success: function (objData) {
                    if(objData === '1'){
                        //Refresh data
                        refresh_data();
                    }else{
                        alert('Có lỗi. Thử lại sau.');
                    }
                },
                error: function (jqXHR, exception) {
                    alert('Có lỗi. Thử lại sau.');
                }
            });
        });
    });
    //Get selected category
    function get_multiple_select_val(com_name) {
        var arr = $(com_name).select2("val");
        var tmp = '';
        for (var i = 0; i < arr.length; i++) {
            if ((i + 1) < arr.length) {
                tmp += arr[i] + "||";
            } else {
                tmp += arr[i];
            }
        }
        return tmp;
    }
    //Set category
    function set_multiple_com(com_name, list_id) {
        $(com_name).val(null).trigger("change");
        if(list_id === '' || list_id === null){
            return;
        }
        var arr_tmp = list_id.split('||');
        $(com_name).val(arr_tmp);
        $(com_name).trigger('change');
    }
    //Verify data
    function verify_data() {
        //Check empty input
        if ($('#txt_title').val() == '') {
            alert('Tên truyện không được rỗng!');
            return false;
        }
        if ($('#txt_alias').val() == '') {
            alert('Tên rút gọn không được rỗng!');
            return false;
        }
        if ($('#com_category').select2('val').length <= 0) {
            alert('Bắt buộc chọn thể loại!');
            return false;
        }
        if ($('#com_project').select2('val').length <= 0) {
            alert('Bắt buộc chọn dự án!');
            return false;
        }
        if ($('#com_state').val() === null) {
            alert('Bắt buộc chọn tình trạng!');
            return false;
        }
        //Check duplicate data
        var result = $.grep(arr_data, function (e) {
            return e.alias === $('#txt_alias').val() && e.content_id !== content_id;
        });
        if (result.length > 0) {
            alert('Dữ liệu trùng!');
            return false;
        }
    }
    /*
     * Refill datatable data
     */
    function refresh_data() {
        //Refresh data
        $.ajax({
            type: 'GET',
            url: "<?php echo base_url() ?>/AdminContent/get_data",
            success: function (data) {
                data = $.parseJSON(data);
                arr_data = data;
                //Fill datatable
                $('#example1 tbody').html('');
                var html = '';
                for (var i = 0; i < data.length; i++) {
                    html += '<tr>'
                            + '<td>' + (i + 1) + '</td>'
                            + '<td>' + data[i]['title'] + '</td>'
                            + '<td><img src="<?php echo base_url();?>' + data[i]['logo'] + '" width="50"/></td>'
                            + '<td>' + data[i]['author'] + '</td>'
                            + '<td>' + data[i]['content_state'] + '</td>'
                            + '<td>' + data[i]['view'] + '</td>'
                            + '<td>' + data[i]['created_date'] + '</td>'
                            + '<td>'
                            + '<a href="#" class="btn-edit" data-index="' + i + '"><i class="fa fa-edit"></i> Sửa</a>'
                            + ' <a href="#" class="btn-remove" data-index="' + i + '"><i class="fa fa-remove"></i> Xóa</a>'
                            + '</td>'
                            + '</tr>';
                }
                $('#example1 tbody').html(html);
                //Initialize datatable
                $('#example1').DataTable();

            },
            error: function () {
                alert('Có lỗi. Thử lại sau.');
            }
        });
    }
</script>