<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">DANH SÁCH MENU</li>
            <li <?php if(!isset($active_menu) || $active_menu == 'setting')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_SETTING?>">
                    <i class="fa fa-cog"></i> <span>Cài đặt</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'content')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_CONTENT?>">
                    <i class="fa fa-book"></i> <span>Quản lý tiểu thuyết</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'season')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_SEASON?>">
                    <i class="fa fa-book"></i> <span>Quản lý tập</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'episodes')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_EPISODES?>">
                    <i class="fa fa-pencil"></i> <span>Quản lý chương</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'project')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_PROJECT?>">
                    <i class="fa fa-map"></i> <span>Quản lý dự án</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'category')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_CATEGORY?>">
                    <i class="fa fa-list-ul"></i> <span>Quản lý thể loại</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'state')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_STATE?>">
                    <i class="fa fa-tint"></i> <span>Quản lý tình trạng</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'user')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_USER?>">
                    <i class="fa fa-users"></i> <span>Quản lý người dùng</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'comment')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_COMMENT?>">
                    <i class="fa fa-comments"></i> <span>Quản lý comment</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>