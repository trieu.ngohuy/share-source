<div id="rpanel">
    <div class="box login" style="display: none;">
        <div class="btitle">ĐĂNG NHẬP</div>
        <div class="bcontent"><a class="button fb" href="https://www.facebook.com/v3.0/dialog/oauth?client_id=783908218461381&amp;redirect_uri=/dang-nhap">ĐĂNG NHẬP BẰNG FACEBOOK</a></div>
    </div>
    <div class="box">
        <div class="btitle">BẢNG XẾP HẠNG</div>
        <div class="bcontent tabpanel">
            <input id="bxh-lnm" type="radio" name="bxh" checked>
            <input id="bxh-onm" type="radio" name="bxh">
            <input id="bxh-ln" type="radio" name="bxh">
            <div class="tabselect">
                <label class="tab" for="bxh-lnm">Truyện của tháng</label>
                <label class="tab" for="bxh-onm" style="display: none;">Tự sáng tác của tháng</label>
                <label class="tab" for="bxh-ln" style="display: none;">Toàn thời gian</label>
            </div>
            <div class="tabcontent">
                <div class="tab ranklist" for="bxh-lnm">
                    <?php
                    $count = 1;
                        foreach ($global_content as $key => $value) {
                            ?>
                                <a class="item" href="<?php echo $value['url']?>">
                                    <div class="index"><?php echo $count?></div>
                                    <div class="rtitle"><?php echo $value['title']?></div>
                                    <div class="rsubtitle"><?php echo $value['view']?> lượt xem</div>
                                </a>
                            <?php
                            $count++;
                        }
                    ?>
                    
                </div>
                <div class="tab ranklist" for="bxh-onm" style="display: none;">
                    <a class="item" href="/truyen/19-quy-vuong-la-mot-tay-ban-tia">
                        <div class="index">1</div>
                        <div class="rtitle">Quỷ Vương là một tay bắn tỉa!</div>
                        <div class="rsubtitle">147 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/102-persona-5-the-novel">
                        <div class="index">2</div>
                        <div class="rtitle">Persona 5 The Novel</div>
                        <div class="rsubtitle">16 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/20-ga-anh-hung-a-quy-vuong-thang-hikimori-va-ke-hoach-tra-thu-the-gioi-">
                        <div class="index">3</div>
                        <div class="rtitle">Gã Anh Hùng, ả Quỷ Vương, thằng hikimori và kế hoạch trả thù thế giới!</div>
                        <div class="rsubtitle">14 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/21-oln">
                        <div class="index">4</div>
                        <div class="rtitle">The Secrets of The Flawless Stone Orion</div>
                        <div class="rsubtitle">10 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/99-halloween-event">
                        <div class="index">5</div>
                        <div class="rtitle">Halloween event</div>
                        <div class="rsubtitle">7 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/54-co-chu-cua-toi-la-nang-ma-ca-rong-de-thuong">
                        <div class="index">6</div>
                        <div class="rtitle">Cô Chủ Của Tôi Là Nàng Ma Cà Rồng Dễ Thương</div>
                        <div class="rsubtitle">3 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/32-one-shot-neu-con-co-ngay-mai">
                        <div class="index">7</div>
                        <div class="rtitle">One-shot: Nếu còn có ngày mai</div>
                        <div class="rsubtitle">2 lượt xem</div>
                    </a>
                </div>
                <div class="tab ranklist" for="bxh-ln" style="display: none;">
                    <a class="item" href="/truyen/5-boushoku-no-berserk-ore-dake-level-toiu-gainen-o-toppa-suru">
                        <div class="index">1</div>
                        <div class="rtitle">Boushoku no Berserk~ Ore dake Level toiu Gainen o Toppa suru </div>
                        <div class="rsubtitle">351878 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/73-vinh-danh-loai-orc-">
                        <div class="index">2</div>
                        <div class="rtitle">오크지만 찬양해! - Vinh Danh Loài Orc!.</div>
                        <div class="rsubtitle">126631 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/17-breakers">
                        <div class="index">3</div>
                        <div class="rtitle">브레이커즈 - Breakers</div>
                        <div class="rsubtitle">72560 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/6-hyakuma-no-aruji">
                        <div class="index">4</div>
                        <div class="rtitle">Hyaku ma no Omo</div>
                        <div class="rsubtitle">45250 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/4-maou-ni-nattanode-dungeon-tsukutte-jingai-musume-to-honobono-suru">
                        <div class="index">5</div>
                        <div class="rtitle">MAOU NI NATTANODE, DUNGEON TSUKUTTE JINGAI MUSUME TO HONOBONO SURU</div>
                        <div class="rsubtitle">44357 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/1-the-forsaken-hero">
                        <div class="index">6</div>
                        <div class="rtitle">The Forsaken Hero</div>
                        <div class="rsubtitle">30302 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/19-quy-vuong-la-mot-tay-ban-tia">
                        <div class="index">7</div>
                        <div class="rtitle">Quỷ Vương là một tay bắn tỉa!</div>
                        <div class="rsubtitle">26820 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/41-goblin-kingdom">
                        <div class="index">8</div>
                        <div class="rtitle">Goblin Kingdom</div>
                        <div class="rsubtitle">20283 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/89-isekai-de-miryoku-cheat-o-tsukatte-dorei-harem-o-tsukutte-mita">
                        <div class="index">9</div>
                        <div class="rtitle">Seichou cheat de nan demo dekiru you ni nattaga, mushoku dake wa yamerarenai youdesu</div>
                        <div class="rsubtitle">17564 lượt xem</div>
                    </a>
                    <a class="item" href="/truyen/3-sekai-ni-fukushuu-wo-chikatta-shounen">
                        <div class="index">10</div>
                        <div class="rtitle">SEKAI NI FUKUSHUU WO CHIKATTA SHOUNEN</div>
                        <div class="rsubtitle">16969 lượt xem</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--.box.donate.btitle ỦNG HỘ CHÚNG TÔI
.bcontent 
a(href='https://unghotoi.com/shinigamilnteam').button ỦNG HỘ NGAY-->
    <div class="box social">
        <div class="btitle">FANPAGE</div>
        <div class="bcontent">
            <div class="fb-page" data-href="https://www.facebook.com/Light-Novel-Synthetic-975010052708775" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Light-Novel-Synthetic-975010052708775" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Light-Novel-Synthetic-975010052708775">Light Novel Synthetic</a></blockquote></div>
        </div>
    </div>
</div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>