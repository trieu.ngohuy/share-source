<div class="box">
    <div class="btitle">BÌNH LUẬN</div>
    <div class="bcontent comment-wrap">
        <div class="comment_form">
		    <form method="post">
		    	Nội dung: <textarea type="text" name="comment[full_text]" rows="5" required=""> </textarea>
		    	<input type="hidden" name="comment[parent_id]" value="0" />
		    	<input type="hidden" name="comment[content_id]" value="<?php echo isset($data['content_id']) ? $data['content_id'] : 0?>" />
		    	<input type="hidden" name="comment[content_episodes_id]" value="<?php echo isset($data['content_episodes_id']) ? $data['content_episodes_id'] : 0?>" />
				<button type="submit" class="comment_button">Trả lời</button>
		    </form>
		</div>
		<div class="comment_list">
			<?php
				foreach ($comment as $key => $value) {
					?>
						<div class="comment_item">
							<p class="comment_logo">
								<img src="<?php echo base_url().IMG_DEFAULT_USER_LOGO?>" width="30" />
							</p>
							<p class="comment_user"><?php echo $value['full_name']?>: </p>
							<p class="comment_content"><?php echo $value['full_text']?></p>
							<p class="comment_date"><?php echo $value['created_date']?></p>
							<p class="comment_reply">
								<button class="comment_button btn_reply" data-id="<?php echo $value['comment_id']?>">Trả lời</button>
							</p>
							<div class="comment_form" style="display: none;" id="frm-<?php echo $value['comment_id']?>">
							    <form method="post">
							    	Nội dung trả lời: <textarea type="text" name="comment[full_text]" rows="5" required=""> </textarea>
							    	<input type="hidden" name="comment[parent_id]" value="<?php echo $value['comment_id']?>"/>
							    	<input type="hidden" name="comment[content_id]" value="<?php echo $value['content_id']?>"/>
							    	<input type="hidden" name="comment[content_episodes_id]" value="<?php echo $value['content_episodes_id']?>"/>

									<button type="button" class="comment_button btn_cancle" data-id="<?php echo $value['comment_id']?>">Hủy</button>
									<button type="submit" class="comment_button">Trả lời</button>
							    </form>
							</div>
							<?php
								if(count($value['detail']) > 0){
									?>
										<div class="comment_sub_list">
											<?php
												foreach ($value['detail'] as $key => $value2) {
													?>
														<div class="comment_item">
															<p class="comment_logo">
																<img src="<?php echo base_url().IMG_DEFAULT_USER_LOGO?>" width="30" />
															</p>
															<p class="comment_user"><?php echo $value2['full_name']?>: </p>
															<p class="comment_content"><?php echo $value2['full_text']?></p>
															<p class="comment_date"><?php echo $value2['created_date']?></p>
														</div>
													<?php
												}
											?>
										</div>
									<?php
								}
							?>
							
						</div>			
					<?php
				}
			?>
			
		</div>
    </div>
</div>