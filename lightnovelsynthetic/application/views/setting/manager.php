<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Cài đặt thông tin website
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <form role="form" method="post" enctype="multipart/form-data" >
        <div class="row">
            <!-- Mesage div -->
            <div class="col-md-12">
                <?php
                if (isset($result)) {
                    if ($result['status'] == 0) {
                        ?>
                        <div class="alert alert-danger alert-dismissible">
                            <h4><i class="icon fa fa-ban"></i> Lỗi!</h4>
                            <?php echo $result['message'] ?>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="alert alert-success alert-dismissible">
                            <h4><i class="icon fa fa-check"></i> Thàng công!</h4>
                            <?php echo $result['message'] ?>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cài đặt thông tin website</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Địa chỉ website</label>
                            <input name="data[website_url]" type="text" class="form-control" id="exampleInputEmail1" placeholder="Địa chỉ website" required=""
                            <?php
                            if (isset($data)) {
                                echo 'value="' . get_setting_value($data, 'website_url') . '"';
                            }
                            ?>
                                   >
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Tiêu đề website</label>
                            <input name="data[website_title]" type="text" class="form-control" id="exampleInputPassword1" placeholder="Tiêu đề website" required=""
                            <?php
                            if (isset($data)) {
                                echo 'value="' . get_setting_value($data, 'website_title') . '"';
                            }
                            ?>
                                   >
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Ghi chú bản quyền</label>
                            <input name="data[footer_note]" type="text" class="form-control" id="exampleInputPassword1" placeholder="Chi chú bản quyền" required=""
                            <?php
                            if (isset($data)) {
                                echo 'value="' . get_setting_value($data, 'footer_note') . '"';
                            }
                            ?>
                                   >
                        </div>
                        <div class="form-group pos-related">
                            <label for="exampleInputFile">Logo</label>
                            <input name="logo" type="file" id="upload_logo">
                            <?php
                            if (isset($data) && get_setting_value($data, 'logo') !== '') {
                                echo '<img class="upload_img" src="' . base_url() . get_setting_value($data, 'logo') . '" class="mg-" width="50"/>';
                            }
                            ?>
                            <p class="help-block">Tải lên logo cho website. Chỉ hổ trợ định dạng png/jpg.</p>
                        </div>
                        <div class="form-group pos-related">
                            <label for="exampleInputFile">Favicon</label>
                            <input name="favicon" type="file" id="upload_favicon">
                            <?php
                            if (isset($data) && get_setting_value($data, 'favicon') !== '') {
                                echo '<img class="upload_img" src="' . base_url() . get_setting_value($data, 'favicon') . '" class="mg-" width="50"/>';
                            }
                            ?>
                            <p class="help-block">Tải lên favicon cho website. Chỉ hổ trợ định dạng png/jpg.</p>
                        </div>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
                <!-- /.box -->

            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tài khoản email</h3>
                        <p class="help-block">Đây là tài khoản dùng để gửi mail nên cần điền thông tin chính xác.</p>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                            <div class="col-sm-10">
                                <input name="data[email_name]" type="email" class="form-control" id="inputEmail3" placeholder="Email" required=""
                                <?php
                                if (isset($data)) {
                                    echo 'value="' . get_setting_value($data, 'email_name') . '"';
                                }
                                ?>
                                       >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                            <div class="col-sm-10">
                                <input name="data[email_password]" type="text" class="form-control" id="inputPassword3" placeholder="Mật khẩu" required=""
                                <?php
                                if (isset($data)) {
                                    echo 'value="' . get_setting_value($data, 'email_password') . '"';
                                }
                                ?>
                                       >
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
        <button type="submit" class="btn btn-primary">Lưu</button>
    </form>
</section>
<!-- /.content -->