<!DOCTYPE html>
<html>
<meta http-req="Content-Type" content="charset=utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title><?php echo get_setting_value($setting, 'website_title'); ?> - <?php echo $website_title;?></title>
<meta name="desc" content="“Hừmmm ... tôi hiểu rồi, cậu là một Chúa Quỷ. Một tên vừa mới được sinh ra.”

Long nữ này nói chuyện với một vẻ rất quan tâm, trong lúc vẫn đang quan sát căn phòng của tôi. Tôi đã cho cô ấy mặc cái áo thun của mình, nên về cơ bản thì cô ấy vẫn còn đang nửa người trần truồng. Điều này mà xảy ra ở Trái Đất thì sẽ gây ...">
<meta name="image" content="<?php echo base_url(). get_setting_value($setting, 'favicon'); ?>">
<link rel="icon" type="image/png" href="<?php echo base_url(). get_setting_value($setting, 'favicon'); ?>">
<meta property="fb:app_id" content="783908218461381">
<!-- facebook-->
<meta property="og:title" content="Chapter 6: Rồng Tối Cao Tự Do">
<meta property="og:description" content="“Hừmmm ... tôi hiểu rồi, cậu là một Chúa Quỷ. Một tên vừa mới được sinh ra.”

Long nữ này nói chuyện với một vẻ rất quan tâm, trong lúc vẫn đang quan sát căn phòng của tôi. Tôi đã cho cô ấy mặc cái áo thun của mình, nên về cơ bản thì cô ấy vẫn còn đang nửa người trần truồng. Điều này mà xảy ra ở Trái Đất thì sẽ gây ...">
<meta property="og:image" content="<?php echo base_url(). get_setting_value($setting, 'favicon'); ?>">

<body oncopy="return false" oncut="return false" onpaste="return false">
    <script>
        window.fbAsyncInit = function() {FB.init({appId      : '783908218461381',cookie     : true,xfbml      : true,version    : 'v2.12'});FB.AppEvents.logPageView();};(function(d, s, id){var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) {return;}js = d.createElement(s); js.id = id;js.src = "https://connect.facebook.net/en_US/sdk.js";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));
    </script>
    <!--div id="fb-root"></div><script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.12&appId=783908218461381&autoLogAppEvents=1';fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script-->
    <?php $this->load->view('elements/frontend/header'); ?> 
    <?php echo $contents?>
    <?php $this->load->view('elements/frontend/footer'); ?> 
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/index.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/header.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/main.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/rpanel.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/footer.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/box.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/login.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/gridlist.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/ranklist.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/tabpanel.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/search.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/story.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/read.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/member.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/list.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/profile.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/comment.css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i|Varela+Round&amp;amp;subset=vietnamese" rel="stylesheet">
    <script src="<?php echo asset_front_url() ?>js/iscript.js"></script>
    <script>
        function click (e) {
            if (!e)
                e = window.event;
            if ((e.type && e.type == "contextmenu") || (e.button && e.button == 2) || (e.which && e.which == 3)) {
                if (window.opera)
                window.alert("");
                return false;
            }
        }
        if (document.layers){
            document.captureEvents(Event.MOUSEDOWN);
        }
        document.onmousedown = click;
        document.oncontextmenu = click;
    </script>
</body>

</html>