<?php

/* 
 * Created date: 11-10-2018
 * Version: 1
 * Lastest edited: 11-10-2018
 * Description: This file return route from controller/method and parameters
 * Author: Zola Web Group
 */
/*
 * Return home url
 */
function url_home(){
   return '/';
}
/*
 * Return admin (setting) url
 */
function url_admin(){
   return 'admin.html';
}
/*
 * Return admin login request url
 */
function url_admin_login_request(){
   return 'admin/login-request.html';
}
/*
 * Return admin lang manager
 */
function url_lang_manager(){
   return 'admin/manager-lang.html';
}
/*
 * Return cart url
 */
function url_cart(){
   return 'cart.html';
}
/*
 * Return switch lang url
 */
function url_switch_lang(){
   return 'switch-lang/';
}