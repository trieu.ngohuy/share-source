<?php

/*
 * Created date: 9-12-2018
 * Version: 1
 * Lastest edited: 9-12-2018
 * Description: This file contains files functtons
 * Author: Zola Web Group
 */
/*
 * Upload images
 */

function upload_images($obj = null, $folder = null, $file_name = null) {
    if ($obj != null && $folder != null && $file_name != null) {

        $config['upload_path'] = './' . USER_UPLOAD_PATH . $folder . '/';
        $config['allowed_types'] = 'jpg|png';

        $obj->load->library('upload', $config);
        if (!$obj->upload->do_upload($file_name)) {
            $error = array('error' => $obj->upload->display_errors());

            return array(
                'status' => false,
                'error' => $error
            );
        } else {
            $data = array('upload_data' => $obj->upload->data());
            return array(
                'status' => true,
                'data' => $data
            );
        }
    }
}

/*
 * Delete images
 */

function delete_images($path) {
    //Check if image exist
    if (file_exists(FCPATH . $path)) {
        //Delete image
        unlink(FCPATH . $path);
    }
}
