<?php

use Cake\Core\Configure;
?>
<form action="" method="post" class="form-horizontal">
    <div class="form-group">
        <label>Tên đăng nhập</label>
        <input type="text" class="form-control" placeholder="Tên đăng nhập" name="UsUserName" required="">
    </div>
    <div class="form-group">
        <label>Mật khẩu</label>
        <input type="password" class="form-control" placeholder="Mật khẩu" name="UsPassword" required="">
    </div>
    <?php
    if (isset($data)) {
        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo Configure::read('Message_904'); ?>
        </div>
        <?php
    }
    ?>
    <!--div class="checkbox">
        <label class="pull-right">
            <?=
            $this->Html->link(
                    'Quên mật khẩu', '/forget-pass'
            )
            ?>
        </label>

    </div-->
    <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Đăng nhập</button>
    <!--div class="register-link m-t-15 text-center">
        <p>Chưa có tài khoản ? <?=
            $this->Html->link(
                    'Đăng ký tài khoản', '/register'
            )
            ?></p>
    </div-->
</form>