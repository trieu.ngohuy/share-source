
<?php

use Cake\Core\Configure;
use Cake\Routing\Router;
?>
<div class="alert alert-danger" style="display: none">
    <ul>

    </ul>
</div>
<div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
    <div class="form-line">
        <input type="text" id="inpUserName" class="form-control" name="UsUserName" placeholder="Tên đăng nhập" required autofocus>
    </div>
</div>
<div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
    <div class="form-line">
        <input type="password" id="inpUsPassword" class="form-control" name="o" placeholder="Mật khẩu" required>
    </div>
</div>
<div class="row">
    <div class="col-xs-8 p-t-5 mg-b-0">

    </div>
    <div class="col-xs-4 mg-b-0">
        <button class="btn btn-block bg-pink waves-effect" type="submit" id="btnLogin">Đăng nhập</button>
    </div>
</div>
<script>

    <?php
    echo "var js_users = " . json_encode($users) . ";\n";
    echo "var loginUrl = '" . Router::url(array('controller' => 'Authenticate', 'action' => 'SaveLoginInfo')) . "';\n";
    echo "var indexUrl = '" . Router::url(array('controller' => 'Booking', 'action' => 'index')) . "';\n";
    ?>
    //Press enter
    $('input').keyup(function(e){
        if(e.keyCode == 13)
        {
            Login();
        }
    });
    /*
    Click login event
     */
    $('body').on('click','#btnLogin', function(){
        Login();
    });
    /*
    Login
     */
    function Login(){
        //Get input data
        var input = {
            UsUserName: $('#inpUserName').val(),
            UsPassword: $('#inpUsPassword').val()
        };
        var valid = true;
        var errMessage = '';

        //Check valid
        if(input['UsUserName'] === ''){
            valid = false;
            errMessage = '<li>Tên đăng nhập bắt buộc</li>';
        }
        if(input['UsPassword'] === ''){
            valid = false;
            errMessage += '<li>Mật khẩu bắt buộc</li>';
        }
        if(valid === false){
            $('.alert').css('display', 'block');
            $('.alert ul').html(errMessage);
            return;
        }

        //Generate password
        input['UsPassword'] = md5('<?php echo Configure::read('MD5_Users') ?>' + input['UsPassword']);
        //Check if valid username and password
        var check = js_users.filter(function(obj){
            return obj.UsUserName === input['UsUserName'] && obj.UsPassword === input['UsPassword']
        });
        if(check.length <= 0){
            valid = false;
            errMessage = '<li>Mật khẩu hoặc tên đăng nhập không chính xác</li>';
        }
        if(valid === false){
            $('.alert').css('display', 'block');
            $('.alert ul').html(errMessage);
            return;
        }

        //Save login info
        if(valid){
            //Show preloader
            $('.page-loader-wrapper').fadeIn();

            //Ajax update data
            $.ajax({
                url: loginUrl,
                type: 'post',
                data: check[0],
                success: function (data) {
                    //Parse return data to javascript object
                    data = JSON.parse(data);

                    //Hide preloader
                    $('.page-loader-wrapper').fadeOut();

                    if (data.success) {
                        //Hide alert
                        $('.alert').css('display', 'none');
                        //Show error modal
                        swal("Thông báo!", "Đăng nhập thành công.", "success");
                        //Go to main page
                        window.location.href = indexUrl;
                    } else {
                        //Hide alert
                        $('.alert').css('display', 'none');
                        //Show error modal
                        swal("Lỗi!", "Không thể đăng nhập.", "error");
                    }
                },
                error: function (xhr, status, error) {
                    //Hide preloader
                    $('.page-loader-wrapper').fadeOut();
                    //Show modal message
                    swal("Lỗi!", "Có lỗi xảy ra. " + error, "error");
                }
            });
        }
    }
</script>