<?php

use Cake\Core\Configure;
use Cake\Routing\Router;
?>
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
        <thead>
            <tr>
                <th>#</th>
                <th>Tên Phòng</th>
                <th>Giá Phòng</th>
                <th>Ngày tạo</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Tên Phòng</th>
                <th>Giá Phòng</th>
                <th>Ngày tạo</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $index = 1; ?>
            <?php foreach ($rooms as $room): ?>
                <tr id="<?php echo $room->RoRoomId ?>">
                    <td><?php echo $index; ?></td>
                    <td><?php echo $room->RoName ?></td>
                    <td><?php echo $room->RoPrice . ' ' . Configure::read('Currency') ?></td>
                    <td><?php echo $room->RoCreatedDate ?></td>
                    <td class="al-c js-sweetalert">
                        <div class="btn-group btn-group-xs" role="group" aria-label="Extra-small button group">
                            <a href="modify/<?php echo $room->RoAlias ?>" class="btn btn-primary waves-effect">
                                <i class="material-icons">edit</i>
                            </a>
                            <button data-type="confirm" class="btn bg-pink waves-effect btn-delete" data-id="<?php echo $room->RoRoomId ?>"
                                    data-title="Xóa dữ liệu" data-text="Bạn có chắc chắn muốn xóa không?" data-mode="warning"
                                    data-confirm="Xóa" data-cancel="Hủy">
                                <i class="material-icons">delete</i>
                            </button>
                        </div>
                    </td>
                </tr>
                <?php $index++; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script>
    var deleteUrl = '<?php echo Router::url(array('controller' => 'Rooms', 'action' => 'delete')); ?>';
</script>
<!--Link to common manager javascript handle-->
<?= $this->Html->script('manager.func.js') ?>