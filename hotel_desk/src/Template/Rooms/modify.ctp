<?php

use Cake\Core\Configure;
use Cake\Routing\Router;
?>
<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Form</strong> thêm mới phòng
                    </div>
                    <form action="" method="post" class="form-horizontal">

                        <div class="card-body card-block">
                            <?php
                            if (isset($data) && $data['status'] == 'false') {
                                ?>
                                <div class="alert alert-danger aler-div" role="alert">
                                    <?php echo $data['message']; ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="RoName" class=" form-control-label">Tên phòng</label></div>
                                <div class="col-12 col-md-10">
                                    <input type="text" id="RoName" name="RoName" class="form-control inpName" maxlength="50" required=""
                                           value="<?php
                                           if (isset($data)) {
                                               echo $data['data']['RoName'];
                                           }
                                           ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="RoAlias" class=" form-control-label">Tên rút gọn</label></div>
                                <div class="col-12 col-md-10">
                                    <input type="text" id="RoAlias" name="RoAlias" class="form-control inpAlias" maxlength="50" required=""
                                           value="<?php
                                           if (isset($data)) {
                                               echo $data['data']['RoAlias'];
                                           }
                                           ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="RoPrice" class=" form-control-label">Giá phòng / 1h</label></div>
                                <div class="col-12 col-md-3">
                                    <input type="number" id="RoPrice" name="RoPrice" class="form-control" maxlength="11" required=""
                                           value="<?php
                                           if (isset($data)) {
                                               echo $data['data']['RoPrice'];
                                           }
                                           ?>">
                                </div>
                                <div class="col-12 col-md-1" id="currencyWrap"><?= Configure::read('Currency') ?></div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm" id="btnSave">
                                <i class="fa fa-dot-circle-o"></i> Lưu
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm" id="btnCancel">
                                <i class="fa fa-ban"></i> Hủy
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>
    <!-- .animated -->
</div>
<!--Common js file-->
<?= $this->Html->script('common.func') ?>
<script>
    var managerUrl = '<?php echo Router::url(array('controller' => 'Rooms', 'action' => 'manager')); ?>';
</script>
<!--New js file-->
<?= $this->Html->script('new.func') ?>