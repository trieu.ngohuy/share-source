<?php

use Cake\Core\Configure;
use Cake\Routing\Router;
?>
<form action="" method="post">
    <?php
    if (isset($data) && $data['status'] == 'false') {
        ?>
        <div class="alert alert-danger aler-div" role="alert">
            <h4>Danh sách lỗi</h4>
            <hr>
            <ul class="err-list">
                <?php
                foreach($data['message'] as $obj){
                    echo $obj;
                }
                ?>
            </ul>
        </div>
        <?php
    }
    ?>
    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Tên phòng</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="text" id="RoName" name="RoName" class="form-control inpName" maxlength="50" required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['RoName'];
                           }
                           ?>"
                           placeholder="Nhập tên phòng...">
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Tên rút gọn</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="text" id="RoAlias" name="RoAlias" class="form-control inpAlias" maxlength="50" required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['RoAlias'];
                           }
                           ?>"
                           placeholder="Nhập rút gọn...">
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Số lượng người lớn tối đa</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="number" id="RoMaxAldult" name="RoMaxAldult" class="form-control inpMaxAldult" maxlength="50" required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['RoMaxAldult'];
                           }
                           ?>">
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Số lượng trẻ em tối đa</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="number" id="RoMaxBaby" name="RoMaxBaby" class="form-control inpMaxBaby" maxlength="50" required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['RoMaxBaby'];
                           }
                           ?>">
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12 demo-masked-input">
            <label for="email_address">Giá phòng / 1 ngày</label>

            <div class="input-group">
                <div class="form-line">                    
                    <input type="text" id="RoPrice" name="RoPrice" class="form-control " required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['RoPrice'];
                           }
                           ?>"
                           onkeyup="PriceMaskInput(this)" >
                </div>
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-primary waves-effect" id="btnSave">Lưu</button>
    <button type="reset" class="btn btn-default waves-effect" id="btnCancel">Hủy</button>
</form>
<script>
    jQuery(document).ready(function () {
        //Click button cancel
        jQuery("#btnCancel").click(function () {
            //Go to manager page
            location.href = '<?php echo Router::url(array('controller' => 'Rooms', 'action' => 'manager')); ?>';
        });
    });
</script>