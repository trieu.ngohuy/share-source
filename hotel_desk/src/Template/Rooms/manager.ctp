<?php

use Cake\Core\Configure;
use Cake\Routing\Router;
?>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Danh sách phòng</strong>
                        <a href="modify" type="button" id="btnAdd" class="btn btn-outline-primary btn-sm fl-right"><i class="fa fa-plus-circle"></i>&nbsp; Thêm mới</a>
                    </div>
                    <div class="card-body">

                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tên Phòng</th>
                                    <th>Giá Phòng</th>
                                    <th>Thứ tự hiển thị</th>
                                    <th>Ngày tạo</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1;?>
                                <?php foreach ($rooms as $room): ?>
                                    <tr id="<?php echo $room->RoRoomId ?>">
                                        <td><?php echo $index;?></td>
                                        <td><?php echo $room->RoName ?></td>
                                        <td><?php echo $room->RoPrice . ' ' . Configure::read('Currency')?></td>
                                        <td><?php echo $room->RoDispOrder ?></td>
                                        <td><?php echo $room->RoCreatedDate ?></td>
                                        <td>
                                            <a href="modify/<?php echo $room->RoAlias ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                            <button class="btn btn-outline-danger btn-sm btnDelete" data-id="<?php echo $room->RoRoomId ?>">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <?php $index++;?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <!-- .animated -->
</div>

<div class="modalConfirmWrap">

</div>
<div class="modalSuccessWrap">

</div>
<div class="modalErrorWrap">

</div>
<script>
    var confirmModalUrl = '<?php echo Router::url(array('controller' => 'Modal', 'action' => 'confirmModal')); ?>';
    var successModalUrl = '<?php echo Router::url(array('controller' => 'Modal', 'action' => 'successModal')); ?>';
    var errorModalUrl = '<?php echo Router::url(array('controller' => 'Modal', 'action' => 'errorModal')); ?>';
    var deleteUrl = '<?php echo Router::url(array('controller' => 'Rooms', 'action' => 'delete')); ?>';
    var deleteMessage = '<?= Configure::read('Message_902') ?>';
    var invalidMessage = '<?= Configure::read('Message_901') ?>';
</script>
<!--Link to common manager javascript handle-->
<?= $this->Html->script('manager.func.js') ?>