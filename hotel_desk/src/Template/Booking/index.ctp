<?php

use Cake\Core\Configure;
use Cake\Routing\Router;
?>
<div class="container calendar-wrap">
    <div class="row">
        <div class="col col-lg-12">
            <section class="card mg-b-0" style="display: none;">
                <div class="card-body text-secondary">
                    <div class="row">
                        <div class="col col-2"><p class="mg-b-0">Tháng 5/2018</p></div>
                        <div class="col col-10">
                            <div class="row">
                                <div class="col col-1"><button type="button" class="btn btn-primary btn-sm">Today</button></div>
                                <div class="col col-5">
                                    <div class="row">
                                        <div class="col col-2"><button type="button" class="btn btn-outline-success btn-sm"><i class="fa fa-arrow-left"></i>&nbsp;</button></div>
                                        <div class="col col-8">
                                            <input id="inpRangeDate" type="text" class="form-control">
                                        </div>
                                        <div class="col col-2"><button type="button" class="btn btn-outline-success btn-sm"><i class="fa fa-arrow-right"></i>&nbsp;</button>                                    </div>
                                    </div>
                                </div>
                                <div class="col col-2"></div>
                                <div class="col col-4">
                                    <div class="row form-group mg-b-0">
                                        <div class="col col-md-7"><label for="selectSm" class=" form-control-label">Số ngày hiển thị</label></div>
                                        <div class="col-12 col-md-5">
                                            <select id="comDateRangeSelect" class="form-control-sm form-control">
                                                <option value="5">5</option>
                                                <option value="10">10</option>
                                                <option value="15">15</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-2 cal-left">
            <section class="card mg-b-0">
                <div class="card-body text-secondary">Phòng \ Ngày</div>
            </section>
            <?php foreach ($rooms as $room): ?>
                <section class="card mg-b-0">
                    <div class="card-body text-secondary"><?php echo $room->RoName ?></div>
                </section>
            <?php endforeach; ?>
        </div>
        <div class="col-lg-10 cal-right src-x">
            <div class="row">
                <div class="row" id="bookingWrap">
                </div>

            </div>
        </div>
    </div>
</div>

<!--Modal add new or edit booking room info-->
<div class="modal fade" id="newBookingModal" tabindex="-1" role="dialog" aria-labelledby="newBookingModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-slg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newBookingModalLabel">Book / Sửa phòng</h5>
            </div>
            <form action="" method="post" class="form-horizontal" id="frmBookingDetail">
                <div class="modal-body">
                    <div id="pay-invoice">
                        <div class="card-body">
                            <div class="row">
                                <input id="inpBookingId" name="BrBookingId" type="hidden" class="form-control cc-name valid">
                                <!--input id="inpRoomId" name="BrRoomId" type="hidden" class="form-control cc-name valid"-->
                                <input id="chkIsVAT" name="BrIsVAT" type="hidden" class="form-control cc-name valid">
                                <input id="inpTransferRoomId" name="BrTransferRoomId" type="hidden" class="form-control cc-name valid">
                                <input id="inpTransferDate" name="BrTransferDate" type="hidden" class="form-control cc-name valid">
                                <div class="col col-3">
                                    <div class="form-group">
                                        <label for="comBookingRoomStatus" class="control-label mb-1">Tình trạng phòng</label>
                                        <select name="BrStatus" id="comBookingRoomStatus" class="form-control">
                                            <!--option value="0">Sẵn sàng nhận phòng</option-->
                                            <option value="1">Đang ở</option>
                                            <!--option value="2">Đang dọn / Chờ dọn phòng</option-->
                                            <option value="3">Đã đặt phòng</option>
                                            <option value="4">Đã kế toán xong / kiểm tra xong</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="comBookingRoom" class="control-label mb-1">Chọn phòng</label>
                                        <select name="BrRoomId" id="comBookingRoom" class="form-control">
                                        </select>
                                        <div class="checkbox " id="extrasCheckboxRoom">
                                            <label for="exTrasCheckboxRoom" class="form-check-label sub-title">
                                            </label>
                                        </div>
                                        <div class="alert alert-danger transfer-room-message" role="alert">

                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <label for="BrCustomerName" class="control-label mb-1">Tên khách hàng</label>
                                        <input required="" placeholder="Tên khách hàng..." id="inpBookingCustomerName" name="BrCustomerName" type="text" class="form-control cc-name valid" data-val="true" data-val-required="Please enter the name" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
                                        <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                                    </div>
                                    <div class="form-group has-success">
                                        <label for="BrCustomerEmail" class="control-label mb-1">Emal khách hàng</label>
                                        <input placeholder="Email khách hàng..." id="inpBookingCustomerEmail" name="BrCustomerEmail" type="email" class="form-control cc-name valid" data-val="true" data-val-required="Please enter the name" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
                                        <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                                    </div>
                                    <div class="form-group has-success">
                                        <label for="BrCustomerPhone" class="control-label mb-1">Sđt khách hàng</label>
                                        <input placeholder="Sđt khách hàng..." id="inpBookingCustomerPhone" name="BrCustomerPhone" type="text" class="form-control cc-name valid" data-val="true" data-val-required="Please enter the name" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
                                        <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                                    </div>
                                </div>
                                <div class="col col-5">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="datBookingFromDate" class="control-label mb-1">Từ ngày</label>
                                                <div class="input-group" id="fromDateWrap">
                                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                    <input name="BrFromDate" class="form-control" type="text" placeholder="click to show datepicker" id="datBookingFromDate" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="datBookingToDate" class="control-label mb-1">Tới ngày</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                <input name="BrToDate" class="form-control" type="text" placeholder="click to show datepicker" id="datBookingToDate" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <a class="btn btn-link decrease-people" data-type="Aldult"><i class="fa fa-minus-circle"></i>&nbsp;</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group has-success mg-b-0">
                                                        <label for="inpBookingAldult" class="control-label mb-1">Người lớn</label>
                                                        <input min="1" value="1" required="" id="inpBookingAldult" name="BrAldult" type="text" class="input-booking-people form-control cc-name valid" data-type="aldult">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="btn btn-link increase-people" data-type="Aldult" ><i class="fa fa-plus-circle"></i>&nbsp;</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <a class="btn btn-link decrease-people" data-type="Baby"><i class="fa fa-minus-circle"></i>&nbsp;</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group has-success">
                                                        <label for="inpBookingChildren" class="control-label mb-1">Trẻ em</label>
                                                        <input min="0" value="0" required="" id="inpBookingBaby" name="BrBaby" type="text" class="input-booking-people form-control cc-name valid" data-type="baby">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="btn btn-link increase-people" data-type="Baby"><i class="fa fa-plus-circle"></i>&nbsp;</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <br>
                                            <span><b>Người lớn: </b> <?php echo $configAldult . ' ' . $currency ?> </span><br>
                                            <span><b>Trẻ em dưới 10 tuổi: </b> Miễn phí</span>
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <label for="inpBookingDeposit" class="control-label mb-1">Đặt cọc</label>
                                        <input min="0" value="0" required="" id="inpBookingDeposit" name="BrDeposit" type="text" class="form-control cc-name valid inpPrice">
                                        <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                                    </div>
                                    <div class="form-group has-success">
                                        <label class="control-label mb-1">Nhận phòng sớm / Trả phòng muộn</label>
                                        <input value="" id="inpBookingModeString" name="BrMode" type="hidden" class="form-control cc-name valid inpPrice">
                                        <div class="form-check-inline form-check" id="divModeWrap">
                                            <div class='row'>
                                                <div class="col col-2">
                                                    <label for="inpBookingModeNone" class="form-check-label ">
                                                        <input checked="" type="checkbox" id="inpBookingModeNone" name="ChkMode" value="0" class="chk-booking-mode form-check-input mg-left-15">Không
                                                    </label>
                                                </div>
                                                <div class="col col-4">
                                                    <div class="row">
                                                        <div class="col col-8 pd-0">
                                                            <label for="inpBookingModeEarly" class="form-check-label ">
                                                                <input type="checkbox" id="inpBookingModeEarly" name="ChkMode" value="1" class="chk-booking-mode form-check-input mg-left-15">Nhập phòng sớm
                                                            </label>
                                                        </div>
                                                        <div class="col col-4 pd-0">
                                                            <select id="comGetRoomEarly" class="form-control-sm form-control">

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col col-3">
                                                    <label for="inpBookingModeBefore16" class="form-check-label ">
                                                        <input type="checkbox" id="inpBookingModeBefore16" name="ChkMode" value="2" class="chk-booking-mode form-check-input mg-left-15">Trả phòng muộn từ 12h10-16h
                                                    </label>
                                                </div>
                                                <div class="col col-3">
                                                    <label for="inpBookingModeAfter16" class="form-check-label ">
                                                        <input type="checkbox" id="inpBookingModeAfter16" name="ChkMode" value="3" class="chk-booking-mode form-check-input mg-left-15">Trả phòng muộn sau 16h
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-services" class="control-label mb-1">Dịch vụ</label>
                                        <input required="" id="inpServices" name="BrServices" type="hidden" class="form-control cc-name valid">
                                        <div id="divServicesDetailWrap">

                                        </div>
                                        <button type="button" id="btnBookingServiceAdd" class="btn btn-link btn-sm" data-toggle="modal" data-target="#servicesModal"><i class="fa fa-plus-circle"></i>&nbsp; Thêm mới dịch vụ</button>
                                    </div>
                                    <div class="form-group">
                                        <label for="inpBookingNote" class="control-label mb-1">Ghi chú</label>
                                        <textarea id="inpBookingNote" name="BrNote" id="textarea-input" rows="2" placeholder="Ghi chú..." class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="form-group alg-right">
                                        <h4 class="mg-bottom-20">Thành tiền</h4>
                                        <div class="row" id="bookingTotalMoneyWrap">
                                            <div class="col-md-12 al-l" id="totalMoneyList">
                                                <p>
                                                    <b>Tiền phòng cũ chuyển qua:</b> <span id="totalMoneyRoomEstras">0</span> <?php echo Configure::read('Currency') ?>
                                                    <br><span class="sub-title" id="roomSubTitle">Chuyển từ phòng 101 (Single) từ ngày 01/06/2018</span>
                                                </p>
                                                <p><b>Tiền phòng:</b> <span id="totalMoneyRoom">0</span> <?php echo Configure::read('Currency') ?></p>
                                                <p><b>Tiền dịch vụ:</b> <span id="totalMoneServices">0</span> <?php echo Configure::read('Currency') ?></p>
                                                <p><b>Tiền đặt cọc:</b> <span id="totalMoneyDeposit">0</span> <?php echo Configure::read('Currency') ?></p>
                                                <p>
                                                    <b>Tiền nhận phòng sớm / trả phòng muộn:</b> <span id="totalMoneyMode">0</span> <?php echo Configure::read('Currency') ?>
                                                    <br><span class="sub-title" id="earlyLateSubTitle"></span>
                                                </p>
                                                <p>
                                                    <b>Tiền thêm người:</b> <span id="totalMoneyPeopelEstras">0</span> <?php echo Configure::read('Currency') ?>
                                                    <br><span class="sub-title" id="peopleSubTitle"></span>
                                                </p>
                                                <div>
                                                    <b>Xuất hóa đơn đỏ:</b>
                                                    <label id="chkVAT" for="chkVATValue" class="form-check-label sub-title">
                                                        <input type="checkbox" id="chkVATValue" name="chkVAT" class="form-check-input">
                                                        Có
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-12 sperate-line"></div>
                                            </div>
                                            <div class="col-md-12">
                                                <b>Tổng tiền:</b>
                                                <p id="totalMoneyAllWrap"><span id="totalMoneyAll">0</span> <?php echo Configure::read('Currency') ?></p>
                                                <p id="totalMoneyVATWrap"><b>VAT (0%):</b> <span id="totalMoneyVAT">0</span> <?php echo Configure::read('Currency') ?></p>
                                                <div class="col-md-12">
                                                    <div class="col-md-12 sperate-line"></div>
                                                </div>
                                                <p id="totalMoneyAfterVATWrap"><span id="totalMoneyAfterVAT">0</span> <?php echo Configure::read('Currency') ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnModalBookingDetailSave" type="button" class="btn btn-primary">Lưu</button>
                    <button id="btnModalBookingDetailExport" type="button" class="btn btn-success">Xuất hóa đơn</button>
                    <button id="btnModalBookingDetailClose" type="button" class="btn btn-secondary">Hủy</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Modal add new services-->
<div class="modal fade" id="servicesModal" tabindex="-1" role="dialog" aria-labelledby="servicesModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="servicesModalLabel">Thêm dịch vụ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="ConvertCheckedCheckboxToString()">Lưu</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
            </div>
        </div>
    </div>
</div>

<!--Modal confirm save data-->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalLabel">Xác nhận</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Lưu dữ liệu?
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" >Hủy</button>
                <button type="button" class="btn btn-primary">Lưu</button>
            </div>
        </div>
    </div>
</div>


<script>
    //Convert list rooms and booking into javascrip array
<?php
echo "var js_rooms = " . json_encode($rooms) . ";\n";
echo "var js_booking = " . json_encode($booking) . ";\n";
echo "var js_services = " . json_encode($services) . ";\n";
echo "var js_get_room_early= " . json_encode($getRoomEarly) . ";\n";
echo "var msgRoomStatusReady = '" . Configure::read('Room_Status_Ready') . "';\n";
echo "var msgRoomStatusStayed = '" . Configure::read('Room_Status_Stayed') . "';\n";
echo "var msgRoomStatusChecked = '" . Configure::read('Room_Status_Checked') . "';\n";
echo "var msgRoomStatusBooked = '" . Configure::read('Room_Status_Booked') . "';\n";
echo "var msgRoomStatusClosed= '" . Configure::read('Room_Status_Closed') . "';\n";
echo "var currency= '" . $currency . "';\n";
echo "var vat= '" . $vat . "';\n";
echo "var js_config_aldult= '" . $configAldult . "';\n";
echo "var js_check_transfer_room_url = '" . Router::url(array('controller' => 'Booking', 'action' => 'CheckTransferRoom')) . "';\n";
?>
</script>
<?= $this->Html->script('common.func.js') ?>
<?= $this->Html->script('booking-room.scripts.js') ?>
