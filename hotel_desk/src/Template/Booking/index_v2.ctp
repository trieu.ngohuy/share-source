<?php

use Cake\Core\Configure;
use Cake\Routing\Router;
?>
<div class="row clearfix" id="divContent">
    <div class="booking-wrap room-wrap" id="roomWrapOuter">
        <div id="roomWrap"></div>
    </div>
    <div class="booking-wrap" id="bookingWrap">

    </div>
</div>
<!--Modal booking detail-->
<?= $this->element('modal-booking') ?>
<!--Modal services-->
<?= $this->element('modal-services') ?>
<script>
<?php
echo "var js_rooms = " . json_encode($rooms) . ";\n";
echo "var js_booking = " . json_encode($booking) . ";\n";
echo "var js_services = " . json_encode($services) . ";\n";
echo "var js_get_room_early= " . json_encode($getRoomEarly) . ";\n";

echo "var msgRoomStatusBooked = '" . Configure::read('Room_Status_Booked') . "';\n";
echo "var msgRoomStatusCheckOut = '" . Configure::read('Room_Status_CheckOut') . "';\n";
echo "var msgRoomStatusCheckIn = '" . Configure::read('Room_Status_CheckIn') . "';\n";
echo "var msgRoomStatusClosed= '" . Configure::read('Room_Status_Closed') . "';\n";

echo "var currency= '" . $currency . "';\n";
echo "var vat= '" . $vat . "';\n";
echo "var js_config_aldult= '" . $configAldult . "';\n";
echo "var js_check_transfer_room_url = '" . Router::url(array('controller' => 'Booking', 'action' => 'CheckTransferRoom')) . "';\n";
echo "var exportUrl = '" . Router::url(array('controller' => 'Pdf', 'action' => 'ExportRoomBill')) . "';\n";
echo "var saveUrl = '" . Router::url(array('controller' => 'Booking', 'action' => 'UpdateBooking')) . "';\n";
echo "var deleteUrl = '" . Router::url(array('controller' => 'Booking', 'action' => 'DeleteBooking')) . "';\n";
echo "var viewPdfUrl = '" . Router::url(array('controller' => 'Pdf', 'action' => 'ViewPdf')) . "';\n";
echo "var loginInfo = '" . json_encode($loginInfo) . "';\n";
?>
loginInfo = JSON.parse(loginInfo);
</script>
<?= $this->Html->script('common.func.js') ?>
<?= $this->Html->script('booking-room.scripts.js') ?>
