<?php

use Cake\Core\Configure;
use Cake\Routing\Router;
?>
<form action="" method="post">

    <?php
    if (isset($data) && $data['status'] == 'false') {
        ?>
        <div class="alert alert-danger aler-div">
            <h4>Danh sách lỗi</h4>
            <hr>
            <ul class="err-list">
                <?php
                foreach ($data['message'] as $obj) {
                    echo $obj;
                }
                ?>
            </ul>
        </div>
        <?php
    }
    ?>
    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Danh mục cha</label>
            <select name="SrParentId" id="comParentServices" class="form-control show-tick">
                <option value="0" <?php
                if (isset($data) && $data['data']['SrParentId'] === 0) {
                    echo 'selected';
                }
                ?>>---</option>
                        <?php foreach ($services as $service): ?>
                    <option value="<?php echo $service->SrServiceId ?>"
                    <?php
                    if (isset($data) && $data['data']['SrParentId'] === $service->SrServiceId) {
                        echo 'selected';
                    }
                    ?>
                            ><?php echo $service->SrName ?></option>
                        <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Tên dịch vụ</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="text" id="SrName" name="SrName" class="form-control inpName" maxlength="50" required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['SrName'];
                           }
                           ?>"
                           placeholder="Nhập tên dịch vụ...">
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Tên rút gọn</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="text" id="SrAlias" name="SrAlias" class="form-control inpAlias" maxlength="50" required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['SrAlias'];
                           }
                           ?>"
                           placeholder="Nhập tên rút gọn...">
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12 demo-masked-input">
            <label for="email_address">Giá dịch vụ</label>

            <div class="input-group">
                <div class="form-line">                    
                    <input type="text" id="SrPrice" name="SrPrice" class="form-control" required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['SrPrice'];
                           }
                           ?>"
                           onkeyup="PriceMaskInput(this)" >
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary waves-effect" id="btnSave">Lưu</button>
    <button type="reset" class="btn btn-default waves-effect" id="btnCancel">Hủy</button>
</form>
<script>
    jQuery(document).ready(function () {
        //Click button cancel
        jQuery("#btnCancel").click(function () {
            //Go to manager page
            location.href = '<?php echo Router::url(array('controller' => 'Services', 'action' => 'manager')); ?>';
        });
    });
</script>