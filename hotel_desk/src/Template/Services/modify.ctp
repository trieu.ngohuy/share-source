<?php

use Cake\Core\Configure;
use Cake\Routing\Router;
?>
<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Form</strong> thêm mới dịch vụ
                    </div>
                    <form action="" method="post" class="form-horizontal">

                        <div class="card-body card-block">
                            <?php
                            if (isset($data) && $data['status'] == 'false') {
                                ?>
                                <div class="alert alert-danger aler-div" role="alert">
                                    <h4>Danh sách lỗi</h4>
                                    <hr>
                                    <ul class="err-list">
                                        <?php 
                                        foreach($data['message'] as $obj){
                                            echo $obj;
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="SrName" class=" form-control-label">Danh mục cha</label></div>
                                <div class="col-12 col-md-10">
                                    <select name="SrParentId" id="comParentServices" class="form-control">                                        
                                        <option value="0" <?php
                                           if (isset($data) && $data['data']['SrParentId'] === 0) {
                                               echo 'selected';
                                           }
                                           ?>>---</option>
                                        <?php foreach ($services as $service): ?>
                                        <option value="<?php echo $service->SrServiceId ?>"
                                                <?php
                                           if (isset($data) && $data['data']['SrParentId'] === $service->SrServiceId) {
                                               echo 'selected';
                                           }
                                           ?>
                                                ><?php echo $service->SrName ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="SrName" class=" form-control-label">Tên dịch vụ</label></div>
                                <div class="col-12 col-md-10">
                                    <input type="text" id="SrName" name="SrName" class="form-control inpName" maxlength="50" required=""
                                           value="<?php
                                           if (isset($data)) {
                                               echo $data['data']['SrName'];
                                           }
                                           ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="SrAlias" class=" form-control-label">Tên rút gọn</label></div>
                                <div class="col-12 col-md-10">
                                    <input type="text" id="SrAlias" name="SrAlias" class="form-control inpAlias" maxlength="50" required=""
                                           value="<?php
                                           if (isset($data)) {
                                               echo $data['data']['SrAlias'];
                                           }
                                           ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="SrPrice" class=" form-control-label">Giá dịch vụ</label></div>
                                <div class="col-12 col-md-3">
                                    <input type="number" id="SrPrice" name="SrPrice" class="form-control" maxlength="11" required=""
                                           value="<?php
                                           if (isset($data)) {
                                               echo $data['data']['SrPrice'];
                                           }
                                           ?>">
                                </div>
                                <div class="col-12 col-md-1" id="currencyWrap"><?= Configure::read('Currency') ?></div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm" id="btnSave">
                                <i class="fa fa-dot-circle-o"></i> Lưu
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm" id="btnCancel">
                                <i class="fa fa-ban"></i> Hủy
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>
    <!-- .animated -->
</div>
<!--Common js file-->
<?= $this->Html->script('common.func') ?>
<script>
    var managerUrl = '<?php echo Router::url(array('controller' => 'Services', 'action' => 'manager')); ?>';
</script>
<!--New js file-->
<?= $this->Html->script('new.func') ?>