<?php

use Cake\Core\Configure;
use Cake\Routing\Router;

?>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
            <thead>
            <tr>
                <th>#</th>
                <th>Tên Dịch Vụ</th>
                <th>Giá Dịch Vụ</th>
                <th>Ngày tạo</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Tên Dịch Vụ</th>
                <th>Giá Dịch Vụ</th>
                <th>Ngày tạo</th>
                <th>Action</th>
            </tr>
            </tfoot>
            <tbody>
<!--            --><?php //$index = 1; ?>
<!--            --><?php //foreach ($services as $service): ?>
<!--                <tr id="--><?php //echo $service->SrServiceId ?><!--">-->
<!--                    <td>--><?php //echo $index; ?><!--</td>-->
<!--                    <td>--><?php //echo $service->SrName ?><!--</td>-->
<!--                    <td>--><?php //echo $service->SrPrice . ' ' . Configure::read('Currency') ?><!--</td>-->
<!--                    <td>--><?php //echo $service->SrCreatedDate ?><!--</td>-->
<!--                    <td class="al-c js-sweetalert">-->
<!--                        <div class="btn-group btn-group-xs" role="group" aria-label="Extra-small button group">-->
<!--                            <a href="modify/--><?php //echo $service->SrAlias ?><!--" class="btn btn-primary waves-effect">-->
<!--                                <i class="material-icons">edit</i>-->
<!--                            </a>-->
<!--                            <button data-type="confirm" class="btn bg-pink waves-effect btn-delete"-->
<!--                                    data-id="--><?php //echo $service->SrServiceId ?><!--"-->
<!--                                    data-title="Xóa dữ liệu" data-text="Bạn có chắc chắn muốn xóa không?"-->
<!--                                    data-mode="warning"-->
<!--                                    data-confirm="Xóa" data-cancel="Hủy">-->
<!--                                <i class="material-icons">delete</i>-->
<!--                            </button>-->
<!--                        </div>-->
<!--                    </td>-->
<!--                </tr>-->
<!--                --><?php //$index++; ?>
<!--            --><?php //endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->Html->script('common.func.js') ?>
    <script>
        <?php
        echo "var js_services = " . json_encode($services) . ";\n";
        echo "var currency= '" . $currency . "';\n";
        ?>
        var deleteUrl = '<?php echo Router::url(array('controller' => 'Services', 'action' => 'delete')); ?>';
    </script>
    <!--Link to common manager javascript handle-->
<?= $this->Html->script('manager.func.js') ?>
<script>
    $(document).ready(function () {
        //Fill Services data
        var data = RecursionTable(0, '', 0 , js_services, '');
        $('tbody').html(data.html);
    });
</script>
