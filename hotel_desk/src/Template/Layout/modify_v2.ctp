<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title><?php echo $title ?></title>
        <!-- Favicon-->
        <link rel="icon" href="../favicon.ico" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <?= $this->Html->css('v2/bootstrap.css') ?>

        <!-- Bootstrap Select Css -->
        <?= $this->Html->css('v2/bootstrap-select.css') ?>

        <!-- Waves Effect Css -->
        <?= $this->Html->css('v2/waves.css') ?>

        <!-- Animation Css -->
        <?= $this->Html->css('v2/animate.css') ?>

        <!-- Custom Css -->
        <?= $this->Html->css('v2/style.css') ?>

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <?= $this->Html->css('v2/theme-green.css') ?>

        <!-- Jquery Core Js -->
        <?= $this->Html->script('v2/jquery.min.js') ?>

    </head>

    <body class="theme-green">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Đang tải...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->
        <!-- Top Bar -->
        <?= $this->element('navbar') ?>
        <!-- #Top Bar -->
        <?= $this->element('left-sidebar_v2') ?>

        <section class="content">
            <div class="container-fluid">                
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <?php echo $title ?>
                                </h2>
                            </div>
                            <div class="body">
                                <?= $this->fetch('content') ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <!-- Slimscroll Plugin Js -->
        <?= $this->Html->script('v2/jquery.slimscroll.js') ?>

        <!-- Waves Effect Plugin Js -->
        <?= $this->Html->script('v2/waves.js') ?>

        <!-- Bootstrap Core Js -->
        <?= $this->Html->script('v2/bootstrap.js') ?>

        <!-- Select Plugin Js -->
        <?= $this->Html->script('v2/bootstrap-select.js') ?>

        <!-- Input Mask Plugin Js -->
        <?= $this->Html->script('v2/jquery.inputmask.bundle.js') ?>
        
        <!-- Custom Js -->
        <?= $this->Html->script('v2/admin.js') ?>
        <?= $this->Html->script('v2/advanced-form-elements.js') ?>
    </body>

</html>