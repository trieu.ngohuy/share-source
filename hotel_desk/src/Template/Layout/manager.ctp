<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $title?></title>
        <meta name="description" content="Sufee Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-icon.png">
        <link rel="shortcut icon" href="../favicon.ico">

        <?= $this->Html->css('normalize.css') ?>
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('bootstrap-datepicker.css') ?>
        <?= $this->Html->css('font-awesome.min.css') ?>
        <?= $this->Html->css('themify-icons.css') ?>
        <?= $this->Html->css('flag-icon.min.css') ?>
        <?= $this->Html->css('cs-skin-elastic.css') ?>
        <?= $this->Html->css('lib/datatable/dataTables.bootstrap.min.css') ?>
        <?= $this->Html->css('scss/style.css') ?>

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

        <?= $this->Html->script('vendor/jquery-2.1.4.min.js') ?>
        
        <!--Bootstrap js-->
        <!--<?= $this->Html->script('bootstrap.min.js') ?>-->
        <?= $this->Html->script('bootstrap-datepicker.js') ?>
        <?= $this->Html->script('common.func.js') ?>
        
    </head>

    <body>

        <?= $this->Flash->render() ?>
        <!-- Left Panel -->

        <?= $this->element('left-sidebar') ?>

        <!-- /#left-panel -->

        <!-- Left Panel -->

        <!-- Right Panel -->

        <div id="right-panel" class="right-panel">

            <!-- Header-->
            <?= $this->element('header') ?>
            <!-- /header -->

            <?= $this->fetch('content') ?>
        </div><!-- /#right-panel -->
        <!-- Right Panel -->
        
        <?= $this->Html->script('popper.min.js') ?>
        <?= $this->Html->script('plugins.js') ?>
        <?= $this->Html->script('main.js') ?>

        <!--Bootstrap js-->
        <?= $this->Html->script('lib/data-table/datatables.min.js') ?>
        <?= $this->Html->script('lib/data-table/dataTables.bootstrap.min.js') ?>
        <?= $this->Html->script('lib/data-table/dataTables.buttons.min.js') ?>
        <?= $this->Html->script('lib/data-table/buttons.bootstrap.min.js') ?>
        <?= $this->Html->script('lib/data-table/jszip.min.js') ?>
        <?= $this->Html->script('lib/data-table/pdfmake.min.js') ?>
        <?= $this->Html->script('lib/data-table/vfs_fonts.js') ?>
        <?= $this->Html->script('lib/data-table/buttons.html5.min.js') ?>
        <?= $this->Html->script('lib/data-table/buttons.print.min.js') ?>
        <?= $this->Html->script('lib/data-table/buttons.colVis.min.js') ?>
        <?= $this->Html->script('lib/data-table/datatables-init.js') ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('#bootstrap-data-table-export').DataTable();
            });
        </script>
    </body>

</html>