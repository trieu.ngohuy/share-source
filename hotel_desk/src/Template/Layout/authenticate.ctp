<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo $title ?></title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <?= $this->Html->css('v2/bootstrap.css') ?>

    <!-- Waves Effect Css -->
    <?= $this->Html->css('v2/waves.css') ?>

    <!-- Animation Css -->
    <?= $this->Html->css('v2/animate.css') ?>

    <!-- Sweetalert Css -->
    <?= $this->Html->css('v2/sweetalert.css') ?>

    <!-- Custom Css -->
    <?= $this->Html->css('v2/style.css') ?>
    <?= $this->Html->css('v2/common.css') ?>

    <!-- Jquery Core Js -->
    <?= $this->Html->script('v2/jquery.min.js') ?>
    <?= $this->Html->script('v2/md5.js') ?>

</head>

<body class="login-page">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Đang tải...</p>
    </div>
</div>
<!-- #END# Page Loader -->
<div class="login-box">
    <div class="logo al-c">
        <?= $this->Html->image('logo.png') ?>
    </div>
    <div class="card">
        <div class="body">
            <?= $this->fetch('content') ?>
        </div>
    </div>
</div>

<!-- Waves Effect Plugin Js -->
<?= $this->Html->script('v2/waves.js') ?>

<!-- Bootstrap Core Js -->
<?= $this->Html->script('v2/bootstrap.js') ?>

<!-- Validation Plugin Js -->
<?= $this->Html->script('v2/jquery.validate.js') ?>

<!-- SweetAlert Plugin Js -->
<?= $this->Html->script('v2/sweetalert.min.js') ?>

<!-- Custom Js -->
<?= $this->Html->script('v2/admin.js') ?>
<?= $this->Html->script('v2/sign-in.js') ?>
</body>

</html>