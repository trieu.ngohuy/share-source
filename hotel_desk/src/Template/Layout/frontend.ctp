<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta charset="ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $title?></title>
        <meta name="description" content="Sufee Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-icon.png">
        <link rel="shortcut icon" href="favicon.ico">

        <?= $this->Html->css('normalize.css') ?>
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('font-awesome.min.css') ?>
        <?= $this->Html->css('themify-icons.css') ?>
        <?= $this->Html->css('flag-icon.min.css') ?>
        <?= $this->Html->css('cs-skin-elastic.css') ?>
        <?= $this->Html->css('scss/style.css') ?>
        <?= $this->Html->css('lib/vector-map/jqvmap.min.css') ?>
        <?= $this->Html->css('daterangepicker.css') ?>

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>


        <!-- Right Panel -->
        <?= $this->Html->script('vendor/jquery-2.1.4.min.js') ?>    
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
        <?= $this->Html->script('plugins.js') ?>
        <?= $this->Html->script('main.js') ?>
        <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script> 

        
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
        <?= $this->Html->script('daterangepicker.js') ?>
        
    </head>

    <body>

        <?= $this->Flash->render() ?>
        <!-- Left Panel -->

        <?= $this->element('left-sidebar') ?>

        <!-- /#left-panel -->

        <!-- Left Panel -->

        <!-- Right Panel -->

        <div id="right-panel" class="right-panel">

            <!-- Header-->
            <?= $this->element('header') ?>
            <!-- /header -->

            <?= $this->fetch('content') ?>
        </div><!-- /#right-panel -->

        <!-- Right Panel -->

    </body>

</html>