<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title><?php echo $title ?></title>
        <!-- Favicon-->
        <?php
        if(isset($isReport)){
            ?>
        <link rel="icon" href="favicon.ico" type="image/x-icon">
                <?php
        }else{
            ?>
        <link rel="icon" href="../favicon.ico" type="image/x-icon">
                <?php
        }
        ?>
        

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <?= $this->Html->css('v2/bootstrap.css') ?>

        <!-- Waves Effect Css -->
        <?= $this->Html->css('v2/waves.css') ?>

        <!-- Animation Css -->
        <?= $this->Html->css('v2/animate.css') ?>

        <!-- Sweetalert Css -->
        <?= $this->Html->css('v2/sweetalert.css') ?>

        <!-- JQuery DataTable Css -->
        <?= $this->Html->css('v2/dataTables.bootstrap.css') ?>

        <!-- Bootstrap Select Css -->
        <?= $this->Html->css('v2/bootstrap-select.css') ?>

        <!-- Bootstrap Material Datetime Picker Css -->
        <?= $this->Html->css('v2/bootstrap-material-datetimepicker.css') ?>

        <!-- Custom Css -->
        <?= $this->Html->css('v2/style.css') ?>
        <?= $this->Html->css('v2/common.css') ?>
        <?= $this->Html->css('v2/manager.css') ?>

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <?= $this->Html->css('v2/theme-green.css') ?>

        <!-- Jquery Core Js -->
        <?= $this->Html->script('v2/jquery.min.js') ?>

        <!-- Moment Plugin Js -->
        <?= $this->Html->script('v2/moment-with-locales.js') ?>

        <!-- Bootstrap Material Datetime Picker Plugin Js -->
        <?= $this->Html->script('v2/bootstrap-material-datetimepicker.js') ?>

        <!--Custom scrip-->
        <?= $this->Html->script('common.func.js') ?>
        <style>
            .table-responsive{
                overflow-x: hidden;
            }
        </style>
    </head>

    <body class="theme-green">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Đang tải...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->
        <!-- Top Bar -->
        <?= $this->element('navbar') ?>
        <!-- #Top Bar -->
        <?= $this->element('left-sidebar_v2') ?>

        <section class="content">
            <div class="container-fluid">                
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <?php echo $title ?>
                                </h2>
                                <?php if (isset($isDisplayButton)) { ?>
                                    <a class="btn btn-primary waves-effect" id="btnAdd" href="modify">
                                        <i class="material-icons">add</i>
                                        <span>Thêm mới</span>
                                    </a>
                                <?php } ?>
                            </div>
                            <div class="body">
                                <?= $this->fetch('content') ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <!-- Bootstrap Core Js -->
        <?= $this->Html->script('v2/bootstrap.js') ?>

        <!-- Select Plugin Js -->
        <?= $this->Html->script('v2/bootstrap-select.js') ?>

        <!-- Slimscroll Plugin Js -->
        <?= $this->Html->script('v2/jquery.slimscroll.js') ?>

        <!-- Waves Effect Plugin Js -->
        <?= $this->Html->script('v2/waves.js') ?>

        <!-- SweetAlert Plugin Js -->
        <?= $this->Html->script('v2/sweetalert.min.js') ?>

        <!-- Jquery DataTable Plugin Js -->
        <?= $this->Html->script('v2/jquery-datatable/jquery.dataTables.js') ?>
        <?= $this->Html->script('v2/jquery-datatable/dataTables.bootstrap.js') ?>
        <?= $this->Html->script('v2/jquery-datatable/dataTables.buttons.min.js') ?>
        <?= $this->Html->script('v2/jquery-datatable/buttons.flash.min.js') ?>
        <?= $this->Html->script('v2/jquery-datatable/jszip.min.js') ?>
        <?= $this->Html->script('v2/jquery-datatable/pdfmake.min.js') ?>
        <?= $this->Html->script('v2/jquery-datatable/vfs_fonts.js') ?>
        <?= $this->Html->script('v2/jquery-datatable/buttons.html5.min.js') ?>
        <?= $this->Html->script('v2/jquery-datatable/buttons.print.min.js') ?>

        <!-- Custom Js -->
        <?= $this->Html->script('v2/admin.js') ?>
        <?= $this->Html->script('v2/dialogs.js') ?>
        <?= $this->Html->script('v2/jquery-datatable/jquery-datatable.js') ?>
    </body>

</html>