<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title><?php echo $title ?></title>
        <!-- Favicon-->
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <?= $this->Html->css('v2/bootstrap.css') ?>

        <!-- Bootstrap Select Css -->
        <?= $this->Html->css('v2/bootstrap-select.css') ?>

        <!-- Waves Effect Css -->
        <?= $this->Html->css('v2/waves.css') ?>

        <!-- Animation Css -->
        <?= $this->Html->css('v2/animate.css') ?>

        <!-- Sweetalert Css -->
        <?= $this->Html->css('v2/sweetalert.css') ?>
        
        <!-- Bootstrap Material Datetime Picker Css -->
        <?= $this->Html->css('v2/bootstrap-material-datetimepicker.css') ?>
        <!-- Custom Css -->

        <?= $this->Html->css('v2/style.css') ?>
        <?= $this->Html->css('v2/booking.css') ?>
        <?= $this->Html->css('v2/common.css') ?>

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <?= $this->Html->css('v2/theme-green.css') ?>

        <!-- Jquery Core Js -->
        <?= $this->Html->script('v2/jquery.min.js') ?>

        <!-- Moment Plugin Js -->
        <?= $this->Html->script('v2/moment-with-locales.js') ?>

        <!-- Bootstrap Material Datetime Picker Plugin Js -->
        <?= $this->Html->script('v2/bootstrap-material-datetimepicker.js') ?>
    </head>

    <body class="theme-green" onscroll="OnScroll()">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Đang tải...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->
        <!-- Top Bar -->
        <?= $this->element('navbar') ?>
        <!-- #Top Bar -->
        <?= $this->element('left-sidebar_v2') ?>

        <section class="content">
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="wrapAll" style="width: 1770px;">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <?php echo $title ?>
                                </h2>
<!--                                <div class="toolbar" id="divToolbar">-->
<?php /*$this->element('toolbar')*/ ?>
<!--                                </div>-->
<!--                                <ul class="header-dropdown toolbar-header-dropdown m-r--5" >-->
<!--                                    <li class="dropdown">-->
<!--                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">-->
<!--                                            <i class="material-icons">more_vert</i>-->
<!--                                        </a>-->
<!--                                        <ul class="dropdown-menu toolbar-dropdown-menu pull-right">-->
<!--                                            <li>-->
                                <?php /*$this->element('toolbar')*/ ?>
<!--                                            </li>-->
<!--                                        </ul>-->
<!--                                    </li>-->
<!--                                </ul>-->
                            </div>
                            <div class="body">
                                <?= $this->fetch('content') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Slimscroll Plugin Js -->
        <?= $this->Html->script('v2/jquery.slimscroll.js') ?>

        <!-- Waves Effect Plugin Js -->
        <?= $this->Html->script('v2/waves.js') ?>

        <!-- Bootstrap Core Js -->
        <?= $this->Html->script('v2/bootstrap.js') ?>

        <!-- Select Plugin Js -->
        <?= $this->Html->script('v2/bootstrap-select.js') ?>

        <!-- SweetAlert Plugin Js -->
        <?= $this->Html->script('v2/sweetalert.min.js') ?>

        <!-- Custom Js -->
        <?= $this->Html->script('v2/admin.js') ?>
        <?= $this->Html->script('v2/dialogs.js') ?>
        
    </body>

</html>