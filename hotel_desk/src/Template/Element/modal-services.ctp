<?php

use Cake\Core\Configure;
?>
<div class="modal fade" id="servicesModal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="servicesModalLabel">Thêm Dịch Vụ</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary waves-effect" onclick="ConvertCheckedCheckboxToString()">Lưu</button>
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Hủy</button>
            </div>
        </div>
    </div>
</div>