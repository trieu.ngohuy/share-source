<?php

use Cake\Routing\Router;
?>
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="<?php echo Router::url(array('controller' => 'Booking', 'action' => 'index')); ?>">
                <?= $this->Html->image('logo.png', array('class' => 'mg-top-10 mg-bottom-20')) ?>
            </a>
            <a class="navbar-brand hidden" href="./"> <?= $this->Html->image('logo2.png') ?></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="<?php echo Router::url(array('controller' => 'Booking', 'action' => 'index')); ?>"> <i class="menu-icon fa fa-hotel"></i>Đặt phòng </a>
                </li>
                <h3 class="menu-title">Quản lý</h3>
                <!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Quản lý User</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-list"></i>
                            <?=
                            $this->Html->link(
                                    'Quản lý', '/user/manager'
                            )
                            ?>
                        </li>
                        <li><i class="fa fa-plus"></i>
                            <?=
                            $this->Html->link(
                                    'Thêm mới', '/user/modify'
                            )
                            ?>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Quản lý phòng</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-list"></i>
                            <?=
                            $this->Html->link(
                                    'Quản lý', '/room/manager'
                            )
                            ?>
                        </li>
                        <li><i class="fa fa-plus"></i>
                            <?=
                            $this->Html->link(
                                    'Thêm mới', '/room/modify'
                            )
                            ?>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-server"></i>Quản lý dịch vụ</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-list"></i>
                            <?=
                            $this->Html->link(
                                    'Quản lý', '/service/manager'
                            )
                            ?>
                        </li>
                        <li><i class="fa fa-plus"></i>
                            <?=
                            $this->Html->link(
                                    'Thêm mới', '/service/modify'
                            )
                            ?>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-file-excel-o"></i>Báo cáo</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-download"></i>                            
                            <?=
                            $this->Html->link(
                                    'Xuất báo cáo', '/export'
                            )
                            ?>
                        </li>
                    </ul>
                </li>
                <li class="active">
                    <a href="<?php echo Router::url(array('controller' => 'Logs', 'action' => 'index')); ?>"> <i class="menu-icon fa fa-history"></i>Lịch sử hoạt động</a>
                </li>
                <li class="active">
                    <a href="<?php echo Router::url(array('controller' => 'Config', 'action' => 'manager')); ?>"> <i class="menu-icon fa fa-gears"></i>Cài đặt</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
</aside>