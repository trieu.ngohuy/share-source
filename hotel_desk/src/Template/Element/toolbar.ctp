<div class="al-l" id="toolbarButtons">
    <button type="button" id="btnToDay" class="btn btn-primary btn-lg waves-effect btn-switch-mode" data-mode="2" data-direction="1">Hôm nay</button>
    <button type="button" id="btnWeek" class="btn btn-default btn-lg waves-effect btn-switch-mode" data-mode="1" data-direction="1">Tuần</button>
    <button type="button" id="btnMonth" class="btn btn-info btn-lg waves-effect btn-switch-mode" data-mode="0" disabled="" data-direction="1">Tháng</button>
</div>
<div class="al-c toolbar-week">
    <button type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float btn-refill-booking" data-direction="0">
        <i class="material-icons">skip_previous</i>
    </button>
</div>
<div class="toolbar-week">
    <div class="form-group">
        <div class="form-line">
            <input type="text" class="al-c form-control" id="inpWeekLabel" value="Tuần 1 ( 1/6-7/6)" disabled>
        </div>
    </div>
</div>
<div class="al-c toolbar-week">
    <button type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float btn-refill-booking" data-direction="2">
        <i class="material-icons">skip_next</i>
    </button>
</div>