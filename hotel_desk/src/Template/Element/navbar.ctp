<?php
use Cake\Routing\Router;
?>
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="<?php echo Router::url(array('controller' => 'Booking', 'action' => 'index')); ?>"><?php echo $websiteName ?></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <?php
            if(isset($isBooking)){
                ?>
                <div class="toolbar" id="divToolbar">
                    <?= $this->element('toolbar') ?>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</nav>