<?php

use Cake\Core\Configure;
?>
<div class="modal fade" id="newBookingModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-slg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="newBookingModalLabel">Thông Tin Đặt Phòng</h4>
            </div>
            <form action="" method="post" id="frmBookingDetail">
                <div class="modal-body">
                    <div id="pay-invoice">
                        <div class="card-body">
                            <div class="row">

                                <!--Hidden input-->
                                <input id="inpBookingId" name="BrBookingId" type="hidden" class="form-control cc-name valid">
                                <input id="chkIsVAT" name="BrIsVAT" type="hidden" class="form-control cc-name valid">
                                <input id="inpTransferRoomId" name="BrTransferRoomId" type="hidden" class="form-control cc-name valid">
                                <input id="inpTransferDate" name="BrTransferDate" type="hidden" class="form-control cc-name valid">

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="comBookingRoomStatus" class="control-label mb-1">Tình trạng phòng</label>
                                        <select name="BrStatus" id="comBookingRoomStatus" class="form-control show-tick">                                            
                                            <option value="1">Đặt phòng</option>
                                            <option value="2">Check in</option>
                                            <option value="3">Check out</option>
                                            <option value="4">Đã kiểm toàn, đã kiểm tra</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="comBookingRoom" class="control-label mb-1">Chọn phòng</label>
                                        <select name="BrRoomId" id="comBookingRoom" class="form-control show-tick">
                                        </select>
                                        <div class="checkbox " id="extrasCheckboxRoom">
                                            <label for="exTrasCheckboxRoom" class="form-check-label sub-title">
                                            </label>
                                        </div>
                                    </div>

                                    <label for="email_address">Tên khách hàng</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input  tabindex="1" required="" placeholder="Tên khách hàng..." id="inpBookingCustomerName" name="BrCustomerName" type="text" class="special-input form-control tab-input tab-input-1" maxlength="25">
                                        </div>
                                    </div>

                                    <label for="email_address">Email khách hàng</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input  tabindex="2" placeholder="Email khách hàng..." id="inpBookingCustomerEmail" name="BrCustomerEmail" type="email" class="form-control tab-input tab-input-2">
                                        </div>
                                    </div>

                                    <label for="email_address">Sđt khách hàng</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input  tabindex="3" placeholder="Sđt khách hàng..." id="inpBookingCustomerPhone" name="BrCustomerPhone" type="text" class="form-control number-input tab-input tab-input-3">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-5">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="email_address">Từ ngày</label>
                                            <div class="form-group">
                                                <div class="form-line" id="fromDateWrap">
                                                    <input name="BrFromDate" class="datepicker form-control" type="text" placeholder="click to show datepicker" id="datBookingFromDate" required/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="email_address">Tới ngày</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input name="BrToDate" class="datepicker form-control" type="text" placeholder="click to show datepicker" id="datBookingToDate" required/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <button data-type="Aldult" type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float decrease-people">
                                                        <i class="material-icons">remove</i>
                                                    </button>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input  tabindex="4" min="0" value="0" required="" id="inpBookingAldult" name="BrAldult" type="text" class="number-input input-booking-people form-control tab-input tab-input-4" data-type="aldult">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <button  data-type="Aldult" type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float increase-people">
                                                        <i class="material-icons">add</i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <button data-type="Baby" type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float decrease-people">
                                                        <i class="material-icons">remove</i>
                                                    </button>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input  tabindex="5" min="0" value="0" required="" id="inpBookingBaby" name="BrBaby" type="text" class="number-input input-booking-people form-control tab-input tab-input-5" data-type="baby">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <button  data-type="Baby" type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float increase-people">
                                                        <i class="material-icons">add</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <br>
                                            <span><b>Người lớn: </b> <?php echo $configAldult . ' ' . $currency ?> </span><br>
                                            <span><b>Trẻ em dưới 10 tuổi: </b> Miễn phí</span>
                                        </div>
                                    </div>
                                    <label for="email_address">Đặt cọc</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input min="0" required="" id="inpBookingDeposit" name="BrDeposit" type="text" class="form-control number-input inpPrice" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <label class="control-label mb-1">Nhận phòng sớm / Trả phòng muộn</label>
                                        <input value="" id="inpBookingModeString" name="BrMode" type="hidden" class="form-control cc-name valid inpPrice">
                                        <div class="demo-checkbox" id="divModeWrap">
                                            <div class='row'>
                                                <div class="col-sm-2">
                                                    <input checked="" type="checkbox" id="md_checkbox_30" name="ChkMode" value="0" class="filled-in chk-col-green chk-booking-mode chk-booking-mode-none">
                                                    <label for="md_checkbox_30">Không</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-8 pd-0">                                                            
                                                            <input type="checkbox" id="md_checkbox_21" name="ChkMode" value="1" class="filled-in chk-col-green chk-booking-mode chk-booking-mode-early">
                                                            <label for="md_checkbox_21">Nhập phòng sớm</label>
                                                        </div>
                                                        <div class="col-sm-12 pd-0">
                                                            <select id="comGetRoomEarly" class="form-control-sm form-control">

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 pd-l-0" id="modeBefore16">
                                                    <input type="checkbox" id="md_checkbox_33" name="ChkMode" value="2" class="filled-in chk-col-green chk-booking-mode chk-booking-mode-before16">
                                                    <label for="md_checkbox_33" class="mode-custom">Trả phòng muộn từ 12h10-16h</label>
                                                </div>
                                                <div class="col-sm-3">                                                    
                                                    <input type="checkbox" id="md_checkbox_26" name="ChkMode" value="3" class="filled-in chk-col-green chk-booking-mode chk-booking-mode-after16">
                                                    <label for="md_checkbox_26" class="mode-custom">Trả phòng muộn sau 16h</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-services" class="control-label mb-1">Dịch vụ</label>
                                        <input required="" id="inpServices" name="BrServices" type="hidden" class="form-control cc-name valid">
                                        <hr>
                                        <div id="divServicesDetailWrap">

                                        </div>
                                        <button type="button" id="btnBookingServiceAdd" class="btn btn-xs btn-info waves-effect" data-toggle="modal" data-target="#servicesModal">
                                            <i class="material-icons">add</i>
                                            <span>Thêm mới dịch vụ</span>
                                        </button>
                                    </div>
                                    <div class="form-group">
                                        <label for="email_address">Ghi chú</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea  tabindex="6" id="inpBookingNote" name="BrNote" id="textarea-input" rows="2" placeholder="Ghi chú..." class="form-control no-resize tab-input tab-input-6"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group al-r">
                                        <h4 class="mg-bottom-20">Thành tiền</h4>
                                        <hr>
                                        <div class="row" id="bookingTotalMoneyWrap">
                                            <div class="col-sm-12 al-l" id="totalMoneyList">
                                                <p>
                                                    <b>Tiền chuyển phòng (Phòng trước đó):</b> <span id="totalMoneyRoomEstras">0</span> <?php echo Configure::read('Currency') ?>
                                                </p>
                                                <p>
                                                    <b>Tiền phòng:</b> <span id="totalMoneyRoom">0</span> <?php echo Configure::read('Currency') ?>
                                                    <br><span class="sub-title" id="roomSubTitle">Chuyển từ phòng 101 (Single) từ ngày 01/06/2018</span>
                                                </p>
                                                <p><b>Tiền dịch vụ:</b> <span id="totalMoneServices">0</span> <?php echo Configure::read('Currency') ?></p>
                                                <p><b>Tiền đặt cọc:</b> <span id="totalMoneyDeposit">0</span> <?php echo Configure::read('Currency') ?></p>
                                                <p>
                                                    <b>Tiền nhận phòng sớm / trả phòng muộn:</b> <span id="totalMoneyMode">0</span> <?php echo Configure::read('Currency') ?>
                                                    <br><span class="sub-title" id="earlyLateSubTitle"></span>
                                                </p>
                                                <p>
                                                    <b>Tiền thêm người:</b> <span id="totalMoneyPeopelEstras">0</span> <?php echo Configure::read('Currency') ?>
                                                    <br><span class="sub-title" id="peopleSubTitle"></span>
                                                </p>
                                                <div>
                                                    <b>Xuất hóa đơn đỏ:</b>
                                                    <input type="checkbox" id="md_checkbox_36" name="BrIsVAT" class="filled-in chk-col-green chk-vat">
                                                    <label for="md_checkbox_36">Có</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 sperate-line"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <h4>Tổng tiền:</h4>
                                                <hr>
                                                <h1 id="totalMoneyAllWrap" class="al-r"><span id="totalMoneyAll">0</span> <?php echo Configure::read('Currency') ?></h1>
                                                    <p id="totalMoneyVATWrap" class="al-r"><b>VAT (0%):</b> <span id="totalMoneyVAT">0</span> <?php echo Configure::read('Currency') ?></>
                                                <hr>
                                                <h1 id="totalMoneyAfterVATWrap"><span id="totalMoneyAfterVAT">0</span> <?php echo Configure::read('Currency') ?></h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnModalBookingDetailSave" type="button" class="btn btn-primary waves-effect" data-type="save">Lưu</button>
                    <button id="btnModalBookingDetailExport" type="button" class="btn btn-info waves-effect">Check out</button>
                    <button id="btnModalBookingDetailClose" type="button" class="btn btn-default waves-effect" data-type="cancel">Hủy</button>
                </div>
            </form>
        </div>
    </div>
</div>