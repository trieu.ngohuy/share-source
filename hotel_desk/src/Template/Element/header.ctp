<?php

use Cake\Core\Configure;
?>
<header id="header" class="header">

    <div class="header-menu">

        <div class="col-sm-7">
            <i class="menu-icon fa fa-home"></i>
            <?= Configure::read('WebsiteName') ?>
            <i class="menu-icon fa fa-arrow-right"></i>
            <?php echo $toolbar_title ?>
        </div>
        <div class="col-sm-5">
            <div class="user-area dropdown float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?= $this->Html->image('favicon.ico', array('class' => 'user-avatar rounded-circle')) ?>

                </a>

                <div class="user-menu dropdown-menu authenticate-drop-menu">
                    <?php
                    if (count($loginInfo) > 0) {
                        echo $this->Html->link(
                                'Đăng xuất', '/logout', array('class' => 'nav-link')
                        );
                    } else {
                        echo $this->Html->link(
                                'Đăng nhập', '/login', array('class' => 'nav-link')
                        );
                    }
                    ?>
                </div>
            </div>

        </div>
    </div>

</header>