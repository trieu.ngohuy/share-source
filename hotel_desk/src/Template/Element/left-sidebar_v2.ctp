<?php

use Cake\Routing\Router;
?>

<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <?= $this->Html->image('logo.png', array('width' => '48', 'height' => '48')) ?>
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $loginInfo['UsFullName'] ?></div>
                <div class="email"><?php echo $loginInfo['UsEmail'] ?></div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <?php
                        if (count($loginInfo) > 0) {
                            ?>
                            <li><a href="<?php echo Router::url(array('controller' => 'Authenticate', 'action' => 'logout')); ?>"><i class="material-icons">input</i>Đăng xuất</a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list" id="catWrap">
                <li class="header">DANH MỤC QUẢN LÝ</li>
                <li <?php if($activeMenu === 'booking'){?>class="active"<?php }?>>
                    <a href="<?php echo Router::url(array('controller' => 'Booking', 'action' => 'index')); ?>" data-type="booking">
                        <i class="material-icons">home</i>
                        <span>Đặt phòng</span>
                    </a>
                </li>
                <li <?php if($activeMenu === 'users'){?>class="active"<?php }?>>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">person</i>
                        <span>Quản lý User</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <?=
                            $this->Html->link(
                                    'Quản lý', '/user/manager'
                            )
                            ?>
                        </li>
                        <li>
                            <?=
                            $this->Html->link(
                                    'Thêm mới', '/user/modify'
                            )
                            ?>
                        </li>
                    </ul>
                </li>
                <li <?php if($activeMenu === 'rooms'){?>class="active"<?php }?>>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">hotel</i>
                        <span>Quản lý Phòng</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <?=
                            $this->Html->link(
                                    'Quản lý', '/room/manager'
                            )
                            ?>
                        </li>
                        <li>
                            <?=
                            $this->Html->link(
                                    'Thêm mới', '/room/modify'
                            )
                            ?>
                        </li>
                    </ul>
                </li>
                <li <?php if($activeMenu === 'services'){?>class="active"<?php }?>>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">room_service</i>
                        <span>Quản lý Dịch vụ</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <?=
                            $this->Html->link(
                                    'Quản lý', '/service/manager'
                            )
                            ?>
                        </li>
                        <li>
                            <?=
                            $this->Html->link(
                                    'Thêm mới', '/service/modify'
                            )
                            ?>
                        </li>
                    </ul>
                </li>
                <li <?php if($activeMenu === 'report'){?>class="active"<?php }?>>
                    <a href="<?php echo Router::url(array('controller' => 'Export', 'action' => 'index')); ?>">
                        <i class="material-icons">archive</i>
                        <span>Báo cáo</span>
                    </a>
                </li>
                <li <?php if($activeMenu === 'logs'){?>class="active"<?php }?>>
                    <a href="<?php echo Router::url(array('controller' => 'Logs', 'action' => 'index')); ?>">
                        <i class="material-icons">history</i>
                        <span>Lịch sử hoạt động</span>
                    </a>
                </li>
                <li <?php if($activeMenu === 'config'){?>class="active"<?php }?>>
                    <a href="<?php echo Router::url(array('controller' => 'Config', 'action' => 'index')); ?>">
                        <i class="material-icons">settings</i>
                        <span>Cài đặt</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2018 <a href="javascript:void(0);">Quản lý khách sạn</a>.
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>