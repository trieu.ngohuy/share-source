<?php

use Cake\Core\Configure;
?>
<div class="content mt-3">
    <div class="alert alert-danger" role="alert">
        <?php echo Configure::read("Message_905") ?>
    </div>
</div>