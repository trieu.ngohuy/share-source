<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
        <thead>
            <tr>
                <th>#</th>
                <th>Nội Dung</th>
                <th>Ngày thực hiện</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Nội Dung</th>
                <th>Ngày thực hiện</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $index = 1; ?>
            <?php foreach ($logs as $log): ?>
                <tr >
                    <td><?php echo $index; ?></td>
                    <td><?php echo $log->LgContent ?></td>
                    <td class="date"><?php echo $log->LgCreatedDate; ?></td>
                </tr>
                <?php $index++; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script>
    /*
     * Document ready
     * @type type
     */
    $(document).ready(function () {
        $(".date").each(function (e) {
            //Get html
            html = $(this).html();
            //Convert to normal date format
            html = ConvertDBDateTime(html);
            //Set html
            $(this).html(html);
        });
    });
    /*
     * Convert db date time to common format
     */
    function ConvertDBDateTime(str){
        //Split string into array by comma\
        arr = str.split(',');
        //Convert current date into array
        date = arr[0].split('/');
        //Return
        return (parseInt(date[1]) < 10 ? '0' + date[1] : date[1]) + '-' + (parseInt(date[0]) < 10 ? '0' + date[0] : date[0]) + '-20' + date[2] + ',' + arr[1];
    }
</script>