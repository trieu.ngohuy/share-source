<?php
use Cake\Core\Configure;
use Cake\Routing\Router;
?>
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
        <thead>
            <tr>
                <th>#</th>
                <th>Username</th>
                <th>Full name</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Username</th>
                <th>Full name</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $index = 1; ?>
            <?php foreach ($users as $user): ?>
                <tr id="<?php echo $user->UsUserId ?>">
                    <td><?php echo $index; ?></td>
                    <td><?php echo $user->UsUserName ?></td>
                    <td><?php echo $user->UsFullName ?></td>
                    <td><?php echo $user->UsPhone ?></td>
                    <td class="al-c js-sweetalert">
                        <div class="btn-group btn-group-xs" role="group" aria-label="Extra-small button group">
                            <a href="modify/<?php echo $user->UsUserName ?>" class="btn btn-primary waves-effect">
                                <i class="material-icons">edit</i>
                            </a>
                            <button data-type="confirm" class="btn bg-pink waves-effect btn-delete" data-id="<?php echo $user->UsUserId ?>"
                                    data-title="Xóa dữ liệu" data-text="Bạn có chắc chắn muốn xóa không?" data-mode="warning"
                                    data-confirm="Xóa" data-cancel="Hủy">
                                <i class="material-icons">delete</i>
                            </button>
                        </div>
                    </td>
                </tr>
                <?php $index++; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script>
    var deleteUrl = '<?php echo Router::url(array('controller' => 'Users', 'action' => 'delete')); ?>';

</script>
<!--Link to common manager javascript handle-->
<?= $this->Html->script('manager.func.js') ?>