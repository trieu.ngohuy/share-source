<?php

use Cake\Routing\Router;
?>
<form action="" method="post" >

    <?php
    if (isset($data) && $data['status'] == 'false') {
        ?>
        <div class="alert alert-danger aler-div">
            <h4>Danh sách lỗi</h4>
            <hr>
            <ul class="err-list">
                <?php
                foreach ($data['message'] as $obj) {
                    echo $obj;
                }
                ?>
            </ul>
        </div>
        <?php
    }
    ?>
    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Tên đầy đủ</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="text" id="UsFullName" name="UsFullName" class="form-control" maxlength="50" required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['UsFullName'];
                           }
                           ?>"
                           placeholder="Nhập tên đầy đủ...">
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Tên đăng nhập</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="text" id="UsUserName" name="UsUserName" class="form-control" maxlength="11" required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['UsUserName'];
                           }
                           ?>"
                        <?php
                        if (isset($alias)) {
                            echo 'readonly';
                        }
                        ?>
                           placeholder="Nhập tên đăng nhập...">
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Mật khẩu</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="password" id="UsPassword" name="UsPassword" class="form-control" maxlength="11"
                        <?php
                        if(isset($alias) === false){
                            echo 'required=""';
                        }
                        ?>>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Nhập lại mật khẩu</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="password" id="UsPassword" name="UsRePassword" class="form-control" maxlength="11"
                        <?php
                        if(isset($alias) === false){
                            echo 'required=""';
                        }
                        ?>>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Loại người dùng</label>
            <select name="UsUserType" id="UsUserType" class="form-control show-tick">
                <option value="1"
                    <?php
                    if (isset($data) && $data['data']['UsUserType'] == 1) {
                        echo 'selected=""';
                    }
                    ?>>Nhân viên</option>
                <option value="0"
                    <?php
                    if (isset($data) && $data['data']['UsUserType'] == 0) {
                        echo 'selected=""';
                    }
                    ?>>Quản lý</option>
            </select>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Email</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="email" id="UsEmail" name="UsEmail" class="form-control" maxlength="50" required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['UsEmail'];
                           }
                           ?>"
                           placeholder="Nhập email...">
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Điện thoại</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="text" id="UsPhone" name="UsPhone" class="form-control" maxlength="11" required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['UsPhone'];
                           }
                           ?>"
                           placeholder="Nhập điện thoại...">
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Địa chỉ</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="text" id="UsAddress" name="UsAddress" class="form-control" maxlength="11" required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['UsAddress'];
                           }
                           ?>"
                           placeholder="Nhập địa chỉ...">
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12">
            <label for="email_address">Số chứng minh nhân dân</label>
            <div class="form-group">
                <div class="form-line">
                    <input type="text" id="UsCardNumber" name="UsCardNumber" class="form-control" maxlength="11" required=""
                           value="<?php
                           if (isset($data)) {
                               echo $data['data']['UsCardNumber'];
                           }
                           ?>"
                           placeholder="Nhập số chứng minh nhân dân...">
                </div>
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-primary waves-effect" id="btnSave">Lưu</button>
    <button type="reset" class="btn btn-default waves-effect" id="btnCancel">Hủy</button>
</form>
<script>
    jQuery(document).ready(function () {
        //Click button cancel
        jQuery("#btnCancel").click(function () {
            //Go to manager page
            location.href = '<?php echo Router::url(array('controller' => 'Users', 'action' => 'manager')); ?>';
        });
    });
</script>