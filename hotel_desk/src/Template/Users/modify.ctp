<?php
use Cake\Routing\Router;
?>
<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Form</strong> thêm mới người dùng
                    </div>
                    <form action="" method="post" class="form-horizontal">

                        <div class="card-body card-block">
                            <?php
                            if (isset($data) && $data['status'] == 'false') {
                                ?>
                                <div class="alert alert-danger aler-div" role="alert">
                                    <h4>Danh sách lỗi</h4>
                                    <hr>
                                    <ul class="err-list">
                                        <?php 
                                        foreach($data['message'] as $obj){
                                            echo $obj;
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="UsFullName" class=" form-control-label">Tên đầy đủ</label></div>
                                <div class="col-12 col-md-10">
                                    <input type="text" id="UsFullName" name="UsFullName" class="form-control" maxlength="50" required=""
                                           value="<?php
                                           if (isset($data)) {
                                               echo $data['data']['UsFullName'];
                                           }
                                           ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="UsUserName" class=" form-control-label">Tên đăng nhập</label></div>
                                <div class="col-12 col-md-10">
                                    <input type="text" id="UsUserName" name="UsUserName" class="form-control" maxlength="11" required=""
                                           value="<?php
                                           if (isset($data)) {
                                               echo $data['data']['UsUserName'];
                                           }
                                           ?>" 
                                           <?php
                                           if (isset($alias)) {
                                               echo 'readonly';
                                           }
                                           ?>>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-2"><label for="UsPassword" class=" form-control-label">Mật khẩu</label></div>
                                <div class="col-12 col-md-10">
                                    <input type="password" id="UsPassword" name="UsPassword" class="form-control" maxlength="11" >
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="UsRePassword" class=" form-control-label">Nhập lại mật khẩu</label></div>
                                <div class="col-12 col-md-10">
                                    <input type="password" id="UsPassword" name="UsRePassword" class="form-control" maxlength="11" >
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="UsUserType" class=" form-control-label">Loại người dùng</label></div>
                                <div class="col-12 col-md-10">
                                    <select name="UsUserType" id="UsUserType" class="form-control-sm form-control">
                                        <option value="1"
                                        <?php
                                        if (isset($data) && $data['data']['UsUserType'] == 1) {
                                            echo 'selected=""';
                                        }
                                        ?>>Nhân viên</option>
                                        <option value="0"
                                        <?php
                                        if (isset($data) && $data['data']['UsUserType'] == 0) {
                                            echo 'selected=""';
                                        }
                                        ?>>Quản lý</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="UsEmail" class=" form-control-label">Email</label></div>
                                <div class="col-12 col-md-10">
                                    <input type="email" id="UsEmail" name="UsEmail" class="form-control" maxlength="50" required=""
                                           value="<?php
                                           if (isset($data)) {
                                               echo $data['data']['UsEmail'];
                                           }
                                           ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="UsPhone" class=" form-control-label">Điện thoại</label></div>
                                <div class="col-12 col-md-10">
                                    <input type="text" id="UsPhone" name="UsPhone" class="form-control" maxlength="11" required=""
                                           value="<?php
                                           if (isset($data)) {
                                               echo $data['data']['UsPhone'];
                                           }
                                           ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="UsAddress" class=" form-control-label">Địa chỉ</label></div>
                                <div class="col-12 col-md-10">
                                    <input type="text" id="UsAddress" name="UsAddress" class="form-control" maxlength="11" required=""
                                           value="<?php
                                           if (isset($data)) {
                                               echo $data['data']['UsAddress'];
                                           }
                                           ?>">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2"><label for="UsCardNumber" class=" form-control-label">Số chứng minh nhân dân </label></div>
                                <div class="col-12 col-md-10">
                                    <input type="text" id="UsCardNumber" name="UsCardNumber" class="form-control" maxlength="11" required=""
                                           value="<?php
                                           if (isset($data)) {
                                               echo $data['data']['UsCardNumber'];
                                           }
                                           ?>">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm" id="btnSave">
                                <i class="fa fa-dot-circle-o"></i> Lưu
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm" id="btnCancel">
                                <i class="fa fa-ban"></i> Hủy
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>
    <!-- .animated -->
</div>
<script>
    jQuery(document).ready(function () {
        //Click button cancel
        jQuery("#btnCancel").click(function () {
            //Go to manager page
            location.href = '<?php echo Router::url(array('controller' => 'Users', 'action' => 'manager')); ?>';
        });
    });
</script>