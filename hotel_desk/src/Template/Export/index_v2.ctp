<?php

use Cake\Core\Configure;
use Cake\Routing\Router;
?>
<div class="row">
    <div class="col-sm-4" style="margin-top: 55px;">
        <div class="form-group">
            <div class="form-line">
                <input type="text" class="datepicker form-control al-c" placeholder="Please choose a date..."
                       id="inpFromDate">
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <button type="button" class="btn bg-cyan waves-effect btn-date" data-count="1">Ngày</button>
            <button type="button" class="btn bg-teal waves-effect btn-date" data-count="7">Tuần</button>
            <button type="button" class="btn bg-green waves-effect btn-date" data-count="0" id="btnMonth">Tháng</button>
            <button type="button" class="btn bg-light-green waves-effect btn-date" data-count="0" id="btnYear">Năm</button>
        </div>
        <div class="form-group">
            <div class="form-line">
                <input type="text" class="datepicker form-control al-c" placeholder="Please choose a date..." id="inpToDate">
            </div>
        </div>
    </div>

    <div class="col-sm-4 al-r">
        <button class="btn btn-primary waves-effect btn-export" id="btnExcel" data-type="excel">
            <i class="material-icons">cloud_download</i>
            <span>Xuất file excel</span>
        </button>
        <button class="btn btn-success waves-effect btn-export" id="btnPrint" data-type="print">
            <i class="material-icons">print</i>
            <span>In báo cáo</span>
        </button>
    </div>
</div>
<div class="al-r">
    <button type="button" class="btn bg-purple waves-effect" id="btnRead">
        <i class="material-icons">search</i>
        <span>Lọc dữ liệu</span>
    </button>
</div>
<hr>
<div id="divTotalMoney" class="al-r">
    <h4>Tổng Tiền</h4>
    <h1><span id="pTotalMoney"></span> <?php echo $currency?></h1>
</div>
<hr>
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
        <thead>
        <tr>
            <th>#</th>
            <th>Tên Khách Hàng</th>
            <th>Tên Phòng</th>
            <th>Giá Phòng</th>
            <th>Ngày Đến</th>
            <th>Ngày Đi</th>
            <th>Tổng Tiền</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>Tên Khách Hàng</th>
            <th>Tên Phòng</th>
            <th>Giá Phòng</th>
            <th>Ngày Đến</th>
            <th>Ngày Đi</th>
            <th>Tổng Tiền</th>
        </tr>
        </tfoot>
        <tbody>
        </tbody>
    </table>
</div>
<script>
    //Create data variables
    <?php
    echo "var js_rooms = " . json_encode($rooms) . ";\n";
    echo "var js_booking = " . json_encode($booking) . ";\n";
    echo "var js_services = " . json_encode($services) . ";\n";
    echo "var currency= '" . $currency . "';\n";
    echo "var js_get_room_early= " . json_encode($getRoomEarly) . ";\n";
    echo "var msgRoomStatusBooked = '" . Configure::read('Room_Status_Booked') . "';\n";
    echo "var msgRoomStatusCheckOut = '" . Configure::read('Room_Status_CheckOut') . "';\n";
    echo "var msgRoomStatusCheckIn = '" . Configure::read('Room_Status_CheckIn') . "';\n";
    echo "var msgRoomStatusClosed= '" . Configure::read('Room_Status_Closed') . "';\n";
    echo "var js_config_aldult= '" . $configAldult . "';\n";
    echo "var msgRoomStatusClosed= '" . Configure::read('Room_Status_Closed') . "';\n";
    echo "var vat= '" . $vat . "';\n";
    echo "var exportUrl = '" . Router::url(array('controller' => 'Pdf', 'action' => 'ExportReport')) . "';\n";
    echo "var exportExcelUrl = '" . Router::url(array('controller' => 'Pdf', 'action' => 'ExportExcel')) . "';\n";
    echo "var viewPdfUrl = '" . Router::url(array('controller' => 'Pdf', 'action' => 'ViewPdf')) . "';\n";
    ?>
    var mode = 'NGÀY';
</script>
<?= $this->Html->script('common.func.js') ?>
<?= $this->Html->script('booking-room.scripts.js') ?>

<script>
    jQuery(document).ready(function () {
        //Init date picker
        $(".datepicker").bootstrapMaterialDatePicker({
            format: dateFormat,
            weekStart: 1,
            time: false,
            currentDate: currentDate,
            switchOnClick: true,
            clearButton: false,
            lang: 'vi',
            okText: 'Chọn',
            cancelText: 'Hủy'
        });
        //Set to date as last day
        var lastDay = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);
        lastDay = ConvertDateToString(lastDay);
        $('#inpToDate').val(lastDay);

        //Set current month days count
        $('#btnMonth').attr('data-count', currentMonthDaysCount);
        //Set current month days count
        var firstDayOfYear = NewDate('1-1-' + currentDate.getFullYear());
        var lastDatOfYear = NewDate('31-12-' + currentDate.getFullYear());
        $('#btnYear').attr('data-count', CountBetweenDays(firstDayOfYear, lastDatOfYear) + 1);

        //Fill data
        FillData();

        //Increase date
        IncreaseDateClick();

        //Read data lick
        ReadDataClick();

        //Export report click
        ExportReport();
    });

    //Fill data
    function FillData(){
        var html = "";
        var totalMoney = 0;

        //Get input data
        var fromDate = $('#inpFromDate').val();
        var toDate = $('#inpToDate').val();
        //Sort data
        var searchBooking = js_booking.filter(x =>
            (CompareTwoDate(x.BrToDate, fromDate) === 1 || CompareTwoDate(x.BrToDate, fromDate) === 0) &&
            (CompareTwoDate(x.BrToDate, toDate) === 2 || CompareTwoDate(x.BrToDate, toDate) === 1)
        );

        //Loop data
        index = 1;
        for(var i = 0 ; i < searchBooking.length ; i++){
            var tmpObj = searchBooking[i];
            var roomObj = js_rooms.find(x => x.RoRoomId === tmpObj['BrRoomId']);
            //Open row
            html += '<tr>';
            //Index
            html += '<td>' +index+ '</td>';
            //Customer name
            html += '<td>' +tmpObj['BrCustomerName']+ '</td>';
            //Room name
            html += '<td>' +roomObj['RoName']+ '</td>';
            //Room price
            //html += '<td>' +FormatPrice(roomObj['RoPrice'])+ ' ' + currency + '</td>';
            html += '<td>' +roomObj['RoPrice']+ '</td>';
            //From date
            html += '<td>' +tmpObj['BrFromDate']+ '</td>';
            //To date
            html += '<td>' +tmpObj['BrToDate']+ '</td>';
            //Total money
            var tmpTotalMoney =  CalculatingRoomFee(tmpObj);
            totalMoney += tmpTotalMoney;
            html += '<td>' + CalculatingRoomFee(tmpObj) + '</td>';
            //Close row
            html += '</tr>';
            index++;
        }
        //Add list data to table
        $('tbody').html(html);
        //Write total money
        $('#pTotalMoney').html(FormatPrice(totalMoney));
    }

    /*
    Event click to increase date
     */
    function IncreaseDateClick(){
        $('.btn-date').click(function(){
            var count = $(this).attr('data-count');
            var fromDate = NewDate($('#inpFromDate').val());
            //Increase date
            fromDate.setDate(fromDate.getDate() + parseInt(count));
            //Set to day
            $('#inpToDate').val(ConvertDateToString(fromDate));
            //Update  mode
            if(parseInt(count) === 1){
                mode = 'NGÀY';
            }else if(parseInt(count) === 7){
                mode = 'TUẦN';
            }else if(parseInt(count) === currentMonthDaysCount ){
                mode = 'THÁNG';
            }else{
                mode = 'NĂM';
            }
        });
    }
    /*
    Read data button click event
     */
    function ReadDataClick(){
        $('#btnRead').click(function(){
            //Destroy old data
            $('.js-basic-example').DataTable().destroy();

            //Fill data
            FillData();

            //Reload jquery data table
            $('.js-basic-example').DataTable({
                responsive: true
            });
        });
    }
    /*
    Export report
     */
    function ExportReport(){
        $('.btn-export').click(function(){
            ExportPdfFile(exportUrl, {
                fromDate: $('#inpFromDate').val(),
                toDate: $('#inpToDate').val(),
                type: $(this).attr('data-type'),
                source: 'report'
            });
        });
    }
</script>
