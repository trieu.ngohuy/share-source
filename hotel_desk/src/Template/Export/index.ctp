<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-6">
                <button type="button" class="btn btn-outline-primary btn-sm">Ngày</button>
                <button type="button" class="btn btn-outline-secondary btn-sm">Tuần</button>
                <button type="button" class="btn btn-outline-success btn-sm">Tháng</button>
                <button type="button" class="btn btn-outline-warning btn-sm">Năm</button>
                <br>
                <div class="row mg-top-10">
                    <form>
                        <div class="input-group col-md-5">
                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            <input class="form-control" type="text" placeholder="click to show datepicker" id="fromDate" />
                        </div>
                        <div class="input-group col-md-5">
                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            <input class="form-control" type="text" placeholder="click to show datepicker" id="toDate" />
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-info">Tìm kiếm</button>
                        </div>
                    </form>
                </div>

            </div>
            <div class="col-md-6 alg-right">
                <button type="button" class="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp; Xuất file excel</button>
                <button type="button" class="btn btn-success"><i class="fa fa-print"></i>&nbsp; In ấn</button>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Dữ liệu đặt phòng</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tên phòng</th>
                                    <th>Từ ngày</th>
                                    <th>Tới ngày</th>
                                    <th>Dịch vụ</th>
                                    <th>Tổng tiền</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1; ?>
                                <?php foreach ($booking as $obj): ?>
                                    <tr>
                                        <td><?php echo $index;?></td>
                                        <td><?php echo $obj->RoName ?></td>
                                        <td><?php echo $obj->BrFromDate ?></td>
                                        <td><?php echo $obj->BrToDate ?></td>
                                        <td>
                                            <ul class="ul-no-style">
                                                <li>Nước uống</li>
                                                <li>Giặt là</li>
                                            </ul>
                                        </td>
                                        <td>320,800 VNĐ</td>
                                    </tr>  
                                    <?php $index++; ?>
                                <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .animated -->
</div>
<script>
    jQuery(document).ready(function () {
        //jQuery('#fromDate, #toDate').datepicker({});
        //Add total money
        html = '<hr>'
                + '<div class="row">'
                + '    <div class="col-md-12 alg-right" ><p><b>Tổng tiền: </b> 320,000 VNĐ</p></div>'
                + '</div>'
                + '<hr>';
        jQuery("#bootstrap-data-table_wrapper > div").last().before(html);
    });
</script>