<?php

use Cake\Core\Configure;
?>
<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="modalErrorLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?=
                $this->Html->image('../img/modal/error.png', array('height' => '50', 'class' => 'mg-right-10'))
                ?>
                <h5 class="modal-title mg-top-10" id="modalErrorLabel">Lỗi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p><?php echo Configure::read('Message_400') ?></p>
                <p id="errTitle">+ ERROR_DETAIL</p>
                <div id="errorWrap">
                    <hr>
                    <p id="errKey"><b>Key: </b></p>
                    <hr>
                    <iframe height="100%" width="100%" id="errorIframe" frameBorder="0"></iframe>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tiếp tục</button>
            </div>
        </div>
    </div>
</div>
<style>
    #errTitle{
        cursor: pointer;
    }
    #errorWrap{
        display: none;
    }
</style>
<script>
    jQuery(document).ready(function(){
        jQuery("#errTitle").click(function(){
            jQuery("#errKey").append(id);
            jQuery("#errorWrap").css('display', 'block');
        });
    });
</script>