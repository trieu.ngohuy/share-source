<div class="modal fade" id="newBookingModal" tabindex="-1" role="dialog" aria-labelledby="newBookingModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newBookingModalLabel">Book / Sửa phòng</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="pay-invoice">
                    <div class="card-body">
                        <form action="" method="post" novalidate="novalidate">
                            <div class="form-group">
                                <label for="cc-room" class="control-label mb-1">Tình trạng phòng</label>
                                <select name="select" id="cc-room" class="form-control">
                                    <option value="-">-</option>
                                    <option value="0">Sẵn sàng nhận phòng</option>
                                    <option value="1">Đang ở</option>
                                    <option value="2">Đang dọn / Chờ dọn phòng</option>
                                    <option value="3">Đã đặt phòng</option>
                                    <option value="4">Đã kế toán xong / kiểm tra xong</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="cc-room" class="control-label mb-1">Chọn phòng</label>
                                <select name="select" id="cc-room" class="form-control">
                                    <option value="0">-</option>
                                    <option value="1">101 (Single)</option>
                                    <option value="2">102 (Double)</option>
                                    <option value="3">103 (Twin Room)</option>
                                </select>
                            </div>
                            <div class="form-group has-success">
                                <label for="cc-name" class="control-label mb-1">Tên khách hàng</label>
                                <input placeholder="Tên khách hàng..." id="cc-name" name="cc-name" type="text" class="form-control cc-name valid" data-val="true" data-val-required="Please enter the name" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
                                <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                            </div>
                            <div class="form-group">
                                <label for="cc-services" class="control-label mb-1">Dịch vụ</label>
                                <select data-placeholder="Your Favorite Football Team" multiple class="standardSelect" tabindex="5">
                                    <option value=""></option>
                                    <optgroup label="Ăn uống">
                                        <option>Nước suối</option>
                                        <option>Đồ ăn</option>
                                    </optgroup>
                                    <optgroup label="Giặt là">
                                        <option>Giặt đồ</option>
                                        <option>Là đồ</option>
                                    </optgroup>
                                    <optgroup label="Khác">
                                        <option>Thuê xe</option>
                                        <option>Tours</option>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="cc-note" class="control-label mb-1">Ghi chú</label>
                                <textarea name="textarea-input" id="textarea-input" rows="9" placeholder="Ghi chú..." class="form-control"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="cc-exp" class="control-label mb-1">Từ ngày</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                            <input class="form-control" type="text" placeholder="click to show datepicker" id="fromDate" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label for="cc-exp" class="control-label mb-1">Từ ngày</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input class="form-control" type="text" placeholder="click to show datepicker" id="toDate" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary">Lưu</button>
            </div>
        </div>
    </div>
</div>