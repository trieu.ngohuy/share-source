<div class="row room-detail-wrap">
    <div class="col-md-4">
        <aside class="profile-nav alt room-detail">
            <section class="card">
                <div class="card-header user-header alt bg-dark">
                    <div class="media">

                        <div class="media-body">
                            <h2 class="text-light display-6">Chị Liên</h2>
                            <p>Đã đặt cọc 300,000 VNĐ</p>
                            <button type="button" class="btn btn-primary btn-sm btnRoomHoverEdit" data-target="#newBookingModal">Thay đổi</button>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <a href="#"> <i class="fa fa-magic"></i> Trạng thái: Đang ở</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#"> <i class="fa fa-tasks"></i> Số lượng khách</a>
                        <div class="weather-category twt-category room-detail-service">
                            <ul>
                                <li class="active">
                                    <h5>2</h5>
                                    Người lớn
                                </li>
                                <li>
                                    <h5>1</h5>
                                    Trẻ em
                                </li>
                                <li>
                                    <h5>0</h5>
                                    Em bé
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>

            </section>
        </aside>
    </div>
</div>