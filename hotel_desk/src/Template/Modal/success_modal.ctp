<?php
use Cake\Core\Configure;
?>
<div class="modal fade" id="modalSuccess" tabindex="-1" role="dialog" aria-labelledby="modalSuccessLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?= $this->Html->image('../img/modal/success.png', 
                        array('height' => '50', 'class' => 'mg-right-10'))
                ?>
                <h5 class="modal-title mg-top-10" id="modalSuccessLabel">Thành Công</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    <?php echo Configure::read('Message_200')?>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tiếp tục</button>
            </div>
        </div>
    </div>
</div>