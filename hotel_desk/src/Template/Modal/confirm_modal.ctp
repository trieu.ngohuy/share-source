<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?= $this->Html->image('../img/modal/confirm.png', 
                        array('height' => '50', 'class' => 'mg-right-10'))
                ?>
                <h5 class="modal-title mg-top-10" id="modalConfirmLabel">...</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    ...
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="modalSubmit" onclick="ProcessFunction()">Tiếp tục</button>
            </div>
        </div>
    </div>
</div>