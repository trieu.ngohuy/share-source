<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Cài đặt</strong> 
                    </div>

                    <div class="card-body card-block">
                        <?php
                        if (isset($return)) {
                            if ($return['status'] == 'true') {
                                ?>
                                <div class="alert alert-success" role="alert">
                                    <?php echo $return['message'] ?>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="alert alert-danger" role="alert">
                                    <?php echo $return['message'] ?>
                                </div>
                                <?php
                            }
                        }
                        ?>
                        <?php foreach ($arrData as $obj): ?>
                            <?php if ($obj->CfAlias != 'get-room-early') { ?>
                                <form action="" method="post" class="form-horizontal">
                                    <div class="row form-group">
                                        <div class="col col-md-2"><label class=" form-control-label"><?php echo $obj->CfName ?></label></div>
                                        <div class="col col-md-8">
                                            <input type="hidden" name="CfConfigId" class="form-control" value="<?php echo $obj->CfConfigId ?>">
                                            <input type="hidden" name="CfName" class="form-control" value="<?php echo $obj->CfName ?>">
                                            <input id="inpValue-<?php echo $obj->CfAlias ?>" type="text" name="CfValue" class="form-control inpValue" required=""
                                                   value="<?php echo $obj->CfValue ?>">
                                        </div>
                                        <div class="col col-md-2">
                                            <button type="submit" class="btn btn-primary btn-sm" id="btnSave">
                                                <i class="fa fa-save"></i> Lưu
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            <?php } else { ?>
                                <hr>
                                <form action="" method="post" class="form-horizontal">
                                    <div class="row form-group">
                                        <div class="col col-md-2"><label class=" form-control-label"><?php echo $obj->CfName ?></label></div>
                                        <div class="col-12 col-md-10">
                                            <input type="hidden" name="CfConfigId" class="form-control" value="<?php echo $obj->CfConfigId ?>">
                                            <input type="hidden" name="CfName" class="form-control" value="<?php echo $obj->CfName ?>">
                                            <input type="hidden" name="CfValue" id="inpSumVal" class="form-control" value="<?php echo $obj->CfValue ?>">
                                            <div id="listHoursWrap">
                                                <?php
                                                $listEarlyConfig = explode('||', $obj->CfValue);

                                                //Get max current item and assign into javascript variable
                                                echo '<script> var maxEarlyRoomItemCount = ' . (count($listEarlyConfig) + 1) . '; </script>';

                                                $index = 1;
                                                foreach ($listEarlyConfig as $item):
                                                    $value = explode(':', $item);
                                                    ?>
                                                    <div class="row form-group" id="itemWrap-<?php echo $index ?>">
                                                        <div class="col col-md-1">                                                        
                                                            <input data-index="0" onchange="UpdateHoursVal(this)" onfocus="InputGetOldValue(this)" id="inpHour-<?php echo $index ?>" type="text" class="form-control inp-hour" required="" value="<?php echo $value[0] ?>h" data-id="<?php echo $index ?>">
                                                        </div>
                                                        <div class="col col-md-7">
                                                            <input data-index="1" onchange="UpdateHoursVal(this)" onfocus="InputGetOldValue(this)" id="inpValue-<?php echo $index ?>" type="text" class="form-control inp-value" required="" value="<?php echo $value[1] ?>" data-id="<?php echo $index ?>">
                                                        </div>
                                                        <div class="col col-md-4">
                                                            <button type="button" class="btn btn-danger btn-sm btn-delete" data-id="<?php echo $index ?>"
                                                                    onclick="RemoveEarlyHourHtml(this)">
                                                                <i class="fa fa-remove"></i> Xóa
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $index++;
                                                endforeach;
                                                ?>
                                            </div>
                                            <button onclick="AddNewEarlyHourHtml()" type="button" id="btnGetRoomEarlyNew" class="btn btn-link btn-sm"><i class="fa fa-plus-circle"></i>&nbsp; Thêm mới</button>
                                            <button type="submit" class="btn btn-primary btn-sm btn-save">
                                                <i class="fa fa-save"></i> Lưu
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <hr>
                            <?php } ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .animated -->
</div>
<!--Common js file-->
<?= $this->Html->script('common.func') ?>
<script>
    /*
     * Add new early hour html
     */
    function AddNewEarlyHourHtml() {
        tmp = '<div class="row form-group" id="itemWrap-' + maxEarlyRoomItemCount + '">'
                + '<div class="col col-md-1">'
                + '    <input data-index="0" onchange="UpdateHoursVal(this)" onfocus="InputGetOldValue(this)" id="inpHour-' + maxEarlyRoomItemCount + '" type="text" class="form-control inp-hour" required="" value="1h" data-id="' + maxEarlyRoomItemCount + '">'
                + '</div>'
                + '<div class="col col-md-7">'
                + '    <input data-index="1" onchange="UpdateHoursVal(this)" onfocus="InputGetOldValue(this)" id="inpValue-' + maxEarlyRoomItemCount + '" type="text" class="form-control inp-value" required="" value="" data-id="' + maxEarlyRoomItemCount + '">'
                + '</div>'
                + '<div class="col col-md-4">'
                + '    <button type="button" class="btn btn-danger btn-sm btn-delete" data-id="' + maxEarlyRoomItemCount + '"'
                + 'onclick="RemoveEarlyHourHtml(this)">'
                + '        <i class="fa fa-remove"></i> Xóa'
                + '    </button>'
                + '</div>'
                + '</div>';
        //Increate current count
        maxEarlyRoomItemCount++;
        //Add new html into page
        jQuery("#listHoursWrap").append(tmp);
    }
    /*
     * Remove current hour item
     */
    function RemoveEarlyHourHtml(e) {
        //Get id
        var id = jQuery(e).attr('data-id');

        //Get html block
        jQuery("#itemWrap-" + id).remove();

        //Update list sumVal
        var sumVal = jQuery("#inpSumVal").val();
        var arrSumval = sumVal.split('||');
        //Remove current hour
        arrSumval.splice(parseInt(id) - 1, 1);
        //Update to text box
        UpdateSumvalString(arrSumval);
    }

    /*
     * Save old value
     */
    function InputGetOldValue(e) {
        jQuery(e).data('val', jQuery(e).val());
    }
    ;

    /*
     * Catch change hour
     */
    function UpdateHoursVal(e) {
        //Get current value
        var val = jQuery(e).val();
        //Get index
        var index = parseInt(jQuery(e).attr('data-index'));
        //Get id
        var id = parseInt(jQuery(e).attr('data-id'));

        //If zero value then reset to old value
        if (val === 'h' || val === '') {
            jQuery(e).val(jQuery(e).data('val'));

        } else {
            UpdateSumEarlyHour(id, val, index);
        }
    }
    ;
    /*
     * Update sum early hour
     */
    function UpdateSumEarlyHour(id, val, index) {
        //Get total value
        var sumVal = jQuery("#inpSumVal").val();
        var arrSumval = sumVal.split('||');
        //If old item then update
        if (id <= arrSumval.length) {
            var tmp = arrSumval[id - 1];
            var arrTmp = tmp.split(':');
            arrTmp[index] = index === 0 ? val.substring(0, val.length - 1) : val;

            tmp = arrTmp.toString().replace(',', ':');
            arrSumval[id - 1] = tmp;
        } else {
            arrSumval[arrSumval.length] = val.substring(0, val.length - 1) + ":";
        }

        //Update to textbox sum val
        UpdateSumvalString(arrSumval);
    }
    /*
     * Update sumval string
     */
    function UpdateSumvalString(arrSumval) {
        //Create sumVal string from arrSumval
        var tmpStr = "";
        for (var i = 0; i < arrSumval.length; i++) {
            tmpStr += arrSumval[i] + "||";
        }
        tmpStr = tmpStr.substring(0, tmpStr.length - 2);
        //Update to textbox sum val
        jQuery("#inpSumVal").val(tmpStr);
    }
</script>