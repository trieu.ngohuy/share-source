<?php
if (isset($return)) {
    if ($return['status'] == 'true') {
        ?>
        <div class="alert alert-success">
            <?php echo $return['message'] ?>
        </div>
        <?php
    } else {
        ?>
        <div class="alert alert-danger">
            <?php echo $return['message'] ?>
        </div>
        <?php
    }
}
?>
<?php foreach ($arrData as $obj): ?>
    <?php
    switch ($obj->CfAlias) {
        case 'export-from-date':
            break;
        case 'export-to-date':
            break;
        case 'export-file-name':
            break;
        case 'icon':
            ?>
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="CfConfigId" class="form-control" value="<?php echo $obj->CfConfigId ?>">
                <input type="hidden" name="CfName" class="form-control" value="<?php echo $obj->CfName ?>">
                <h2 class="card-inside-title"><?php echo $obj->CfName ?></h2>
                <div class="row clearfix">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <input id="inpValue-<?php echo $obj->CfAlias ?>" name="CfValue" type="text"
                                       class="form-control inpValue" required=""
                                       placeholder="<?php echo $obj->CfName ?>"
                                       value="<?php echo $obj->CfValue ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary btn-sm waves-effect" id="btnSave">
                            <i class="fa fa-save"></i> Lưu
                        </button>
                    </div>
                </div>
            </form>
            <?php
            break;
        case 'logo':
            ?>
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="CfConfigId" class="form-control" value="<?php echo $obj->CfConfigId ?>">
                <input type="hidden" name="CfName" class="form-control" value="<?php echo $obj->CfName ?>">
                <h2 class="card-inside-title"><?php echo $obj->CfName ?></h2>
                <div class="row clearfix">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <input id="inpValue-<?php echo $obj->CfAlias ?>" name="CfValue" type="text"
                                       class="form-control inpValue" required=""
                                       placeholder="<?php echo $obj->CfName ?>"
                                       value="<?php echo $obj->CfValue ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary btn-sm waves-effect" id="btnSave">
                            <i class="fa fa-save"></i> Lưu
                        </button>
                    </div>
                </div>
            </form>
            <?php
            break;
        case 'get-room-early':
            ?>
            <hr>
            <form action="" method="post">
                <input type="hidden" name="CfConfigId" class="form-control" value="<?php echo $obj->CfConfigId ?>">
                <input type="hidden" name="CfName" class="form-control" value="<?php echo $obj->CfName ?>">
                <input type="hidden" name="CfValue" id="inpSumVal" class="form-control"
                       value="<?php echo $obj->CfValue ?>">
                <h2 class="card-inside-title"><?php echo $obj->CfName ?></h2>
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div id="listHoursWrap">
                            <?php
                            $listEarlyConfig = explode('||', $obj->CfValue);

                            //Get max current item and assign into javascript variable
                            echo '<script> var maxEarlyRoomItemCount = ' . (count($listEarlyConfig) + 1) . '; </script>';

                            $index = 1;
                            foreach ($listEarlyConfig as $item):
                                $value = explode(':', $item);
                                ?>
                                <div class="row clearfix" id="itemWrap-<?php echo $index ?>">
                                    <div class="col col-md-1">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input data-index="0" onchange="UpdateHoursVal(this)"
                                                       onfocus="InputGetOldValue(this)"
                                                       id="inpHour-<?php echo $index ?>"
                                                       type="text" class="form-control inp-hour" required=""
                                                       value="<?php echo $value[0] ?>h" data-id="<?php echo $index ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-md-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input data-index="1" onchange="UpdateHoursVal(this)"
                                                       onfocus="InputGetOldValue(this)"
                                                       id="inpValue-<?php echo $index ?>"
                                                       type="text" class="form-control inp-value" required=""
                                                       value="<?php echo $value[1] ?>" data-id="<?php echo $index ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-md-4">
                                        <button type="button" class="btn btn-danger btn-sm btn-delete waves-effect"
                                                data-id="<?php echo $index ?>"
                                                onclick="RemoveEarlyHourHtml(this)">
                                            <i class="fa fa-remove"></i> Xóa
                                        </button>
                                    </div>
                                </div>
                                <?php
                                $index++;
                            endforeach;
                            ?>
                        </div>
                        <button type="button" class="btn btn-success waves-effect" onclick="AddNewEarlyHourHtml()"
                                id="btnGetRoomEarlyNew">
                            <i class="material-icons">add</i>
                            <span>Thêm mới</span>
                        </button>
                        <button type="submit" class="btn btn-primary btn-sm btn-save waves-effect">
                            <i class="fa fa-save"></i> Lưu
                        </button>
                    </div>
                </div>
            </form>
            <hr>
            <?php
            break;
        default:
            ?>
            <form action="" method="post">
                <input type="hidden" name="CfConfigId" class="form-control" value="<?php echo $obj->CfConfigId ?>">
                <input type="hidden" name="CfName" class="form-control" value="<?php echo $obj->CfName ?>">
                <h2 class="card-inside-title"><?php echo $obj->CfName ?></h2>
                <div class="row clearfix">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <input id="inpValue-<?php echo $obj->CfAlias ?>" name="CfValue" type="text"
                                       class="form-control inpValue" required=""
                                       placeholder="<?php echo $obj->CfName ?>"
                                       value="<?php echo $obj->CfValue ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary btn-sm waves-effect" id="btnSave">
                            <i class="fa fa-save"></i> Lưu
                        </button>
                    </div>
                </div>
            </form>
        <?php
    }
    ?>
<?php endforeach; ?>
<!--Common js file-->
<?= $this->Html->script('common.func') ?>
<script>
    /*
    Save button click
    */
    $('body').on('click', '.btn-save', function () {
        //Get id
        //Get input data
        //Save data
    });
    /*
     * Add new early hour html
     */
    function AddNewEarlyHourHtml() {
        tmp = '<div class="row clearfix" id="itemWrap-' + maxEarlyRoomItemCount + '">'
            + '<div class="col col-md-1">'
            + '   <div class="form-group">'
            + '     <div class="form-line">'
            + '      <input data-index="0" onchange="UpdateHoursVal(this)" onfocus="InputGetOldValue(this)" id="inpHour-' + maxEarlyRoomItemCount + '" type="text" class="form-control inp-hour" required="" value="1h" data-id="' + maxEarlyRoomItemCount + '">'
            + '     </div>'
            + '    </div>'
            + '</div>'
            + '<div class="col col-md-7">'
            + '   <div class="form-group">'
            + '     <div class="form-line">'
            + '    <input data-index="1" onchange="UpdateHoursVal(this)" onfocus="InputGetOldValue(this)" id="inpValue-' + maxEarlyRoomItemCount + '" type="text" class="form-control inp-value" required="" value="" data-id="' + maxEarlyRoomItemCount + '">'
            + '     </div>'
            + '    </div>'
            + '</div>'
            + '<div class="col col-md-4">'
            + '    <button type="button" class="btn btn-danger btn-sm btn-delete" data-id="' + maxEarlyRoomItemCount + '"'
            + 'onclick="RemoveEarlyHourHtml(this)">'
            + '        <i class="fa fa-remove"></i> Xóa'
            + '    </button>'
            + '</div>'
            + '</div>';
        //Increate current count
        maxEarlyRoomItemCount++;
        //Add new html into page
        jQuery("#listHoursWrap").append(tmp);
    }

    /*
     * Remove current hour item
     */
    function RemoveEarlyHourHtml(e) {
        //Get id
        var id = jQuery(e).attr('data-id');

        //Get html block
        jQuery("#itemWrap-" + id).remove();

        //Update list sumVal
        var sumVal = jQuery("#inpSumVal").val();
        var arrSumval = sumVal.split('||');
        //Remove current hour
        arrSumval.splice(parseInt(id) - 1, 1);
        //Update to text box
        UpdateSumvalString(arrSumval);
    }

    /*
     * Save old value
     */
    function InputGetOldValue(e) {
        jQuery(e).data('val', jQuery(e).val());
    }
    ;

    /*
     * Catch change hour
     */
    function UpdateHoursVal(e) {
        //Get current value
        var val = jQuery(e).val();
        //Get index
        var index = parseInt(jQuery(e).attr('data-index'));
        //Get id
        var id = parseInt(jQuery(e).attr('data-id'));

        //If zero value then reset to old value
        if (val === 'h' || val === '') {
            jQuery(e).val(jQuery(e).data('val'));

        } else {
            UpdateSumEarlyHour(id, val, index);
        }
    }
    ;

    /*
     * Update sum early hour
     */
    function UpdateSumEarlyHour(id, val, index) {
        //Get total value
        var sumVal = jQuery("#inpSumVal").val();
        var arrSumval = sumVal.split('||');
        //If old item then update
        if (id <= arrSumval.length) {
            var tmp = arrSumval[id - 1];
            var arrTmp = tmp.split(':');
            arrTmp[index] = index === 0 ? val.substring(0, val.length - 1) : val;

            tmp = arrTmp.toString().replace(',', ':');
            arrSumval[id - 1] = tmp;
        } else {
            arrSumval[arrSumval.length] = val.substring(0, val.length - 1) + ":";
        }

        //Update to textbox sum val
        UpdateSumvalString(arrSumval);
    }

    /*
     * Update sumval string
     */
    function UpdateSumvalString(arrSumval) {
        //Create sumVal string from arrSumval
        var tmpStr = "";
        for (var i = 0; i < arrSumval.length; i++) {
            tmpStr += arrSumval[i] + "||";
        }
        tmpStr = tmpStr.substring(0, tmpStr.length - 2);
        //Update to textbox sum val
        jQuery("#inpSumVal").val(tmpStr);
    }
</script>