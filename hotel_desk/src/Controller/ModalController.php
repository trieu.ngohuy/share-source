<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use \Cake\Core\Configure;

/**
 * Description of ModalController
 *
 * @author h_trieu
 */
class ModalController extends AppController {

    public function initialize() {
        parent::initialize();
        // Set the layout
        $this->viewBuilder()->setLayout('empty');
    }

    /*
     * Confirm modal
     */

    public function confirmModal() {

        try {
            
        } catch (Exception $exc) {
            //Set message
            $this->set('message', Configure::read('Message_400'));
        }
    }
    /*
     * Success modal
     */
    public function successModal() {

        try {
            
        } catch (Exception $exc) {
            //Set message
            $this->set('message', Configure::read('Message_400'));
        }
    }
    /*
     * Error modal
     */
    public function errorModal() {

        try {
            
        } catch (Exception $exc) {
            //Set message
            $this->set('message', Configure::read('Message_400'));
        }
    }
    /*
     * Booking room hover info
     */
    public function bookingRoomInfo(){
        try {
            
        } catch (Exception $exc) {
            //Set message
            $this->set('message', Configure::read('Message_400'));
        }
    }
    /*
     * Booking room hover new
     */
    public function bookingRoomNew(){
        try {
            
        } catch (Exception $exc) {
            //Set message
            $this->set('message', Configure::read('Message_400'));
        }
    }
    /*
     * Booking room detail
     */
    public function bookingRoomDetail(){
        try {
            
        } catch (Exception $exc) {
            //Set message
            $this->set('message', Configure::read('Message_400'));
        }
    }
}
