<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use \Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Description of UsersController
 *
 * @author h_trieu
 */
class UsersController extends AppController {

    public function initialize() {
        parent::initialize();
        
        //Set toolbar title
        $this->set('toolbar_title', 'Quản lý người dùng');

        //Go to login page if not login
        if (count($this->loginInfo) <= 0) {
            $this->redirect('/login');
        }
        //There is no permission
        if ($this->loginInfo['UsUserType'] != 0) {
            $this->redirect('/');
        }
        //Active menu item
        $this->set('activeMenu', 'users');
        
    }

    /*
     * Manager function
     */

    public function manager() {
        // Set the layout
        $this->viewBuilder()->setLayout('manager_v2');
        //Set page title
        $this->set('title', 'Quản lý người dùng');
        //Get list users
        $users = $this->Users->find('all', array('order' => array('UsCreatedDate ASC')));
        //Passing data into view
        $this->set(compact('users'));
        //Display add new button
        $this->set('isDisplayButton', true);
        //Set layout
        $this->render('manager_v2');
    }

    /*
     * Get data
     */

    function GetData($alias) {
        $data = $this->Users->find('all', array(
            'conditions' => array('UsUserName' => $alias)
        ));
        $data = $data->toArray();
        //Set data to view
        $tmp = array(
            'UsUserId' => $data[0]['UsUserId'],
            'UsUserName' => $data[0]['UsUserName'],
            'UsFullName' => $data[0]['UsFullName'],
            'UsEmail' => $data[0]['UsEmail'],
            'UsPhone' => $data[0]['UsPhone'],
            'UsAddress' => $data[0]['UsAddress'],
            'UsCardNumber' => $data[0]['UsCardNumber'],
            'UsUserType' => $data[0]['UsUserType']
        );
        $this->set('data', array(
            'status' => 'true',
            'data' => $tmp
        ));
        return $tmp;
    }

    /*
     * Save data
     */

    function SaveData($data, $oldData, $alias) {
        //Create insert object
        $objTable = TableRegistry::get('Users');
        $obj = $objTable->newEntity();

        //Add data to insert       
        if ($alias != null) {
            $obj->UsUserId = $oldData['UsUserId'];
        } else {
            $obj->UsUserId = md5(Configure::read('MD5_Users') . $data['UsUserName']);
        }

        $obj->UsUserName = $data['UsUserName'];
        if ($data['UsPassword'] !== "") {
            $obj->UsPassword = md5(Configure::read('MD5_Users') . $data['UsPassword']);
        }
        $obj->UsFullName = $data['UsFullName'];
        $obj->UsEmail = $data['UsEmail'];
        $obj->UsPhone = $data['UsPhone'];
        $obj->UsAddress = $data['UsAddress'];
        $obj->UsCardNumber = $data['UsCardNumber'];
        $obj->UsUserType = $data['UsUserType'];
        $queryObj = $objTable->save($obj);

        //Save data
        if (!empty($queryObj)) {

            //Establish message
            if ($alias != null) {
                $content = "đã chỉnh sửa thông tin người dùng: <b>" . $data['UsUserName'] . "</b>";
            } else {
                $content = "đã thêm người dùng mới: <b>" . $data['UsUserName'] . "</b>";
            }
            //Save actitive
            $this->SaveActitive($content);

            //Go to manager page
            $this->redirect('/user/manager');
        } else {
            $this->set('data', array(
                'status' => 'false',
                'message' => Configure::read('Message_400'),
                'data' => $data
            ));
        }
    }

    /*
     * Validate input data
     */

    function ValidateData($data, $alias) {
        $validate = true;
        $arrError = array();
        //Check password
        if ($data['UsPassword'] !== "" || $data['UsRePassword'] !== "") {
            if ($data['UsPassword'] !== $data['UsRePassword']) {
                array_push($arrError, '<li>Mật khẩu không khớp</li>');
                $validate = false;
            }
        }
        if ($alias == null) {
            //Check alias name
            $check = $this->Users->find('all', array(
                'conditions' => array('UsUserName' => $data['UsUserName'])
            ));
            //If data exist
            if ($check->count() > 0) {
                array_push($arrError, '<li>Tên đăng nhập đã tồn tại: ' . $data['UsUserName'] . '</li>');
                $validate = false;
            }
        }
        $this->set('data', array(
            'status' => 'false',
            'message' => $arrError,
            'data' => $data
        ));
        return $validate;
    }

    /*
     * Modify function
     */

    public function modify($alias = null) {
        // Set the layout
        $this->viewBuilder()->setLayout('modify_v2');
        //Set page title
        $this->set('title', 'Thêm mới người dùng');
        try {
            $data = $this->request->getData();
            $oldData = array();
            //Edit
            if ($alias != null) {
                $oldData = $this->GetData($alias);
                //Set toolbar title
                $this->set('alias', $alias);
            }
            //There is request
            if ($this->request->is('post') && $this->ValidateData($data, $alias)) {
                //Update data
                $this->SaveData($data, $oldData, $alias);
            }
            $this->set('toolbar_title', 'Thêm mới người dùng');
            
            //Set layout
            $this->render('modify_v2');
        } catch (Exception $exc) {
            $this->ExceptionHandle();
        }
    }

    /*
     * Delete function
     */

    public function delete() {
        //Disable render layout
        $this->layout = false;
        $this->autoRender = false;
        $this->response->type('json');
        $json = "";
        try {
            //Get post data
            $data = $this->request->data;

            //If valid id
            if ($this->request->is('post')) {
                //Get current data
                $obj = $this->Users->get($data['id']);
                //Delete 
                if ($this->Users->delete($obj)) {
                    $json = json_encode(array(
                        'success' => true,
                        'message' => Configure::read('Message_200')
                    ));
                    //Establish message
                    $content = "đã xóa người dùng: <b>" . $obj->UsUserName . "</b>";
                    //Save actitive
                    $this->SaveActitive($content);
                } else {
                    $json = json_encode(array(
                        'success' => false,
                        'message' => Configure::read('Message_400')
                    ));
                }
            } else {
                //Set message
                $json = json_encode(array(
                    'success' => false,
                    'message' => Configure::read('Message_400')
                ));
            }
            //Return json
            $this->response->body($json);
            return $this->response;
        } catch (Exception $exc) {
            //Set message
            $json = json_encode(array(
                'success' => false,
                'message' => Configure::read('Message_400')
            ));
            //Return json            
            $this->response->body($json);
            return $this->response;
        }
    }

}
