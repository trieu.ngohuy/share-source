<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Date;

/**
 * Description of ExportController
 *
 * @author h_trieu
 */
class ExportController extends AppController {

    public function initialize() {
        parent::initialize();
        // Set the layout
        $this->viewBuilder()->setLayout('manager_v2');
        //Set variable to check if curent page is report page
        $this->set('isReport', true);
        //Set toolbar title
        $this->set('toolbar_title', 'Xuất báo cáo');
        //Set page title
        $this->set('title', 'Xuất báo cáo');
        //Active menu item
        $this->set('activeMenu', 'report');
    }

    /*
     * Index function
     */

    public function index() {
        //Load modal
        $this->loadModel('BookingRoom');
        $this->loadModel('Rooms');
        $this->loadModel('Services');

        //Export
        if ($this->request->is('post')) {
            //Update search conditional
        }

        //Get list services
        $services = $this->Services->find('all', array('order' => array('SrCreatedDate ASC')));
        //Get list rooms
        $rooms = $this->Rooms->find('all', array('order' => array('RoCreatedDate ASC')));
        //Get list booking rooms
        $booking = $this->BookingRoom->find('all', array('order' => array('BrCreatedDate ASC')));
        //Get early hours
        $this->set('getRoomEarly', $this->GetConfigData('get-room-early'));
        //Aldult
        $this->set('configAldult', $this->ConvertStringToPrice($this->GetConfigData('aldult')));
        //VAT
        $this->set('vat', $this->GetConfigData('vat'));

        //Set variables
        $this->set(compact('booking'));
        $this->set(compact('rooms'));
        $this->set(compact('services'));
        //Set layout
        $this->render('index_v2');
    }

}
