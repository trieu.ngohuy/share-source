<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Description of AuthenticateController
 *
 * @author h_trieu
 */
class AuthenticateController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        // Set the layout
        $this->viewBuilder()->setLayout('authenticate');
        //Load modal
        $this->loadModel('Users');
        //Set page title
        $this->set('title', 'Đăng nhập');
    }

    /*
     * Login function
     */

    public function login()
    {
        try {

            //Go to login page if not login
            if (count($this->loginInfo) > 0) {
                $this->redirect('/');
            }

            //Get all users
            $users = $this->Users->find('all');
            $this->set(compact('users'));
            //Set layout
            $this->render('login_v2');
        } catch (Exception $exc) {
            $this->ExceptionHandle();
        }
    }

    /*
     * Save login info
     */
    function SaveLoginInfo()
    {
        try{
            //Disable render layout
            $this->viewBuilder()->setLayout(false);
            $this->autoRender = false;
            $this->response->withType('json');
            $json = "";

            //Get post data
            $data = $this->request->getData();
            //Save login user id
            $this->SaveConfigData('LoginUserInfo', $data['UsUserId']);

            //Establish message
            $content = "đã đăng nhập vào lúc: <b>" . date("d-m-Y h:i:s a", time()) . "</b>";
            //Save actitive
            $this->SaveActitive($content);

            //Set message
            $json = json_encode(array(
                'success' => true
            ));
            //Return json
            $body = $this->response->getBody();
            $body->write($json);
            //$this->response->withBody($json);
            return $this->response;
        } catch (Exception $exc) {
            $this->ExceptionHandle();
        }

    }

    /*
     * Logout function
     */

    public function logout()
    {
        //Establish message
        date_default_timezone_set("Asia/Bangkok");
        $content = "đã đăng xuất vào lúc: <b>" . date("d-m-Y h:i:s a", time()) . "</b>";
        //Save actitive
        $this->SaveActitive($content);

        //Clear login variable
        $this->SaveConfigData('LoginUserInfo', '');

        //Go to homepage
        $this->redirect('/login');
    }

}
