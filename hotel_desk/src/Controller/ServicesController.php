<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use \Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Description of ServicesController
 *
 * @author h_trieu
 */
class ServicesController extends AppController {

    public function initialize() {
        parent::initialize();

        //Set toolbar title
        $this->set('toolbar_title', 'Quản lý dịch vụ');
        //Go to login page if not login
        if (count($this->loginInfo) <= 0) {
            $this->redirect('/login');
        }
        //There is no permission
        if ($this->loginInfo['UsUserType'] != 0) {
            $this->redirect('/');
        }
        //Active menu item
        $this->set('activeMenu', 'services');
    }

    /*
     * Manager function
     */

    public function manager() {
        // Set the layout
        $this->viewBuilder()->setLayout('manager_v2');
        //Get list data
        $services = $this->Services->find('all', array('order' => array('SrCreatedDate ASC')));

        //Passing data into view
        $this->set(compact('services'));
        //Set page title
        $this->set('title', 'Quản lý dịch vụ');
        //Display add new button
        $this->set('isDisplayButton', true);
        //Set layout
        $this->render('manager_v2');
    }

    /*
     * Get data
     */

    function GetData($alias) {
        $data = $this->Services->find('all', array(
            'conditions' => array('SrAlias' => $alias)
        ));
        $data = $data->toArray();
        //Set data to view
        $tmp = array(
            'SrServiceId' => $data[0]['SrServiceId'],
            'SrName' => $data[0]['SrName'],
            'SrAlias' => $data[0]['SrAlias'],
            'SrPrice' => $data[0]['SrPrice'],
            'SrParentId' => $data[0]['SrParentId']
        );
        $this->set('data', array(
            'status' => 'true',
            'data' => $tmp
        ));
        return $tmp;
    }

    /*
     * Save data
     */

    function SaveData($data, $oldData, $alias) {
        //Create insert object
        $objTable = TableRegistry::get('Services');
        $obj = $objTable->newEntity();

        //Add data to insert       
        if ($alias != null) {
            $obj->SrServiceId = $oldData['SrServiceId'];
        } else {
            $obj->SrServiceId = md5(Configure::read('MD5_Services') . $data['SrAlias']);
        }
        $obj->SrName = $data['SrName'];
        $obj->SrAlias = $data['SrAlias'];
        $tmp = str_replace(',', '', $data['SrPrice']);
        $tmp = str_replace('_', '', $tmp);
        $tmp = str_replace(' ', '', $tmp);
        $tmp = str_replace('VNĐ', '', $tmp);
        $obj->SrPrice = $tmp;
        $obj->SrParentId = $data['SrParentId'];
        $queryObj = $objTable->save($obj);
        //Save data
        if (!empty($queryObj)) {

            //Establish message
            if ($alias != null) {
                $content = "đã chỉnh sửa thông tin dịch vụ: <b>" . $data['SrName'] . "</b>";
            } else {
                $content = "đã thêm dịch vụ mới: <b>" . $data['SrName'] . "</b>";
            }
            //Save actitive
            $this->SaveActitive($content);
            //Go to manager page
            $this->redirect('/service/manager');
        } else {
            $this->set('data', array(
                'status' => 'false',
                'message' => Configure::read('Message_400'),
                'data' => $data
            ));
        }
    }

    /*
     * Validate input data
     */

    function ValidateData($data, $alias) {
        $validate = true;
        $arrError = array();
        if ($alias == null) {
            //Check alias name
            $check = $this->Services->find('all', array(
                'conditions' => array('SrAlias' => $data['SrAlias'])
            ));
            //If data exist
            if ($check->count() > 0) {
                array_push($arrError, '<li>Tên dịch vụ đã tồn tại: ' . $data['SrAlias'] . '</li>');
                $validate = false;
            }
        }
        $this->set('data', array(
            'status' => 'false',
            'message' => $arrError,
            'data' => $data
        ));
        return $validate;
    }

    /*
     * Modify function
     */

    public function modify($alias = null) {
        // Set the layout
        $this->viewBuilder()->setLayout('modify_v2');
        //Set page title
        $this->set('title', 'Thêm mới dịch vụ');
        try {
            $data = $this->request->getData();
            $oldData = array();
            //Edit
            if ($alias != null) {
                $oldData = $this->GetData($alias);
                //Set toolbar title
                $this->set('alias', $alias);
            }
            //There is request
            if ($this->request->is('post') && $this->ValidateData($data, $alias)) {
                //Update data
                $this->SaveData($data, $oldData, $alias);
            }
            //Get list data
            $services = $this->Services->find('all', array(
                'order' => array('SrCreatedDate ASC'),
                'conditions' => array('SrParentId' => '0')
            ));

            //Passing data into view
            $this->set(compact('services'));
            $this->set('toolbar_title', 'Thêm mới dịch vụ');

            //Set layout
            $this->render('modify_v2');
        } catch (Exception $exc) {
            $this->ExceptionHandle();
        }
    }

    /*
     * Delete function
     */

    public function delete() {
        //Disable render layout
        $this->layout = false;
        $this->autoRender = false;
        $this->response->type('json');
        $json = "";
        try {
            //Get post data
            $data = $this->request->data;

            //If valid id
            if ($this->request->is('post')) {
                //Get current room
                $obj = $this->Services->get($data['id']);
                //Delete room
                if ($this->Services->delete($obj)) {
                    $json = json_encode(array(
                        'success' => true,
                        'message' => Configure::read('Message_200')
                    ));
                    //Establish message
                    $content = "đã xóa dịch vụ: <b>" . $obj->SrName . "</b>";
                    //Save actitive
                    $this->SaveActitive($content);
                } else {
                    $json = json_encode(array(
                        'success' => false,
                        'message' => Configure::read('Message_400')
                    ));
                }
            } else {
                //Set message
                $json = json_encode(array(
                    'success' => false,
                    'message' => Configure::read('Message_400')
                ));
            }
            //Return json
            $this->response->body($json);
            return $this->response;
        } catch (Exception $exc) {
            //Set message
            $json = json_encode(array(
                'success' => false,
                'message' => Configure::read('Message_400')
            ));
            //Return json            
            $this->response->body($json);
            return $this->response;
        }
    }

}
