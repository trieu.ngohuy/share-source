<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use \Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;

/**
 * Description of ConfigController
 *
 * @author h_trieu
 */
class ConfigController extends AppController {

    public function initialize() {
        parent::initialize();
        // Set the layout
        $this->viewBuilder()->setLayout('modify_v2');
        //Set toolbar title
        $this->set('toolbar_title', 'Cài đặt');
        //Go to login page if not login
        if (count($this->loginInfo) <= 0) {
            $this->redirect('/login');
        }
        //There is no permission
        if ($this->loginInfo['UsUserType'] != 0) {
            $this->redirect('/');
        }
        //Active menu item
        $this->set('activeMenu', 'config');
    }

    /*
     * Manager function
     */

    public function manager() {
        try {
            //Get list rooms
            $arrData = $this->Config->find('all', array(
                'order' => array('CfCreatedDate DESC'),
                'conditions' => array(
                    "CfAlias !=" => "login-user-info"
                )
            ));

            //If post
            if ($this->request->is('post')) {
                $return = $this->SaveData($this->request->getData());
                //Send to view
                $this->set(array(
                    'return' => $return
                ));
            }

            //Passing data into view
            $this->set(compact('arrData'));
            //Set page title
            $this->set('title', 'Cài đặt');
            //Set layout
            $this -> render('manager_v2');
        } catch (Exception $exc) {
            $this->ExceptionHandle();
        }
    }

    /*
     * Save data
     */

    function SaveData($data) {
        //Create insert object
        $objTable = TableRegistry::get('Config');
        $obj = $objTable->newEntity();

        //Add data to insert
        $obj->CfConfigId = $data['CfConfigId'];
        $obj->CfValue = $data['CfValue'];
        $queryObj = $objTable->save($obj);

        //Save data
        if (!empty($queryObj)) {

            //Establish message
            $content = "đã chỉnh sửa thông tin cài đặt: <b>" . $data['CfName'] . "</b>";
            //Save actitive
            $this->SaveActitive($content);            
            //Return
            return array(
                'status' => 'true',
                'message' => Configure::read('Message_200')
            );
        } else {
            return array(
                'status' => 'false',
                'message' => Configure::read('Message_400')
            );
        }
    }

}
