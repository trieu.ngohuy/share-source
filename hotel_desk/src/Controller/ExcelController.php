<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use DateTime;

require_once(ROOT . DS . 'vendor' . DS . 'PHPExcel' . DS . 'Classes' . DS . 'PHPExcel.php');
require_once(ROOT . DS . 'vendor' . DS . 'PHPExcel' . DS . 'Classes' . DS . 'PHPExcel' . DS . 'IOFactory.php');


/**
 * Description of ExcelController
 *
 * @author h_trieu
 */
class ExcelController extends AppController
{
    /*
     * Excel version
     */

    public $ExcelVersion = 'Excel2007';
    /*
     * Excel extinction
     */
    public $ExcelExtinction = '.xlsx';
    /*
     * Path
     */
    public $path = ROOT . DS . 'Files' . DS;
    /*
     * Prefix room bill
     */
    public $prefixRoomBill = 'HoaDonPhong';
    /*
     * File template room bill
     */
    public $templateRoomBillName = 'Room_Bill_Template';

    public function initialize()
    {
        parent::initialize();
        // Set the layout
        $this->viewBuilder()->setLayout('empty');
    }

    /*
     * Open excel file
     */

    function OpenExcelFile($fileName)
    {
        $excelObj = \PHPExcel_IOFactory::createReader($this->ExcelVersion);
        $excelObj = $excelObj->load($this->path . $fileName);
        $excelObj->setActiveSheetIndex(0);
        return $excelObj;
    }

    /*
     * Save excel file
     */

    function SaveExcelFile($excelObj, $fileName)
    {
        $objWriter = \PHPExcel_IOFactory::createWriter($excelObj, $this->ExcelVersion);
        $objWriter->save($this->path . $fileName);
    }

    /*
     * Generate file name
     */

    function GenerateFileName($mode, $data)
    {
        //Get current date
        $date = new DateTime();
        $date = date_format($date, 'Y-m-d H:i:s');
        $tmp = '';
        //Establish middle name string
        if ($mode === $this->prefixRoomBill) {
            $this->loadModel('Rooms');
            //Get room object
            $room = $this->Rooms->find('all', array('conditions' => array('RoRoomId' => $data['BrRoomId'])));
            $room = $room->toArray();
            $tmp = $data['BrCustomerName'] . '_' . $room[0]['RoName'];
        }
        //Return
        return $mode . '_' . $tmp . '_' . $date . $this->ExcelExtinction;
    }

    /*
     * Export excel
     */
    public function ExprortExcel()
    {
        header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("content-disposition: attachment;filename=Baocao_" . date('d-m-Y H:i:s') . ".xls");

        //Write excel content
        echo "";
        exit();
    }

    /*
     * Export room bill
     */

    public function ExportRoomBill($bookingId)
    {

        try {
            //Begin rows index
            $templateRowIndex = 11;
            $beginRowIndex = 12;

            //Get booking info
            $this->loadModel('BookingRoom');
            $bookingObj = $this->BookingRoom->find('all', array('conditions' => array(
                'BrBookingId' => $bookingId
            )));
            $bookingObj = $bookingObj->toArray();

            //Open template file
            $excelObj = $this->OpenExcelFile('Report_Template_1' . $this->ExcelExtinction);

            // Set active sheet
            $excelObj->setActiveSheetIndex(0);

            // Get active sheet obj
            $activeSheet = $excelObj->getActiveSheet();

            //$activeSheet
            //    ->setCellValue('A1', 'EDITED Last Name')
            //    ->setCellValue('B1', 'EDITED First Name')
            //    ->setCellValue('C1', 'EDITED Age')
            //    ->setCellValue('D1', 'EDITED Sex')
            //    ->setCellValue('E1', 'EDITED Location');

            //Copy rows and format
            //$this->CopyRows($activeSheet, 11, 12, 1, 40);

            //Set rows height
            //$activeSheet->getRowDimension('11')->setRowHeight(300);

            //Set column height
            //$excelObj->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

            //Delete template rows
            //$activeSheet->removeRow(11, 1);

            //Save file
            $this->SaveExcelFile($excelObj, 'aaa.xlsx');
        } catch (Exception $exc) {
            $this->ExceptionHandle();
        }
    }

    /*
     * Copy rows and format
     */
    function CopyRows(PHPExcel_Worksheet $sheet, $srcRow, $dstRow, $height, $width)
    {
        for ($row = 0; $row < $height; $row++) {
            for ($col = 0; $col < $width; $col++) {
                $cell = $sheet->getCellByColumnAndRow($col, $srcRow + $row);
                $style = $sheet->getStyleByColumnAndRow($col, $srcRow + $row);
                $dstCell = \PHPExcel_Cell::stringFromColumnIndex($col) . (string)($dstRow + $row);
                $sheet->setCellValue($dstCell, $cell->getValue());
                $sheet->duplicateStyle($style, $dstCell);
            }

            $h = $sheet->getRowDimension($srcRow + $row)->getRowHeight();
            $sheet->getRowDimension($dstRow + $row)->setRowHeight($h);
        }

        foreach ($sheet->getMergeCells() as $mergeCell) {
            $mc = explode(":", $mergeCell);
            $col_s = preg_replace("/[0-9]*/", "", $mc[0]);
            $col_e = preg_replace("/[0-9]*/", "", $mc[1]);
            $row_s = ((int)preg_replace("/[A-Z]*/", "", $mc[0])) - $srcRow;
            $row_e = ((int)preg_replace("/[A-Z]*/", "", $mc[1])) - $srcRow;

            if (0 <= $row_s && $row_s < $height) {
                $merge = $col_s . (string)($dstRow + $row_s) . ":" . $col_e . (string)($dstRow + $row_e);
                $sheet->mergeCells($merge);
            }
        }
    }

}
