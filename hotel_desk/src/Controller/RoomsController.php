<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use \Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;

/**
 * Description of RoomsController
 *
 * @author h_trieu
 */
class RoomsController extends AppController {

    public function initialize() {
        parent::initialize();

        //Set toolbar title
        $this->set('toolbar_title', 'Quản lý phòng');
        //Go to login page if not login
        if (count($this->loginInfo) <= 0) {
            $this->redirect('/login');
        }
        //There is no permission
        if ($this->loginInfo['UsUserType'] != 0) {
            $this->redirect('/');
        }
        //Active menu item
        $this->set('activeMenu', 'rooms');
    }

    /*
     * Manager function
     */

    public function manager() {
        // Set the layout
        $this->viewBuilder()->setLayout('manager_v2');
        //Get list rooms
        $rooms = $this->Rooms->find('all', array('order' => array('RoCreatedDate ASC')));

        //Passing data into view
        $this->set(compact('rooms'));
        //Set page title
        $this->set('title', 'Quản lý phòng');
        //Display add new button
        $this->set('isDisplayButton', true);
        //Set layout
        $this->render('manager_v2');
    }

    /*
     * Get data
     */

    function GetData($alias) {
        $data = $this->Rooms->find('all', array(
            'conditions' => array('RoAlias' => $alias)
        ));
        $data = $data->toArray();
        //Set data to view
        $tmp = array(
            'RoRoomId' => $data[0]['RoRoomId'],
            'RoName' => $data[0]['RoName'],
            'RoAlias' => $data[0]['RoAlias'],
            'RoMaxAldult' => $data[0]['RoMaxAldult'],
            'RoMaxBaby' => $data[0]['RoMaxBaby'],
            'RoPrice' => $data[0]['RoPrice']
        );
        $this->set('data', array(
            'status' => 'true',
            'data' => $tmp
        ));
        return $tmp;
    }

    /*
     * Save data
     */

    function SaveData($data, $oldData, $alias) {
        //Create insert object
        $objTable = TableRegistry::get('Rooms');
        $obj = $objTable->newEntity();

        //Add data to insert        
        if ($alias != null) {
            $obj->RoRoomId = $oldData['RoRoomId'];
        } else {
            $obj->RoRoomId = md5(Configure::read('MD5_Rooms') . $data['RoAlias']);
        }

        $obj->RoName = $data['RoName'];
        $obj->RoAlias = $data['RoAlias'];
        $obj->RoMaxAldult = $data['RoMaxAldult'];
        $obj->RoMaxBaby = $data['RoMaxBaby'];

        $tmp = str_replace(',', '', $data['RoPrice']);
        $tmp = str_replace('_', '', $tmp);
        $tmp = str_replace(' ', '', $tmp);
        $tmp = str_replace('VNĐ', '', $tmp);
        $obj->RoPrice = $tmp;

        $queryObj = $objTable->save($obj);

        //Save data
        if (!empty($queryObj)) {

            //Establish message
            if ($alias != null) {
                $content = "đã chỉnh sửa thông tin phòng: <b>" . $data['RoName'] . "</b>";
            } else {
                $content = "đã thêm phòng mới: <b>" . $data['RoName'] . "</b>";
            }
            //Save actitive
            $this->SaveActitive($content);
            //Go to manager page
            $this->redirect('/room/manager');
        } else {
            $this->set('data', array(
                'status' => 'false',
                'message' => Configure::read('Message_400'),
                'data' => $data
            ));
        }
    }

    /*
     * Validate input data
     */

    function ValidateData($data, $alias) {
        $validate = true;
        $arrError = array();
        if ($alias == null) {
            //Check alias name
            $check = $this->Rooms->find('all', array(
                'conditions' => array('RoAlias' => $data['RoAlias'])
            ));
            //If data exist
            if ($check->count() > 0) {
                array_push($arrError, '<li>Tên phòng đã tồn tại: ' . $data['RoAlias'] . '</li>');
                $validate = false;
            }
        }
        $this->set('data', array(
            'status' => 'false',
            'message' => $arrError,
            'data' => $data
        ));
        return $validate;
    }

    /*
     * Modify function
     */

    public function modify($alias = null) {
        // Set the layout
        $this->viewBuilder()->setLayout('modify_v2');
        //Set page title
        $this->set('title', 'Thêm mới phòng');
        try {
            $data = $this->request->getData();
            $oldData = array();
            //Edit
            if ($alias != null) {
                $oldData = $this->GetData($alias);
                //Set toolbar title
                $this->set('alias', $alias);
            }
            //There is request
            if ($this->request->is('post') && $this->ValidateData($data, $alias)) {
                //Update data
                $this->SaveData($data, $oldData, $alias);
            }
            //Set title
            $this->set('toolbar_title', 'Thêm mới phòng');

            //Set layout
            $this->render('modify_v2');
        } catch (Exception $exc) {
            $this->ExceptionHandle();
        }
    }

    /*
     * Delete function
     */

    public function delete() {
        //Disable render layout
        $this->layout = false;
        $this->autoRender = false;
        $this->response->type('json');
        $json = "";
        try {
            //Get post data
            $data = $this->request->data;

            //If valid id
            if ($this->request->is('post')) {
                //Get current data
                $obj = $this->Rooms->get($data['id']);
                //Delete 
                if ($this->Rooms->delete($obj)) {
                    $json = json_encode(array(
                        'success' => true,
                        'message' => Configure::read('Message_200')
                    ));
                    //Establish message
                    $content = "đã xóa phòng: <b>" . $obj->RoName . "</b>";
                    //Save actitive
                    $this->SaveActitive($content);
                } else {
                    $json = json_encode(array(
                        'success' => false,
                        'message' => Configure::read('Message_400')
                    ));
                }
            } else {
                //Set message
                $json = json_encode(array(
                    'success' => false,
                    'message' => Configure::read('Message_400')
                ));
            }
            //Return json
            $this->response->body($json);
            return $this->response;
        } catch (Exception $exc) {
            //Set message
            $json = json_encode(array(
                'success' => false,
                'message' => Configure::read('Message_400')
            ));
            //Return json            
            $this->response->body($json);
            return $this->response;
        }
    }

}
