<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Chronos\Date;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    /*
     * Upload path
     */
    public $upload_path = ROOT . DS . 'webroot' . DS . 'upload';
    /*
     * Date format
     */
    public $dateFromat = 'd-m-Y';
    /*
     * Login user info
     */
    public $loginInfo = array();
    /*
     * Currency
     */
    public $currency = '';

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        //Get login info
        $this->loginInfo = $this->GetLoginInfo();
        $this->set('loginInfo', $this->loginInfo);

        //Get website name
        $this->set('websiteName', $this->GetConfigData('website-name'));
        $this->currency = $this->GetConfigData('currency');
        $this->set('currency', $this->currency);
        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    }

    /*
     * Exception handle
     */

    public function ExceptionHandle()
    {
        //Set message
        $json = json_encode(array(
            'success' => false,
            'message' => Configure::read('Message_400')
        ));
        //Return json         
        $this->layout = false;
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->body($json);
        return $this->response;
    }

    /*
     * Get login info
     */

    public function GetLoginInfo()
    {
        $tmp = array();

        //Load modal
        $this->loadModel('Config');
        $this->loadModel('Users');

        //Get config value
        $data = $this->Config->find('all', array(
            'conditions' => array('CfName' => 'LoginUserInfo')
        ));
        $tmpData = $data->toArray();
        //If already login
        if ($data->count() > 0 && $tmpData[0]['CfValue'] != '') {
            $tmp = array(
                'CfValue' => $tmpData[0]['CfValue']
            );
            //get urser info
            $user = $this->Users->find('all', array(
                'conditions' => array('UsUserId' => $tmp['CfValue'])
            ));
            $user = $user->toArray();
            $tmp = array(
                'UsUserId' => $user[0]['UsUserId'],
                'UsUserName' => $user[0]['UsUserName'],
                'UsFullName' => $user[0]['UsFullName'],
                'UsEmail' => $user[0]['UsEmail'],
                'UsPhone' => $user[0]['UsPhone'],
                'UsAddress' => $user[0]['UsAddress'],
                'UsCardNumber' => $user[0]['UsCardNumber'],
                'UsUserType' => $user[0]['UsUserType']
            );
        }

        //Return data
        return $tmp;
    }

    /*
     * Get previous data
     */

    public function GetPreviousUrl()
    {
        return $this->referer('/', true);
    }

    /*
     * Save actitive
     */

    public function SaveActitive($content)
    {
        try {
            //Set time zone
            date_default_timezone_set("Asia/Bangkok");

            //Get log user info
            $logUser = $this->GetLoginInfo();
            //Establish message        
            $content = "Người dùng <b>" . $logUser['UsUserName'] . "</b> " . $content;
            //Load modal
            $this->loadModel('Logs');
            //Create save object
            $objTable = TableRegistry::get('Logs');
            $obj = $objTable->newEntity();

            //Add data to insert
            $obj->LgLogId = rand(1, 1000000);
            $obj->LgContent = $content;
            $obj->LgCreatedDate = date("Y-m-d h:i:s", time());
            //Save data
            $queryObj = $objTable->save($obj);
        } catch (Exception $ex) {
            $this->ExceptionHandle();
        }
    }

    /*
     * Write config data
     */

    public function SaveConfigData($key, $value)
    {
        try {
            date_default_timezone_set("Asia/Bangkok");
            //load modal
            $this->loadModel('Config');

            //Establish save object
            $objTable = TableRegistry::get('Config');
            $obj = $objTable->newEntity();
            $obj->CfConfigId = md5(Configure::read('MD5_Config') . $key);
            $obj->CfName = $key;
            $obj->CfValue = $value;

            //Save data
            $objTable->save($obj);
        } catch (Exception $ex) {
            $this->ExceptionHandle();
        }
    }

    /*
     * Get config data
     */

    public function GetConfigData($data)
    {
        $this->loadModel('Config');
        $obj = $this->Config->find('all', array(
            'conditions' => array('CfAlias' => $data)
        ));
        $obj = $obj->toArray();
        return $obj[0]['CfValue'];
    }

    /*
     * Fomart price
     */

    public function ConvertStringToPrice($value)
    {
        $formatedPrice = "";
        //Convert to string
        $value = (string)$value;
        //Loop each char of value
        $count = 1;
        for ($i = strlen($value) - 1; $i >= 0; $i--) {
            //Add ',' between each 3 number
            if ($count >= 3 && $i > 0 && $count % 3 === 0) {
                $formatedPrice .= $value[$i] . ",";
            } else {
                $formatedPrice .= $value[$i];
            }
            $count++;
        }
        //Rever formated price then return
        $formatedPrice = strrev($formatedPrice);
        return $formatedPrice;
    }

    /*
     * Get number of days between two days
     */

    function CountDaysBetweenTwoDates($day1, $day2)
    {
        $datetime1 = new Date($this->ConvertBackToOriginalDateString($day1));
        $datetime2 = new Date($this->ConvertBackToOriginalDateString($day2));
        $difference = $datetime1->diff($datetime2);
//        echo 'Difference: '.$difference->y.' years, ' 
//                   .$difference->m.' months, ' 
//                   .$difference->d.' days';
        return $difference->d;
    }

    /*
     * Convert to format d-m-Y
     */
    function ConvertBackToOriginalDateString($str)
    {
        //Convert to arr
        $arr = explode("-", $str);
        return $arr[2] . '-' . $arr[1] . '-' . $arr[0];
    }

    /*
     * Compare two dates
     */
    function CompareTwoDates($day1, $day2)
    {
        //Create dates
        $day1 = new Date($day1);
        $day1 = strtotime($day1);
        $day2 = new Date($day2);
        $day2 = strtotime($day2);
        //day1 > day2
        if ($day1 > $day2) {
            return 0;
        }
        //Equal
        if ($day1 === $day2) {
            return 1;
        }
        //day1 < day2
        if ($day1 < $day2) {
            return 2;
        }
    }

    /*
     * Get single row from db
     */
    function GetSingleRow($table, $arrCondition, $arrSort)
    {
        //Get room Info
        $dataObj = $this->$table->find('all', array(
            'order' => $arrSort,
            'conditions' => $arrCondition));
        $dataObj = $dataObj->toArray();
        return $dataObj[0];
    }

    /*
     * Get all rows
     */
    function GetAll($table, $arrCondition, $arrSort)
    {
        //Get room Info
        $dataObj = $this->$table->find('all', array(
            'order' => $arrSort,
            'conditions' => $arrCondition));
        $dataObj = $dataObj->toArray();
        return $dataObj;
    }

    /*
     * Find in array
     */
    function FindInArray($arr, $key, $val)
    {
        for ($i = 0; $i < count($arr); $i++) {
            if ($arr[$i][$key] === $val) {
                return $arr[$i];
            }
        }
        return [];
    }

    /*
     * Remove all vietnamese special character to en
     */

    public function vn_to_en($str)
    {

        $unicode = array(

            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

            'd' => 'đ',

            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

            'i' => 'í|ì|ỉ|ĩ|ị',

            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

            'y' => 'ý|ỳ|ỷ|ỹ|ỵ',

            'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

            'D' => 'Đ',

            'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

            'I' => 'Í|Ì|Ỉ|Ĩ|Ị',

            'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

            'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

            'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

        );

        foreach ($unicode as $nonUnicode => $uni) {

            $str = preg_replace("/($uni)/i", $nonUnicode, $str);

        }
        $str = str_replace(' ', '_', $str);

        return $str;

    }
}
