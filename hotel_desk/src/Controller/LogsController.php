<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use \Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Description of LogsController
 *
 * @author h_trieu
 */
class LogsController extends AppController {

    public function initialize() {
        parent::initialize();
        // Set the layout
        $this->viewBuilder()->setLayout('manager_v2');
        //Set toolbar title
        $this->set('toolbar_title', 'Lịch sử hoạt động');

        //Go to login page if not login
        if (count($this->loginInfo) <= 0) {
            $this->redirect('/login');
        }
        //There is no permission
        if ($this->loginInfo['UsUserType'] != 0) {
            $this->redirect('/');
        }
        //Active menu item
        $this->set('activeMenu', 'logs');
    }

    /*
     * Manager function
     */

    public function index() {
        //Set page title
        $this->set('title', 'Lịch sử hoạt động');
        //Get list data
        $logs = $this->Logs->find('all', array('order' => array('LgCreatedDate DESC')));
        //Passing data into view
        $this->set(compact('logs'));
        //Set layout
        $this->render('index_v2');
    }

}
