<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Chronos\Date;

/**
 * Description of BookingController
 *
 * @author h_trieu
 */
class BookingController extends AppController {

    public function initialize() {
        parent::initialize();
        // Set the layout
        $this->viewBuilder()->setLayout('frontend_v2');
        //Load model
        $this->loadModel('BookingRoom');
        //Set toolbar title
        $this->set('toolbar_title', 'Đặt phòng');
        //Set page title
        $this->set('title', 'Đặt phòng');
        //Get currency
        $this->set('currency', $this->GetConfigData('currency'));
        //Get early hours
        $this->set('getRoomEarly', $this->GetConfigData('get-room-early'));
        //Aldult
        $this->set('configAldult', $this->ConvertStringToPrice($this->GetConfigData('aldult')));
        //VAT
        $this->set('vat', $this->GetConfigData('vat'));
        //Go to login page if not login
        if (count($this->loginInfo) <= 0) {
            $this->redirect('/login');
        }
        //Active menu item
        $this->set('activeMenu', 'booking');
        //Display toolbar
        $this->set('isBooking', true);
    }

    /*
     * Index function
     */

    public function index() {
        //Load modal
        $this->loadModel('Rooms');
        $this->loadModel('Services');

        //Get list rooms
        $rooms = $this->Rooms->find('all', array('order' => array('RoCreatedDate ASC')));
        //Get list booking rooms
        $booking = $this->BookingRoom->find('all', array(
            'order' => array('BrCreatedDate ASC')
        ));
        //Get list services
        $services = $this->Services->find('all', array('order' => array('SrCreatedDate ASC')));

        //Return
        $this->set(array('rooms' => $rooms, 'booking' => $booking, 'services' => $services));
        //Set layout
        $this->render('index_v2');
    }

    /*
     * Get max id
     */

    function GetMaxId($arrData) {
        $maxID = 0;
        $tmp = $arrData->toArray();
        for ($i = 0; $i < count($tmp); $i++) {
            if ($tmp[$i]['BrBookingId'] > $maxID) {
                $maxID = $tmp[$i]['BrBookingId'];
            }
        }
        return $maxID + 1;
    }

    /*
     * convert date to yyyy/mm/dd
     */
    function ConvertToBackOriginalDate($str){
        //Split by '-'
        $arr = explode('-', $str);
        //return
        return $arr[2] . '-' . $arr[1] . '-' . $arr[0];
    }
    /*
     * Update booking
     */
    public function UpdateBooking() {

        try {
            //Disable render layout
            $this->viewBuilder()->setLayout(false);
            $this->autoRender = false;
            $this->response->withType('json');
            $json = "";
            //Get Post data
            $data = $this->request->getData();
            //Create save data upject
            $objTable = TableRegistry::get('BookingRoom');
            $obj = $objTable->newEntity();
            
            //Update data
            $obj->BrBookingId = $data['BrBookingId'];            
            $obj->BrCustomerName = $data['BrCustomerName'];
            $obj->BrCustomerEmail = $data['BrCustomerEmail'];
            $obj->BrCustomerPhone = $data['BrCustomerPhone'];
            $obj->BrRoomId = $data['BrRoomId'];
            $obj->BrTransferRoomId = $data['BrTransferRoomId'];            
            $obj->BrFromDate = $data['BrFromDate'];
            $obj->BrToDate = $data['BrToDate'];
            if ($data['BrTransferDate'] !== '') {
                $obj->BrTransferDate = $data['BrTransferDate'];
            }
            $obj->BrNote = $data['BrNote'];
            $obj->BrServices = $data['BrServices'];
            $obj->BrStatus = $data['BrStatus'];
            $obj->BrDeposit = $data['BrDeposit'];
            $obj->BrAldult = $data['BrAldult'];
            $obj->BrBaby = $data['BrBaby'];
            $obj->BrMode = $data['BrMode'];
            $obj->BrIsVAT = $data['BrIsVAT'];
            $obj->BrCreateUserId = $this->loginInfo['UsUserId'];
            //Update services
            //Update to db
            $queryObj = $objTable->save($obj);

            //Check if update data success
            if($queryObj){
                $json = json_encode(array(
                    'success' => true
                ));
                /*
                 * Update activity
                 */
                    //Establish message
                    if ($data['BrBookingId'] !== null) {
                        $content = "đã chỉnh sửa thông tin đặt phòng của khách: <b>" . $data['BrCustomerName'] . "</b>";
                    } else {
                        $content = "đã thêm đặt phòng mới cho khách : <b>" . $data['BrCustomerName'] . "</b>";
                    }
                    //Save actitive
                    $this->SaveActitive($content);
            }else{
                //Set message
                $json = json_encode(array(
                    'success' => false
                ));
            }

            //Return json
            $body = $this->response->getBody();
            $body->write($json);
            //$this->response->withBody($json);
            return $this->response;
        } catch (Exception $exc) {
            $this->ExceptionHandle();
        }
    }
    /*
     * Delete booking 
     */
    public function DeleteBooking() {

        try {
            //Disable render layout
            $this->viewBuilder()->setLayout(false);
            $this->autoRender = false;
            $this->response->withType('json');
            $json = "";
            //Get Post data
            $data = $this->request->getData();
            //Create save data upject
            $objTable = TableRegistry::get('BookingRoom');
            $obj = $this->BookingRoom->get($data['BrBookingId']);       
            //Update to db
            $queryObj = $objTable->delete($obj);

            //Check if update data success
            if($queryObj){
                $json = json_encode(array(
                    'success' => true
                ));
                /*
                 * Update activity
                 */
                    //Establish message
                    $content = "đã xóa đặt phòng của khách: <b>" . $data['BrCustomerName'] . "</b>";
                    //Save actitive
                    $this->SaveActitive($content);
            }else{
                //Set message
                $json = json_encode(array(
                    'success' => false
                ));
            }

            //Return json
            $body = $this->response->getBody();
            $body->write($json);
            //$this->response->withBody($json);
            return $this->response;
        } catch (Exception $exc) {
            $this->ExceptionHandle();
        }
    }

}
