<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Rooms extends Entity {

    protected $_accessible = [
        '*' => true,
        'RoRoomId' => false,
        'slug' => false,
    ];

}
