/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : quanlyks

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 08/07/2018 16:39:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for booking_room
-- ----------------------------
DROP TABLE IF EXISTS `booking_room`;
CREATE TABLE `booking_room`  (
  `BrBookingId` int(11) NOT NULL AUTO_INCREMENT,
  `BrCustomerName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BrCustomerEmail` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BrCustomerPhone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BrRoomId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BrTransferRoomId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BrTransferDate` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `BrFromDate` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BrToDate` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BrNote` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BrStatus` int(1) NOT NULL COMMENT '1 - Booked, 2 - Check In, 3 - Check out, 4 - Closed',
  `BrDeposit` int(11) NULL DEFAULT NULL,
  `BrAldult` int(2) NULL DEFAULT 0,
  `BrBaby` int(2) NULL DEFAULT 0,
  `BrServices` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `BrMode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '0 - None, 1 - Early, 2 - Before 16, 3 - After 16',
  `BrIsVAT` int(1) NULL DEFAULT 0,
  `BrCreateUserId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BrCreatedDate` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`BrBookingId`) USING BTREE,
  INDEX `Rooms`(`BrRoomId`) USING BTREE,
  INDEX `CreateUser`(`BrCreateUserId`) USING BTREE,
  INDEX `Status`(`BrStatus`) USING BTREE,
  INDEX `TransferRooms`(`BrTransferRoomId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 91 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of booking_room
-- ----------------------------
INSERT INTO `booking_room` VALUES (8, 'b', '', '', 'F5D5C1A388392710CDAFD107DA186790', '', NULL, '18-06-2018', '21-06-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-22 07:06:08');
INSERT INTO `booking_room` VALUES (9, 'Example 22', '', '', 'D3FD84E3DF290744DAC5A35660CFECC3', '', NULL, '22-06-2018', '23-06-2018', 'This is note.\nThis is another note.', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-22 07:39:40');
INSERT INTO `booking_room` VALUES (10, '1', '', '', 'e2c737da514561089dc532af9caf0014', '', NULL, '22-06-2018', '23-06-2018', '', 3, 0, 1, 0, '', '0', NULL, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-22 07:39:46');
INSERT INTO `booking_room` VALUES (11, 'Example 22 33 44', '', '', 'b2bbadcd9dbd1a713dfe60f12a990cb1', '', NULL, '20-06-2018', '23-06-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-22 07:39:51');
INSERT INTO `booking_room` VALUES (12, 'c', '', '', 'cebf3ee19ac57b67916a955589eb3219', '', NULL, '21-06-2018', '23-06-2018', '', 3, 0, 0, 0, '', '0', NULL, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-22 07:42:22');
INSERT INTO `booking_room` VALUES (13, 'Example', '', '', 'F5D5C1A388392710CDAFD107DA186790', '', NULL, '14-06-2018', '15-06-2018', '', 1, 200000, 0, 0, '11:1||12:1', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-28 10:48:46');
INSERT INTO `booking_room` VALUES (14, 'Example 3', '', '', 'b2bbadcd9dbd1a713dfe60f12a990cb1', '', NULL, '14-06-2018', '15-06-2018', '', 3, 110000, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-29 06:09:21');
INSERT INTO `booking_room` VALUES (15, 'Example 1', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '29-06-2018', '30-06-2018', '', 3, 0, 1, 0, '', '0', 0, '42f217e545b1a8351372b61609dd3866', '2018-06-29 09:39:36');
INSERT INTO `booking_room` VALUES (16, 'Example 28', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '28-06-2018', '29-06-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:13:21');
INSERT INTO `booking_room` VALUES (17, 'Example 27', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '27-06-2018', '28-06-2018', '', 3, 0, 0, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:13:36');
INSERT INTO `booking_room` VALUES (19, 'Example 30/6', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '30-06-2018', '01-07-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:16:33');
INSERT INTO `booking_room` VALUES (20, 'Example 31_7', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '31-07-2018', '01-08-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:42:27');
INSERT INTO `booking_room` VALUES (21, 'Example 30_7', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '30-07-2018', '31-07-2018', '', 3, 0, 0, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:42:41');
INSERT INTO `booking_room` VALUES (22, 'Example 29_7', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '29-07-2018', '30-07-2018', '', 4, 0, 0, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:43:07');
INSERT INTO `booking_room` VALUES (23, 'Example 28_7', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '28-07-2018', '29-07-2018', '', 2, 0, 0, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:43:20');
INSERT INTO `booking_room` VALUES (24, 'Example 1_2', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '01-02-2018', '04-02-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 02:23:21');
INSERT INTO `booking_room` VALUES (25, 'Example 28_2', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '28-02-2018', '01-03-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 02:24:45');
INSERT INTO `booking_room` VALUES (26, 'Example 1_2', '', '', 'D3FD84E3DF290744DAC5A35660CFECC3', '', NULL, '01-02-2018', '02-02-2018', '', 3, 0, 0, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 02:24:57');
INSERT INTO `booking_room` VALUES (27, 'Example 5_7', '', '', 'D3FD84E3DF290744DAC5A35660CFECC3', '', NULL, '05-07-2018', '08-07-2018', '', 3, 150000, 4, 0, '3:1||6:1', '1:1||2', 1, '42f217e545b1a8351372b61609dd3866', '2018-07-02 02:53:34');
INSERT INTO `booking_room` VALUES (28, 'Example 1_7 2', '', '', 'F5D5C1A388392710CDAFD107DA186790', '', NULL, '01-07-2018', '03-07-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 03:02:32');
INSERT INTO `booking_room` VALUES (29, 'Example 1_7 3', '', '', 'e2c737da514561089dc532af9caf0014', '', NULL, '01-07-2018', '04-07-2018', '', 2, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 03:02:45');
INSERT INTO `booking_room` VALUES (30, 'Example 2_7', '', '', 'b2bbadcd9dbd1a713dfe60f12a990cb1', '', NULL, '02-07-2018', '04-07-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 03:04:41');
INSERT INTO `booking_room` VALUES (31, 'Example 2_7 2', '', '', 'cebf3ee19ac57b67916a955589eb3219', '', NULL, '02-07-2018', '03-07-2018', '', 2, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 07:46:34');
INSERT INTO `booking_room` VALUES (32, 'Example 1_8 22 33 44 55 6', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '01-08-2018', '02-08-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 09:03:54');
INSERT INTO `booking_room` VALUES (33, 'Example 1_8 2', '', '', 'D3FD84E3DF290744DAC5A35660CFECC3', '', NULL, '01-08-2018', '03-08-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 09:05:29');
INSERT INTO `booking_room` VALUES (34, 'Example 2/8 33 44 55 66 7', '', '', 'F5D5C1A388392710CDAFD107DA186790', '', NULL, '02-08-2018', '03-08-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 09:40:59');
INSERT INTO `booking_room` VALUES (35, 'Example 2\\.6', '', '', 'ec90cbb7dc5aba67d1df0a7c7b0c588f', '', NULL, '21-06-2018', '22-06-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 10:05:45');
INSERT INTO `booking_room` VALUES (36, 'Anh Luận', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '04-07-2018', '05-07-2018', '', 3, 0, 1, 0, '9:1', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 08:41:28');
INSERT INTO `booking_room` VALUES (37, 'Anh Khánh', '', 'a', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '03-07-2018', '04-07-2018', 'Note 1\nNote 2\nNote 3', 3, 0, 1, 0, '9:1', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 08:42:26');
INSERT INTO `booking_room` VALUES (38, 'chị Nam', '', '', 'D3FD84E3DF290744DAC5A35660CFECC3', '', NULL, '03-07-2018', '04-07-2018', '', 1, 0, 1, 0, '', '0', 0, '42f217e545b1a8351372b61609dd3866', '2018-07-03 08:44:12');
INSERT INTO `booking_room` VALUES (39, 'Anh Luan 1', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '03-08-2018', '04-08-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:13:48');
INSERT INTO `booking_room` VALUES (40, 'qwertyuio', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '04-08-2018', '05-08-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:16:05');
INSERT INTO `booking_room` VALUES (41, 'qwertyuio', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '01-09-2018', '02-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:18:14');
INSERT INTO `booking_room` VALUES (42, 'qwertyuio', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '02-09-2018', '03-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:18:22');
INSERT INTO `booking_room` VALUES (43, 'qwertyuio', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '03-09-2018', '04-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:18:30');
INSERT INTO `booking_room` VALUES (44, 'qwertyuio', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '04-09-2018', '05-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:18:38');
INSERT INTO `booking_room` VALUES (45, 'qwertyuio', '', '', 'D3FD84E3DF290744DAC5A35660CFECC3', '', NULL, '02-09-2018', '06-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:18:49');
INSERT INTO `booking_room` VALUES (46, 'qwertyuio', '', '', 'F5D5C1A388392710CDAFD107DA186790', '', NULL, '04-09-2018', '07-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:19:00');
INSERT INTO `booking_room` VALUES (47, 'qwertyuio', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '05-09-2018', '06-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:19:09');
INSERT INTO `booking_room` VALUES (48, 'qwertyuio', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '06-09-2018', '07-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:19:32');
INSERT INTO `booking_room` VALUES (49, 'qwertyuio', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '08-09-2018', '09-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:36:02');
INSERT INTO `booking_room` VALUES (50, 'qwertyuio', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '09-09-2018', '10-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:36:11');
INSERT INTO `booking_room` VALUES (51, 'qwertyuio', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '10-09-2018', '11-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:36:19');
INSERT INTO `booking_room` VALUES (52, 'qwertyuio', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '11-09-2018', '12-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:36:28');
INSERT INTO `booking_room` VALUES (53, 'qwertyuio', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '12-09-2018', '13-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:37:36');
INSERT INTO `booking_room` VALUES (54, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '01-10-2018', '02-10-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:39:04');
INSERT INTO `booking_room` VALUES (55, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '02-10-2018', '03-10-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:39:09');
INSERT INTO `booking_room` VALUES (56, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '03-10-2018', '04-10-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:39:14');
INSERT INTO `booking_room` VALUES (57, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '04-10-2018', '05-10-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:39:19');
INSERT INTO `booking_room` VALUES (58, 'qwertyuiopasdfghjklzxcvbn', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '05-10-2018', '06-10-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:54:01');
INSERT INTO `booking_room` VALUES (59, 'qwertyuiopasdfghjklzxcvbn', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '01-11-2018', '02-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:42:01');
INSERT INTO `booking_room` VALUES (60, 'qwertyuiopasdfghjklzxcvbn', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '02-11-2018', '03-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:42:33');
INSERT INTO `booking_room` VALUES (61, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '03-11-2018', '04-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:42:54');
INSERT INTO `booking_room` VALUES (62, 'Hoàng thị Mai Anh', '', '', 'F5D5C1A388392710CDAFD107DA186790', '', NULL, '03-07-2018', '04-07-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:53:15');
INSERT INTO `booking_room` VALUES (63, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '06-10-2018', '07-10-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:54:13');
INSERT INTO `booking_room` VALUES (64, 'qwertyuiopasdfghjklzx', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '07-10-2018', '08-10-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:54:26');
INSERT INTO `booking_room` VALUES (65, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '08-10-2018', '09-10-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:54:46');
INSERT INTO `booking_room` VALUES (66, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '09-10-2018', '10-10-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:04:02');
INSERT INTO `booking_room` VALUES (67, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '04-11-2018', '05-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:07:37');
INSERT INTO `booking_room` VALUES (68, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '05-11-2018', '06-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:07:43');
INSERT INTO `booking_room` VALUES (69, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '06-11-2018', '07-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:07:49');
INSERT INTO `booking_room` VALUES (70, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '07-11-2018', '08-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:07:56');
INSERT INTO `booking_room` VALUES (71, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '08-11-2018', '09-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:08:03');
INSERT INTO `booking_room` VALUES (72, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '09-11-2018', '10-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:08:10');
INSERT INTO `booking_room` VALUES (73, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '10-11-2018', '11-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:19:38');
INSERT INTO `booking_room` VALUES (74, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '11-11-2018', '12-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:19:46');
INSERT INTO `booking_room` VALUES (75, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '12-11-2018', '13-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:19:53');
INSERT INTO `booking_room` VALUES (76, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '13-11-2018', '14-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:19:59');
INSERT INTO `booking_room` VALUES (77, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '14-11-2018', '15-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:20:05');
INSERT INTO `booking_room` VALUES (78, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '15-11-2018', '16-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:20:13');
INSERT INTO `booking_room` VALUES (79, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '16-11-2018', '17-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:20:20');
INSERT INTO `booking_room` VALUES (80, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '17-11-2018', '18-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:20:27');
INSERT INTO `booking_room` VALUES (81, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '18-11-2018', '19-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:20:38');
INSERT INTO `booking_room` VALUES (82, 'qwertyuiopasdfghjkl', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '19-11-2018', '20-11-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:20:44');
INSERT INTO `booking_room` VALUES (83, '11 22 33 44 55 66 77 88 9', '', '', 'cebf3ee19ac57b67916a955589eb3219', '', NULL, '03-07-2018', '04-07-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 10:23:38');
INSERT INTO `booking_room` VALUES (85, 'Example 5_7', '', '', 'b2bbadcd9dbd1a713dfe60f12a990cb1', '', NULL, '05-07-2018', '06-07-2018', '', 3, 0, 1, 0, '', '0', 0, '42f217e545b1a8351372b61609dd3866', '2018-07-04 16:19:59');
INSERT INTO `booking_room` VALUES (86, 'qwertyuio', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '07-09-2018', '08-09-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 09:19:43');
INSERT INTO `booking_room` VALUES (87, 'Example 5_7', '', '1', '47f8694d10bb7383fb288aa71ddb4542', '', NULL, '05-07-2018', '06-07-2018', '', 1, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-08 09:15:16');
INSERT INTO `booking_room` VALUES (88, 'Example 5_7', '', '1', 'cebf3ee19ac57b67916a955589eb3219', '', NULL, '05-07-2018', '06-07-2018', '', 1, 0, 0, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-08 09:36:16');
INSERT INTO `booking_room` VALUES (89, 'Example 5_7', '', '1', 'ec90cbb7dc5aba67d1df0a7c7b0c588f', '', NULL, '05-07-2018', '06-07-2018', '', 1, 0, 0, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-08 09:36:23');
INSERT INTO `booking_room` VALUES (90, 'Example 5_7', '', '1', 'b3087d6c1be84da147e3692b5ce10a41', '', NULL, '05-07-2018', '06-07-2018', '', 1, 0, 0, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-08 09:36:30');

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config`  (
  `CfConfigId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CfName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CfAlias` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CfValue` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `CfCreatedDate` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`CfConfigId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('', 'Tên Khách sạn', 'hotel-name', 'Khách Sạn Demo', '2018-06-07 14:37:05');
INSERT INTO `config` VALUES ('1ac713007216c70d7b19433e3dc5731e', 'Giá phòng người lớn / 1 người', 'aldult', '100000', '2018-05-25 13:49:56');
INSERT INTO `config` VALUES ('49704672ddfc31c3afce000550f28b5e', 'Đơn vị tiền tệ', 'currency', 'VNĐ', '2018-05-25 13:43:07');
INSERT INTO `config` VALUES ('5394b98ed37cd79e174bc32a49a1a073', 'Tên Website', 'website-name', 'Quản lý khách sạn', '2018-05-25 13:49:56');
INSERT INTO `config` VALUES ('68be316a72eb5d14e3c40ff9f962c6a4', 'VAT', 'vat', '10', '2018-06-05 00:37:41');
INSERT INTO `config` VALUES ('6ebbc2a4a7b0863ebd4749cfe9aa95f9', 'Nhận phòng sớm', 'get-room-early', '1:100000||2:150000||4:250000||5:500000', '2018-05-25 13:49:56');
INSERT INTO `config` VALUES ('7abd623eff0c0e7d1945132d767fafe8', 'Email', 'email', 'zolawebgroup@gmail.com', '2018-06-07 14:37:24');
INSERT INTO `config` VALUES ('8e17ec05fc398bb49b3f48e88f7ef2e7', 'Số điện thoại', 'phone', '01674210615', '2018-06-07 14:37:59');
INSERT INTO `config` VALUES ('a198493f632696d0e4e5d37c9cf83485', 'Địa chỉ', 'address', '72, Tên Đường Đây, Thừa Thiên Huế, Việt Nam', '2018-06-07 14:38:28');
INSERT INTO `config` VALUES ('aa9c668a5430223533ce6bfc7c447b87', 'Website', 'website', 'zolawebgroup.com', '2018-06-07 14:38:49');
INSERT INTO `config` VALUES ('d4349bc5b935d0d27cfcf92949e3c9e3', 'LoginUserInfo', 'login-user-info', '5a4dabc96d48d4b1744d1a6c57876531', '2018-05-24 02:47:44');

-- ----------------------------
-- Table structure for logs
-- ----------------------------
DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs`  (
  `LgLogId` int(11) NOT NULL,
  `LgContent` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LgCreatedDate` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`LgLogId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Write log of every actitive on websites' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of logs
-- ----------------------------
INSERT INTO `logs` VALUES (6531, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Khánh</b>', '2018-07-08 04:26:31');
INSERT INTO `logs` VALUES (12238, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:17:05');
INSERT INTO `logs` VALUES (15497, 'Người dùng <b>nhanviena</b> đã đăng xuất vào lúc: <b>03-07-2018 03:43:41 pm</b>', '2018-07-03 03:43:41');
INSERT INTO `logs` VALUES (16327, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:36:02');
INSERT INTO `logs` VALUES (20752, 'Người dùng <b>admin</b> đã đăng xuất vào lúc: <b>05-07-2018 12:30:55 am</b>', '2018-07-05 12:30:55');
INSERT INTO `logs` VALUES (41718, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjklzxcvbn</b>', '2018-07-03 04:42:33');
INSERT INTO `logs` VALUES (43824, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin người dùng: <b>nhanviena</b>', '2018-07-04 11:16:09');
INSERT INTO `logs` VALUES (47913, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiop</b>', '2018-07-03 04:36:28');
INSERT INTO `logs` VALUES (68665, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:20:20');
INSERT INTO `logs` VALUES (70008, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:18:14');
INSERT INTO `logs` VALUES (70038, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Khánh</b>', '2018-07-05 12:28:17');
INSERT INTO `logs` VALUES (71045, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 28_7</b>', '2018-07-05 12:34:45');
INSERT INTO `logs` VALUES (83893, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 04:39:14');
INSERT INTO `logs` VALUES (89844, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghj</b>', '2018-07-03 04:39:40');
INSERT INTO `logs` VALUES (93232, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 04:19:43');
INSERT INTO `logs` VALUES (95368, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:20:13');
INSERT INTO `logs` VALUES (99182, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>chị Nam</b>', '2018-07-03 03:41:39');
INSERT INTO `logs` VALUES (112793, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiop</b>', '2018-07-03 04:36:49');
INSERT INTO `logs` VALUES (116150, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:07:56');
INSERT INTO `logs` VALUES (120309, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>11 22 33 44 55 66 77 88 9</b>', '2018-07-03 03:50:42');
INSERT INTO `logs` VALUES (144604, 'Người dùng <b>admin</b> đã đăng nhập vào lúc: <b>03-07-2018 03:43:49 pm</b>', '2018-07-03 03:43:49');
INSERT INTO `logs` VALUES (156495, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Khánh</b>', '2018-07-05 12:30:01');
INSERT INTO `logs` VALUES (169709, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfgh</b>', '2018-07-03 04:39:46');
INSERT INTO `logs` VALUES (171998, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:36:11');
INSERT INTO `logs` VALUES (173798, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 04:53:47');
INSERT INTO `logs` VALUES (176453, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:35:55');
INSERT INTO `logs` VALUES (179566, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 04:19:59');
INSERT INTO `logs` VALUES (185395, 'Người dùng <b>nhanviena</b> đã đăng nhập vào lúc: <b>04-07-2018 11:16:20 pm</b>', '2018-07-04 11:16:20');
INSERT INTO `logs` VALUES (190247, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:37:43');
INSERT INTO `logs` VALUES (206524, 'Người dùng <b>admin</b> đã đăng xuất vào lúc: <b>03-07-2018 03:43:55 pm</b>', '2018-07-03 03:43:55');
INSERT INTO `logs` VALUES (214997, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Luận 1</b>', '2018-07-03 04:13:06');
INSERT INTO `logs` VALUES (215973, 'Người dùng <b>admin</b> đã đăng xuất vào lúc: <b>04-07-2018 11:16:13 pm</b>', '2018-07-04 11:16:13');
INSERT INTO `logs` VALUES (221344, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 04:54:13');
INSERT INTO `logs` VALUES (222504, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopas</b>', '2018-07-03 04:40:21');
INSERT INTO `logs` VALUES (225495, 'Người dùng <b>nhanviena</b> đã đăng xuất vào lúc: <b>05-07-2018 12:32:40 am</b>', '2018-07-05 12:32:40');
INSERT INTO `logs` VALUES (227936, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfg</b>', '2018-07-03 04:43:01');
INSERT INTO `logs` VALUES (237671, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:20:06');
INSERT INTO `logs` VALUES (240357, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 30_7</b>', '2018-07-05 12:35:29');
INSERT INTO `logs` VALUES (240906, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjklzxcvbn</b>', '2018-07-03 04:42:27');
INSERT INTO `logs` VALUES (245972, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Luận</b>', '2018-07-03 04:12:55');
INSERT INTO `logs` VALUES (246857, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>a</b>', '2018-07-03 04:16:06');
INSERT INTO `logs` VALUES (247010, 'Người dùng <b>nhanviena</b> đã đăng nhập vào lúc: <b>05-07-2018 12:33:25 am</b>', '2018-07-05 12:33:25');
INSERT INTO `logs` VALUES (249604, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>11 22 33 44 55 66 77 88 9</b>', '2018-07-03 05:23:38');
INSERT INTO `logs` VALUES (262635, 'Người dùng <b>admin</b> đã đăng xuất vào lúc: <b>04-07-2018 11:14:01 pm</b>', '2018-07-04 11:14:01');
INSERT INTO `logs` VALUES (279053, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwert</b>', '2018-07-04 10:52:48');
INSERT INTO `logs` VALUES (285126, 'Người dùng <b>admin</b> đã xóa đặt phòng của khách: <b></b>', '2018-07-04 11:53:49');
INSERT INTO `logs` VALUES (295319, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:18:38');
INSERT INTO `logs` VALUES (300110, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjklzx</b>', '2018-07-03 04:54:26');
INSERT INTO `logs` VALUES (309601, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Khánh</b>', '2018-07-05 12:30:31');
INSERT INTO `logs` VALUES (314698, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:04:02');
INSERT INTO `logs` VALUES (317963, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiop</b>', '2018-07-03 04:36:41');
INSERT INTO `logs` VALUES (318329, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:08:03');
INSERT INTO `logs` VALUES (319977, 'Người dùng <b>nhanviena</b> đã đăng xuất vào lúc: <b>05-07-2018 12:34:18 am</b>', '2018-07-05 12:34:18');
INSERT INTO `logs` VALUES (326935, 'Người dùng <b>admin</b> đã đăng xuất vào lúc: <b>05-07-2018 12:33:21 am</b>', '2018-07-05 12:33:21');
INSERT INTO `logs` VALUES (330201, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 5_7</b>', '2018-07-08 04:36:30');
INSERT INTO `logs` VALUES (375092, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjk</b>', '2018-07-03 04:39:28');
INSERT INTO `logs` VALUES (405915, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 5_7</b>', '2018-07-08 04:15:16');
INSERT INTO `logs` VALUES (409394, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 04:39:04');
INSERT INTO `logs` VALUES (416749, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:20:38');
INSERT INTO `logs` VALUES (423218, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 28_7</b>', '2018-07-05 12:33:58');
INSERT INTO `logs` VALUES (427277, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:20:44');
INSERT INTO `logs` VALUES (432740, 'Người dùng <b>admin</b> đã đăng xuất vào lúc: <b>02-07-2018 06:44:45 pm</b>', '2018-07-02 06:45:45');
INSERT INTO `logs` VALUES (465210, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:08:10');
INSERT INTO `logs` VALUES (482819, 'Người dùng <b>nhanviena</b> đã đăng nhập vào lúc: <b>02-07-2018 06:44:49 pm</b>', '2018-07-02 06:46:49');
INSERT INTO `logs` VALUES (487702, 'Người dùng <b>admin</b> đã đăng xuất vào lúc: <b>04-07-2018 11:17:45 pm</b>', '2018-07-04 11:17:45');
INSERT INTO `logs` VALUES (488343, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:18:49');
INSERT INTO `logs` VALUES (490174, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:19:53');
INSERT INTO `logs` VALUES (494843, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 5_7</b>', '2018-07-08 04:36:23');
INSERT INTO `logs` VALUES (495240, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:18:30');
INSERT INTO `logs` VALUES (506287, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:19:32');
INSERT INTO `logs` VALUES (512299, 'Người dùng <b>admin</b> đã đăng nhập vào lúc: <b>02-07-2018 06:44:55 pm</b>', '2018-07-02 06:47:55');
INSERT INTO `logs` VALUES (513215, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:35:48');
INSERT INTO `logs` VALUES (518494, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Hoàng thị Mai Anh</b>', '2018-07-03 04:53:15');
INSERT INTO `logs` VALUES (522980, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qqqqqqqqqqqqqqqqqqqqqqqqq</b>', '2018-07-03 04:42:01');
INSERT INTO `logs` VALUES (524109, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Khánh</b>', '2018-07-04 11:17:14');
INSERT INTO `logs` VALUES (524994, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 5_7</b>', '2018-07-04 11:19:59');
INSERT INTO `logs` VALUES (537842, 'Người dùng <b>nhanviena</b> đã đăng xuất vào lúc: <b>04-07-2018 11:16:55 pm</b>', '2018-07-04 11:16:55');
INSERT INTO `logs` VALUES (538910, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 2_7 2</b>', '2018-07-05 12:34:35');
INSERT INTO `logs` VALUES (551087, 'Người dùng <b>admin</b> đã đăng nhập vào lúc: <b>05-07-2018 12:34:25 am</b>', '2018-07-05 12:34:25');
INSERT INTO `logs` VALUES (561921, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:36:19');
INSERT INTO `logs` VALUES (569581, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Luan 2</b>', '2018-07-03 04:14:06');
INSERT INTO `logs` VALUES (570945, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Luận</b>', '2018-07-03 03:38:55');
INSERT INTO `logs` VALUES (573212, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdf</b>', '2018-07-03 04:39:57');
INSERT INTO `logs` VALUES (582764, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwert</b>', '2018-07-03 04:40:29');
INSERT INTO `logs` VALUES (586884, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:19:59');
INSERT INTO `logs` VALUES (590394, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:07:30');
INSERT INTO `logs` VALUES (592133, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 5_7</b>', '2018-07-08 04:36:16');
INSERT INTO `logs` VALUES (594056, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasd</b>', '2018-07-03 04:40:15');
INSERT INTO `logs` VALUES (609314, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfg</b>', '2018-07-03 04:39:51');
INSERT INTO `logs` VALUES (610138, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:07:49');
INSERT INTO `logs` VALUES (610505, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Khánh</b>', '2018-07-05 12:29:54');
INSERT INTO `logs` VALUES (619050, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 5_7</b>', '2018-07-04 11:18:38');
INSERT INTO `logs` VALUES (620844, 'Người dùng <b>admin</b> đã đăng xuất vào lúc: <b>02-07-2018 06:49:28 pm</b>', '2018-07-02 06:49:28');
INSERT INTO `logs` VALUES (626496, 'Người dùng <b>admin</b> đã đăng nhập vào lúc: <b>04-07-2018 11:17:05 pm</b>', '2018-07-04 11:17:05');
INSERT INTO `logs` VALUES (627503, 'Người dùng <b>admin</b> đã đăng nhập vào lúc: <b>05-07-2018 12:32:46 am</b>', '2018-07-05 12:32:46');
INSERT INTO `logs` VALUES (636506, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:19:46');
INSERT INTO `logs` VALUES (638947, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 04:19:19');
INSERT INTO `logs` VALUES (641449, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyu</b>', '2018-07-03 04:16:47');
INSERT INTO `logs` VALUES (641658, 'Người dùng <b>nhanviena</b> đã đăng nhập vào lúc: <b>03-07-2018 03:37:52 pm</b>', '2018-07-03 03:37:52');
INSERT INTO `logs` VALUES (643616, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdf</b>', '2018-07-03 04:43:06');
INSERT INTO `logs` VALUES (654531, 'Người dùng <b>admin</b> đã đăng nhập vào lúc: <b>02-07-2018 06:49:38 pm</b>', '2018-07-02 06:49:38');
INSERT INTO `logs` VALUES (654712, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Hoàng thị Mai Anh</b>', '2018-07-03 03:42:42');
INSERT INTO `logs` VALUES (657532, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwerty</b>', '2018-07-03 04:16:38');
INSERT INTO `logs` VALUES (659455, 'Người dùng <b>admin</b> đã đăng nhập vào lúc: <b>04-07-2018 11:20:38 pm</b>', '2018-07-04 11:20:38');
INSERT INTO `logs` VALUES (659821, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopas</b>', '2018-07-03 04:40:07');
INSERT INTO `logs` VALUES (675904, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 5_7</b>', '2018-07-04 11:17:36');
INSERT INTO `logs` VALUES (679200, 'Người dùng <b>nhanviena</b> đã đăng xuất vào lúc: <b>04-07-2018 11:20:33 pm</b>', '2018-07-04 11:20:33');
INSERT INTO `logs` VALUES (682465, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 04:54:46');
INSERT INTO `logs` VALUES (692933, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 04:39:19');
INSERT INTO `logs` VALUES (698334, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:07:37');
INSERT INTO `logs` VALUES (711304, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjklzxcvbn</b>', '2018-07-03 04:54:01');
INSERT INTO `logs` VALUES (719727, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 28_7</b>', '2018-07-05 12:33:09');
INSERT INTO `logs` VALUES (725960, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Khánh</b>', '2018-07-03 03:39:53');
INSERT INTO `logs` VALUES (736329, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:19:38');
INSERT INTO `logs` VALUES (743256, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 2_7 2</b>', '2018-07-05 12:32:55');
INSERT INTO `logs` VALUES (745636, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 29_7</b>', '2018-07-05 12:34:54');
INSERT INTO `logs` VALUES (747437, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasd</b>', '2018-07-03 04:40:02');
INSERT INTO `logs` VALUES (747803, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiop</b>', '2018-07-03 04:16:56');
INSERT INTO `logs` VALUES (750153, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiop</b>', '2018-07-03 04:37:36');
INSERT INTO `logs` VALUES (751160, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:38:48');
INSERT INTO `logs` VALUES (765961, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Luan 1</b>', '2018-07-03 04:13:48');
INSERT INTO `logs` VALUES (766236, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 04:39:09');
INSERT INTO `logs` VALUES (767074, 'Người dùng <b>admin</b> đã đăng nhập vào lúc: <b>03-07-2018 03:44:00 pm</b>', '2018-07-03 03:44:01');
INSERT INTO `logs` VALUES (782805, 'Người dùng <b>admin</b> đã đăng xuất vào lúc: <b>03-07-2018 03:37:45 pm</b>', '2018-07-03 03:37:45');
INSERT INTO `logs` VALUES (817597, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 5_7</b>', '2018-07-05 12:31:11');
INSERT INTO `logs` VALUES (817994, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Khánh</b>', '2018-07-05 12:30:43');
INSERT INTO `logs` VALUES (820954, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwert</b>', '2018-07-03 04:16:32');
INSERT INTO `logs` VALUES (823243, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:07:44');
INSERT INTO `logs` VALUES (824222, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>fsdfsdafasdfasdfasdfasdfa</b>', '2018-07-03 03:42:10');
INSERT INTO `logs` VALUES (824799, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:19:00');
INSERT INTO `logs` VALUES (827118, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 2_7 2</b>', '2018-07-05 12:33:35');
INSERT INTO `logs` VALUES (834321, 'Người dùng <b>admin</b> đã đăng nhập vào lúc: <b>04-07-2018 11:15:54 pm</b>', '2018-07-04 11:15:54');
INSERT INTO `logs` VALUES (842392, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Khánh</b>', '2018-07-03 03:40:11');
INSERT INTO `logs` VALUES (846070, 'Người dùng <b>admin</b> đã xóa đặt phòng của khách: <b></b>', '2018-07-04 11:54:26');
INSERT INTO `logs` VALUES (857911, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiop</b>', '2018-07-03 04:19:09');
INSERT INTO `logs` VALUES (858216, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Luận</b>', '2018-07-03 04:13:14');
INSERT INTO `logs` VALUES (891480, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Khánh</b>', '2018-07-05 12:29:11');
INSERT INTO `logs` VALUES (910798, 'Người dùng <b>admin</b> đã xóa đặt phòng của khách: <b></b>', '2018-07-04 11:54:40');
INSERT INTO `logs` VALUES (910859, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Example 1_7 3</b>', '2018-07-04 11:17:23');
INSERT INTO `logs` VALUES (913788, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:18:22');
INSERT INTO `logs` VALUES (921174, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuio</b>', '2018-07-03 04:36:58');
INSERT INTO `logs` VALUES (933625, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 04:38:10');
INSERT INTO `logs` VALUES (939576, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdf</b>', '2018-07-03 04:42:54');
INSERT INTO `logs` VALUES (949791, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin người dùng: <b>nhanviena</b>', '2018-07-03 03:37:37');
INSERT INTO `logs` VALUES (953126, 'Người dùng <b>nhanviena</b> đã đăng nhập vào lúc: <b>05-07-2018 12:31:02 am</b>', '2018-07-05 12:31:02');
INSERT INTO `logs` VALUES (954926, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>qwertyuiopasdfghjkl</b>', '2018-07-03 05:20:27');
INSERT INTO `logs` VALUES (957477, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>chị Nam</b>', '2018-07-03 03:41:47');
INSERT INTO `logs` VALUES (958069, 'Người dùng <b>nhanviena</b> đã đăng nhập vào lúc: <b>04-07-2018 11:17:50 pm</b>', '2018-07-04 11:17:50');

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `PePermissionId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `PeModuleName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PeModuleAlias` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PeMode` int(1) NULL DEFAULT 0 COMMENT '0 - Staff, 1 - Manager',
  `PeCreatedDate` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PePermissionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('0A022B15D0664F9E010B7B59232C7E4D', 'Staff', 'staff', 0, '2018-05-14 17:12:42');
INSERT INTO `permission` VALUES ('825A58891AA23780412B6AC9ABC1718A', 'Manager', 'manager', 1, '2018-05-14 17:15:03');

-- ----------------------------
-- Table structure for rooms
-- ----------------------------
DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms`  (
  `RoRoomId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `RoName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `RoAlias` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RoPrice` int(11) NOT NULL,
  `RoMaxAldult` int(11) NULL DEFAULT 0,
  `RoMaxBaby` int(11) NULL DEFAULT 0,
  `RoCreatedDate` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`RoRoomId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of rooms
-- ----------------------------
INSERT INTO `rooms` VALUES ('14D4A6B9DBE8E194104624460995BE2F', '102 (Double)', '102-double', 450000, 2, 1, '2018-05-15 14:05:48');
INSERT INTO `rooms` VALUES ('47f8694d10bb7383fb288aa71ddb4542', 'd', 'd', 1500000, 0, 0, '2018-06-22 07:12:58');
INSERT INTO `rooms` VALUES ('b2bbadcd9dbd1a713dfe60f12a990cb1', '104 (Family)', '104-(Family)', 520000, 3, 2, '2018-05-23 07:41:06');
INSERT INTO `rooms` VALUES ('b3087d6c1be84da147e3692b5ce10a41', 'c', 'c', 1250000, 0, 0, '2018-06-22 07:12:50');
INSERT INTO `rooms` VALUES ('cebf3ee19ac57b67916a955589eb3219', 'a', 'a', 1000000, 0, 0, '2018-06-22 07:12:29');
INSERT INTO `rooms` VALUES ('D3FD84E3DF290744DAC5A35660CFECC3', '202 (Family)', '202-family', 650000, 4, 2, '2018-05-15 14:07:37');
INSERT INTO `rooms` VALUES ('e2c737da514561089dc532af9caf0014', '101 (Single)', '101-(Single)', 350000, 2, 1, '2018-05-22 14:46:32');
INSERT INTO `rooms` VALUES ('ec90cbb7dc5aba67d1df0a7c7b0c588f', 'b', 'b', 600000, 0, 0, '2018-06-22 07:12:40');
INSERT INTO `rooms` VALUES ('F5D5C1A388392710CDAFD107DA186790', '103 (Twin)', '103-twin', 550000, 2, 1, '2018-05-15 14:07:37');

-- ----------------------------
-- Table structure for services
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services`  (
  `SrServiceId` int(11) NOT NULL AUTO_INCREMENT,
  `SrName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SrAlias` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SrParentId` int(11) NULL DEFAULT NULL,
  `SrPrice` int(50) NULL DEFAULT NULL COMMENT '30000 / 1Kg',
  `SrCurrency` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SrCreatedDate` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`SrServiceId`) USING BTREE,
  INDEX `Parent`(`SrParentId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of services
-- ----------------------------
INSERT INTO `services` VALUES (1, 'Giặt là', 'giat-la', 0, NULL, NULL, '2018-05-01 16:55:53');
INSERT INTO `services` VALUES (3, 'Quần dài', 'quan-dai', 1, 50000, 'Cái', '2018-05-30 16:56:14');
INSERT INTO `services` VALUES (4, 'Áo dài', 'ao-dai', 1, 30000, 'Cái', '2018-05-30 16:56:26');
INSERT INTO `services` VALUES (5, 'Ăn uống', 'an-uong', 0, NULL, NULL, '2018-05-30 00:00:00');
INSERT INTO `services` VALUES (6, 'Pepsi', 'pepsi', 5, 11000, 'Chai', '2018-05-30 16:56:49');
INSERT INTO `services` VALUES (7, 'Coca', 'coca', 5, 10000, 'Chai', '2018-05-30 16:56:56');
INSERT INTO `services` VALUES (9, 'Đưa đón tại sân bay', 'dau-don-tai-san-bay', 0, 120000, 'Lần', '2018-05-30 16:57:24');
INSERT INTO `services` VALUES (10, 'Thuê xe', 'thue-xe-du-lich', 0, NULL, NULL, '2018-05-30 16:57:55');
INSERT INTO `services` VALUES (11, 'Xe 16 chỗ', 'xe-16-cho', 10, 350000, 'Chiếc', '2018-05-30 16:58:08');
INSERT INTO `services` VALUES (12, 'Xe 32 chỗ', 'xe-32-cho', 10, 500000, 'Chiếc', '2018-05-30 16:58:24');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `UsUserId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UsUserName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UsPassword` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UsFullName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UsEmail` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UsPhone` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UsAddress` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `UsAvatar` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `UsCardNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `UsUserType` int(1) NOT NULL COMMENT '0 - Admin, 1 - Staff',
  `UsCreatedDate` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UsUserId`) USING BTREE,
  INDEX `Permission`(`UsUserType`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('42f217e545b1a8351372b61609dd3866', 'nhanviena', '90fb71fe7b3a2831e33bd75bbf06a288', 'Nhân viên A B', 'a@gmail.com', '123325435', 'sadassad', NULL, '213213123', 1, '2018-05-29 10:18:34');
INSERT INTO `users` VALUES ('5a4dabc96d48d4b1744d1a6c57876531', 'admin', '5a4dabc96d48d4b1744d1a6c57876531', 'Admin', 'zolawebgroup@gmail.com', '2369584625', 'Thừa Thiên Huế', '', '3269812510', 0, '2018-05-14 17:24:00');

SET FOREIGN_KEY_CHECKS = 1;
