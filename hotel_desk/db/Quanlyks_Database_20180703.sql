-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 03, 2018 at 09:02 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quanlyks`
--
CREATE DATABASE IF NOT EXISTS `quanlyks` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `quanlyks`;

-- --------------------------------------------------------

--
-- Table structure for table `booking_room`
--

CREATE TABLE `booking_room` (
  `BrBookingId` int(11) NOT NULL,
  `BrCustomerName` varchar(50) NOT NULL,
  `BrCustomerEmail` varchar(50) NOT NULL,
  `BrCustomerPhone` varchar(50) NOT NULL,
  `BrRoomId` varchar(50) NOT NULL,
  `BrTransferRoomId` varchar(50) DEFAULT NULL,
  `BrTransferDate` text,
  `BrFromDate` text NOT NULL,
  `BrToDate` text NOT NULL,
  `BrNote` text NOT NULL,
  `BrStatus` int(1) NOT NULL COMMENT '0 - Ready, 1 - Stayed, 2 - Checked, 3 - Booked, 4 - Closed',
  `BrDeposit` int(11) DEFAULT NULL,
  `BrAldult` int(2) DEFAULT '0',
  `BrBaby` int(2) DEFAULT '0',
  `BrServices` text,
  `BrMode` varchar(10) DEFAULT '0' COMMENT '0 - None, 1 - Early, 2 - Before 16, 3 - After 16',
  `BrIsVAT` int(1) DEFAULT '0',
  `BrCreateUserId` varchar(50) NOT NULL,
  `BrCreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `booking_room`
--

REPLACE INTO `booking_room` (`BrBookingId`, `BrCustomerName`, `BrCustomerEmail`, `BrCustomerPhone`, `BrRoomId`, `BrTransferRoomId`, `BrTransferDate`, `BrFromDate`, `BrToDate`, `BrNote`, `BrStatus`, `BrDeposit`, `BrAldult`, `BrBaby`, `BrServices`, `BrMode`, `BrIsVAT`, `BrCreateUserId`, `BrCreatedDate`) VALUES
(6, 'a', '', '', '14D4A6B9DBE8E194104624460995BE2F', NULL, NULL, '22-06-2018', '26-06-2018', '', 1, 500000, 4, 0, '', '1:4||2', 1, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-22 07:05:40'),
(8, 'b', '', '', 'F5D5C1A388392710CDAFD107DA186790', '', NULL, '18-06-2018', '21-06-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-22 07:06:08'),
(9, 'Example 22', '', '', 'D3FD84E3DF290744DAC5A35660CFECC3', '', NULL, '22-06-2018', '23-06-2018', 'This is note.\nThis is another note.', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-22 07:39:40'),
(10, '1', '', '', 'e2c737da514561089dc532af9caf0014', '', NULL, '22-06-2018', '23-06-2018', '', 3, 0, 1, 0, '', '0', NULL, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-22 07:39:46'),
(11, 'Example 22 33 44', '', '', 'b2bbadcd9dbd1a713dfe60f12a990cb1', '', NULL, '20-06-2018', '23-06-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-22 07:39:51'),
(12, 'c', '', '', 'cebf3ee19ac57b67916a955589eb3219', '', NULL, '21-06-2018', '23-06-2018', '', 3, 0, 0, 0, '', '0', NULL, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-22 07:42:22'),
(13, 'Example', '', '', 'F5D5C1A388392710CDAFD107DA186790', '', NULL, '14-06-2018', '15-06-2018', '', 1, 200000, 0, 0, '11:1||12:1', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-28 10:48:46'),
(14, 'Example 3', '', '', 'b2bbadcd9dbd1a713dfe60f12a990cb1', '', NULL, '14-06-2018', '15-06-2018', '', 3, 110000, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-06-29 06:09:21'),
(15, 'Example 1', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '29-06-2018', '30-06-2018', '', 3, 0, 1, 0, '', '0', 0, '42f217e545b1a8351372b61609dd3866', '2018-06-29 09:39:36'),
(16, 'Example 28', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '28-06-2018', '29-06-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:13:21'),
(17, 'Example 27', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '27-06-2018', '28-06-2018', '', 3, 0, 0, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:13:36'),
(18, 'Example 1_7', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '01-07-2018', '02-07-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:16:20'),
(19, 'Example 30/6', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '30-06-2018', '01-07-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:16:33'),
(20, 'Example 31_7', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '31-07-2018', '01-08-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:42:27'),
(21, 'Example 30_7', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '30-07-2018', '31-07-2018', '', 3, 0, 0, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:42:41'),
(22, 'Example 29_7', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '29-07-2018', '30-07-2018', '', 3, 0, 0, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:43:07'),
(23, 'Example 28_7', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '28-07-2018', '29-07-2018', '', 3, 0, 0, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-01 07:43:20'),
(24, 'Example 1_2', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '01-02-2018', '04-02-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 02:23:21'),
(25, 'Example 28_2', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '28-02-2018', '01-03-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 02:24:45'),
(26, 'Example 1_2', '', '', 'D3FD84E3DF290744DAC5A35660CFECC3', '', NULL, '01-02-2018', '02-02-2018', '', 3, 0, 0, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 02:24:57'),
(27, 'Example 5_7', '', '', 'D3FD84E3DF290744DAC5A35660CFECC3', '', NULL, '05-07-2018', '09-07-2018', '', 1, 150000, 4, 0, '3:1||6:1', '1:1||2', 1, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 02:53:34'),
(28, 'Example 1_7 2', '', '', 'F5D5C1A388392710CDAFD107DA186790', '', NULL, '01-07-2018', '03-07-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 03:02:32'),
(29, 'Example 1_7 3', '', '', 'e2c737da514561089dc532af9caf0014', '', NULL, '01-07-2018', '04-07-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 03:02:45'),
(30, 'Example 2_7', '', '', 'b2bbadcd9dbd1a713dfe60f12a990cb1', '', NULL, '02-07-2018', '04-07-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 03:04:41'),
(31, 'Example 2_7 2', '', '', 'cebf3ee19ac57b67916a955589eb3219', '', NULL, '02-07-2018', '03-07-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 07:46:34'),
(32, 'Example 1_8 22 33 44 55 6', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '01-08-2018', '02-08-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 09:03:54'),
(33, 'Example 1_8 2', '', '', 'D3FD84E3DF290744DAC5A35660CFECC3', '', NULL, '01-08-2018', '03-08-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 09:05:29'),
(34, 'Example 2/8 33 44 55 66 7', '', '', 'F5D5C1A388392710CDAFD107DA186790', '', NULL, '02-08-2018', '03-08-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 09:40:59'),
(35, 'Example 2\\.6', '', '', 'ec90cbb7dc5aba67d1df0a7c7b0c588f', '', NULL, '21-06-2018', '22-06-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-02 10:05:45'),
(36, 'Anh Luận', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '04-07-2018', '05-07-2018', '', 3, 0, 1, 0, '9:1', '0', 0, '42f217e545b1a8351372b61609dd3866', '2018-07-03 08:41:28'),
(37, 'Anh Khánh', '', '', '14D4A6B9DBE8E194104624460995BE2F', '', NULL, '03-07-2018', '04-07-2018', '', 1, 0, 1, 0, '9:1', '0', 0, '42f217e545b1a8351372b61609dd3866', '2018-07-03 08:42:26'),
(38, 'chị Nam', '', '', 'D3FD84E3DF290744DAC5A35660CFECC3', '', NULL, '03-07-2018', '04-07-2018', '', 1, 0, 1, 0, '', '0', 0, '42f217e545b1a8351372b61609dd3866', '2018-07-03 08:44:12'),
(39, 'Hoàng thị Mai Anh', '', '', 'F5D5C1A388392710CDAFD107DA186790', '', NULL, '03-07-2018', '04-07-2018', '', 3, 0, 1, 0, '', '0', 0, '42f217e545b1a8351372b61609dd3866', '2018-07-03 08:44:43'),
(40, '11 22 33 44 55 66 77 88 9', '', '', 'cebf3ee19ac57b67916a955589eb3219', '', NULL, '03-07-2018', '04-07-2018', '', 3, 0, 1, 0, '', '0', 0, '5a4dabc96d48d4b1744d1a6c57876531', '2018-07-03 08:53:15');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `CfConfigId` varchar(50) NOT NULL,
  `CfName` varchar(50) DEFAULT NULL,
  `CfAlias` varchar(50) DEFAULT NULL,
  `CfValue` text,
  `CfCreatedDate` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `config`
--

REPLACE INTO `config` (`CfConfigId`, `CfName`, `CfAlias`, `CfValue`, `CfCreatedDate`) VALUES
('', 'Tên Khách sạn', 'hotel-name', 'Khách Sạn Demo', '2018-06-07 14:37:05'),
('1ac713007216c70d7b19433e3dc5731e', 'Giá phòng người lớn / 1 người', 'aldult', '100000', '2018-05-25 13:49:56'),
('49704672ddfc31c3afce000550f28b5e', 'Đơn vị tiền tệ', 'currency', 'VNĐ', '2018-05-25 13:43:07'),
('5394b98ed37cd79e174bc32a49a1a073', 'Tên Website', 'website-name', 'Quản lý khách sạn', '2018-05-25 13:49:56'),
('68be316a72eb5d14e3c40ff9f962c6a4', 'VAT', 'vat', '10', '2018-06-05 00:37:41'),
('6ebbc2a4a7b0863ebd4749cfe9aa95f9', 'Nhận phòng sớm', 'get-room-early', '1:100000||2:150000||4:250000||5:500000', '2018-05-25 13:49:56'),
('7abd623eff0c0e7d1945132d767fafe8', 'Email', 'email', 'zolawebgroup@gmail.com', '2018-06-07 14:37:24'),
('8e17ec05fc398bb49b3f48e88f7ef2e7', 'Số điện thoại', 'phone', '01674210615', '2018-06-07 14:37:59'),
('a198493f632696d0e4e5d37c9cf83485', 'Địa chỉ', 'address', '72, Tên Đường Đây, Thừa Thiên Huế, Việt Nam', '2018-06-07 14:38:28'),
('aa9c668a5430223533ce6bfc7c447b87', 'Website', 'website', 'zolawebgroup.com', '2018-06-07 14:38:49'),
('d4349bc5b935d0d27cfcf92949e3c9e3', 'LoginUserInfo', 'login-user-info', '5a4dabc96d48d4b1744d1a6c57876531', '2018-05-24 02:47:44');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `LgLogId` int(11) NOT NULL,
  `LgContent` text NOT NULL,
  `LgCreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Write log of every actitive on websites' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `logs`
--

REPLACE INTO `logs` (`LgLogId`, `LgContent`, `LgCreatedDate`) VALUES
(15497, 'Người dùng <b>nhanviena</b> đã đăng xuất vào lúc: <b>03-07-2018 03:43:41 pm</b>', '2018-07-03 03:43:41'),
(99182, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>chị Nam</b>', '2018-07-03 03:41:39'),
(120309, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>11 22 33 44 55 66 77 88 9</b>', '2018-07-03 03:50:42'),
(144604, 'Người dùng <b>admin</b> đã đăng nhập vào lúc: <b>03-07-2018 03:43:49 pm</b>', '2018-07-03 03:43:49'),
(206524, 'Người dùng <b>admin</b> đã đăng xuất vào lúc: <b>03-07-2018 03:43:55 pm</b>', '2018-07-03 03:43:55'),
(432740, 'Người dùng <b>admin</b> đã đăng xuất vào lúc: <b>02-07-2018 06:44:45 pm</b>', '2018-07-02 06:45:45'),
(482819, 'Người dùng <b>nhanviena</b> đã đăng nhập vào lúc: <b>02-07-2018 06:44:49 pm</b>', '2018-07-02 06:46:49'),
(512299, 'Người dùng <b>admin</b> đã đăng nhập vào lúc: <b>02-07-2018 06:44:55 pm</b>', '2018-07-02 06:47:55'),
(570945, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Luận</b>', '2018-07-03 03:38:55'),
(620844, 'Người dùng <b>admin</b> đã đăng xuất vào lúc: <b>02-07-2018 06:49:28 pm</b>', '2018-07-02 06:49:28'),
(641658, 'Người dùng <b>nhanviena</b> đã đăng nhập vào lúc: <b>03-07-2018 03:37:52 pm</b>', '2018-07-03 03:37:52'),
(654531, 'Người dùng <b>admin</b> đã đăng nhập vào lúc: <b>02-07-2018 06:49:38 pm</b>', '2018-07-02 06:49:38'),
(654712, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Hoàng thị Mai Anh</b>', '2018-07-03 03:42:42'),
(725960, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Khánh</b>', '2018-07-03 03:39:53'),
(767074, 'Người dùng <b>admin</b> đã đăng nhập vào lúc: <b>03-07-2018 03:44:00 pm</b>', '2018-07-03 03:44:01'),
(782805, 'Người dùng <b>admin</b> đã đăng xuất vào lúc: <b>03-07-2018 03:37:45 pm</b>', '2018-07-03 03:37:45'),
(824222, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>fsdfsdafasdfasdfasdfasdfa</b>', '2018-07-03 03:42:10'),
(842392, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>Anh Khánh</b>', '2018-07-03 03:40:11'),
(949791, 'Người dùng <b>admin</b> đã chỉnh sửa thông tin người dùng: <b>nhanviena</b>', '2018-07-03 03:37:37'),
(957477, 'Người dùng <b>nhanviena</b> đã chỉnh sửa thông tin đặt phòng của khách: <b>chị Nam</b>', '2018-07-03 03:41:47');

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `PePermissionId` varchar(50) NOT NULL,
  `PeModuleName` varchar(50) DEFAULT NULL,
  `PeModuleAlias` varchar(50) DEFAULT NULL,
  `PeMode` int(1) DEFAULT '0' COMMENT '0 - Staff, 1 - Manager',
  `PeCreatedDate` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `permission`
--

REPLACE INTO `permission` (`PePermissionId`, `PeModuleName`, `PeModuleAlias`, `PeMode`, `PeCreatedDate`) VALUES
('0A022B15D0664F9E010B7B59232C7E4D', 'Staff', 'staff', 0, '2018-05-14 17:12:42'),
('825A58891AA23780412B6AC9ABC1718A', 'Manager', 'manager', 1, '2018-05-14 17:15:03');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `RoRoomId` varchar(50) NOT NULL,
  `RoName` varchar(50) NOT NULL,
  `RoAlias` varchar(50) DEFAULT NULL,
  `RoPrice` int(11) NOT NULL,
  `RoMaxAldult` int(11) DEFAULT '0',
  `RoMaxBaby` int(11) DEFAULT '0',
  `RoCreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `rooms`
--

REPLACE INTO `rooms` (`RoRoomId`, `RoName`, `RoAlias`, `RoPrice`, `RoMaxAldult`, `RoMaxBaby`, `RoCreatedDate`) VALUES
('14D4A6B9DBE8E194104624460995BE2F', '102 (Double)', '102-double', 450000, 2, 1, '2018-05-15 14:05:48'),
('47f8694d10bb7383fb288aa71ddb4542', 'd', 'd', 1500000, 0, 0, '2018-06-22 07:12:58'),
('b2bbadcd9dbd1a713dfe60f12a990cb1', '104 (Family)', '104-(Family)', 520000, 3, 2, '2018-05-23 07:41:06'),
('b3087d6c1be84da147e3692b5ce10a41', 'c', 'c', 1250000, 0, 0, '2018-06-22 07:12:50'),
('cebf3ee19ac57b67916a955589eb3219', 'a', 'a', 1000000, 0, 0, '2018-06-22 07:12:29'),
('D3FD84E3DF290744DAC5A35660CFECC3', '202 (Family)', '202-family', 650000, 4, 2, '2018-05-15 14:07:37'),
('e2c737da514561089dc532af9caf0014', '101 (Single)', '101-(Single)', 350000, 2, 1, '2018-05-22 14:46:32'),
('ec90cbb7dc5aba67d1df0a7c7b0c588f', 'b', 'b', 600000, 0, 0, '2018-06-22 07:12:40'),
('F5D5C1A388392710CDAFD107DA186790', '103 (Twin)', '103-twin', 550000, 2, 1, '2018-05-15 14:07:37');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `SrServiceId` int(11) NOT NULL,
  `SrName` varchar(50) DEFAULT NULL,
  `SrAlias` varchar(50) DEFAULT NULL,
  `SrParentId` int(11) DEFAULT NULL,
  `SrPrice` int(50) DEFAULT NULL COMMENT '30000 / 1Kg',
  `SrCurrency` varchar(50) DEFAULT NULL,
  `SrCreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `services`
--

REPLACE INTO `services` (`SrServiceId`, `SrName`, `SrAlias`, `SrParentId`, `SrPrice`, `SrCurrency`, `SrCreatedDate`) VALUES
(1, 'Giặt là', 'giat-la', 0, NULL, NULL, '2018-05-01 16:55:53'),
(3, 'Quần dài', 'quan-dai', 1, 50000, 'Cái', '2018-05-30 16:56:14'),
(4, 'Áo dài', 'ao-dai', 1, 30000, 'Cái', '2018-05-30 16:56:26'),
(5, 'Ăn uống', 'an-uong', 0, NULL, NULL, '2018-05-30 00:00:00'),
(6, 'Pepsi', 'pepsi', 5, 11000, 'Chai', '2018-05-30 16:56:49'),
(7, 'Coca', 'coca', 5, 10000, 'Chai', '2018-05-30 16:56:56'),
(9, 'Đưa đón tại sân bay', 'dau-don-tai-san-bay', 0, 120000, 'Lần', '2018-05-30 16:57:24'),
(10, 'Thuê xe', 'thue-xe-du-lich', 0, NULL, NULL, '2018-05-30 16:57:55'),
(11, 'Xe 16 chỗ', 'xe-16-cho', 10, 350000, 'Chiếc', '2018-05-30 16:58:08'),
(12, 'Xe 32 chỗ', 'xe-32-cho', 10, 500000, 'Chiếc', '2018-05-30 16:58:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UsUserId` varchar(50) NOT NULL,
  `UsUserName` varchar(50) NOT NULL,
  `UsPassword` varchar(50) NOT NULL,
  `UsFullName` varchar(50) NOT NULL,
  `UsEmail` varchar(25) NOT NULL,
  `UsPhone` varchar(25) NOT NULL,
  `UsAddress` text,
  `UsAvatar` text,
  `UsCardNumber` varchar(20) DEFAULT NULL,
  `UsUserType` int(1) NOT NULL COMMENT '0 - Admin, 1 - Staff',
  `UsCreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

REPLACE INTO `users` (`UsUserId`, `UsUserName`, `UsPassword`, `UsFullName`, `UsEmail`, `UsPhone`, `UsAddress`, `UsAvatar`, `UsCardNumber`, `UsUserType`, `UsCreatedDate`) VALUES
('42f217e545b1a8351372b61609dd3866', 'nhanviena', '42f217e545b1a8351372b61609dd3866', 'Nhân viên A B', 'a@gmail.com', '123325435', 'sadassad', NULL, '213213123', 1, '2018-05-29 10:18:34'),
('5a4dabc96d48d4b1744d1a6c57876531', 'admin', '5a4dabc96d48d4b1744d1a6c57876531', 'Admin', 'zolawebgroup@gmail.com', '2369584625', 'Thừa Thiên Huế', '', '3269812510', 0, '2018-05-14 17:24:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking_room`
--
ALTER TABLE `booking_room`
  ADD PRIMARY KEY (`BrBookingId`) USING BTREE,
  ADD KEY `Rooms` (`BrRoomId`) USING BTREE,
  ADD KEY `CreateUser` (`BrCreateUserId`) USING BTREE,
  ADD KEY `Status` (`BrStatus`) USING BTREE,
  ADD KEY `TransferRooms` (`BrTransferRoomId`) USING BTREE;

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`CfConfigId`) USING BTREE;

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`LgLogId`) USING BTREE;

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`PePermissionId`) USING BTREE;

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`RoRoomId`) USING BTREE;

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`SrServiceId`) USING BTREE,
  ADD KEY `Parent` (`SrParentId`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UsUserId`) USING BTREE,
  ADD KEY `Permission` (`UsUserType`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking_room`
--
ALTER TABLE `booking_room`
  MODIFY `BrBookingId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `SrServiceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
