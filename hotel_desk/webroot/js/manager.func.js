/*
 * Javascript handles manager page
 */
/* global deleteUrl*/

/*
 * Handel sweet confirm dialog buttons click
 */
function SweetAlertHandleButtonClick(obj, isConfirm) {
    //If click submit button
    if (isConfirm) {
        //Get id
        var id = $(obj).attr('data-id');
        //Delete
        $.ajax({
            url: deleteUrl,
            type: 'post',
            data: {
                id: id
            },
            success: function (data) {

                if (data.success) {
                    //Show success modal
                    swal("Thành công!", "Xóa dữ liệu thành công", "success");
                    //Delete data on grid
                    var table = $('.js-basic-example').DataTable();
                    table.row('#' + id).remove().draw(false);
                } else {
                    //Show error modal
                    swal("Lỗi!", "Không thể xóa", "error");
                }
            },
            error: function (xhr, status, error) {
                //Show modal message
                swal("Lỗi!", "Có lỗi xảy ra. " + error, "error");
            }
        });
    }
}

/*
Recursion table data
 */
function RecursionTable(index, seperate, parten_id, arrData, html) {
    if (html === '') {
        html = '';
    }

    for (var i = 0; i < arrData.length; i++) {

        var tmp = arrData[i];

        if (tmp['SrParentId'] === parten_id) {
            //Increase index
            index++;

            //Import current html

            if(tmp['SrParentId'] !== 0){
                html += '<tr id="' + tmp['SrServiceId'] + '">' +
                    '                    <td>' + index + '</td>' +
                    '                    <td>' + seperate + ' ' + tmp['SrName'] + '</td>';
            }else{
                html += '<tr id="' + tmp['SrServiceId'] + '">' +
                    '                    <td>' + index + '</td>' +
                    '                    <td><b>' + seperate + ' ' + tmp['SrName'] + '</b></td>';
            }

            if((tmp['SrParentId'] === 0 && (tmp['SrPrice'] !== 0 && tmp['SrPrice'] !== null)) || (tmp['SrParentId'] !== 0 && tmp['SrPrice'] !== null)){
                html += '                    <td>' + FormatPrice(tmp['SrPrice']) + ' ' + currency + '</td>';
            }else{
                html += '                    <td>--</td>';
            }


            html += '                    <td>' + ConvertDefaultDatetime(tmp['SrCreatedDate']) + '</td>' +
                '                    <td class="al-c js-sweetalert">' +
                '                        <div class="btn-group btn-group-xs" role="group" aria-label="Extra-small button group">' +
                '                            <a href="modify/' + tmp['SrAlias'] + '" class="btn btn-primary waves-effect">' +
                '                                <i class="material-icons">edit</i>' +
                '                            </a>' +
                '                            <button data-type="confirm" class="btn bg-pink waves-effect btn-delete"' +
                '                                    data-id="' + tmp['SrServiceId'] + '"' +
                '                                    data-title="Xóa dữ liệu" data-text="Bạn có chắc chắn muốn xóa không?"' +
                '                                    data-mode="warning"' +
                '                                    data-confirm="Xóa" data-cancel="Hủy">' +
                '                                <i class="material-icons">delete</i>' +
                '                            </button>' +
                '                        </div>' +
                '                    </td>' +
                '                </tr>';

            //Get sub html
            var extras = RecursionTable(index, '    ' + seperate + '------', tmp['SrServiceId'], arrData, html);
            html = extras.html;
            index = extras.index;
        }
    }
    //return
    return {
        html: html,
        index: index
    };
}
/*
Convert default date time
 */
function ConvertDefaultDatetime(str){
    //Split
    var split = str.split(dateDevideCharacter);
    return split[2].substr(0,2) + dateDevideCharacter + split[1] + dateDevideCharacter + split[0];
}