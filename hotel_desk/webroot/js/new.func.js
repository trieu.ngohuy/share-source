$(document).ready(function () {
    //Click button cancel
    $("#btnCancel").click(function () {
        //Go to manager page
        location.href = indexUrl;
    });
    //Press enter
    $('input').keyup(function(e){
        if(e.keyCode == 13)
        {
            SaveData();
        }
    })
    /*
    Click login event
     */
    $('body').on('click','#btnSave', function(){
        SaveData();
    });
});
/*
Save data function
 */
function SaveData(){

    //Hide alert
    $('.alert').css('display', 'none');
    //Scroll top
    $(window).scrollTop(0);

    //Get input
    var input = GetInput();

    //Check valid
    var tmp = CheckValid();
    if(tmp.valid === false){
        $('.alert').css('display', 'block');
        $('.alert ul').html(tmp.message);
        return;
    }

    //Show preloader
    $('.page-loader-wrapper').fadeIn();


    //Ajax update data
    $.ajax({
        url: insertUrl,
        type: 'post',
        data: input,
        success: function (data) {
            //Parse return data to javascript object
            data = JSON.parse(data);

            //Hide preloader
            $('.page-loader-wrapper').fadeOut();

            if (data.success) {

                //Show error modal
                swal("Thông báo!", "Lưu thành công.", "success");
                //Go to main page
                window.location.href = indexUrl;
            } else {
                //Show error modal
                swal("Lỗi!", "Không thể lưu.", "error");
            }
        },
        error: function (xhr, status, error) {
            //Hide preloader
            $('.page-loader-wrapper').fadeOut();
            //Show modal message
            swal("Lỗi!", "Có lỗi xảy ra. " + error, "error");
        }
    });
}