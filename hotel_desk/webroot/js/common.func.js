/*
 * Date devide character
 * @type String
 */
var dateDevideCharacter = '-';
/*
 * Date format
 * @type String
 */
var dateFormat = 'DD-MM-YYYY';
/*
 * Document ready
 * @type type
 */
$(document).ready(function () {
    //Prevent type character into input
    $('.number-input').on('keypress', function(e){
        return e.metaKey || // cmd/ctrl
            e.which <= 0 || // arrow keys
            e.which == 8 || // delete key
            /[0-9]/.test(String.fromCharCode(e.which)); // numbers
    });
    //Side bar category click
    $('body').on('click', '#catWrap a', function (e) {
        //Get type
        var type = $(this).attr('data-type');
        //Get url
        var url = $(this).attr('href');
        //Except click booking link
        if(type === undefined && loginInfo['UsUserType'] === 1){
            swal("Lỗi!", "Nhân viên không được quyền truy cập trang này.", "error");
            e.preventDefault();
        }else{
            window.location.href = url;
        }
    });
});
/*
 * Format price
 * @param {type} value
 * @returns {undefined}
 */
function FormatPrice(value) {
    var formatedPrice = "";
    //Convert to string
    value = value.toString();
    //Loop each char of value
    var count = 1;
    for (var i = value.length - 1; i >= 0; i--) {
        //Add ',' between each 3 number
        if (count >= 3 && i > 0 && count % 3 === 0) {
            formatedPrice += value[i] + ",";
        } else {
            formatedPrice += value[i];
        }
        count++;
    }
    //Rever formated price then return
    return formatedPrice.split('').reverse().join('');
}
/*
 * Convert date to string format: MM/dd/YYYY
 * @param {type} date
 * @returns {String}
 */
function ConvertDateToString(date) {
    if (date instanceof Date === false) {
        date = NewDate(date);
    }
    var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    var month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
    return day + dateDevideCharacter + month + dateDevideCharacter + parseInt(date.getFullYear());
}
/*
 * Create new date
 * @param {type} str
 * @returns {Date}
 */
function NewDate(str) {
    //null then get current day
    if (str === undefined) {
        var date = new Date();
        str = date.getDate() + dateDevideCharacter + (date.getMonth() + 1) + dateDevideCharacter + date.getFullYear();
    }

    //If date object
    if(str instanceof Date){
        str = ConvertDateToString(str);
    }

    str = str.split(dateDevideCharacter);
    return new Date(parseInt(str[2]) + dateDevideCharacter + parseInt(str[1]) + dateDevideCharacter + parseInt(str[0]));
}
/*
 * Get days counts
 * @param {type} fromDate
 * @param {type} toDate
 * @returns {Number}
 */
function CountBetweenDays(fromDate, toDate) {
    //Check if not date
    if (fromDate instanceof Date === false) {
        fromDate = NewDate(fromDate);
    }
    if (toDate instanceof Date === false) {
        toDate = NewDate(toDate);
    }
    return Math.round((toDate - fromDate) / (1000 * 60 * 60 * 24));
}
/*
 * Combine sperate day, month and year into one date string
 * @param {type} day
 * @param {type} month
 * @param {type} year
 * @returns {dateDevideCharacter|String}
 */
function CombineIntoDateString(day, month, year) {
    day = day < 10 ? '0' + day : day;
    month = month < 10 ? '0' + month : month;
    return day + dateDevideCharacter + month + dateDevideCharacter + parseInt(year);
}
/*
 * Convert to english
 * @param {type} str
 * @returns {unresolved}
 */
function ConvertToEnglish(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}
/*
 * Convert to alias
 * @param {type} str
 * @returns {unresolved}
 */
function AutoGenerateAlias(str) {
    //convert to english
    str = ConvertToEnglish(str);
    //Replace '' => -
    str = str.replace(/ /g, "-");
    return str;
}
/*
 * Count number of days between from date and to date
 */
function GetListBetweenDays(startDate, endDate) {
    var dates = [],
            currentDate = startDate,
            addDays = function (days) {
                var date = new Date(this.valueOf());
                date.setDate(date.getDate() + days);
                return date;
            };
    while (currentDate <= endDate) {
        dates.push(currentDate);
        currentDate = addDays.call(currentDate, 1);
    }
    return dates;
}
;
/*
 * Compare 2 days
 */
function CompareTwoDate(day1, day2) {
    
    //tmp = ConvertDateToString(day1);
    if (day1 instanceof Date === false) {
        day1 = NewDate(day1);
    }

    //tmp = ConvertDateToString(day2);
    if (day2 instanceof Date === false) {
        day2 = NewDate(day2);
    }

    //day1 > day2
    if (day1.getTime() > day2.getTime()) {
        return 0;
    }
    //day1 = day2
    if (day1.getTime() === day2.getTime()) {
        return 1;
    }
    //day1 < day2
    if (day1.getTime() < day2.getTime()) {
        return 2;
    }
}
/*
Export pdf file
*/
function ExportPdfFile(exportUrl, input){
    //Show preloader
    $('.page-loader-wrapper').fadeIn();
    //Delete
    $.ajax({
        url: exportUrl,
        type: 'post',
        data: input,
        success: function (data) {
            //Hide preloader
            $('.page-loader-wrapper').fadeOut();

            if (data.success) {

                //Save excel file
                if(input['type'] === 'excel'){
                    window.location.href = viewPdfUrl + '\\' + data.file_name;
                }else{
                    //Open pdf file in new tab
                    window.open(viewPdfUrl + '\\' + data.file_name, '_blank');
                }

                //Update booking
                if(input['source'] === undefined){
                    //Hanle when success export pdf
                    HandleAfterExport(input);
                }else{
                    swal("Thông báo!", "Xuất báo cáo thành công.", "success");
                }


            } else {
                //Show error modal
                swal("Lỗi!", "Không thể xuất file", "error");
            }
        },
        error: function (xhr, status, error) {
            //Hide preloader
            $('.page-loader-wrapper').fadeOut();
            //Show modal message
            swal("Lỗi!", "Có lỗi xảy ra. " + error, "error");
        }
    });
}

/*
Price input
 */
String.prototype.reverse = function () {
    return this.split("").reverse().join("");
}
/*
On key up
 */
function PriceMaskInput(input) {
    var x = input.value;
    var recentCharacter = x.substr(x.length - 1);
    //If type character
    if(isNaN(parseInt(recentCharacter))){
        x = x.substr(0, x.length - 1);
    }
    x = x.replace(/,/g, ""); // Strip out all commas
    x = x.reverse();
    x = x.replace(/.../g, function (e) {
        return e + ",";
    }); // Insert new commas
    x = x.reverse();
    x = x.replace(/^,/, ""); // Remove leading comma
    input.value = x;
}
