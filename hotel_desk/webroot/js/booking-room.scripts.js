/*
 * Room status
 * @type type
 */
/* global js_booking, js_rooms, msgRoomStatusClosed, msgRoomStatusBooked, currency, js_services, js_get_room_early, vat, js_config_aldult, js_check_transfer_room_url, dateFormat, dateDevideCharacter, msgRoomStatusCheckOut, msgRoomStatusCheckIn */

var roomStatus = {
    Booked: 1,
    CheckIn: 2,
    CheckOut: 3,
    Closed: 4
};
/*
 * Room status mode
 * @type type
 */
var roomStatusMode = {
    None: 0,
    Early: 1,
    Before16: 2,
    After16: 3
};
/*
 * Is form change
 * @type Boolean
 */
var isFormChanged = false;
/*
 * Modal booking detail first init
 * @type Boolean
 */
var modalBookingInitStatus = true;
/*
 * Current date
 * @type Date
 */
var currentDate = NewDate();
/*
 * Current month days count
 * @type Number
 */
var currentMonthDaysCount = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0).getDate();
/*
 * Current working booking obj
 * @type type
 */
var bookingObj = {};
/*
 Booking block class
 */
var bookingBlockclass = 'booking-block';
/*
 Booking row
 */
var bookingRowClass = 'booking-row';
/*
 Booking hover class
 */
var bookingHoverclass = 'booking-hover';
var bookingHoverNewclass = 'booking-hover-new';
var bookingHoverDetailclass = 'booking-hover-detail';
/*
 Booking width
 */
var bookingWidthWeek = 100;
var bookingWidthMonth = 53;
/*
 Max characters
 */
var maxCharacterMonthBlock = 5;
var maxCharacterWeekBlock = 12;
var extrasCharacterWidth = 7.5;
/*
 Week days count
 */
var weekDaysCount = 7;

/*
 Display mode: 0 - month, 1 - week
 */
var displayMode = 0;
/*
 Toolbar obj
 */
var toolbarObj = {};
/*
 Week object
 */
var weekArr = [0, 1, 2, 3, 4, 5, 6];
/*
 * Check if ajax call
 * @type Boolean
 */
var isAjaxLoaded = false;
/*
 * Document ready
 * @type type
 */
jQuery(document).ready(function () {

    //Convert get early hour to arr
    js_get_room_early = GetEarlyHour(js_get_room_early);

    //Fill list room data
    FillRoomsData();

    //Fill booking data
    toolbarObj['Month'] = currentDate.getMonth() + 1;
    toolbarObj['Year'] = currentDate.getFullYear();
    var firstDate = '1' + dateDevideCharacter + (currentDate.getMonth() + 1) + dateDevideCharacter + currentDate.getFullYear();
    var lastDate = new Date(currentDate.getFullYear(), (currentDate.getMonth() + 1), 0);
    toolbarObj['Start'] = NewDate(firstDate);
    toolbarObj['End'] = NewDate(ConvertDateToString(lastDate));
    toolbarObj['DayCount'] = lastDate.getDate();
    toolbarObj['Mode'] = 0;
    FillBookingData(NewDate(firstDate), currentMonthDaysCount);

    //Update toolbar text
    UpdateToolBarText();

    //Fill combobox list rooms in modal booking detail
    for (i = 0; i < js_rooms.length; i++) {
        jQuery("#comBookingRoom").append('<option value="' + js_rooms[i]['RoRoomId'] + '">' + js_rooms[i]['RoName'] + '</option>');
    }

    //Button clicks
    JqueryEvents();

    //Modal handler
    ModalHandler();

    //Fill data to modal services
    AddDataToModalServices();

    //Fill data to combobox get room early
    FillComboboxGetRoomEarly();

    //Init date picker
    $(".datepicker").bootstrapMaterialDatePicker({
        format: dateFormat,
        clearButton: true,
        weekStart: 1,
        time: false,
        currentDate: currentDate,
        switchOnClick: true,
        clearButton: false,
        lang: 'vi',
        okText: 'Chọn',
        cancelText: 'Hủy'
                //minDate : currentDate
    });

    //Booking grid jump to current day
    JumpToActiveDate();
});

/*
 Fill room data
 */
function FillRoomsData() {
    var html = '';

    //Add title
    html += '<div class="' + bookingBlockclass + '">Phòng / Ngày</div>';
    //Loop data
    for (var i = 0; i < js_rooms.length; i++) {
        html += '<div class="' + bookingBlockclass + '">' + js_rooms[i]['RoName'] + '</div>';
    }

    //Append html
    $('#roomWrap').html(html);
}

/*
 * Fill booking data into main booking grid
 * @returns {undefined}
 */
function FillBookingData(start, daysCount) {

    //Remove all old content
    $('#bookingWrap').html('');
    //Get list rooms count
    var roomCount = js_rooms.length;

    //Insert range date title
    InsertBlockDateTitle(NewDate(start), daysCount);

    //Html content
    $html = "";

    //Loop list range date
    for (var i = 0; i < roomCount; i++) {
        //Add wrap div
        $html = '<div class="' + bookingRowClass + '">';
        var tmpRangeDate = displayMode !== 1 ? daysCount : weekDaysCount;

        var tmpNextDate = '';
        var loopDate = NewDate(start);
        for (var j = 1; j <= daysCount; j++) {
            //Block length
            var blockLength = 1;
            //Block name
            var customerName = '-';
            //Block class
            var blockClass = '';
            //Get current block date
            var blockDate = CombineIntoDateString(loopDate.getDate(), (loopDate.getMonth() + 1), toolbarObj['Year']);
            if (tmpNextDate === '') {
                tmpNextDate = blockDate;
            }
            //Find if current date has booking
            var check = CheckBooking(blockDate, js_rooms[i].RoRoomId);
            var hoverClass = "";
            var bookingInfoHtml = "";
            var blockIndex = '' + j;
            if (check !== false) {
                blockLength = check.currentMonthDaysCount;

                //Get block index
                blockIndex = GetBlockIndex(j, blockLength);

                //Increase day
                tmpNextDate = NewDate(tmpNextDate);
                tmpNextDate.setDate(tmpNextDate.getDate() + blockLength);
                tmpNextDate = ConvertDateToString(tmpNextDate);

                //Current booking object
                var tmpBookingObj = js_booking[check.index];
                //Set block name
                customerName = tmpBookingObj['BrCustomerName'];
                //Set room status class
                blockClass = GetRoomStatusClass(tmpBookingObj['BrStatus']);
                //Set hover class
                hoverClass = bookingHoverDetailclass;
                //Set booking info
                bookingInfoHtml = BookingHoverInfoHtml({
                    CustomerName: customerName,
                    Deposit: GetDepositStatus(tmpBookingObj['BrDeposit']),
                    BookingId: tmpBookingObj['BrBookingId'],
                    RoomStatus: GetRoomStatus(tmpBookingObj['BrStatus']),
                    StayDate: tmpBookingObj['BrFromDate'] + ' đến ' + tmpBookingObj['BrToDate'],
                    TotalPrice: CalculatingRoomFee(tmpBookingObj),
                    Note: tmpBookingObj['BrNote']
                });
            } else {
                //Set hover class
                hoverClass = bookingHoverNewclass;
                //Set booking info html
                if (CompareTwoDate(tmpNextDate, loopDate) === 0 || CompareTwoDate(tmpNextDate, loopDate) === 1) {
                    bookingInfoHtml = BookingNewHtml({
                        RoomId: js_rooms[i].RoRoomId,
                        BlockDate: tmpNextDate !== '' ? tmpNextDate : blockDate
                    });
                }

                //Update current block date
                tmpNextDate = NewDate(tmpNextDate);
                tmpNextDate.setDate(tmpNextDate.getDate() + 1);
                tmpNextDate = ConvertDateToString(tmpNextDate);
            }

            //Calculating extras width
            var extrasWidth = 0;
            var blockWidth = displayMode !== 1 ? bookingWidthMonth : bookingWidthWeek;
            var currentBlockWidth = blockWidth * blockLength;
            var blockMaxCharacter = displayMode !== 1 ? maxCharacterMonthBlock : maxCharacterWeekBlock;
            var currentMaxCharacter = blockMaxCharacter;
            if (blockLength > 1) {
                currentMaxCharacter = parseInt(currentBlockWidth / extrasCharacterWidth);
            }

            if (customerName.length > currentMaxCharacter) {
                extrasWidth = parseInt((customerName.length - currentMaxCharacter) * extrasCharacterWidth);
            }

            //Add date block
            $html += BookingSingleBlockHtml({
                BlockLength: blockLength,
                BlockClass: blockClass,
                CustomerName: customerName,
                HoverClass: hoverClass,
                Html: bookingInfoHtml,
                ActiveDayClass: GetActiveDayClass(loopDate),
                Width: currentBlockWidth,
                Index: blockIndex,
                ExtrasWidth: extrasWidth,
                LoopIndex: j
            });

            //Reset block length
            blockLength = 1;
            if (check !== false) {
                j += check.currentMonthDaysCount - 1;
                //Increase date
                loopDate.setDate(loopDate.getDate() + check.currentMonthDaysCount);
            } else {
                //Increase date
                loopDate.setDate(loopDate.getDate() + 1);
            }
            //Break
            if (j === tmpRangeDate) {
                break;
            }
        }

        $html += '</div>';

        //Set html content
        jQuery("#bookingWrap").append($html);
    }

    //Expand grid block width
    ExpandGridWidth();
}

/*
 * Init booking single block html
 * @param {type} objData
 * @returns {String}
 */
function BookingSingleBlockHtml(objData) {
    var style = "";
    /*
     * Set margin for hovert detail at last few blocks
     */
    if (objData.HoverClass === bookingHoverDetailclass) {
        if (toolbarObj['Mode'] === 0) {
            if (objData.LoopIndex >= toolbarObj['DayCount'] - 2) {
                style = 'style="margin-left: -202px"';
            } else {
                style = 'style="margin-left: ' + objData.Width + 'px"';
            }
        } else {
            if (objData.LoopIndex === toolbarObj['DayCount']) {
                style = 'style="margin-left: -78px"';
            } else {
                style = 'style="margin-left: ' + objData.Width + 'px"';
            }
        }
    }

    var tmp = '<div class="' + bookingBlockclass + ' ' + objData.ActiveDayClass + ' ' + objData.BlockClass + '" style="width: ' + objData.Width + 'px" data-width="' + objData.Width + '" data-extras="' + objData.ExtrasWidth + '" data-index="' + objData.Index + '">'
            + objData.CustomerName +
            '<div class="' + bookingHoverclass + ' ' + objData.HoverClass + '" ' + style + '> ' + objData.Html + ' </div>' +
            '</div>';
    return tmp;
}

/*
 * Init booking new html
 * @param {type} objData
 * @returns {String}
 */
function BookingNewHtml(objData) {
    var tmp = '<button type="button" class="btn btn-primary btn-booking-room-new" data-room-id="' + objData.RoomId + '" data-current-date="' + objData.BlockDate + '">+</button>';
    return tmp;
}

/*
 * Init booking hover info html
 * @param {type} objData
 * @returns {String}
 */
function BookingHoverInfoHtml(objData) {
    var tmp = '<button type="button" class="btn bg-blue waves-effect btn-room-hover-edit hover-button" ' +
            ' data-booking-id="' + objData.BookingId + '">Thay đổi</button>' +
            '<button type="button" class="btn bg-red waves-effect btn-room-hover-remove hover-button" ' +
            ' data-booking-id="' + objData.BookingId + '" data-type="remove">Xóa</button>' +
            '               <p class="card-text" id="bookingHoverTitle">' + objData.CustomerName + '</p>' +
            '<p class="card-text" id="bookingHoverDeposit">' + objData.Deposit + '</p>' +
            '<hr>' +
            '<div id="divInfoWrap">' +
            '    <p><b>Ngày ở:</b> ' + objData.StayDate + '</p>' +
            '    <p><b>Tình trạng:</b> ' + objData.RoomStatus + '</p>' +
            '    <p><b>Tổng tiền:</b> ' + FormatPrice(objData.TotalPrice) + ' ' + currency + '</p>' +
            '    <div class="hover-note"><p class="title"><b>Ghi chú:</b></p><p class="content"> ' + objData.Note.replace('\n', '<br>') + '</p></div>' +
            '</div>';
    return tmp;
}

/*
 * Draw main booking grid title
 * @param {type} currentMonthDaysCount
 * @param {type} dateObj
 * @returns {undefined}
 */
function InsertBlockDateTitle(start, dayCount) {
    //Add wrap div
    $html = '<div class="' + bookingRowClass + '" id="divBookingHeader"><div id="divBookingHeaderInner">';
    var loopDate = start;
    var blockWidth = displayMode !== 1 ? bookingWidthMonth : bookingWidthWeek;
    for (var j = 1; j <= dayCount; j++) {

        var id = '';
        if (CompareTwoDate(loopDate, currentDate) === 1) {
            var id = 'id="activePosition"';
        }

        //Add date block
        $html += '<div class="' + bookingBlockclass + ' ' + GetActiveDayClass(loopDate) + '" style="width:' + blockWidth + 'px"  data-width="' + blockWidth + '" data-extras="0" data-index="' + j + '"' +
                ' ' + id + '>' +
                loopDate.getDate() + '/' + (loopDate.getMonth() + 1) +
                '</div>';
        //Increase date
        loopDate.setDate(loopDate.getDate() + 1);
    }
    $html += '</div></div>';
    //Set html content
    jQuery("#bookingWrap").append($html);
}

/*
 * Get active day class
 * @param {type} index
 * @returns {String}
 */
function GetActiveDayClass(date) {

    var tmpActiveDayClass = "";
    if (CompareTwoDate(date, currentDate) === 1) {
        tmpActiveDayClass = "booking-current-day-background";
    }
    return tmpActiveDayClass;
}

/*
 * Check if current day has booking
 * @param {type} date
 * @param {type} roomId
 * @returns {CheckBooking.booking-room.scriptsAnonym$0|Boolean}
 */
function CheckBooking(date, roomId) {
    for (var i = 0; i < js_booking.length; i++) {

        var toDay = NewDate(js_booking[i]['BrToDate']);
        toDay.setDate(toDay.getDate() - 1);
        var fromDate = NewDate(js_booking[i]['BrFromDate']);
        date = NewDate(date);

        //if (js_booking[i]['BrRoomId'] === roomId && CompareTwoDate(date, js_booking[i]['BrFromDate']) === 1) {
        if (js_booking[i]['BrRoomId'] === roomId && (fromDate.getTime() <= date.getTime() && date.getTime() <= toDay.getTime())) {

            //Check from toolbar Start
            if (CompareTwoDate(fromDate, toolbarObj['Start']) === 2 &&
                    CompareTwoDate(toDay, toolbarObj['Start']) === 1) {
                fromDate = toolbarObj['Start'];
            }

            //Check from toolbar End
            if (CompareTwoDate(toDay, toolbarObj['End']) === 0) {
                var tmp = NewDate(toolbarObj['End']);
                tmp.setDate(tmp.getDate() + 1);
                toDay = tmp;
            } else {
                toDay = js_booking[i]['BrToDate'];
            }

            var daysCount = CountBetweenDays(fromDate, toDay);

            return {
                'currentMonthDaysCount': daysCount,
                'index': i
            };
        }
    }
    return false;
}

/*
 * Get room status class
 * @param {type} status
 * @returns {String}
 */
function GetRoomStatusClass(status) {
    //Màu cam: Khách booking
    if (status === roomStatus.Booked) {
        return 'room-status room-status-booked';
    }
    //Màu xanh da trời: Khách đang ở
    if (status === roomStatus.CheckIn) {
        return 'room-status room-status-checkin';
    }
    //Màu cam nhạt: Đã check out
    if (status === roomStatus.CheckOut) {
        return 'room-status room-status-checkout';
    }
    //Màu đỏ: Đã kết toán và kiểm tra xong
    if (status === roomStatus.Closed) {
        return 'room-status room-status-closed';
    }
}

/*
 * Get room status
 * @param {type} roomStatus
 * @returns {String}
 */
function GetRoomStatus(val) {
    //Convert to int
    val = parseInt(val);
    //1 - Booked, 2 - Check In, 3 - Check out, 4 - Closed
    if (val === roomStatus.Booked) {
        return msgRoomStatusBooked;
    }
    if (val === roomStatus.CheckIn) {
        return msgRoomStatusCheckIn;
    }
    if (val === roomStatus.CheckOut) {
        return msgRoomStatusCheckOut;
    }

    if (val === roomStatus.Closed) {
        return msgRoomStatusClosed;
    }
}

/*
 * Establish deposit status string
 * @param {type} mount
 * @returns {String}
 */
function GetDepositStatus(mount) {
    if (mount === 0) {
        return "Không đặt cọc";
    } else {
        return "Đã đặt cọc " + FormatPrice(mount) + " " + currency;
    }
}

/*
 * Modal handler
 * @returns {undefined}
 */
function ModalHandler() {
    //Modal services on hide
    jQuery('#servicesModal').on('hidden.bs.modal', function () {
        setTimeout(function () {
            jQuery("body").addClass('modal-open');
        }, 1);
    });
    //modal booking detail on hide
    jQuery('#newBookingModal').on('hidden.bs.modal', function () {
        setTimeout(function () {
            //Update variable modal booking detail first init
            modalBookingInitStatus = true;
            //Reset data on modal booking detail
            ResetModalDetail();
        }, 1);
    });
    //Modal services on show
    jQuery('#servicesModal').on('shown.bs.modal', function () {
        ConvertStringToCheckedCheckbox();
    });
    //Button save click
    jQuery('#btnModalBookingDetailSave').on('click', function () {
        //Show sweet confirm dialog
        showConfirmMessage($(this), {
            title: 'Lưu dữ liệu!',
            text: 'Bạn có muốn lưu dữ liệu không?',
            btnDeleteText: 'Lưu',
            btnCancelText: 'Hủy',
            mode: 'warning'
        });
    });
    //Button cancel click
    jQuery('#btnModalBookingDetailClose').on('click', function () {
        HanleModalOnClose();
    });
}
/*
 Handle modal on closing
 */
function HanleModalOnClose() {
    if (isFormChanged) {
        //Show sweet confirm dialog
        showConfirmMessage($(this), {
            title: 'Lưu dữ liệu!',
            text: 'Dữ liệu thay đổi. Bạn có muốn lưu dữ liệu không?',
            btnDeleteText: 'Lưu',
            btnCancelText: 'Hủy',
            mode: 'warning'
        });
    } else {
        //Close modal booking detail
        jQuery('#newBookingModal').modal('hide');
        jQuery('#newBookingModal').removeClass('show');
    }
}
/*
 Check if string containts special characters
 */
function isValid(str) {
    return !/[~`!@#$%\^&*()+=\-\[\]\\';,/{}|\\":<>\?]/g.test(str);
}
/*
 * Button clicks
 * @returns {undefined}
 */
function JqueryEvents() {
    //Catch press tab event
    $('body').on('keydown', '.tab-input', function (e) {
        if (e.keyCode === 9 && isAjaxLoaded === true) {
            
            var id = $(this).attr('id');
            var tabIndex = parseInt($(this).attr('tabindex'));
            if (tabIndex === 6) {
                tabIndex = 1;
            } else {
                tabIndex++;
            }
            //Blur
            $("#" + id).blur();
            $("#" + id).blur();
            //Set focus
            $(".tab-input-" + tabIndex).focus();
            $(".tab-input-" + tabIndex).focus();

            console.log('Press tab on ' + id);
        }
    });
    //Booking block hover
    $(document).on({
        mouseenter: function () {
            if ($(this).children('.booking-hover-detail').length > 0) {

                var position = $(this).position();
                var top = position.top;
                var childHeight = $(this).children('.booking-hover-detail').height();
                var windowHeight = $(window).height();

                if ((top + childHeight) > windowHeight) {
                    $(this).children('.booking-hover-detail').css('top', '-183px');
                } else {
                    $(this).children('.booking-hover-detail').css('top', '0px');
                }
                console.log(top + '/' + $(this).children('.booking-hover-detail').height() + '/' + windowHeight);
            }
        },
        mouseleave: function () {
            $(this).children('.booking-hover-detail').css('top', '0px');
        }
    }, ".booking-block");


    //Prevent input type special characters
    $(".special-input").keypress(function (event) {
        var character = String.fromCharCode(event.keyCode);
        return isValid(character);
    });

    //Switch display mode buttons click
    SwitchDisplayModeEvents();

    //Export room bill
    $('#btnModalBookingDetailExport').on('click', function () {
        //Get id
        var id = $(this).attr('data-id');
        //Create export url
        exportUrl = exportUrl;
        //Update ajax call status
        isAjaxLoaded = true;
        //Export
        ExportPdfFile(exportUrl, {
            id: id
        });
    });
    //Catch press esc button
    $(document).keyup(function (e) {
        if (e.keyCode == 27 && $('#newBookingModal').is(':visible')) {
            HanleModalOnClose();
        }
    });

    //Form has change
    jQuery("#newBookingModal").change(function () {
        isFormChanged = true;
    });
    //Combobox room change
    jQuery("#comBookingRoom").change(function () {
        //Check valid transfer room
        TransferRoom('room');
    });
    //From date and to date change date
    jQuery("#datBookingFromDate").on('change.dp', function () {

        //Reset date time picker limit
        ResetDateTimePickerLimit("#datBookingFromDate");

        //Check valid transfer room
        if (modalBookingInitStatus === false) {
            TransferRoom('fromDate');
        }

    });
    jQuery("#datBookingToDate").on('change.dp', function () {

        //Reset date time picker limit
        ResetDateTimePickerLimit("#datBookingToDate");

        //Check valid transfer room
        if (modalBookingInitStatus === false) {
            TransferRoom('toDate');
        }

    });

    //Increase people button click
    jQuery('.increase-people').on('click', function () {

        //Get type
        var type = jQuery(this).attr('data-type');
        //Get current value
        var val = parseInt(jQuery('#inpBooking' + type).val()) + 1;
        //Get room id
        var roomId = jQuery('#comBookingRoom option:selected').val();
        //Get current room obj        
        var roomObj = js_rooms.find(x => x.RoRoomId === roomId);

        //Update value
        jQuery('#inpBooking' + type).val(val);

        //Over value of max baby
        if (type === 'Baby' && val > roomObj['RoMaxBaby']) {
            jQuery('#inpBooking' + type).val(val - 1);
        }

        //Over value of max aldult
        if (type === 'Aldult' && val > roomObj['RoMaxAldult']) {
            CalculatingRoomFee();
        }
    });
    jQuery('.decrease-people').on('click', function () {

        //Get type
        var type = jQuery(this).attr('data-type');
        //Get current value
        var val = parseInt(jQuery('#inpBooking' + type).val()) - 1;

        //Update value
        jQuery('#inpBooking' + type).val(val);

        //Equal 0 then reset
        if (val <= 0) {
            jQuery('#inpBooking' + type).val(0);
        }

        //Over value of max aldult
        if (type === 'Aldult') {
            CalculatingRoomFee();
        }
    });
    //Descrease people button click

    //Plus people checkbox changed event
    jQuery('#exTrasCheckboxAldult').on('change', function () { // on change of state
        //Set vat value
        if (this.checked) // if changed state is "CHECKED"
        {
            //Get booking id
            var bookingId = parseInt(jQuery("#inpBookingId").val());
            //Search booking
            var search = js_booking.find(x => x.BrBookingId === bookingId);
            //Update vat bool value
            jQuery("#inpBookingAldult").val(search['BrAldult']);
            jQuery("#inpBookingExtrasAldult").val(search['BrExtrasAldult']);
            //Update display text
            jQuery("#exTrasCheckboxAldultLabel").val('+' + search['BrExtrasAldult'] + ' người');
            //Update display text in calculating money
            jQuery("#peopleSubTitle").html('Người lớn: +' + search['BrExtrasAldult'] + ' người');
            //Display extras aldult input
            jQuery("#inpBookingExtrasAldult").css('display', 'block');
        } else {
            //Hide extras aldult input
            jQuery("#inpBookingExtrasAldult").css('display', 'none');
            //Update extras value
            jQuery("#inpBookingExtrasAldult").val(0);
            //Update display text
            jQuery("#exTrasCheckboxAldultLabel").val('Thêm người');
            //Remove display text in calculating money
            jQuery("#peopleSubTitle").html('');
        }
        //Calculating total fee
        CalculatingRoomFee();
    });
    //People aldult input change
    jQuery('#inpBookingExtrasAldult').on('input', function (e) {
        var isPlusPeople = jQuery("#exTrasCheckboxAldult").is(':checked');
        //Check if checkbox add people checked
        if (isPlusPeople) {
            //Get current extras value
            var tmp = parseInt(jQuery("#inpBookingExtrasAldult").val());
            //Update extras aldult
            jQuery("#inpBookingExtrasAldult").val(tmp);
            //Update display text in calculating money
            jQuery("#peopleSubTitle").html('Người lớn: +' + tmp + ' người');
            //Calculating total fee
            CalculatingRoomFee();
        }
    });
    //VAT checkbox checked
    jQuery('.chk-vat').on('change', function () { // on change of state
        //Set vat value
        if (this.checked) // if changed state is "CHECKED"
        {
            //Update vat bool value
            jQuery(".chk-vat").val(1);
        } else {
            //Update vat bool value
            jQuery(".chk-vat").val(0);
        }
        //Calculating total fee
        CalculatingRoomFee();
    });
    //Combobox early hour change
    jQuery("#comGetRoomEarly").change(function () {
        //Reset booking room mode
        ConvertModeCheckedCheckboxToString();
    });
    //People input change
    jQuery('.input-booking-people').keyup(function (e) {

        var val = $(this).val();

        //Remove all first 0 number
        var index = -1;
        for (var i = 0; i < val.length; i++) {
            if (val[i] !== '0') {
                break;
            } else {
                index = i;
            }
        }
        if (index !== -1) {
            val = val.substr(index + 1, val.length - 1);
            $(this).val(val);
        }

        //Empty then reset to 0
        if (val === '') {
            $(this).val(0);
        }
        //Recalculating price
        CalculatingRoomFee();
    });
    //Deposit input change
    jQuery('.inpPrice').keyup(function (e) {

        //Format price
        PriceMaskInput(this);

        //Remove all first 0 number
        var val = $(this).val();
        var index = -1;
        for (var i = 0; i < val.length; i++) {
            if (val[i] !== '0') {
                break;
            } else {
                index = i;
            }
        }
        if (index !== -1) {
            val = val.substr(index + 1, val.length - 1);
            $(this).val(val);
        }
        //Empty then reset to 0
        if (val === '') {
            $(this).val(0);
        }
        //Recalculating price
        CalculatingRoomFee();
    });

    //Check get room early / Check out late change
    jQuery('.chk-booking-mode').on('change', function () {
        var val = this.value;
        var isChecked = jQuery(this).is(":checked");

        //Checked none --> reset all
        if (val === '0') {
            jQuery(".chk-booking-mode-early").prop("checked", false);
            jQuery(".chk-booking-mode-before16").prop("checked", false);
            jQuery(".chk-booking-mode-after16").prop("checked", false);

            //Reset if uncheck none
            if (!isChecked) {
                jQuery(".chk-booking-mode-none").prop("checked", true);
            }
        } else {
            //Check any -> uncheck none
            jQuery(".chk-booking-mode-none").prop("checked", false);
        }
        //Check before 16 -> uncheck after 16
        if (val === '2' && isChecked === true) {
            jQuery(".chk-booking-mode-after16").prop("checked", false);
        }
        //Check after 16 -> uncheck before 16
        if (val === '3' && isChecked === true) {
            jQuery(".chk-booking-mode-before16").prop("checked", false);
        }

        //Uncheck all then checked none
        if (jQuery('.chk-booking-mode:checked').length <= 0) {
            jQuery(".chk-booking-mode-none").prop("checked", true);
        }

        //Reset booking room mode
        ConvertModeCheckedCheckboxToString();

    });

    //Delete booking button
    $('body').on('click', '.btn-room-hover-remove', function () {
        //Show sweet confirm dialog
        showConfirmMessage($(this), {
            title: 'Xóa dữ liệu!',
            text: 'Bạn có chắc chắn muốn xóa dữ liệu không?',
            btnDeleteText: 'Xóa',
            btnCancelText: 'Hủy',
            mode: 'warning'
        });
    });
    //Edit booking room button
    $('body').on('click', '.btn-room-hover-edit', function () {
        //jQuery(".btn-room-hover-edit").on('click', function () {
        //Update variable modal booking detail first init
        modalBookingInitStatus = true;
        //Get attr booking room id
        var bookingId = jQuery(this).attr('data-booking-id');
        //Get booking room info base on id
        bookingObj = js_booking.find(x => x.BrBookingId === parseInt(bookingId));

        //Check if staff and current date is outside booking range

        //Get room info
        var room = js_rooms.find(x => x.RoRoomId === bookingObj['BrRoomId']);
        /*
         * Fill booking room data
         */
        //Id
        jQuery('#inpBookingId').val(bookingObj['BrBookingId']);
        //Add attr booking id to button export room bill
        jQuery('#btnModalBookingDetailExport').attr('data-id', bookingObj['BrBookingId']);

        //Room id
        //jQuery('#inpRoomId').val(bookingObj['BrRoomId']);
        //is VAT
        jQuery('.chk-vat').val(bookingObj['BrIsVAT']);
        if (bookingObj['BrIsVAT'] === 1) {
            jQuery('.chk-vat').prop('checked', true);
        }
        //Transfer room id
        jQuery('#inpTransferRoomId').val(bookingObj['BrTransferRoomId']);
        //Transfer date
        jQuery('#inpTransferDate').val(bookingObj['BrTransferDate']);
        //Room status
        //jQuery("#comBookingRoomStatus option[value='" + bookingObj['BrStatus'] + "']").prop("selected", "selected");
        $('#comBookingRoomStatus').selectpicker('val', bookingObj['BrStatus']);
        //Combobox room
        //jQuery("#comBookingRoom option[value='" + bookingObj['BrRoomId'] + "']").prop("selected", "selected");
        $('#comBookingRoom').selectpicker('val', bookingObj['BrRoomId']);
        //Transfer room        
        DisplayTranferRoomInfo(bookingObj['BrTransferRoomId'], bookingObj['BrTransferDate']);
        //Customer name
        jQuery('#inpBookingCustomerName').val(bookingObj['BrCustomerName']);
        //Customer email
        jQuery('#inpBookingCustomerEmail').val(bookingObj['BrCustomerEmail']);
        //Customer name
        jQuery('#inpBookingCustomerPhone').val(bookingObj['BrCustomerPhone']);

        //Date time picker init
        var fromDate = NewDate(bookingObj['BrFromDate']);
        var toDate = NewDate(bookingObj['BrToDate']);
        DateTimePickerInit(fromDate, toDate);

        //Aldult
        jQuery('#inpBookingAldult').val(bookingObj['BrAldult']);
        //Baby
        jQuery('#inpBookingBaby').val(bookingObj['BrBaby']);
        //Deposit
        jQuery('#inpBookingDeposit').val(bookingObj['BrDeposit']);
        jQuery('#inpBookingDeposit').keyup();
        //Mode
        //jQuery("input[name=BrMode]").val(bookingObj['BrMode']);
        ConvertStringToModeCheckboxes(bookingObj['BrMode']);
        ConvertModeCheckedCheckboxToString();
        //Note
        jQuery('#inpBookingNote').val(bookingObj['BrNote']);
        //Services
        jQuery('#inpServices').val(bookingObj['BrServices']);
        DisplaySelectedServices();
        //Total money
        CalculatingRoomFee();
        //Show modal0
        jQuery('#newBookingModal').modal('toggle');
        //Update variable modal booking detail first init
        modalBookingInitStatus = false;
        //Reset variable data change
        isFormChanged = false;

        //Disable form
        if (loginInfo['UsUserType'] === 1 && CompareTwoDate(fromDate, currentDate) !== 1 && bookingObj['BrStatus'] !== roomStatus.CheckIn) {
            DisableForm(true);
        } else {
            DisableForm(false);
        }
    });

    //Open new modal
    $('body').on('click', '.btn-booking-room-new', function () {

        //Init date time picker
        var blockDate = jQuery(this).attr('data-current-date');
        var fromDate = NewDate(blockDate);
        var toDate = NewDate(blockDate);
        toDate.setDate(toDate.getDate() + 1);

        //Check if staff and block date is passed then return
        if (loginInfo['UsUserType'] === 1 && CompareTwoDate(NewDate(blockDate), currentDate) === 2) {
            //Show error modal
            swal("Lỗi!", "Nhân viên không được thêm đặt phòng từ ngày " + ConvertDateToString(currentDate) + " trở về sau.", "error");
            return;
        }

        //Update variable modal booking detail first init
        modalBookingInitStatus = true;
        //Open modal
        jQuery('#newBookingModal').modal('toggle');
        jQuery('#newBookingModal').removeClass('in');
        jQuery('#newBookingModal').addClass('show');

        //Get room id
        var roomId = jQuery(this).attr('data-room-id');

        //Set combobox booking room status
        $('#comBookingRoomStatus').selectpicker('val', 1);
        //Set combobox room
        $('#comBookingRoom').selectpicker('val', roomId);

        //Init date time picker
        DateTimePickerInit(fromDate, toDate);

        //Mode
        jQuery('#inpBookingModeString').val('0');
        //VAT
        jQuery('.chk-vat').val('0');

        //Update working bookinh obj
        bookingObj = {
            BrRoomId: roomId,
            BrFromDate: ConvertDateToString(fromDate),
            BrToDate: ConvertDateToString(toDate)
        };

        //Calculating price
        CalculatingRoomFee();

        //Disable export button
        jQuery("#btnModalBookingDetailExport").prop("disabled", true);
        //Update variable modal booking detail first init
        modalBookingInitStatus = false;
        //Reset variable data change
        isFormChanged = false;

    });
}

/*
 * Convert string to mode checkboxes
 * @param {type} str
 * @returns {undefined}
 */
function ConvertStringToModeCheckboxes(str) {

    //Uncheck all checkboxes
    jQuery("input[name=ChkMode]").prop("checked", false);
    //Remove selected in combobox early hours
    jQuery('#comGetRoomEarly option:selected').removeAttr('selected');

    //Convert to array
    var arr = str.split('||');
    //Loop
    for (var i = 0; i < arr.length; i++) {
        //None
        if (arr[i] === '0') {
            jQuery(".chk-booking-mode-none").prop("checked", true);
        }
        //Before 16
        else if (arr[i] === '2') {
            jQuery(".chk-booking-mode-before16").prop("checked", true);
        }
        //After 16
        else if (arr[i] === '3') {
            jQuery(".chk-booking-mode-after16").prop("checked", true);
        } else {
            //Early
            jQuery(".chk-booking-mode-early").prop("checked", true);
            //Set combobox early hout selected
            var tmp = arr[i].split(':');
            jQuery("#comGetRoomEarly option[value='" + tmp[1] + "']").prop("selected", "selected");
        }
    }

}

/*
 * Convert mode checked checkboxes to string
 * @returns {undefined}
 */
function ConvertModeCheckedCheckboxToString() {
    var arrSelected = [];

    //Get list checked checkbox
    jQuery('.chk-booking-mode:checked').each(function () {
        //Get id
        var val = jQuery(this).val();
        //Get number
        var earlyHour = jQuery('#comGetRoomEarly option:selected').val();
        //Add to selected array
        arrSelected.push([parseInt(val), parseInt(earlyHour)]);
    });

    //Convert array to string
    var strSelected = '';
    for (var i = 0; i < arrSelected.length; i++) {
        if (arrSelected[i][0] !== 1) {
            strSelected += arrSelected[i][0] + '||';
        } else {
            strSelected += arrSelected[i][0] + ':' + arrSelected[i][1] + '||';
        }

    }
    //Remove last 2 characters ||
    strSelected = strSelected.substring(0, strSelected.length - 2);

    //Set to textbox input
    jQuery("#inpBookingModeString").val(strSelected);

    //Calculating total room fee
    CalculatingRoomFee();
}

/*
 * Reset modal detail info
 * @returns {undefined}
 */
function ResetModalDetail() {
    //Reset form disable
    DisableForm(false);
    //Current woking modal
    bookingObj = {};
    //Id
    jQuery('#inpBookingId').val('');
    //Room id
    //jQuery('#inpRoomId').val('');
    //is VAT
    jQuery('#chkIsVAT').val(0);
    jQuery('.chk-vat').prop('checked', false);
    //Extras aldult
    jQuery('#inpBookingExtrasAldult').val(0);
    jQuery('#exTrasCheckboxAldult').prop('checked', false);
    jQuery("#exTrasCheckboxAldultLabel").val('Thêm người');
    jQuery("#peopleSubTitle").html('');
    //Transfer room id
    jQuery('#inpTransferRoomId').val('');
    //Transfer date
    jQuery('#inpTransferDate').val('');
    //Tranfer display
    jQuery('#extrasCheckboxRoom').css('display', 'none');
    //Reset combobox room status
    //jQuery('#comBookingRoomStatus option:selected').removeAttr('selected');
    //Reset combobox rom
    //jQuery('#comBookingRoom option:selected').removeAttr('selected');
    //Hide alert message
    jQuery(".transfer-room-message").css('display', 'none');
    //Customer name
    jQuery('#inpBookingCustomerName').val('');
    //Customer email
    jQuery('#inpBookingCustomerEmail').val('');
    //Customer phone
    jQuery('#inpBookingCustomerPhone').val('');
    //From date
    jQuery("#datBookingFromDate").prop("disabled", false);

    //Remove hidden input BrFromDate
    jQuery('#extrasFromDate').remove();
    //Aldult
    jQuery('#inpBookingAldult').val(0);
    //Baby
    jQuery('#inpBookingBaby').val(0);
    //Deposit
    jQuery('#inpBookingDeposit').val(0);
    //Mode
    jQuery("input[name=BrMode]").val('0');
    ConvertStringToModeCheckboxes('0');

    //Services
    jQuery('#inpServices').val('');
    jQuery("#divServicesDetailWrap").html('');
    //Note
    jQuery('#inpBookingNote').val('');
    //Total money
    jQuery("#totalMoneyRoom").html(FormatPrice('0'));
    jQuery("#totalMoneServices").html(FormatPrice('0'));
    jQuery("#totalMoneyDeposit").html(FormatPrice('0'));
    jQuery("#totalMoneyMode").html(FormatPrice('0'));
    jQuery("#totalMoneyAll").html(FormatPrice('0'));
    //Disable export button
    jQuery("#btnModalBookingDetailExport").prop("disabled", false);
    jQuery("#btnModalBookingDetailSave").prop("disabled", false);

}

/*
 * Calculating room total fee
 * @returns {.js_rooms@call;find.RoPrice|Number|tmpRoom.RoPrice}
 */
function CalculatingRoomFee(tmpBookingObj) {
    var totalFee = 0;
    /*
     * Room fee
     */
    //Get room id and room info
    var roomId = tmpBookingObj === undefined ? jQuery("#comBookingRoom option:selected").val() : tmpBookingObj['BrRoomId'];
    var tmpRoom = js_rooms.find(x => x.RoRoomId === roomId);
    var roomFee = tmpRoom['RoPrice'];
    //Get booking room id
    var bookingId = tmpBookingObj === undefined ? jQuery("#inpBookingId").val() : tmpBookingObj['BrBookingId'];
    if (bookingId !== "") {
        //Search booking info
        var tmpBooking = js_booking.find(x => x.BrBookingId === parseInt(bookingId));
        //Count stay days        
        var fromDate = NewDate(tmpBookingObj === undefined ? jQuery('#datBookingFromDate').val() : tmpBooking['BrFromDate']);
        var toDate = NewDate(tmpBookingObj === undefined ? jQuery('#datBookingToDate').val() : tmpBooking['BrToDate']);
        if (tmpBookingObj === undefined ? jQuery('#inpTransferDate').val() !== '' && jQuery('#inpTransferDate').val() !== null :
                tmpBooking['BrTransferDate'] !== '' && tmpBooking['BrTransferDate'] !== null) {
            fromDate = NewDate(tmpBookingObj === undefined ? jQuery('#inpTransferDate').val() : tmpBooking['BrTransferDate']);
        }
        var daysCount = CountBetweenDays(fromDate, toDate);
        roomFee = roomFee * daysCount;
    }
    //Update total fee   
    totalFee = roomFee;
    //Display room fee
    jQuery("#totalMoneyRoom").html(FormatPrice(roomFee));
    //jQuery("#totalMoneyRoomEstras").html(FormatPrice(roomFee));

    //Transfer room fee

    var transferRoomId = tmpBookingObj === undefined ? jQuery("#inpTransferRoomId").val() : tmpBookingObj['BrTransferRoomId'];
    var transferDate = tmpBookingObj === undefined ? jQuery("#inpTransferDate").val() : tmpBookingObj['BrTransferDate'];
    var transferRoomFee = DisplayTranferRoomInfo(transferRoomId, transferDate);
    //Count days and calculating fee
    if (transferDate !== '' && transferDate !== null) {
        var fromDate = NewDate(tmpBookingObj === undefined ? jQuery('#datBookingFromDate').val() : tmpBooking['BrFromDate']);
        var toDate = NewDate(transferDate);
        var daysCount = CountBetweenDays(fromDate, toDate);
        transferRoomFee = transferRoomFee * daysCount;
    }
    totalFee += transferRoomFee;
    jQuery("#totalMoneyRoomEstras").html(FormatPrice(transferRoomFee));
    //jQuery("#totalMoneyRoom").html(FormatPrice(transferRoomFee));

    //Service fee
    var arrServices = tmpBookingObj === undefined ? jQuery("#inpServices").val() : tmpBookingObj['BrServices'];
    var servicesFee = 0;
    arrServices = arrServices !== '' && arrServices !== null ? arrServices.split('||') : [];
    for (var i = 0; i < arrServices.length; i++) {
        var tmpObj = arrServices[i].split(':');
        //Search service price
        var tmpSearch = js_services.find(x => x.SrServiceId === parseInt(tmpObj[0]));
        servicesFee += parseInt(tmpObj[1]) * tmpSearch['SrPrice'];
    }
    //Display
    jQuery("#totalMoneServices").html(FormatPrice(servicesFee));
    //Update total fee

    totalFee += servicesFee;

    //Mode fee
    var mode = tmpBookingObj === undefined ? jQuery("#inpBookingModeString").val() : tmpBookingObj['BrMode'];
    var modeFee = 0;
    var arrMode = mode !== '' && mode !== null ? mode.split('||') : [];
    var subTitle = "";
    for (var i = 0; i < arrMode.length; i++) {
        if (arrMode[i] === '0') {
            //Reset sub title
            subTitle = '';
        } else if (arrMode[i] === '2') {
            modeFee += tmpRoom['RoPrice'] / 2;
            //Add to sub title
            subTitle += 'Trả phòng muộn từ 12h10 - 16h: ' + FormatPrice(tmpRoom['RoPrice'] / 2) + currency + ', ';
        } else if (arrMode[i] === '3') {
            modeFee += tmpRoom['RoPrice'];
            //Add to sub title
            subTitle += 'Trả phòng muộn sau 16h: ' + FormatPrice(tmpRoom['RoPrice']) + currency + ', ';
        } else {
            var tmpGetRoomEarly = arrMode[i].split(':');
            //Get early hour price
            var tmpSearch = js_get_room_early.find(x => x.hour === tmpGetRoomEarly[1]);
            modeFee += parseInt(tmpSearch.price);
            //Add to sub title
            subTitle += 'Nhân phòng sớm ' + tmpGetRoomEarly[1] + 'h: ' + FormatPrice(tmpSearch.price) + currency + ', ';
        }

    }
    totalFee += modeFee;
    jQuery("#totalMoneyMode").html(FormatPrice(modeFee));
    //Establish mode string under mode fee
    jQuery('#earlyLateSubTitle').html(subTitle.substr(0, subTitle.length - 2));
    //Deposit
    var tmpDeposit = (tmpBookingObj === undefined ? jQuery("#inpBookingDeposit").val() : tmpBookingObj['BrDeposit']) + '';
    tmpDeposit = tmpDeposit.replace(',', '');
    tmpDeposit = parseInt(tmpDeposit);
    totalFee -= tmpDeposit;
    jQuery("#totalMoneyDeposit").html(FormatPrice(tmpDeposit));

    //Add more people
    var extrasAldult = tmpBookingObj === undefined ? parseInt(jQuery('#inpBookingAldult').val()) : tmpBookingObj['BrAldult'];
    extrasAldult = extrasAldult > tmpRoom['RoMaxAldult'] ? extrasAldult - tmpRoom['RoMaxAldult'] : 0;
    var extrasAldultFee = extrasAldult * parseInt(js_config_aldult.replace(',', ''));
    totalFee += extrasAldultFee;
    jQuery("#totalMoneyPeopelEstras").html(FormatPrice(extrasAldultFee));
    if (extrasAldult > 0) {
        jQuery("#peopleSubTitle").html('Người lớn: +' + extrasAldult + ' người');
    } else {
        jQuery("#peopleSubTitle").html('');
    }

    //Total fee
    jQuery("#totalMoneyAll").html(FormatPrice(totalFee));
    //VAT
    var vatFee = 0;
    if (tmpBookingObj === undefined ? jQuery(".chk-vat").is(":checked") : tmpBookingObj['BrIsVAT'] === 1) {
        vatFee = (totalFee * vat) / 100;
        //Display vat fee
        jQuery("#totalMoneyVAT").html(FormatPrice(vatFee));
        //Display vat value
        jQuery("#totalMoneyVATWrap b").html('VAT (' + vat + '%):');
    } else {
        //Display vat fee
        jQuery("#totalMoneyVAT").html(FormatPrice(vatFee));
        //Display vat value
        jQuery("#totalMoneyVATWrap b").html('VAT (0%):');
    }
    totalFee += vatFee;
    //After VAT
    jQuery("#totalMoneyAfterVAT").html(FormatPrice(totalFee));
    return totalFee;
}

/*
 * Ger early hour
 * @param {type} str
 * @returns {Array|GetEarlyHour.returnList}
 */
function GetEarlyHour(str) {
    var returnList = [];
    //Convert to arr
    var arrData = str.split("||");
    //Loop
    for (var i = 0; i < arrData.length; i++) {
        var tmp = arrData[i].split(':');
        returnList.push(
                {
                    'hour': tmp[0],
                    'price': tmp[1]
                }
        );
    }
    return returnList;
}

/*
 * Date time picker init
 * @param {type} id
 * @returns {undefined}
 */
//function DatetimeRangePickerInit(inpStartDate, inpEndDate, inpRangeDate) {
//    //Date time picker config
//    updateConfig(inpRangeDate);
//}
/*
 * Datetime range picker config
 */
//function updateConfig(inpRangeDate) {
//    var options = {};
//    var currentDate = new Date();
//
//    //Date limit
//    //Get current date range
//    var tmpDateRange = jQuery("#comDateRangeSelect option:selected").val();
//    options.dateLimit = {days: parseInt(tmpDateRange)};
//
//    //Start date
//    options.startDate = ConvertDateToString(currentDate);
//
//    //End date
//    options.endDate = ConvertDateToString(new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0));
//}
/*
 * Add data to modal services
 */
function AddDataToModalServices() {
    var html = '';
    for (var i = 0; i < js_services.length; i++) {
        //Skip if current service is child service
        if (js_services[i]['SrParentId'] !== 0) {
            continue;
        }
        //Check if has child services
        var tmp = js_services.filter(function (v) {
            return v.SrParentId === js_services[i]['SrServiceId'];
        });
        if (tmp.length <= 0) {
            html += '<div class="row">'
                    + '    <div class="col-xs-3 col-sm-3"><b>* ---</b></div>'
                    + '    <div class="col-xs-9 col-sm-9">'
                    + '        <div class="row mg-bottom-10">'
                    + '            <div class="col-xs-5 col-sm-5 div-services-child-name" >'
                    + '                <input type="checkbox" class="filled-in chk-col-green chk-services-item" id="chkServicesItem-' + js_services[i]['SrServiceId'] + '" name="chkServicesName" data-id="' + js_services[i]['SrServiceId'] + '"> '
                    + '                 <label for="chkServicesItem-' + js_services[i]['SrServiceId'] + '">' + js_services[i]['SrName'] + '</label>'
                    + '             </div>'
                    + '            <div class="col-xs-4 col-sm-4 services-modal-input div-services-child-price pd-0">' + FormatPrice(js_services[i]['SrPrice']) + ' ' + currency + '/' + js_services[i]['SrCurrency'] + '</div>'
                    + '            <div class="col-xs-3 col-sm-3">'
                    + '                <input value="0" min="0" name="inpServicesNumber" type="number" id="inpServiceNumber-' + js_services[i]['SrServiceId'] + '" class="inp-service-number form-control services-modal-number" data-id="' + js_services[i]['SrServiceId'] + '">'
                    + '            </div>'
                    + '        </div>'
                    + '    </div>'
                    + '</div>';
        } else {
            html += '<div class="row">'
                    + '    <div class="col-xs-3 col-sm-3"><b>* ' + js_services[i]['SrName'] + '</b></div>'
                    + '    <div class="col-xs-9 col-sm-9">';
            for (var j = 0; j < tmp.length; j++) {
                html += '        <div class="row mg-bottom-10">'
                        + '            <div class="col-xs-5 col-sm-5 div-services-child-name" >'
                        + '                <input type="checkbox" class="filled-in chk-col-green chk-services-item" id="chkServicesItem-' + tmp[j]['SrServiceId'] + '" name="chkServicesName" data-id="' + tmp[j]['SrServiceId'] + '"> '
                        + '                 <label for="chkServicesItem-' + tmp[j]['SrServiceId'] + '">' + tmp[j]['SrName'] + '</label>'
                        + '             </div>'
                        + '            <div class="col-xs-4 col-sm-4 services-modal-input div-services-child-price pd-0">' + FormatPrice(tmp[j]['SrPrice']) + ' ' + currency + '/' + tmp[j]['SrCurrency'] + '</div>'
                        + '            <div class="col-xs-3 col-sm-3">'
                        + '                <input value="0" min="0" name="inpServicesNumber" type="number" id="inpServiceNumber-' + tmp[j]['SrServiceId'] + '" class="inp-service-number form-control services-modal-number" data-id="' + tmp[j]['SrServiceId'] + '">'
                        + '            </div>'
                        + '        </div>';
            }
            html += '    </div>'
                    + '</div>';

        }

    }
    //Add script
    html += '<script>' +
            'jQuery(\'.chk-services-item\').on(\'change\', function () {\n' +
            '        //Get id\n' +
            '        var id = $(this).attr(\'data-id\');\n' +
            '        if (this.checked) // if changed state is "CHECKED"\n' +
            '        {\n' +
            '            $(\'#inpServiceNumber-\' + id).val(\'1\');\n' +
            '        }else{\n' +
            '            $(\'#inpServiceNumber-\' + id).val(\'0\');\n' +
            '        }\n' +
            '    });' +
            '</script>';
    //Add html to modal
    jQuery("#servicesModal .modal-body").html(html);
}

/*
 * Convert string to checked checkbox
 */
function ConvertStringToCheckedCheckbox() {
    //Uncheck all checkbox
    jQuery('.chk-services-item').removeAttr('checked');
    //Reset all number
    jQuery(".inp-service-number").val(0);
    //Get string
    var str = jQuery("#inpServices").val();
    //If null then return
    if (str === '') {
        return;
    }
    //Convert to array
    var arrData = str.split('||');
    //Loop
    for (var i = 0; i < arrData.length; i++) {
        //Conver to array
        var tmpArr = arrData[i].split(':');
        var id = parseInt(tmpArr[0]);
        //Set checkbox selected 
        jQuery('#chkServicesItem-' + id).prop('checked', true);
        //Set number
        jQuery("#inpServiceNumber-" + id).val(tmpArr[1]);
    }
}

/*
 * Establish services selected
 */
function ConvertCheckedCheckboxToString() {
    var arrSelected = [];

    //Get list checked checkbox
    jQuery('.chk-services-item:checked').each(function () {
        //Get id
        var id = jQuery(this).attr('data-id');
        //Get number
        var number = jQuery('#inpServiceNumber-' + id).val();
        //Add to selected array
        arrSelected.push([parseInt(id), parseInt(number)]);
    });

    //Convert array to string
    var strSelected = '';
    for (var i = 0; i < arrSelected.length; i++) {
        strSelected += arrSelected[i][0] + ':' + arrSelected[i][1] + '||';
    }
    //Remove last 2 characters ||
    strSelected = strSelected.substring(0, strSelected.length - 2);

    //Set to textbox input
    jQuery("#inpServices").val(strSelected);

    //Display selected services
    DisplaySelectedServices();

    //Calculating total room fee
    CalculatingRoomFee();

    //Close modal
    jQuery('#servicesModal').modal('hide');
}

/*
 * Display selected services
 */
function DisplaySelectedServices() {
    //Get string
    var str = jQuery("#inpServices").val();
    //Convert to array
    var arrData = str !== '' ? str.split('||') : [];
    var html = '';
    //Loop
    for (var i = 0; i < arrData.length; i++) {
        //Conver to array
        var tmpArr = arrData[i].split(':');
        //Get name of current service
        var currentServices = js_services.find(x => x.SrServiceId === parseInt(tmpArr[0]));
        html += '<div class="alert alert-info service-item">'
                + currentServices['SrName'] + ' <span class="badge badge-pill badge-info">' + tmpArr[1] + '</span>'
                + '     </div>';
    }
    //Set selected services html
    jQuery('#divServicesDetailWrap').html(html);
}

/*
 * Fill data to combobox get room early
 */
function FillComboboxGetRoomEarly() {
    //Convert to array
    //Loop
    for (var i = 0; i < js_get_room_early.length; i++) {
        var tmp = js_get_room_early[i];
        //Add option
        jQuery("#comGetRoomEarly").append(
                '<option value="' + tmp.hour + '" data-price-"' + tmp.price + '">' + tmp.hour + 'h - ' + FormatPrice(tmp.price) + ' ' + currency, tmp.hour + '</option>');
    }
}

/*
 * Display transfer room info
 */
function DisplayTranferRoomInfo(roomId, date) {
    if (roomId !== '' && roomId !== null) {
        //Get transfer room info
        var transferRoom = js_rooms.find(x => x.RoRoomId === roomId);
        //Set content
        jQuery("#roomSubTitle").html('Chuyển sang từ phòng ' + transferRoom['RoName'] + ' từ ngày ' + ConvertDateToString(NewDate(date)));
        jQuery("#extrasCheckboxRoom").css('display', 'block');
        jQuery("#extrasCheckboxRoom label").html('Chuyển sang từ phòng ' + transferRoom['RoName'] + ' từ ngày ' + ConvertDateToString(NewDate(date)));
        //return price
        return transferRoom['RoPrice'];
    } else {
        //Reset content
        jQuery("#roomSubTitle").html('');
        jQuery("#extrasCheckboxRoom").css('display', 'none');
        jQuery("#extrasCheckboxRoom label").html('');
        //return 0
        return 0;
    }
}

/*
 * Get max id
 */
function GetMaxId() {
    var maxID = 0;
    for (var i = 0; i < js_booking.length; i++) {
        if (js_booking[i]['BrBookingId'] > maxID) {
            maxID = js_booking[i]['BrBookingId'];
        }
    }
    return maxID + 1;
}

/*
 * Transfer room
 */
function TransferRoom(mode) {
    //Get info: room id, booking id, from date, to date, search data
    var valid = true;
    var roomId = jQuery('#comBookingRoom option:selected').val();
    var fromDate = jQuery('#datBookingFromDate').val();
    var toDate = jQuery('#datBookingToDate').val();
    var search = [];

    //Get search data
    if (bookingObj['BrBookingId'] !== '') {
        search = js_booking.filter(x => x.BrRoomId === roomId && x.BrBookingId !== bookingObj['BrBookingId']);
    } else {
        search = js_booking.filter(x => x.BrRoomId === roomId);
    }

    //Conflic
    valid = CheckConlicTransferRoom(search, fromDate, toDate);
    if (valid === false) {
        //alert('conflict');
        showWithTitleMessage({
            title: "Lỗi!",
            text: "Từ ngày " + ConvertDateToString(fromDate) + " tới " + ConvertDateToString(toDate) +
                    " đã được đặt phòng hoặc đang có người ở.",
            mode: 'warning'
        });
    }

    //If edit then check valid extras conditions
    if (mode === 'room' && bookingObj['BrStatus'] === roomStatus.CheckIn) {

        //First date
//        var fromDate = NewDate(bookingObj['BrFromDate']);
//        if (valid && CompareTwoDate(fromDate, currentDate) === 1) {
//            alert('Đang ở ngày 1 !!! Modal confirm,có save k? Save -> tăng transfer lên 1 ngày và lưu, 2 k lưu , 3 bỏ');
//            valid = false;
//        }
        //Last day
        var toDate = NewDate(bookingObj['BrToDate']);
        toDate.setDate(toDate.getDate() - 1);
        if (valid && CompareTwoDate(toDate, currentDate) === 1) {
            //alert('Last day');
            showWithTitleMessage({
                title: "Lỗi!",
                text: "Ngày hiện tại là ngày checkout nên không thể chuyển phòng.",
                mode: 'warning'
            });
            valid = false;
        }

        //Stay 1 day
        if (valid && CountBetweenDays(bookingObj['BrFromDate'], bookingObj['BrToDate']) === 1) {
            //alert('1 Date');
            showWithTitleMessage({
                title: "Lỗi!",
                text: "Khách " + bookingObj['BrCustomerName'] + " chỉ ở 1 ngày nên không chuyển phòng.",
                mode: 'warning'
            });
            valid = false;
        }

        //Already transfered
        if (valid && bookingObj['BrTransferDate'] !== '' && bookingObj['BrTransferDate'] !== null) {
            //alert('already transfer');
            showWithTitleMessage({
                title: "Lỗi!",
                text: "Tối đa chuyển phòng 2 lần",
                mode: 'danger'
            });
            valid = false;
        }
        //Save transfer info
        if (valid) {
            SaveTransferRoom(roomId);
        }
    }

    //Invalid then reset value
    if (valid === false) {
        ResetTransferRoom();
    }

    //Calculating too fee
    CalculatingRoomFee();
}

/*
 * Check conflic transfer room
 * @param {type} search
 * @param {type} fromDate
 * @param {type} toDate
 * @returns {undefined}
 */
function CheckConlicTransferRoom(arrData, fromDate, toDate) {

    //Create date object
    fromDate = NewDate(fromDate);
    toDate = NewDate(toDate);

    //Decrease 1 day
    if (CompareTwoDate(fromDate, toDate) === 2) {
        toDate.setDate(toDate.getDate() - 1);
    }

    //Get list days from fromdate to todate
    var arrDays = GetListBetweenDays(fromDate, toDate);
    //Loop data to check
    for (var i = 0; i < arrData.length; i++) {

        fromDate = NewDate(arrData[i]['BrFromDate']);
        toDate = NewDate(arrData[i]['BrToDate']);
        toDate.setDate(toDate.getDate() - 1);

        //Loop list days
        for (var j = 0; j < arrDays.length; j++) {
            if (arrDays[j].getTime() >= fromDate.getTime() &&
                    arrDays[j].getTime() <= toDate.getTime()) {
                return false;
            }
        }
    }
    return true;
}

/*
 * Save transfer room info
 * @returns {undefined}
 */
function SaveTransferRoom(roomId) {
    //Get info
    var fromDate = NewDate(jQuery('#datBookingFromDate').val());
    var toDate = NewDate(jQuery('#datBookingToDate').val());
    //If change back to old then reset
    if (roomId === bookingObj['BrRoomId'] &&
            CompareTwoDate(fromDate, bookingObj['BrFromDate']) === 1 &&
            CompareTwoDate(toDate, bookingObj['BrToDate']) === 1) {
        jQuery('#inpTransferRoomId').val('');
        jQuery('#inpTransferDate').val('');
    } else {
        jQuery('#inpTransferRoomId').val(bookingObj['BrRoomId']);
        fromDate.setDate(fromDate.getDate() + 1);
        jQuery('#inpTransferDate').val(ConvertDateToString(fromDate));
    }
}

/*
 * Reset transfer room
 * @returns {undefined}
 */
function ResetTransferRoom() {
    //Update variable modal booking detail first init
    modalBookingInitStatus = true;
    //Combobox room
    jQuery("#comBookingRoom option[value='" + bookingObj['BrRoomId'] + "']").prop("selected", "selected");
    //From date
    jQuery('#datBookingFromDate').val(bookingObj['BrFromDate']);
    //To date
    jQuery('#datBookingToDate').val(bookingObj['BrToDate']);
    //Reset value
    modalBookingInitStatus = false;
}

/*
 * Date time picker init
 * @param {type} fromDate
 * @param {type} toDate
 * @returns {undefined}
 */
function DateTimePickerInit(fromDate, toDate) {
    $("#datBookingFromDate").val(ConvertDateToString(fromDate));
    $("#datBookingToDate").val(ConvertDateToString(toDate));

//    //input var
//    var input = {
//        format: dateFormat,
//        clearButton: true,
//        weekStart: 1,
//        time: false,
//        currentDate: fromDate
//    };
//    //If fromDate smaller then current date
//    if (CompareTwoDate(fromDate, currentDate) !== 2) {
//        input['minDate'] = currentDate;
//    }
//    //From date
//    $("#datBookingFromDate").bootstrapMaterialDatePicker(input);
//
//    //If toDate smaller then current date
//    input['currentDate'] = toDate;
//    if (CompareTwoDate(toDate, currentDate) !== 2) {
//        input['minDate'] = currentDate;
//    }
//    //From date
//    $("#datBookingToDate").bootstrapMaterialDatePicker(input);
}

/*
 * Reset date time picker limit
 */
function ResetDateTimePickerLimit(id) {

    //Get fromdate and todate
    var fromDate = NewDate(jQuery('#datBookingFromDate').val());
    var toDate = NewDate(jQuery('#datBookingToDate').val());

    //If fromDAte or toDate empty then return
    if (jQuery('#datBookingFromDate').val() === '' || jQuery('#datBookingToDate').val() === '') {
        return;
    }

    // //Select current date smaller than current day
    // if (CompareTwoDate(fromDate, currentDate) === 2) {
    //     fromDate = currentDate;
    // }

    //If fromDate and smaller or toDAte and bigger then return
    if (CompareTwoDate(fromDate, toDate) === 2) {
        return;
    }

    //Update variable modal booking detail first init
    modalBookingInitStatus = true;
    //If fromDate then increase toDate up to 1 day
    if (id === '#datBookingFromDate') {
        fromDate.setDate(fromDate.getDate() + 1);
        jQuery('#datBookingToDate').val(ConvertDateToString(fromDate));
    }
    //If toDate then decrease fromDate down to 1 day
    if (id === '#datBookingToDate') {
        if (toDate.getTime() > currentDate.getTime()) {
            toDate.setDate(toDate.getDate() - 1);
            jQuery('#datBookingFromDate').val(ConvertDateToString(toDate));
        } else {
            jQuery('#datBookingFromDate').val(ConvertDateToString(toDate));
            toDate.setDate(toDate.getDate() + 1);
            jQuery('#datBookingToDate').val(ConvertDateToString(toDate));
        }
    }
    //Reset value
    modalBookingInitStatus = false;
}

//Disable form
function DisableForm(status) {
    //Input
    jQuery("#frmBookingDetail :input").prop("disabled", status);
    //Checkbox
    jQuery("#frmBookingDetail :checkbox").prop("disabled", status);
    //Combobox
    jQuery("#frmBookingDetail :select.dropdown").prop("disabled", status);
    //Textarea
    jQuery("#inpBookingNote").prop("disabled", status);
    //Buttons
    jQuery("#btnModalBookingDetailSave").prop("disabled", status);
    jQuery("#btnModalBookingDetailExport").prop("disabled", false);
    jQuery("#btnModalBookingDetailClose").prop("disabled", false);
}

/*
 * Handel sweet confirm dialog buttons click
 */
function SweetAlertHandleButtonClick(obj, isConfirm) {
    if (isConfirm) {
        //Save data
        if (obj.attr('data-type') === 'remove') {
            RemoveBooking(parseInt(obj.attr('data-booking-id')));
        } else {
            SubmitFormHandle();
        }

    } else {
        //Reset value
        isFormChanged = false;
        //Close modal booking detail
        if (obj.attr('data-type') === 'cancel' || obj.attr('data-type') === undefined) {
            jQuery('#newBookingModal').modal('hide');
            jQuery('#newBookingModal').removeClass('show');
        }

    }
}

/*
 Export room bill
 */
function ExportRoomBill(obj) {
    //Show preloader
    $('.page-loader-wrapper').fadeIn();
//Get id
    var id = $(obj).attr('data-id');
    //Delete
    $.ajax({
        url: exportUrl + '\\' + id,
        type: 'post',
        data: {
            id: id
        },
        success: function (data) {
            //Hide preloader
            $('.page-loader-wrapper').fadeOut();
            if (data.success) {
                window.open(viewPdfUrl + '\\' + data.file_name, '_blank');

            } else {
                //Show error modal
                swal("Lỗi!", "Không thể xuất file", "error");
            }
        },
        error: function (xhr, status, error) {
            //Show modal message
            swal("Lỗi!", "Có lỗi xảy ra. " + error, "error");
        }
    });
}

/*
 Catch booking grid onscroll
 */
function OnScroll() {
    var scrollPosLeft = $(document).scrollLeft();
    var scrollPosTop = $(document).scrollTop();
    console.log('Left: ' + scrollPosLeft + ' - Top: ' + scrollPosTop);
    //Fixed date title
    if (scrollPosTop >= 150) {
        //Add class
        $('#divBookingHeaderInner').addClass('fixed-div date-fixed');
        //Set top
        $('#divBookingHeaderInner').attr('style', 'top: ' + ($(this).scrollTop() - 30) + 'px !important');
        //Set margin
        $('#divBookingHeaderInner').css('margin-left', '157px');
    } else {
        //Remove class
        $('#divBookingHeaderInner').removeClass('fixed-div date-fixed');
        //Set margin
        $('#divBookingHeaderInner').css('margin-left', '0px');
    }

    //Fixed room title
    if (scrollPosLeft >= 120) {

        //Add classes
        $('#roomWrap').addClass('fixed-div room-fixed');

        //Set margin
        $('#bookingWrap').css('margin-left', '115px');
        if (scrollPosTop >= 150) {
            $('#divBookingHeaderInner').css('margin-left', '130px');
        }
        //Set left
        $('#roomWrap').attr('style', 'left: ' + ($(this).scrollLeft() - 29) + 'px !important');
    } else {
        //Remove classes
        $('#roomWrap').removeClass('fixed-div room-fixed');

        //Reset margin left
        $('#bookingWrap').css('margin-left', '0px');
    }
}

/*
 Booking grid jump to active day
 */
function JumpToActiveDate() {

    //Get browser width
    var pageWidth = $(window).width();
    //Get active day prosition
    if ($("#activePosition").length > 0) {
        var activePosition = $("#activePosition").offset().left;
    } else {
        var activePosition = 0;
    }

    var jumpPosition = 0;

    //If active date outside of visible area then jump, else, jump to left
    if (activePosition < pageWidth) {
        //Jump
        $(document).scrollLeft(jumpPosition);
    } else {
        jumpPosition = (activePosition - pageWidth) + 1000;
        //Jump
        $(document).scrollLeft(jumpPosition);
    }

}

/*
 Switch display mode events
 */
function SwitchDisplayModeEvents() {
    $('.btn-switch-mode, .btn-refill-booking').on('click', function () {
        //Get mode
        var mode = $(this).data('mode');

        //If click todate button then reset month and year
        if (mode === 2) {
            toolbarObj['Month'] = currentDate.getMonth() + 1;
            toolbarObj['Year'] = currentDate.getFullYear();
        }

        //Update direction
        toolbarObj['Direction'] = $(this).data('direction');

        //If click button switch
        if ($(this).data('direction') === 1 && mode !== 2) {
            //Update mode
            displayMode = parseInt(mode);
            //Update toolbar object
            toolbarObj['Mode'] = mode;
            //Enable/Disable button
            if (mode !== 1) {
                $('#btnWeek').prop('disabled', false);
                $('#btnMonth').prop('disabled', true);
            } else {
                $('#btnWeek').prop('disabled', true);
                $('#btnMonth').prop('disabled', false);
            }
        }

        //Handle change booking data where click to toolbar
        InitBooking();
    });
}

/*
 Get booking range and set info
 */
function InitBooking() {
    switch (toolbarObj['Direction']) {
        case 0:
            //Month and todate
            if (toolbarObj['Mode'] !== 1) {
                var startDate = NewDate(toolbarObj['Start']);
                startDate.setMonth(startDate.getMonth() - 1, 1);
                toolbarObj['Start'] = startDate;
                toolbarObj['Month'] = startDate.getMonth() + 1;
                toolbarObj['Year'] = startDate.getFullYear();
                var lastDate = new Date(currentDate.getFullYear(), toolbarObj['Month'], 0);
                toolbarObj['End'] = NewDate(ConvertDateToString(lastDate));
                toolbarObj['DayCount'] = lastDate.getDate();
            }
            //Week
            else {
                var tmp = NewDate(toolbarObj['Start']);
                tmp.setDate(tmp.getDate() - 1);
                toolbarObj['Month'] = tmp.getMonth() + 1;
                toolbarObj['Year'] = tmp.getFullYear();

                //Update toolbar object
                tmp = GetWeekDates(tmp.getDate());
                toolbarObj['Start'] = tmp.Start;
                toolbarObj['End'] = tmp.End;
                toolbarObj['DayCount'] = tmp.DayCount;

            }

            break;
        case 1:
            //Month and todate
            if (toolbarObj['Mode'] !== 1) {
                //Update toolbar object
                toolbarObj['Start'] = NewDate('1' + dateDevideCharacter + toolbarObj['Month'] + dateDevideCharacter + currentDate.getFullYear());
                var lastDate = new Date(currentDate.getFullYear(), toolbarObj['Month'], 0);
                toolbarObj['End'] = NewDate(ConvertDateToString(lastDate));
                toolbarObj['DayCount'] = lastDate.getDate();
            }
            //Week
            else {
                //Current month
                if (toolbarObj['Month'] === currentDate.getMonth() + 1) {
                    var tmp = GetWeekDates(currentDate.getDate());
                }
                //Other month
                else {
                    var tmp = GetWeekDates(1);
                }
                //Update toolbar object
                toolbarObj['Start'] = tmp.Start;
                toolbarObj['End'] = tmp.End;
                toolbarObj['DayCount'] = tmp.DayCount;
            }

            break;
        case 2:
            //Month and todate
            if (toolbarObj['Mode'] !== 1) {
                var startDate = NewDate(toolbarObj['End']);
                startDate.setMonth(startDate.getMonth() + 1, 1);
                toolbarObj['Start'] = startDate;
                toolbarObj['Month'] = startDate.getMonth() + 1;
                toolbarObj['Year'] = startDate.getFullYear();
                var lastDate = new Date(currentDate.getFullYear(), toolbarObj['Month'], 0);
                toolbarObj['End'] = NewDate(ConvertDateToString(lastDate));
                toolbarObj['DayCount'] = lastDate.getDate();
            }
            //Week
            else {
                var tmp = NewDate(toolbarObj['End']);
                tmp.setDate(tmp.getDate() + 1);
                toolbarObj['Month'] = tmp.getMonth() + 1;
                toolbarObj['Year'] = tmp.getFullYear();

                //Update toolbar object
                tmp = GetWeekDates(tmp.getDate());
                toolbarObj['Start'] = tmp.Start;
                toolbarObj['End'] = tmp.End;
                toolbarObj['DayCount'] = tmp.DayCount;

            }

            break;
    }

    //Scroll to active date
    if ((CompareTwoDate(toolbarObj['Start'], currentDate) === 2 || CompareTwoDate(toolbarObj['Start'], currentDate) === 1) &&
            (CompareTwoDate(toolbarObj['End'], currentDate) === 0 || CompareTwoDate(toolbarObj['End'], currentDate) === 1)) {
        JumpToActiveDate();
        OnScroll();
    }

    //Refill booking data
    FillBookingData(NewDate(toolbarObj['Start']), toolbarObj['DayCount']);
    //Update toolbar text
    UpdateToolBarText();


}

/*
 Get week start and end date
 */
function GetWeekDates(day) {
    //Create date object
    var firstDate = NewDate(day + dateDevideCharacter + toolbarObj['Month'] + dateDevideCharacter + toolbarObj['Year']);
    var lastDate = NewDate(day + dateDevideCharacter + toolbarObj['Month'] + dateDevideCharacter + toolbarObj['Year']);
    //Get week day
    var weekIndex = firstDate.getDay();
    weekIndex = weekIndex === 0 ? 7 : weekIndex;

    //Get begin date of week
    firstDate.setDate(firstDate.getDate() - (weekIndex - weekArr[1]));
    //Get last date of week
    lastDate.setDate(lastDate.getDate() + (weekArr.length - weekIndex))

    //Return
    return {
        Start: firstDate,
        End: lastDate,
        DayCount: weekDaysCount,
    }
}

/*
 Update toolbar text
 */
function UpdateToolBarText() {
    //Establish start and end date string
    var start = toolbarObj['Start'].getDate() + '/' + (toolbarObj['Start'].getMonth() + 1);
    var end = toolbarObj['End'].getDate() + '/' + (toolbarObj['End'].getMonth() + 1);

    if (toolbarObj['Mode'] !== 1) {
        $('#inpWeekLabel').val('Tháng ' + toolbarObj['Month'] + ' (' + start + ' - ' + end + '), ' + toolbarObj['Year']);
    } else {
        $('#inpWeekLabel').val('Tuần (' + start + ' - ' + end + '), ' + toolbarObj['Year']);
    }
}

/*
 Expand grid width
 */
function ExpandGridWidth() {
    var totalExtras = 0;
    var arrExtras = [];
    //Loop all booking block to get max extras
    $("#bookingWrap .booking-block ").each(function (e) {
        //Get input
        var width = parseInt($(this).attr('data-width'));
        var extras = parseInt($(this).attr('data-extras'));
        var index = "" + $(this).data('index');
        var html = $(this).html();

        //If current customer name is longer than max
        if (extras !== 0) {

            //Add to array extras
            var exist = false;
            var checkExtras = 0;
            var checkIndex = 0;
            var arrCurrentIndex = index.split(',');
            for (var i = 0; i < arrExtras.length; i++) {

                var arrIndex = arrExtras[i]['index'].split(',');

                for (var j = 0; j < arrCurrentIndex.length; j++) {
                    if (jQuery.inArray(arrCurrentIndex[j], arrIndex) !== -1) {
                        exist = true;
                        checkExtras = arrExtras[i]['extras'];
                        checkIndex = i;
                    }
                }
            }
            if (exist) {
                if (extras > checkExtras) {
                    arrExtras[checkIndex]['extras'] = extras;
                }
            } else {
                arrExtras.push({
                    index: index,
                    extras: extras
                })
            }
        }

    });

    //Loop arr of extras to increase width
    for (var i = 0; i < arrExtras.length; i++) {
        $('#bookingWrap .booking-block').each(function (e) {
            //Get input
            var subWidth = parseInt($(this).attr('data-width'));
            var subIndex = $(this).data('index') + "";
            subIndex = subIndex.split(',');
            var checkIndex = arrExtras[i]['index'].split(',');
            var currentWdith = parseInt($(this).css('width'));
            var html = $(this).html();

            //Check if index exist
            var exist = false;
            for (var j = 0; j < checkIndex.length; j++) {
                if (jQuery.inArray("" + checkIndex[j], subIndex) !== -1) {
                    exist = true;
                }
            }
            //If same column
            if (exist) {
                var extrasWidth = subWidth + arrExtras[i]['extras'];
                //Reset width and data
                if (currentWdith < extrasWidth) {
                    $(this).css('width', extrasWidth);
                    $(this).attr('data-width', extrasWidth);
                }
                //Reset booking hover detail margin
                if ($(this).children('.booking-hover-detail').length > 0) {
                    //Get margin left
                    var marginLeft = $(this).children('.booking-hover-detail').css('margin-left');
                    marginLeft = marginLeft.substring(0, marginLeft.length - 2);
                    marginLeft = parseInt(marginLeft);
                    var rightHoverPositionIndex = toolbarObj['Mode'] === 0 ? toolbarObj['DayCount'] - 2 : toolbarObj['DayCount'];

                    //Check if current block is last 3 days in month or last day in week
                    var isLastDays = true;
                    for (var j = 0; j < subIndex.length; j++) {
                        if (parseInt(subIndex) >= rightHoverPositionIndex) {
                            isLastDays = false;
                        }
                    }

                    if (extrasWidth > marginLeft && isLastDays) {
                        $(this).children('.booking-hover-detail').css('margin-left', extrasWidth - 20);
                    }
                }
            }

        });

        //Update wrap width
        totalExtras += arrExtras[i]['extras'];
    }

    //Extras width
    if (toolbarObj['Mode'] === 0) {
        totalExtras += 3;
    } else {
        totalExtras += 33;
    }


    //Extras percent
    //totalExtras += (totalExtras * 2) / 100;

    //Set total width: 30 - 1770, 31 - 1827, 7 - 1780
    var totalWidth = 0;
    if (displayMode === 0) {
        if (toolbarObj['DayCount'] === 30) {
            totalWidth = 1770;
        } else if (toolbarObj['DayCount'] === 31) {
            totalWidth = 1827;
        } else if (toolbarObj['DayCount'] === 28) {
            totalWidth = 1657;
        } else {
            totalWidth = 1714;
        }
    } else {
        totalWidth = 873;
    }
    $('#wrapAll').css('width', totalWidth + totalExtras);
    console.log('in');
}

/*
 * Get block index
 */
function GetBlockIndex(index, daysCount) {
    daysCount = daysCount + index - 1;
    var listIndex = "";
    for (var i = index; i <= daysCount; i++) {
        listIndex += i + ',';
    }
    return listIndex.substr(0, listIndex.length - 1)
}
/*
 Submit handle
 */
function SubmitFormHandle(isExport) {
    //Show preloader
    $('.page-loader-wrapper').fadeIn();

    //Check valid
    var check = CheckDataValid();
    if (check.Valid === false) {
        //Hide preloader
        $('.page-loader-wrapper').fadeOut();
        //Show error modal
        swal("Lỗi!", check.Message, "error");
        return;
    }
    //Get data
    var input = {
        BrCustomerName: $('#inpBookingCustomerName').val(),
        BrCustomerEmail: $('#inpBookingCustomerEmail').val(),
        BrCustomerPhone: $('#inpBookingCustomerPhone').val(),
        BrRoomId: $('#comBookingRoom option:selected').val(),
        BrTransferRoomId: $('#inpTransferRoomId').val(),
        BrFromDate: $('#datBookingFromDate').val(),
        BrToDate: $('#datBookingToDate').val(),
        BrTransferDate: $('#inpTransferDate').val(),
        BrNote: $('#inpBookingNote').val(),
        BrServices: $('#inpServices').val(),
        BrStatus: parseInt($('#comBookingRoomStatus option:selected').val()),
        BrDeposit: parseInt($('#inpBookingDeposit').val().replace(',', '')),
        BrAldult: parseInt($('#inpBookingAldult').val()),
        BrBaby: parseInt($('#inpBookingBaby').val()),
        BrMode: $('#inpBookingModeString').val(),
        BrIsVAT: parseInt($('#md_checkbox_36').val()),
        BrCreateUserId: loginInfo['UsUserId'],
    };

    //Get booking Id
    if ($('#inpBookingId').val() !== '') {
        input['BrBookingId'] = parseInt($('#inpBookingId').val());
    } else {
        var maxId = 0;
        for (var i = 0; i < js_booking.length; i++) {
            if (js_booking[i]['BrBookingId'] > maxId) {
                maxId = js_booking[i]['BrBookingId'];
            }
        }
        input['BrBookingId'] = maxId + 1;
    }
    
    //Update ajax call status
    isAjaxLoaded = true;
    
    //Ajax update data
    $.ajax({
        url: saveUrl,
        type: 'post',
        data: input,
        success: function (data) {
            //Parse return data to javascript object
            data = JSON.parse(data);

            //Hide preloader
            $('.page-loader-wrapper').fadeOut();

            //Hide booking modal
            if (isExport === undefined) {
                jQuery('#newBookingModal').modal('hide');
                jQuery('#newBookingModal').removeClass('show');
            }

            if (data.success) {
                //Show success modal
                if (isExport === undefined) {
                    swal("Thông báo!", "Lưu đặt phòng thành công.", "success");
                }

                //Update total booking list
                if ($('#inpBookingId').val() !== '') {
                    var index = js_booking.findIndex(x => x.BrBookingId === parseInt(input['BrBookingId']));
                    js_booking[index] = input;
                } else {
                    js_booking.push(input);
                }

                //Refill booking grid
                FillBookingData(toolbarObj['Start'], toolbarObj['DayCount']);
            } else {
                //Show error modal
                swal("Lỗi!", "Không thể lưu đặt phòng.", "error");
            }
        },
        error: function (xhr, status, error) {
            //Hide preloader
            $('.page-loader-wrapper').fadeOut();
            //Show modal message
            swal("Lỗi!", "Có lỗi xảy ra. " + error, "error");
        }
    });
}
/*
 * Delete booking
 * @param {type} id
 * @returns {undefined}
 */
function RemoveBooking(id) {
    //Show preloader
    $('.page-loader-wrapper').fadeIn();
    //Get data
    var input = {
        BrBookingId: id
    };
    //Update ajax call status
    isAjaxLoaded = true;
    //Ajax update data
    $.ajax({
        url: deleteUrl,
        type: 'post',
        data: input,
        success: function (data) {
            //Parse return data to javascript object
            data = JSON.parse(data);

            //Hide preloader
            $('.page-loader-wrapper').fadeOut();

            if (data.success) {
                //Show success modal
                swal("Thông báo!", "Xóa đặt phòng thành công.", "success");

                //Remove from booking list
                var index = js_booking.findIndex(x => x.BrBookingId === parseInt(input['BrBookingId']));
                js_booking.splice(index, 1);

                //Refill booking grid
                FillBookingData(toolbarObj['Start'], toolbarObj['DayCount']);
            } else {
                //Show error modal
                swal("Lỗi!", "Không xóa lưu đặt phòng.", "error");
            }
        },
        error: function (xhr, status, error) {
            //Hide preloader
            $('.page-loader-wrapper').fadeOut();
            //Show modal message
            swal("Lỗi!", "Có lỗi xảy ra. " + error, "error");
        }
    });
}
//Check valid image
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
/*
 Check save data valid
 */
function CheckDataValid() {
    valid = true;
    message = '';
    //Customer name empty
    if ($('#inpBookingCustomerName').val() === '') {
        valid = false;
        message += ' Hãy điền tên khách hàng trước khi lưu.\n';
    }
    //Check valid email
    if ($('#inpBookingCustomerEmail').val() !== '' && isEmail($('#inpBookingCustomerEmail').val()) === false) {
        valid = false;
        message += 'Địa chỉ email không hợp lệ.\n';
    }
    // if ($('#inpBookingCustomerPhone').val() === '') {
    //     valid = false;
    //     message += 'Hãy điền số điện thoại trước khi lưu.\n';
    // }
    //Phone is empty
    return {
        Valid: valid,
        Message: message.substring(0, message.length - 1)
    };
}
//Hanle when success export pdf
function HandleAfterExport(data) {

    //Set combobox room status into check out
    $('#comBookingRoomStatus').selectpicker('val', roomStatus.CheckOut);

    //Save data
    SubmitFormHandle(true);

    //Disable form
    if (loginInfo['UsUserType'] === 1) {
        DisableForm(true);
    }

    //Update list booking
    var index = js_booking.findIndex(x => x.BrBookingId === parseInt(data['id']));
    var tmp = js_booking[index];
    tmp['BrStatus'] = roomStatus.CheckOut;
    js_booking[index] = tmp;

    //Refill booking grid
    FillBookingData(toolbarObj['Start'], toolbarObj['DayCount']);

    //Reset data change variable
    isFormChanged = false;
}