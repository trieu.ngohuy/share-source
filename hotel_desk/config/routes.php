<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    //$routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);
    $routes->connect('/', ['controller' => 'Booking', 'action' => 'index']);
    
    /*
     * RoomsController Routes
     */
    $routes->connect('/rooms/*', ['controller' => 'Rooms', 'action' => 'index']);
    
    /*
     * AuthenticateController Routes
     */
    $routes->connect('/login', ['controller' => 'Authenticate', 'action' => 'login']);
    $routes->connect('/logout', ['controller' => 'Authenticate', 'action' => 'logout']);
    
    /*
     * Users Routes
     */
    $routes->connect('/user/manager', ['controller' => 'Users', 'action' => 'manager']);
    $routes->connect('/user/modify/*', ['controller' => 'Users', 'action' => 'modify']);
    $routes->connect('/user/delete', ['controller' => 'Users', 'action' => 'delete']);
    /*
     * Error Routes
     */
    $routes->connect('/error/no-permission', ['controller' => 'Error', 'action' => 'noPermission']);
    /*
     * Export excel and pdf Routes
     */
    $routes->connect('/export-room-bill', ['controller' => 'Pdf', 'action' => 'ExportRoomBill']);
    $routes->connect('/export-report', ['controller' => 'Pdf', 'action' => 'ExportReport']);
    $routes->connect('/view-pdf/*', ['controller' => 'Pdf', 'action' => 'ViewPdf']);
    $routes->connect('/export-excel/*', ['controller' => 'Pdf', 'action' => 'ExportExcel']);
    
    /*
     * Rooms Routes
     */
    $routes->connect('/room/manager', ['controller' => 'Rooms', 'action' => 'manager']);
    $routes->connect('/room/modify/*', ['controller' => 'Rooms', 'action' => 'modify']);
    $routes->connect('/room/delete', ['controller' => 'Rooms', 'action' => 'delete']);
    
    /*
     * Services Routes
     */
    $routes->connect('/service/manager', ['controller' => 'Services', 'action' => 'manager']);
    $routes->connect('/service/modify/*', ['controller' => 'Services', 'action' => 'modify']);
    $routes->connect('/service/delete', ['controller' => 'Services', 'action' => 'delete']);
    
    /*
     * Modal Routes
     */
    $routes->connect('/modal/confirm-modal', ['controller' => 'Modal', 'action' => 'confirmModal']);
    $routes->connect('/modal/success-modal', ['controller' => 'Modal', 'action' => 'successModal']);
    $routes->connect('/modal/error-modal', ['controller' => 'Modal', 'action' => 'errorModal']);
    
    /*
     * Export Routes
     */
    $routes->connect('/export', ['controller' => 'Export', 'action' => 'index']);
    $routes->connect('/export/*', ['controller' => 'Export', 'action' => 'export']);
    
    /*
     * Export Routes
     */
    $routes->connect('/locg', ['controller' => 'Export', 'action' => 'index']);
    
    /*
     * Config Routes
     */
    $routes->connect('/config', ['controller' => 'Config', 'action' => 'manager']);
    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
