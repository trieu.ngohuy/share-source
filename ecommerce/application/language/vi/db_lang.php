<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['website_title'] = 'Web bán hàng';
$lang['home'] = 'Trang chủ';
$lang['search'] = 'Tìm kiếm';
$lang['my_account'] = 'Tài khoản';
$lang['shopping_bag'] = 'Giỏ hàng';
$lang['items'] = 'sản phẩm';
$lang['total'] = 'Tổng tiền';
$lang['checkout'] = 'Thanh toán';
$lang['remove'] = 'Xóa';
$lang['search_des'] = 'Gõ từ cần tìm..';
$lang['cart_quantity'] = 'SL';
$lang['empty'] = 'Rỗng';
$lang['empty_des'] = 'Không có sản phẩm trong giỏ hàng.';
//Footer
$lang['follow_us'] = 'Mạng xã hội';
$lang['subs_title'] = 'Đăng ký theo dõi';
$lang['subs_des'] = 'Nhận thông tin mới nhất về chúng thôi.';
$lang['subs_agreement'] = 'Đăng ký để nhận tin tức và chương trình khuyến mãi mới nhất từ chúng tôi. Bằng cách chọn tham gia, bạn đồng ý Chính sách riêng tư và các điều khoản của chúng tôi.';
$lang['subs_email'] = 'Địa chỉ email';
$lang['submit'] = 'Đồng ý';
//Product category
$lang['shop_all'] = 'Tất cả sản phẩm trong';
$lang['bestsellers'] = 'bán chạy nhất';
$lang['new_in'] = 'Sản phẩm mới trong';
//Product category detail
$lang['results'] = 'Sản phẩm';
$lang['sort_by'] = 'Sắp xếp';
$lang['view'] = 'Hiển thị';
$lang['search_all'] = 'Tìm sản phẩm trong';
$lang['recommended'] = 'Sản phẩm đề xuất';
$lang['high_price'] = 'Giá cao trước';
$lang['low_price'] = 'Giá thấp trước';
$lang['newest'] = 'Sản phẩm mới trước';
$lang['all'] = 'Tất cả';
$lang['low'] = 'Thấp';
$lang['to'] = '-';
$lang['high'] = 'Cao';
$lang['clear_all'] = 'Xóa lọc';
$lang['your_selection'] = 'Mục lọc';
$lang['product_type'] = 'Loại sản phẩm';
$lang['style'] = 'Kiểu dáng';
$lang['color'] = 'Màu sắc';
$lang['material'] = 'Chất liệu';
$lang['price'] = 'Giá';
$lang['size'] = 'Kích cỡ';
//Product search
$lang['you_search_for'] = 'Hiển thị kết quả cho từ khóa';
//Product detail
$lang['code'] = 'MÃ SẢN PHẨM';
$lang['add_to_bag'] = 'THÊM VÀ GIỎ HÀNG';
$lang['share'] = 'Chia sẽ';
$lang['detail'] = 'MÔ TẢ SẢN PHẨM';
$lang['feature'] = 'GIAO HÀNH VÀ THANH TOÁN';
$lang['related'] = 'SẢN PHẨM CÓ THỂ BẠN THÍCH';
$lang['qty'] = 'Số lượng';
$lang['pro_text'] = 'FREESHIP đơn trên 300,000 vnđ | GIẢM 10% lần tiếp theo';
$lang['refund_maintenance'] = 'ĐỔI TRẢ VÀ BẢO HÀNH';
$lang['full_text'] = 'NỘI DUNG SẢN PHẨM';
//Blog
$lang['bg_all'] = 'TẤT CẢ';
$lang['bg_empty'] = 'Không có bài viết nào.';
$lang['bg_title'] = 'Tin tức';
//Blog detail
$lang['posted_on'] = 'Ngày tạo';
$lang['prev_article'] = 'BÀI TRƯỚC';
$lang['nex_article'] = 'BÀI TIẾP';
//Cart
$lang['my_bag'] = 'Giỏ hàng';
$lang['product'] = 'Sản phẩm';
$lang['quantity'] = 'SL';
$lang['price'] = 'Giá';
$lang['order_summary'] = 'Tổng hợp đơn hàng';
$lang['delivery_address'] = 'Địa chỉ giao hàng';
$lang['payment_method'] = 'Phương thức thanh toán';
$lang['please_select'] = '--- Chọn ---';
$lang['pay_on_receive'] = 'Thanh toán khi nhận hàng';
$lang['complete_order'] = 'Mua hàng';
$lang['add_voucher_code'] = 'Thêm mã giảm giá';
$lang['enter_code'] = 'Nhập mã';
$lang['add'] = 'Thêm';
$lang['voucher_applied'] = 'Mã giảm giá đã áp dụng';
$lang['max_sale'] = 'Giảm tối đa';
$lang['min_order'] = 'Đơn hàng tối thiểu';
$lang['expire_date'] = 'Ngày hết hạn';
$lang['rules'] = 'Thể lệ';
$lang['no_delivery1'] = 'Không có địa chỉ giao hàng. Vào';
$lang['no_delivery2'] = 'để tạo ngay.';
$lang['transfer_fee'] = 'Phí vận chuyển';
//Cart success
$lang['payment_success'] = 'GIAO DỊCH THÀNH CÔNG';
$lang['payment_des'] = 'Giao dịch của bạn đã thành công. Chúng đang cố gắng giao hàng tới cho bạn càng nhanh càng tốt.';
$lang['continue_shopping'] = 'TIẾP TỤC MUA HÀNG';
//Store
$lang['enter_store_name'] = 'Gõ tên cửa hàng';
$lang['no_store'] = 'Không có cửa hàng ở khu vực này';
$lang['no_place'] = 'Dữ liệu rỗng.';
$lang['open_until'] = 'Mở cửa từ';
$lang['today'] = 'trong ngày';
//404
$lang['oops'] = 'Thật tiếc';
$lang['cant_find_page'] = 'Trang bạn vừa truy cập không tồn tại.';
$lang['404_des'] = 'Đừng lo lắng - các trang khác vẫn bình thường chỉ riêng trang bạn vừa mới tìm có thể đã bị xóa hoặc đã đổi tên khác.';
$lang['404_home'] = 'TRANG CHỦ';
//login
$lang['lg_title'] = 'Đăng nhập hoặc đăng ký tài khoản';
$lang['email'] = 'Nhập email';
$lang['continue'] = 'TIẾP TỤC';
$lang['err_invalid_email'] = 'Địa chỉ email không đúng, hãy gõ lại.';
$lang['welcome_back'] = 'Chào mừng';
$lang['please_type_password'] = ', hãy nhập mật khẩu.';
$lang['reset_password'] = 'ĐỔI MẬT KHẨU';
$lang['firstname'] = 'Tên';
$lang['lastname'] = 'Họ và tên lót';
$lang['password'] = 'Mật khẩu';
$lang['er_require'] = 'Trường bắt buộc nhập';
$lang['err_policy'] = 'Hãy chấp nhận chính sách và các điều khoản.';
//My account
$lang['my_account_2'] = 'Thông tin tài khoản';
$lang['logout'] = 'Đăng xuất';
$lang['account_detail'] = 'Chi tiết tài khoản';
$lang['edit'] = 'Sửa';
$lang['delete'] = 'Xóa';
$lang['change_password'] = 'Thay đổi mật khẩu';
$lang['address_book'] = 'Địa chỉ nhận hàng';
$lang['manager_address_book'] = 'Quản lý địa chỉ nhận hàng';
$lang['default_address_book'] = 'Địa chỉ nhận hàng mặc định';
$lang['my_order'] = 'Danh sách đơn hàng';
$lang['manager_my_order'] = 'Hiển thị tất cả đơn hàng';
$lang['latest_orders'] = 'Đơn hàng gần đây';
$lang['order_date'] = 'Ngày đặt hàng';
$lang['status'] = 'Tình trạng đơn hàng';
$lang['payed'] = 'Đã thanh toán';
$lang['hello'] = 'Xin chào';
$lang['no_default_address'] = 'Không có địa chỉ giao hàng mặc định nào.';
$lang['no_order'] = 'Chưa có đơn hàng nào.';
//Account edit
$lang['account_detail'] = 'Chỉnh sửa thông tin tài khoản';
$lang['title'] = 'DANH XƯNG';
$lang['enter_firstname'] = 'Nhập tên';
$lang['enter_lastname'] = 'Nhập họ và tên lót';
$lang['new_password'] = 'MẬT KHẨU MỚI';
$lang['confirm_new_password'] = 'NHẬP LẠI MẬT KHẨU MỚI';
$lang['save'] = 'LƯU';
//Address book
$lang['address_edit'] = 'Chỉnh sửa thông tin địa chỉ giao hàng';
$lang['additional_address'] = 'Các địa chỉ còn lại';
$lang['add_new_address'] = 'THÊM ĐỊA CHỈ MỚI';
$lang['no_address'] = 'Không có địa chỉ nào';
$lang['country'] = 'ĐẤT NƯỚC';
$lang['province'] = 'TỈNH / THÀNH PHỐ';
$lang['district'] = 'HUYỆN / QUẬN';
$lang['ward'] = 'XÃ / PHƯỜNG';
$lang['address'] = 'Địa chỉ';
$lang['enter_address'] = 'Nhập địa chỉ';
$lang['save_address'] = 'LƯU THÔNG TIN';
//My order
$lang['order_id'] = 'Mã đơn hàng';
//Message
$lang['msg_email_reset_password_title'] = 'Chúng tôi đã gửi mail reset password tới mail của bạn.';
$lang['msg_email_reset_password_content'] = 'Lấy lại mật khẩu ngay bây giờ! <br />
Đừng lo lắng !! Truy cập đường link dưới để lấy lại mật khẩu của bạn: <br />';
$lang['msg_email_reset_password_content_2'] = 'Nếu bạn không muốn lấy lại mật khẩu, có thể bỏ qua email này và tiếp tục mua sắm trên website của chúng tôi.';
$lang['msg_confirm_password_not_equal'] = 'Mật khẩu và mật khẩu nhập lại không khớp.';
$lang['msg_success_update'] = 'Dữ liệu đã được lưu.';
$lang['msg_invalid_voucher'] = 'Mã voucher không đúng, hãy nhập lại.';
$lang['msg_succes_add_to_cart'] = 'Thêm sản phẩm vào giỏ hàng thành công.';
$lang['msg_update_cart'] = 'Sản phẩm đã có trong giỏ hàng. Update sản phẩm thành công.';
