<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['website_title'] = 'Oliver Bonas';
$lang['home'] = 'Home';
$lang['search'] = 'Search';
$lang['my_account'] = 'My account';
$lang['shopping_bag'] = 'Shopping bag';
$lang['items'] = 'items';
$lang['total'] = 'Total';
$lang['checkout'] = 'Checkout';
$lang['remove'] = 'Remove';
$lang['search_des'] = 'Type what you want to search..';
$lang['cart_quantity'] = 'Qty';
$lang['empty'] = 'Empty';
$lang['empty_des'] = 'There is nothing in your bag.';
//Footer
$lang['follow_us'] = 'Follow Us';
$lang['subs_title'] = 'Email Sign Up';
$lang['subs_des'] = 'To receive updates & offers, sign up below.';
$lang['subs_agreement'] = 'Sign up to receive the latest news and promotions from OB. By opting in you accept the Privacy policy and Terms and Conditions';
$lang['subs_email'] = 'Email address';
$lang['submit'] = 'Submit';
//Product category
$lang['shop_all'] = 'Shop all';
$lang['bestsellers'] = 'Bestsellers';
$lang['new_in'] = 'New-in';
//Product category detail
$lang['results'] = 'Results';
$lang['sort_by'] = 'Sort by';
$lang['view'] = 'View';
$lang['search_all'] = 'Search all';
$lang['recommended'] = 'Recommended';
$lang['high_price'] = 'High price';
$lang['low_price'] = 'Low price';
$lang['newest'] = 'Newst in';
$lang['all'] = 'All';
$lang['low'] = 'Low';
$lang['to'] = 'to';
$lang['high'] = 'High';
$lang['clear_all'] = 'Clear all';
$lang['your_selection'] = 'Your selection';
$lang['product_type'] = 'Product type';
$lang['style'] = 'Style';
$lang['color'] = 'Color';
$lang['material'] = 'Material';
$lang['price'] = 'Price';
$lang['size'] = 'Size';
//Product search
$lang['you_search_for'] = 'Your search results for';
//Product detail
$lang['code'] = 'CODE';
$lang['add_to_bag'] = 'ADD TO BAG';
$lang['share'] = 'Share';
$lang['detail'] = 'IT"S IN THE DETAIL';
$lang['feature'] = 'TRANSFER AND PAYMENT';
$lang['related'] = 'YOU MAY ALSO LIKE';
$lang['qty'] = 'Quantity';
$lang['pro_text'] = 'FREESHIP application for over VND 300,000 | 10% discount next time';
$lang['refund_maintenance'] = 'REFUND AND MAINTENANCE';
$lang['full_text'] = 'PRODUCT DETAIL';
//Blog
$lang['bg_all'] = 'ALL';
$lang['bg_empty'] = 'There is no article.';
$lang['bg_title'] = 'Blog';
//Blog detail
$lang['posted_on'] = 'Posted on';
$lang['prev_article'] = 'PREVIOUS POST';
$lang['nex_article'] = 'NEXT POST';
//Cart
$lang['my_bag'] = 'My bag';
$lang['product'] = 'Product';
$lang['quantity'] = 'Quantity';
$lang['price'] = 'Unit price';
$lang['order_summary'] = 'Order Summary';
$lang['delivery_address'] = 'Delivery address';
$lang['payment_method'] = 'Payment methods';
$lang['please_select'] = '--- Please select ---';
$lang['pay_on_receive'] = 'Pay on received';
$lang['complete_order'] = 'Complete order';
$lang['add_voucher_code'] = 'Add voucher code';
$lang['enter_code'] = 'Enter code';
$lang['add'] = 'Add';
$lang['voucher_applied'] = 'Voucher applied';
$lang['max_sale'] = 'Maximum sale';
$lang['min_order'] = 'Min order';
$lang['expire_date'] = 'Expire date';
$lang['rules'] = 'Description';
$lang['no_delivery1'] = 'There is no delivery address. Go to';
$lang['no_delivery2'] = 'to create now.';
$lang['transfer_fee'] = 'Transfer fee';
//Cart success
$lang['payment_success'] = 'PAYMENT SUCCESSFULL';
$lang['payment_des'] = 'Your payment for order successfull. We are working on it to deliver goods to you as soon as posible.';
$lang['continue_shopping'] = 'CONTINUE SHOPPING';
//Store
$lang['enter_store_name'] = 'Enter store name';
$lang['no_store'] = 'There is no store in this place.';
$lang['no_place'] = 'There is no store yet. Be back later';
$lang['open_until'] = 'Open from';
$lang['today'] = 'today';
//404
$lang['oops'] = 'Ooops';
$lang['cant_find_page'] = 'We can not find that page!';
$lang['404_des'] = 'Don’t worry – everything is working as usual, but the page you were trying to get to can’t be found and might have been moved or changed.';
$lang['404_home'] = 'GO TO HOME';
//login
$lang['lg_title'] = 'Sign in or create an account';
$lang['email'] = 'Type email';
$lang['continue'] = 'CONTINUE';
$lang['err_invalid_email'] = 'Email is invalid. Please type again.';
$lang['welcome_back'] = 'Welcome back';
$lang['please_type_password'] = ', please type password.';
$lang['reset_password'] = 'RESET PASSWORD';
$lang['firstname'] = 'Firstname';
$lang['lastname'] = 'Lastname';
$lang['password'] = 'Password';
$lang['err_require'] = 'This field is require.';
$lang['err_policy'] = 'Please accept Private Policy and Terms and Conditions.';
//My account
$lang['my_account_2'] = 'My account';
$lang['logout'] = 'Logout';
$lang['account_detail'] = 'Account detail';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Remove';
$lang['change_password'] = 'Change password';
$lang['address_book'] = 'Delivery address';
$lang['manager_address_book'] = 'Manager delivery address';
$lang['default_address_book'] = 'Default delivery address';
$lang['my_order'] = 'My orders';
$lang['manager_my_order'] = 'View all orders';
$lang['latest_orders'] = 'Here is your latest orders';
$lang['order_date'] = 'Order date';
$lang['status'] = 'Status';
$lang['payed'] = 'Payed';
$lang['hello'] = 'Hello';
$lang['no_default_address'] = 'You have no default address.';
$lang['no_order'] = 'You have no order.';
//Account edit
$lang['account_detail'] = 'Account detail';
$lang['title'] = 'TITLE';
$lang['enter_firstname'] = 'Enter firstname';
$lang['enter_lastname'] = 'Enter lastname';
$lang['new_password'] = 'NEW PASSWORD';
$lang['confirm_new_password'] = 'CONFIRM NEW PASSWORD';
$lang['save'] = 'SAVE';
//Address book
$lang['address_edit'] = 'Address edit';
$lang['additional_address'] = 'Additional address';
$lang['add_new_address'] = 'ADD NEW ADDRESS';
$lang['no_address'] = 'You have no address.';
$lang['country'] = 'COUNTRY';
$lang['province'] = 'PROVINCE / CITY';
$lang['district'] = 'DISTRICT';
$lang['ward'] = 'WARD';
$lang['address'] = 'Address';
$lang['enter_address'] = 'Enter address';
$lang['save_address'] = 'SAVE ADDRESS';
//My order
$lang['order_id'] = 'Order ID';
//Message
$lang['msg_email_reset_password_title'] = 'We have sent you an email with a link to reset your password.';
$lang['msg_email_reset_password_content'] = 'Reset your password now! <br />
Dont worry! Click on the link below to reset your password: <br />';
$lang['msg_email_reset_password_content_2'] = 'If you didnt request a password reset, feel free to delete this email and carry on shopping.';
$lang['msg_confirm_password_not_equal'] = 'Password and confirm password are not equal.';
$lang['msg_success_update'] = 'Your data have been saved.';
$lang['msg_invalid_voucher'] = 'Voucher code invalid. Place type again.';
$lang['msg_succes_add_to_cart'] = 'Product add into cart successfull.';
$lang['msg_update_cart'] = 'Product already added in cart. Update successfull.';
