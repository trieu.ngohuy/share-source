<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class DBHandle {

    var $_error_message = "Check Input parameters.";
    /*
     * Constructor
     */

    function Constructor() {
        //...
    }

    /*
     * Load database
     */

    function LoadDatabase($obj = null) {
        //Check input parameters
        if ($obj == null) {
            throw new Exception($this->_error_message);
        }
        //Check if database exist
        $db = $obj->load->database();
        if (!$db) {
            throw new Exception($obj->db->error());
        }
    }
}
