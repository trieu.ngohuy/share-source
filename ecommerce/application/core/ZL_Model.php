<?php

class Zl_model extends CI_Model {

    private $_error_message = "Check Input parameters.";
    private $_join_prefix = "tbl_";
    private $_object;
    private $_table;
    private $_active_language_id = 1;
    private $_list_multilanguage_table = '';
    private $_mode = '';

    /*
     * Constructor
     */

    function __construct($table = null, $object = null) {
        parent::__construct();
        if ($table == null || $object == null) {
            throw new Exception($this->_error_message);
        } else {
            //Prefix table name
            $this->_table = $this->db->dbprefix($table);
            //Protect identifiers
            $this->db->protect_identifiers($this->_table);
            //Set model object
            $this->_object = $object;

            $this->_list_multilanguage_table = $this->config->item('multi_language_list_table');

            $this->_mode = $this->session->userdata(SS_MODE);
            if($this->_mode == 'client'){
              $tmp = $this->session->userdata(SITE_LANG);
              $this->_active_language_id = $tmp['lang_id'];
            }else {
              $tmp = $this->session->userdata(SS_ADMIN_ACTIVE_LANG);
              $this->_active_language_id = $tmp['lang_id'];
            }

        }
    }
    /*
    * Valid images data
    */
    function valid_image_data($data, $type){
      //image
      if(!is_array($data)){
        if(!$this->is_file_exist($data)){
          $data = $this->fill_sample($data, $type);
        }else{
          $data = base_url() . $data;
        }
      }else{
          //list images
          foreach($data as &$value){
            if(!$this->is_file_exist($value)){
              $value = $this->fill_sample($value, $type);
            }else{
              $value = base_url() . $value;
            }
          }
          unset($value);
      }
      return $data;
    }
    /*
    * Fill sample image data
    */
    function fill_sample($data, $type){
      $pre = 'zl_';
      if($type == $pre . 'banner'){
        return base_url() . SAP_BANNER;
      }else if($type == $pre . 'product_category_med'){
        return base_url() . SAP_PROCAT_MED;
      }else if($type == $pre . 'product_category_big'){
        return base_url() . SAP_PROCAT_BIG;
      }else if($type == $pre . 'product_sld1'){
        return base_url() . SAP_PROCAT_SLD1;
      }else if($type == $pre . 'product_sld2'){
        return base_url() . SAP_PROCAT_SLD2;
      }else{
        return $data;
      }
    }
    /*
    *Check if file exist
    */
    function is_file_exist($src){
      if(file_exists($src)) {
        return true;
      }
      else {
        return false;
      }
    }
    /*
     * Format data
     */
    function format_data($arrData = array()) {
        foreach ($arrData as &$value) {
            $value = $this->format_data_object($value);
        }
        unset($value);
        return $arrData;
    }
    /*
     * Format object data
     */
    function format_data_object($objData) {
      //url
      $objData['url'] = $this->routes($objData);
      //Images
      $objData = $this->img($objData);
      //Created date
      if(isset($objData['created_date'])){
          $objData['created_date_number'] = $objData['created_date'];
          $objData['created_date'] = date(DATE_FORMAT, $objData['created_date']);
      }
      return $objData;
    }
    /*
    * Establish img path
    */
    function img($objData){
      //logo
      if (isset($objData['logo'])) {
        if($this->_mode === 'client'){
            $objData['logo'] = $this->valid_image_data($objData['logo'], $this->_table);
        }else{
          $objData['logo'] = base_url() . $objData['logo'];
        }

      }
      //big logo
      if (isset($objData['big_logo'])) {
        if($this->_mode === 'client'){
            $objData['big_logo'] = $this->valid_image_data($objData['big_logo'], $this->_table . '_big');
        }else{
            $objData['big_logo'] = base_url() . $objData['big_logo'];
        }
      }
      //medium logo
      if (isset($objData['medium_logo'])) {
        if($this->_mode === 'client'){
            $objData['medium_logo'] = $this->valid_image_data($objData['medium_logo'], $this->_table . '_med');
        }else{
            $objData['medium_logo'] = base_url() . $objData['medium_logo'];
        }

      }
      //slide1
      if (isset($objData['slide1'])) {
        $tmp = explode('||', $objData['slide1']);
        // foreach($tmp as &$value){
        //   $value = base_url() . $value;
        // }
        // unset($value);
        $objData['slide1'] = $this->valid_image_data($tmp, $this->_table . '_sld1');
      }
      //slide1
      if (isset($objData['slide2'])) {
        $tmp = explode('||', $objData['slide2']);
        // foreach($tmp as &$value){
        //   $value = base_url() . $value;
        // }
        // unset($value);
        $objData['slide2'] = $this->valid_image_data($tmp, $this->_table . '_sld2');
      }

      return $objData;
    }
    /*
     * Establish routes
     */

    function routes($objData) {
        //content
        if ($this->_table == 'zl_content') {
            return base_url() . URL_CONTENT . $objData['alias'];
        }
        //content category
        if ($this->_table == 'zl_content_category') {
            return base_url() . URL_CONTENTCAT . $objData['alias'];
        }
        //product
        if ($this->_table == 'zl_product') {
            return base_url() . URL_PRODUCT . $objData['alias'];
        }
        //product category
        if ($this->_table == 'zl_product_category') {
            return base_url() . URL_PRODUCTCAT . $objData['alias'];
        }
        //article
        if ($this->_table == 'zl_article') {
            return base_url() . URL_ARTICLE . $objData['alias'];
        }
        //store
        if ($this->_table == 'zl_store') {
            return base_url() . URL_STORE . '/' . $objData['alias'];
        }
        //lang
        if ($this->_table == 'zl_lang') {
            return base_url() . URL_SWITCH_LANG . $objData['code'];
        }
        //menu, banner
        if($this->_table == 'zl_menu' || $this->_table == 'zl_banner'){
          //product category
          if($objData['controller'] == 'product' && $objData['function'] == 'index'){
            return base_url() . URL_PRODUCTCAT . $objData['params'];
          }
          //product
          if($objData['controller'] == 'product' && $objData['function'] == 'detail'){
            return base_url() . URL_PRODUCT . $objData['params'];
          }
          //content category
          if($objData['controller'] == 'content' && $objData['function'] == 'index'){
            return base_url() . URL_CONTENTCAT . $objData['params'];
          }
          //content
          if($objData['controller'] == 'content' && $objData['function'] == 'detail'){
            return base_url() . URL_CONTENT . $objData['params'];
          }
          //my account
          if($objData['controller'] == 'access' && $objData['function'] == 'myaccount'){
            return base_url() . URL_MY_ACCOUNT;
          }
          //address book
          if($objData['controller'] == 'access' && $objData['function'] == 'addressbook'){
            return base_url() . URL_ADDRESS_BOOK;
          }
          //my orders
          if($objData['controller'] == 'access' && $objData['function'] == 'myorders'){
            return base_url() . URL_MY_ACCOUNT_ORDERS;
          }
          //store
          if($objData['controller'] == 'store' && $objData['function'] == 'index'){
            return base_url() . URL_STORE;
          }
        }
    }

    /*
     * Escape data array
     */

    function EscapeData($arrData = null) {
        if ($arrData == null) {
            throw new Exception($this->_error_message);
        } else {
            foreach ($arrData as $key => &$value) {
                //Escape value
                if (is_array($value)) {
                    $value = $this->EscapeData($value);
                } else {
                    $value = $this->db->escape_str($value);
                }
                //Escape key
                $tmpKey = $this->db->escape_str($key);
                $arrData[$tmpKey] = $arrData[$key];
            }
            unset($value);
            return $arrData;
        }
    }

    /*
     * Get single data
     */

    function get_single_data($arrCondition = array(), $arrJoin = null) {
        //Condition
        if (count($arrCondition) > 0) {
            //Current lang
            if($this->session->userdata(SS_MODE) == 'client' && strpos($this->_list_multilanguage_table, $this->_table) !== false){
              $arrCondition['lang_id'] = $this->_active_language_id;
            }

            //Escape queries
            $arrCondition = $this->EscapeData($arrCondition);
            $this->db->where($arrCondition);
        }
        //Join
        if ($arrJoin != null) {
            //Escape queries
            $arrJoin = $this->EscapeData($arrJoin);
            //Select from main table
            $this->db->from($this->_table . ' as ' . $this->_join_prefix . $this->_table);
            //Join
            foreach ($arrJoin as $value) {
                $this->db->join($value['strTable'], $value['strCondition']);
            }
        }
        //Query
        $objResult = $this->db->get($this->_table);
        //Handle errors
        if ($objResult) {
            //$objData = $objResult->custom_row_object(0, $this->_object);
            $objData = $objResult->row_array();
            if($objData){
                $objData = $this->format_data_object($objData);
            }
            return $objData ? $objData : false;
        } else {
            throw new Exception($this->db->error());
        }
    }

    /*
     * Get list data
     */

    function get_list_data($select = null, $arrCondition = null, $arrJoin = null, $count = null, $paging = null, $order_by = null) {
        //Set setlect
        if($select != null){
            $this->db->select($select);
        }

        //Current lang
        if( $this->_mode == 'client' && strpos($this->_list_multilanguage_table, $this->_table) !== false){
          $arrCondition['lang_id'] = $this->_active_language_id;
        }
        //Condition
        if ($arrCondition != null) {
            //Escape queries
            $arrCondition = $this->EscapeData($arrCondition);
            $this->db->where($arrCondition);
        }
        //Join
        if ($arrJoin != null) {
            //Escape queries
            $arrJoin = $this->EscapeData($arrJoin);
            //Select from main table
            $this->db->from($this->_table . ' as ' . $this->_join_prefix . $this->_table);
            //Join
            foreach ($arrJoin as $key => $value) {
                $this->db->join($key, $value);
            }
        }
        //Limit
        if ($count != null && $paging != null) {
            $this->db->limit($this->db->escape_str($count), $this->db->escape_str($paging));
        }
        //Order by
        if($order_by != null){
            $this->db->order_by($order_by[0] == null ? 'created_date' : $order_by[0],
            $order_by[1] == null ? 'asc' : $order_by[1]);
        }
        //Query
        $objResult = $this->db->get($this->_table);
        //Handle errors
        if ($objResult) {
            //$arrData = $objResult->custom_result_object($this->_object);
            $arrData = $objResult->result_array();
            $arrData = $this->format_data($arrData);
            return $arrData;
        } else {
            throw new Exception($this->db->error());
        }
    }

    /*
     * Insert new data
     */

    function insert_new_data($arrData = null) {
        if ($arrData == null) {
            throw new Exception($this->_error_message);
        } else {
          //Current lang
          // if($this->_mode === 'client' && strpos($this->_list_multilanguage_table, $this->_table) !== false){
          //   $arrData['lang_id'] = $this->_active_language_id;
          // }
            //Escape insert data
            $arrData = $this->EscapeData($arrData);
            $objResult = $this->db->insert($this->_table, $arrData);
            //Handle errors
            if (!$objResult) {
                throw new Exception($this->db->error());
            }else{
              return $this->db->insert_id();
            }
        }
    }
    /*
     * Update existing data
     */

    function update_existing_data($arrCondition = null, $arrData = null) {
        if ($arrData == null || $arrCondition == null) {
            throw new Exception($this->_error_message);
        } else {
          //Current lang
          // if($this->_mode == 'client' && strpos($this->_list_multilanguage_table, $this->_table) !== false){
          //   $arrCondition['lang_id'] = $this->_active_language_id;
          // }
            //Escape insert data
            $arrCondition = $this->EscapeData($arrCondition);
            $arrData = $this->EscapeData($arrData);
            //Insert data
            $this->db->where($arrCondition);
            $objResult = $this->db->update($this->_table, $arrData);
            //Handle errors
            if (!$objResult) {
                throw new Exception($this->db->error());
            }
        }
    }

    /*
     * Update existing data
     */

    function delete_data($arrCondition = null) {
        if ($arrCondition == null) {
            throw new Exception($this->_error_message);
        } else {
            //Escape insert data
            $arrCondition = $this->EscapeData($arrCondition);
            //Insert data
            $this->db->where($arrCondition);
            $objResult = $this->db->delete($this->_table);
            //Handle errors
            if (!$objResult) {
                throw new Exception($this->db->error());
            }
        }
    }

}
