<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class ZLBase_Controller extends CI_Controller{
  /*
   * Constuctor function
   */

  function __construct() {
      parent::__construct();
  }
  /*
  * Get homepage config data
  */
  function get_homepage_config($value){
    //Get homepage conig data
    $config = json_decode($value, true);
    foreach($config as $value){
      if($value['lang_id'] == $this->arr_view_data['active_lang']['lang_id']){
        $config = $value['config'];
        break;
      }
    }
    $arr = array();
    //Get top bannerfunction
    $arr['top_banner'] = array(
      'url' => $this->routes($config['top_banner']['controller'], $config['top_banner']['function'], $config['top_banner']['params']),
      'img' => base_url() . $config['top_banner']['img']
    );
    //Get list top cat
    $arr_products = array();
    foreach($config['top_productcats'] as $value){
      $arr_products[] = $this->productcat_model->get_single_data(array(
        'product_category_gid' => $value
      ));
    }
    $arr['top_productcats'] = $arr_products;
    //Get list bottom cat
    $arr_products = array();
    foreach($config['bottom_productcats'] as $value){
      $arr_products[] = $this->productcat_model->get_single_data(array(
        'product_category_gid' => $value
      ));
    }
    $arr['bottom_productcats'] = $arr_products;
    //Get bottom banner
    $arr['bottom_banner'] = array(
              'url' => $this->routes($config['bottom_banner']['controller'], $config['bottom_banner']['function'], $config['bottom_banner']['params']),
              'img' => base_url() . $config['bottom_banner']['img']
            );
    //Title
    $arr['title'] = $config['title'];
    //Description
    $arr['description'] = $config['description'];
    return $arr;
  }

  /*
  * Routes
  */
  function routes($controller, $function, $params){
    //product category
    if($controller == 'product' && $function == 'index'){
      return base_url() . URL_PRODUCTCAT . $params;
    }
    //product
    if($controller == 'product' && $function == 'detail'){
      return base_url() . URL_PRODUCT . $params;
    }
    //content category
    if($controller == 'content' && $function == 'index'){
      return base_url() . URL_CONTENTCAT . $params;
    }
    //content
    if($controller == 'content' && $function == 'detail'){
      return base_url() . URL_CONTENT . $params;
    }
    //access
    if($controller == 'access' && $function == 'myaccount'){
      return base_url() . URL_MY_ACCOUNT;
    }
    if($controller == 'access' && $function == 'addressbook'){
      return base_url() . URL_ADDRESS_BOOK;
    }
    if($controller == 'access' && $function == 'myorders'){
      return base_url() . URL_MY_ACCOUNT_ORDERS;
    }
  }
  /*
  * Heirarchicle array
  */
  function hierarchical_array($arr_data, $key, $parent_gid, $str = '', $is_hierarchical = false){
    $arr = array();
    foreach($arr_data as $value){
        if($value['parent_gid'] == $parent_gid){

          //Hierarchical title
          if($is_hierarchical){
            $value['detail']['vi']['dsp_title'] = $str . ' ' .$value['detail']['vi']['title'];
            $value['detail']['en']['dsp_title'] = $str . ' ' .$value['detail']['en']['title'];
          }else{
              $value['title'] = $str . ' ' .$value['title'];
          }

          $arr[] = $value;
          $arr = array_merge($arr, $this->hierarchical_array($arr_data, $key, $value[$key], $str . '---', $is_hierarchical));
        }
    }

    //Reverse
    // $tmp = array();
    // foreach(array_reverse($arr, true) as $value){
    //   $tmp[] = $value;
    // }
    return $arr;
  }
  /*
  * Cart progressing
  */
  function progress_cart_data($arr_data, $arr_return){

    //Load model
    $this->load->model('voucher_model');
    $this->load->model('product_model');
    $this->load->model('properties_model');

    $total = 0;
    $arr_return['products'] = array();
    foreach($arr_data['products'] as $value){
        $tmp = $this->product_model->get_single_data(array(
            'product_gid' => $value['product_gid']
        ));
        $tmp['count'] = $value['count'];
        $tmp['price'] = $this->format_price($tmp['price_vn']);
        $tmp['price_total'] = $this->format_price($tmp['price_vn'] * $value['count']);

        //Get color
        $color = $this->properties_model->get_single_data(array('product_properties_gid' => $value['color_gid']));
        $tmp['color'] = $color['title'];

        //Get material
        $color = $this->properties_model->get_single_data(array('product_properties_gid' => $value['material_gid']));
        $tmp['material'] = $color['title'];

        //Get size
        $color = $this->properties_model->get_single_data(array('product_properties_gid' => $value['size_gid']));
        $tmp['size'] = $color['title'];

        $arr_return['products'][] = $tmp;
        $total += $value['count'] * $tmp['price_vn'];
    }
    unset($value);
    $arr_return['total_original'] = $this->format_price($total);
    //Discount using voucher
    if(isset($arr_data['voucher_gid']) && $arr_data['voucher_gid'] != 0){
      $voucher = $this->voucher_model->get_single_data(array(
          'voucher_gid' => $arr_data['voucher_gid']
      ));
      $voucher['min_order'] = $this->format_price($voucher['min_order']);
      //$voucher['end_date'] = date(DATE_FORMAT, $voucher['end_date']);
      if($voucher['percent'] == 0){
          $voucher['price'] = $this->format_price($voucher['money']);
          $total = $total - $voucher['money'];
      }else{
          $voucher['price'] = $this->format_price(($total * $voucher['percent']) / 100);
          $total = $total - ($total * $voucher['percent']) / 100;
      }
      $arr_return['voucher'] = $voucher;
    }

    $arr_return['total_number'] = $total;
    $arr_return['total'] = $this->format_price($total);

    //Convert to usd for paypal
    if($this->arr_view_data['active_lang']['code'] != 'usd'){
      $arr_return['total_usd'] = $arr_return['total_number'] * $this->arr_view_data['active_lang']['rate_usd'];
    }

    //Get payment info
    $payment = array();
    if(isset($arr_data['payment_method'])){
      if($arr_data['payment_method'] == 1){
        $payment['method'] = 'Thanh toán khi nhận hàng';
      }else{
        $payment['method'] = 'Paypal';
      }

      //Order users
      if($arr_data['user_id'] === 0){
        $payment['address'] = $arr_data['delivery_address'];
        $payment['full_name'] = $arr_data['full_name'];
        $payment['phone'] = $arr_data['phone'];
      }else{
        $user = $this->user_model->get_single_data(array('user_id' => $arr_data['user_id']));
        $tmp = $this->get_delivery_address($user['address']);
        $payment['address'] = $tmp[$arr_data['delivery_address']]['address'];
        $payment['full_name'] = $user['firstname'] . ' ' . $user['lastname'];
        $payment['phone'] = $user['phone'];
      }
      $arr_return['payment'] = $payment;
    }

    return $arr_return;
  }
  /*
  * Get delivery address
  */
  function get_delivery_address($str = null){
    $this->load->model('places_model');
    //Get delivery address
    if($str === null){
        $delivery_address = json_decode(stripslashes($this->arr_view_data['login_data']['address']), true);
    }else{
      $delivery_address = json_decode(stripslashes($str), true);
    }

    $arr_address = array();
    if(count($delivery_address) > 0){
      foreach($delivery_address as $key => $value){
        $tmp = array();
        //Get country
        $country = $this->places_model->get_single_data(array('places_gid' => $value['country_gid']));
        //Get provinces
        $province = $this->places_model->get_single_data(array('places_gid' => $value['province_gid']));
        //Get district
        $district = $this->places_model->get_single_data(array('places_gid' => $value['district_gid']));
        //Get ward
        $ward = $this->places_model->get_single_data(array('places_gid' => $value['ward_gid']));

        array_push($arr_address, array(
          'key' => $key,
          'address' => $value['address'] . ', ' . $ward['title'] . ', ' . $district['title'] . ', ' . $province['title'] . ', ' . $country['title'],
          'add' => $value['address'],
          'ward' => $ward['title'],
          'ward_gid' => $ward['places_gid'],
          'district' => $district['title'],
          'district_gid' => $district['places_gid'],
          'provinces' => $province['title'],
          'province_gid' => $province['places_gid'],
          'country' => $country['title'],
          'country_gid' => $country['places_gid'],
        ));
      }
      unset($value);
    }

    return $arr_address;
  }
}

class ZLFront_Controller extends ZLBase_Controller {

    protected $arr_view_data = array();

    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Set working mode
        $this->session->set_userdata(SS_MODE, 'client');

        //Load model
        $this->load->model('product_model');
        $this->load->model('productcat_model');
        $this->load->model('setting_model');
        $this->load->model('properties_model');

        //Get login user
        $this->get_login_data();
        //Subcribes
        $this->subscribes();
        //Get active language
        $this->get_active_language();
        //Get header data
        $this->get_header_data();
        //Get footer data
        $this->get_footer_data();
        //Get setting data
        $this->get_setting_data();

        try {
            //Get login user
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
    * Valid link
    */
    function valid_link($data, $model, $alias, $key){
      //Get current link
      if($data == false){

        //Get link
        $lang = $this->arr_view_data['active_lang']['lang_id'];
        $this->session->set_userdata(SS_MODE,'switch_lang');

        //Get previous langdata
        $tmp = $this->$model->get_single_data(array(
          'alias' => $alias
        ));

        //Get current lang data
        $tmp = $this->$model->get_single_data(array(
          $key => $tmp[$key],
          'lang_id' => $lang
        ));

        $this->session->set_userdata(SS_MODE,'client');
        redirect($tmp['url'], 'refresh');
      }
    }
    /*
    * Get detault language translate
    */
    function get_default_translate(){
      $arr_translate = array();
      //Header
      $arr_translate['search'] = $this->lang->line('search');
      $arr_translate['my_account'] = $this->lang->line('my_account');
      $arr_translate['shopping_bag'] = $this->lang->line('shopping_bag');
      $arr_translate['items'] = $this->lang->line('items');
      $arr_translate['total'] = $this->lang->line('total');
      $arr_translate['checkout'] = $this->lang->line('checkout');
      $arr_translate['remove'] = $this->lang->line('remove');
      $arr_translate['search_des'] = $this->lang->line('search_des');
      $arr_translate['cart_quantity'] = $this->lang->line('cart_quantity');
      $arr_translate['empty'] = $this->lang->line('empty');
      $arr_translate['empty_des'] = $this->lang->line('empty_des');
      //Footer
      $arr_translate['follow_us'] = $this->lang->line('follow_us');
      $arr_translate['subs_title'] = $this->lang->line('subs_title');
      $arr_translate['subs_des'] = $this->lang->line('subs_des');
      $arr_translate['subs_agreement'] = $this->lang->line('subs_agreement');
      $arr_translate['subs_email'] = $this->lang->line('subs_email');
      $arr_translate['submit'] = $this->lang->line('submit');

      return $arr_translate;
    }
    /*
    * Get login data
    */
    public function get_login_data(){
      //Get login user
      $arr_login_user = $this->session->userdata(SS_LOGIN);
      $this->arr_view_data['login_data'] = $arr_login_user;
    }

    /*
    * Create breadcrumb
    */
    function create_breadcrumb($arr_data, $product_category_gid, $index = 0){
        $arr = array();
        foreach($arr_data as $value){
            if($value['product_category_gid'] == $product_category_gid){
                $arr[] = array(
                    'title' => $value['title'],
                    'url' => $value['url']
                );
                $arr = array_merge($arr, $this->create_breadcrumb($arr_data, $value['parent_gid'], $index+1));
            }
        }
        //Add home
        if($index == 0){
            $arr[] = array(
                'title' => $this->lang->line('home'),
                'url' => base_url()
            );
        }

        //Reverse
        $tmp = array();
        foreach(array_reverse($arr, true) as $value){
          $tmp[] = $value;
        }
        return $tmp;
    }
    /*
    * Create breadcrumb
    */
    function recursive_array($arr_data, $parent_gid, $index = 0){
        $arr = array();
        foreach($arr_data as $value){
            if($value['product_category_gid'] == $product_category_gid){
                $arr[] = array(
                    'title' => $value['title'],
                    'url' => $value['url']
                );
                $arr = array_merge($arr, $this->create_breadcrumb($arr_data, $value['parent_gid'], $index+1));
            }
        }
        //Add home
        if($index == 0){
            $arr[] = array(
                'title' => $this->lang->line('home'),
                'url' => base_url()
            );
        }

        //Reverse
        $tmp = array();
        foreach(array_reverse($arr, true) as $value){
          $tmp[] = $value;
        }
        return $tmp;
    }
    /*
    * Get setting data
    */
    function get_setting_data(){
      $setting = $this->setting_model->get_list_data();
      $arr_setting = array();
      foreach($setting as $value){
        //logo and favicon
        if($value['alias'] == 'logo' || $value['alias'] == 'favicon'){
          $value['value'] = base_url() . $value['value'];
        }
        //Homepage config
        if($value['alias'] == 'home_config'){
            $value['value'] = $value['value'];
        }
        //transfer fee
        if($value['alias'] == 'transfer_fee'){
            $value['value'] = $this->format_price($value['value']);
        }
        $arr_setting[$value['alias']] = array(
          'title' => $value['title'],
          'value' => $value['value']
        );
      }
      $this->arr_view_data['setting'] = $arr_setting;
    }
    /*
    * Get active language
    */
    public function get_active_language(){
      $this->load->model('lang_model');
      if($this->session->userdata(SITE_LANG) == null){
        $tmp = $this->lang_model->get_list_data();
        $this->session->set_userdata(SITE_LANG, $tmp[0]);
      }
      $tmp = $this->session->userdata(SITE_LANG);
      if($tmp['lang_id'] == 3 || $tmp['lang_id'] == 4){
        $tmp['lang_id'] = 2;
        $this->session->set_userdata(SITE_LANG, $tmp);
        //Go to homepage
        redirect(base_url(), 'refresh');
      }

      $this->arr_view_data['active_lang'] = $this->session->userdata(SITE_LANG);
    }
    /*
    * Get header data
    */
    function get_header_data(){
      //Get list lang
      $this->load->model('lang_model');
      $this->arr_view_data['lang'] = $this->lang_model->get_list_data();
      //Get cart
      $this->arr_view_data['cart'] = $this->GetCart();
      //Get menu
      $this->arr_view_data['header_menu'] = $this->get_menu();
    }
    /*
    * Create menu
    */
    function get_menu($type = 2){

      //Get list parent menu
      $this->load->model('menu_model');
      $header_menu = $this->menu_model->get_single_data(array(
        'parent_gid' => 0,
        'type' => $type
      ));
      $data = $this->menu_model->get_list_data(null, array(
        'parent_gid' => $header_menu['menu_gid']
      ));
      $arr_menu = array();

      //Loop parent menu
      foreach($data as $value){

        //Add parent title and url
        $tmp_menu = array(
          'url' => $value['url'],
          'title' => $value['title'],
          'menu_gid' => $value['menu_gid']
        );

        //Get list sub menu - header
        $tmp = $this->menu_model->get_list_data(null, array(
          'parent_gid' => $value['menu_gid'],
          'type' => 0
        ));

        //Loop sub menu - header
        $tmp_submenu = array();
        foreach($tmp as $value2){
          $tmp_submenu[] = array(
            'title' => $value2['title'],
            'menu_gid' => $value2['menu_gid'],
            'url' => $value2['url'],
            'detail' => $this->menu_model->get_list_data(null, array(
              'parent_gid' => $value2['menu_gid']
            ))
          );
        }
        $tmp_menu['detail'] = $tmp_submenu;

        //Get feature
        $tmp_feature = $this->menu_model->get_list_data(null, array(
          'parent_gid' => $value['menu_gid'],
          'type' => 1
        ));

        if(count($tmp_feature) > 0){
          $arr_feature = array();
          //Loop feature
          foreach($tmp_feature as $value3){
            if($value3['controller'] == 'product'){
              $arr_feature[] = $this->productcat_model->get_single_data(array(
                'alias' => $value3['params']
              ));
            }
          }
          $tmp_menu['feature'] = $arr_feature;
        }

        $arr_menu[] = $tmp_menu;
      }
      return $arr_menu;
    }
    /*
    * get footer data
    */
    function get_footer_data(){
      //Get menu
      $this->arr_view_data['footer_menu'] = $this->get_menu(3);
    }
    /*
    * Subscribed form submit handler
    */
    function subscribes(){
        $this->load->model('subscribes_model');
        if ($this->input->post('subscribes')) {
            $arr_post_data = $this->input->post('subscribes');
            //Check if email exist
            $tmp = $this->subscribes_model->get_single_data(array(
                'email' => $arr_post_data['email']
            ));
            if($tmp != false){
                $this->arr_view_data['subcribes_submit'] = array(
                    'status' => 0,
                    'message' => "You are subscribed already"
                );
            }else{
                //Insert subscribes email
                $tmp = $this->subscribes_model->insert_new_data(array(
                    'email' => $arr_post_data['email'],
                    'created_date' => strtotime(date(DATE_FORMAT))
                ));
                $this->arr_view_data['subcribes_submit'] = array(
                    'status' => 1,
                    'message' => "Success"
                );
            }
        }
    }
    /*
    * Get cart
    * array (product_gid => 1, count => 1)
    */
    function GetCart(){
        //Load model
        $this->load->model('voucher_model');
        //Get login session
        $cart = array();

        // $this->session->set_userdata(SS_CART_ORDER, array(
        //     'products' => array(
        //         array('product_gid' => 1, 'count' => 1),
        //         array('product_gid' => 2, 'count' => 2),
        //     ),
        //     'address_index' => 0,
        //     'payment' => array(
        //       'method' => 'Card',
        //       'created_date' => 1544482800,
        //       'token' => 'GWEWFEW34324RGREEREGEG32FW34FWG',
        //       'status' => 0, /*0 - pending, 1 - payed*/
        //       'card_info' => array(
        //         'card_holder' => 'NGO HUY TRIEU',
        //         'card_number' => 3242342342423434
        //       )
        //     )
        // ));
        if($this->session->userdata(SS_CART_ORDER) != null){
          $ss_cart = $this->session->userdata(SS_CART_ORDER);
          $cart = $this->progress_cart_data($ss_cart, $cart);
        }
        return $cart;
    }
    /*
    * Cart progressing
    */
    // function progress_cart_data($arr_data, $arr_return){
    //   $total = 0;
    //   $arr_return['products'] = array();
    //   foreach($arr_data['products'] as $value){
    //       $tmp = $this->product_model->get_single_data(array(
    //           'product_gid' => $value['product_gid']
    //       ));
    //       $tmp['count'] = $value['count'];
    //       $tmp['price'] = $this->format_price($tmp['price_vn']);
    //       $tmp['price_total'] = $this->format_price($tmp['price_vn'] * $value['count']);
    //
    //       //Get color
    //       $color = $this->properties_model->get_single_data(array('product_properties_gid' => $tmp['color_gid']));
    //       $tmp['color'] = $color['title'];
    //
    //       //Get material
    //       $color = $this->properties_model->get_single_data(array('product_properties_gid' => $tmp['material_gid']));
    //       $tmp['material'] = $color['title'];
    //
    //       //Get size
    //       $color = $this->properties_model->get_single_data(array('product_properties_gid' => $tmp['size_gid']));
    //       $tmp['size'] = $color['title'];
    //
    //       $arr_return['products'][] = $tmp;
    //       $total += $value['count'] * $tmp['price_vn'];
    //   }
    //   unset($value);
    //   $arr_return['total_original'] = $this->format_price($total);
    //   //Discount using voucher
    //   if(isset($arr_data['voucher_gid']) && $arr_data['voucher_gid'] != 0){
    //     $voucher = $this->voucher_model->get_single_data(array(
    //         'voucher_gid' => $arr_data['voucher_gid']
    //     ));
    //     $voucher['min_order'] = $this->format_price($voucher['min_order']);
    //     //$voucher['end_date'] = date(DATE_FORMAT, $voucher['end_date']);
    //     if($voucher['percent'] == 0){
    //         $voucher['price'] = $this->format_price($voucher['money']);
    //         $total = $total - $voucher['money'];
    //     }else{
    //         $voucher['price'] = $this->format_price(($total * $voucher['percent']) / 100);
    //         $total = $total - ($total * $voucher['percent']) / 100;
    //     }
    //     $arr_return['voucher'] = $voucher;
    //   }
    //
    //   $arr_return['total_number'] = $total;
    //   $arr_return['total'] = $this->format_price($total);
    //
    //   //Convert to usd for paypal
    //   if($this->arr_view_data['active_lang']['code'] != 'usd'){
    //     $arr_return['total_usd'] = $arr_return['total_number'] * $this->arr_view_data['active_lang']['rate_usd'];
    //   }
    //
    //   return $arr_return;
    // }
    /*
    * Convert int int price format
    */
    function format_price($price){
        $str_price = '';

        //Convert
        if($this->arr_view_data['active_lang']['code'] != 'vi'){
          $price = round($price * $this->arr_view_data['active_lang']['rate'], 0);
        }

        $tmp = $price;
        $price = str_split(strrev($price));
        foreach ($price as $key => $value)
        {
            if(($key + 1) % 3 == 0 && $key < (strlen($tmp) - 1)){
                $str_price .=$value . ',';
            }else{
                $str_price .=$value;
            }
        }
        unset($char);
        return strrev($str_price) . ' ' . $this->arr_view_data['active_lang']['currency'];
    }
}

class ZLAdmin_Controller extends ZLBase_Controller {

    protected $arr_view_data = array();

    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Set working mode
        $this->session->set_userdata(SS_MODE, 'admin');

        //Check if user login
        if (!$this->session->userdata(SS_ADMIN_LOGIN)) {
            redirect(URL_ADMIN_LOGIN, 'refresh');
        }
        //Get common data
        try {
            //Get login user
            $this->get_login_data();
            //Load model
            $this->load->model('setting_model');
            $config_data = $this->setting_model->get_list_data();
            $this->arr_view_data['setting'] = $config_data;
            //Get active lang
            $this->get_active_language();

        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    //Upload images
    function upload_images($key, $folder) {
        $data = array();

        //Is valid
        if (isset($_FILES[$key]) && $_FILES[$key]['name'] != '') {
          //Check file type
          if (($_FILES[$key]["type"] !== "image/png") &&
          ($_FILES[$key]["type"] !== "image/jpg") && ($_FILES[$key]["type"] !== "image/jpeg")){
            $data = array(
              'messsage' => 'Chỉ hỗ trợ định dạng: JPG,PNG,JPEG',
              'status' => 0
            );
          }
          //Check file size
          else if($_FILES[$key]["size"] <= 0){
            $data = array(
              'messsage' => 'Kích thước ảnh tối đa 2MB.',
              'status' => 0
            );
          }else {
            //Get login user data
            $arr_login_user = $this->session->userdata(SS_ADMIN_LOGIN);
            //Upload image
            $sourcePath = $_FILES[$key]['tmp_name']; // Storing source path of the file in a variable
            $targetPath = USER_UPLOAD_PATH . $arr_login_user['username'] . '/' . $folder . '/' . $_FILES[$key]['name']; // Target path where file is to be stored
            move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
            $data = array(
              'status' => 1,
              'path' => $targetPath
            );
          }
        }else{
          $data = array(
            'status' => 2, //Not progress
            'path' => ''
          );
        }

        //Return
        return $data;
    }
    /*
    * Get active lang
    */
    function get_active_language(){
      //Set active lang
      if($this->session->userdata(SS_ADMIN_ACTIVE_LANG) == null){
        $tmp = $this->lang_model->get_list_data();
        $this->session->set_userdata(SS_ADMIN_ACTIVE_LANG, $tmp[0]);
      }
      $this->arr_view_data['active_lang']  = $this->session->userdata(SS_ADMIN_ACTIVE_LANG);

    }
    /*
    * Get login data
    */
    public function get_login_data(){
      //Get login user
      $arr_login_user = $this->session->userdata(SS_ADMIN_LOGIN);
      $this->arr_view_data['login_data'] = $arr_login_user;
    }
    /*
     * Convert config string to array
     */
    function convert_to_array($arr_tmp) {
        if ($arr_tmp != '') {
            $arr_config = array();
            $arr_tmp = explode('||', $arr_tmp);
            foreach ($arr_tmp as $value) {
                $arr_value = explode(':', $value);
                $arr_config[$arr_value[0]] = $arr_value[1];
            }
            unset($value);
        }
        return $arr_config;
    }
    /*
    * Convert int int price format
    */
    function format_price($price){
        $str_price = '';

        //Get vi language
        $this->load->model('lang_model');
        $lang = $this->lang_model->get_single_data(array(
          'code' => 'vi'
        ));

        $tmp = $price;
        $price = str_split(strrev($price));
        foreach ($price as $key => $value)
        {
            if(($key + 1) % 3 == 0 && $key < (strlen($tmp) - 1)){
                $str_price .=$value . ',';
            }else{
                $str_price .=$value;
            }
        }
        unset($char);
        return strrev($str_price) . ' ' . $lang['currency'];
    }
    /*
    * format multilanguage array
    */
    function format_multilanguage_array($arr, $model, $key_field, $is_hierarchical = false){

      //get common field
      $data = array();
      foreach($arr as $value){
        $data[] = $value;
      }

      //Add multilang data
      foreach($data as &$value){
        //vi
        $vi = $this->$model->get_single_data(array(
          $key_field => $value[$key_field],
          'lang_id' => 1
        ));
        //en
        $en = $this->$model->get_single_data(array(
          $key_field => $value[$key_field],
          'lang_id' => 2
        ));
        //add
        $value['detail'] = array(
          'vi' => $vi,
          'en' => $en
        );
      }
      unset($value);

      //Hierarchical data
      if($is_hierarchical){
        $data = $this->hierarchical_array($data, $key_field, 0, '', true);
      }

      return $data;
    }
    /*
    * Get max value in array
    */
    function get_max_value($arr, $key){
      $max = 0;
      foreach($arr as $value){
        if($value[$key] > $max){
          $max = $value[$key];
        }
      }
      return $max;
    }
}
