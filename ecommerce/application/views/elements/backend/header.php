<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url() . URL_ADMIN_SETTING ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>QUẢN</b> LÝ</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php
                        if (isset($login_data['config']['logo'])) {
                            echo base_url() . $login_data['config']['logo'];
                        } else {
                            echo get_no_image();
                        }
                        ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo $login_data['firstname'] . " " . $login_data['lastname'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php
                            if (isset($login_data['config']['logo'])) {
                                echo base_url() . $login_data['config']['logo'];
                            } else {
                                echo get_no_image();
                            }
                            ?>" class="img-circle" alt="User Image">

                            <p><?php echo $login_data['firstname'] . " " . $login_data['lastname'] ?></p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">

                            </div>
                            <div class="pull-right">
                                <a href="<?php echo base_url() . URL_ADMIN_LOGOUT ?>" class="btn btn-default btn-flat">Đăng xuất</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
