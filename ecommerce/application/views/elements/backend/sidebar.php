<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">DANH SÁCH MENU</li>
            <li <?php if(!isset($active_menu) || $active_menu == 'setting')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_SETTING?>">
                    <i class="fa fa-cog"></i> <span>Cài đặt</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'banner')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_BANNER?>">
                    <i class="fa fa-sliders"></i> <span>Quản lý Banner</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'subscribes')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_SUBSCRIBES?>">
                    <i class="fa fa-inbox"></i> <span>Quản lý đăng ký</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'productcat')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_PRODUCTCAT?>">
                    <i class="fa fa-tasks"></i> <span>Quản lý Danh Mục Sản Phẩm</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'product')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_PRODUCT?>">
                    <i class="fa fa-legal"></i> <span>Quản lý Sản Phẩm</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'contentcat')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_BLOG?>">
                    <i class="fa fa-newspaper-o"></i> <span>Quản lý Blog</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'content')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_ARCTICLE?>">
                    <i class="fa fa-book"></i> <span>Quản lý Bài Viết</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'voucher')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_VOUCHER?>">
                    <i class="fa fa-pencil"></i> <span>Quản lý Voucher</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'cart')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_ORDER?>">
                    <i class="fa fa-list-ul"></i> <span>Quản lý Đơn Hàng</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'store')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_STORE?>">
                    <i class="fa fa-globe"></i> <span>Quản lý Địa điểm</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'store_detail')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_STORE_DETAIL?>">
                    <i class="fa fa-map"></i> <span>Quản lý cửa hàng</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'menu')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_MENU?>">
                    <i class="fa fa-tint"></i> <span>Quản lý Menu</span>
                </a>
            </li>
            <li <?php if(isset($active_menu) && $active_menu == 'user')
            echo 'class="active"';?>>
                <a href="<?php echo base_url() . URL_ADMIN_USER?>">
                    <i class="fa fa-users"></i> <span>Quản lý người dùng</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
