<div id="header">
  <!-- Top header -->
  <div class="row" id="top-header">
    <div class="col-md-2" id="top-left">
      <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          <img src="<?php echo $active_lang['logo']?>" height="30px"/>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
          <?php
            foreach($lang as $value){
              ?>
                <li <?php if($value['lang_id'] == $active_lang['lang_id']){echo 'class="active"';}?>><a href="<?php echo $value['url']?>"><img src="<?php echo $value['logo']?>" height="30px"/> <?php echo $value['title']?> (<?php echo $value['currency']?>)</a></li>
              <?php
            }
          ?>
        </ul>
        <a href="<?php echo base_url() . URL_STORE?>">
          <img src="<?php echo asset_front_url() . 'img/icon/placeholder.png'?>" height="20px"/>
        </a>
        <a href="#" id="mobile_menu_icon" v-on:click="show_mobile_menu()">
          <img src="<?php echo asset_front_url() . 'img/icon/menu.png'?>" height="20px"/>
        </a>
      </div>
    </div>
    <div class="col-md-7 al-center" id="logo-wrap">
      <a href="<?php echo base_url()?>">
        <img src="<?php echo asset_front_url() . 'img/logo.svg'?>" width="320px"/>
      </a>
    </div>
    <div class="col-md-3 al-right" id="top-right">
      <a href="#" v-on:click="show_search()">
        <img src="<?php echo asset_front_url() . 'img/icon/search.png'?>" height="15px"/>
         <?php echo $translate['search']?>
      </a>
      <a href="<?php echo base_url() . URL_MY_ACCOUNT?>">
        <img src="<?php echo asset_front_url() . 'img/icon/user.png'?>" height="15px"/>
        <?php echo $translate['my_account']?>
      </a>
      <a href="#" v-on:click="show_cart()" id="cart_click">
        <img src="<?php echo asset_front_url() . 'img/icon/cart.png'?>" height="15px"/>
         <span id="cart_dsp_count">
         {{cart_products_count}}
         </span>
      </a>
    </div>
  </div>
  <br>
  <!-- Menu -->
  <div class="row al-center" id="menu">
    <ul id="top-menu">
      <?php
        foreach($header_menu as $value){
          ?>
            <li <?php if(count($value['detail']) > 0 || (isset($value['feature']) && count($value['feature']) > 0)) {echo ' @mouseover="show_sub_menu('.$value['menu_gid'].')" @mouseleave="active_menu_item = 0"';}?>>
              <a href="<?php echo $value['url']?>"><?php echo $value['title']?></a>
              <?php if(count($value['detail']) > 0 || (isset($value['feature']) && count($value['feature']) > 0)){
                ?>
                  <transition name="fade">
                    <div class="sub-menu" v-if="active_menu_item == <?php echo $value['menu_gid']?>">
                      <div class="row">
                        <!-- Sub menu -->
                        <?php
                          $md_col = 0;
                          $lg_col = 0;
                          foreach($value['detail'] as $value2){
                            ?>
                              <div class="col-md-3 col-lg-2">
                                <p class="al-left"><b><?php echo $value2['title']?></b></p>
                                <ul>
                                  <?php
                                    foreach($value2['detail'] as $value3){
                                      ?>
                                        <li><a href="<?php echo $value3['url']?>"><?php echo $value3['title']?></a></li>
                                      <?php
                                    }
                                  ?>
                                </ul>
                              </div>
                            <?php
                            $md_col += 3;
                            $lg_col += 2;
                          }
                        ?>
                        <!-- Feature -->
                        <?php
                          if(isset($value['feature']) && count($value['feature']) > 0){

                            //Calculating div width
                            $sub_lg_col = 12 - $lg_col;
                            $sub_md_col = 12 - $md_col;
                            $child_lg_col = 3;
                            $child_md_col = 5;

                            if($sub_lg_col < 8){
                              if($sub_lg_col == 6){
                                $child_lg_col = 4;
                              }
                              if($sub_lg_col == 4){
                                $child_lg_col = 6;
                              }
                              if($sub_lg_col < 2){
                                $sub_lg_col = 2;
                                $child_lg_col = 12;
                              }
                            }else{
                              $sub_lg_col = 8;
                            }

                            if($sub_md_col < 6){
                              if($sub_md_col == 6){
                                $child_md_col = 5;
                              }
                              if($sub_md_col < 3){
                                $chil_md_col = 3;
                                $child_md_col = 12;
                              }
                            }else{
                              $sub_md_col = 6;
                            }

                            ?>
                            <div class="col-md-<?php echo $sub_md_col?> col-lg-<?php echo $sub_lg_col?>">
                              <div class="row">
                                <?php
                                  foreach($value['feature'] as $value2){
                                    ?>
                                      <div class="col-md-<?php echo $child_md_col?> col-lg-<?php echo $child_lg_col?>">
                                        <img src="<?php echo $value2['medium_logo']?>" class="wd-full" />
                                        <a href="<?php echo $value2['url']?>"><?php echo $value2['title']?></a>
                                      </div>
                                    <?php
                                  }
                                ?>
                              </div>
                            </div>
                            <?php
                          }
                        ?>
                      </div>
                    </div>
                  </transition>
                <?php
              }?>
            </li>
          <?php
        }
      ?>
    </ul>
  </div>
  <!-- Mobile menu -->
  <transition name="slide-fade">
    <div class="row" id="mobile-menu" v-if="mobile.is_display">
    <a href="#" id="close" v-on:click="show_mobile_menu()">
      <img src="<?php echo asset_front_url() . 'img/icon/cancel.png'?>" height="20px" />
    </a>
    <p >SHOP BY CATEGORY</p>
    <br>
    <ul>
      <?php
        foreach($header_menu as $value){
          ?>
            <li <?php if(count($value['detail']) > 0 || (isset($value['feature']) && count($value['feature']) > 0)) {echo ' @mouseover="show_mobile_sub_menu('.$value['menu_gid'].')" @mouseleave="mobile.active_item = 0"';}?>>
              <a href="<?php echo $value['url']?>"><?php echo $value['title']?></a>
              <?php if(count($value['detail']) > 0 || (isset($value['feature']) && count($value['feature']) > 0)) {
                ?>
                  <img class="fl-right" src="<?php echo asset_front_url() . 'img/icon/right-arrow.png'?>" height="15px"/>
                <?php
              }?>
              <?php if(count($value['detail']) > 0 || (isset($value['feature']) && count($value['feature']) > 0)){
                ?>
                <transition name="fade">
                  <div class="mobile-submenu" v-if="mobile.active_item == <?php echo $value['menu_gid'];?>">
                    <p>FEATURE</p>
                    <br>
                    <?php
                      if(isset($value['feature']) && count($value['feature']) > 0){
                        ?>
                          <div class="feature-wrap">
                            <?php
                              foreach($value['feature'] as $value2){
                                ?>
                                  <div class="feature">
                                    <div class="img-div" style="background-image: url('<?php echo $value2['medium_logo']?>')">
                                    </div>
                                    <a href="<?php echo $value2['url']?>"><?php echo $value2['title']?></a>
                                  </div>
                                <?php
                              }
                            ?>
                          </div>
                          <br>
                        <?php
                      }
                    ?>
                    <ul>
                      <?php
                        foreach($value['detail'] as $value2){
                          ?>
                            <li <?php if(count($value2['detail']) > 0){echo '@mouseover="show_mobile_sub2_menu('.$value2['menu_gid'].')" @mouseleave="mobile.active_sub_item = 0"';}?>>
                              <a href="#"><?php echo $value2['title']?></a>
                              <?php
                                if(count($value2['detail']) > 0){
                                    ?>
                                      <img class="fl-right" src="<?php echo asset_front_url() . 'img/icon/down-arrow.png'?>" height="15px"/>
                                      <transition name="fade">
                                        <ul class="mobile-sub2menu" v-if="mobile.active_sub_item == <?php echo $value2['menu_gid']?>">
                                          <?php
                                            foreach($value2['detail'] as $value3){
                                              ?>
                                                <li><a href="<?php echo $value3['url']?>"><?php echo $value3['title']?></a></li>
                                              <?php
                                            }
                                          ?>
                                        </ul>
                                      </transition>
                                    <?php
                                }
                              ?>

                            </li>
                          <?php
                        }
                      ?>
                    </ul>
                  </div>
                </transition>
                <?php
              }
              ?>
            </li>
          <?php
        }
      ?>
    </ul>
  </div>
  </transition>
  <!-- Search form -->
  <br>
  <transition name="bounce">
    <div class="row" id="search-wrap" v-if="display_search">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <form method="post" action="<?php echo base_url() . URL_SEARCH?>">
          <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="<?php echo $translate['search_des']?>" name="search">
            <div class="input-group-append">
              <img src="<?php echo asset_front_url() . 'img/icon/search.png'?>" height="20px"/>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-2"></div>
    </div>
  </transition>
  <!-- Cart -->
  <transition name="slide-fade">
    <div id="cart-wrap" v-if="display_cart" v-html="cart_html">
      
    </div>
  </transition>
</div>

<!-- Header Vuejs -->
<script>
  var url_basket = '<?php echo base_url(). URL_CART?>';
  <?php
    $js_array = json_encode($cart);
    echo "var arr_cart = " . $js_array . ";\n";
  ?>
  var url_basket = '<?php echo base_url() . URL_CART?>';
  var arr_translate = {
    'shopping_bag' : '<?php echo $translate['shopping_bag']?>',
    'items' : '<?php echo $translate['items']?>',
    'cart_quantity' : '<?php echo $translate['cart_quantity']?>',
    'total' : '<?php echo $translate['total']?>',
    'checkout' : '<?php echo $translate['checkout']?>',
    'empty_des' : '<?php echo $translate['empty_des']?>'
  }
</script>
<script type="text/javascript" src="<?php echo asset_front_url() ?>js/vuejs/vue.header.js"></script>
