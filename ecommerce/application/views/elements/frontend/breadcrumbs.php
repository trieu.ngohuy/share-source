<ul id="breadcrumb">
  <?php
    foreach($breadcrumb as $key => $value){
      ?>
        <li><a href="<?php echo $value['url']?>"><?php echo $value['title']?></a></li>
        <?php
          if($key < count($breadcrumb) - 1){
            ?>
                <li><img src="<?php echo asset_front_url() . 'img/icon/right-arrow.png'?>" width="10px"/></li>
            <?php
          }
        ?>
      <?php
    }
  ?>
</ul>
