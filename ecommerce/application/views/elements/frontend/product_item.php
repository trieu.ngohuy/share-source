<a href="<?php echo $product_item['url']?>">
  <img src="<?php echo $product_item['logo']?>" class="d-block w-100" alt="...">
</a>
<div class="al-center">
  <br>
  <a href="<?php echo $product_item['url']?>" class="underline"><?php echo mb_strtoupper($product_item['title'])?></a>
  <p><?php echo $product_item['price']?></p>
</div>
