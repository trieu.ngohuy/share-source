<div class="bd-example">
  <div id="productcat_slide-<?php echo $product_slide['key'] ?>" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <?php
        foreach($product_slide['data'] as $key => $value){
          if($key == 0 || ($key > 0 && $key % 6 == 0)){
          ?>
            <li data-target="#productcat_slide-<?php echo $product_slide['key'] ?>" data-slide-to="<?php echo $key?>" <?php if($key == 0){ echo 'class="active"';}?>></li>
          <?php
          }
        }
      ?>
    </ol>
    <div class="carousel-inner">
      <?php
        foreach($product_slide['data'] as $key => $value){
          if($key == 0){
            ?>
              <div class="carousel-item <?php if($key == 0){ echo 'active';}?>">
                <div class="row">
            <?php
          }
          ?>
                <div class="col-md-2 col-lg-2">
                  <?php
                  $product_item['product_item'] = $value;
                  $this->load->view('elements/frontend/product_item', $product_item); ?>
                </div>
          <?php
          if(($key + 1) % 6 == 0 || $key == count($product_slide['data']) - 1){
            ?>
                </div>
              </div>
            <?php
          }
          if(($key + 1) % 6 == 0 && $key < count($product_slide['data']) - 1){
            ?>
            <div class="carousel-item">
              <div class="row">
            <?php
          }
        }
      ?>
    </div>
    <a class="carousel-control-prev" href="#productcat_slide-<?php echo $product_slide['key'] ?>" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#productcat_slide-<?php echo $product_slide['key'] ?>" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<script>
  $('.bd-example').carousel({
      interval: 5000
  });
</script>
