<div id="footer">
  <div class="row">
    <?php
      foreach($footer_menu as $value){
        ?>
        <div class="col-md-3 col-lg-2">
          <p class="title"><?php echo $value['title']?></p>
          <ul>
            <?php
              foreach($value['detail'] as $value2){
                  ?>
                    <li><a href="<?php echo $value2['url']?>"><?php echo $value2['title']?></a></li>
                  <?php
              }
            ?>
          </ul>
        </div>
        <?php
      }
    ?>
    <div class="col-md-3 col-lg-3 social">
      <p class="title"><?php echo $translate['follow_us']?></p>
      <ul>
        <li><a href="<?php echo $setting['facebook']['value']?>"><img src="<?php echo asset_front_url() . 'img/icon/fb.png'?>" height="25px"/></a></li>
        <li><a href="<?php echo $setting['twitter']['value']?>"><img src="<?php echo asset_front_url() . 'img/icon/tw.png'?>" height="25px"/></a></li>
        <li><a href="<?php echo $setting['instagram']['value']?>"><img src="<?php echo asset_front_url() . 'img/icon/in.png'?>" height="25px"/></a></li>
        <li><a href="<?php echo $setting['pinterest']['value']?>"><img src="<?php echo asset_front_url() . 'img/icon/pi.png'?>" height="25px"/></a></li>
      </ul>
    </div>
    <div class="col-lg-3" id="subscribes">
      <p class="title"><?php echo $translate['subs_title']?></p>
      <form method="post" id="frmSubscribes">
        <?php
          $color = 'red';
          if(isset($subcribes_submit)){
            if($subcribes_submit['status'] == 1){
              ?>
                <div class="alert alert-success" role="alert"><?php echo $subcribes_submit['message']?></div>
              <?php
            }else{
              ?>
                <div class="alert alert-danger" role="alert"><?php echo $subcribes_submit['message']?></div>
              <?php
            }
          }
        ?>
        <div class="form-group">
          <label for="exampleInputPassword1"><?php echo $translate['subs_des']?></label>
          <input type="email" name="subscribes[email]" class="form-control" id="exampleInputPassword1" placeholder="<?php echo $translate['subs_email']?>" required>
        </div>
        <div class="form-check">
          <input type="checkbox" name="subscribes[policy]" class="form-check-input" id="exampleCheck1" required>
          <label class="form-check-label" for="exampleCheck1">
            <?php echo $translate['subs_agreement']?>
          </label>
        </div>
        <br>
        <button type="submit" class="btn btn-primary"><?php echo $translate['submit']?></button>
      </form>
    </div>
  </div>
  <hr>
  <div id="copyright" class="al-center">
    <p><?php echo $setting['footer_note']['value']?></p>
  </div>
</div>
