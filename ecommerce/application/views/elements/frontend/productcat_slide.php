<div class="bd-example">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <?php
        foreach($productcat_slide as $key => $value){
          if($key == 0 || ($key > 0 && $key % 4 == 0)){
          ?>
            <li data-target="#carouselExampleCaptions" data-slide-to="<?php echo $key?>" <?php if($key == 0){ echo 'class="active"';}?>></li>
          <?php
          }
        }
      ?>
    </ol>
    <div class="carousel-inner">
      <?php
        foreach($productcat_slide as $key => $value){
          if($key == 0 || ($key + 1) % 5 == 0){
            ?>
              <div class="carousel-item <?php if($key == 0){ echo 'active';}?>">
                <div class="row">
            <?php
          }
          ?>
                <div class="col-md-3 col-lg-3">
                  <a href="<?php echo $value['url']?>">
                    <img src="<?php echo $value['medium_logo']?>" class="d-block w-100" alt="...">
                  </a>
                  <!-- <div class="carousel-caption d-none d-md-block">
                    <h5>First slide label</h5>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                  </div> -->
                </div>
          <?php
          if(($key + 1) % 4 == 0 || $key == count($productcat_slide) - 1){
            ?>
                </div>
              </div>
            <?php
          }
        }
      ?>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<script>
  $('.bd-example').carousel({
      interval: 5000
  });
</script>
