<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quản lý Đăng ký
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header al-right">
                    <button type="button" id="btn_new" class="btn btn-primary">Thêm mới</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Email</th>
                                <th>Ngày đăng ký</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($data as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $value['email']; ?></td>
                                    <td><?php echo $value['created_date']; ?></td>
                                    <td>
                                        <a href="#" class="btn-edit" data-index="<?php echo $count - 1; ?>"><i class="fa fa-edit"></i> Sửa</a>
                                        <a href="#" class="btn-remove" data-index="<?php echo $count - 1; ?>"><i class="fa fa-remove"></i> Xóa</a>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>#</th>
                              <th>Email</th>
                              <th>Ngày đăng ký</th>
                              <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade" id="modal-data">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Default Modal</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <label>Email</label>
                <input id="email" name="data[email]" type="email" class="form-control" placeholder="Email">
            </div>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="btn-save">Lưu</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Style -->
<style type="text/css">
    .select2-container .select2-selection--single {
        height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 31px;
    }
    table .mul{
      padding-top: 15px;
    }
    table hr{
      margin: 0px;
    }
</style>
<!-- page script -->
<script>
    var mode = 'new';
    var key = 0;
    var key_str = 'subscribes_id';
    var url_update = "<?php echo base_url() ?>AdminSubscribes/execute_query";
    var url_delete = "<?php echo base_url() ?>AdminSubscribes/delete";
    var url_refresh = "<?php echo base_url() ?>AdminSubscribes/get_data";
    $(function () {
        //Initialize datatable
        $('#example1').DataTable();
    });
    //Convert php array to js array
    <?php
    $js_array = json_encode($data);
    echo "var arr_data = " . $js_array . ";\n";
    ?>

    $(document).ready(function () {

        //Click new button
        $(document).on('click', '#btn_new', function (e) {
            //Set mode and key value
            mode = 'new';
            key = 0;
            //Change modal title
            $('.modal-title').html('Thêm mới');

            /*Set modal custom data*/
            var data = {
              'email' : ''
            };
            update_modal_data(data);
            /*End*/

            //Open modal
            $("#modal-data").modal();
        });
        //Click edit button
        $(document).on('click', '.btn-edit', function (e) {
            //Set mode and key value
            mode = 'edit';
            var index = $(this).attr('data-index');
            var obj_data = arr_data[index];
            key = obj_data[key_str];
            //Change modal title
            $('.modal-title').html('Chỉnh sửa');

            /*Set modal custom data*/
            var data = {
              'email' : obj_data['email']
            };
            update_modal_data(data);
            /*End*/

            //Open modal
            $("#modal-data").modal();
        });
        //Click remove button
        $(document).on('click', '.btn-remove', function (e) {
            if (confirm("Are you sure you want to delete this?")) {
                //Get id
                var index = $(this).attr('data-index');
                //Get edit data
                var obj_data = arr_data[index];
                //Project id
                key = obj_data[key_str];

                //Create post data
                var fd = new FormData();
                fd.append(key_str, key);

                $.ajax({
                    type: 'POST',
                    url: url_delete,
                    data: fd,
                    processData: false,
                    contentType: false,
                    cache:false,
                    enctype: 'multipart/form-data',
                    success: function (objData) {
                        //Refresh data
                        refresh_data();
                    },
                    error: function (jqXHR, exception) {
                        alert('Có lỗi. Thử lại sau.');
                    }
                });
            }
        });
        //Modal save button click
        $(document).on('click', '#btn-save', function (e) {

            //If add new then check if duplicate alias
            if (verify_data() === false) {
                return;
            }
            //Hide modal
            $('#modal-data').modal('toggle');

            //Ajax
            $.ajax({
                type: 'POST',
                url: url_update,
                data: get_update_data(),
                processData: false,
                contentType: false,
                cache:false,
                enctype: 'multipart/form-data',
                success: function (objData) {
                  if(objData !== ''){
                    objData = $.parseJSON(objData);
                    alert(objData["messsage"]);
                  }else{
                    //Refresh data
                    refresh_data();
                  }
                },
                error: function (jqXHR, exception) {
                    alert('Có lỗi. Thử lại sau.');
                }
            });
        });
    });
    //Update modal data
    function update_modal_data(data){
      //common
      var tmp = data;
      $('#email').val(tmp['email']);
    }
    //Verify data
    function verify_data() {
        //Check empty input
        if ($('#email').val() === '') {
            alert('Email không được rỗng!');
            return false;
        }
    }
    //Get data
    function refresh_data() {
        //Refresh data
        $.ajax({
            type: 'GET',
            url: url_refresh,
            success: function (data) {
                data = $.parseJSON(data);
                arr_data = data;
                //Fill datatable
                $('#example1').DataTable().destroy();
                $('#example1 tbody').html('');
                var html = '';
                var detail = [];
                var val = '';
                for (var i = 0; i < data.length; i++) {
                    html += '<tr>'
                            + '<td>' + (i + 1) + '</td>'
                            + '<td>' + data[i]['email'] + '</td>'
                            + '<td>' + data[i]['created_date'] + '</td>'
                    html += '<td>'
                            + '<a href="#" class="btn-edit" data-index="' + i + '"><i class="fa fa-edit"></i> Sửa</a>'
                            + ' <a href="#" class="btn-remove" data-index="' + i + '"><i class="fa fa-remove"></i> Xóa</a>'
                            + '</td>'
                            + '</tr>';
                }
                $('#example1 tbody').html(html);
                //Initialize datatable
                $('#example1').DataTable().draw();

            },
            error: function () {
                alert('Có lỗi. Thử lại sau.');
            }
        });
    }
    //Get update data
    function get_update_data(){
      var fd = new FormData();

      //Common
      fd.append('email', $('#email').val());
      fd.append(key_str, key);

      return fd;
    }
</script>
