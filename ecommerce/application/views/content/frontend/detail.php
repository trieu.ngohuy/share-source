<div class="container-fluid" id="content-detail-wrap">
  <h4 class="al-center"><?php echo $data['title']?></h4>
  <br>
  <p class="al-center"><?php echo $translate['posted_on']?> <?php echo $data['created_date']?></p>
  <br><br>
  <!-- <div class="row">
    <div class="col-md-1 col-lg-2"></div>
    <div class="col-md-1 col-lg-8"><img src="<?php echo $data['logo']?>" width="100%"/></div>
    <div class="col-md-1 col-lg-2"></div>
  </div>
  <br> -->
  <div class="row">
    <div class="col-md-1 col-lg-2"></div>
    <div class="col-md-1 col-lg-8"><?php echo $data['full_text']?></div>
    <div class="col-md-1 col-lg-2"></div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-3">
        <a href="<?php echo $previous_link?>" class="wd-full btn btn-success btn-custom btn-cus-success"><?php echo $translate['prev_article']?></a>
    </div>
    <div class="col-md-4 al-center">
      <div class="social content-detail-social wd-full">
        <p><?php echo $translate['share']?></p>
        <ul class="nav justify-content-center" id="ul-social">
				  <li class="nav-item">
				    <a href="<?php echo $setting['facebook']['value']?>"><img src="<?php echo asset_front_url() . 'img/icon/fb.png'?>" height="25px"/></a>
				  </li>
				  <li class="nav-item">
				    <a href="<?php echo $setting['twitter']['value']?>"><img src="<?php echo asset_front_url() . 'img/icon/tw.png'?>" height="25px"/></a>
				  </li>
				  <li class="nav-item">
				    <a href="<?php echo $setting['instagram']['value']?>"><img src="<?php echo asset_front_url() . 'img/icon/in.png'?>" height="25px"/></a>
				  </li>
					<li class="nav-item">
				    <a href="<?php echo $setting['pinterest']['value']?>"><img src="<?php echo asset_front_url() . 'img/icon/pi.png'?>" height="25px"/></a>
				  </li>
				</ul>
      </div>
    </div>
    <div class="col-md-3">
        <a href="<?php echo $next_link?>" class="wd-full btn btn-success btn-custom btn-cus-success"><?php echo $translate['nex_article']?></a>
    </div>
    <div class="col-md-1"></div>
  </div>
</div>
<br><br>
