<div class="container-fluid" id="content-index-wrap">
  <h4 class="al-center"><?php echo $translate['bg_title']?></h4>
  <br>
  <!--List category-->
  <div id="menu" class="row al-center">
    <ul id="top-menu">
      <?php
        foreach($category as $value){
          ?>
          <li>
            <a href="<?php echo $value['url']?>"><?php echo mb_strtoupper($value['title'])?></a>
          </li>
          <?php
        }
      ?>
    </ul>
  </div>
  <br><br>
  <!-- List contents -->
  <div class="row" id="detail">
    <?php
    if(count($data) > 0){
      foreach($data as $key => $value){
        ?>
        <div class="col-md-4 col-lg-4 blog-item" @mouseover="show_item(<?php echo $key + 1?>)" @mouseleave="active_item = 0">
          <div class="img" style="background-image: url(<?php echo $value['logo']?>)">
          </div>
          <div class="text" v-if="active_item != <?php echo $key + 1?>">
            <h5 class="title"><a href="<?php echo $value['url']?>"><?php echo strtoupper($value['title'])?></a></h5>
          </div>
          <transition name="fade">
            <div class="text text2" v-if="active_item == <?php echo $key + 1?>">
              <h5 class="title"><a href="<?php echo $value['url']?>"><?php echo strtoupper($value['title'])?></a></h5>
              <p class="description"><?php echo strtoupper($value['intro_text'])?></p>
            </div>
          </transition>
        </div>
        <?php
      }
    }else{
      ?>
        <p class="al-center wd-full"><?php echo $translate['bg_empty']?></p>
      <?php
    }
    ?>
  </div>
  <br>
  <br>
</div>
<script type="text/javascript" src="<?php echo asset_front_url() ?>js/vuejs/vue.blog.js"></script>
