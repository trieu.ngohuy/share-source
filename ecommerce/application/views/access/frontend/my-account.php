<br>
<div class="container-fluid" id="my-account-wrap">
  <div class="row">
    <div class="col-md-3 col-lg-3" id="left">
      <ul>
        <li <?php if($active == 'account'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT?>"><?php echo $translate['my_account_2']?></a></li>
        <li <?php if($active == 'edit'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT_EDIT?>"><?php echo $translate['account_detail']?></a></li>
        <li <?php if($active == 'address'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_ADDRESS_BOOK?>"><?php echo $translate['address_book']?></a></li>
        <li <?php if($active == 'orders'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT_ORDERS?>"><?php echo $translate['my_order']?></a></li>
        <li class="last"><a href="<?php echo base_url() . 'account/logout'?>"><?php echo $translate['logout']?></a></li>
      </ul>
    </div>
    <div class="col-md-9 col-lg-9" id="right">
      <h3><?php echo $translate['hello']?> <?php echo $login_data['lastname']?></h3>
      <br>
      <!--Infomation-->
      <div class="row">
        <div class="col-md-8 col-lg-8"><h5><?php echo $translate['account_detail']?></h5></div>
        <div class="col-md-4 col-lg-4 al-right">
          <a href="<?php echo base_url() . URL_MY_ACCOUNT_EDIT?>" class="underline"><b><?php echo $translate['edit']?> | </b></a>
          <a href="<?php echo base_url() . URL_MY_ACCOUNT_EDIT?>" class="underline"><b><?php echo $translate['change_password']?></b></a>
        </div>
      </div>
      <p><?php echo $login_data['firstname'] . ' ' . $login_data['lastname']?> - <?php echo $login_data['email']?></p>
      <hr>
      <br>
      <!-- Address -->
      <div class="row">
        <div class="col-md-8 col-lg-8"><h5><?php echo $translate['address_book']?></h5></div>
        <div class="col-md-4 col-lg-4 al-right">
          <a href="<?php echo base_url() . URL_ADDRESS_BOOK?>" class="underline"><b><?php echo $translate['manager_address_book']?></b></a>
        </div>
      </div>
      <br>
      <?php
        if(count($delivery_address) <= 0){
          ?>
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <p><?php echo $translate['no_default_address']?></p>
            </div>
          </div>
          <?php
        }else{
          ?>
          <div class="row">
            <div class="col-md-6 col-lg-6">
              <div id="default-address">
                <p><b><?php echo $translate['default_address_book']?></b></p>
                <div class="row">
                  <div class="col-md-8 col-lg-8"><p><b><?php echo $login_data['firstname'] . ' ' . $login_data['lastname']?></b></p></div>
                  <div class="col-md-4 col-lg-4 al-right">
                    <a href="<?php echo base_url() . URL_ADDRESS_BOOK . '0'?>" class="underline"><?php echo $translate['edit']?> | </a>
                    <a href="#" class="underline" v-on:click="delete_address(0)"><?php echo $translate['delete']?></a>
                  </div>
                </div>
                <p><?php echo $delivery_address['add']?></p>
                <p><?php echo $delivery_address['ward']?></p>
                <p><?php echo $delivery_address['district']?></p>
                <p><?php echo $delivery_address['provinces']?></p>
                <p><?php echo $delivery_address['country']?></p>
              </div>
            </div>
          </div>
          <?php
        }
      ?>
      <hr>
      <br>
      <!-- Orders -->
      <div class="row">
        <div class="col-md-8 col-lg-8"><h5><?php echo $translate['my_order']?></h5></div>
        <div class="col-md-4 col-lg-4 al-right">
          <a href="<?php echo base_url() . URL_MYORDERS?>" class="underline"><b><?php echo $translate['manager_my_order']?></b></a>
        </div>
      </div>
      <?php
        if(count($orders) <= 0){
          ?>
          <!-- Empty -->
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <br>
              <p><?php echo $translate['no_order']?></p>
              <br>
              <a href="<?php echo base_url()?>" class="btn btn-success btn-custom"><?php echo $translate['continue_shopping']?></a>
            </div>
          </div>
          <?php
        }else{
          ?>
          <!-- Last orders -->
          <div id="cart-index-wrap">
            <div class="row">
              <div class="col-md-12 col-lg-12">
                <br>
                <p><?php echo $translate['latest_orders']?>:</p>
                <hr>
                <p><b><?php echo $translate['order_date']?>: </b> <?php echo $orders['created_date']?></p>
                <p><b><?php echo $translate['status']?>: </b> <?php echo $orders['status_text']?></p>
                <br>
              </div>
              <div class="col-md-12 col-lg-12" id="data">
                <table class="table">
                  <thead>
                      <tr>
                          <th scope="col" colspan="2"><?php echo $translate['product']?></th>
                          <th scope="col" width="30px" class="al-center"><?php echo $translate['quantity']?></th>
                          <th scope="col" width="120px" class="al-center"><?php echo $translate['price']?></th>
                          <th scope="col" width="120px" class="al-center"><?php echo $translate['total']?></th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php
                      foreach($orders['products'] as $key => $value){
                        ?>
                        <tr <?php if($key == 0){echo 'class="first"';}?>>
                          <th><a href="<?php echo $value['url']?>"><img src="<?php echo $value['logo']?>" width="70px"/></a></th>
                          <th>
                            <a href="<?php echo $value['url']?>"><?php echo $value['title']?></a>
                            <p><?php echo $translate['material']?>: <?php echo $value['material']?></p>
                            <p><?php echo $translate['color']?>: <?php echo $value['color']?></p>
                            <p><?php echo $translate['size']?>: <?php echo $value['size']?></p>
                          </th>
                          <th class="al-center"><?php echo $value['count']?></th>
                          <th class="al-center"><?php echo $value['price']?></th>
                          <th class="al-center"><?php echo $value['price_total']?></th>
                        </tr>
                        <?php
                      }
                    ?>
                  </tbody>
              </table>
              </div>
            </div>
            <div class="row" id="right">
              <div class="col-md-12 col-lg-12">
                <h5 class="title"><?php echo $translate['order_summary']?></h5>
                <div id="cart-summary">
                  <br>
                  <div class="row">
                    <div class="col-md-6 col-lg-6"><b><?php echo $translate['total']?> (<?php echo count($orders['products'])?>)</b></div>
                    <div class="col-md-6 col-lg-6 al-right"><?php echo $orders['total_original']?></div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-6 col-lg-6"><b>Voucher</b></div>
                    <div class="col-md-6 col-lg-6 al-right"><?php echo $orders['voucher']['price']?></div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-md-6 col-lg-6"><b><?php echo $translate['total']?> (<?php echo count($orders['products'])?>)</b></div>
                    <div class="col-md-6 col-lg-6 al-right"><?php echo $orders['total']?></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
      ?>

      <br>
    </div>
  </div>
</div>
<br><br>
<script type="text/javascript">
    var url_delete = '<?php echo base_url(). 'Access/Delete'?>';
</script>
<script type="text/javascript" src="<?php echo asset_front_url() ?>js/vuejs/vue.addressbook.js"></script>
