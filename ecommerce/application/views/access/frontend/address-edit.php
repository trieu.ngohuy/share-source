<br>
<div class="container-fluid address-edit-wrap" id="my-account-wrap">
  <div class="row">
    <div class="col-md-3 col-lg-3" id="left">
      <ul>
        <li <?php if($active == 'account'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT?>"><?php echo $translate['my_account_2']?></a></li>
        <li <?php if($active == 'edit'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT_EDIT?>"><?php echo $translate['account_detail']?></a></li>
        <li <?php if($active == 'address'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_ADDRESS_BOOK?>"><?php echo $translate['address_book']?></a></li>
        <li <?php if($active == 'orders'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT_ORDERS?>"><?php echo $translate['my_order']?></a></li>
        <li class="last"><a href="<?php echo base_url() . 'account/logout'?>"><?php echo $translate['logout']?></a></li>
      </ul>
    </div>
    <div class="col-md-9 col-lg-9" id="right">
      <h3><?php echo $translate['address_edit']?></h3>
      <br>
      <div class="row">
        <div class="col-md-8 col-lg-8" id="detail">
          <?php
            if(isset($address_message)){
              if($address_message['status'] == 1){
                ?>
                  <div class="alert alert-success" role="alert"><?php echo $address_message['message']?></div>
                <?php
              }else{
                ?>
                  <div class="alert alert-danger" role="alert"><?php echo $address_message['message']?></div>
                <?php
              }
            }
          ?>
          <form method="post">
            <div class="row">
              <div class="col-md-6 col-lg-6">
                <div class="form-group">
                  <label for="comTitle"><?php echo $translate['country']?></label>
                  <select class="form-control" id="comTitle" name="address[country_gid]" required v-model="com_country">
                    <option value="" :selected="true"> <?php echo $translate['please_select']?> </option>
                    <option v-bind:value="value.places_gid" v-for="value in arr_data.country" v-if="value.type == 0"> {{value.title}} </option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="comTitle"><?php echo $translate['province']?></label>
                  <select class="form-control" id="comTitle" name="address[province_gid]" required v-model="com_province">
                    <option value=""> <?php echo $translate['please_select']?> </option>
                    <option v-bind:value="value.places_gid" v-for="value in arr_data.province"> {{value.title}} </option>
                  </select>
                </div>
              </div>
              <div class="col-md-6 col-lg-6">

                <div class="form-group">
                  <label for="comTitle"><?php echo $translate['district']?></label>
                  <select class="form-control" id="comTitle" name="address[district_gid]" required v-model="com_district">
                    <option value=""> <?php echo $translate['please_select']?> </option>
                    <option v-bind:value="value.places_gid" v-for="value in arr_data.district"> {{value.title}} </option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="comTitle"><?php echo $translate['ward']?></label>
                  <select class="form-control" id="comTitle" name="address[ward_gid]" required v-model="com_ward">
                    <option value=""> <?php echo $translate['please_select']?> </option>
                    <option v-bind:value="value.places_gid" v-for="value in arr_data.ward"> {{value.title}} </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="txtAddress"><?php echo $translate['address']?></label>
              <input value="<?php if(isset($data['add'])){echo $data['add'];}?>" type="text" name="address[address]" class="form-control" id="txtAddress" placeholder="<?php echo $translate['enter_address']?>" required>
            </div>
            <br>
            <div class="row">
              <div class="col-md-7 col-lg-7"><button type="submit" class="btn btn-custom wd-full"><?php echo $translate['save_address']?></button></div>
              <div class="col-md-5 col-lg-5"></div>
            </div>
          </form>
        </div>
        <div class="col-md-4 col-lg-4"></div>
      </div>
    </div>
  </div>
</div>
<br><br>
<script>
<?php
  $js_array = json_encode($places);
  echo "var arr_places = ". $js_array . ";\n";
  $js_array = json_encode($data);
  echo "var arr_data = ". $js_array . ";\n";
?>
</script>
<script type="text/javascript" src="<?php echo asset_front_url() ?>js/vuejs/vue.addressedit.js"></script>
