<div class="btitle">Đăng ký</div>
<br>
<?php
    if (isset($result)) {
        if ($result['status'] == 0) {
            ?>
            <div class="btitle alert-div" style="background: red;">
                <p>Lỗi!</p>
                <hr>
                <?php echo $result['message'] ?>
            </div>
            <?php
        } else {
            ?>
            <div class="btitle alert-div" style="background: green;">
                <p>Thành công!</p>
                <hr>
                <?php echo $result['message'] ?>
            </div>
            <?php
        }
    }
?>

<div class="bcontent">
    <form method="post">
        <div class="search-box">
            Họ và Tên
            <input type="text" name="data[full_name]" required="" minlength="10"
            <?php
            if (isset($arr_post_data['full_name'])) {
                echo 'value="' . $arr_post_data['full_name'] . '"';
            }
            ?>/>
        </div>
        <div class="search-box">
            Email đăng ký
            <input type="text" name="data[email]" required="" minlength="10"
            <?php
            if (isset($arr_post_data['email'])) {
                echo 'value="' . $arr_post_data['email'] . '"';
            }
            ?>/>
        </div>
        <div class="search-box">
            Tên đăng nhập
            <input type="text" name="data[username]" required="" minlength="6"
            <?php
            if (isset($arr_post_data['username'])) {
                echo 'value="' . $arr_post_data['username'] . '"';
            }
            ?>/>
        </div>
        <div class="search-box">
            Mật khẩu
            <input type="password" name="data[password]" required="" minlength="6"
            <?php
            if (isset($arr_post_data['password'])) {
                echo 'value="' . $arr_post_data['password'] . '"';
            }
            ?>/>
        </div>
        <div class="search-box">
            Nhập lại mật khẩu
            <input type="password" name="data[confirm_password]" required="" minlength="6"
            <?php
            if (isset($arr_post_data['confirm_password'])) {
                echo 'value="' . $arr_post_data['confirm_password'] . '"';
            }
            ?>/>
        </div>
        <div class="search-box">
            <a href="<?php echo base_url() . URL_LOGIN?>" style="color: #fff;">Đã có tài khoản? Hãy đăng nhập ngay.</a>
        </div>
        <button class="seemore" type="submit">Đăng ký</button>
    </form>
</div>