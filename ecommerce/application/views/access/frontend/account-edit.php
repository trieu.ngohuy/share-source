<br>
<div class="container-fluid account-edit-wrap" id="my-account-wrap">
  <div class="row">
    <div class="col-md-3 col-lg-3" id="left">
      <ul>
        <li <?php if($active == 'account'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT?>"><?php echo $translate['my_account_2']?></a></li>
        <li <?php if($active == 'edit'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT_EDIT?>"><?php echo $translate['account_detail']?></a></li>
        <li <?php if($active == 'address'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_ADDRESS_BOOK?>"><?php echo $translate['address_book']?></a></li>
        <li <?php if($active == 'orders'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT_ORDERS?>"><?php echo $translate['my_order']?></a></li>
        <li class="last"><a href="<?php echo base_url() . 'account/logout'?>"><?php echo $translate['logout']?></a></li>
      </ul>
    </div>
    <div class="col-md-9 col-lg-9" id="right">
      <h3><?php echo $translate['account_detail']?></h3>
      <br>
      <div class="row">
        <div class="col-md-8 col-lg-8" id="detail">
          <?php
            if(isset($account_edit_message)){
              if($account_edit_message['status'] == 1){
                ?>
                  <div class="alert alert-success" role="alert"><?php echo $account_edit_message['message']?></div>
                <?php
              }else{
                ?>
                  <div class="alert alert-danger" role="alert"><?php echo $account_edit_message['message']?></div>
                <?php
              }
            }
          ?>
          <form method="post">
            <div class="row">
              <div class="col-md-7 col-lg-7">
                <div class="form-group">
                  <label for="comTitle"><?php echo $translate['title']?></label>
                  <select class="form-control" id="comTitle" name="account[prefix]" required>
                    <option value=""> <?php echo $translate['please_select']?> </option>
                    <option value="Ms" <?php if($login_data['prefix'] == 'Ms'){echo 'selected';}?>> Ms </option>
                    <option value="Miss" <?php if($login_data['prefix'] == 'Miss'){echo 'selected';}?>> Miss </option>
                    <option value="Mrs" <?php if($login_data['prefix'] == 'Mrs'){echo 'selected';}?>> Mrs </option>
                    <option value="Mr" <?php if($login_data['prefix'] == 'Mr'){echo 'selected';}?>> Mr </option>
                    <option value="Dr" <?php if($login_data['prefix'] == 'Dr'){echo 'selected';}?>> Dr </option>
                    <option value="Prof" <?php if($login_data['prefix'] == 'Prof'){echo 'selected';}?>> Prof </option>
                    <option value="Rev" <?php if($login_data['prefix'] == 'Rev'){echo 'selected';}?>> Rev </option>
                  </select>
                </div>
              </div>
              <div class="col-md-5 col-lg-5"></div>
            </div>
            <div class="form-group">
              <label for="txtFirstName"><?php echo mb_strtoupper($translate['firstname'])?></label>
              <input value="<?php echo $login_data['firstname']?>" type="text" name="account[firstname]" class="form-control" id="txtFirstName" placeholder="<?php echo $translate['enter_firstname']?>" required>
            </div>
            <div class="form-group">
              <label for="txtLastName"><?php echo mb_strtoupper($translate['lastname'])?></label>
              <input value="<?php echo $login_data['lastname']?>" type="text" name="account[lastname]" class="form-control" id="txtLastName" placeholder="<?php echo $translate['enter_lastname']?>" required>
            </div>
            <div class="form-group">
              <label for="txtEmail"><?php echo mb_strtoupper($translate['subs_email'])?></label>
              <input value="<?php echo $login_data['email']?>" type="email" name="account[email]" class="form-control" id="txtEmail" placeholder="<?php echo $translate['email']?>" required>
            </div>
            <br>
            <h4><?php echo $translate['change_password']?></h4>
            <br>
            <div class="form-group">
              <label for="txtPassword"><?php echo $translate['new_password']?></label>
              <input type="password" name="account[password]" class="form-control" id="txtPassword">
            </div>
            <div class="form-group">
              <label for="txtRePassword"><?php echo $translate['confirm_new_password']?></label>
              <input type="password" name="account[repassword]" class="form-control" id="txtRePassword">
            </div>
            <br>
            <div class="row">
              <div class="col-md-7 col-lg-7"><button type="submit" class="btn btn-custom wd-full"><?php echo $translate['save']?></button></div>
              <div class="col-md-5 col-lg-5"></div>
            </div>
          </form>
        </div>
        <div class="col-md-4 col-lg-4"></div>
      </div>
    </div>
  </div>
</div>
<br><br>
