<div class="container-fluid">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-lg-3"></div>
      <div class="col-md-6 col-lg-6">
        <br>
        <br>
        <form method="post" id="frmLogin" v-on:submit="FormSubmit">
          <h5 class="al-center title"><?php echo $translate['lg_title']?></h5>
          <br><br>
          <?php
            if(isset($login_message)){
              ?>
                <div class="alert alert-<?php if($login_message['status'] == 1){echo 'success';}else{echo 'danger';}?>" role="alert"><?php echo $login_message['message']?></div>
              <?php
            }
          ?>
          <div class="form-group">
            <input type="text" @keydown.enter.prevent="" v-model="email" name="data[email]" value="" placeholder="<?php echo $translate['email']?>" class="form-control" />
            <p v-if="error_message.email != ''" class="err_div">{{error_message.email}}</p>
          </div>
          <div class="form-group" v-if="mode == 1">
            <p><?php echo $translate['welcome_back']?> {{fullname}} <?php echo $translate['please_type_password']?></p>
          </div>
          <div class="form-group" v-if="mode == 2">
            <input type="text" @keydown.enter.prevent="" v-model="firstname" name="data[firstname]" value="" placeholder="<?php echo $translate['firstname']?>" class="form-control" />
            <p v-if="error_message.firstname != ''" class="err_div">{{error_message.firstname}}</p>
          </div>
          <div class="form-group" v-if="mode == 2">
            <input type="text" @keydown.enter.prevent="" v-model="lastname" name="data[lastname]" value="" placeholder="<?php echo $translate['lastname']?>" class="form-control" />
            <p v-if="error_message.lastname != ''" class="err_div">{{error_message.lastname}}</p>
          </div>
          <div class="form-group" v-if="mode != 0">
            <input type="password" @keydown.enter.prevent="" v-model="password" name="data[password]" value="" placeholder="<?php echo $translate['password']?>" class="form-control" />
            <p v-if="error_message.password != ''" class="err_div">{{error_message.password}}</p>
          </div>
          <div class="form-check policy" v-if="mode == 2">
            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" v-model="policy" value="1">
            <label class="form-check-label" for="defaultCheck1">
              <?php echo $translate['subs_agreement']?>
            </label>
          </div>
          <p v-if="error_message.policy != ''" class="err_div err_policy">{{error_message.policy}}</p>
          <br>
          <div class="form-group button" v-if="mode != 1">
              <input type="submit" value="<?php echo $translate['continue']?>" class="btn btn-success wd-full"/>
          </div>
          <div class="form-group row button" v-if="mode == 1">
              <div class="col-md-6">
                  <input type="submit" value="<?php echo $translate['continue']?>" class="btn btn-success wd-full"/>
              </div>
              <div class="col-md-6">
                  <input type="hidden" name="data[reset_password]" v-model="is_reset_password" />
                  <input v-on:click="ResetPassword()" id="btnResetPassword" type="button" value="<?php echo $translate['reset_password']?>" class="btn btn-default width-full"/>
              </div>
          </div>
        </form>
        <br>
        <br>
      </div>
      <div class="col-md-3 col-lg-3"></div>
    </div>
  </div>
</div>

<script type="text/javascript">
    var err_email = '<?php echo $translate['err_invalid_email']?>';
    var err_require = '<?php echo $translate['err_require']?>';
    var err_policy = '<?php echo $translate['err_policy']?>';
    var url_checkvalid = '<?php echo base_url(). 'Access/CheckValid'?>';
</script>
<script type="text/javascript" src="<?php echo asset_front_url() ?>js/vuejs/vue.login.js"></script>
