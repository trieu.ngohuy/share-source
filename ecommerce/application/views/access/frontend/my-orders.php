<br>
<div class="container-fluid" id="my-account-wrap">
  <div class="row">
    <div class="col-md-3 col-lg-3" id="left">
      <ul>
        <li <?php if($active == 'account'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT?>"><?php echo $translate['my_account_2']?></a></li>
        <li <?php if($active == 'edit'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT_EDIT?>"><?php echo $translate['account_detail']?></a></li>
        <li <?php if($active == 'address'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_ADDRESS_BOOK?>"><?php echo $translate['address_book']?></a></li>
        <li <?php if($active == 'orders'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT_ORDERS?>"><?php echo $translate['my_order']?></a></li>
        <li class="last"><a href="<?php echo base_url() . 'account/logout'?>"><?php echo $translate['logout']?></a></li>
      </ul>
    </div>
    <div class="col-md-9 col-lg-9" id="right">
      <h3><?php echo $translate['my_order']?></h3>
      <br>
      <?php
        if(count($orders) <= 0){
          ?>
          <!-- Empty -->
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <br>
              <p><?php echo $translate['no_order']?></p>
              <br>
              <a href="<?php echo base_url()?>" class="btn btn-custom"><?php echo $translate['continue_shopping']?></a>
            </div>
          </div>
          <?php
        }else{
          ?>
          <!-- Last orders -->
          <div id="cart-index-wrap">
            <?php
              foreach($orders as $key => $value){
                ?>
                <br>
                <h5><?php echo $key + 1?>. <?php echo $translate['order_id']?>: <?php echo $value['code']?></h5>
                <hr>
                <p><b><?php echo $translate['order_date']?>: </b> <?php echo $value['created_date']?></p>
                <p><b><?php echo $translate['status']?>: </b> <?php echo $value['status_text']?></p>
                <br>

                <div class="row">
                  <div class="col-md-8 col-lg-8">
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col" colspan="2"><?php echo $translate['product']?></th>
                          <th scope="col" class="al-center" width="30px"><?php echo $translate['quantity']?></th>
                          <th scope="col" class="al-center" width="120px"><?php echo $translate['price']?></th>
                          <th scope="col" class="al-center" width="120px"><?php echo $translate['total']?></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          foreach($value['products'] as $key => $value1){
                            ?>
                            <tr <?php if($key == 0){echo 'class="first"';}?>>
                              <th><a href="<?php echo $value1['url']?>"><img src="<?php echo $value1['logo']?>" width="70px"/></a></th>
                              <th>
                                <a href="<?php echo $value1['url']?>"><?php echo $value1['title']?></a>
                                <p><?php echo $translate['material']?>: <?php echo $value1['material']?></p>
                                <p><?php echo $translate['color']?>: <?php echo $value1['color']?></p>
                                <p><?php echo $translate['size']?>: <?php echo $value1['size']?></p>
                              </th>
                              <th class="al-center"><?php echo $value1['count']?></th>
                              <th class="al-center"><?php echo $value1['price']?></th>
                              <th class="al-center"><?php echo $value1['price_total']?></th>
                            </tr>
                            <?php
                          }
                        ?>

                      </tbody>
                      </table>
                    <?php
                      if(isset($value['voucher'])){
                        ?>
                        <p><b><?php echo $translate['voucher_applied']?>: </b></p>
                        <hr>
                        <br>
                        <div class="row" id="voucher-detail">
                          <div class="col-md-3 col-lg-3 al-center" id="vch-left">
                            <img src="<?php echo asset_front_url() . 'img/icon/tag.png'?>" width="100px"/>
                          </div>
                          <div class="col-md-9 col-lg-9">
                            <h5><?php echo $value['voucher']['title']?></h5>
                            <hr>
                            <p><b><?php echo $translate['max_sale']?>:</b>
                            <?php
                              if ($value['voucher']['percent'] != 0){
                                echo $value['voucher']['percent'] .'%';
                              }else{
                                echo $value['voucher']['price'];
                              }
                            ?>
                            </p>
                            <p><b><?php echo $translate['min_order']?>:</b> <?php echo $value['voucher']['min_order']?></p>
                            <p><b><?php echo $translate['expire_date']?>:</b> <?php echo $value['voucher']['end_date']?></p>
                            <p><b><?php echo $translate['rules']?>:</b> <?php echo $value['voucher']['intro_text']?></p>
                          </div>
                        </div>
                        <?php
                      }
                    ?>
                    <br>
                    <p><b>Thông tin thanh toán: </b></p>
                    <hr>
                    <br>
                    <div class="row" id="voucher-detail">
                      <div class="col-md-3 col-lg-3 al-center" id="vch-left">
                        <img src="https://image.flaticon.com/icons/svg/164/164436.svg" width="100px"/>
                      </div>
                      <div class="col-md-9 col-lg-9">
                        <p><b>Tên người mua:</b> <?php echo $value['payment']['full_name']?></p>
                        <p><b>Số điện thoại người mua:</b> <?php echo $value['payment']['phone']?></p>
                        <p><b>Địa chỉ giao hàng:</b> <?php echo $value['payment']['address']?></p>
                        <p><b>Phương thức thanh toán:</b> <?php echo $value['payment']['method']?></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-lg-4" id="right">
                    <h5 class="title"><?php echo $translate['order_summary']?></h5>
                    <div id="cart-summary">
                      <br>
                      <div class="row">
                        <div class="col-md-6 col-lg-6"><b><?php echo $translate['total']?> (<?php echo count($value['products'])?>)</b></div>
                        <div class="col-md-6 col-lg-6 al-right"><?php echo $value['total_original']?></div>
                      </div>
                      <?php
                        if(isset($value['voucher'])){
                          ?>
                          <br>
                          <div class="row">
                            <div class="col-md-6 col-lg-6"><b>Voucher</b></div>
                            <div class="col-md-6 col-lg-6 al-right"><?php echo $value['voucher']['price']?></div>
                          </div>
                          <hr>
                          <div class="row">
                            <div class="col-md-6 col-lg-6"><b><?php echo $translate['total']?> (<?php echo count($value['products'])?>)</b></div>
                            <div class="col-md-6 col-lg-6 al-right"><?php echo $value['total']?></div>
                          </div>
                          <?php
                        }
                      ?>
                    </div>

                  </div>
                </div>
                <br>
                <?php
              }
            ?>
          </div>
          <?php
        }
      ?>

      <br>
    </div>
  </div>
</div>
<br><br>
