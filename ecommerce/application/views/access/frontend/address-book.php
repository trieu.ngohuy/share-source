<br>
<div class="container-fluid address-book-wrap" id="my-account-wrap">
    <div class="row">
        <div class="col-md-3 col-lg-3" id="left">
            <ul>
              <li <?php if($active == 'account'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT?>"><?php echo $translate['my_account_2']?></a></li>
              <li <?php if($active == 'edit'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT_EDIT?>"><?php echo $translate['account_detail']?></a></li>
              <li <?php if($active == 'address'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_ADDRESS_BOOK?>"><?php echo $translate['address_book']?></a></li>
              <li <?php if($active == 'orders'){echo 'class="active"';}?>><a href="<?php echo base_url() . URL_MY_ACCOUNT_ORDERS?>"><?php echo $translate['my_order']?></a></li>
              <li class="last"><a href="<?php echo base_url() . 'account/logout'?>"><?php echo $translate['logout']?></a></li>
            </ul>
        </div>
        <div class="col-md-9 col-lg-9" id="right">
            <h3><?php echo $translate['manager_address_book']?></h3>
            <br>
            <?php
                if(count($delivery_address) > 0){
                    ?>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div id="default-address">
                                    <p><b><?php echo $translate['default_address_book']?></b></p>
                                    <div class="row">
                                    <div class="col-md-8 col-lg-8"><p><b><?php echo $login_data['firstname'] . ' ' . $login_data['lastname']?></b></p></div>
                                    <div class="col-md-4 col-lg-4 al-right">
                                        <a href="<?php echo base_url() . URL_ADDRESS_BOOK . '0'?>" class="underline"><?php echo $translate['edit']?> | </a>
                                        <a href="#" class="underline" v-on:click="delete_address(0)">delete</a>
                                    </div>
                                    </div>
                                    <p><?php echo $delivery_address[0]['add']?></p>
                                    <p><?php echo $delivery_address[0]['ward']?></p>
                                    <p><?php echo $delivery_address[0]['district']?></p>
                                    <p><?php echo $delivery_address[0]['provinces']?></p>
                                    <p><?php echo $delivery_address[0]['country']?></p>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h5><?php echo $translate['additional_address']?></h5>
                        <div class="row">
                        <?php
                            foreach($delivery_address as $key => $value){
                                if($key != 0){
                                ?>
                                    <div class="col-md-6 col-lg-6">
                                        <div id="default-address">
                                            <div class="row">
                                                <div class="col-md-8 col-lg-8"><p><b><?php echo $login_data['firstname'] . ' ' . $login_data['lastname']?></b></p></div>
                                                <div class="col-md-4 col-lg-4 al-right">
                                                    <a href="<?php echo base_url() . URL_ADDRESS_BOOK . $key?>" class="underline"><?php echo $translate['edit']?> | </a>
                                                    <a href="#" class="underline" v-on:click="delete_address(<?php echo $key?>)"><?php echo $translate['delete']?></a>
                                                </div>
                                            </div>
                                            <p><?php echo $value['add']?></p>
                                            <p><?php echo $value['ward']?></p>
                                            <p><?php echo $value['district']?></p>
                                            <p><?php echo $value['provinces']?></p>
                                            <p><?php echo $value['country']?></p>
                                        </div>
                                        <br>
                                    </div>
                                <?php
                                }
                            }
                        ?>
                        </div>
                    <?php
                }else{
                    ?>
                    <p><?php echo $translate['no_address']?></p>
                    <?php
                }
            ?>
            <br>
            <a href="<?php echo base_url() . URL_ADDRESS_BOOK . 'new'?>" class="btn btn-success btn-custom"><?php echo $translate['add_new_address']?></a>
        </div>
    </div>
</div>
<br><br>
<script type="text/javascript">
    var url_delete = '<?php echo base_url(). 'Access/Delete'?>';
</script>
<script type="text/javascript" src="<?php echo asset_front_url() ?>js/vuejs/vue.addressbook.js"></script>
