<div class="container-fluid" id="cart-index-wrap">
  <!-- Title -->
  <div class="row">
    <div class="col-md-12 col-lg-12 title">
      <h3><?php echo $translate['my_bag']?></h3>
      <p>
        <?php
        if(isset($cart['products']) && count($cart['products']) > 0){
          echo count($cart['products']) . ' ' . $translate['items'];
        }else{
          echo $translate['empty'];
        }
         ?>
      </p>
    </div>
  </div>
  <br>
  <?php
    if(!isset($cart['products']) || count($cart['products']) <= 0){
      ?>
      <!-- Bag empty -->
      <div class="row">
        <div class="col-md-12 col-lg-12 al-center">
          <p><?php echo $translate['empty_des']?></p>
          <br>
          <button type="button" class="btn btn-success" v-on:click="homepage()">CONTINUE SHOPPING</button>
        </div>
      </div>
      <br><br>
      <?php
    }else{
      ?>
      <!-- Detail -->
      <div class="row">
        <div class="col-md-8 col-lg-8">
          <table class="table">
            <thead>
              <tr>
                <th scope="col" colspan="2">Product</th>
                <th scope="col" class="al-center" width="70px"><?php echo $translate['quantity']?></th>
                <th scope="col" class="al-center" width="120px"><?php echo $translate['price']?></th>
                <th scope="col" class="al-center" width="120px"><?php echo $translate['total']?></th>
                <th scope="col" class="al-center" width="50px"></th>
              </tr>
            </thead>
            <tbody>
              <?php
                foreach($cart['products'] as $key => $value){
                  ?>
                  <tr <?php if($key == 0){echo 'class="first"';}?>>
                    <th><a href="<?php echo $value['url']?>"><img src="<?php echo $value['logo']?>" width="70px"/></a></th>
                    <th>
                      <a href="<?php echo $value['url']?>"><?php echo $value['title']?></a>
                      <p><?php echo $translate['material']?>: <?php echo $value['material']?></p>
                      <p><?php echo $translate['color']?>: <?php echo $value['color']?></p>
                      <p><?php echo $translate['size']?>: <?php echo $value['size']?></p>
                    </th>
                    <th>
                      <input type="number" data-index="<?php echo $key?>" name="quantity[val]" class="form-control inp-quantity" value="<?php echo $value['count']?>"/>
                    </th>
                    <th class="al-center"><?php echo $value['price']?></th>
                    <th class="al-center" id='price_total_<?php echo $key?>'><?php echo $value['price_total']?></th>
                    <th><a href="#" class="underline" v-on:click="remove_product(<?php echo $key?>)"><?php echo $translate['remove']?></a></th>
                  </tr>
                  <?php
                }
              ?>

            </tbody>
            </table>
          <?php
            if(!isset($cart['voucher'])){
              ?>
              <form method="post">
                <div class="row" id="form-voucher">
                    <div class="col-md-9 col-lg-9">
                    <div class="form-group">
                      <label for="exampleInputEmail1"><?php echo $translate['add_voucher_code']?></label>
                      <input required name="voucher[code]" type="text" class="form-control" id="exampleInputEmail1" placeholder="<?php echo $translate['enter_code']?>">
                      <?php
                        if(isset($voucher_submit)){
                          ?>
                          <br>
                            <div class="alert alert-danger" role="alert"><?php echo $voucher_submit['message']?></div>
                          <?php
                        }
                      ?>
                    </div>
                  </div>
                    <div class="col-md-3 col-lg-3"><button type="submit" class="wd-full btn btn-success"><?php echo $translate['add']?></button></div>
                </div>
              </form>
              <br>
              <?php
            }else{
              ?>
              <p><?php echo $translate['voucher_applied']?></p>
              <hr>
              <br>
              <div class="row" id="voucher-detail">
                <div class="col-md-3 col-lg-3 al-center" id="vch-left">
                  <img src="<?php echo asset_front_url() . 'img/icon/tag.png'?>" width="100px"/>
                </div>
                <div class="col-md-9 col-lg-9">
                  <h5><?php echo $cart['voucher']['title']?></h5>
                  <hr>
                  <p><b><?php echo $translate['max_sale']?>:</b>
                  <?php
                    if ($cart['voucher']['percent'] != 0){
                      echo $cart['voucher']['percent'] .'%';
                    }else{
                      echo $cart['voucher']['price'];
                    }
                  ?>
                  </p>
                  <p><b><?php echo $translate['min_order']?>:</b> <?php echo $cart['voucher']['min_order']?></p>
                  <p><b><?php echo $translate['expire_date']?>:</b> <?php echo $cart['voucher']['end_date']?></p>
                  <p><b><?php echo $translate['rules']?>:</b> <?php echo $cart['voucher']['intro_text']?></p>
                </div>
              </div>
              <?php
            }
          ?>
        </div>
        <div class="col-md-4 col-lg-4" id="right">
          <h5 class="title"><?php echo $translate['order_summary']?></h5>
          <div id="cart-summary">
            <br>
            <div class="row">
              <div class="col-md-6 col-lg-6"><b><?php echo $translate['total']?> (<?php echo count($cart['products'])?>)</b></div>
              <div class="col-md-6 col-lg-6 al-right" id="div-total-original"><?php echo $cart['total_original']?></div>
            </div>
            <?php
              if(isset($cart['voucher'])){
                ?>
                <br>
                <div class="row">
                  <div class="col-md-6 col-lg-6"><b>Voucher</b></div>
                  <div class="col-md-6 col-lg-6 al-right"><?php echo $cart['voucher']['price']?></div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-6 col-lg-6"><b><?php echo $translate['total']?> (<?php echo count($cart['products'])?>)</b></div>
                  <div class="col-md-6 col-lg-6 al-right" id="div-total"><?php echo $cart['total']?></div>
                </div>
                <?php
              }
            ?>
            <hr>
          </div>
          <br>
          <?php
            if($delivery_address === null){
              ?>
              <form method="post" id="frmCart">
                <div id="card-detail">
                  <p><b>Thanh toán không cần tài khoản</b></p>
                  <br>
                  <div class="form-group required">
                    <label for="input-payment-country" class="control-label">Họ và tên</label>
                    <input required="required" name="payment[full_name]" type="text" placeholder="Nhập họ và tên" class="form-control">
                  </div>
                  <div class="form-group required">
                    <label for="input-payment-country" class="control-label">Số điện thoại</label>
                    <input required="required" name="payment[phone]" type="text" placeholder="Nhập số điện thoại" class="form-control">
                  </div>
                  <!-- <div class="row">
                    <div class="col-md-6">
                      <div class="form-group required">
                        <label for="input-payment-country" class="control-label">Đất nước</label>
                        <select class="form-control" id="com_country" name="address[country]" required v-model="com_country">
                          <option value="" :selected="true"> <?php echo $translate['please_select']?> </option>
                          <option v-bind:value="value.places_gid" v-for="value in arr_data.country" v-if="value.type == 0"> {{value.title}} </option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group required">
                        <label for="input-payment-country" class="control-label">Tỉnh / thành phố</label>
                        <select class="form-control" id="com_city" name="address[ward]" required v-model="com_province">
                          <option value="" :selected="true"> <?php echo $translate['please_select']?> </option>
                          <option v-bind:value="value.places_gid" v-for="value in arr_data.province"> {{value.title}} </option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group required">
                        <label for="input-payment-country" class="control-label">Quận / huyện</label>
                        <select class="form-control" id="com_district" name="address[district]" required v-model="com_district">
                          <option value="" :selected="true"> <?php echo $translate['please_select']?> </option>
                          <option v-bind:value="value.places_gid" v-for="value in arr_data.district"> {{value.title}} </option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group required">
                        <label for="input-payment-country" class="control-label">Phường / xã</label>
                        <select class="form-control" id="com_ward" name="address[ward]" required v-model="com_ward">
                          <option value="" :selected="true"> <?php echo $translate['please_select']?> </option>
                          <option v-bind:value="value.places_gid" v-for="value in arr_data.ward"> {{value.title}} </option>
                        </select>
                      </div>
                    </div>
                  </div> -->
                  <div class="form-group required">
                    <label for="input-payment-country" class="control-label">Địa chỉ</label>
                    <input required="required" name="payment[address]" type="text" placeholder="Nhập địa chỉ" class="form-control">
                  </div>
                  <br>
                  <div class="form-group required">
                    <label for="input-payment-country" class="control-label"><b><?php echo $translate['payment_method']?></b></label>
                    <select class="form-control" id="input-payment-country" name="payment[method]" required v-model="pay_method" @change="select_paymethod()">
                      <option value="0"> <?php echo $translate['please_select']?> </option>
                      <option value="1"><?php echo $translate['pay_on_receive']?></option>
                      <option value="2">Paypal</option>
                    </select>
                  </div>
                  <input type="hidden" id="txt_payment_info" name="payment[info]" class="form-control" value=""/>
                  <div id="cod" v-if="pay_method == 99">
                    <hr>
                    <p><?php echo $translate['transfer_fee']?>: <b><?php echo $setting['transfer_fee']['value']?></b></p>
                  </div>
                </div>
                <br>
                <div id="paypal-wrap" v-bind:class="paypal_class">

                </div>
                <button :disabled="pay_method == 0" v-if="pay_method != 2" type="submit" class="wd-full btn btn-success" <?php if($delivery_address != null && count($delivery_address) <= 0){echo 'disabled';}?>><?php echo mb_strtoupper($translate['complete_order'])?></button>
                <br><br>
                <div class="form-group required al-center">
                  <p><b>Secure by</b></p>
                  <img style="height: 70px" src="<?php echo asset_front_url() . 'img/payment/paypal.png'?>" />
                  <img style="height: 50px" src="<?php echo asset_front_url() . 'img/payment/visa.png'?>" />
                  <img style="height: 50px" src="<?php echo asset_front_url() . 'img/payment/mastercard.png'?>" />
                </div>
              </form>
              <?php
            }else{
              ?>
              <?php if(count($delivery_address) <= 0){
                ?>
                <div class="alert alert-danger" role="alert">
                  <?php echo $translate['no_delivery1'] . ' <a href="'.base_url() . URL_MY_ACCOUNT.'">' . $translate['my_account'] . '</a> ' .$translate['no_delivery2'];?>
                </div>
                <?php
              }?>
              <form method="post" id="frmCart">
                <div id="card-detail">
                  <div class="form-group required">
                    <label for="input-payment-country" class="control-label"><b><?php echo $translate['delivery_address']?></b></label>
                    <select <?php if(count($delivery_address) <= 0){echo 'disabled';}?> class="form-control" id="input-payment-country" name="payment[address]" required>
                      <?php
                        foreach($delivery_address as $value){
                            ?>
                              <option value="<?php echo $value['key']?>"><?php echo $value['address']?></option>
                            <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group required">
                    <label for="input-payment-country" class="control-label"><b><?php echo $translate['payment_method']?></b></label>
                    <select <?php if(count($delivery_address) <= 0){echo 'disabled';}?> class="form-control" id="input-payment-country" name="payment[method]" required v-model="pay_method" @change="select_paymethod()">
                      <option value="0"> <?php echo $translate['please_select']?> </option>
                      <option value="1"><?php echo $translate['pay_on_receive']?></option>
                      <option value="2">Paypal</option>
                    </select>
                  </div>
                  <input type="hidden" id="txt_payment_info" name="payment[info]" class="form-control" value=""/>
                  <div id="cod" v-if="pay_method == 99">
                    <hr>
                    <p><?php echo $translate['transfer_fee']?>: <b><?php echo $setting['transfer_fee']['value']?></b></p>
                  </div>
                </div>
                <br>
                <div id="paypal-wrap" v-bind:class="paypal_class">

                </div>
                <button  v-if="pay_method != 2" type="submit" class="wd-full btn btn-success" <?php if(count($delivery_address) <= 0){echo 'disabled';}?>><?php echo mb_strtoupper($translate['complete_order'])?></button>
                <br><br>
                <div class="form-group required al-center">
                  <p><b>Secure by</b></p>
                  <img style="height: 70px" src="<?php echo asset_front_url() . 'img/payment/paypal.png'?>" />
                  <img style="height: 50px" src="<?php echo asset_front_url() . 'img/payment/visa.png'?>" />
                  <img style="height: 50px" src="<?php echo asset_front_url() . 'img/payment/mastercard.png'?>" />
                </div>
              </form>
              <?php
            }
          ?>
        </div>
      </div>
      <?php
    }
  ?>
</div>
<br><br>
<style>
  .dsp-none{
    display: none;
  }
</style>
<script type="text/javascript">
    var url_home = '<?php echo base_url()?>';
    var url_remove = '<?php echo base_url(). 'Cart/RemoveProduct'?>';
    <?php
    //$js_array = json_encode($places);
    //echo "var arr_places = ". $js_array . ";\n";
    echo "var arr_places = [];\n";
    ?>
    var total_usd = '<?php echo $cart['total_usd']?>';
</script>
<script type="text/javascript" src="<?php echo asset_front_url() ?>js/vuejs/vue.cart.js"></script>
<script src="https://www.paypal.com/sdk/js?client-id=Abnm4vZfnffFwCJaZhNW1Sh-IUApFbkzaxw0xZ--0YlMrngNYxAyc-3X-0RtaYQTS15zhX6WS-gO6qSP"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script>
  $('.inp-quantity').on('keyup', function(){
    $this = $(this);
    setTimeout(function(){
      var val = $this.val();
      var index= $this.attr('data-index');
      if(val == '' || val == 0){
        val = 1;
        $this.val(val);
      }
      var fd = new FormData();
      fd.append('index', index);
      fd.append('quantity', val);

      $.ajax({
          type: 'POST',
          url: '<?php echo base_url() ?>Cart/UpdateQuantity',
          data: fd,
          processData: false,
          contentType: false,
          cache:false,
          enctype: 'multipart/form-data',
          success: function (objData) {
              //json
              objData = $.parseJSON(objData);
              //Update product total price
              $('#price_total_' + index).html(objData['price_total']);
              //update total_usd
              total_usd = objData['total_usd'];
              //Update total_original
              $('#div-total-original').html(objData['total_original']);
              if($('#div-total').length > 0){
                  $('#div-total').html(objData['total']);
              }

              //Update cart preview popup
              vue_header.arr_data.cart = objData;
              vue_header.reload_carthtml();
          },
          error: function (jqXHR, exception) {
              alert('Có lỗi. Thử lại sau.');
          }
      });
    }, 1500);
  });
  paypal.Buttons({
    createOrder: function(data, actions) {
      return actions.order.create({
      purchase_units: [{
        amount: {
        value: total_usd
        }
      }]
      });
    },
    onApprove: function(data, actions) {
      // Capture the funds from the transaction
      return actions.order.capture().then(function(details) {
      // Show a success message to your buyer
        //alert('Transaction completed by ' + details.payer.name.given_name);
        $('#txt_payment_info').val(JSON.stringify(details));
        $('#frmCart').submit();
      });
    }
    }).render('#paypal-wrap');
</script>
