<div class="container-fluid al-center" id="cart-index-wrap">
  <br><br>
  <h5><?php echo $translate['payment_success']?></h5>
  <br>
  <p><?php echo $translate['payment_des']?></p>
  <br>
  <button type="button" class="btn btn-success btn-custom" v-on:click="homepage()"><?php echo $translate['continue_shopping']?></button>
  <br><br>
</div>
<script type="text/javascript">
    var url_home = '<?php echo base_url()?>';
</script>
<script type="text/javascript" src="<?php echo asset_front_url() ?>js/vuejs/vue.cart.js"></script>
