<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quản lý đơn hàng
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <!-- <div class="box-header al-right">
                    <button type="button" id="btn_new" class="btn btn-primary">Thêm mới</button>
                </div> -->
                <!-- /.box-header -->
                <div class="box-body">
                  <?php
                    if(count($data) <= 0){
                      ?>
                      <!-- Empty -->
                      <br>
                      <p>Chưa có đơn hàng nào.</p>
                      <br>
                      <?php
                    }else{
                      ?>
                      <!-- Last orders -->
                      <div id="cart-index-wrap">
                        <?php
                          foreach($data as $key => $value){
                            ?>
                            <br>
                            <div class="row">
                              <div class="col-md-10"><h4><b><?php echo $key + 1?>. Mã đơn hàng: <?php echo $value['code']?></b></h4></div>
                              <div class="col-md-2 al-right" style="padding-top: 10px;"><a href="#" class="btn-remove" data-id="<?php echo $value['cart_id']?>">Xóa đơn hàng</a></div>
                            </div>
                            <hr>
                            <p><b>Ngày đặt hàng: </b> <?php echo $value['created_date']?></p>
                            <div class="row">
                              <div class="col-md-2" style="padding-top: 5px;"><b>Tình trạng đơn hàng:</b></div>
                                <div class="col-md-2">
                                  <select id="com_status" class="form-control select2" data-id="<?php echo $value['cart_id']?>">
                                    <option value="0" <?php if ($value['status'] == "0"){echo "selected";}?> > Đang xử lý </option>
                                    <option value="1" <?php if ($value['status'] == "1"){echo "selected";}?> > Đã thanh toán </option>
                                  </select>
                              </div>
                            </div>
                            <br>

                            <div class="row">
                              <div class="col-md-8 col-lg-8">
                                <table class="table">
                                  <thead>
                                    <tr>
                                      <th scope="col" colspan="2">Tên sản phẩm</th>
                                      <th scope="col" class="al-center" width="30px">SL</th>
                                      <th scope="col" class="al-center" width="120px">Giá</th>
                                      <th scope="col" class="al-center" width="120px">Tổng giá</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                      foreach($value['products'] as $key => $value1){
                                        ?>
                                        <tr <?php if($key == 0){echo 'class="first"';}?>>
                                          <th><img src="<?php echo $value1['logo']?>" width="70px"/></th>
                                          <th>
                                            <p><?php echo $value1['title']?></p>
                                            <p>Chất liệu: <?php echo $value1['material']?></p>
                                            <p>Màu sắc: <?php echo $value1['color']?></p>
                                            <p>Kích thước: <?php echo $value1['size']?></p>
                                          </th>
                                          <th class="al-center"><?php echo $value1['count']?></th>
                                          <th class="al-center"><?php echo $value1['price']?></th>
                                          <th class="al-center"><?php echo $value1['price_total']?></th>
                                        </tr>
                                        <?php
                                      }
                                    ?>

                                  </tbody>
                                  </table>
                                <?php
                                  if(isset($value['voucher'])){
                                    ?>
                                    <p><b>Voucher đã áp dụng: </b></p>
                                    <hr>
                                    <br>
                                    <div class="row" id="voucher-detail">
                                      <div class="col-md-3 col-lg-3 al-center" id="vch-left">
                                        <img src="<?php echo asset_front_url() . 'img/icon/tag.png'?>" width="100px"/>
                                      </div>
                                      <div class="col-md-9 col-lg-9">
                                        <h5><?php echo $value['voucher']['title']?></h5>
                                        <hr>
                                        <p><b>Giảm tối đa:</b>
                                        <?php
                                          if ($value['voucher']['percent'] != 0){
                                            echo $value['voucher']['percent'] .'%';
                                          }else{
                                            echo $value['voucher']['price'];
                                          }
                                        ?>
                                        </p>
                                        <p><b>Đơn hàng tối thiểu:</b> <?php echo $value['voucher']['min_order']?></p>
                                        <p><b>Ngày hết hạn:</b> <?php echo $value['voucher']['end_date']?></p>
                                        <p><b>Thể lệ:</b> <?php echo $value['voucher']['intro_text']?></p>
                                      </div>
                                    </div>
                                    <?php
                                  }
                                ?>
                                <br>
                                <p><b>Thông tin thanh toán: </b></p>
                                <hr>
                                <br>
                                <div class="row" id="voucher-detail">
                                  <div class="col-md-3 col-lg-3 al-center" id="vch-left">
                                    <img src="https://image.flaticon.com/icons/svg/164/164436.svg" width="100px"/>
                                  </div>
                                  <div class="col-md-9 col-lg-9">
                                    <p><b>Tên người mua:</b> <?php echo $value['payment']['full_name']?></p>
                                    <p><b>Số điện thoại người mua:</b> <?php echo $value['payment']['phone']?></p>
                                    <p><b>Địa chỉ giao hàng:</b> <?php echo $value['payment']['address']?></p>
                                    <p><b>Phương thức thanh toán:</b> <?php echo $value['payment']['method']?></p>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-4 col-lg-4" id="right">
                                <h5 class="title">Tổng hợp đơn hàng</h5>
                                <div id="cart-summary">
                                  <br>
                                  <div class="row">
                                    <div class="col-md-6 col-lg-6"><b>Tổng tiền (<?php echo count($value['products'])?>)</b></div>
                                    <div class="col-md-6 col-lg-6 al-right"><?php echo $value['total_original']?></div>
                                  </div>
                                  <?php
                                    if(isset($value['voucher'])){
                                      ?>
                                      <br>
                                      <div class="row">
                                        <div class="col-md-6 col-lg-6"><b>Voucher</b></div>
                                        <div class="col-md-6 col-lg-6 al-right"><?php echo $value['voucher']['price']?></div>
                                      </div>
                                      <hr>
                                      <div class="row">
                                        <div class="col-md-6 col-lg-6">Tổng tiền (<?php echo count($value['products'])?>)</b></div>
                                        <div class="col-md-6 col-lg-6 al-right"><?php echo $value['total']?></div>
                                      </div>
                                      <?php
                                    }
                                  ?>
                                </div>

                              </div>
                            </div>
                            <br>
                            <?php
                          }
                        ?>
                      </div>
                      <?php
                    }
                  ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.modal -->
<!-- Style -->
<style>
    .select2-container .select2-selection--single {
        height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 31px;
    }
    #right .title {
        margin: 0px;
        padding: 10px;
        font-weight: 600;
    }
    thead th, #right .title {
        background: darkgrey;
    }
</style>
<!-- page script -->
<script>
    var mode = 'new';
    var key = 0;
    var key_str = 'cart_id';
    var url_update = "<?php echo base_url() ?>AdminCart/execute_query";
    var url_delete = "<?php echo base_url() ?>AdminCart/delete";
    var url_refresh = "<?php echo base_url() ?>AdminCart/get_data";
    //Convert php array to js array
    <?php
    $js_array = json_encode($data);
    echo "var arr_data = " . $js_array . ";\n";
    ?>

    $(document).ready(function () {

        $(document).on('click', '.btn-remove', function (e) {
            if (confirm("Are you sure you want to delete this?")) {
                //Get id
                var key = $(this).attr('data-id');

                //Create post data
                var fd = new FormData();
                fd.append(key_str, key);

                $.ajax({
                    type: 'POST',
                    url: url_delete,
                    data: fd,
                    processData: false,
                    contentType: false,
                    cache:false,
                    enctype: 'multipart/form-data',
                    success: function (objData) {
                        //Refresh data
                        refresh_data();
                    },
                    error: function (jqXHR, exception) {
                        alert('Có lỗi. Thử lại sau.');
                    }
                });
            }
        });
        $(document).on('change', '#com_status', function (e) {
            //Get id
            key = $(this).attr('data-id');
            //Ajax
            $.ajax({
                type: 'POST',
                url: url_update,
                data: get_update_data(),
                processData: false,
                contentType: false,
                cache:false,
                enctype: 'multipart/form-data',
                success: function (objData) {
                  if(objData !== ''){
                    objData = $.parseJSON(objData);
                    alert(objData["messsage"]);
                  }else{
                    //Refresh data
                    refresh_data();
                  }
                },
                error: function (jqXHR, exception) {
                    alert('Có lỗi. Thử lại sau.');
                }
            });
        });
    });
    //Get data
    function refresh_data() {
        //Refresh data
        $.ajax({
            type: 'GET',
            url: url_refresh,
            success: function (data) {
                data = $.parseJSON(data);
                arr_data = data;
                var html = '';
                if(data.length <= 0){
                  html += '<br>'
                  +'<p>Chưa có đơn hàng nào.</p>'
                  +'<br>';
                }else {
                  html += '<div id="cart-index-wrap">';
                  for(var i = 0 ; i < data.length ; i++){
                    html += '<br>'
                    +'<div class="row">'
                    +'  <div class="col-md-10"><h4><b>'+(i+1)+'. Mã đơn hàng: '+data[i]['code']+'</b></h4></div>'
                    +'  <div class="col-md-2 al-right" style="padding-top: 10px;"><a href="#" class="btn-remove" data-id="'+data[i]['cart_id']+'">Xóa đơn hàng</a></div>'
                    +'</div>'
                    +'<hr>'
                    +'<p><b>Ngày đặt hàng: </b> '+data[i]['created_date']+'</p>'
                    +'<div class="row">'
                    +'  <div class="col-md-2" style="padding-top: 5px;"><b>Tình trạng đơn hàng:</b></div>'
                    +'    <div class="col-md-2"><select id="com_status" class="form-control select2">'
                    +'      <option value="0" '+(data[i]['status'] == 0 ? 'selected' : '')+' > Đang xử lý </option>'
                    +'      <option value="1" '+(data[i]['status'] == 1 ? 'selected' : '')+' > Đã thanh toán </option>'
                    +'    </select>'
                    +'  </div>'
                    +'</div>'
                    +'<br>';

                    html += '<div class="row">'
                      +'<div class="col-md-8 col-lg-8">'
                        +'<table class="table">'
                          +'<thead>'
                          +'  <tr>'
                          +'    <th scope="col" colspan="2">Tên sản phẩm</th>'
                          +'    <th scope="col" class="al-center" width="30px">SL</th>'
                          +'    <th scope="col" class="al-center" width="120px">Giá</th>'
                          +'    <th scope="col" class="al-center" width="120px">Tổng giá</th>'
                          +'  </tr>'
                          +'</thead>'
                          +'<tbody>';
                            for(var j = 0 ; j < data[i]['products'].length ; j++){
                              var tmp = data[i]['products'][j];
                              html += '<tr '+(j == 0 ? 'class="first"' : '')+'>'
                              +'  <th><img src="'+tmp['logo']+'" width="70px"/></th>'
                              +'  <th>'
                              +'    <p>'+tmp['title']+'</p>'
                              +'    <p>Chất liệu: '+tmp['material']+'</p>'
                              +'    <p>Màu sắc: '+tmp['color']+'</p>'
                              +'    <p>Kích thước: '+tmp['size']+'</p>'
                              +'  </th>'
                              +'  <th class="al-center">'+tmp['count']+'</th>'
                              +'  <th class="al-center">'+tmp['price']+'</th>'
                              +'  <th class="al-center">'+tmp['price_total']+'</th>'
                              +'</tr>';
                            }
                      html +='    </tbody>'
                          +'</table>';

                          if(data[i]['voucher'] !== undefined){
                            tmp = data[i]['voucher'];
                            html += '<p>Voucher đã áp dụng: </p>'
                            +'<hr>'
                            +'<br>'
                            +'<div class="row" id="voucher-detail">'
                            +'  <div class="col-md-3 col-lg-3 al-center" id="vch-left">'
                            +'    <img src="<?php echo asset_front_url() . 'img/icon/tag.png'?>" width="100px"/>'
                            +'  </div>'
                            +'  <div class="col-md-9 col-lg-9">'
                            +'    <h5>'+tmp['title']+'</h5>'
                            +'    <hr>'
                            +'    <p><b>Giảm tối đa:</b>';
                            if (tmp['percent'] != 0){
                              html += tmp['percent'] + '%';
                            }else{
                              html += tmp['price'];
                            }
                            html += '    </p>'
                            +'    <p><b>Đơn hàng tối thiểu:</b> '+tmp['min_order']+'</p>'
                            +'    <p><b>Ngày hết hạn:</b> '+tmp['end_date']+'</p>'
                            +'    <p><b>Thể lệ:</b> '+tmp['intro_text']+'</p>'
                            +'  </div>'
                            +'</div>';
                          }
                      html += '</div>'
                      +'<div class="col-md-4 col-lg-4" id="right">'
                        +'<h5 class="title">Tổng hợp đơn hàng</h5>'
                        +'<div id="cart-summary">'
                        +'  <br>'
                        +'  <div class="row">'
                        +'    <div class="col-md-6 col-lg-6"><b>Tổng tiền ('+data[i]['products'].length+')</b></div>'
                        +'    <div class="col-md-6 col-lg-6 al-right">'+data[i]['total_original']+'</div>'
                        +'  </div>'
                            if(data[i]['voucher'] !== undefined){
                              html += '<br>'
                              +'<div class="row">'
                              +'  <div class="col-md-6 col-lg-6"><b>Voucher</b></div>'
                              +'  <div class="col-md-6 col-lg-6 al-right">'+data[i]['voucher']['price']+'</div>'
                              +'</div>'
                              +'<hr>'
                              +'<div class="row">'
                              +'  <div class="col-md-6 col-lg-6">Tổng tiền ('+data[i]['products'].length+')</b></div>'
                              +'  <div class="col-md-6 col-lg-6 al-right">'+data[i]['total']+'</div>'
                              +'</div>';
                            }
                    html +='    </div>'

                    +'  </div>'
                    +'</div>'
                    +'<br>';
                  }
                  html += '</div>';
                }
                $('.box-body').html(html);
            },
            error: function () {
                alert('Có lỗi. Thử lại sau.');
            }
        });
    }
    //Get update data
    function get_update_data(){
      var fd = new FormData();

      //Common
      fd.append(key_str, key);
      fd.append('status', $("#com_status option:selected").val());

      return fd;
    }
</script>
