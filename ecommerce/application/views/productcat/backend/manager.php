<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quản lý Danh Mục Sản Phẩm
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header al-right">
                    <button type="button" id="btn_new" class="btn btn-primary">Thêm mới</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Logo</th>
                                <th>Loại</th>
                                <th>Vị trí hiển thị</th>
                                <th>Ngày tạo</th>
                                <th width="15px"></th>
                                <th>Tiêu đề</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($data as $value) {

                              //type
                              $type = '';
                              if($value['type'] == 0){
                                $type ='Danh mục sản phẩm';
                              }else if($value['type'] == 1){
                                $type ='Loại sản phẩm';
                              }if($value['type'] == 2){
                                $type ='Kiểu sản phẩm';
                              }
                              //display_position
                              $position = '';
                              if($value['display_position'] == 0){
                                $position ='--';
                              }else if($value['display_position'] == 1){
                                $position ='Trang chủ (danh mục trên)';
                              }if($value['display_position'] == 2){
                                $position ='Trang chủ (danh mục dưới)';
                              }
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><img src="<?php echo $value['medium_logo']; ?>" width="100px"/></td>
                                    <td><?php echo $type; ?></td>
                                    <td><?php echo $position; ?></td>
                                    <td><?php echo $value['created_date']; ?></td>
                                    <td class="mul">
                                      <img src="<?php echo base_url() . 'assets/img/lang/vi.png'?>" height="30px">
                                      <hr>
	                                    <img src="<?php echo base_url() . 'assets/img/lang/en.png'?>" height="30px">
                                    </td>
                                    <td class="mul">
                                      <p><?php
                                        $key = 'vi';
                                        $element = 'dsp_title';
                                        if($value['detail'][$key][$element] == null || trim($value['detail'][$key][$element]) == ''){
                                          echo '(Not set)';
                                        }else{
                                          echo $value['detail'][$key][$element];
                                        }
                                      ?></p>
                                      <hr>
                                      <p><?php
                                        $key = 'en';
                                        if($value['detail'][$key][$element] == null || trim($value['detail'][$key][$element]) == ''){
                                          echo '(Not set)';
                                        }else{
                                          echo $value['detail'][$key][$element];
                                        }
                                      ?></p>
                                    </td>
                                    <td style="padding-top: 28px">
                                        <a href="#" class="btn-edit" data-index="<?php echo $count - 1; ?>"><i class="fa fa-edit"></i> Sửa</a>
                                        <a href="#" class="btn-remove" data-index="<?php echo $count - 1; ?>"><i class="fa fa-remove"></i> Xóa</a>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>#</th>
                              <th>Ảnh</th>
                              <th>Mã sản phẩm</th>
                              <th>Giá bán</th>
                              <th>Ngày tạo</th>
                              <th width="15px"></th>
                              <th>Tiêu đề</th>
                              <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade" id="modal-data">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Default Modal</h4>
          </div>
          <div class="modal-body">
            <div class="row" id="common">
              <div class="col-md-12">
                <p><b>Cài đặt chung</b></p>
                <div class="content-wrap">
                  <div class="form-group">
                      <label>Danh mục cha</label>
                      <select id="com_parent" class="form-control select2" data-placeholder="Chọn danh mục cha" style="width: 100%;">
                        <option value="0"> -- </option>
                        <?php
                          foreach($parent as $value){
                            ?>
                            <option value="<?php echo $value['product_category_gid']?>"> <?php echo $value['title']?> </option>
                            <?php
                          }
                        ?>
                      </select>
                  </div>

                  <div class="form-group pos-related" id="medium_logo">
                  </div>
                  <div class="form-group pos-related" id="big_logo">
                  </div>
                  <div class="form-group">
                      <label>Loại danh mục</label>
                      <div class="radio">
                          <label>
                            <input type="radio" name="type" value="0" checked="">
                            Danh mục sản phẩm
                          </label>
                          <label>
                            <input type="radio" name="type" value="1">
                            Loại sản phẩm
                          </label>
                          <label>
                            <input type="radio" name="tye" value="2">
                            Kiểu sản phẩm
                          </label>
                        </div>
                  </div>
                  <div class="form-group">
                      <label>Vị trí hiển thị</label>
                      <div class="radio">
                          <label>
                            <input type="radio" name="display_position" value="0" checked="">
                            Không
                          </label>
                          <label>
                            <input type="radio" name="display_position" value="1">
                            Trang chủ (danh mục trên)
                          </label>
                          <label>
                            <input type="radio" name="display_position" value="2">
                            Trang chủ (danh mục dưới)
                          </label>
                        </div>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <div class="row" id="multilang">
              <div class="col-md-6">
                <img src="<?php echo base_url() . 'assets/img/lang/vi.png'?>" width="25px"/> <b>Tiếng việt</b>
                <div><br></div>
                <div class="content-wrap">
                  <div class="form-group">
                      <label>Tên sản phẩm</label>
                      <input id="vi_title" name="data[title]" type="text" class="form-control" placeholder="Tên sản phẩm">
                  </div>
                  <div class="form-group">
                      <label>Tên rút gọn</label>
                      <input id="vi_alias" type="text" class="form-control" placeholder="Tên rút gọn">
                      <p class="help-block">Tên rút gọn không được trùng.</p>
                  </div>
                  <div class="form-group">
                      <label>Slogan</label>
                      <!-- <textarea id="vi_intro" name="ckeditor" class="form-control" rows="10" placeholder="Mô tả"></textarea> -->
                      <div class="document-editor vi_slogan">
                        <div class="toolbar-container"></div>
                        <div class="content-container">
                          <div id="vi_slogan" class="editor"></div>
                        </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <label>Mô tả</label>
                      <!-- <textarea id="vi_fulltext" name="ckeditor" class="form-control" rows="10" placeholder="Chi tiết sản phẩm"></textarea> -->
                      <div class="document-editor vi_intro">
                        <div class="toolbar-container"></div>
                        <div class="content-container">
                          <div id="vi_intro" class="editor"></div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <img src="<?php echo base_url() . 'assets/img/lang/en.png'?>" width="25px"/> <b>Tiếng anh</b>
                <div><br></div>
                <div class="content-wrap">
                  <div class="form-group">
                      <label>Tên sản phẩm</label>
                      <input id="en_title" name="data[title]" type="text" class="form-control" placeholder="Tên sản phẩm">
                  </div>
                  <div class="form-group">
                      <label>Tên rút gọn</label>
                      <input id="en_alias" type="text" class="form-control" placeholder="Tên rút gọn">
                      <p class="help-block">Tên rút gọn không được trùng.</p>
                  </div>
                  <div class="form-group">
                      <label>Slogan</label>
                      <div class="document-editor en_slogan">
                        <div class="toolbar-container"></div>
                        <div class="content-container">
                          <div id="en_slogan" class="editor"></div>
                        </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <label>Mô tả</label>
                      <div class="document-editor en_intro">
                        <div class="toolbar-container"></div>
                        <div class="content-container">
                          <div id="en_intro" class="editor"></div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="btn-save">Lưu</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Style -->
<style type="text/css">
    .select2-container .select2-selection--single {
        height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 31px;
    }
    table .mul{
      padding-top: 15px;
    }
    table hr{
      margin: 0px;
    }
</style>
<!-- page script -->
<script>
    var mode = 'new';
    var key = 0;
    var key_str = 'product_category_gid';
    var url_update = "<?php echo base_url() ?>AdminProductcat/execute_query";
    var url_delete = "<?php echo base_url() ?>AdminProductcat/delete";
    var url_refresh = "<?php echo base_url() ?>AdminProductcat/get_data";
    //Convert php array to js array
    <?php
    $js_array = json_encode($data);
    echo "var arr_data = " . $js_array . ";\n";
    ?>

    $(document).ready(function () {

        //First init
        first_init();

        //Click new button
        $(document).on('click', '#btn_new', function (e) {
            //Set mode and key value
            mode = 'new';
            key = 0;
            //Change modal title
            $('.modal-title').html('Thêm mới');

            /*Set modal custom data*/
            var data = {
              'parent_gid' : 0,
              'medium_logo' : '',
              'big_logo' : '',
              'type' : '0',
              'display_position' : 0,
              'vi' : {
                'title' : '',
                'alias' : '',
                'slogan' : '',
                'intro' : ''
              },
              'en' : {
                'title' : '',
                'alias' : '',
                'slogan' : '',
                'intro' : ''
              }
            };
            update_modal_data(data);
            /*End*/

            //Open modal
            $("#modal-data").modal();
        });
        //Click edit button
        $(document).on('click', '.btn-edit', function (e) {
            //Set mode and key value
            mode = 'edit';
            var index = $(this).attr('data-index');
            var obj_data = arr_data[index];
            key = obj_data[key_str];
            //Change modal title
            $('.modal-title').html('Chỉnh sửa');

            /*Set modal custom data*/
            var vi_detail = obj_data['detail']['vi'];
            var en_detail = obj_data['detail']['en'];
            var data = {
              'parent_gid' : obj_data['parent_gid'],
              'medium_logo' : obj_data['medium_logo'],
              'big_logo' : obj_data['big_logo'],
              'type' : obj_data['type'],
              'display_position' : obj_data['display_position'],
              'vi' : {
                'title' : vi_detail['title'],
                'alias' : vi_detail['alias'],
                'slogan' : vi_detail['slogan'],
                'intro' : vi_detail['intro_text']
              },
              'en' : {
                'title' : en_detail['title'],
                'alias' : en_detail['alias'],
                'slogan' : en_detail['slogan'],
                'intro' : en_detail['intro_text']
              }
            };
            update_modal_data(data);
            /*End*/

            //Open modal
            $("#modal-data").modal();
        });
        //Click remove button
        $(document).on('click', '.btn-remove', function (e) {
            if (confirm("Are you sure you want to delete this?")) {
                //Get id
                var index = $(this).attr('data-index');
                //Get edit data
                var obj_data = arr_data[index];
                //Project id
                key = obj_data[key_str];

                //Create post data
                var fd = new FormData();
                fd.append(key_str, key);

                $.ajax({
                    type: 'POST',
                    url: url_delete,
                    data: fd,
                    processData: false,
                    contentType: false,
                    cache:false,
                    enctype: 'multipart/form-data',
                    success: function (objData) {
                        //Refresh data
                        refresh_data();
                    },
                    error: function (jqXHR, exception) {
                        alert('Có lỗi. Thử lại sau.');
                    }
                });
            }
        });
        //Modal save button click
        $(document).on('click', '#btn-save', function (e) {

            //If add new then check if duplicate alias
            if (verify_data() === false) {
                return;
            }
            //Hide modal
            $('#modal-data').modal('toggle');

            //Ajax
            $.ajax({
                type: 'POST',
                url: url_update,
                data: get_update_data(),
                processData: false,
                contentType: false,
                cache:false,
                enctype: 'multipart/form-data',
                success: function (objData) {
                  if(objData !== ''){
                    objData = $.parseJSON(objData);
                    alert(objData["messsage"]);
                  }else{
                    //Refresh data
                    refresh_data();
                  }
                },
                error: function (jqXHR, exception) {
                    alert('Có lỗi. Thử lại sau.');
                }
            });
        });
    });
    //Update modal data
    function update_modal_data(data){
      //common
      var tmp = data;
      sigf_init('Logo Nhỏ', 'medium_logo', tmp['medium_logo']);
      sigf_init('Logo to', 'big_logo', tmp['big_logo']);

      //parent
      $('#com_parent').val(tmp['parent_gid']).trigger('change');
      $("input[name=type][value='"+tmp['type']+"']").prop("checked",true);
      $("input[name=display_position][value='"+tmp['display_position']+"']").prop("checked",true);


      //vi
      tmp = data['vi'];
      $('#vi_title').val(tmp['title']);
      $('#vi_alias').val(tmp['alias']);
      arr_ckeditor['vi_slogan'].setData( tmp['slogan'].replace(/\\/g, '') );
      arr_ckeditor['vi_intro'].setData( tmp['intro'].replace(/\\/g, '') );

      //en
      tmp = data['en'];
      $('#en_title').val(tmp['title']);
      $('#en_alias').val(tmp['alias']);
      arr_ckeditor['en_slogan'].setData( tmp['slogan'].replace(/\\/g, '') );
      arr_ckeditor['en_intro'].setData( tmp['intro'].replace(/\\/g, '') );
    }
    //Verify data
    function verify_data() {
        /*===Common===*/

        //parent
        if ($('#com_parent option:selected').val() === '') {
            alert('Hãy chọn danh mục cha!');
            return false;
        }
        //medium logo
        if ($('#medium_logo input').val() === '') {
            alert('Hãy thêm ảnh logo nhỏ!');
            return false;
        }
        //big logo
        if ($('#big_logo input').val() === '') {
            alert('Hãy thêm ảnh logo to!');
            return false;
        }
        /*===End Common===*/

        /*===vi===*/
        if ($('#vi_title').val() === '') {
            alert('Tiêu đề tiếng việt không được để trống!');
            return false;
        }
        if ($('#vi_alias').val() === '') {
            alert('Alias sản phẩm không được để trống!');
            return false;
        }
        /*===end vi===*/

        /*===en===*/
        if ($('#en_title').val() === '') {
            alert('Tiêu đề tiếng anh không được để trống!');
            return false;
        }
        if ($('#en_alias').val() === '') {
            alert('Alias sản phẩm không được để trống!');
            return false;
        }
        //compare title in vi and en
        if ($('#vi_alias').val() === $('#en_alias').val()) {
            alert('Tiêu đề tiếng việt và tiếng ảnh không được trùng!');
            return false;
        }
        /*===end en===*/
    }
    //Get data
    function refresh_data() {
        //Refresh data
        $.ajax({
            type: 'GET',
            url: url_refresh,
            success: function (data) {
                data = $.parseJSON(data);
                arr_data = data;
                //Fill datatable
                $('#example1').DataTable().destroy();
                $('#example1 tbody').html('');
                var html = '';
                var detail = [];
                var val = '';
                for (var i = 0; i < data.length; i++) {
                    var type = '' ;
                    if(data[i]['type'] === "0"){
                      type = 'Danh mục sản phẩm';
                    }else if(data[i]['type'] === "1"){
                      type = 'Loại sản phẩm';
                    }else if(data[i]['type'] === "2"){
                      type = 'Kiểu sản phẩm';
                    }
                    var position = '';
                    if(data[i]['display_position'] === "0"){
                      position = '--';
                    }else if(data[i]['display_position'] === "1"){
                      position = 'Trang chủ (danh mục trên)';
                    }else if(data[i]['display_position'] === "2"){
                      position = 'Trang chủ (danh mục dưới)';
                    }
                    html += '<tr>'
                            + '<td>' + (i + 1) + '</td>'
                            + '<td><img src="' + data[i]['medium_logo'] + '" width="100px"/></td>'
                            + '<td>' + type + '</td>'
                            + '<td>' + position + '</td>'
                            + '<td>' + data[i]['created_date'] + '</td>'
                            + '<td class="mul">'
                            + '      <img src="<?php echo base_url() . 'assets/img/lang/vi.png'?>" height="30px">'
                            + '      <hr>'
                            + '      <img src="<?php echo base_url() . 'assets/img/lang/en.png'?>" height="30px">'
                            + '</td>';
                    html += multilanguage_item(data[i]['detail'], 0, 'dsp_title');
                    html += '<td style="padding-top: 28px">'
                            + '<a href="#" class="btn-edit" data-index="' + i + '"><i class="fa fa-edit"></i> Sửa</a>'
                            + ' <a href="#" class="btn-remove" data-index="' + i + '"><i class="fa fa-remove"></i> Xóa</a>'
                            + '</td>'
                            + '</tr>';
                }
                $('#example1 tbody').html(html);
                //Initialize datatable
                $('#example1').DataTable().draw();

            },
            error: function () {
                alert('Có lỗi. Thử lại sau.');
            }
        });
    }
    //Get update data
    function get_update_data(){
      var fd = new FormData();

      //Common
      fd.append('parent_gid', $("#com_parent option:selected").val());

      var url = "<?php echo base_url();?>";
      var path = $('#medium_logo input').val();
      if(path !== ''){
          path = path.substring(url.length, path.length);
      }
      fd.append('medium_logo', path);

      //big_logo
      path = $('#big_logo input').val();
      if(path !== ''){
          path = path.substring(url.length, path.length);
      }
      fd.append('big_logo', path);

      fd.append('type', $('input[name=type]:checked').val());
      fd.append('display_position', $('input[name=display_position]:checked').val());
      fd.append(key_str, key);

      //vi
      fd.append('vi_title', $('#vi_title').val());
      fd.append('vi_alias', $('#vi_alias').val());
      fd.append('vi_slogan', arr_ckeditor['vi_slogan'].getData());
      fd.append('vi_intro', arr_ckeditor['vi_intro'].getData());
      //en
      fd.append('en_title', $('#en_title').val());
      fd.append('en_alias', $('#en_alias').val());
      fd.append('en_slogan', arr_ckeditor['en_slogan'].getData());
      fd.append('en_intro', arr_ckeditor['en_intro'].getData());

      return fd;
    }
    //First init
    function first_init(){
      //Initialize datatable
      $(function () {
          $('#example1').DataTable();
      });
      //Title alias
      $("#vi_title").on('input', function () {
          $("#vi_alias").val(convert_vi_to_en($(this).val()));
      });
      $("#en_title").on('input', function () {
          $("#en_alias").val(convert_vi_to_en($(this).val()));
      });
      //Init ckeditor
      init_ckeditor('vi_slogan');
      init_ckeditor('vi_intro');
      init_ckeditor('en_slogan');
      init_ckeditor('en_intro');
      //sigf events
      sigf_events();
    }
</script>
