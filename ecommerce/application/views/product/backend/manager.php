<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quản lý Sản Phẩm
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header al-right">
                    <button type="button" id="btn_new" class="btn btn-primary">Thêm mới</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Ảnh</th>
                                <th>Mã sản phẩm</th>
                                <th>Giá bán</th>
                                <th>Ngày tạo</th>
                                <th width="15px"></th>
                                <th>Tiêu đề</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($data as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><img src="<?php echo $value['logo']; ?>" width="100px"/></td>
                                    <td><?php echo $value['code']; ?></td>
                                    <td><?php echo $value['price']; ?></td>
                                    <td><?php echo $value['created_date']; ?></td>
                                    <td class="mul">
                                      <img src="<?php echo base_url() . 'assets/img/lang/vi.png'?>" height="30px">
                                      <hr>
	                                    <img src="<?php echo base_url() . 'assets/img/lang/en.png'?>" height="30px">
                                    </td>
                                    <td class="mul">
                                      <p><?php
                                        $key = 'vi';
                                        $element = 'title';
                                        if($value['detail'][$key][$element] == null || trim($value['detail'][$key][$element]) == ''){
                                          echo '(Not set)';
                                        }else{
                                          echo $value['detail'][$key][$element];
                                        }
                                      ?></p>
                                      <hr>
                                      <p><?php
                                        $key = 'en';
                                        if($value['detail'][$key][$element] == null || trim($value['detail'][$key][$element]) == ''){
                                          echo '(Not set)';
                                        }else{
                                          echo $value['detail'][$key][$element];
                                        }
                                      ?></p>
                                    </td>
                                    <td style="padding-top: 28px">
                                        <a href="#" class="btn-edit" data-index="<?php echo $count - 1; ?>"><i class="fa fa-edit"></i> Sửa</a>
                                        <a href="#" class="btn-remove" data-index="<?php echo $count - 1; ?>"><i class="fa fa-remove"></i> Xóa</a>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>#</th>
                              <th>Ảnh</th>
                              <th>Mã sản phẩm</th>
                              <th>Giá bán</th>
                              <th>Ngày tạo</th>
                              <th width="15px"></th>
                              <th>Tiêu đề</th>
                              <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade" id="modal-data">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Default Modal</h4>
          </div>
          <div class="modal-body">
            <div class="row" id="common">
              <div class="col-md-12">
                <p><b>Cài đặt chung</b></p>
                <div class="content-wrap">
                  <div class="form-group">
                      <label>Danh mục sản phẩm</label>
                      <select id="com_productcat" class="form-control select2" data-placeholder="Chọn danh mục sản phẩm" style="width: 100%;">
                        <option value=""> -- Chọn -- </option>
                        <?php
                          foreach($productcat as $value){
                            ?>
                            <option value="<?php echo $value['product_category_gid']?>"> <?php echo $value['title']?> </option>
                            <?php
                          }
                        ?>
                      </select>
                  </div>
                  <div class="form-group">
                      <label>Màu nền</label>
                      <input id="bg_color" type="text" class="form-control">
                  </div>
                  <div class="form-group">
                      <label>Mã sản phẩm</label>
                      <input id="code" type="text" class="form-control">
                  </div>
                  <div class="form-group">
                      <label>Giá bán (vnđ)</label>
                      <input id="price" type="number" class="form-control">
                  </div>
                  <div class="form-group">
                      <label>Số lượng có sẵn</label>
                      <input id="warehouse" type="number" class="form-control">
                  </div>
                  <div class="form-group pos-related" id="logo">
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group pos-related mulf-upload" id="slide1">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group pos-related mulf-upload" id="slide2">
                      </div>
                    </div>
                  </div>
                  <div id="mulf-css"></div>
                  <div class="form-group multiselect-wrap">
                      <label>Màu sắc</label>
                      <select id="com_color" multiple="multiple" with="100%">
                        <?php
                          foreach($colors as $value){
                            ?>
                            <option value="<?php echo $value['product_properties_gid']?>"> <?php echo $value['title']?> </option>
                            <?php
                          }
                        ?>
                      </select>
                  </div>
                
                  <div class="form-group multiselect-wrap">
                      <label>Chất liệu</label>
                      <select id="com_material" multiple="multiple" with="100%">
                        <?php
                          foreach($material as $value){
                            ?>
                            <option value="<?php echo $value['product_properties_gid']?>"> <?php echo $value['title']?> </option>
                            <?php
                          }
                        ?>
                      </select>
                  </div>
                  <div class="form-group multiselect-wrap">
                      <label>Kích thước</label>
                      <select id="com_size" multiple="multiple" with="100%">
                        <?php
                          foreach($size as $value){
                            ?>
                            <option value="<?php echo $value['product_properties_gid']?>"> <?php echo $value['title']?> </option>
                            <?php
                          }
                        ?>
                      </select>
                  </div>
                  <div class="form-group">
                      <label>Sản phẩm mới</label>
                      <div class="radio">
                          <label>
                            <input type="radio" name="is_new" value="0" checked="">
                            No
                          </label>
                          <label>
                            <input type="radio" name="is_new" value="1">
                            Yes
                          </label>
                        </div>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <div class="row" id="multilang">
              <div class="col-md-6">
                <img src="<?php echo base_url() . 'assets/img/lang/vi.png'?>" width="25px"/> <b>Tiếng việt</b>
                <div><br></div>
                <div class="content-wrap">
                  <div class="form-group">
                      <label>Tên sản phẩm</label>
                      <input id="vi_title" name="data[title]" type="text" class="form-control" placeholder="Tên sản phẩm">
                  </div>
                  <div class="form-group">
                      <label>Tên rút gọn</label>
                      <input id="vi_alias" type="text" class="form-control" placeholder="Tên rút gọn">
                      <p class="help-block">Tên rút gọn không được trùng.</p>
                  </div>
                  <div class="form-group">
                      <label>Mô tả</label>
                      <!-- <textarea id="vi_intro" name="ckeditor" class="form-control" rows="10" placeholder="Mô tả"></textarea> -->
                      <div class="document-editor vi_intro">
                        <div class="toolbar-container"></div>
                        <div class="content-container">
                          <div id="vi_intro" class="editor"></div>
                        </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <label>Chi tiết sản phẩm</label>
                      <!-- <textarea id="vi_fulltext" name="ckeditor" class="form-control" rows="10" placeholder="Chi tiết sản phẩm"></textarea> -->
                      <div class="document-editor vi_fulltext">
                        <div class="toolbar-container"></div>
                        <div class="content-container">
                          <div id="vi_fulltext" class="editor"></div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <img src="<?php echo base_url() . 'assets/img/lang/en.png'?>" width="25px"/> <b>Tiếng anh</b>
                <div><br></div>
                <div class="content-wrap">
                  <div class="form-group">
                      <label>Tên sản phẩm</label>
                      <input id="en_title" name="data[title]" type="text" class="form-control" placeholder="Tên sản phẩm">
                  </div>
                  <div class="form-group">
                      <label>Tên rút gọn</label>
                      <input id="en_alias" type="text" class="form-control" placeholder="Tên rút gọn">
                      <p class="help-block">Tên rút gọn không được trùng.</p>
                  </div>
                  <div class="form-group">
                      <label>Mô tả</label>
                      <!-- <textarea id="en_intro" name="editor1" class="form-control" rows="10" placeholder="Mô tả"></textarea> -->
                      <div class="document-editor en_intro">
                        <div class="toolbar-container"></div>
                        <div class="content-container">
                          <div id="en_intro" class="editor"></div>
                        </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <label>Chi tiết sản phẩm</label>
                      <!-- <textarea id="en_fulltext" name="editor1" class="form-control" rows="10" placeholder="Chi tiết sản phẩm"></textarea> -->
                      <div class="document-editor en_fulltext">
                        <div class="toolbar-container"></div>
                        <div class="content-container">
                          <div id="en_fulltext" class="editor"></div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="btn-save">Lưu</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Style -->
<style type="text/css">
    .select2-container .select2-selection--single {
        height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 31px;
    }
    table .mul{
      padding-top: 15px;
    }
    table hr{
      margin: 0px;
    }
    .multiselect-wrap .btn-group, .multiselect-wrap button, .multiselect-container{
      width: 100%
    }
    .multiselect-wrap button{
      text-align: left;
    }
    .btn .caret {
        float: right;
        margin-top: 8px;
    }
</style>
<!-- page script -->
<script>
    var mode = 'new';
    var key = 0;
    var key_str = 'product_gid';
    var url_update = "<?php echo base_url() ?>AdminProduct/execute_query";
    var url_delete = "<?php echo base_url() ?>AdminProduct/delete";
    var url_refresh = "<?php echo base_url() ?>AdminProduct/get_data";
    //Convert php array to js array
    <?php
    $js_array = json_encode($data);
    echo "var arr_data = " . $js_array . ";\n";
    ?>

    $(document).ready(function () {

        //First init
        first_init();

        //Click new button
        $(document).on('click', '#btn_new', function (e) {
            //Set mode and key value
            mode = 'new';
            key = 0;
            //Change modal title
            $('.modal-title').html('Thêm mới');

            /*Set modal custom data*/
            var data = {
              'productcat' : 0,
              'logo' : '',
              'bg_color' : '',
              'slide1' : '',
              'slide2' : '',
              'code' : Math.floor(Math.random() * 9000000) + 1000000,
              'price' : '0',
              'color' : '',
              'material' : '',
              'size' : '',
              'warehouse' : '0',
              'is_new' : '0',
              'vi' : {
                'title' : '',
                'alias' : '',
                'intro' : '',
                'fulltext' : ''
              },
              'en' : {
                'title' : '',
                'alias' : '',
                'intro' : '',
                'fulltext' : ''
              }
            };
            update_modal_data(data);
            /*End*/

            //Open modal
            $("#modal-data").modal();
        });
        //Click edit button
        $(document).on('click', '.btn-edit', function (e) {
            //Set mode and key value
            mode = 'edit';
            var index = $(this).attr('data-index');
            var obj_data = arr_data[index];
            key = obj_data[key_str];
            //Change modal title
            $('.modal-title').html('Chỉnh sửa');

            /*Set modal custom data*/
            var vi_detail = obj_data['detail']['vi'];
            var en_detail = obj_data['detail']['en'];
            var data = {
              'productcat' : obj_data['product_category_gid'],
              'logo' : obj_data['logo'],
              'bg_color' : obj_data['bg_color'],
              'slide1' : obj_data['slide1'],
              'slide2' : obj_data['slide2'],
              'code' : obj_data['code'],
              'price' : obj_data['price_vn'],
              'color' : obj_data['color_gid'],
              'material' : obj_data['material_gid'],
              'size' : obj_data['size_gid'],
              'warehouse' : obj_data['warehouse'],
              'is_new' : obj_data['is_new'],
              'vi' : {
                'title' : vi_detail['title'],
                'alias' : vi_detail['alias'],
                'intro' : vi_detail['intro_text'],
                'fulltext' : vi_detail['full_text']
              },
              'en' : {
                'title' : en_detail['title'],
                'alias' : en_detail['alias'],
                'intro' : en_detail['intro_text'],
                'fulltext' : en_detail['full_text']
              }
            };
            update_modal_data(data);
            /*End*/

            //Open modal
            $("#modal-data").modal();
        });
        //Click remove button
        $(document).on('click', '.btn-remove', function (e) {
            if (confirm("Are you sure you want to delete this?")) {
                //Get id
                var index = $(this).attr('data-index');
                //Get edit data
                var obj_data = arr_data[index];
                //Project id
                key = obj_data[key_str];

                //Create post data
                var fd = new FormData();
                fd.append(key_str, key);

                $.ajax({
                    type: 'POST',
                    url: url_delete,
                    data: fd,
                    processData: false,
                    contentType: false,
                    cache:false,
                    enctype: 'multipart/form-data',
                    success: function (objData) {
                        //Refresh data
                        refresh_data();
                    },
                    error: function (jqXHR, exception) {
                        alert('Có lỗi. Thử lại sau.');
                    }
                });
            }
        });
        //Modal save button click
        $(document).on('click', '#btn-save', function (e) {

            //If add new then check if duplicate alias
            if (verify_data() === false) {
                return;
            }
            //Hide modal
            $('#modal-data').modal('toggle');

            //Ajax
            $.ajax({
                type: 'POST',
                url: url_update,
                data: get_update_data(),
                processData: false,
                contentType: false,
                cache:false,
                enctype: 'multipart/form-data',
                success: function (objData) {
                  if(objData !== ''){
                    objData = $.parseJSON(objData);
                    alert(objData["messsage"]);
                  }else{
                    //Refresh data
                    refresh_data();
                  }
                },
                error: function (jqXHR, exception) {
                    alert('Có lỗi. Thử lại sau.');
                }
            });
        });
    });
    //Update modal data
    function update_modal_data(data){
      //common
      var tmp = data;
      sigf_init('Logo', 'logo', tmp['logo']);
      //Add multi image upload
      mulf_init('Hình ảnh mô tả (slide1)','slide1',tmp['slide1']);
      mulf_init('Hình ảnh mô tả (slide2)','slide2',tmp['slide2']);

      if(tmp['productcat'] == 0){
          $('#com_productcat').val($('#com_productcat option:eq(0)').val()).trigger('change');
      }else{
        $('#com_productcat').val(tmp['productcat']).trigger('change');
      }
      $('#code').val(tmp['code']);
      $('#bg_color').val(tmp['bg_color']);
      $('#bg_color').css('background-color', tmp['bg_color']);
      $('#price').val(tmp['price']);
      $('#warehouse').val(tmp['warehouse']);
      $("input[name=is_new][value='"+tmp['is_new']+"']").prop("checked",true);
      if(tmp['color'] == ''){
        var arr_tmp = [];
          $('#com_color option').each(function(){
            arr_tmp.push($(this).val());
          });
          $('#com_color').multiselect('deselect', arr_tmp);
      }else{
        $('#com_color').multiselect('select', tmp['color'].split(','));
      }
      if(tmp['material'] == ''){
        var arr_tmp = [];
          $('#com_material option').each(function(){
            arr_tmp.push($(this).val());
          });
          $('#com_material').multiselect('deselect', arr_tmp);
      }else{
        $('#com_material').multiselect('select', tmp['material'].split(','));
      }
      if(tmp['size'] == ''){
        var arr_tmp = [];
          $('#com_size option').each(function(){
            arr_tmp.push($(this).val());
          });
          $('#com_size').multiselect('deselect', arr_tmp);
      }else{
        $('#com_size').multiselect('select', tmp['size'].split(','));
      }

      //vi
      tmp = data['vi'];
      $('#vi_title').val(tmp['title']);
      $('#vi_alias').val(tmp['alias']);
      $('.editor').html('<p>Demo</p>');
      arr_ckeditor['vi_intro'].setData( tmp['intro'].replace(/\\/g, '') );
      arr_ckeditor['vi_fulltext'].setData( tmp['fulltext'].replace(/\\/g, '') );

      //en
      tmp = data['en'];
      $('#en_title').val(tmp['title']);
      $('#en_alias').val(tmp['alias']);
      arr_ckeditor['en_intro'].setData( tmp['intro'].replace(/\\/g, '') );
      arr_ckeditor['en_fulltext'].setData( tmp['fulltext'].replace(/\\/g, '') );
    }
    //Verify data
    function verify_data() {
        /*===Common===*/

        //productcat
        if ($('#com_productcat option:selected').val() === '') {
            alert('Hãy chọn danh mục sản phẩm!');
            return false;
        }
        //code
        if ($('#code').val() === '') {
            alert('Hãy thêm mã sản phẩm!');
            return false;
        }
        //price
        if ($('#price').val() === '') {
            alert('Hãy nhập giá bán!');
            return false;
        }
        //wearhouse
        if ($('#wearhouse').val() === '') {
            alert('Hãy thêm số lượng tồn kho!');
            return false;
        }
        //logo
        if ($('#logo input').val() === '') {
            alert('Hãy thêm ảnh logo!');
            return false;
        }
        //slide 1
        var slide = '';
        $('#slide1 .mulf_input').each(function(){
          //Get image path
          slide += $(this).val();
        });
        if(slide === ''){
          alert('Hãy thêm ảnh cho slide1!');
          return false;
        }
        //slide 2
        slide = '';
        $('#slide2 .mulf_input').each(function(){
          //Get image path
          slide += $(this).val();
        });
        if(slide === ''){
          alert('Hãy thêm ảnh cho slide2!');
          return false;
        }
        //color
        if ($('#com_color option:selected').val() === '') {
            alert('Hãy chọn màu sắc!');
            return false;
        }
        //material
        if ($('#com_material option:selected').val() === '') {
            alert('Hãy chọn nguyên liệu!');
            return false;
        }
        //size
        if ($('#com_size option:selected').val() === '') {
            alert('Hãy chọn kích thước!');
            return false;
        }
        /*===End Common===*/

        /*===vi===*/
        if ($('#vi_title').val() === '') {
            alert('Tiêu đề tiếng việt không được để trống!');
            return false;
        }
        if ($('#vi_alias').val() === '') {
            alert('Alias sản phẩm không được để trống!');
            return false;
        }
        /*===end vi===*/

        /*===en===*/
        if ($('#en_title').val() === '') {
            alert('Tiêu đề tiếng anh không được để trống!');
            return false;
        }
        if ($('#en_alias').val() === '') {
            alert('Alias sản phẩm không được để trống!');
            return false;
        }
        //compare title in vi and en
        if ($('#vi_alias').val() === $('#en_alias').val()) {
            alert('Tiêu đề tiếng việt và tiếng ảnh không được trùng!');
            return false;
        }
        /*===end en===*/
    }
    //Get data
    function refresh_data() {
        //Refresh data
        $.ajax({
            type: 'GET',
            url: url_refresh,
            success: function (data) {
                data = $.parseJSON(data);
                arr_data = data;
                //Fill datatable
                $('#example1').DataTable().destroy();
                $('#example1 tbody').html('');
                var html = '';
                var detail = [];
                var val = '';
                for (var i = 0; i < data.length; i++) {
                    html += '<tr>'
                            + '<td>' + (i + 1) + '</td>'
                            + '<td><img src="' + data[i]['logo'] + '" width="100px"/></td>'
                            + '<td>' + data[i]['code'] + '</td>'
                            + '<td>' + data[i]['price'] + '</td>'
                            + '<td>' + data[i]['created_date'] + '</td>'
                            + '<td class="mul">'
                            + '      <img src="<?php echo base_url() . 'assets/img/lang/vi.png'?>" height="30px">'
                            + '      <hr>'
                            + '      <img src="<?php echo base_url() . 'assets/img/lang/en.png'?>" height="30px">'
                            + '</td>';
                    html += multilanguage_item(data[i]['detail'], 0, 'title');
                    html += '<td style="padding-top: 28px">'
                            + '<a href="#" class="btn-edit" data-index="' + i + '"><i class="fa fa-edit"></i> Sửa</a>'
                            + ' <a href="#" class="btn-remove" data-index="' + i + '"><i class="fa fa-remove"></i> Xóa</a>'
                            + '</td>'
                            + '</tr>';
                }
                $('#example1 tbody').html(html);
                //Initialize datatable
                $('#example1').DataTable().draw();

            },
            error: function () {
                alert('Có lỗi. Thử lại sau.');
            }
        });
    }
    //Get update data
    function get_update_data(){
      var fd = new FormData();

      //Common
      var url = "<?php echo base_url();?>";
      var path = $('#logo input').val();
      if(path !== ''){
          path = path.substring(url.length, path.length);
      }
      fd.append('logo', path);

      //Slide 1
      var slide = '';
      $('#slide1 .mulf_input').each(function(){
        //Get image path
        var url = "<?php echo base_url();?>";
        var path = $(this).val();
        if(path !== ''){
            slide += path.substring(url.length, path.length) + '||';
        }
      });
      fd.append('slide1', slide.substring(0, slide.length - 2));
      //Slide 2
      slide = '';
      $('#slide2 .mulf_input').each(function(){
        //Get image path
        var url = "<?php echo base_url();?>";
        var path = $(this).val();
        if(path !== ''){
            slide += path.substring(url.length, path.length) + '||';
        }
      });
      fd.append('slide2', slide.substring(0, slide.length - 2));

      fd.append('code', $('#code').val());
      fd.append('bg_color', $('#bg_color').val());
      fd.append('price', $('#price').val());
      fd.append('warehouse', $('#warehouse').val());
      fd.append('product_category_gid', $("#com_productcat option:selected").val());
      fd.append('is_new', $('input[name=is_new]:checked').val());
      fd.append(key_str, key);

      //color
      var tmp = '';
      $('#com_color option:selected').each(function(){
        tmp += $(this).val() + ',';
      });
      fd.append('color_gid', tmp.substring(0, tmp.length - 1));
      tmp = '';
      $('#com_material option:selected').each(function(){
        tmp += $(this).val() + ',';
      });
      fd.append('material_gid', tmp.substring(0, tmp.length - 1));
      tmp = '';
      $('#com_size option:selected').each(function(){
        tmp += $(this).val() + ',';
      });
      fd.append('size_gid', tmp.substring(0, tmp.length - 1));

      //vi
      fd.append('vi_title', $('#vi_title').val());
      fd.append('vi_alias', $('#vi_alias').val());
      fd.append('vi_intro', arr_ckeditor['vi_intro'].getData());
      fd.append('vi_fulltext', arr_ckeditor['vi_fulltext'].getData());
      //en
      fd.append('en_title', $('#en_title').val());
      fd.append('en_alias', $('#en_alias').val());
      fd.append('en_intro', arr_ckeditor['en_intro'].getData());
      fd.append('en_fulltext', arr_ckeditor['en_fulltext'].getData());

      return fd;
    }
    //First init
    function first_init(){
      //Initialize datatable
      $(function () {
          $('#example1').DataTable();
      });
      //Title alias
      $("#vi_title").on('input', function () {
          $("#vi_alias").val(convert_vi_to_en($(this).val()));
      });
      $("#en_title").on('input', function () {
          $("#en_alias").val(convert_vi_to_en($(this).val()));
      });
      //Init ckeditor
      //init_ckeditor(['vi_intro', 'vi_fulltext','en_intro', 'en_fulltext']);
      init_ckeditor('vi_intro');
      init_ckeditor('vi_fulltext');
      init_ckeditor('en_intro');
      init_ckeditor('en_fulltext');

      //Add multi image upload
      // mulf_init('Hình ảnh mô tả (slide1)','slide1', []);
      // mulf_init('Hình ảnh mô tả (slide2)','slide2', []);
      //mulf css
      mulf_css('mulf-upload', 'mulf-css');
      //mulf events
      mulf_events();
      //sigf events
      sigf_events();
      //color picker
      $('#bg_color').colorpicker();
      $('#bg_color').on('change', function() {
        $('#bg_color').css('background-color', $(this).val());
      });
      //multiselect set
      //$('#example-select').multiselect('select', ['1', '2', '4']);
      //$('#example-select').multiselect('deselect', ['1', '2', '4']);
      // $('#com_color option:selected').each(function() {
      //                   alert($(this).val());
      //               });
      $('#com_color').multiselect();
      $('#com_material').multiselect();
      $('#com_size').multiselect();
    }
</script>
