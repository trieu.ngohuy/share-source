<div class="container-fluid" id="product-cats-wrap">
  <!-- Breadcrumb and filter -->
  <div class="row">
    <div class="col-md-5 col-lg-5">
      <ul id="breadcrumb">
        <li><a href="#" class="last">(<?php echo count($products)?> <?php echo $translate['results']?>)</a></li>
      </ul>
    </div>
    <div class="col-md-7 col-lg-7">
      <ul id="paging">
        <li>
          <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              <?php echo $translate['sort_by']?>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li v-on:click="order_grid(0)"><a href="#"><?php echo $translate['recommended']?></a></li>
              <li v-on:click="order_grid(1)"><a href="#"><?php echo $translate['high_price']?></a></li>
              <li v-on:click="order_grid(2)" v-on:click="order_grid(0)"><a href="#"><?php echo $translate['low_price']?></a></li>
              <li v-on:click="order_grid(3)"><a href="#"><?php echo $translate['newest']?></a></li>
            </ul>
          </div>
        </li>
        <li v-bind:class="grid_size == 3 ? 'paging active' : 'paging'" v-on:click="change_grid_size(3)"><a href="#">4</a></li>
        <li v-bind:class="grid_size == 4 ? 'paging active' : 'paging'" v-on:click="change_grid_size(4)"><a href="#">3</a></li>
        <li><p><?php echo $translate['view']?>:</p></li>
      </ul>
    </div>
  </div>
  <br><br>
  <!-- Main -->
  <div class="row">
    <div class="col-md-3 col-lg-3" id="left">
      <div id="filter" v-if="filter.length > 0">
        <div class="row">
          <div class="col-md-7 col-lg-7"><?php echo $translate['your_selection']?></div>
          <div class="col-md-5 col-lg-5 al-right">
            <a href="#" class="underline" v-on:click="remove_filter('all')"><?php echo $translate['clear_all']?></a>
          </div>
        </div>
        <ul>
          <li v-for="value in filter">
            <a href="#"><b>{{value.text}}</b></a>
            <img src="<?php echo asset_front_url() . 'img/icon/cancel.png'?>" height="10px" v-on:click="remove_filter(value.type, value.val)"/>
          </li>
        </ul>
      </div>
      <ul>
        <li @mouseover="show_sublist(1)" @mouseleave="left_active_item = 0">
          <a href="#"><?php echo mb_strtoupper($translate['product_type'])?></a>
          <img src="<?php echo asset_front_url() . 'img/icon/plus.png'?>" height="10px" />
          <transition name="fade">
            <ul class="sub-list" v-if="left_active_item == 1">
              <li v-for="value in arr_data.product_type" v-on:click="add_filter('product_type', value.product_category_gid, value.title)">
                <a href="#">{{value.title}}</a>
              </li>
            </ul>
          </transition>
        </li>
        <li @mouseover="show_sublist(2)" @mouseleave="left_active_item = 0">
          <a href="#"><?php echo mb_strtoupper($translate['style'])?></a>
          <img src="<?php echo asset_front_url() . 'img/icon/plus.png'?>" height="10px" />
          <transition name="fade">
            <ul class="sub-list" v-if="left_active_item == 2">
              <li v-for="value in arr_data.style" v-on:click="add_filter('style', value.product_category_gid, value.title)">
                <a href="#">{{value.title}}</a>
              </li>
            </ul>
          </transition>
        </li>
        <li @mouseover="show_sublist(3)" @mouseleave="left_active_item = 0">
          <a href="#"><?php echo mb_strtoupper($translate['color'])?></a>
          <img src="<?php echo asset_front_url() . 'img/icon/plus.png'?>" height="10px" />
          <transition name="fade">
            <ul class="sub-list" v-if="left_active_item == 3">
              <li v-for="value in arr_data.color" v-on:click="add_filter('color', value.product_properties_gid, value.title)">
                <a href="#">{{value.title}}</a>
              </li>
            </ul>
          </transition>
        </li>
        <li @mouseover="show_sublist(4)" @mouseleave="left_active_item = 0">
          <a href="#"><?php echo mb_strtoupper($translate['material'])?></a>
          <img src="<?php echo asset_front_url() . 'img/icon/plus.png'?>" height="10px" />
          <transition name="fade">
            <ul class="sub-list" v-if="left_active_item == 4">
              <li v-for="value in arr_data.material" v-on:click="add_filter('material', value.product_properties_gid, value.title)">
                <a href="#">{{value.title}}</a>
              </li>
            </ul>
          </transition>
        </li>
        <li @mouseover="show_sublist(5)" @mouseleave="left_active_item = 0">
          <a href="#"><?php echo mb_strtoupper($translate['price'])?></a>
          <img src="<?php echo asset_front_url() . 'img/icon/plus.png'?>" height="10px" />
          <div class="row sub-list" id="price" class="sub-list" v-if="left_active_item == 5">
            <div class="col-lg-5">
              <div class="form-group">
                <label for="exampleInputEmail1"><?php echo $translate['low']?></label>
                <input v-model="min_price" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
              </div>
            </div>
            <div class="col-lg-2 al-center"><?php echo $translate['to']?></div>
            <div class="col-lg-5">
              <div class="form-group">
                <label for="exampleInputEmail1"><?php echo $translate['high']?></label>
                <input v-model="max_price" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
              </div>
            </div>
          </div>
        </li>
        <li @mouseover="show_sublist(6)" @mouseleave="left_active_item = 0">
          <a href="#"><?php echo mb_strtoupper($translate['size'])?></a>
          <img src="<?php echo asset_front_url() . 'img/icon/plus.png'?>" height="10px" />
          <transition name="fade">
            <ul class="sub-list" v-if="left_active_item == 6">
              <li v-for="value in arr_data.size" v-on:click="add_filter('size', value.product_properties_gid, value.title)">
                <a href="#">{{value.title}}</a>
              </li>
            </ul>
          </transition>
        </li>
      </ul>
      <button type="button" class="btn btn-default wd-full" v-on:click="remove_filter('all')"><?php echo $translate['clear_all']?></button>
    </div>
    <div class="col-md-9 col-lg-9" id="right">
      <p class="al-center"><?php echo $translate['you_search_for']?>:</p>
      <h3 class="al-center"><?php echo $key?></h3>
      <br>
      <div class="row">

        <div v-bind:class="'col-md-' + grid_size + ' col-lg-' + grid_size" v-for="value in arr_data.products">
          <a v-bind:href="value.url">
            <img v-bind:src="value.logo" class="d-block w-100" alt="...">
          </a>
          <div class="al-center">
            <br>
            <a v-bind:href="value.url" class="underline">{{value.title}}</a>
            <p>{{value.price}}</p>
          </div>
        </div>

      </div>
    </div>
  </div>
  <br>
</div>
<script>
<?php
  $js_array = json_encode($products);
  echo "var arr_products = ". $js_array . ";\n";

  $js_array = json_encode($product_type);
  echo "var arr_product_type = ". $js_array . ";\n";

  $js_array = json_encode($style);
  echo "var arr_style = ". $js_array . ";\n";

  $js_array = json_encode($material);
  echo "var arr_material = ". $js_array . ";\n";

  $js_array = json_encode($color);
  echo "var arr_color = ". $js_array . ";\n";

  $js_array = json_encode($size);
  echo "var arr_size = ". $js_array . ";\n";
?>
</script>
<!-- Product category Vuejs -->
<script type="text/javascript" src="<?php echo asset_front_url() ?>js/vuejs/vue.productcat.js"></script>
