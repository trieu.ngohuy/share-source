<div class="container-fluid">
	<!-- Top cats -->
	<div class="container">
		<div class="al-center">
			<h4><?php echo $title?></h4>
			<p><?php echo $description?></p>
			<br>
			<a href="<?php echo $objData['url']?>"><?php echo mb_strtoupper($translate['shop_all'])?> <?php echo mb_strtoupper($objData['title'])?></a>
		</div>
		<br>
		<div class="row">
			<?php
				foreach($list_top_cats_gid as $value){
					?>
					<div class="col-md-6 col-lg-6">
						<a href="<?php echo $value['url']?>">
							<img src="<?php echo $value['big_logo']?>" width="100%" />
						</a>
						<div class="caption al-center">
							<br>
							<h3><?php echo $value['title']?></h3>
							<br>
							<p><?php echo $value['slogan']?></p>
							<br>
							<a href="<?php echo $value['url']?>"><?php echo mb_strtoupper($value['title'])?></a>
						</div>
					</div>
					<?php
				}
			?>
		</div>
	</div>
	<br><br>
	<!-- Bestsellers -->
	<div class="row">
		<div class="col-lg-12">
			<div class="al-center">
				<h5><?php echo $objData['title']?> <?php echo $translate['bestsellers']?></h5>
				<br>
			</div>
			<?php
      $product_slide['product_slide']['data'] = $list_bestsellers_products_gid;
			$product_slide['product_slide']['key'] = 'bestsellers';
      $this->load->view('elements/frontend/product_slide', $product_slide); ?>
		</div>
	</div>
	<!--Middle cats-->
	<div class="row">
		<div class="col-lg-12">
			<br><br>
			<?php
      $productcat_slide['productcat_slide'] = $list_middle_cats_gid;
      $this->load->view('elements/frontend/productcat_slide_withcap', $productcat_slide); ?>
		</div>
	</div>
	<!-- New in -->
	<div class="row">
		<div class="col-lg-12">
			<br><br>
			<div class="al-center">
				<h5><?php echo $translate['new_in']?> <?php echo $objData['title']?></h5>
				<br>
			</div>
			<?php
      $product_slide['product_slide']['data'] = $list_newin_products_gid;
			$product_slide['product_slide']['key'] = 'newin';
      $this->load->view('elements/frontend/product_slide', $product_slide); ?>
		</div>
	</div>
	<!-- Bottom cats-->
	<div class="container">
		<br><br>
		<div class="row">
			<?php
				foreach($list_bottom_cats_gid as $value){
					?>
					<div class="col-md-6 col-lg-6">
						<a href="<?php echo $value['url']?>">
							<img src="<?php echo $value['big_logo']?>" width="100%" />
						</a>
						<div class="caption al-center">
							<br>
							<h3><?php echo $value['title']?></h3>
							<br>
							<p><?php echo $value['slogan']?></p>
							<br>
							<a href="<?php echo $value['url']?>"><?php echo mb_strtoupper($value['title'])?></a>
						</div>
					</div>
					<?php
				}
			?>
		</div>
		<br>
	</div>
</div>
