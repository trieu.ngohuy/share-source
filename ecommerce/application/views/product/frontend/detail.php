<div class="container-fluid" id="product-detail-wrap">
	<!-- Breadcumbs  -->
	<div class="row">
    <div class="col-lg-12">
			<ul id="breadcrumb">
        <?php
          foreach($breadcrumb as $key => $value){
            ?>
              <li><a href="<?php echo $value['url']?>"><?php echo $value['title']?></a></li>
              <?php
                if($key < count($breadcrumb) - 1){
                  ?>
                      <li><img src="<?php echo asset_front_url() . 'img/icon/right-arrow.png'?>" width="10px"/></li>
                  <?php
                }
              ?>
            <?php
          }
        ?>
      </ul>
    </div>
	</div>
	<br><br>
	<!-- Main -->
	<div class="container">
		<!-- Slide and basi info-->
		<div class="row" id="pro-block-1" style="background-color: <?php echo $detail['bg_color']?>;">
			<div class="col-md-7 col-lg-7 left" class="slide" id="slide1">
				<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				  <div class="carousel-inner">
						<?php
							foreach($detail['slide1'] as $key => $value){
								?>
									<div class="carousel-item <?php if($key == 0){echo 'active';}?>">
										<div class="row">
											<div class="col-md-2 col-lg-2"></div>
											<div class="col-md-8 col-lg-8">
												<div class="easyzoom easyzoom--overlay">
													<a href="<?php echo $value?>">
														<img class="d-block w-100" src="<?php echo $value?>" alt="<?php echo $detail['title']?>">
													</a>
												</div>
											</div>
											<div class="col-md-2 col-lg-2"></div>
										</div>
									</div>
								<?php
							}
						?>

				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				    <span>
							<img src="<?php echo base_url() . 'assets/front/img/icon/prev.png'?>" width="30px" />
						</span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				    <span>
							<img src="<?php echo base_url() . 'assets/front/img/icon/next.png'?>" width="30px" />
						</span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
			<div class="col-md-5 col-lg-5 right">
				<h4><?php echo $detail['title']?></h4>
				<h4 id="price">
					<?php
					if(isset($detail['price_sale'])){
						echo $detail['price_sale'];
						echo '<span>'.$detail['price'].'</span>';
					}else{
						echo $detail['price'];
					}
					?>
				</h4>
				<br>
				<p><?php echo $translate['material']?></p>
				<div id="lst-size" class="al-center">
					<?php
						foreach($material as $value){
								echo '<span v-on:click="set_material('.$value['product_properties_gid'].')" v-bind:class="{ active : active_material == '.$value['product_properties_gid'].' }" id="sz-'.$value['product_properties_gid'].'" data-gid="'.$value['product_properties_gid'].'">'.$value['title'].'</span>';
						}
					?>
				</div>
				<br>
				<p><?php echo $translate['size']?></p>
				<div id="lst-size" class="al-center">
					<?php
						foreach($size as $value){
								echo '<span v-on:click="set_size('.$value['product_properties_gid'].')" v-bind:class="{ active : active_size == '.$value['product_properties_gid'].' }" id="sz-'.$value['product_properties_gid'].'" data-gid="'.$value['product_properties_gid'].'">'.$value['title'].'</span>';
						}
					?>
				</div>
				<br>
				<p><?php echo $translate['color']?></p>
				<ul class="nav justify-content-center" id="ul-color">
					<?php
						foreach($color as $value){
								?>
								<li class="nav-item">
									<p v-on:click="set_color(<?php echo $value['product_properties_gid']?>)" v-bind:class="{ active : active_color == <?php echo $value['product_properties_gid']?> }" id="cl-<?php echo $value['product_properties_gid']?>" data-gid="<?php echo $value['product_properties_gid']?>" style="background: <?php echo $value['attr']?>;"></p>
								</li>
								<?php
						}
					?>
				</ul>
				<br>
				<p><?php echo $translate['pro_text']?></p>
				<br>
				<div class="row">
					<div class="col-md-4 col-lg-4" style="padding-top: 10px"><p><?php echo $translate['qty']?></p></div>
					<div class="col-md-8 col-lg-8" id="qty-form">
						<img src="<?php echo base_url() . 'assets/front/img/icon/decrease.png'?>" width="30px;" v-on:click="decrease()"/>
						<span>{{qty}}</span>
						<img src="<?php echo base_url() . 'assets/front/img/icon/increase.png'?>" width="30px;" v-on:click="increase()"/>
					</div>
				</div>
				<br>
				<div>
					<div class="alert alert-success" role="alert" v-if="response_msg != ''">{{response_msg}}</div>
					<input type="hidden" name="add_to_cart[product_gid]" value="<?php echo $detail['product_gid']?>" v-model="product_gid"/>
					<input type="hidden" name="add_to_cart[size_gid]" value="0" v-model="active_size" />
					<input type="hidden" name="add_to_cart[color_gid]" value="0" v-model="active_color" />
					<input type="hidden" name="add_to_cart[material_gid]" value="0" v-model="active_material" />
					<input type="hidden" name="add_to_cart[qty]" value="1" v-model="qty" />
					<button type="button" class="wd-full btn btn-success btn-cus-success"v-on:click="form_submit()"><?php echo $translate['add_to_bag']?></button>
				</div>
				<br>
				<ul class="nav justify-content-center" id="ul-social">
				  <li class="nav-item">
				    <p><?php echo $translate['share']?></p>
				  </li>
				  <li class="nav-item">
				    <a href="<?php echo $setting['facebook']['value']?>"><img src="<?php echo asset_front_url() . 'img/icon/fb.png'?>" height="25px"/></a>
				  </li>
				  <li class="nav-item">
				    <a href="<?php echo $setting['twitter']['value']?>"><img src="<?php echo asset_front_url() . 'img/icon/tw.png'?>" height="25px"/></a>
				  </li>
				  <li class="nav-item">
				    <a href="<?php echo $setting['instagram']['value']?>"><img src="<?php echo asset_front_url() . 'img/icon/in.png'?>" height="25px"/></a>
				  </li>
					<li class="nav-item">
				    <a href="<?php echo $setting['pinterest']['value']?>"><img src="<?php echo asset_front_url() . 'img/icon/pi.png'?>" height="25px"/></a>
				  </li>
				</ul>
			</div>
		</div>
		<br><br>
		<!-- Detail -->
		<div class="row" id="pro-block-2">
			<div class="col-md-5 col-lg-5 left" id="detail-wrap">
				<p class="title"><?php echo $translate['detail']?></p>
				<p><?php echo $detail['intro_text']?></p>
				<div class="detail">
					<div class="detail-title" v-on:click="show_trfpay()">
						<a href="#" class="title"><?php echo $translate['feature']?></a>
						<img src="<?php echo asset_front_url() . 'img/icon/plus.png'?>" height="10px"/>
					</div>
					<transition name="fade">
						<div class="detail-dropdown" v-if="is_trfpay">
							<?php echo $detail['tra_pay']?>
						</div>
					</transition>
				</div>
				<div class="detail">
					<div class="detail-title" v-on:click="show_refmat()">
						<a href="#" class="title"><?php echo $translate['refund_maintenance']?></a>
						<img src="<?php echo asset_front_url() . 'img/icon/plus.png'?>" height="10px"/>
					</div>
					<transition name="fade">
						<div class="detail-dropdown" v-if="is_refmat">
							<?php echo $detail['ref_mai']?>
						</div>
					</transition>
				</div>
			</div>
			<div class="col-md-7 col-lg-7 right" class="slide" id="slide2">
				<div id="carouselExampleControls2" class="carousel slide" data-ride="carousel">
				  <div class="carousel-inner">
						<?php
							foreach($detail['slide2'] as $key => $value){
								?>
									<div class="carousel-item <?php if($key == 0){echo 'active';}?>">
										<div class="row">
											<div class="col-md-2 col-lg-2"></div>
											<div class="col-md-8 col-lg-8">
												<div class="easyzoom easyzoom--overlay">
													<a href="<?php echo $value?>">
														<img class="d-block w-100" src="<?php echo $value?>" alt="<?php echo $detail['title']?>">
													</a>
												</div>
											</div>
											<div class="col-md-2 col-lg-2"></div>
										</div>
									</div>
								<?php
							}
						?>

				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-slide="prev">
						<span>
							<img src="<?php echo base_url() . 'assets/front/img/icon/prev.png'?>" width="30px" />
						</span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleControls2" role="button" data-slide="next">
						<span>
							<img src="<?php echo base_url() . 'assets/front/img/icon/next.png'?>" width="30px" />
						</span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
			<br>
		</div>
		<!-- Full text -->
		<div class="row detail" id="div-content">
				<div class="detail-title">
					<a href="#" class="title"><?php echo $translate['full_text']?></a>
					<img src="<?php echo asset_front_url() . 'img/icon/plus.png'?>" height="10px"/>
				</div>
				<div class="col-md-12"><?php echo $detail['full_text']?></div>
		</div>
		<br>
		<br>
		<!-- Related -->
		<div class="row">
			<div class="col-lg-12">
				<p><?php echo $translate['related']?></p>
				<br>
				<?php
	      $product_slide['product_slide']['data'] = $related;
				$product_slide['product_slide']['key'] = 'related';
	      $this->load->view('elements/frontend/product_slide', $product_slide); ?>
			</div>
		</div>
	</div>
	<br><br>
	<link rel="stylesheet" href="<?php echo libs_easyzoom_url()?>easyzoom.css" >
	<script type="text/javascript" src="<?php echo libs_easyzoom_url() ?>easyzoom.js"></script>
	<script>
		// Instantiate EasyZoom instances
		var $easyzoom = $('.easyzoom').easyZoom();
		<?php
    $js_array = json_encode($color);
    echo "var arr_color = " . $js_array . ";\n";
		$js_array = json_encode($material);
    echo "var arr_material = " . $js_array . ";\n";
		$js_array = json_encode($size);
    echo "var arr_size = " . $js_array . ";\n";
    ?>
		var url_cart = '<?php echo base_url() . 'Product/add_to_cart'?>';
		var gid = parseInt('<?php echo $detail['product_gid']?>');
		//update_cart
		function update_cart(response){
			var html = '';
			var data = response['data'];
			//Show cart
			vue_header.display_cart = true;

			setTimeout(function(){
				//Update cart count display
				if(data['products'].length > 0 ){
					vue_header.cart_products_count = '(' + data['products'].length + ')';
				}else{
					vue_header.cart_products_count = '';
				}
				
				//clean all old html
				vue_header.cart_html = '';
				//add cart title
				html += '<div class="row">'
	        +'<div class="col-md-6"><b><?php echo $translate['shopping_bag']?></b></div>'
	        +'<div class="col-md-6 al-right" id="cart_count">'
	          + data['products'].length+' <?php echo $translate['items']?>'
	        +'</div>'
	      +'</div>';

				//Add cart detail
				if(data['products'].length > 0){
					html += '<br>'
					+'<div id="cart-list-products">';
					for(var i = 0 ; i < data['products'].length ; i++){
						var tmp = data['products'][i];

						html += '<div class="row">'
								+'<div class="col-md-3">'
									+'<img src="'+tmp['logo']+'" class="wd-full"/>'
								+'</div>'
								+'<div class="col-md-6">'
									+'<a href="'+tmp['url']+'">'+tmp['title']+'</a>'
									+'<p><?php echo $translate['cart_quantity']?>: '+tmp['count']+'</p>'
								+'</div>'
								+'<div class="col-md-3 al-right"><p>'+tmp['price']+'</p></div>'
						+'</div>';
						if(i < data['products'].length - 1){
							html += '<hr>';
						}
					}
					html += '</div>';

					//Add cart total
					html += '<br>'
					+'<div class="row" id="cart_total">'
						+'<div class="col-md-7"><b><?php echo $translate['total']?></b></div>'
						+'<div class="col-md-5 al-right"><b>'+data['total']+'</b></div>'
					+'</div>'
					+'<hr>';
					//Add cart checkout button
					html += '<a class="wd-full btn btn-success anc-btn-success" href="<?php echo base_url() . URL_CART?>"><?php echo $translate['checkout']?></a>';
				}else{
					html += '<hr>'
      				+'<p class="al-center" id="cart_empty"><?php echo $translate['empty_des']?></p>';
				}

				//Update html
				vue_header.cart_html = html;
				//Update message
				vue_productdetail.response_msg = response['message'];
			}, 2000);
		}
	</script>
</div>
<!-- Product category Vuejs -->
<script type="text/javascript" src="<?php echo asset_front_url() ?>js/vuejs/vue.productdetail.js"></script>
