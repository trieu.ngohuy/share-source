<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quản lý Địa điểm
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header al-right">
                    <button type="button" id="btn_new" class="btn btn-primary">Thêm mới</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Ngày tạo</th>
                                <th width="15px"></th>
                                <th>Tiêu đề</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($data as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $value['created_date']; ?></td>
                                    <td class="mul">
                                      <img src="<?php echo base_url() . 'assets/img/lang/vi.png'?>" height="30px">
                                      <hr>
	                                    <img src="<?php echo base_url() . 'assets/img/lang/en.png'?>" height="30px">
                                    </td>
                                    <td class="mul">
                                      <p><?php
                                        $key = 'vi';
                                        $element = 'title';
                                        if($value['detail'][$key][$element] == null || trim($value['detail'][$key][$element]) == ''){
                                          echo '(Not set)';
                                        }else{
                                          echo $value['detail'][$key][$element];
                                        }
                                      ?></p>
                                      <hr>
                                      <p><?php
                                        $key = 'en';
                                        if($value['detail'][$key][$element] == null || trim($value['detail'][$key][$element]) == ''){
                                          echo '(Not set)';
                                        }else{
                                          echo $value['detail'][$key][$element];
                                        }
                                      ?></p>
                                    </td>
                                    <td style="padding-top: 28px">
                                        <a href="#" class="btn-edit" data-index="<?php echo $count - 1; ?>"><i class="fa fa-edit"></i> Sửa</a>
                                        <a href="#" class="btn-remove" data-index="<?php echo $count - 1; ?>"><i class="fa fa-remove"></i> Xóa</a>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>#</th>
                              <th>Ngày tạo</th>
                              <th width="15px"></th>
                              <th>Tiêu đề</th>
                              <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade" id="modal-data">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Default Modal</h4>
          </div>
          <div class="modal-body">
            <div class="row" id="common">
              <div class="col-md-12">
                <p><b>Cài đặt chung</b></p>
                <div class="content-wrap">
                  <div class="form-group">
                      <label>Bản đồ</label>
                      <input id="map" name="data[map]" type="text" class="form-control" placeholder="Bản đồ">
                      <div id="pre_map">
                        <iframe src="" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <div class="row" id="multilang">
              <div class="col-md-6">
                <img src="<?php echo base_url() . 'assets/img/lang/vi.png'?>" width="25px"/> <b>Tiếng việt</b>
                <div><br></div>
                <div class="content-wrap">
                  <div class="form-group">
                      <label>Tên sản phẩm</label>
                      <input id="vi_title" name="data[title]" type="text" class="form-control" placeholder="Tên sản phẩm">
                  </div>
                  <div class="form-group">
                      <label>Tên rút gọn</label>
                      <input id="vi_alias" type="text" class="form-control" placeholder="Tên rút gọn">
                      <p class="help-block">Tên rút gọn không được trùng.</p>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <img src="<?php echo base_url() . 'assets/img/lang/en.png'?>" width="25px"/> <b>Tiếng anh</b>
                <div><br></div>
                <div class="content-wrap">
                  <div class="form-group">
                      <label>Tên sản phẩm</label>
                      <input id="en_title" name="data[title]" type="text" class="form-control" placeholder="Tên sản phẩm">
                  </div>
                  <div class="form-group">
                      <label>Tên rút gọn</label>
                      <input id="en_alias" type="text" class="form-control" placeholder="Tên rút gọn">
                      <p class="help-block">Tên rút gọn không được trùng.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="btn-save">Lưu</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Style -->
<style type="text/css">
    .select2-container .select2-selection--single {
        height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 31px;
    }
    table .mul{
      padding-top: 15px;
    }
    table hr{
      margin: 0px;
    }
</style>
<!-- page script -->
<script>
    var mode = 'new';
    var key = 0;
    var key_str = 'store_gid';
    var url_update = "<?php echo base_url() ?>AdminStore/execute_query";
    var url_delete = "<?php echo base_url() ?>AdminStore/delete";
    var url_refresh = "<?php echo base_url() ?>AdminStore/get_data";
    //Convert php array to js array
    <?php
    $js_array = json_encode($data);
    echo "var arr_data = " . $js_array . ";\n";
    ?>

    $(document).ready(function () {

        //First init
        first_init();

        //Click new button
        $(document).on('click', '#btn_new', function (e) {
            //Set mode and key value
            mode = 'new';
            key = 0;
            //Change modal title
            $('.modal-title').html('Thêm mới');

            /*Set modal custom data*/
            var data = {
              'map': '',
              'vi' : {
                'title' : '',
                'alias' : ''
              },
              'en' : {
                'title' : '',
                'alias' : ''
              }
            };
            update_modal_data(data);
            /*End*/

            //Open modal
            $("#modal-data").modal();
        });
        //Click edit button
        $(document).on('click', '.btn-edit', function (e) {
            //Set mode and key value
            mode = 'edit';
            var index = $(this).attr('data-index');
            var obj_data = arr_data[index];
            key = obj_data[key_str];
            //Change modal title
            $('.modal-title').html('Chỉnh sửa');

            /*Set modal custom data*/
            var vi_detail = obj_data['detail']['vi'];
            var en_detail = obj_data['detail']['en'];
            var data = {
              'map': obj_data['map'],
              'vi' : {
                'title' : vi_detail['title'],
                'alias' : vi_detail['alias']
              },
              'en' : {
                'title' : en_detail['title'],
                'alias' : en_detail['alias']
              }
            };
            update_modal_data(data);
            /*End*/

            //Open modal
            $("#modal-data").modal();
        });
        //Click remove button
        $(document).on('click', '.btn-remove', function (e) {
            if (confirm("Are you sure you want to delete this?")) {
                //Get id
                var index = $(this).attr('data-index');
                //Get edit data
                var obj_data = arr_data[index];
                //Project id
                key = obj_data[key_str];

                //Create post data
                var fd = new FormData();
                fd.append(key_str, key);

                $.ajax({
                    type: 'POST',
                    url: url_delete,
                    data: fd,
                    processData: false,
                    contentType: false,
                    cache:false,
                    enctype: 'multipart/form-data',
                    success: function (objData) {
                        //Refresh data
                        refresh_data();
                    },
                    error: function (jqXHR, exception) {
                        alert('Có lỗi. Thử lại sau.');
                    }
                });
            }
        });
        //Modal save button click
        $(document).on('click', '#btn-save', function (e) {

            //If add new then check if duplicate alias
            if (verify_data() === false) {
                return;
            }
            //Hide modal
            $('#modal-data').modal('toggle');

            //Ajax
            $.ajax({
                type: 'POST',
                url: url_update,
                data: get_update_data(),
                processData: false,
                contentType: false,
                cache:false,
                enctype: 'multipart/form-data',
                success: function (objData) {
                  if(objData !== ''){
                    objData = $.parseJSON(objData);
                    alert(objData["messsage"]);
                  }else{
                    //Refresh data
                    refresh_data();
                  }
                },
                error: function (jqXHR, exception) {
                    alert('Có lỗi. Thử lại sau.');
                }
            });
        });
    });
    //Update modal data
    function update_modal_data(data){
      //common
      $('#map').val(data['map']);
      $('#pre_map iframe').attr('src', data['map']);

      //vi
      tmp = data['vi'];
      $('#vi_title').val(tmp['title']);
      $('#vi_alias').val(tmp['alias']);

      //en
      tmp = data['en'];
      $('#en_title').val(tmp['title']);
      $('#en_alias').val(tmp['alias']);
    }
    //Verify data
    function verify_data() {
        /*===Common===*/
        if ($('#map').val() === '') {
            alert('Bản đồ không được để trống!');
            return false;
        }
        /*===End Common===*/

        /*===vi===*/
        if ($('#vi_title').val() === '') {
            alert('Tiêu đề tiếng việt không được để trống!');
            return false;
        }
        if ($('#vi_alias').val() === '') {
            alert('Alias sản phẩm không được để trống!');
            return false;
        }
        /*===end vi===*/

        /*===en===*/
        if ($('#en_title').val() === '') {
            alert('Tiêu đề tiếng anh không được để trống!');
            return false;
        }
        if ($('#en_alias').val() === '') {
            alert('Alias sản phẩm không được để trống!');
            return false;
        }
        //compare title in vi and en
        if ($('#vi_alias').val() === $('#en_alias').val()) {
            alert('Tiêu đề tiếng việt và tiếng ảnh không được trùng!');
            return false;
        }
        /*===end en===*/
    }
    //Get data
    function refresh_data() {
        //Refresh data
        $.ajax({
            type: 'GET',
            url: url_refresh,
            success: function (data) {
                data = $.parseJSON(data);
                arr_data = data;
                //Fill datatable
                $('#example1').DataTable().destroy();
                $('#example1 tbody').html('');
                var html = '';
                var detail = [];
                var val = '';
                for (var i = 0; i < data.length; i++) {
                    html += '<tr>'
                            + '<td>' + (i + 1) + '</td>'
                            + '<td>' + data[i]['created_date'] + '</td>'
                            + '<td class="mul">'
                            + '      <img src="<?php echo base_url() . 'assets/img/lang/vi.png'?>" height="30px">'
                            + '      <hr>'
                            + '      <img src="<?php echo base_url() . 'assets/img/lang/en.png'?>" height="30px">'
                            + '</td>';
                    html += multilanguage_item(data[i]['detail'], 0, 'title');
                    html += '<td style="padding-top: 28px">'
                            + '<a href="#" class="btn-edit" data-index="' + i + '"><i class="fa fa-edit"></i> Sửa</a>'
                            + ' <a href="#" class="btn-remove" data-index="' + i + '"><i class="fa fa-remove"></i> Xóa</a>'
                            + '</td>'
                            + '</tr>';
                }
                $('#example1 tbody').html(html);
                //Initialize datatable
                $('#example1').DataTable().draw();

            },
            error: function () {
                alert('Có lỗi. Thử lại sau.');
            }
        });
    }
    //Get update data
    function get_update_data(){
      var fd = new FormData();

      //Common
      fd.append(key_str, key);
      fd.append('map', $('#map').val());
      //vi
      fd.append('vi_title', $('#vi_title').val());
      fd.append('vi_alias', $('#vi_alias').val());
      //en
      fd.append('en_title', $('#en_title').val());
      fd.append('en_alias', $('#en_alias').val());

      return fd;
    }
    //First init
    function first_init(){
      //Initialize datatable
      $(function () {
          $('#example1').DataTable();
      });
      //Title alias
      $("#vi_title").on('input', function () {
          $("#vi_alias").val(convert_vi_to_en($(this).val()));
      });
      $("#en_title").on('input', function () {
          $("#en_alias").val(convert_vi_to_en($(this).val()));
      });

      //map onchange
      $('#map').on('keyup', function(){
        $('#pre_map iframe').attr('src', $(this).val());
      });
    }
</script>
