<div class="container-fluid" id="store-wrap">
  <?php
    if(count($location) <= 0){
      ?>
        <br><br>
        <p class="al-center"><?php echo $translate['no_place']?></p>
        <br><br>
      <?php
    }else{
      ?>
      <div class="mapouter" id="header-map">
        <div class="gmap_canvas">
          <iframe width="100%" height="400" id="gmap_canvas" src="<?php echo $active_location['map']?>" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
        </div>
      </div>
      <div class="form-group">
        <form method="post">
          <input type="text" name="store" class="form-control" placeholder="<?php echo $translate['enter_store_name']?>" value="<?php echo $key?>" />
        </form>
      </div>
      <br>
      <div id="menu" class="row al-center">
        <ul id="top-menu">
          <?php
            foreach($location as $value){
              ?>
              <li>
                <a href="<?php echo $value['url']?>"><?php echo mb_strtoupper($value['title'])?></a>
              </li>
              <?php
            }
          ?>
          <li>
            <a href="<?php echo base_url() . URL_STORE?>"><?php echo $translate['bg_all']?></a>
          </li>
        </ul>
      </div>
      <br>
      <h4><?php echo $active_location['title']?></h4>
      <?php
        if(count($detail) <=0 ){
          ?>
            <p class="al-center"><?php echo $translate['no_store']?></div>
          <?php
        }else{
          ?>
          <div id="list-store">
            <?php
            foreach($detail as $key => $value){
              if($key == 0 || ($key + 1) % 3 == 0){
                ?>
                  <div class="row list-item">
                <?php
              }
              ?>
              <div class="col-md-6 col-lg-6">
                <div class="row header">
                  <div class="col-md-6 col-lg-4"><a href="#" class="underline"><?php echo $value['title']?></a></div>
                  <div class="col-md-6 col-lg-8 al-right open-time">
                    <?php echo $translate['open_until']?> <?php echo $value['close_time']?> <?php echo $translate['today']?>
                    <img src="<?php echo asset_front_url() . 'img/icon/plus.png'?>" height="15px"/>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-lg-6"><?php echo $value['address']?></div>
                  <div class="col-md-6 col-lg-6 al-right">
                    <div class="gmap_canvas">
                      <iframe width="100%" height="150" id="gmap_canvas" src="<?php echo $value['map']?>" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                    </div>
                  </div>
                </div>
                <hr>
              </div>
              <?php
              if(($key == count($detail) - 1) || ($key + 1) % 2 == 0){
                ?>
                  </div>
                <?php
              }
            }
            ?>
          </div>
          <?php
        }
      ?>
      <?php
    }
  ?>
</div>
<br><br>
<style>
.mapouter{position:relative;text-align:right;width:100%;}
.gmap_canvas {overflow:hidden;background:none!important;width:100%;}
</style>
