<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quản lý banner
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header al-right">
                    <button type="button" id="btn_new" class="btn btn-primary">Thêm mới</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Ảnh</th>
                                <th>Vị trí hiển thị</th>
                                <th>Ngày tạo</th>
                                <th width="15px"></th>
                                <th>Tiêu đề</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($data as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><img src="<?php echo $value['logo']; ?>" width="100px"/></td>
                                    <td><?php
                                      if($value['type'] == 0){
                                        echo 'Banner Trên';
                                      }else{
                                        echo 'Banner Dưới';
                                      }
                                     ?></td>
                                    <td><?php echo $value['created_date']; ?></td>
                                    <td class="mul">
                                      <img src="<?php echo base_url() . 'assets/img/lang/vi.png'?>" height="30px">
                                      <hr>
	                                    <img src="<?php echo base_url() . 'assets/img/lang/en.png'?>" height="30px">
                                    </td>
                                    <td class="mul">
                                      <p><?php
                                        $key = 'vi';
                                        $element = 'title';
                                        if($value['detail'][$key][$element] == null || trim($value['detail'][$key][$element]) == ''){
                                          echo '(Not set)';
                                        }else{
                                          echo $value['detail'][$key][$element];
                                        }
                                      ?></p>
                                      <hr>
                                      <p><?php
                                        $key = 'en';
                                        if($value['detail'][$key][$element] == null || trim($value['detail'][$key][$element]) == ''){
                                          echo '(Not set)';
                                        }else{
                                          echo $value['detail'][$key][$element];
                                        }
                                      ?></p>
                                    </td>
                                    <td style="padding-top: 28px">
                                        <a href="#" class="btn-edit" data-index="<?php echo $count - 1; ?>"><i class="fa fa-edit"></i> Sửa</a>
                                        <a href="#" class="btn-remove" data-index="<?php echo $count - 1; ?>"><i class="fa fa-remove"></i> Xóa</a>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>#</th>
                              <th>Ảnh</th>
                              <th>Vị trí hiển thị</th>
                              <th>Ngày tạo</th>
                              <th></th>
                              <th>Tiêu đề</th>
                              <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade" id="modal-data">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Default Modal</h4>
          </div>
          <div class="modal-body">
            <div class="row" id="common">
              <div class="col-md-12">
                <p><b>Cài đặt chung</b></p>
                <div class="content-wrap">
                  <div class="form-group pos-related">
                      <label for="exampleInputFile">Logo</label>
                      <input name="logo" type="file" id="logo">
                      <img class="upload_img" src="" height="100%" id="img_logo"/>
                      <p class="help-block">Chỉ hổ trợ định dạng png/jpg.</p>
                  </div>
                  <div class="form-group">
                      <label>Vị trí hiển thị</label>
                      <div class="radio">
                          <label>
                            <input type="radio" name="type" value="0" checked="">
                            Phía trên
                          </label>
                          <label>
                            <input type="radio" name="type" value="1">
                            Phía dưới
                          </label>
                        </div>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <div class="row" id="multilang">
              <div class="col-md-6">
                <img src="<?php echo base_url() . 'assets/img/lang/vi.png'?>" width="25px"/> <b>Tiếng việt</b>
                <div><br></div>
                <div class="content-wrap">
                  <div class="form-group">
                      <label>Tên banner</label>
                      <input id="vi_title" name="data[title]" type="text" class="form-control" placeholder="Tên banner">
                  </div>
                  <div class="form-group">
                      <label>Link tự gõ</label>
                      <input id="vi_link" name="data[link]" type="text" class="form-control" placeholder="Link">
                      <p class="help-block">Nếu link này để rỗng thì sẽ lấy giá trị chọn đường dẫn bên dưới.</p>
                  </div>
                  <div class="form-group">
                      <label>Chọn đường dẫn</label>
                      <select id="vi_comlink" class="form-control select2" data-placeholder="Chọn đường dẫn" style="width: 100%;">
                        <optgroup label="Khác">
                          <option value="" data-controller="" data-function="" data-params=""> -- Chọn -- </option>
                          <option value="home" data-controller="home" data-function="index" data-params=""> Trang chủ </option>
                          <option value="store" data-controller="store" data-function="index" data-params=""> Cửa hàng </option>
                          <option value="blog" data-controller="content" data-function="index" data-params=""> Blog </option>
                        </optgroup>
                        <optgroup label="Danh mục Sản phẩm">
                          <?php
                            foreach($vi_productcat as $value){
                              ?>
                              <option value="<?php echo $value['alias']?>" data-controller="product" data-function="index" data-params="<?php echo $value['alias']?>"> <?php echo $value['title']?> </option>
                              <?php
                            }
                          ?>
                        </optgroup>
                        <optgroup label="Sản phẩm">
                          <?php
                            foreach($vi_products as $value){
                              ?>
                              <option value="<?php echo $value['alias']?>" data-controller="product" data-function="detail" data-params="<?php echo $value['alias']?>"> <?php echo $value['title']?> </option>
                              <?php
                            }
                          ?>
                        </optgroup>
                        <optgroup label="Danh mục Bài viết">
                          <?php
                            foreach($vi_contentcat as $value){
                              ?>
                              <option value="<?php echo $value['alias']?>" data-controller="content" data-function="index" data-params="<?php echo $value['alias']?>"> <?php echo $value['title']?> </option>
                              <?php
                            }
                          ?>
                        </optgroup>
                        <optgroup label="Bài viết">
                          <?php
                            foreach($vi_contents as $value){
                              ?>
                              <option value="<?php echo $value['alias']?>" data-controller="content" data-function="detail" data-params="<?php echo $value['alias']?>"> <?php echo $value['title']?> </option>
                              <?php
                            }
                          ?>
                        </optgroup>
                      </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <img src="<?php echo base_url() . 'assets/img/lang/en.png'?>" width="25px"/> <b>Tiếng anh</b>
                <div><br></div>
                <div class="content-wrap">
                  <div class="form-group">
                      <label>Tên banner</label>
                      <input id="en_title" name="data[title]" type="text" class="form-control" placeholder="Tên banner">
                  </div>
                  <div class="form-group">
                      <label>Link tự gõ</label>
                      <input id="en_link" name="data[link]" type="text" class="form-control" placeholder="Link">
                      <p class="help-block">Nếu link này để rỗng thì sẽ lấy giá trị chọn đường dẫn bên dưới.</p>
                  </div>
                  <div class="form-group">
                      <label>Chọn đường dẫn</label>
                      <select id="en_comlink" class="form-control select2" data-placeholder="Chọn đường dẫn" style="width: 100%;">
                        <optgroup label="Khác">
                          <option value="" data-controller="" data-function="" data-params=""> -- Chọn -- </option>
                          <option value="home" data-controller="home" data-function="index" data-params=""> Trang chủ </option>
                          <option value="store" data-controller="store" data-function="index" data-params=""> Cửa hàng </option>
                          <option value="blog" data-controller="content" data-function="index" data-params=""> Blog </option>
                        </optgroup>
                        <optgroup label="Danh mục Sản phẩm">
                          <?php
                            foreach($en_productcat as $value){
                              ?>
                              <option value="<?php echo $value['alias']?>" data-controller="product" data-function="index" data-params="<?php echo $value['alias']?>"> <?php echo $value['title']?> </option>
                              <?php
                            }
                          ?>
                        </optgroup>
                        <optgroup label="Sản phẩm">
                          <?php
                            foreach($en_products as $value){
                              ?>
                              <option value="<?php echo $value['alias']?>" data-controller="product" data-function="detail" data-params="<?php echo $value['alias']?>"> <?php echo $value['title']?> </option>
                              <?php
                            }
                          ?>
                        </optgroup>
                        <optgroup label="Danh mục Bài viết">
                          <?php
                            foreach($en_contentcat as $value){
                              ?>
                              <option value="<?php echo $value['alias']?>" data-controller="content" data-function="index" data-params="<?php echo $value['alias']?>"> <?php echo $value['title']?> </option>
                              <?php
                            }
                          ?>
                        </optgroup>
                        <optgroup label="Bài viết">
                          <?php
                            foreach($en_contents as $value){
                              ?>
                              <option value="<?php echo $value['alias']?>" data-controller="content" data-function="detail" data-params="<?php echo $value['alias']?>"> <?php echo $value['title']?> </option>
                              <?php
                            }
                          ?>
                        </optgroup>
                      </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="btn-save">Lưu</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Style -->
<style type="text/css">
    .select2-container .select2-selection--single {
        height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 31px;
    }
    table .mul{
      padding-top: 15px;
    }
    table hr{
      margin: 0px;
    }
</style>
<!-- page script -->
<script>
    var mode = 'new';
    var key = 0;
    var key_str = 'banner_gid';
    var url_update = "<?php echo base_url() ?>AdminBanner/execute_query";
    var url_delete = "<?php echo base_url() ?>AdminBanner/delete";
    var url_refresh = "<?php echo base_url() ?>AdminBanner/get_data";
    //Convert php array to js array
    <?php
    $js_array = json_encode($data);
    echo "var arr_data = " . $js_array . ";\n";
    ?>

    $(document).ready(function () {

        //First config
        first_init();

        //Click new button
        $(document).on('click', '#btn_new', function (e) {
            //Set mode and key value
            mode = 'new';
            key = 0;
            //Change modal title
            $('.modal-title').html('Thêm mới');

            /*Set modal custom data*/
            var data = {
              'logo' : '',
              'type' : '0',
              'vi' : {
                'title' : '',
                'link' : '',
                'controller' : '',
                'function' : '',
                'params' : ''
              },
              'en' : {
                'title' : '',
                'link' : '',
                'controller' : '',
                'function' : '',
                'params' : '',
              }
            };
            update_modal_data(data);
            /*End*/

            //Open modal
            $("#modal-data").modal();
        });
        //Click edit button
        $(document).on('click', '.btn-edit', function (e) {
            //Set mode and key value
            mode = 'edit';
            var index = $(this).attr('data-index');
            var obj_data = arr_data[index];
            key = obj_data[key_str];
            //Change modal title
            $('.modal-title').html('Chỉnh sửa');

            /*Set modal custom data*/
            var vi_detail = obj_data['detail']['vi'];
            var en_detail = obj_data['detail']['en'];
            var data = {
              'logo' : obj_data['logo'],
              'type' : obj_data['type'],
              'vi' : {
                'title' : vi_detail['title'],
                'link' : vi_detail['link'],
                'controller' : vi_detail['controller'],
                'function' : vi_detail['function'],
                'params' : vi_detail['params'],
              },
              'en' : {
                'title' : en_detail['title'],
                'link' : en_detail['link'],
                'controller' : en_detail['controller'],
                'function' : en_detail['function'],
                'params' : en_detail['params'],
              }
            };
            update_modal_data(data);
            /*End*/

            //Open modal
            $("#modal-data").modal();
        });
        //Click remove button
        $(document).on('click', '.btn-remove', function (e) {
            if (confirm("Are you sure you want to delete this?")) {
                //Get id
                var index = $(this).attr('data-index');
                //Get edit data
                var obj_data = arr_data[index];
                //Project id
                key = obj_data[key_str];

                //Create post data
                var fd = new FormData();
                fd.append(key_str, key);

                $.ajax({
                    type: 'POST',
                    url: url_delete,
                    data: fd,
                    processData: false,
                    contentType: false,
                    cache:false,
                    enctype: 'multipart/form-data',
                    success: function (objData) {
                        //Refresh data
                        refresh_data();
                    },
                    error: function (jqXHR, exception) {
                        alert('Có lỗi. Thử lại sau.');
                    }
                });
            }
        });
        //Modal save button click
        $(document).on('click', '#btn-save', function (e) {

            //If add new then check if duplicate alias
            if (verify_data() === false) {
                return;
            }
            //Hide modal
            $('#modal-data').modal('toggle');

            //Ajax
            $.ajax({
                type: 'POST',
                url: url_update,
                data: get_update_data(),
                processData: false,
                contentType: false,
                cache:false,
                enctype: 'multipart/form-data',
                success: function (objData) {
                  if(objData !== ''){
                    objData = $.parseJSON(objData);
                    alert(objData["messsage"]);
                  }else{
                    //Refresh data
                    refresh_data();
                  }
                },
                error: function (jqXHR, exception) {
                    alert('Có lỗi. Thử lại sau.');
                }
            });
        });
    });
    //Update modal data
    function update_modal_data(data){
      //common
      var tmp = data;
      if (tmp['logo'] !== '' && tmp['logo'] !== null) {
          $('#img_logo').show();
          $('#img_logo').attr('src', tmp['logo']);
      }else{
          $('#img_logo').hide();
      }
      $("input[name=type][value='"+tmp['type']+"']").prop("checked",true);

      //vi
      tmp = data['vi'];
      $('#vi_title').val(tmp['title']);
      $('#vi_link').val(tmp['link']);
      if(tmp['controller'] == '' && tmp['function'] == '' && tmp['params'] == ''){
        $('#vi_comlink').val($('#vi_comlink option:eq(0)').val()).trigger('change');
      }
      else if(tmp['controller'] == 'home'){
        $('#vi_comlink').val('home').trigger('change');
      }
      else if(tmp['controller'] == 'store'){
        $('#vi_comlink').val('store').trigger('change');
      }
      else if(tmp['controller'] == 'content' && tmp['function'] == 'index' && tmp['params'] == ''){
        $('#vi_comlink').val('blog').trigger('change');
      }else{
        $('#vi_comlink').val(tmp['params']).trigger('change');
      }

      //en
      tmp = data['en'];
      $('#en_title').val(tmp['title']);
      $('#en_link').val(tmp['link']);
      if(tmp['controller'] == '' && tmp['function'] == '' && tmp['params'] == ''){
        $('#en_comlink').val($('#en_comlink option:eq(0)').val()).trigger('change');
      }
      else if(tmp['controller'] == 'home'){
        $('#en_comlink').val('home').trigger('change');
      }
      else if(tmp['controller'] == 'store'){
        $('#en_comlink').val('store').trigger('change');
      }
      else if(tmp['controller'] == 'content' && tmp['function'] == 'index' && tmp['params'] == ''){
        $('#en_comlink').val('blog').trigger('change');
      }else{
        $('#en_comlink').val(tmp['params']).trigger('change');
      }
    }
    //Verify data
    function verify_data() {
        //Check empty input
        if ($('#txt_title').val() === '') {
            alert('Title không được rỗng!');
            return false;
        }
    }
    //Get data
    function refresh_data() {
        //Refresh data
        $.ajax({
            type: 'GET',
            url: url_refresh,
            success: function (data) {
                data = $.parseJSON(data);
                arr_data = data;
                //Fill datatable
                $('#example1').DataTable().destroy();
                $('#example1 tbody').html('');
                var html = '';
                var detail = [];
                var val = '';
                for (var i = 0; i < data.length; i++) {
                  var link = '';
                  if(data[i]['link'] == ''){
                    link = '<a href="'+data[i]['url']+'">Click</a>';
                  }else{
                    link = '<a href="'+data[i]['link']+'">Click</a>';
                  }
                  var type = '';
                  if(data[i]['type'] == 0){
                    type = 'Banner trên';
                  }else {
                    type = 'Banner dưới';
                  }

                    html += '<tr>'
                            + '<td>' + (i + 1) + '</td>'
                            + '<td><img src="' + data[i]['logo'] + '" width="100px"/></td>'
                            + '<td>' + type + '</td>'
                            + '<td>' + data[i]['created_date'] + '</td>'
                            + '<td class="mul">'
                            + '      <img src="<?php echo base_url() . 'assets/img/lang/vi.png'?>" height="30px">'
                            + '      <hr>'
                            + '      <img src="<?php echo base_url() . 'assets/img/lang/en.png'?>" height="30px">'
                            + '</td>';
                    html += multilanguage_item(data[i]['detail'], 0, 'title');
                    html += '<td style="padding-top: 28px">'
                            + '<a href="#" class="btn-edit" data-index="' + i + '"><i class="fa fa-edit"></i> Sửa</a>'
                            + ' <a href="#" class="btn-remove" data-index="' + i + '"><i class="fa fa-remove"></i> Xóa</a>'
                            + '</td>'
                            + '</tr>';
                }
                $('#example1 tbody').html(html);
                //Initialize datatable
                $('#example1').DataTable().draw();

            },
            error: function () {
                alert('Có lỗi. Thử lại sau.');
            }
        });
    }
    //Get update data
    function get_update_data(){
      var fd = new FormData();

      //Common
      var logo_upload = $('#logo')[0].files[0];
      if(logo_upload != undefined){
          fd.append('logo', logo_upload);
      }
      fd.append('type', $('input[name=type]:checked').val());
      fd.append(key_str, key);

      //vi
      fd.append('vi_title', $('#vi_title').val());
      fd.append('vi_link', $('#vi_link').val());
      fd.append('vi_controller', $("#vi_comlink option:selected").attr("data-controller"));
      fd.append('vi_function', $("#vi_comlink option:selected").attr("data-function"));
      fd.append('vi_params', $("#vi_comlink option:selected").attr("data-params"));
      //en
      fd.append('en_title', $('#en_title').val());
      fd.append('en_link', $('#en_link').val());
      fd.append('en_controller', $("#en_comlink option:selected").attr("data-controller"));
      fd.append('en_function', $("#en_comlink option:selected").attr("data-function"));
      fd.append('en_params', $("#en_comlink option:selected").attr("data-params"));

      return fd;
    }
    //First init
    function first_init(){
      //Initialize datatable
      $(function () {
          $('#example1').DataTable();
      });
    }
</script>
