<div id="top_banner">
  <!-- <a href="<?php echo $setting['home_config']['value']['top_banner']['url']?>">
    <img src="<?php echo $setting['home_config']['value']['top_banner']['img']?>" width="100%"/>
  </a> -->
  <div id="csouTop" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <?php
        foreach($top_banner as $key => $value){
          ?>
          <div class="carousel-item <?php if($key === 0){echo 'active';}?>">
            <img class="d-block w-100" src="<?php echo $value['logo']?>" alt="First slide">
          </div>
          <?php
        }
      ?>
    </div>
    <a class="carousel-control-prev" href="#csouTop" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#csouTop" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<br>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12 al-center">
      <h4><?php echo $setting['home_config']['value']['title']?></h4>
      <!-- <p><?php echo $setting['home_config']['value']['description']?></p> -->
      <br>
    </div>
    <div class="col-lg-12">
      <?php
      $productcat_slide['productcat_slide'] = $top_cat;
      $this->load->view('elements/frontend/productcat_slide', $productcat_slide); ?>
    </div>
  </div>
  <br><br><br>
  <div class="row">
    <?php
      foreach($bottom_cat as $value){
        ?>
          <div class="col-md-6 col-lg-6">
            <?php
            $productcat_item['productcat_item'] = array(
              'url' => $value['url'],
              'title' => $value['title'],
              'logo' => $value['big_logo'],
              'slogan' => $value['slogan']
            );
            $this->load->view('elements/frontend/productcat_item', $productcat_item); ?>
          </div>
        <?php
      }
    ?>
  </div>
</div>
<br>
<div id="bottom_banner">
  <div id="csouBottom" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <?php
        foreach($bottom_banner as $key => $value){
          ?>
          <div class="carousel-item <?php if($key === 0){echo 'active';}?>">
            <img class="d-block w-100" src="<?php echo $value['logo']?>" alt="First slide">
          </div>
          <?php
        }
      ?>
    </div>
    <a class="carousel-control-prev" href="#csouBottom" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#csouBottom" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <!-- <a href="<?php echo $setting['home_config']['value']['bottom_banner']['url']?>">
    <img src="<?php echo $setting['home_config']['value']['bottom_banner']['img']?>" width="100%"/>
  </a> -->
</div>
