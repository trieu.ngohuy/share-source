<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo asset_front_url()?>css/bootstrap.min.css" >
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Custom -->
    <link rel="stylesheet" href="<?php echo asset_front_url()?>css/custom.css" >
    <!-- Common -->
    <link rel="stylesheet" href="<?php echo asset_url()?>css/common.css" >

    <title>Oliver Bonas</title>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="<?php echo asset_front_url()?>js/bootstrap.min.js"></script>
    <!-- Vuejs -->
    <script type="text/javascript" src="<?php echo libs_vuejs_url() ?>vue.min.js"></script>
    <script type="text/javascript" src="<?php echo libs_vuejs_url() ?>axios.min.js"></script>
    <link rel="stylesheet" href="<?php echo libs_vuejs_url()?>vue.transition.css" >
    <script type="text/javascript" src="<?php echo asset_front_url() ?>js/vuejs/vue.common.js"></script>
    <!-- Custom Script -->
    <!-- <script type="text/javascript" src="<?php echo asset_front_url() ?>js/script.js"></script> -->
    <style>
    .carousel-control-next, .carousel-control-prev {
        width: 5%;
      }
    </style>
  </head>
  <body>
    <div class="container-fluid">
        <?php $this->load->view('elements/frontend/header'); ?>
    </div>
    <?php echo $contents?>
    <?php $this->load->view('elements/frontend/footer'); ?>
  </body>
</html>
