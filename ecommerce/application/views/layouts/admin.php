<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo get_setting_value($setting, 'website_title'); ?></title>
        <link rel="icon" type="image/png" href="<?php
        if (get_setting_value($setting, 'favicon')) {
            echo base_url() . get_setting_value($setting, 'favicon');
        } else {
            echo get_no_image();
        }
        ?>">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo libs_url() ?>bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo libs_url() ?>font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo libs_url() ?>Ionicons/css/ionicons.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?php echo libs_url() ?>select2/dist/css/select2.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo asset_admin_url() ?>css/AdminLTE.min.css">
        <!-- Admin css -->
        <link rel="stylesheet" href="<?php echo asset_admin_url() ?>css/admin_css.css">
        <!-- Common css -->
        <link rel="stylesheet" href="<?php echo asset_url() ?>css/common.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo asset_admin_url() ?>css/skins/_all-skins.min.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="<?php echo libs_url() ?>morris.js/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?php echo libs_url() ?>jvectormap/jquery-jvectormap.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="<?php echo libs_url() ?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="<?php echo libs_url() ?>bootstrap-daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="<?php echo libs_plugins_url() ?>bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?php echo libs_url() ?>datatables.net-bs/css/dataTables.bootstrap.min.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <!-- jQuery 3 -->
        <script src="<?php echo libs_url() ?>jquery/dist/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="<?php echo libs_url() ?>jquery-ui/jquery-ui.min.js"></script>

        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo libs_url() ?>bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo libs_url() ?>datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo libs_url() ?>datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="<?php echo libs_url() ?>select2/dist/js/select2.full.min.js"></script>
        <!-- Zola common script -->
        <script src="<?php echo libs_url() ?>zolaweb/js/zl_script.js"></script>
        <!-- CK Editor -->
        <!-- <script src="<?php echo libs_url() ?>ckeditor5/ckeditor.js"></script> -->
        <link rel="stylesheet" href="<?php echo libs_url() ?>ckeditor5-document/sample/css/sample.css">
        <script src="<?php echo libs_url() ?>ckeditor5-document/ckeditor.js"></script>

        <!-- CK Finder -->
        <script src="<?php echo libs_url() ?>ckfinder/ckfinder.js"></script>
        <!-- daterangepicker -->
        <script src="<?php echo libs_url() ?>moment/min/moment.min.js"></script>
        <script src="<?php echo libs_url() ?>bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="<?php echo libs_url() ?>bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

        <!-- xdsoft datetimepicker -->
        <link rel="stylesheet" href="<?php echo libs_xdsoft_url() ?>jquery.datetimepicker.min.css">
        <script src="<?php echo libs_xdsoft_url() ?>jquery.datetimepicker.full.min.js"></script>

        <!--Bootstrap colorpicker-->
        <link rel="stylesheet" href="<?php echo libs_boocolp_url() ?>dist/css/bootstrap-colorpicker.css">
        <script src="<?php echo libs_boocolp_url() ?>dist/js/bootstrap-colorpicker.js"></script>

        <!-- Vuejs -->
        <script type="text/javascript" src="<?php echo libs_vuejs_url() ?>vue.min.js"></script>
        <script type="text/javascript" src="<?php echo libs_vuejs_url() ?>axios.min.js"></script>
        <link rel="stylesheet" href="<?php echo libs_vuejs_url()?>vue.transition.css" >
        <script type="text/javascript" src="<?php echo asset_front_url() ?>js/vuejs/vue.common.js"></script>

        <!-- Bootstrap multiple select -->
        <link rel="stylesheet" href="<?php echo libs_bootstrapmsel_url()?>bootstrap-multiselect.css" >
        <script type="text/javascript" src="<?php echo libs_bootstrapmsel_url() ?>bootstrap-multiselect.js"></script>

        <!--Common js-->
        <script type="text/javascript" src="<?php echo asset_front_url() ?>js/admin.js"></script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper" id="root">

            <?php $this->load->view('elements/backend/header'); ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php $this->load->view('elements/backend/sidebar'); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php echo $contents; ?>
            </div>
            <!-- /.content-wrapper -->
            <?php $this->load->view('elements/backend/footer'); ?>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">
                        <h3 class="control-sidebar-heading">Recent Activity</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                        <p>Will be 23 on April 24th</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-user bg-yellow"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                                        <p>New phone +1(800)555-1234</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                        <p>nora@example.com</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-file-code-o bg-green"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                        <p>Execution time 5 seconds</p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                        <h3 class="control-sidebar-heading">Tasks Progress</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Custom Template Design
                                        <span class="label label-danger pull-right">70%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Update Resume
                                        <span class="label label-success pull-right">95%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Laravel Integration
                                        <span class="label label-warning pull-right">50%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Back End Framework
                                        <span class="label label-primary pull-right">68%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->
                    <!-- Settings tab content -->
                    <div class="tab-pane" id="control-sidebar-settings-tab">
                        <form method="post">
                            <h3 class="control-sidebar-heading">General Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Report panel usage
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Some information about this general settings option
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Allow mail redirect
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Other sets of options are available
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Expose author name in posts
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Allow the user to show his name in blog posts
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <h3 class="control-sidebar-heading">Chat Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Show me as online
                                    <input type="checkbox" class="pull-right" checked>
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Turn off notifications
                                    <input type="checkbox" class="pull-right">
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Delete chat history
                                    <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                                </label>
                            </div>
                            <!-- /.form-group -->
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        <!-- Morris.js charts -->
        <script src="<?php echo libs_url() ?>raphael/raphael.min.js"></script>
        <script src="<?php echo libs_url() ?>morris.js/morris.min.js"></script>
        <!-- Sparkline -->
        <script src="<?php echo libs_url() ?>jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="<?php echo libs_plugins_url() ?>jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="<?php echo libs_plugins_url() ?>jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="<?php echo libs_url() ?>jquery-knob/dist/jquery.knob.min.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo libs_plugins_url() ?>bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- Slimscroll -->
        <script src="<?php echo libs_url() ?>jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo libs_url() ?>fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo asset_admin_url() ?>js/adminlte.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="<?php echo asset_admin_url() ?>js/pages/dashboard.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo asset_admin_url() ?>js/demo.js"></script>
    </body>
</html>
