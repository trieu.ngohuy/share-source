<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quản lý Voucher
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header al-right">
                    <button type="button" id="btn_new" class="btn btn-primary">Thêm mới</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tên</th>
                                <th>Mã</th>
                                <th>(%) giảm giá</th>
                                <th>Giảm tối đa</th>
                                <th>Đơn hàng tối thiểu</th>
                                <th>Ngày tạo</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($data as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $value['title']; ?></td>
                                    <td><?php echo $value['code']; ?></td>
                                    <td><?php echo $value['percent']; ?></td>
                                    <td><?php echo $value['money_str']; ?></td>
                                    <td><?php echo $value['min_order_str']; ?></td>
                                    <td><?php echo $value['created_date']; ?></td>
                                    <td>
                                        <a href="#" class="btn-edit" data-index="<?php echo $count - 1; ?>"><i class="fa fa-edit"></i> Sửa</a>
                                        <a href="#" class="btn-remove" data-index="<?php echo $count - 1; ?>"><i class="fa fa-remove"></i> Xóa</a>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Tên</th>
                                <th>Mã</th>
                                <th>(%) giảm giá</th>
                                <th>Ngày tạo</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade" id="modal-data">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Tên tập</label>
                    <input id="txt_title" type="text" class="form-control" placeholder="Tên tập">
                </div>
                <div class="form-group">
                    <label>Tên rút gọn</label>
                    <input id="txt_alias" type="text" class="form-control" placeholder="Tên rút gọn">
                    <p class="help-block">Tên rút gọn không được trùng.</p>
                </div>
                <div class="form-group">
                    <label>Mã</label>
                    <input id="txt_code" type="text" class="form-control" placeholder="Mã">
                    <p class="help-block">Mã không được trùng.</p>
                </div>
                <div class="form-group">
                    <label>(%) giảm giá</label>
                    <input id="txt_percent" type="number" class="form-control" placeholder="%">
                </div>
                <div class="form-group">
                    <label>Giảm tối đa</label>
                    <input id="txt_money" type="number" class="form-control">
                </div>
                <div class="form-group">
                    <label>Giá trị đơn hàng tối thiểu</label>
                    <input id="txt_minorder" type="number" class="form-control">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class='input-group date' id='startday'>
                                <input type='text' class="form-control" id="txt_startdate"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class='input-group date' id='endday'>
                                <input type='text' class="form-control" id="txt_enddate"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Mô tả</label>
                    <textarea id="edi_description" class="form-control" rows="5" placeholder="Mô tả"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="btn-save">Lưu</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Style -->
<style>
    .select2-container .select2-selection--single {
        height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 31px;
    }
</style>
<!-- page script -->
<script>

  //Sdsoft datetimepicket
  var datetime_format = 'DD/MM/YY HH:mm';
  var current_date = new Date();
  current_date = moment(current_date).format(datetime_format);
  $.datetimepicker.setLocale('vi');
  var option = {
    dayOfWeekStart : 1,
    lang:'vi',
  	format:'d/m/Y H:i'
  }
  $('#txt_startdate').datetimepicker(option);
  $('#txt_enddate').datetimepicker(option);
  //$('#txt_startdate').datetimepicker({value:'2011/12/11 12:00'})
    // $('#txt_startdate').datetimepicker();
    // $('#txt_enddate').datetimepicker({
    //     useCurrent: false //Important! See issue #1075
    // });
    // $("#txt_startdate").on("dp.change", function (e) {
    //     $('#txt_enddate').data("DateTimePicker").minDate(e.date);
    // });
    // $("#txt_enddate").on("dp.change", function (e) {
    //     $('#txt_startdate').data("DateTimePicker").maxDate(e.date);
    // });
    //Convert php array to js array
<?php
$js_array = json_encode($data);
echo "var arr_data = " . $js_array . ";\n";
?>
    var mode = 'new';
    var voucher_id = 0;
    $(function () {
        //Initialize datatable
        $('#example1').DataTable();
        //Initialize Select2 Elements
        $('.select2').select2();
    });
    $(document).ready(function () {

        //Title on change
        $("#txt_title").on('input', function () {
            $("#txt_alias").val(convert_vi_to_en($(this).val()));
        });
        //Click new button
        $(document).on('click', '#btn_new', function (e) {
            //Update mode
            mode = 'new';
            //Set title
            $('.modal-title').html('Thêm mới');
            //Project id
            voucher_id = 0;
            //Reset modal data
            $("#txt_title").val('');
            $("#txt_alias").val('');
            $("#txt_percent").val(0);
            $("#txt_money").val(0);
            $("#txt_minorder").val(0);
            $('#txt_startdate').datetimepicker({value:current_date});
            $('#txt_enddate').datetimepicker({value:current_date});
            $("#edi_description").val('');
            //Generrating voucher code
            $.ajax({
                type: 'GET',
                url: "<?php echo base_url() ?>/AdminVoucher/GenerateVoucherCode",
                success: function (objData) {
                    $("#txt_code").val(objData);
                    //Open modal
                    $("#modal-data").modal();
                },
                error: function (jqXHR, exception) {
                    alert('Có lỗi. Thử lại sau.');
                }
            });

        });
        //Click remove button
        $(document).on('click', '.btn-remove', function (e) {
            if (confirm("Are you sure you want to delete this?")) {
                //Get id
                var index = $(this).attr('data-index');
                //Get edit data
                var obj_data = arr_data[index];
                //Project id
                voucher_id = obj_data['voucher_id'];
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url() ?>/AdminVoucher/delete",
                    data: {
                        'voucher_id': voucher_id
                    },
                    success: function (objData) {
                        // if(objData === '1'){
                        //     //Refresh data
                        //     refresh_data();
                        // }else{
                        //     alert('Có lỗi. Thử lại sau.');
                        // }
                    },
                    error: function (jqXHR, exception) {
                        alert('Có lỗi. Thử lại sau.');
                    }
                });
            }
        });
        //Click edit button
        $(document).on('click', '.btn-edit', function (e) {
            //Update mode
            mode = 'edit';
            //Get id
            var index = $(this).attr('data-index');
            //Set title
            $('.modal-title').html('Chỉnh sửa');
            //Get edit data
            var obj_data = arr_data[index];
            //Project id
            voucher_id = obj_data['voucher_id'];
            //Fill modal data
            $("#txt_title").val(obj_data['title']);
            $("#txt_alias").val(obj_data['alias']);
            $("#txt_code").val(obj_data['code']);
            $("#txt_percent").val(obj_data['percent']);
            $("#txt_money").val(obj_data['money']);
            $("#txt_minorder").val(obj_data['min_order']);
            $('#txt_startdate').datetimepicker({value:obj_data['start_date']});
            $('#txt_enddate').datetimepicker({value:obj_data['end_date']});
            $("#edi_description").val(obj_data['intro_text']);
            //Open modal
            $("#modal-data").modal();
        });

        //Modal save button click
        $('#btn-save').click(function () {

            //If add new then check if duplicate alias
            if (verify_data() === false) {
                return;
            }

            //Get submit data
            var fd = new FormData();
            fd.append('voucher_id', voucher_id);
            fd.append('title', $('#txt_title').val());
            fd.append('alias', $('#txt_alias').val());
            fd.append('code', $('#txt_code').val());
            fd.append('percent', $('#txt_percent').val());
            fd.append('money', $('#txt_money').val());
            fd.append('min_order', $('#txt_minorder').val());
            fd.append('start_date', $('#txt_startdate').val());
            fd.append('end_date', $('#txt_enddate').val());
            fd.append('intro_text', $('#edi_description').val());
            fd.append('content_id', $('#com_content').val());

            //Hide modal
            $('#modal-data').modal('toggle');
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url() ?>/AdminVoucher/execute_query",
                data: fd,
                processData: false,
                contentType: false,
                success: function (objData) {
                    //Refresh data
                    refresh_data();
                },
                error: function (jqXHR, exception) {
                    alert('Có lỗi. Thử lại sau.');
                }
            });
        });
    });
    //Verify data
    function verify_data() {
        //Check empty input
        if ($('#txt_title').val() == '') {
            alert('Tên tập không được rỗng!');
            return false;
        }
        if ($('#txt_alias').val() == '') {
            alert('Tên rút gọn không được rỗng!');
            return false;
        }
        //Check duplicate data
//        var result = $.grep(arr_data, function (e) {
//            return e.alias === $('#txt_alias').val() && e.voucher_id !== voucher_id;
//        });
//        if (result.length > 0) {
//            alert('Dữ liệu trùng!');
//            return false;
//        }
    }
    /*
     * Refill datatable data
     */
    function refresh_data() {
        //Refresh data
        $.ajax({
            type: 'GET',
            url: "<?php echo base_url() ?>/AdminVoucher/get_data",
            success: function (data) {
                data = $.parseJSON(data);
                arr_data = data;
                //Fill datatable
                $('#example1 tbody').html('');
                var html = '';
                for (var i = 0; i < data.length; i++) {
                    html += '<tr>'
                            + '<td>' + (i + 1) + '</td>'
                            + '<td>' + data[i]['title'] + '</td>'
                            + '<td>' + data[i]['code'] + '</td>'
                            + '<td>' + data[i]['percent'] + '</td>'
                            + '<td>' + data[i]['money_str'] + '</td>'
                            + '<td>' + data[i]['min_order_str'] + '</td>'
                            + '<td>' + data[i]['created_date'] + '</td>'
                            + '<td>'
                            + '<a href="#" class="btn-edit" data-index="' + i + '"><i class="fa fa-edit"></i> Sửa</a>'
                            + ' <a href="#" class="btn-remove" data-index="' + i + '"><i class="fa fa-remove"></i> Xóa</a>'
                            + '</td>'
                            + '</tr>';
                }
                $('#example1 tbody').html(html);
                //Initialize datatable
                $('#example1').DataTable();

            },
            error: function () {
                alert('Có lỗi. Thử lại sau.');
            }
        });
    }
</script>
