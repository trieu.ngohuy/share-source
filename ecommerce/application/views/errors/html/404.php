<div class="container-fluid al-center" id="cart-index-wrap">
  <br><br>
  <br>
  <h5><?php echo $translate['oops']?>...<br><?php echo $translate['cant_find_page']?></h5>
  <img src="<?php echo asset_front_url() . 'img/icon/error-404.png'?>" width="100px"/>
  <br>
  <p><?php echo $translate['404_des']?></p>
  <br>
  <button type="button" class="btn btn-success btn-custom" v-on:click="homepage()"><?php echo $translate['404_home']?></button>
  <br><br>
</div>
<script type="text/javascript">
    var url_home = '<?php echo base_url()?>';
</script>
<script type="text/javascript" src="<?php echo asset_front_url() ?>js/vuejs/vue.cart.js"></script>
