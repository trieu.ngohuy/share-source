<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = 'Error/error_404';
$route['translate_uri_dashes'] = FALSE;
/*
 * Admin route
 */
$route['admin.html'] = 'AdminSetting/Index';
$route['admin/login.html'] = 'Access/AdminLogin';
$route['admin/logout.html'] = 'Access/AdminLogout';
$route['admin/setting.html'] = 'AdminSetting/Index';
$route['admin/product-category.html'] = 'AdminProductcat/Index';
$route['admin/product.html'] = 'AdminProduct/Index';
$route['admin/blog.html'] = 'AdminContentcat/Index';
$route['admin/article.html'] = 'AdminContent/Index';
$route['admin/menu.html'] = 'AdminMenu/Index';
$route['admin/newsletter.html'] = 'AdminNewsLetter/Index';
$route['admin/store.html'] = 'AdminStore/Index';
$route['admin/store-detail.html'] = 'AdminStoredetail/Index';
$route['admin/voucher.html'] = 'AdminVoucher/Index';
$route['admin/user.html'] = 'AdminUser/Index';
$route['admin/order.html'] = 'AdminCart/Index';
$route['admin/banner.html'] = 'AdminBanner/Index';
$route['admin/subscribes.html'] = 'AdminSubscribes/Index';

/*
 * Client route
 */
//Login
$route['account/login'] = 'access/clientlogin';
//Logout
$route['account/logout'] = 'access/clientlogout';
//Reset password
$route['account/reset-password/(:any)'] = 'access/resetpassword';
//Profile
$route['account'] = 'access/myaccount';
//Profile Edit
$route['account/edit'] = 'access/accountedit';
//Address manager
$route['account/address-book'] = 'access/addressbook';
//Address edit
$route['account/address-book/(:any)'] = 'access/addressbook/$1';
//Orders
$route['account/my-orders'] = 'access/myorders';
//Cart
$route['basket'] = 'cart/index';
//Checkout finish
$route['basket/success/(:any)'] = 'cart/success/$1';
//Store
$route['about-us/store-locator'] = 'store/index';
$route['about-us/store-locator/(:any)'] = 'store/index/$1';
//Search product
$route['search/(:any)'] = 'product/search';
//Product category
$route['category/(:any)'] = 'product/index/$1';
$route['category/(:any)/(:any)'] = 'product/index/$1/$2';
// $route['category/(:any)/category/(:any)'] = 'product/index/$1/$2/$3';
// $route['category/(:any)/accessories/(:any)'] = 'product/index/$1/$2/$3';
//Product detail
$route['product/(:any)'] = 'product/detail/$1';
//Search products
$route['search'] = 'product/search';
//Blog and blog detail
$route['blog'] = 'content/index';
$route['blog/(:any)'] = 'content/index/$1';
$route['blog/detail/(:any)'] = 'content/detail/$1';
//Article detail
$route['article/(:any)'] = 'article/detail/$1/';
//Switch language
$route['switch-lang/(:any)'] = 'LanguageSwitcher/SwitchLang/$1';
