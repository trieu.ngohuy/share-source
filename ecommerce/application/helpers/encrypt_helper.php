<?php

/*
 * Created date: 21-09-2018
 * Version: 1
 * Lastest edited: 21-09-2018
 * Description: This file return base url for specific folders like assets...
 * Author: Zola Web Group
 */
/*
 * Establish password
 */
function password_generator($str = null){
    if($str != null){
        return 'Zl_' . $str . '!123';
    }
}
/*
 * Encrypt password
 */

function password_encrypt($str = null) {
    if ($str == null) {
        return '';
    } else {
        return password_hash($str, PASSWORD_BCRYPT);
    }
}

/*
 * Verify password
 */

function password_check($str = null, $hash = null) {
    if ($str == null || $hash == null) {
        return false;
    } else if (password_verify($str, $hash)) {
        return true;
    } else {
        return false;
    }
}
