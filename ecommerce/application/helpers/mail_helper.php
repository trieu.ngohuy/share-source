<?php

/*
 * Created date: 8-12-2018
 * Version: 1
 * Lastest edited: 8-12-2018
 * Description: This file contents email functions
 * Author: Zola Web Group
 */
/*
 * Send mail
 */

function send_mail($obj = null, $arr_data = null) {
    if ($obj != null && $arr_data != null) {
        //Get setting data
        $arr_setting_data = $obj->setting_model->get_list_data();
        
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => get_setting_value($arr_setting_data, 'email_name'), // change it to yours
            'smtp_pass' => get_setting_value($arr_setting_data, 'email_password'), // change it to yours
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'wordwrap' => TRUE
        );

        //$message = '';
        $obj->load->library('email', $config);
        $obj->email->set_newline("\r\n");
        $obj->email->from(get_setting_value($arr_setting_data, 'email_name')); // change it to yours
        if (isset($arr_data['arr_emails']) && is_array($arr_data['arr_emails'])) {
            for ($i = 0; $i < count($arr_data['arr_emails']); $i++) {
                $obj->email->to($arr_data['arr_emails'][$i]);
            }
        } else if (isset($arr_data['arr_emails']) && $arr_data['arr_emails'] !== ''){
            $obj->email->to($arr_data['arr_emails']);
        }else{
            $obj->email->to(get_setting_value($arr_setting_data, 'email_name'));
        }
        //$obj->email->to('xxx@gmail.com'); // change it to yours
        if (isset($arr_data['subject'])) {
            $obj->email->subject($arr_data['subject']);
        }
        if (isset($arr_data['content'])) {
            $obj->email->message($arr_data['content'] . 'Link: ' . get_setting_value($arr_setting_data, 'website_url'));
        }
        
        if ($obj->email->send() === false) {
            throw new Exception($obj->email->print_debugger());
        }
    }
}
