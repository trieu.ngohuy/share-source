<?php

/*
 * Created date: 06-11-2018
 * Version: 1
 * Lastest edited: 06-11-2018
 * Description: This file handle lang neccessary functions
 * Author: Zola Web Group
 */
/*
 * Return curent lang code
 */

function get_current_lang() {
    $ci = & get_instance();
    $siteLang = $ci->session->userdata(SITE_LANG);
    if ($siteLang) {
        return $siteLang;
    } else {
        return get_lang_data();
    }
}

/*
 * Get default lang
 */

function get_lang_data($arrCondition = array()) {
    $ci = & get_instance();
    //$ci->dbhandle->LoadDatabase($ci);
    $ci->load->model('lang_model');
    // if (count($arrCondition) <= 0) {
    //     $arrCondition = array(
    //         'default' => 1
    //     );
    // }
    $objLang = $ci->lang_model->get_single_data($arrCondition);
    if (!$objLang) {
        throw new Exception('An error occurred when switch language!');
    } else {
        return $objLang;
    }
}
