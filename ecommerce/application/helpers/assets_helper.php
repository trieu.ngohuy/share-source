<?php

/*
 * Created date: 21-09-2018
 * Version: 1
 * Lastest edited: 21-09-2018
 * Description: This file return base url for specific folders like assets...
 * Author: Zola Web Group
 */
/*
 * Return assets url
 */
function asset_url(){
   return base_url().'assets/';
}
/*
 * Return assets admin url
 */
function asset_admin_url(){
   return base_url().'assets/admin/';
}
/*
 * Return assets front url
 */
function asset_front_url(){
   return base_url().'assets/front/';
}
/*
 * Return libs url
 */
function libs_url(){
   return base_url().'libs/';
}
/*
 * Return libs mdb url
 */
function libs_mdb_url(){
   return base_url().'libs/mdb/';
}
/*
 * Return libs jquery url
 */
function libs_jquery_url(){
   return base_url().'libs/jquery/';
}
/*
 * Return libs zolaweb url
 */
function libs_zl_url(){
   return base_url().'libs/zolaweb/';
}
/*
 * Return libs ckeditor url
 */
function libs_ckeditor_url(){
   return base_url().'libs/ckeditor/';
}
/*
 * Return libs plugins url
 */
function libs_plugins_url(){
   return base_url().'libs/plugins/';
}
/*
 * Return libs slick url
 */
function libs_slick_url(){
   return base_url().'libs/slick/';
}
/*
 * Return libs vuejs url
 */
function libs_vuejs_url(){
   return base_url().'libs/vuejs/';
}
/*
 * Return libs easyzoom url
 */
function libs_easyzoom_url(){
   return base_url().'libs/easyzoom/';
}
/*
 * Return libs sdsoft datetimepicker
 */
function libs_xdsoft_url(){
   return base_url().'libs/xdsoft/';
}
/*
 * Return libs boottrap colorpicker
 */
function libs_boocolp_url(){
   return base_url().'libs/bootstrap-colorpicker/';
}
/*
 * Return libs bootstrap-vue url
 */
function libs_bootstrapvue_url(){
   return base_url().'libs/bootstrap-vue/';
}

/*
 * Return libs bootstrap-multiselect url
 */
function libs_bootstrapmsel_url(){
   return base_url().'libs/bootstrap-multiselect/';
}
