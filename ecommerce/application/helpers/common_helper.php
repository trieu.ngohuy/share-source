<?php

/*
 * Created date: 9-12-2018
 * Version: 1
 * Lastest edited: 9-12-2018
 * Description: This file contains common functtons
 * Author: Zola Web Group
 */
/*
 * Genrater token
 */
function create_token(){
    return bin2hex(openssl_random_pseudo_bytes(64));
}
/*
 * Recursive array
 */

function recursive_data($data, $parent_id = 0, $arr_data = array()) {
    foreach ($data as $value) {
        if((int)$value['parent_id'] === (int)$parent_id &&
            (int)$value['comment_id'] !== (int)$parent_id){
            //echo $value['parent_id'] .'/'. $parent_id . '<br>';

            array_merge($arr_data, recursive_data($data, $value['comment_id'], $arr_data));

            // print_r($arr_data);
            // echo '<br>';
        }
    }
    return $arr_data;
}
/*
 * Return no image url
 */
function get_no_image(){
    return asset_url() . 'img/no_image.png';
}
/*
 * Convert string to array
 */
function convert_to_array($arr_tmp) {
    $arr_config = array();
    if ($arr_tmp != '') {
        $arr_tmp = explode('||', $arr_tmp);
        foreach ($arr_tmp as $value) {
            $arr_value = explode(':', $value);
            $arr_config[$arr_value[0]] = $arr_value[1];
        }
        unset($value);
    }
    return $arr_config;
}
/*
* Get setting value
*/
function get_setting_value($arr_data = null, $name = null) {
    //Valid input
    if($arr_data === null || $name === null){
        return '';
    }
    //Get config data
    //print_r($arr_data);
    foreach ($arr_data as $value) {
        if($value['alias'] === $name){
            return $value['value'];
        }
    }
    return '';
}
/*
* Sorting array
*/
function sort_array(&$array, $attr)
{
    $sortarray = array();
    foreach ($array as $key => $row)
    {
        $sortarray[$key] = $row[$attr];
    }

    array_multisort($sortarray, SORT_DESC, $array);
}
/*
* Sorting array descending
*/
function sort_array_descending( $a, $b ) {
    //return  strtotime($b['created_date']) - strtotime($a['created_date']);
    return  $b['content_episodes_id'] - $a['content_episodes_id'];
}
/*
* Sort array alphabetically
*/
function sort_array_2($array){
    usort($array, function($a, $b)
    {
        return strcmp($a['title'], $b['title']);
    });
    return $array;
}
/*
* Create breadcrumb
*/
function create_breadcrumb($arr_data, $parent_gid, $index = 0){
    $arr = array();
    foreach($arr_data as $value){
        if($value['product_category_gid'] == $parent_gid){
            $arr[] = array(
                'title' => $value['title'],
                'url' => $value['url']
            );
            $arr = array_merge($arr, create_breadcrumb($arr_data, $value['parent_gid'], $index+1));
        }
    }
    //Add home
    if($index == 0){
        $arr[] = array(
            'title' => 'Home',
            'url' => base_url()
        );
    }
    return $arr;
}
