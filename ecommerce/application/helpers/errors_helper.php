<?php

/* 
 * Created date: 21-09-2018
 * Version: 1
 * Lastest edited: 21-09-2018
 * Description: This file return base url for specific folders like assets...
 * Author: Zola Web Group
 */
/*
 * Custom exception output data
 */
function custom_exception($obj, $exc){
    $arrTrace = explode("#",$exc->getTraceAsString());
    $strTrace = "";
    foreach ($arrTrace as $value) {
        $strTrace .= $value . '<br>';
    }
    $strTrace = substr($strTrace, 0, strlen($strTrace) - 4);
   show_error($exc->getMessage() . $strTrace, 500, 'An Error Was Encountered');
}