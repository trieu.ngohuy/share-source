<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminUser extends ZLAdmin_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('user_model');
        $this->load->helper('encrypt');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            //$this->arr_view_data = array();

            //Active menu
            $this->arr_view_data['active_menu'] = 'user';
            
            //Get list data
            $arr_data = $this->user_model->get_list_data();
            //Set post data back to view
            $this->arr_view_data['data'] = $arr_data;

            //Set layout
            $this->layout->load('admin', 'user/backend/manager', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    //Update/New data
    function execute_query() {
        //Get post data
        $user_id = $_POST['user_id'];
        $lastname = $_POST['lastname'];
        $firstname = $_POST['firstname'];
        $username = $_POST['username'];
        $prefix = $_POST['prefix'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $type = $_POST['type'];
        $enabled = $_POST['enabled'];

        //Encrypt password
        $arr_post = array(
            'firstname' => $firstname,
            'lastname' => $lastname,
            'username' => $username,
            'prefix' => $prefix,
            'email' => $email,
            'type' => $type,
            'enabled' => $enabled,
        );
        if ($password != '') {
            $arr_post['password'] = password_encrypt(password_generator($password));
        }

        //Execute query
        if ($user_id == 0) {
            //New
            $arr_post['created_date'] = strtotime(date(DATE_FORMAT));
            $arr_data = $this->user_model->insert_new_data($arr_post);
        } else {
            //Update
            $arr_data = $this->user_model->update_existing_data(array(
                'user_id' => $user_id
                    ), $arr_post);
        }
    }

    //Get data
    function get_data() {

        //Get list data
        $arr_data = $this->user_model->get_list_data();
        unset($value);
        echo json_encode($arr_data);
    }

    //Delete
    function delete() {
        //Get post data
        $user_id = $_POST['user_id'];

        //Execute query
        $this->user_model->delete_data(array(
            'user_id' => $user_id
        ));
    }

}
