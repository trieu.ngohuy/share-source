<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminVoucher extends ZLAdmin_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('voucher_model');
    }

    /*
    * Generate voucher code
    */
    public function GenerateVoucherCode(){
        echo strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10));
    }
    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            //$this->arr_view_data = array();

            //Active menu
            $this->arr_view_data['active_menu'] = 'voucher';

            //Get list data
            $arr_data = $this->voucher_model->get_list_data();
            foreach($arr_data as &$value){
              $value['money_str'] =$this->format_price($value['money']);
              $value['min_order_str'] =$this->format_price($value['min_order']);
            }
            //Set post data back to view
            $this->arr_view_data['data'] = $arr_data;

            //Set layout
            $this->layout->load('admin', 'voucher/backend/manager', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    //Update/New data
    function execute_query() {
        //Get post data
        $voucher_id = $_POST['voucher_id'];
        $arr_query_data = array(
            'title' => $_POST['title'],
            'alias' => $_POST['alias'],
            'code' => $_POST['code'],
            'percent' => $_POST['percent'],
            'money' => $_POST['money'],
            'min_order' => $_POST['min_order'],
            'intro_text' => $_POST['intro_text'],
            'start_date' => $_POST['start_date'],
            'end_date' => $_POST['end_date']
        );

        //Execute query
        if ($voucher_id == 0) {
            $arr_query_data['created_date'] = strtotime(date(DATE_FORMAT));
            //New
            $arr_data = $this->voucher_model->insert_new_data($arr_query_data);
        } else {
            //Update
            $arr_data = $this->voucher_model->update_existing_data(array(
                'voucher_id' => $voucher_id
                    ), $arr_query_data);
        }
    }

    //Get data
    function get_data() {

        //Get list data
        $arr_data = $this->voucher_model->get_list_data();
        foreach($arr_data as &$value){
          $value['money_str'] =$this->format_price($value['money']);
          $value['min_order_str'] =$this->format_price($value['min_order']);
        }
        echo json_encode($arr_data);
    }

    //Delete
    function delete() {
        //Get post data
        $voucher_id = $_POST['voucher_id'];

        //Get detail
        $arr_data = $this->voucher_model->get_single_data(array(
            'voucher_id' => $voucher_id
        ));

        //Execute query
        $this->voucher_model->delete_data(array(
            'voucher_id' => $voucher_id
        ));
    }

}
