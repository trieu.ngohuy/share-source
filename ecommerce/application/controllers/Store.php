<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends ZLFront_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('store_model');
        $this->load->model('storedetail_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index($alias = '') {
        try {
            $search_key = '';
            //Get translate
            $this->get_translate();
            //Get list store
            $list_location = $this->store_model->get_list_data();
            $this->arr_view_data['location'] = $list_location;
            //Get list store detail
            $active_list = array();
            if($alias == '' && count($list_location) > 0){
                $active_list = $list_location[0];
            }
            if($alias != ''){
              $active_list = $this->store_model->get_single_data(array(
                'alias' => $alias
              ));
              //Check valid data
              if($active_list === false){
                redirect(URL_HOME, 'refresh');
              }
            }
            $list_detail = $this->storedetail_model->get_list_data(null, array(
              'store_gid' => $active_list['store_gid']
            ));
            //search
            if ($this->input->post('store')) {
              $search_key = $this->input->post('store');
              if($search_key != ''){
                $tmp = array();
                foreach($list_detail as $value){
                  if (strpos(mb_strtolower($value['title']), mb_strtolower($search_key)) !== false) {
                      array_push($tmp, $value);
                  }
                }
                $list_detail = $tmp;
              }
            }

            $this->arr_view_data['key'] = $search_key;
            $this->arr_view_data['active_location'] = $active_list;
            $this->arr_view_data['detail'] = $list_detail;
            //Set layout
            $this->layout->load('front', 'store/frontend/index', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
    * Get translate
    */
    function get_translate(){
      $arr_translate = $this->get_default_translate();

      $arr_translate['enter_store_name'] = $this->lang->line('enter_store_name');
      $arr_translate['no_store'] = $this->lang->line('no_store');
      $arr_translate['no_place'] = $this->lang->line('no_place');
      $arr_translate['bg_all'] = $this->lang->line('bg_all');
      $arr_translate['open_until'] = $this->lang->line('open_until');
      $arr_translate['today'] = $this->lang->line('today');

      $this->arr_view_data['translate'] = $arr_translate;
    }
}
