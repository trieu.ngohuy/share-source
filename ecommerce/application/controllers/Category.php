<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends ZLFront_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('category_model');
        $this->load->model('content_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            $arr_data = $this->category_model->get_list_data();

            //Set post data back to view
            $this->arr_view_data['data'] = $arr_data;

            //Website title
            $this->arr_view_data['website_title'] = 'Thể loại';

            //Set layout
            $this->layout->load('category', 'category/frontend/index', $this->arr_view_data);   
            
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
    * Detail function
    */
    public function detail($alias){
        try {
            //Get category detail
            $obj_data = $this->category_model->get_single_data(array(
                'alias' =>$alias
            ));
            //Get list content
            $arr_content = sort_array_2($this->content_model->get_list_data());
            $arr_data = array();
            foreach ($arr_content as $value) {
                $arr_tmp = explode('||', $value['list_content_category_id']);
                if (in_array($obj_data['content_category_id'], $arr_tmp)) {
                    $arr_data[] = $value;
                }
            }
            unset($value);

            //Set post data back to view
            $this->arr_view_data['data'] = $arr_data;
            $this->arr_view_data['detail'] = $obj_data;

            //Website title
            $this->arr_view_data['website_title'] = $obj_data['title'];
            
            //Set layout
            $this->layout->load('project', 'category/frontend/detail', $this->arr_view_data);   
            
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
}
