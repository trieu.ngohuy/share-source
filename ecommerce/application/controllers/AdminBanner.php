<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminBanner extends ZLAdmin_Controller {
  private $_table = 'banner';
  private $_key = 'banner_gid';
  private $_max_key = 'banner_id';
  private $_model = 'banner_model';
    /*
     * Constuctor function
     */
    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model($this->_model);
        //Load extras model
        $this->load->model('product_model');
        $this->load->model('contentcat_model');
        $this->load->model('content_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            //$this->arr_view_data = array();

            //Active menu
            $this->arr_view_data['active_menu'] = $this->_table;

            //Get main list data
            $this->arr_view_data['data'] = $this->get_main_data();

            //Get extras data
            $this->get_extras_data();

            //Set layout
            $this->layout->load('admin', $this->_table . '/backend/manager', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    //Update/New data
    function execute_query() {
        //Get post data
        $key = $_POST[$this->_key];
        $data = array();
        $model = $this->_model;
        //Get common data
        $data = array(
          'type' => $_POST['type'],
        );
        //Upload logo
        $upload = $this->upload_images('logo', $this->_table);
        if($upload['status'] == 0){
          echo json_encode($upload);
          return;
        }else if($upload['status'] == 1){
          $data['logo'] = $upload['path'];
        }
        //vi
        $vi_data = array();
        $lang = 'vi_';
        $vi_data['title'] =$_POST[$lang . 'title'];
        $vi_data['link'] =$_POST[$lang . 'link'];
        $vi_data['controller'] =$_POST[$lang . 'controller'];
        $vi_data['function'] =$_POST[$lang . 'function'];
        $vi_data['params'] =$_POST[$lang . 'params'];
        $vi_data = array_merge($vi_data, $data);
        //en
        $en_data = array();
        $lang = 'en_';
        $en_data['title'] =$_POST[$lang . 'title'];
        $en_data['link'] =$_POST[$lang . 'link'];
        $en_data['controller'] =$_POST[$lang . 'controller'];
        $en_data['function'] =$_POST[$lang . 'function'];
        $en_data['params'] =$_POST[$lang . 'params'];
        $en_data = array_merge($en_data, $data);

        //Execute query
        if ($key == 0) {

          //Get max gid
          $max = $this->get_max_value($this->$model->get_list_data(), $this->_max_key) + 1;

          //Add lang_id
          $vi_data['lang_id'] = 1;
          $en_data['lang_id'] = 2;
          //Add created_date
          $vi_data['created_date'] = strtotime(date(DATE_FORMAT));
          $en_data['created_date'] = strtotime(date(DATE_FORMAT));
          //Add gid
          $vi_data[$this->_key] = $max;
          $en_data[$this->_key] = $max;

          //New
          $this->$model->insert_new_data($vi_data);
          $this->$model->insert_new_data($en_data);

        } else {
          //Update
          $this->$model->update_existing_data(array(
              $this->_key => $key,
              'lang_id' => 1
            ), $vi_data);
            $this->$model->update_existing_data(array(
                $this->_key => $key,
                'lang_id' => 2
              ), $en_data);
        }
    }

    //Get data
    function get_data() {
        //Get list data
        echo json_encode($this->get_main_data());
    }
    //Delete
    function delete() {
        //Execute query
        $model = $this->_model;
        $this->$model->delete_data(array(
            $this->_key => $_POST[$this->_key]
        ));
    }
    /*
    * Get main list data
    */
    function get_main_data(){
      $model = $this->_model;
      $arr_data = $this->$model->get_list_data(null, array(
        'lang_id' => 1
      ));
      return $this->format_multilanguage_array($arr_data, $this->_model, $this->_key);
    }
    /*
    * Get extras data
    */
    private function get_extras_data(){
      //Get list product category
      $this->arr_view_data['vi_productcat'] = $this->hierarchical_array($this->productcat_model->get_list_data(null, array('lang_id' => 1)), 'product_category_gid', 0);
      $this->arr_view_data['en_productcat'] = $this->hierarchical_array($this->productcat_model->get_list_data(null, array('lang_id' => 2)), 'product_category_gid', 0);
      //Get list products
      $this->arr_view_data['vi_products'] = $this->product_model->get_list_data(null, array('lang_id' => 1));
      $this->arr_view_data['en_products'] = $this->product_model->get_list_data(null, array('lang_id' => 2));
      //Get list content category
      $this->arr_view_data['vi_contentcat'] = $this->contentcat_model->get_list_data(null, array('lang_id' => 1));
      $this->arr_view_data['en_contentcat'] = $this->contentcat_model->get_list_data(null, array('lang_id' => 2));
      //Get list content
      $this->arr_view_data['vi_contents'] = $this->content_model->get_list_data(null, array('lang_id' => 1));
      $this->arr_view_data['en_contents'] = $this->content_model->get_list_data(null, array('lang_id' => 2));
    }
}
