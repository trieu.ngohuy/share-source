<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends ZLFront_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('product_model');
        $this->load->model('productcat_model');
        $this->load->model('properties_model');
    }
    /**
     * Index Page for this controller.
     */
    public function index($alias, $mode = null) {
        try {
            //Get translate
            $this->get_translate();
            //Get list product category
            $obj_cat = $this->productcat_model->get_single_data(array(
                'alias' => $alias
            ));

            //Check valid data
            if($obj_cat === false){
              $this->valid_link($obj_cat, 'productcat_model', $alias, 'product_category_gid');
            }

            //Check valid data
            if($obj_cat === false){
              redirect(URL_HOME, 'refresh');
            }

            //Website title
            $obj_cat['url'] = $obj_cat['url'] . '/all';
            $this->arr_view_data['objData'] = $obj_cat;

            //Set layout
            if($obj_cat['parent_gid'] == 0 && $mode == null){

              //If there is no config then go to cat detail page
              if($obj_cat['config'] == ''){
                redirect(URL_PRODUCTCAT . $obj_cat['alias'] . '/all', 'refresh');
              }

                //Read config json
                $json = json_decode($obj_cat['config'], true);
                //Get data
                $this->arr_view_data = $this->get_cat_index_data($json, $this->arr_view_data);
                $this->layout->load('front', 'product/frontend/index', $this->arr_view_data);
            }else{
                $this->arr_view_data = $this->get_cat_detail_data($obj_cat, $this->arr_view_data, $mode);
                $this->layout->load('front', 'product/frontend/cat_detail', $this->arr_view_data);
            }
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
    * Get translate
    */
    function get_translate(){
      $arr_translate = $this->get_default_translate();

      //Product category
      $arr_translate['shop_all'] = $this->lang->line('shop_all');
      $arr_translate['bestsellers'] = $this->lang->line('bestsellers');
      $arr_translate['new_in'] = $this->lang->line('new_in');
      //Product category detail
      $arr_translate['results'] = $this->lang->line('results');
      $arr_translate['sort_by'] = $this->lang->line('sort_by');
      $arr_translate['view'] = $this->lang->line('view');
      $arr_translate['search_all'] = $this->lang->line('search_all');
      $arr_translate['recommended'] = $this->lang->line('recommended');
      $arr_translate['high_price'] = $this->lang->line('high_price');
      $arr_translate['low_price'] = $this->lang->line('low_price');
      $arr_translate['newest'] = $this->lang->line('newest');
      $arr_translate['all'] = $this->lang->line('all');
      $arr_translate['low'] = $this->lang->line('low');
      $arr_translate['to'] = $this->lang->line('to');
      $arr_translate['high'] = $this->lang->line('high');
      $arr_translate['clear_all'] = $this->lang->line('clear_all');
      $arr_translate['your_selection'] = $this->lang->line('your_selection');
      $arr_translate['product_type'] = $this->lang->line('product_type');
      $arr_translate['style'] = $this->lang->line('style');
      $arr_translate['color'] = $this->lang->line('color');
      $arr_translate['material'] = $this->lang->line('material');
      $arr_translate['price'] = $this->lang->line('price');
      $arr_translate['size'] = $this->lang->line('size');
      //Search
      $arr_translate['you_search_for'] = $this->lang->line('you_search_for');
      //Product detail
      $arr_translate['code'] = $this->lang->line('code');
      $arr_translate['add_to_bag'] = $this->lang->line('add_to_bag');
      $arr_translate['share'] = $this->lang->line('share');
      $arr_translate['detail'] = $this->lang->line('detail');
      $arr_translate['feature'] = $this->lang->line('feature');
      $arr_translate['related'] = $this->lang->line('related');
      $arr_translate['qty'] = $this->lang->line('qty');
      $arr_translate['pro_text'] = $this->lang->line('pro_text');
      $arr_translate['refund_maintenance'] = $this->lang->line('refund_maintenance');
      $arr_translate['full_text'] = $this->lang->line('full_text');

      $this->arr_view_data['translate'] = $arr_translate;
    }
    /*
    * Get cat detail data
    */
    function get_cat_detail_data($obj_cat, $arr_view_data, $mode){
        //Get breadcrumbs
        $arr = $this->productcat_model->get_list_data();
        //print_r($arr);
        $arr_view_data['breadcrumb'] = $this->create_breadcrumb($arr, $obj_cat['product_category_gid']);

        //Get list subcat
        $arr_subcat = $this->productcat_model->get_list_data(null, array(
            'parent_gid' => $obj_cat['product_category_gid']
        ));
        $arr_view_data['sub_cat'] = $arr_subcat;

        //Get product type
        $arr_product = array();
        $arr_style = array();
        $product_type = $this->productcat_model->get_list_data(null, array(
            'parent_gid' => $obj_cat['product_category_gid'],
            'type' => 1
        ));
        $arr_view_data['product_type'] = $product_type;
        foreach($product_type as $value){
            //Get list style
            $style = $this->productcat_model->get_list_data(null, array(
                'parent_gid' => $value['product_category_gid'],
                'type' => 2
            ));
            $arr_style = array_merge($arr_style, $style);
            //Get list products
            foreach($style as $value2){
                $tmp = $this->product_model->get_list_data(null, array(
                    'product_category_gid' => $value2['product_category_gid']
                ));
                foreach($tmp as &$value3){
                  $value3['price'] = $this->format_price($value3['price_vn']);
                }
                unset($value3);
                $arr_product = array_merge($arr_product, $tmp);
            }
            unset($value2);
        }
        unset($value);
        $arr_view_data['products'] = $arr_product;
        $arr_view_data['style'] = $arr_style;
        //Get list color
        $arr_view_data['color'] = $this->properties_model->get_list_data(null, array(
            'type' => 0
        ));
        //Get list material
        $arr_view_data['material'] = $this->properties_model->get_list_data(null, array(
            'type' => 1
        ));
        //Get list size
        $arr_view_data['size'] = $this->properties_model->get_list_data(null, array(
            'type' => 2
        ));
        //Get subcat
        $arr_view_data['subcat'] = $this->productcat_model->get_list_data(null, array(
            'parent_gid' => $obj_cat['product_category_gid']
        ));

        return $arr_view_data;
    }
    /*
    * Get cat index data
    */
    function get_cat_index_data($json, $arr_view_data){
      //title
      $arr_view_data['title'] = $json['title'];
      //description
      $arr_view_data['description'] = $json['description'];
      //Get top cats
      $arr_main_cat = array();
      foreach($json['list_top_cats_gid'] as $value){
          $arr_main_cat[] = $this->productcat_model->get_single_data(array(
              'product_category_gid' => $value
          ));
      }
      unset($value);
      $arr_view_data['list_top_cats_gid'] = $arr_main_cat;
      //Show bestsellers
      if($json['show_bestsellers']){
          $arr_view_data['show_bestsellers'] = $json['show_bestsellers'];
          $arr_view_data['list_bestsellers_products_gid'] = $this->get_list_data($json, 'list_bestsellers_products_gid', 'product_model', 'product_gid');
      }
      //Show newin
      if($json['show_newin']){
          $arr_view_data['show_newin'] = $json['show_newin'];
          $arr_view_data['list_newin_products_gid'] = $this->get_list_data($json, 'list_newin_products_gid', 'product_model', 'product_gid');
      }
      //Show middle cats
      if($json['show_middle_cats']){
          $arr_view_data['show_middle_cats'] = $json['show_middle_cats'];
          $arr_view_data['list_middle_cats_gid'] = $this->get_list_data($json, 'list_middle_cats_gid', 'productcat_model', 'product_category_gid');
      }
      //Show bottom cats
      if($json['show_bottom_cats']){
          $arr_view_data['show_bottom_cats'] = $json['show_bottom_cats'];
          $arr_view_data['list_bottom_cats_gid'] = $this->get_list_data($json, 'list_bottom_cats_gid', 'productcat_model', 'product_category_gid');
      }
      return $arr_view_data;
    }
    /*
    * Get list data
    */
    function get_list_data($json, $name, $model, $field){
        $arr = array();
        foreach($json[$name] as $value){
            $tmp = $this->$model->get_single_data(array(
                $field => $value
            ));
            if(isset($tmp['price_vn'])){
                $tmp['price'] = $this->format_price($tmp['price_vn']);
            }
            $arr[] = $tmp;
        }
        unset($value);
        return $arr;
    }
    /**
     * Detail
     */
    public function detail($alias) {
        try {
            //$this->session->set_userdata(SS_CART_ORDER, null);
            //Get translate
            $this->get_translate();

            //Get project detail
            $obj_data = $this->product_model->get_single_data(array(
                'alias' =>$alias
            ));

            //Check valid data
            if($obj_data === false){
              $this->valid_link($obj_data, 'product_model', $alias, 'product_gid');
            }

            //price
            $obj_data['price'] = $this->format_price($obj_data['price_vn']);
            if($obj_data['price_vn_sale'] !== "0"){
                $obj_data['price_sale'] = $this->format_price($obj_data['price_vn_sale']);
            }else{
              $obj_data['price_vn_sale'] = 0;
            }

            //Get color
            $this->get_properties('color', 0, $obj_data['color_gid']);

            //Get size
            $this->get_properties('size', 2, $obj_data['size_gid']);

            //Get material
            $this->get_properties('material', 1, $obj_data['material_gid']);

            //Get related products
            $arr_data = $this->product_model->get_list_data(null, array(
                'product_category_gid' =>$obj_data['product_category_gid']
            ));
            foreach($arr_data as &$value){
              $value['price'] = $this->format_price($value['price_vn']);
            }
            unset($value);

            //Get breadcrumbs
            $arr = $this->productcat_model->get_list_data();
            $tmp = $this->create_breadcrumb($arr, $obj_data['product_category_gid']);
            array_push($tmp, array(
              'title' => $obj_data['title'],
              'url' => $obj_data['url']
            ));
            $this->arr_view_data['breadcrumb'] = $tmp;

            //Set post data back to view
            $this->arr_view_data['detail'] = $obj_data;
            $this->arr_view_data['related'] = $arr_data;

            //Website title
            $this->arr_view_data['website_title'] = $obj_data['title'];

            //Add to cart
            //$this->add_to_cart();

            //Set layout
            $this->layout->load('front', 'product/frontend/detail', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
    * Add to cart
    */
    function add_to_cart(){
      $arr_post_data = $_POST;
      $arr_return = array(
        'status' => 1
      );
      //Get cart
      $cart = $this->session->userdata(SS_CART_ORDER);

      //Add code
      if(!isset($cart['code'])){
        $cart['code'] = strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10));
      }

      //Check if product already added to cart
      if(!isset($cart['products'])){
        $cart['products'][] = array(
          'product_gid' => $arr_post_data['product_gid'],
          'color_gid' => $arr_post_data['color_gid'],
          'material_gid' => $arr_post_data['material_gid'],
          'size_gid' => $arr_post_data['size_gid'],
          'count' => $arr_post_data['qty']
        );
        //Return message
        // $this->arr_view_data['add_to_cart'] = array(
        //     'status' => 1,
        //     'message' => $this->lang->line('msg_succes_add_to_cart')
        // );
        $arr_return['message'] = $this->lang->line('msg_succes_add_to_cart');
      }else{
        $index = -1;
        $count = 0;
        foreach($cart['products'] as $key => $value){
          if($arr_post_data['product_gid'] == $value['product_gid']){
            $index = $key;
            $count = $value['count'];
          }
        }
        if($index == -1){
          array_push($cart['products'], array(
            'product_gid' => $arr_post_data['product_gid'],
            'color_gid' => $arr_post_data['color_gid'],
            'material_gid' => $arr_post_data['material_gid'],
            'size_gid' => $arr_post_data['size_gid'],
            'count' => $arr_post_data['qty']
          ));
          //Return message
          // $this->arr_view_data['add_to_cart'] = array(
          //     'status' => 1,
          //     'message' => $this->lang->line('msg_succes_add_to_cart')
          // );
          $arr_return['message'] = $this->lang->line('msg_succes_add_to_cart');
        }else{
          $cart['products'][$index]['count'] = $arr_post_data['qty'];
          $cart['products'][$index]['color_gid'] = $arr_post_data['color_gid'];
          $cart['products'][$index]['material_gid'] = $arr_post_data['material_gid'];
          $cart['products'][$index]['size_gid'] = $arr_post_data['size_gid'];
          //Return message
          // $this->arr_view_data['add_to_cart'] = array(
          //     'status' => 1,
          //     'message' => $this->lang->line('msg_update_cart')
          // );
          $arr_return['message'] = $this->lang->line('msg_update_cart');
        }
      }

      //Update cart
      $this->session->set_userdata(SS_CART_ORDER, $cart);
      $arr_return['data'] = $this->GetCart();
      echo json_encode($arr_return);
    }
    /*
    * Search products
    */
    function search()
    {
      try{
        //Get translate
        $this->get_translate();
        $arr_post_data = $this->input->post('search');
        if($arr_post_data == null){
          redirect(URL_404, 'refresh');
        }
        //Get product type
        $arr_product = array();
        $arr_style = array();
        $product_type = $this->productcat_model->get_list_data(null, array(
          'type' => 1
        ));
        $this->arr_view_data['product_type'] = $product_type;
        foreach($product_type as $value){
            //Get list style
            $style = $this->productcat_model->get_list_data(null, array(
                'parent_gid' => $value['product_category_gid']
            ));
            $arr_style = array_merge($arr_style, $style);
            //Get list products
            foreach($style as $value2){
                $tmp = $this->product_model->get_list_data(null, array(
                    'product_category_gid' => $value2['product_category_gid']
                ));
                foreach($tmp as &$value3){
                  $value3['price'] = $this->format_price($value3['price_vn']);
                  if (strpos(mb_strtolower($value3['title']), mb_strtolower($arr_post_data)) !== false) {
                      array_push($arr_product, $value3);
                  }
                }
                unset($value3);

            }
            unset($value2);
        }
        unset($value);
        $this->arr_view_data['products'] = $arr_product;
        $this->arr_view_data['style'] = $arr_style;
        //Get list color
        $this->arr_view_data['color'] = $this->properties_model->get_list_data(null, array(
            'type' => 0
        ));
        //Get list material
        $this->arr_view_data['material'] = $this->properties_model->get_list_data(null, array(
            'type' => 1
        ));
        //Get list size
        $this->arr_view_data['size'] = $this->properties_model->get_list_data(null, array(
            'type' => 2
        ));
        //key
        $this->arr_view_data['key'] = $arr_post_data;

        $this->layout->load('front', 'product/frontend/search', $this->arr_view_data);
      } catch (Exception $exc) {
          custom_exception($this, $exc);
      }
    }
    /*
    * Get product detail properties
    */
    function get_properties($key, $type, $data){
      $arr_data = array();
      $data = explode(',', $data);
      foreach($data as $value){
        $tmp = $this->properties_model->get_single_data(array(
          'type' => $type,
          'product_properties_gid' => $value
        ));
        $arr_data[]= $tmp;
      }
      unset($value);
      $this->arr_view_data[$key] = $arr_data;
    }
}
