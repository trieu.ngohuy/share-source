<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LanguageSwitcher extends CI_Controller {

    public function __construct() {

        parent::__construct();
    }

    /*
     * Switch lang
     */
    function SwitchLang($language = "") {
        if($language == ""){
            $language = get_lang_data(array('code' => DEFAULT_LANG));
        }else{
            $language = get_lang_data(array('code' => $language));
        }
        if($this->session->userdata(SS_MODE) == 'client'){
            $this->session->set_userdata(SITE_LANG, $language);
        }else{
          $this->session->set_userdata(SS_ADMIN_ACTIVE_LANG, $language);
        }
        //Go to homepage
        redirect($_SERVER['HTTP_REFERER']);
    }
}
