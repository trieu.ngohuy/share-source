<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends ZLFront_Controller {

  /*
   * Constructor
   */

   function __construct() {
      parent::__construct();

      $this->load->model('banner_model');
    }
    /**
     * Index Page for this controller.
     */
    public function index() {
        //Get translate
        $this->get_translate();
        //Website title
        $this->arr_view_data['website_title'] = 'Trang chủ';

        //Get top banner
        $tmp = $this->banner_model->get_list_data(null, array(
          'type' => 0
        ));
        $this->arr_view_data['top_banner'] = $tmp;

        //Get bottom banner
        $tmp = $this->banner_model->get_list_data(null, array(
          'type' => 1
        ));
        $this->arr_view_data['bottom_banner'] = $tmp;

        //Get top product category
        $tmp = $this->productcat_model->get_list_data(null, array(
          'display_position' => 1
        ));
        $this->arr_view_data['top_cat'] = $tmp;

        //Get bottom product category
        $tmp = $this->productcat_model->get_list_data(null, array(
          'display_position' => 2
        ));
        $this->arr_view_data['bottom_cat'] = $tmp;

        //Get homepage conig data
        $config = json_decode($this->arr_view_data['setting']['home_config']['value'], true);
        foreach($config as $value){
          if($value['lang_id'] == $this->arr_view_data['active_lang']['lang_id']){
            $config = $value['config'];
            break;
          }
        }
        $arr = array();
        //Get top bannerfunction
        $arr['top_banner'] = array(
          'url' => $this->routes($config['top_banner']['controller'], $config['top_banner']['function'], $config['top_banner']['params']),
          'img' => base_url() . $config['top_banner']['img']
        );
        //Get list top cat
        $arr_products = array();
        foreach($config['top_productcats'] as $value){
          $arr_products[] = $this->productcat_model->get_single_data(array(
            'product_category_gid' => $value
          ));
        }
        $arr['top_productcats'] = $arr_products;
        //Get list bottom cat
        $arr_products = array();
        foreach($config['bottom_productcats'] as $value){
          $arr_products[] = $this->productcat_model->get_single_data(array(
            'product_category_gid' => $value
          ));
        }
        $arr['bottom_productcats'] = $arr_products;
        //Get bottom banner
        $arr['bottom_banner'] = array(
                  'url' => $this->routes($config['bottom_banner']['controller'], $config['bottom_banner']['function'], $config['bottom_banner']['params']),
                  'img' => base_url() . $config['bottom_banner']['img']
                );
        //Title
        $arr['title'] = $config['title'];
        //Description
        $arr['description'] = $config['description'];
        $this->arr_view_data['setting']['home_config']['value'] = $arr;

        //Set layout
        $this->layout->load('front', 'home/index', $this->arr_view_data);
    }
    /*
    * Get translate
    */
    function get_translate(){
      $arr_translate = $this->get_default_translate();
      $this->arr_view_data['translate'] = $arr_translate;
    }

}
