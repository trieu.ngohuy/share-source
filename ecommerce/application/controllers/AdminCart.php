<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminCart extends ZLAdmin_Controller {
  private $_table = 'cart';
  private $_key = 'cart_id';
  private $_max_key = 'cart_id';
  private $_model = 'cart_model';
    /*
     * Constuctor function
     */
    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model($this->_model);
        //Load extras model
        $this->load->model('product_model');
        $this->load->model('cartdetail_model');
        $this->load->model('user_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            //$this->arr_view_data = array();

            //Active menu
            $this->arr_view_data['active_menu'] = $this->_table;

            //Get main list data
            $this->arr_view_data['data'] = $this->get_main_data();

            //Get extras data
            $this->get_extras_data();

            //Set layout
            $this->layout->load('admin', $this->_table . '/backend/manager', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    //Update/New data
    function execute_query() {
        //Get post data
        $key = $_POST[$this->_key];
        $data = array();
        $model = $this->_model;
        //Get common data
        $data = array(
          'status' => $_POST['status']
        );
        //Execute query
        if ($key == 0) {

          // //Get max gid
          // $max = $this->get_max_value($this->$model->get_list_data(), $this->_max_key) + 1;
          //
          // //Add lang_id
          // $vi_data['lang_id'] = 1;
          // $en_data['lang_id'] = 2;
          // //Add created_date
          // $vi_data['created_date'] = strtotime(date(DATE_FORMAT));
          // $en_data['created_date'] = strtotime(date(DATE_FORMAT));
          // //Add gid
          // $vi_data[$this->_key] = $max;
          // $en_data[$this->_key] = $max;
          // // echo $key;
          // // print_r($vi_data);
          // // print_r($en_data);
          // //New
          // $this->$model->insert_new_data($vi_data);
          // $this->$model->insert_new_data($en_data);

        } else {
          // echo $key;
          // print_r($vi_data);
          // print_r($en_data);
          //Update
          $this->$model->update_existing_data(array(
              $this->_key => $key,
            ), $data);
            // $this->$model->update_existing_data(array(
            //     $this->_key => $key,
            //     'lang_id' => 2
            //   ), $en_data);
        }
    }

    //Get data
    function get_data() {
        //Get list data
        echo json_encode($this->get_main_data());
    }
    //Delete
    function delete() {
        //Execute query
        $model = $this->_model;
        $this->$model->delete_data(array(
            $this->_key => $_POST[$this->_key]
        ));
    }
    /*
    * Get main list data
    */
    function get_main_data(){
      $model = $this->_model;
      $arr_data = $this->$model->get_list_data();
      foreach($arr_data as &$value){
        //Status
        if($value['status'] == 0){
          $value['status_text'] = 'Progressing';
        }elseif($value['status'] == 1){
          $value['status_text'] = 'Payed';
        }
        //Cart detail
        $value['products'] = $this->cartdetail_model->get_list_data(null, array(
          'cart_id' => $value['cart_id']
        ));
        //Progressing
        $value = $this->progress_cart_data($value, $value);
      }
      unset($value);
      return $arr_data;
    }
    /*
    * Get extras data
    */
    private function get_extras_data(){
      // //colors
      // $this->arr_view_data['colors'] = $this->properties_model->get_list_data(null, array('lang_id' => 1, 'type' => 0));
      // //material
      // $this->arr_view_data['material'] = $this->properties_model->get_list_data(null, array('lang_id' => 1, 'type' => 1));
      // //size
      // $this->arr_view_data['size'] = $this->properties_model->get_list_data(null, array('lang_id' => 1, 'type' => 2));
      // //product category
      // $this->arr_view_data['productcat'] = $this->productcat_model->get_list_data(null, array('lang_id' => 1));
    }
}
