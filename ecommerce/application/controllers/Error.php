<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Error extends ZLFront_Controller {
  /*
   * Constuctor function
   */
  function __construct() {
      parent::__construct();
  }
  /*
  * 404
  */
  function error_404(){
    //Get translate
    $this->get_translate();
    $this->layout->load('front', 'errors/html/404', $this->arr_view_data);
  }
  /*
  * Get translate
  */
  function get_translate(){
    $arr_translate = $this->get_default_translate();

    $arr_translate['oops'] = $this->lang->line('oops');
    $arr_translate['cant_find_page'] = $this->lang->line('cant_find_page');
    $arr_translate['404_des'] = $this->lang->line('404_des');
    $arr_translate['404_home'] = $this->lang->line('404_home');

    $this->arr_view_data['translate'] = $arr_translate;
  }
}
