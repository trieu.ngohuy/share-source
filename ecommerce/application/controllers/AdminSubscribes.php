<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminSubscribes extends ZLAdmin_Controller {
  private $_table = 'subscribes';
  private $_key = 'subscribes_id';
  private $_max_key = 'subscribes_id';
  private $_model = 'subscribes_model';
    /*
     * Constuctor function
     */
    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model($this->_model);
        //Load extras model
        // $this->load->model('product_model');
        // $this->load->model('contentcat_model');
        // $this->load->model('content_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            //$this->arr_view_data = array();

            //Active menu
            $this->arr_view_data['active_menu'] = $this->_table;

            //Get main list data
            $this->arr_view_data['data'] = $this->get_main_data();

            //Get extras data
            //$this->get_extras_data();

            //Set layout
            $this->layout->load('admin', $this->_table . '/backend/manager', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    //Update/New data
    function execute_query() {
        //Get post data
        $key = $_POST[$this->_key];
        $data = array();
        $model = $this->_model;
        
        //Get common data
        $data = array(
          'email' => $_POST['email'],
        );

        //Execute query
        if ($key == 0) {

          //Add created_date
          $data['created_date'] = strtotime(date(DATE_FORMAT));

          //New
          $this->$model->insert_new_data($data);

        } else {
          //Update
          $this->$model->update_existing_data(array(
              $this->_key => $key
            ), $data);
        }
    }

    //Get data
    function get_data() {
        //Get list data
        echo json_encode($this->get_main_data());
    }
    //Delete
    function delete() {
        //Execute query
        $model = $this->_model;
        $this->$model->delete_data(array(
            $this->_key => $_POST[$this->_key]
        ));
    }
    /*
    * Get main list data
    */
    function get_main_data(){
      $model = $this->_model;
      $arr_data = $this->$model->get_list_data();
      return $arr_data;
    }
    /*
    * Get extras data
    */
    private function get_extras_data(){
      //Get list product category
      $this->arr_view_data['vi_productcat'] = $this->hierarchical_array($this->productcat_model->get_list_data(null, array('lang_id' => 1)), 'product_category_gid', 0);
      $this->arr_view_data['en_productcat'] = $this->hierarchical_array($this->productcat_model->get_list_data(null, array('lang_id' => 2)), 'product_category_gid', 0);
      //Get list products
      $this->arr_view_data['vi_products'] = $this->product_model->get_list_data(null, array('lang_id' => 1));
      $this->arr_view_data['en_products'] = $this->product_model->get_list_data(null, array('lang_id' => 2));
      //Get list content category
      $this->arr_view_data['vi_contentcat'] = $this->contentcat_model->get_list_data(null, array('lang_id' => 1));
      $this->arr_view_data['en_contentcat'] = $this->contentcat_model->get_list_data(null, array('lang_id' => 2));
      //Get list content
      $this->arr_view_data['vi_contents'] = $this->content_model->get_list_data(null, array('lang_id' => 1));
      $this->arr_view_data['en_contents'] = $this->content_model->get_list_data(null, array('lang_id' => 2));
    }
}
