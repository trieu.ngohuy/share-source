<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminContentcat extends ZLAdmin_Controller {
  private $_table = 'contentcat';
  private $_key = 'content_category_gid';
  private $_max_key = 'content_category_id';
  private $_model = 'contentcat_model';
    /*
     * Constuctor function
     */
    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model($this->_model);
        //Load extras model
        //$this->load->model('productcat_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            //$this->arr_view_data = array();

            //Active menu
            $this->arr_view_data['active_menu'] = $this->_table;

            //Get main list data
            $this->arr_view_data['data'] = $this->get_main_data();

            //Get extras data
            $this->get_extras_data();

            //Set layout
            $this->layout->load('admin', $this->_table . '/backend/manager', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    //Update/New data
    function execute_query() {
        //Get post data
        $key = $_POST[$this->_key];
        $data = array();
        $model = $this->_model;
        //Get common data
        $data = array();

        //vi
        $vi_data = array();
        $lang = 'vi_';
        $vi_data['title'] =$_POST[$lang . 'title'];
        $vi_data['alias'] =$_POST[$lang . 'alias'];
        $vi_data = array_merge($vi_data, $data);
        //en
        $en_data = array();
        $lang = 'en_';
        $en_data['title'] =$_POST[$lang . 'title'];
        $en_data['alias'] =$_POST[$lang . 'alias'];
        $en_data = array_merge($en_data, $data);

        //Execute query
        if ($key == 0) {

          //Get max gid
          $max = $this->get_max_value($this->$model->get_list_data(), $this->_max_key) + 1;

          //Add lang_id
          $vi_data['lang_id'] = 1;
          $en_data['lang_id'] = 2;
          //Add created_date
          $vi_data['created_date'] = strtotime(date(DATE_FORMAT));
          $en_data['created_date'] = strtotime(date(DATE_FORMAT));
          //Add gid
          $vi_data[$this->_key] = $max;
          $en_data[$this->_key] = $max;
          //echo $key;
          // print_r($vi_data);
          // print_r($en_data);
          //New
          $this->$model->insert_new_data($vi_data);
          $this->$model->insert_new_data($en_data);

        } else {
          // echo $key;
          // print_r($vi_data);
          // print_r($en_data);
          //Update
          $this->$model->update_existing_data(array(
              $this->_key => $key,
              'lang_id' => 1
            ), $vi_data);
            $this->$model->update_existing_data(array(
                $this->_key => $key,
                'lang_id' => 2
              ), $en_data);
        }
    }

    //Get data
    function get_data() {
        //Get list data
        echo json_encode($this->get_main_data());
    }
    //Delete
    function delete() {
        //Execute query
        $model = $this->_model;
        $this->$model->delete_data(array(
            $this->_key => $_POST[$this->_key]
        ));
    }
    /*
    * Get main list data
    */
    function get_main_data(){
      $model = $this->_model;
      $arr_data = $this->$model->get_list_data(null, array(
        'lang_id' => 1
      ));
      return $this->format_multilanguage_array($arr_data, $this->_model, $this->_key);
    }
    /*
    * Get extras data
    */
    private function get_extras_data(){
      //colors
      // $model = $this->_model;
      // $this->arr_view_data['parent'] = $this->$model->get_list_data(null, array('lang_id' => 1));
      // //material
      // $this->arr_view_data['material'] = $this->properties_model->get_list_data(null, array('lang_id' => 1, 'type' => 1));
      // //size
      // $this->arr_view_data['size'] = $this->properties_model->get_list_data(null, array('lang_id' => 1, 'type' => 2));
      // //product category
      // $this->arr_view_data['productcat'] = $this->productcat_model->get_list_data(null, array('lang_id' => 1));
    }
}
