<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends ZLFront_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        // if ($this->session->userdata(SS_LOGIN) == null) {
        //     redirect(URL_LOGIN, 'refresh');
        // }

        //Load model
        $this->load->model('cart_model');
        $this->load->model('cartdetail_model');
        $this->load->model('places_model');
        $this->load->model('voucher_model');
    }

    /*
    * Index function
    */
    function index(){
        try{
            //Get translate
            $this->get_translate();

            //Get delivery address
            if ($this->session->userdata(SS_LOGIN) !== null) {
                $this->arr_view_data['delivery_address'] = $this->get_delivery_address();
            }else{
              $this->arr_view_data['delivery_address'] = null;
              $this->arr_view_data['places'] = $this->places_model->get_list_data();
            }

            //Submit voucher
            $this->submit_voucher();
            //Submit
            $this->submit_cart();

            //Update quantity
            if ($this->input->post('quantity')) {
              $arr_post_data = $this->input->post('quantity');
              $cart = $this->session->userdata(SS_CART_ORDER);
              $cart['products'][$arr_post_data['key']]['count'] = $arr_post_data['val'];
              $this->session->set_userdata(SS_CART_ORDER, $cart);
              $this->arr_view_data['cart'] = $this->GetCart();
            }
            $this->layout->load('front', 'cart/frontend/index', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
    * Get translate
    */
    function get_translate(){
      $arr_translate = $this->get_default_translate();

      $arr_translate['my_bag'] = $this->lang->line('my_bag');
      $arr_translate['product'] = $this->lang->line('product');
      $arr_translate['quantity'] = $this->lang->line('quantity');
      $arr_translate['price'] = $this->lang->line('price');
      $arr_translate['order_summary'] = $this->lang->line('order_summary');
      $arr_translate['delivery_address'] = $this->lang->line('delivery_address');
      $arr_translate['payment_method'] = $this->lang->line('payment_method');
      $arr_translate['please_select'] = $this->lang->line('please_select');
      $arr_translate['pay_on_receive'] = $this->lang->line('pay_on_receive');
      $arr_translate['complete_order'] = $this->lang->line('complete_order');
      $arr_translate['add_voucher_code'] = $this->lang->line('add_voucher_code');
      $arr_translate['enter_code'] = $this->lang->line('enter_code');
      $arr_translate['add'] = $this->lang->line('add');
      $arr_translate['voucher_applied'] = $this->lang->line('voucher_applied');
      $arr_translate['max_sale'] = $this->lang->line('max_sale');
      $arr_translate['min_order'] = $this->lang->line('min_order');
      $arr_translate['expire_date'] = $this->lang->line('expire_date');
      $arr_translate['rules'] = $this->lang->line('rules');
      $arr_translate['material'] = $this->lang->line('material');
      $arr_translate['color'] = $this->lang->line('color');
      $arr_translate['size'] = $this->lang->line('size');
      $arr_translate['no_delivery1'] = $this->lang->line('no_delivery1');
      $arr_translate['no_delivery2'] = $this->lang->line('no_delivery2');
      $arr_translate['transfer_fee'] = $this->lang->line('transfer_fee');
      $arr_translate['payment_success'] = $this->lang->line('payment_success');
      $arr_translate['payment_des'] = $this->lang->line('payment_des');
      $arr_translate['continue_shopping'] = $this->lang->line('continue_shopping');

      $this->arr_view_data['translate'] = $arr_translate;
    }
    /*
    * Submit cart
    */
    function submit_cart(){
      if ($this->input->post('payment')) {
          $arr_post_data = $this->input->post('payment');
          $token = create_token();
          $cart_info = array(
            'delivery_address' => $arr_post_data['address'],
            'full_name' => $arr_post_data['full_name'],
            'phone' => $arr_post_data['phone'],
            'payment_method' => $arr_post_data['method'],
            'payment_info' => $arr_post_data['info'],
            'token' => $token,
            'created_date' => strtotime(date(DATE_FORMAT)),
            'code' => strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10))
          );

          if($arr_post_data['method'] == 1){
            $cart_info['status'] = 0;
          }
          if($arr_post_data['method'] == 2){
            $cart_info['status'] = 1;
          }

          if($this->arr_view_data['login_data'] !== null){
            $cart_info['user_id'] = $this->arr_view_data['login_data']['user_id'];
          }

          if(isset($this->arr_view_data['cart']['voucher'])){
            $cart_info['voucher_gid'] = $this->arr_view_data['cart']['voucher']['voucher_gid'];
          }
          //Save cart
          $cart_id = $this->cart_model->insert_new_data($cart_info);
          //Save cart detail
          foreach($this->arr_view_data['cart']['products'] as $value){
            $this->cartdetail_model->insert_new_data(array(
              'product_gid' => $value['product_gid'],
              'count' => $value['count'],
              'cart_id' => $cart_id,
              'created_date' => strtotime(date(DATE_FORMAT))
            ));
          }
          //Redirect to success page
           $this->session->set_userdata(SS_CART_ORDER, null);
          redirect(URL_CART_SUCCESS . '/' . $token, 'refresh');
      }
    }
    /*
    * Submit voucher
    */
    function submit_voucher(){
      if ($this->input->post('voucher')) {
        $arr_post_data = $this->input->post('voucher');
        //Check voucher
        $voucher = $this->voucher_model->get_single_data(array('code' => $arr_post_data['code']));
        if($voucher != false){
          $cart = $this->session->userdata(SS_CART_ORDER);
          $cart['voucher_gid'] = $voucher['voucher_gid'];
          $this->session->set_userdata(SS_CART_ORDER, $cart);
          $this->arr_view_data['cart'] = $this->GetCart();
        }else{
          $this->arr_view_data['voucher_submit'] = array(
              'status' => 0,
              'message' => $this->lang->line('msg_invalid_voucher')
          );
        }
      }
    }
    /*
    * Remove product
    */
    function RemoveProduct(){
      try{
        $cart = $this->session->userdata(SS_CART_ORDER);
        if($cart['products'] && count($cart['products']) <= 1){
          $cart['products'] = array();
        }else{
          $count = 0;
          $index = $this->input->post("index",false);
          foreach($cart['products'] as $key => &$value){
            if($count == $index){
              unset($cart['products'][$key]);
            }
            $count++;
          }
          unset($value);
        }
        $this->session->set_userdata(SS_CART_ORDER, $cart);
        redirect(URL_CART, 'refresh');
      } catch (Exception $exc) {
          custom_exception($this, $exc);
      }
    }
    /*
    * Remove product
    */
    function UpdateQuantity(){
      try{

        //Get params
        $index = $this->input->post("index",false);
        $quantity = $this->input->post("quantity",false);

        //Get cart
        $cart = $this->session->userdata(SS_CART_ORDER);
        //Update quantity
        $cart['products'][$this->input->post("index",false)]['count'] = $quantity;
        $this->session->set_userdata(SS_CART_ORDER, $cart);
        //Get update cart
        $cart = $this->GetCart();
        $tmp = $cart['products'][$this->input->post("index",false)];
        $data = array(
          'price_total' => $tmp['price_total'],
          'total_original' => $cart['total_original'],
          'total' => $cart['total'],
          'total_usd' => $cart['total_usd'],
          'products' => $cart['products']
        );
        echo json_encode($data, JSON_UNESCAPED_UNICODE );
      } catch (Exception $exc) {
          custom_exception($this, $exc);
      }
    }
    /*
    * Payment success page
    */
    function success($token){
        try{
            //Get order token
            if($token == null){
                redirect(URL_404, 'refresh');
            }
            //Get translate
            $this->get_translate();
            //Get cart detail
            $cart_detail = $this->cart_model->get_single_data(array(
              'token' => $token
            ));
            //Get order token
            if($cart_detail == false){
                redirect(URL_404, 'refresh');
            }
            //Get login user info
            //Submit
            //Update cart status
            //Send order mail to user and admin
            //Remove token
            $this->layout->load('front', 'cart/frontend/success', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /**
     * Index Page for this controller.
     */
    public function detail($alias) {
        try {

            //Post comment
            if ($this->input->post('comment')) {
                //Get post data
                $arr_post_data = $this->input->post('comment');
                //Insert comment
                $this->comment_model->insert_new_data(
                    array(
                        'parent_id' => $arr_post_data['parent_id'],
                        'content_id' => $arr_post_data['content_id'],
                        'content_episodes_id' => $arr_post_data['content_episodes_id'],
                        'full_text' => $arr_post_data['full_text'],
                        'user_id' => $this->arr_view_data['login_data']['user_id'],
                        'created_date' => strtotime(date(DATE_FORMAT)),
                    )
                );
            }

            //Get detail
            $obj_data = $this->episodes_model->get_single_data(array(
                'alias' =>$alias
            ));
            $obj_data['full_text'] = preg_replace("/[\\n\\r]+/", "", htmlspecialchars_decode($obj_data['full_text']));
            $obj_data['footer_note'] = preg_replace("/[\\n\\r]+/", "", htmlspecialchars_decode($obj_data['footer_note']));

            //Get season detail
            $obj_season = $this->season_model->get_single_data(array(
                'content_season_id' =>$obj_data['content_season_id']
            ));
            $this->arr_view_data['season'] = $obj_season;

            //Get previous and next chapter
            $arr_episodes = $this->episodes_model->get_list_data(null, array(
                'content_season_id' =>$obj_season['content_season_id']
            ));
            $active_index = 0;
            $previous_url = '';
            $next_url = '';
            foreach ($arr_episodes as $key => $value) {
                if($value['alias'] === $alias){
                    $active_index = $key;
                }
            }
            if(($active_index - 1) >= 0){
                $previous_url = $arr_episodes[$active_index - 1]['url'];
            }
            if(($active_index + 1) <= (count($arr_episodes) - 1)){
                $next_url = $arr_episodes[$active_index + 1]['url'];
            }
            $this->arr_view_data['previous_url'] = $previous_url;
            $this->arr_view_data['next_url'] = $next_url;

            //Get content detail
            $obj_content = $this->content_model->get_single_data(array(
                'content_id' =>$obj_season['content_id']
            ));
            $this->arr_view_data['content'] = $obj_content;
            //Update view
            $this->content_model->update_existing_data(
                array('content_id' => $obj_content['content_id']),
                array('view' => $obj_content['view'] + 1)
            );

            //Get comment
            $arr_comment = $this->comment_model->get_list_data(null,
                array(
                    'content_episodes_id' => $obj_data['content_episodes_id']
                )
            );
            $arr_tmp_comment = array();
            foreach ($arr_comment as $key => $value) {

                if($value['parent_id'] !== "0"){
                    continue;
                }
                //get user full name
                $tmp = $this->user_model->get_single_data(array(
                    'user_id' => $value['user_id']
                ));
                $value['full_name'] = $tmp['full_name'];
                //Get detail
                $arr_detail = array();
                foreach ($arr_comment as $key => &$value2) {
                    if($value['comment_id'] === $value2['parent_id']){
                        //get user full name
                        $tmp = $this->user_model->get_single_data(array(
                            'user_id' => $value2['user_id']
                        ));
                        $value2['full_name'] = $tmp['full_name'];
                        $arr_detail[] = $value2;
                    }
                }
                $value['detail'] = $arr_detail;
                $arr_tmp_comment[] = $value;
            }
            //$arr_comment = recursive_data($arr_comment, 0, array());
            $this->arr_view_data['comment'] = $arr_tmp_comment;

            //Set post data back to view
            $this->arr_view_data['data'] = $obj_data;

            //Website title
            $this->arr_view_data['website_title'] = $obj_data['title'];

            //Set layout
            $this->layout->load('episodes', 'episodes/frontend/detail', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
}
