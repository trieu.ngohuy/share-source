<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends ZLFront_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('content_model');
        $this->load->model('contentcat_model');
    }

    /*
     * Content detail
     */
    function detail($alias){
        try{
            //Get translate
            $this->get_translate();
            //Get content detail
            $obj_data = $this->content_model->get_single_data(array(
                'alias' => $alias
            ));

            //Check valid data
            if($obj_data === false){
                $this->valid_link($obj_data, 'content_model', $alias, 'content_gid');
            }
  
            //Check valid data
            if($obj_data === false){
              redirect(URL_HOME, 'refresh');
            }

            $obj_data['created_date'] = date("F d, Y", $obj_data['created_date_number']);

            //Get list contents
            $list_contents = $this->content_model->get_list_data();
            //Get current article index
            $index = -1;
            foreach($list_contents as $key => $value){
                if($value['content_gid'] == $obj_data['content_gid']){
                    $index = $key;
                }
            }
            //Get next link
            $next_link = "#";
            if($index < count($list_contents) - 1){
                $next_link = $list_contents[$index + 1]['url'];
            }
            //Get previous link
            $previous_link = "#";
            if($index > 0){
                $previous_link = $list_contents[$index - 1]['url'];
            }
            //Set post data back to view
            $this->arr_view_data['data'] = $obj_data;
            $this->arr_view_data['next_link'] = $next_link;
            $this->arr_view_data['previous_link'] = $previous_link;

            //Website title
            $this->arr_view_data['website_title'] = $obj_data['title'];

            //Set layout
            $this->layout->load('front', 'content/frontend/detail', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
    * Get translate
    */
    function get_translate(){
      $arr_translate = $this->get_default_translate();

      //blog
      $arr_translate['bg_empty'] = $this->lang->line('bg_empty');
      $arr_translate['bg_title'] = $this->lang->line('bg_title');
      //Blog detail
      $arr_translate['posted_on'] = $this->lang->line('posted_on');
      $arr_translate['prev_article'] = $this->lang->line('prev_article');
      $arr_translate['nex_article'] = $this->lang->line('nex_article');
      $arr_translate['share'] = $this->lang->line('share');

      $this->arr_view_data['translate'] = $arr_translate;
    }
    /*
     * Index Page for this controller.
     */
    public function index($alias = null) {
        try {
            //Get translate
            $this->get_translate();
            //Get list category
            $list_category = $this->contentcat_model->get_list_data();
            foreach($list_category as &$value){
                $value['title'] = strtoupper($value['title']);
            }
            array_push($list_category, array(
              'title' => $this->lang->line('bg_all'),
              'url' => base_url() . URL_CONTENTCAT
            ));
            unset($value);
            $obj_category  =array();
            //Get list content
            if($alias != null){
                $obj_category = $this->contentcat_model->get_single_data(array(
                    'alias' =>$alias
                ));
    
                //Check valid data
                if($obj_category === false){
                    $this->valid_link($obj_category, 'contentcat_model', $alias, 'content_category_gid');
                }

                $list_contents = $this->content_model->get_list_data(null, array(
                    'content_category_gid' =>$obj_category['content_category_gid'],
                ));
            }else{
                $obj_category = array(
                    'title' => 'The Oliver Bonas Blog'
                );
                $list_contents = $this->content_model->get_list_data(null, array(
                  'content_category_gid !=' => -1
                ));
            }

            //Set post data back to view
            $this->arr_view_data['data'] = $list_contents;
            $this->arr_view_data['category'] = $list_category;

            //Website title
            $this->arr_view_data['website_title'] = $obj_category['title'];

            //Set layout
            $this->layout->load('front', 'content/frontend/index', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
}
