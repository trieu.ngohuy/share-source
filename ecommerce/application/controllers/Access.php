<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends ZLFront_Controller {

    private $_view_folder = "access/";

    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();
        //Load encrypt model
        $this->load->helper('encrypt');

        //Load model
        $this->load->model('user_model');
        $this->load->model('cart_model');
        $this->load->model('cartdetail_model');
    }
    function load_global_data(&$arr_view_data){
        //Get login user
        $arr_login_user = $this->session->userdata(SS_LOGIN);
        $arr_view_data['login_data'] = $arr_login_user;
         //Load model
        $this->load->model('setting_model');
        $config_data = $this->setting_model->get_list_data();
        $arr_view_data['setting'] = $config_data;
        //Load state for header
        $this->load->model('state_model');
        $state = $this->state_model->get_list_data();
        $arr_view_data['state'] = $state;

        //Get list content for ranking
        $this->load->model('content_model');
        $content = $this->content_model->get_list_data();
        sort_array($content, 'view');
        $arr_view_data['global_content'] = $content;
    }
    /*
     * Admin logout
     */

    public function AdminLogout() {
        try {
            //Remove login data
            $this->session->set_userdata(SS_ADMIN_LOGIN, null);
            //Redirect to login page
            redirect(URL_ADMIN_LOGIN, 'refresh');
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    /*
     * client logout
     */

    public function ClientLogout() {
        try {
            //Remove login data
            $this->session->set_userdata(SS_LOGIN, null);
            //Redirect to login page
            redirect(URL_HOME, 'refresh');
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
     * Client login
     */

    public function ClientLogin() {
        try {
            //Get translate
            $this->get_translate();
            $arr_view_data = array();
            //$this->session->set_userdata(SS_LOGIN, null);
            //Redirect to homepage if already login
            if ($this->session->userdata(SS_LOGIN)) {
                redirect(URL_HOME, 'refresh');
            }
            //Form submitted
            if ($this->input->post('data')) {
                $this->Login(SS_LOGIN, URL_HOME, 0);
            }

            //$this->load_global_data($arr_view_data);
            $this->layout->load('front', 'access/frontend/login', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    /*
     * Admin Login
     */

    public function AdminLogin() {
        try {
            $arr_view_data = array();

            //Redirect to homepage if already login
            if ($this->session->userdata(SS_ADMIN_LOGIN)) {
                redirect(URL_ADMIN_SETTING, 'refresh');
            }
            //Form submitted
            if ($this->input->post('data')) {
                $arr_view_data = $this->Login2($arr_view_data, SS_ADMIN_LOGIN, URL_ADMIN_SETTING, 1);
            }

            $this->layout->load('admin_access', 'access/backend/login', $arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
    * Admin login
    */
    function Login2($arr_view_data, $session, $redirect_url, $user_type){

        //Get post data
        $arr_post_data = $this->input->post('data');

        //Get user from username
        $arr_data = $this->user_model->get_single_data(array(
            'username' => $arr_post_data['username'],
            'type' => $user_type
        ));

        //Username exist
        if ($arr_data != false) {
            //Check if password is correct
            if (password_check(password_generator($arr_post_data['password']), $arr_data['password'])) {
                //Save session login user inf
                $this->session->set_userdata($session, $arr_data);
                //Redirect into setting page
                redirect($redirect_url, 'refresh');
            } else {
                $arr_view_data['result'] = array(
                    'status' => 0,
                    'message' => 'Mật khẩu không đúng.'
                );
            }
        }else{
            $arr_view_data['result'] = array(
                'status' => 0,
                'message' => 'Địa chỉ email không đúng.'
            );
        }
        return $arr_view_data;
    }
    /*
     * Login function
     * Return:
     * 0 - error
     * 1 - succeed
     * 2 - continue
     */

    private function Login($session, $redirect_url, $user_type) {
        //Get post data
        $arr_post_data = $this->input->post('data');

        //Reset password
        if(isset($arr_post_data['reset_password']) && $arr_post_data['reset_password'] == 1){
          //Create token
          $token = create_token();
          //Update token into user table
          $this->user_model->update_existing_data(array(
            'email' => $arr_post_data['email']
          ), array(
            'reset_token' => $token
          ));
          //Send mail
          send_mail($this, array(
            'arr_emails' => $arr_post_data['email'],
            'subject' => $this->lang->line('msg_email_reset_password_title'),
            'content' => $this->MailContent($token)
          ));
          //Return message
          $this->arr_view_data['login_message'] = array(
              'status' => 1,
              'message' => $this->lang->line('msg_email_reset_password_title')
          );
        }else {
          //Get user from username
          $arr_data = $this->user_model->get_single_data(array(
              'email' => $arr_post_data['email'],
              'type' => $user_type
          ));

          //Email exist
          if ($arr_data != false) {
              //Check if password is correct
              if (password_check(password_generator($arr_post_data['password']), $arr_data['password'])) {
                  //Save session login user inf
                  $this->session->set_userdata($session, $arr_data);
                  //Redirect into setting page
                  redirect($redirect_url, 'refresh');
              } else {
                  $this->arr_view_data['login_message'] = array(
                      'status' => 0,
                      'message' => 'Invalid login or password.'
                  );
              }

          } else {
              $this->ClientRegister($arr_post_data);
          }
        }
    }

    /*
    * Reset mail content
    */
    function MailContent($token){
        $html = $this->lang->line('msg_email_reset_password_content')
        .base_url() . URL_RESET_PASSWORD . $token.
        $this->lang->line('msg_email_reset_password_content2');
        return $html;
    }
    /*
     * Client register
     */

    public function ClientRegister($arr_post_data) {
        try {
            $arr_data = $this->user_model->get_list_data();
            $max_sorting = 1;
            foreach($arr_data as $value){
                if($value['sorting'] > $max_sorting){
                    $max_sorting = $value['sorting'];
                }
            }
            //Get list users
            $this->user_model->insert_new_data(array(
                'firstname' => $arr_post_data['firstname'],
                'lastname' => $arr_post_data['lastname'],
                'email' => $arr_post_data['email'],
                'password' => password_encrypt(password_generator($arr_post_data['password'])),
                'type' => 0,
                'enabled' => 1,
                'sorting' => $max_sorting + 1,
                'created_date' => strtotime(date(DATE_FORMAT)),
            ));
            // Redirect to profile page
            redirect(URL_HOME, 'refresh');
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    /*
    * Client forget password
    */
    public function clientForgetPassword(){
        try {

        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
    * Edit
    */
    function AccountEdit(){
      try {
          $valid = true;
          //Get translate
          $this->get_translate();
          //active
          $this->arr_view_data['active'] = 'edit';

          //Redirect
          if ($this->session->userdata(SS_LOGIN) == null) {
              redirect(URL_LOGIN, 'refresh');
          }
          //Update
          if ($this->input->post('account')) {
            //Get post data
            $arr_post_data = $this->input->post('account');
            $arr_data = array(
              'prefix' => $arr_post_data['prefix'],
              'firstname' => $arr_post_data['firstname'],
              'lastname' => $arr_post_data['lastname'],
              'email' => $arr_post_data['email'],
            );
            //Pasword
            if($arr_post_data['password'] != '' || $arr_post_data['repassword'] != ''){
              if($arr_post_data['password'] != $arr_post_data['repassword']){
                $this->arr_view_data['account_edit_message'] = array(
                    'status' => 0,
                    'message' => $this->lang->line('msg_confirm_password_not_equal')
                );
                $valid = false;
              }else{
                $arr_data['password'] = password_generator($arr_post_data['password']);
              }
            }
            //save data
            if($valid){
              $this->user_model->update_existing_data(array(
                'user_id' => $this->arr_view_data['login_data']['user_id']
              ), $arr_data);
              //Update
              $objUser = $this->user_model->get_single_data(array(
                'user_id' => $this->arr_view_data['login_data']['user_id']
              ));
              $this->session->set_userdata(SS_LOGIN, $objUser);
              $this->arr_view_data['login_data'] = $this->session->userdata(SS_LOGIN);
              //Send message
              $this->arr_view_data['account_edit_message'] = array(
                  'status' => 1,
                  'message' => $this->lang->line('msg_success_update')
              );
            }
          }

          $this->layout->load('front', 'access/frontend/account-edit', $this->arr_view_data);
      } catch (Exception $exc) {
          custom_exception($this, $exc);
      }
    }
    /*
     * My account
     */

    public function MyAccount() {
        try {
            //Get translate
            $this->get_translate();
            //active
            $this->arr_view_data['active'] = 'account';

            //Redirect
            if ($this->session->userdata(SS_LOGIN) == null) {
                redirect(URL_LOGIN, 'refresh');
            }

            //Get default delivery address
            $tmp = $this->get_delivery_address();
            $this->arr_view_data['delivery_address'] = count($tmp) > 0 ? $tmp[0] : $tmp;

            //Get orders
            $arr_orders = $this->cart_model->get_list_data(null, array(
              'user_id' => $this->arr_view_data['login_data']['user_id']
            ));
            if(count($arr_orders) > 0){
              $arr_orders = $arr_orders[count($arr_orders) - 1];
              //Status
              if($arr_orders['status'] == 0){
                $arr_orders['status_text'] = 'Progressing';
              }elseif($arr_orders['status'] == 1){
                $arr_orders['status_text'] = 'Payed';
              }
              //Cart detail
              $arr_orders['products'] = $this->cartdetail_model->get_list_data(null, array(
                'cart_id' => $arr_orders['cart_id']
              ));
              //Progressing
              $arr_orders = $this->progress_cart_data($arr_orders, $arr_orders);
            }
            $this->arr_view_data['orders'] = $arr_orders;

            $this->layout->load('front', 'access/frontend/my-account', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
    * My orders
    */
    public function MyOrders(){
        try{
            //Get translate
            $this->get_translate();
            //active
            $this->arr_view_data['active'] = 'orders';

            //Redirect to homepage if already login
            if ($this->session->userdata(SS_LOGIN) == null) {
                redirect(URL_LOGIN, 'refresh');
            }

            //Get orders
            $arr_orders = $this->cart_model->get_list_data(null, array(
              'user_id' => $this->arr_view_data['login_data']['user_id']
            ));
            if(count($arr_orders) > 0){
              foreach($arr_orders as &$value){
                //Status
                if($value['status'] == 0){
                  $value['status_text'] = 'Progressing';
                }elseif($value['status'] == 1){
                  $value['status_text'] = 'Payed';
                }
                //Cart detail
                $value['products'] = $this->cartdetail_model->get_list_data(null, array(
                  'cart_id' => $value['cart_id']
                ));
                //Progressing
                $value = $this->progress_cart_data($value, $value);
              }
              unset($value);
            }
            $this->arr_view_data['orders'] = $arr_orders;

            $this->layout->load('front', 'access/frontend/my-orders', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
    * Address book
    */
    function AddressBook($alias = null){
        try{
            //Get translate
            $this->get_translate();
            //active
            $this->arr_view_data['active'] = 'address';

            //Redirect to homepage if already login
            if ($this->session->userdata(SS_LOGIN) == null) {
                redirect(URL_LOGIN, 'refresh');
            }

            //Get list delivery address
            $delivery_address = $this->get_delivery_address();
            $this->arr_view_data['delivery_address'] = $delivery_address;

            //Submit data
            if ($this->input->post('address')) {
              $arr_post_data = $this->input->post('address');

              //Get update list
              $tmp = array();
              foreach($delivery_address as $value){
                array_push($tmp, array(
                    'address' => $value['add'],
                    'country_gid' => $value['country_gid'],
                    'province_gid' => $value['province_gid'],
                    'district_gid' => $value['district_gid'],
                    'ward_gid' => $value['ward_gid']
                ));
              }
              //Update
              if($alias != 'new'){
                $tmp[$alias]['address'] = $arr_post_data['address'];
                $tmp[$alias]['country_gid'] = $arr_post_data['country_gid'];
                $tmp[$alias]['province_gid'] = $arr_post_data['province_gid'];
                $tmp[$alias]['district_gid'] = $arr_post_data['district_gid'];
                $tmp[$alias]['ward_gid'] = $arr_post_data['ward_gid'];
              }
              //New
              else{
                array_push($tmp, array(
                    'address' => $arr_post_data['address'],
                    'country_gid' => $arr_post_data['country_gid'],
                    'province_gid' => $arr_post_data['province_gid'],
                    'district_gid' => $arr_post_data['district_gid'],
                    'ward_gid' => $arr_post_data['ward_gid']
                ));
              }
                //Update
                $this->user_model->update_existing_data(array(
                    'user_id' => $this->arr_view_data['login_data']['user_id']
                ),array(
                    'address' => json_encode($tmp, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)
                ));

                //Update login data
                $tmp = $this->user_model->get_single_data(array(
                    'user_id' => $this->arr_view_data['login_data']['user_id']
                ));
                $this->session->set_userdata(SS_LOGIN, $tmp);
                $this->get_login_data();

                //Go to manager book
                redirect(URL_ADDRESS_BOOK, 'refresh');
            }

            //Manager
            if($alias == null){
                $this->layout->load('front', 'access/frontend/address-book', $this->arr_view_data);
            }else{
                //Get list places
                $tmp = $this->places_model->get_list_data();
                $this->arr_view_data['places'] = $tmp;

                $data = array();

                if($alias != 'new'){
                    foreach($delivery_address as $key => $value){
                        if($alias == $key){
                            $data = $value;
                            break;
                        }
                    }
                }
                $this->arr_view_data['data'] = $data;
                $this->layout->load('front', 'access/frontend/address-edit', $this->arr_view_data);
            }

        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
    * Check valid information
    */
    function CheckValid(){
        try{
            $data = $this->user_model->get_single_data(array(
                'email' => $this->input->post("email",false),
                'type' => 0
            ));
            if(count($data) > 0){
                echo json_encode($data);
            }else{
                echo false;
            }
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    //Delete
    function Delete() {
        //Get post data
        $index = $_POST['index'];

        //Get list delivery address
        $delivery_address = $this->get_delivery_address();
        $this->arr_view_data['delivery_address'] = $delivery_address;

        //Get update list
        $tmp = array();
        foreach($delivery_address as $key => $value){
            if($key != $index){
                array_push($tmp, array(
                    'address' => $value['add'],
                    'country_gid' => $value['country_gid'],
                    'province_gid' => $value['province_gid'],
                    'district_gid' => $value['district_gid'],
                    'ward_gid' => $value['ward_gid']
                ));
            }
        }
        //Update
        $this->user_model->update_existing_data(array(
            'user_id' => $this->arr_view_data['login_data']['user_id']
        ),array(
            'address' => json_encode($tmp, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)
        ));

        //Update login data
        $tmp = $this->user_model->get_single_data(array(
            'user_id' => $this->arr_view_data['login_data']['user_id']
        ));
        $this->session->set_userdata(SS_LOGIN, $tmp);
        $this->get_login_data();
    }
    /*
    * Get translate
    */
    function get_translate(){
      $arr_translate = $this->get_default_translate();
      //login
      $arr_translate['lg_title'] = $this->lang->line('lg_title');
      $arr_translate['email'] = $this->lang->line('email');
      $arr_translate['continue'] = $this->lang->line('continue');
      $arr_translate['err_invalid_email'] = $this->lang->line('err_invalid_email');
      $arr_translate['welcome_back'] = $this->lang->line('welcome_back');
      $arr_translate['please_type_password'] = $this->lang->line('please_type_password');
      $arr_translate['reset_password'] = $this->lang->line('reset_password');
      $arr_translate['firstname'] = $this->lang->line('firstname');
      $arr_translate['lastname'] = $this->lang->line('lastname');
      $arr_translate['err_require'] = $this->lang->line('err_require');
      $arr_translate['password'] = $this->lang->line('password');
      $arr_translate['subs_agreement'] = $this->lang->line('subs_agreement');
      $arr_translate['err_policy'] = $this->lang->line('err_policy');
      //My account
      $arr_translate['my_account_2'] = $this->lang->line('my_account_2');
      $arr_translate['logout'] = $this->lang->line('logout');
      $arr_translate['account_detail'] = $this->lang->line('account_detail');
      $arr_translate['edit'] = $this->lang->line('edit');
      $arr_translate['delete'] = $this->lang->line('delete');
      $arr_translate['change_password'] = $this->lang->line('change_password');
      $arr_translate['address_book'] = $this->lang->line('address_book');
      $arr_translate['manager_address_book'] = $this->lang->line('manager_address_book');
      $arr_translate['default_address_book'] = $this->lang->line('default_address_book');
      $arr_translate['my_order'] = $this->lang->line('my_order');
      $arr_translate['manager_my_order'] = $this->lang->line('manager_my_order');
      $arr_translate['latest_orders'] = $this->lang->line('latest_orders');
      $arr_translate['order_date'] = $this->lang->line('order_date');
      $arr_translate['status'] = $this->lang->line('status');
      $arr_translate['payed'] = $this->lang->line('payed');
      $arr_translate['no_default_address'] = $this->lang->line('no_default_address');
      $arr_translate['no_order'] = $this->lang->line('no_order');
      $arr_translate['continue_shopping'] = $this->lang->line('continue_shopping');
      $arr_translate['product'] = $this->lang->line('product');
      $arr_translate['quantity'] = $this->lang->line('quantity');
      $arr_translate['price'] = $this->lang->line('price');
      $arr_translate['order_summary'] = $this->lang->line('order_summary');
      $arr_translate['material'] = $this->lang->line('material');
      $arr_translate['color'] = $this->lang->line('color');
      $arr_translate['size'] = $this->lang->line('size');
      $arr_translate['hello'] = $this->lang->line('hello');
      //Account edit
      $arr_translate['title'] = $this->lang->line('title');
      $arr_translate['enter_firstname'] = $this->lang->line('enter_firstname');
      $arr_translate['enter_lastname'] = $this->lang->line('enter_lastname');
      $arr_translate['new_password'] = $this->lang->line('new_password');
      $arr_translate['confirm_new_password'] = $this->lang->line('confirm_new_password');
      $arr_translate['please_select'] = $this->lang->line('please_select');
      $arr_translate['email'] = $this->lang->line('email');
      $arr_translate['save'] = $this->lang->line('save');
      //Address book
      $arr_translate['address_edit'] = $this->lang->line('address_edit');
      $arr_translate['additional_address'] = $this->lang->line('additional_address');
      $arr_translate['add_new_address'] = $this->lang->line('add_new_address');
      $arr_translate['country'] = $this->lang->line('country');
      $arr_translate['province'] = $this->lang->line('province');
      $arr_translate['district'] = $this->lang->line('district');
      $arr_translate['ward'] = $this->lang->line('ward');
      $arr_translate['address'] = $this->lang->line('address');
      $arr_translate['enter_address'] = $this->lang->line('enter_address');
      $arr_translate['save_address'] = $this->lang->line('save_address');
      $arr_translate['no_address'] = $this->lang->line('no_address');
      //My order
      $arr_translate['order_id'] = $this->lang->line('order_id');
      $arr_translate['voucher_applied'] = $this->lang->line('voucher_applied');
      $arr_translate['max_sale'] = $this->lang->line('max_sale');
      $arr_translate['min_order'] = $this->lang->line('min_order');
      $arr_translate['expire_date'] = $this->lang->line('expire_date');
      $arr_translate['rules'] = $this->lang->line('rules');
      $this->arr_view_data['translate'] = $arr_translate;
    }
}
