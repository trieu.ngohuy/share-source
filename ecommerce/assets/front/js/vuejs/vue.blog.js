var vue_blog = new Vue({
  el: '#content-index-wrap',
  data: function(){
    return {
      active_item: 0
    }
  },
  methods: {
    show_item: function(val){
      this.active_item = val;
    }
  }
});
