var vue_login = new Vue({
  el: '#frmLogin',
  data: function(){
    return {
      email: '',
      firstname: '',
      lastname: '',
      password: '',
      policy: false,
      is_reset_password: 0,
      error_message: {
        email: '',
        password: '',
        firstname: '',
        lastname: '',
        policy: ''
      },
      mode: 0, /*0-invalid, 1-login, 2-registration */
      fullname: ''
    }
  },
  watch: {
    email: function(val){
      this.error_message.email = VueCheckEmailFormat(val);
      if(this.error_message.email != ''){
        this.error_message.email = err_email;
        this.mode = 0;
      }else{
        this.CheckValidEmail();
      }
    },
    password: function(val){
      this.password = val;
    },
    firstname: function(val){
      this.firstname = val;
    },
    lastname: function(val){
      this.lastname = val;
    },
    policy: function(val){
      this.policy = val;
    }
  },
  methods: {
    CheckValidEmail: function(){
      var formData=new FormData();
      formData.append("email",this.email);
      var data = this;

      axios.post(url_checkvalid, formData)
      .then(function (response) {
        if(response.data != ""){
          data.fullname = response.data['lastname'] + " " + response.data['firstname'];
          data.mode = 1;
        }else{
          data.mode = 2;
        }
      })
      .catch(function (error) {
          alert('Có lỗi. Thử lại sau.');
      });
    },
    FormSubmit: function(event){
      var isValid = true;
      var data = this;
      if(data.mode == 0){
        event.preventDefault();
      }
      if(data.password == ""){
        data.error_message.password = err_require;
        isValid = false;
      }else{
        data.error_message.password = "";
      }
      if(data.mode == 2){
        if(data.firstname == ""){
          data.error_message.firstname = err_require;
          isValid = false;
        }else{
          data.error_message.firstname = "";
        }
        if(data.lastname == ""){
          data.error_message.lastname = error_message;
          isValid = false;
        }else{
          data.error_message.lastname = "";
        }
        if(data.policy == false){
          data.error_message.policy = err_policy;
          isValid = false;
        }else{
          data.error_message.policy = "";
        }
      }
      if(!isValid){
        event.preventDefault();
      }
    },
    ResetPassword: function(){
      this.is_reset_password = 1;
      setTimeout(function(){
        $('#frmLogin').submit();
      }, 2000);
    }
  }
});
