var vue_email_reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/;
//Sort array of objects by properties
function sort_objects_array(arr, key, mode){
  return arr.sort(function (a, b) {
      var x = a[key]; var y = b[key];
      if($.isNumeric(x)){
        x = parseInt(x);
      }
      if($.isNumeric(y)){
        y = parseInt(y);
      }
      if(mode == 'desc'){
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
      }else{
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
      }
  });
}
/*
* Check if valid email format
*/
function VueCheckEmailFormat(val){
    if(val == "" || !vue_email_reg.test(val)){
        return "Email is invalid. Please type again";
    }else{
        return "";
    }
}
