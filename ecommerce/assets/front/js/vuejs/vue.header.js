var vue_header = new Vue({
  el: '#header',
  data : function(){
    return {
      display_cart: false,
      display_search: false,
      active_menu_item: 0,
      cart_html: '',
      cart_products_count: '',
      arr_data: {
        cart: arr_cart,
        translate: arr_translate
      },
      mobile: {
        is_display: false,
        active_item: 0,
        active_sub_item: 0
      }
    };
  },
  beforeMount() {
    this.reload_carthtml();
  },
  methods : {
    reload_carthtml: function(){
      var data = this.arr_data.cart;
      var html = '';
      //Update cart count display
      if(data['products'] !== undefined && data['products'].length > 0){
        this.cart_products_count = '(' + data['products'].length + ')';
      }else{
        this.cart_products_count = '';
      }
      
      //clean all old html
      this.cart_html = '';
      //add cart title
      html += '<div class="row">'
        +'<div class="col-md-6"><b>'+this.arr_data.translate['shopping_bag']+'</b></div>'
        +'<div class="col-md-6 al-right" id="cart_count">'
          + (data['products'] === undefined ? 0 : data['products'].length)+' '+this.arr_data.translate['items']
        +'</div>'
      +'</div>';

      //Add cart detail

      if(data['products'] !== undefined && data['products'].length > 0){
        html += '<br>'
        +'<div id="cart-list-products">';
        for(var i = 0 ; i < data['products'].length ; i++){
          var tmp = data['products'][i];

          html += '<div class="row">'
              +'<div class="col-md-3">'
                +'<img src="'+tmp['logo']+'" class="wd-full"/>'
              +'</div>'
              +'<div class="col-md-6">'
                +'<a href="'+tmp['url']+'">'+tmp['title']+'</a>'
                +'<p>'+this.arr_data.translate['cart_quantity']+': '+tmp['count']+'</p>'
              +'</div>'
              +'<div class="col-md-3 al-right"><p>'+tmp['price']+'</p></div>'
          +'</div>';

          if(i < data['products'].length - 1 ){
            html += '<hr>';
          }
        }
        html += '</div>';

        //Add cart total
        html += '<br>'
        +'<div class="row" id="cart_total">'
          +'<div class="col-md-7"><b>'+this.arr_data.translate['total']+'</b></div>'
          +'<div class="col-md-5 al-right"><b>'+data['total']+'</b></div>'
        +'</div>'
        +'<hr>';
        //Add cart checkout button
        html += '<a class="wd-full btn btn-success anc-btn-success" href="'+url_basket+'">'+this.arr_data.translate['checkout']+'</a>';

      }else{
        html += '<hr>'
        +'<p class="al-center" id="cart_empty">'+this.arr_data.translate['empty_des']+'</p>';
      }

      //Update html
      this.cart_html = html;
    },
    basket: function(){
      location.href = url_basket;
    },
    show_cart : function(){

      window.addEventListener('resize', this.handleResize)
      if(window.innerWidth <= 576){
          return;
      }

      if(this.display_cart){
        this.display_cart = false;
      }else{
        this.display_cart = true;
      }
    },
    show_search : function(){
      if(this.display_search){
        this.display_search = false;
      }else{
        this.display_search = true;
      }
    },
    show_sub_menu: function(val){
      this.active_menu_item = val;
    },
    show_mobile_menu: function(){
      if(this.mobile.is_display){
        this.mobile.is_display = false;
      }else{
        this.mobile.is_display = true;
      }
    },
    show_mobile_sub_menu: function(val){
      this.mobile.active_item = val;
    },
    show_mobile_sub2_menu: function(val){
      this.mobile.active_sub_item = val;
    }
  }
});
