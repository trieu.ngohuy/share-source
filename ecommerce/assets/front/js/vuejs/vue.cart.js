var vue_cart = new Vue({
    el: '#cart-index-wrap',
    data: function() {
        return {
            pay_method: 0,
            paypal_class: 'dsp-none',
            arr_data: {
              places: arr_places,
              country: arr_places,
              province: [],
              district: [],
              ward: []
            },
            com_country: "",
            com_province: "",
            com_district: "",
            com_ward: "",
        }
    },
    beforeMount() {
      if (this.arr_data.places.length > 0) {
          var country_gid = -1;
          var tmp = this.load_data(this.arr_data, country_gid);
          this.arr_data = tmp.arr_data;
          this.com_province = tmp.com_province;
          this.com_district = tmp.com_district;
          this.com_ward = tmp.com_ward;
      }
    },
    methods: {
        remove_product: function(val) {
            var formData = new FormData();
            formData.append("index", val);

            axios.post(url_remove, formData)
                .then(function(response) {
                    location.reload();
                })
                .catch(function(error) {
                    alert('Có lỗi. Thử lại sau.');
                });
        },
        homepage: function() {
            location.href = url_home;
        },
        select_paymethod: function() {
            //this.pay_method = val;
            if (this.pay_method == 2) {
                this.paypal_class = "";
            } else {
                this.paypal_class = "dsp-none";
            }
        },
        load_data: function(arr_data, country_gid) {
            //province
            var arr_province = [];
            var active_province = "";
            for (var i = 0; i < this.arr_data.places.length; i++) {
                if (this.arr_data.places[i].parent_gid == country_gid) {
                    arr_province.push(this.arr_data.places[i]);
                }
            }
            arr_data.province = arr_province;
            //district
            var arr_district = [];
            if (arr_province.length > 0) {
                active_province = arr_province[0].places_gid;
                for (var i = 0; i < this.arr_data.places.length; i++) {
                    if (this.arr_data.places[i].parent_gid == arr_province[0].places_gid) {
                        arr_district.push(this.arr_data.places[i]);
                    }
                }
            }
            arr_data.district = arr_district;
            //ward
            var active_district = "";
            var active_ward = "";
            var arr_ward = [];
            if (arr_district.length > 0) {
                active_district = arr_district[0].places_gid;
                for (var i = 0; i < this.arr_data.places.length; i++) {
                    if (this.arr_data.places[i].parent_gid == arr_district[0].places_gid) {
                        arr_ward.push(this.arr_data.places[i]);
                    }
                }
            }
            arr_data.ward = arr_ward;
            if (arr_ward.length > 0) {
                active_ward = arr_ward[0].places_gid;
            }
            return {
                arr_data: arr_data,
                com_province: active_province,
                com_district: active_district,
                com_ward: active_ward
            };
        }
    },
    watch: {
      com_country: function(val) {
          var tmp = this.load_data(this.arr_data, val);
          this.arr_data = tmp.arr_data;

          this.com_province = "";
          this.com_district = "";
          this.com_ward = "";
      },
      com_province: function(val) {
          //district
          var arr_district = [];
          for (var i = 0; i < this.arr_data.places.length; i++) {
              if (this.arr_data.places[i].parent_gid == val) {
                  arr_district.push(this.arr_data.places[i]);
              }
          }
          this.arr_data.district = arr_district;
          //ward
          if (arr_district.length > 0) {
              var arr_ward = [];
              for (var i = 0; i < this.arr_data.places.length; i++) {
                  if (this.arr_data.places[i].parent_gid == arr_district[0].places_gid) {
                      arr_ward.push(this.arr_data.places[i]);
                  }
              }
              this.arr_data.ward = arr_ward;
          }

          this.com_district = "";
      },
      com_district: function(val) {
          //ward
          var arr_ward = [];
          for (var i = 0; i < this.arr_data.places.length; i++) {
              if (this.arr_data.places[i].parent_gid == val) {
                  arr_ward.push(this.arr_data.places[i]);
              }
          }

          this.arr_data.ward = arr_ward;
          this.com_ward = "";
      }
    }
});
