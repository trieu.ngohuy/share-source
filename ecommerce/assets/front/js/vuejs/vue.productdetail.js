var vue_productdetail = new Vue({
  el: '#product-detail-wrap',
  data: function(){
    return {
      is_trfpay: false,
      is_refmat: false,
      active_color: 0,
      active_size: 0,
      active_material: 0,
      qty: 1,
      product_gid: gid,
      response_msg: ''
    }
  },
  beforeMount() {
    if(arr_color.length == 1){
      this.active_color = arr_color[0]['product_properties_gid'];
    }
    if(arr_material.length == 1){
      this.active_material = arr_material[0]['product_properties_gid'];
    }
    if(arr_size.length == 1){
      this.active_size = arr_size[0]['product_properties_gid'];
    }
  },
  methods: {
    show_trfpay: function(){
      if(this.is_trfpay){
        this.is_trfpay = false;
      }else{
        this.is_trfpay = true;
      }
    },
    show_refmat: function(){
      if(this.is_refmat){
        this.is_refmat = false;
      }else{
        this.is_refmat = true;
      }
    },
    set_color: function(val){
      this.active_color = val;
    },
    set_size: function(val){
      this.active_size = val;
    },
    set_material: function(val){
      this.active_material = val;
    },
    increase: function(){
      this.qty = this.qty + 1;
    },
    decrease: function(){
      if(this.qty > 0){
          this.qty = this.qty - 1;
      }
    },
    form_submit: function(){
      if(this.active_size == 0){
        alert('Hãy chọn kích thước sản phẩm!');
        event.preventDefault();
      }else if(this.active_color == 0){
        alert('Hãy chọn màu sản phẩm!');
        event.preventDefault();
      }else if(this.active_material == 0){
        alert('Hãy chọn chất liệu sản phẩm!');
        event.preventDefault();
      }else if(this.qty == 0){
        alert('Số lượng sản phẩm phải lớn hơn 1!');
      }else{
        this.add_to_cart();
      }
    },
    add_to_cart: function(){
      var formData=new FormData();
      formData.append("product_gid",this.product_gid);
      formData.append("color_gid",this.active_color);
      formData.append("material_gid",this.active_material);
      formData.append("size_gid",this.active_size);
      formData.append("qty",this.qty);

      axios.post(url_cart, formData)
      .then(function (response) {
        update_cart(response.data);
      })
      .catch(function (error) {
          alert('Có lỗi. Thử lại sau.');
      });
    }
  }
});
