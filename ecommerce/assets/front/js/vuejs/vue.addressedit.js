var vue_addressedit = new Vue({
    el: '#my-account-wrap',
    data: function() {
        return {
            arr_data: {
                places: arr_places,
                data: arr_data,
                country: arr_places,
                province: [],
                district: [],
                ward: []
            },
            com_country: "",
            com_province: "",
            com_district: "",
            com_ward: "",
            is_init: true
        };
    },
    beforeMount() {
        if (this.arr_data.places.length > 0 && !this.is_empty(this.arr_data.data)) {
            var country_gid = -1;
            for (var i = 0; i < this.arr_data.places.length; i++) {
                if (this.arr_data.places[i].title.toLowerCase() == this.arr_data.data.country.toLowerCase()) {
                    country_gid = this.arr_data.places[i].places_gid;
                    this.com_country = country_gid;
                }
            }
            var tmp = this.load_data(this.arr_data, country_gid);
            this.arr_data = tmp.arr_data;
            this.com_province = tmp.com_province;
            this.com_district = tmp.com_district;
            this.com_ward = tmp.com_ward;
        }
    },
    watch: {
        com_country: function(val) {
            var tmp = this.load_data(this.arr_data, val);
            this.arr_data = tmp.arr_data;
            if(!this.is_init){
                this.com_province = "";
                this.com_district = "";
                this.com_ward = "";
            }
        },
        com_province: function(val) {
            //district
            var arr_district = [];
            for (var i = 0; i < this.arr_data.places.length; i++) {
                if (this.arr_data.places[i].parent_gid == val) {
                    arr_district.push(this.arr_data.places[i]);
                }
            }
            this.arr_data.district = arr_district;
            //ward
            if (arr_district.length > 0) {
                var arr_ward = [];
                for (var i = 0; i < this.arr_data.places.length; i++) {
                    if (this.arr_data.places[i].parent_gid == arr_district[0].places_gid) {
                        arr_ward.push(this.arr_data.places[i]);
                    }
                }
                this.arr_data.ward = arr_ward;
            }

            if(!this.is_init){
                this.com_district = "";
            }
        },
        com_district: function(val) {
            //ward
            var arr_ward = [];
            for (var i = 0; i < this.arr_data.places.length; i++) {
                if (this.arr_data.places[i].parent_gid == val) {
                    arr_ward.push(this.arr_data.places[i]);
                }
            }
            this.arr_data.ward = arr_ward;
            if(!this.is_init){
                this.com_ward = "";
            }else{
              this.is_init = false;
            }
        }
    },
    methods: {
        load_data: function(arr_data, country_gid) {
            //province
            var arr_province = [];
            var active_province = "";
            for (var i = 0; i < this.arr_data.places.length; i++) {
                if (this.arr_data.places[i].parent_gid == country_gid) {
                    arr_province.push(this.arr_data.places[i]);
                }
            }
            arr_data.province = arr_province;
            //district
            var arr_district = [];
            if (arr_province.length > 0) {
                active_province = arr_province[0].places_gid;
                for (var i = 0; i < this.arr_data.places.length; i++) {
                    if (this.arr_data.places[i].parent_gid == arr_province[0].places_gid) {
                        arr_district.push(this.arr_data.places[i]);
                    }
                }
            }
            arr_data.district = arr_district;
            //ward
            var active_district = "";
            var active_ward = "";
            var arr_ward = [];
            if (arr_district.length > 0) {
                active_district = arr_district[0].places_gid;
                for (var i = 0; i < this.arr_data.places.length; i++) {
                    if (this.arr_data.places[i].parent_gid == arr_district[0].places_gid) {
                        arr_ward.push(this.arr_data.places[i]);
                    }
                }
            }
            arr_data.ward = arr_ward;
            if (arr_ward.length > 0) {
                active_ward = arr_ward[0].places_gid;
            }
            return {
                arr_data: arr_data,
                com_province: active_province,
                com_district: active_district,
                com_ward: active_ward
            };
        },
        is_empty: function(obj) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key))
                    return false;
            }
            return true;
        }
    }
});
