var vue_addressbook = new Vue({
    el: '#my-account-wrap',
    data: function() {
        return {

        }
    },
    methods: {
        delete_address: function(val) {
            var formData = new FormData();
            formData.append("index", val);
            axios.post(url_delete, formData)
                .then(function(response) {
                    //Reload page
                    location.reload();
                })
                .catch(function(error) {
                    alert('Có lỗi. Thử lại sau.');
                });
        }
    }
});