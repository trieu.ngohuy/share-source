var vue_productcat = new Vue({
  el: '#product-cats-wrap',
  data: function(){
    return {
      left_active_item: 0,
      grid_size: 3,
      arr_data: {
        'products' : arr_products,
        'product_type' : arr_product_type,
        'style' : arr_style,
        'material' : arr_material,
        'color' : arr_color,
        'size' : arr_size
      },
      min_price: '',
      max_price: '',
      keyword: '',
      filter: [],
      order: 0,
    };
  },
  watch: {
    keyword: function(val){
      this.keyword = val;
      //Reload grid
      this.arr_data = this.reload_grid(this.arr_data, this.filter);
    },
    min_price: function(val){
      this.min_price = val;
      if((this.min_price == "" || this.min_price == 0) && (this.max_price == "" || this.max_price == 0)){
        this.filter = this.filter.filter(function( obj ) {
          return obj.type !== 'price';
        });
      }else {
        var index = this.filter.findIndex(x => x.type ==="price");
        if(index != -1){
            this.filter[index] = { 'val' : val, 'text' : this.min_price + 'vnđ - ' + this.max_price + 'vnđ', 'type' : 'price'};
        }else{
            this.filter.push({ 'val' : val, 'text' : this.min_price + 'vnđ - ' + this.max_price + 'vnđ', 'type' : 'price'});
        }
      }

      //Reload grid
      this.arr_data = this.reload_grid(this.arr_data, this.filter);
    },
    max_price: function(val){
      this.max_price = val;
      if((this.min_price == "" || this.min_price == 0) && (this.max_price == "" || this.max_price == 0)){
        this.filter = this.filter.filter(function( obj ) {
          return obj.type !== 'price';
        });
      }else {
        var index = this.filter.findIndex(x => x.type ==="price");
        if(index != -1){
            this.filter[index] = { 'val' : val, 'text' : this.min_price + 'vnđ - ' + this.max_price + 'vnđ', 'type' : 'price'};
        }else{
            this.filter.push({ 'val' : val, 'text' : this.min_price + 'vnđ - ' + this.max_price + 'vnđ', 'type' : 'price'});
        }
      }

      //Reload grid
      this.arr_data = this.reload_grid(this.arr_data, this.filter);
    },
  },
  methods: {
    show_sublist: function(val){
      this.left_active_item = val;
    },
    change_grid_size: function(val){
      this.grid_size = val;
    },
    order_grid: function(val){
      //0 or 3 -> order newest date
      if(val == 0 || val == 3){
        this.arr_data.products = sort_objects_array(this.arr_data.products, 'created_date_number', 'desc');
      }
      //1 -> price high -> low
      if(val == 1){
        this.arr_data.products = sort_objects_array(this.arr_data.products, 'price_vn', 'desc');
      }
      //2 -> price low -> high
      if(val == 2){
        this.arr_data.products = sort_objects_array(this.arr_data.products, 'price_vn', 'asc');
      }
    },
    loop_filter: function(arr_data, filter, type, val){
      if(filter.length <= 0){
        return true;
      }
      var tmp = false;

      //Check if there is style in filter
      if(type == 'style'){
        var count_style = 0;
        for(var i = 0 ; i < filter.length ; i++){
          if(filter[i].type == type){
            count_style++;
          }
        }
        if(count_style > 0){
          for(var i = 0 ; i < filter.length ; i++){
            if(type = filter[i].type && filter[i].val == val){
              tmp = true;
            }
          }
        }else{
          //Check if there is product_type
          var count_type = 0;
          for(var i = 0 ; i < filter.length ; i++){
            if(filter[i].type == 'product_type'){
              count_type++;
            }
          }
          if(count_type > 0 ){
            for(var i = 0 ; i < arr_data.style.length ; i++){
              if(arr_data.style[i].product_category_gid == val){
                tmp = true;
              }
            }
          }else{
            tmp = true;
          }
        }
      }else{
        var count = 0;
        for(var i = 0 ; i < filter.length ; i++){
          if(filter[i].type == type){
            count++;
          }
        }
        if(count <= 0){
          return true;
        }else{
          if(type == 'product_type'){
            for(var i = 0 ; i < filter.length ; i++){
              if(type = filter[i].type && filter[i].val == val){
                tmp = true;
              }
            }
          }else{
            val = val.split(',');
            for(var i = 0 ; i < filter.length ; i++){
              if(type = filter[i].type && val.indexOf(filter[i].val) !== -1){
                tmp = true;
              }
            }
          }
        }
      }

      return tmp;
    },
    reload_grid: function(arr_data, filter){
      var reload_data = [];
      arr_data.products = arr_products;
      for(var i = 0 ; i < arr_data.products.length ; i++) {
        var tmp = true;
        //product_category_gid (style)
        tmp = this.loop_filter(arr_data, filter, 'style', arr_data.products[i].product_category_gid);
        //color
        var tmp1 = this.loop_filter(arr_data, filter, 'color', arr_data.products[i].color_gid);
        if(tmp){
          tmp = tmp1;
        }
        //material
        tmp1 = this.loop_filter(arr_data, filter, 'material', arr_data.products[i].material_gid);
        if(tmp){
          tmp = tmp1;
        }
        //size
        tmp1 = this.loop_filter(arr_data, filter, 'size', arr_data.products[i].size_gid);
        if(tmp){
          tmp = tmp1;
        }

        //keyword
        if(arr_data.products[i].title.toLowerCase().indexOf(this.keyword.toLowerCase()) >= 0){
          tmp1 = true;
        }else{
          tmp1 = false;
        }
        if(tmp){
          tmp = tmp1;
        }

        //price
        if((this.min_price == "" || this.min_price == 0) && (this.max_price == "" || this.max_price == 0)){
          tmp1 = true;
        }else if((parseInt(this.min_price) <= parseInt(arr_data.products[i].price_vn)) && (parseInt(arr_data.products[i].price_vn) <= parseInt(this.max_price))){
          tmp1 = true;
        }else{
          tmp1 = false;
        }
        if(tmp){
          tmp = tmp1;
        }

        if(tmp){
          reload_data.push(arr_data.products[i]);
        }
      };
      arr_data.products = reload_data;
      return arr_data;
    },
    add_filter: function(type, val, text){
      //Add filter
      this.filter.push({ 'val' : val, 'text' : text, 'type' : type});

      //Reload filter
      this.arr_data = this.reload_filter(this.arr_data, this.filter);
      //Reload grid
      this.arr_data = this.reload_grid(this.arr_data, this.filter);
    },
    reload_filter: function(arr_data, filter){
      //Reset default data
      arr_data.product_type = arr_product_type;
      arr_data.style = arr_style;
      arr_data.material = arr_material;
      arr_data.color = arr_color;
      arr_data.size = arr_size;

      tmp_style = [];

      for(var i = 0 ; i < filter.length ; i++){
        //Reload filter
        if(filter[i].type == 'product_type'){
          arr_data.product_type = arr_data.product_type.filter(function( obj ) {
            return obj.product_category_gid !== filter[i].val;
          });
          $.merge(tmp_style, arr_style.filter(function( obj ) {
            return obj.parent_gid !== filter[i].val;
          }));
        }
        if(filter[i].type == 'style'){
          arr_data.style = arr_data.style.filter(function( obj ) {
            return obj.product_category_gid !== filter[i].val;
          });
        }
        if(filter[i].type == 'material'){
          arr_data.material = arr_data.material.filter(function( obj ) {
            return obj.product_properties_gid !== filter[i].val;
          });
        }
        if(filter[i].type == 'color'){
          arr_data.color = arr_data.color.filter(function( obj ) {
            return obj.product_properties_gid !== filter[i].val;
          });
        }
        if(filter[i].type == 'size'){
          arr_data.size = arr_data.size.filter(function( obj ) {
            return obj.product_properties_gid !== filter[i].val;
          });
        }
      }

      //Update data
      if(tmp_style.length > 0){
          arr_data.style = tmp_style;
      }

      return arr_data;
    },
    remove_filter: function(type, val){
      if(type == 'all'){
        this.filter = [];
        this.keyword = "";
      }else{
        this.filter = this.filter.filter(function( obj ) {
          return !(obj.type == type && obj.val == val);
        });
      }
      if(type == 'price' || type == 'all'){
        this.min_price = 0;
        this.max_price = 0;
      }
      //Reload filter
      this.arr_data = this.reload_filter(this.arr_data, this.filter);
      //Reload grid
      this.arr_data = this.reload_grid(this.arr_data, this.filter);
    }
  }
});
