//variables
var arr_ckeditor = [];

var mulf_item = 'mulf_item';
var mulf_title = 'mulf_title';
var mulf_input = 'mulf_input';
var mulf_img = 'mulf_img';
var mulf_remove = 'mulf_remove';
var mulf_wrap = 'mulf_wrap';
var mulf_add_btn = 'mulf_add';
var mulf_choose = 'mulf_choose';

var sigf_title = 'sigf_title';
var sigf = 'sigf_wrap';
var sigf_input = 'sigf_input';
var sigf_img = 'sigf_img';
var sigf_remove = 'sigf_remove';
var sigf_choose = 'sigf_choose';
var sigf_wrap = 'sigf_wrap';

var no_img = 'https://c-lj.gnst.jp/public/img/common/noimage.jpg?20181011050048';


//Display multilanguage item
function multilanguage_item(data, type, key) {
    var html = '<td class="mul">';
    //text
    if (type == 0) {
        var val = '';

        detail = data['vi'];
        if (detail[key] == null || $.trim(detail[key]) == '') {
            if (key == 'link' && $.trim(detail['url']) !== null && $.trim(detail['url']) !== '') {
                val = detail['url'];
            } else {
                val = '(Note set)';
            }
        } else {
            val = detail[key];
        }
        html += '      <p>' + val + '</p>';

        html += '      <hr>';

        detail = data['en'];
        if (detail[key] == null || $.trim(detail[key]) == '') {
            if (key == 'link' && $.trim(detail['url']) !== null && $.trim(detail['url']) !== '') {
                val = detail['url'];
            } else {
                val = '(Note set)';
            }
        } else {
            val = detail[key];
        }
        html += '      <p>' + val + '</p>';
    }
    html += '</td>';
    return html;
}
//Ini ckeditor
function init_ckeditor(id){
  //Init ckeditor
  var option = {
    toolbar: [ 'ckfinder', '|', 'heading', '|', 'bold', 'italic', 'underline', 'link', 'bulletedList', 'numberedList', 'blockQuote', 'alignment', 'undo', 'redo', 'insertTable'],
    ckfinder: {
      // Upload the images to the server using the CKFinder QuickUpload command.
      uploadUrl: 'https://localhost/webbanhang/libs/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&responseType=json',
      openerMethod: 'popup'
    }
  };
  DecoupledEditor
    .create( document.querySelector( '#' + id ), {
      toolbar: [ 'ckfinder', '|', 'heading', '|', 'bold', 'italic', 'underline', 'link', 'bulletedList', 'numberedList', 'blockQuote', 'alignment', 'undo', 'redo', 'insertTable'],
      ckfinder: {
        // Upload the images to the server using the CKFinder QuickUpload command.
        uploadUrl: 'https://localhost/webbanhang/libs/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&responseType=json',
        openerMethod: 'popup'
      }
    } )
    .then( editor => {
      const toolbarContainer = document.querySelector( '.' + id + ' .toolbar-container' );

      toolbarContainer.prepend( editor.ui.view.toolbar.element );

      window.editor = editor;

      //Add editor
      arr_ckeditor[id]  = editor;
    } )
    .catch( err => {
      console.error( err.stack );
    } );
}
//init multi file upload
function mulf_init(title, name, arr){
  var html = '';

  //Add title
  html += '<div id="'+mulf_title+'">';
  html += '<label>'+title+'  </label>';
  html += '<input type="button" id="'+mulf_add_btn+'" value="Add more" onclick="mulf_add(\''+name+'\')"/>';
  html += '</div>';
  html += '<br />';
  //Add wrap div
  html += '<div id="'+mulf_wrap+'">';

  //Close wrap
  html += '</div>';

  //Display html
  $('#' + name).html(html);

  if(arr.length <= 0){
    //Add default item
    mulf_add(name);
  }else{
    for(var i = 0 ; i < arr.length ; i++){
      mulf_add(name, arr[i]);
    }
  }
}
//Add item
function mulf_add(name, src){
  var html = '';
  var index = 0;
  var val = '';
  if(src == "" || src == undefined){
    src = no_img;
  }else{
    val = src;
  }
  //count
  index = $('#' + name + ' .'+mulf_item).length + 1;

  //wrap
  html += '<div class="'+mulf_item+'" id="'+mulf_item+'_'+index+'">';

  //preview img
  html += '<img class="'+mulf_img+'" id="'+mulf_img+'_'+index+'" src="'+src+'" height="100px" width="100px"/>';
  //input
  html += '<input class="'+mulf_input+'" id="'+mulf_input+'_'+index+'" name="'+name+'" data-index="'+index+'" type="hidden" value="'+val+'">';
  html += '<br />';
  //choose
  html += '<a class="'+mulf_choose+'" id="'+mulf_choose+'_'+index+'" href="#" data-index="'+index+'" onclick="handle_ckfinder(\''+name+'\', \''+mulf_img+'\', \''+mulf_input+'\', '+index+')">Choose</a>';
  //remove
  html += '<a class="'+mulf_remove+'" id="'+mulf_remove+'_'+index+'" href="#" data-index="'+index+'" data-name="'+name+'">Remove</a>';

  html += '</div>';

  $('#' + name + ' #' + mulf_wrap).append(html);
}
//Add css
function mulf_css(element, id){
  var css = '<style>';

  //padding top
  css += '.'+mulf_wrap+', .'+element+'{float:left; width:100%;}';
  css += '.'+mulf_item+'{margin-bottom: 10px;padding: 10px;width:121px;float:left;}';
  css += '.'+mulf_choose+'{margin-right: 10px;}';
  css += '.'+mulf_choose+', .'+mulf_remove+'{margin-right: 5px;}';
  css += '#'+mulf_title+' label{margin-right: 10px;}';
  //end
  css += '</style>';

  //append css
  $('#' + id).append(css);
}
//events
function mulf_events(){
  //click remove
  $('body').on('click', '.'+mulf_remove+'', function(){
    //Get attribute
    var index = $(this).attr('data-index');
    //Get current element name
    var name = $(this).attr('data-name');
    //Remove mulf-item
    $('#' + name + ' #'+mulf_item+'_' + index).remove();
  });
}
//Hanle CKFinder
function handle_ckfinder(name, id_img, id_input, index){
  CKFinder.modal({
       chooseFiles: true,
       onInit: function( finder ) {
           finder.on( 'files:choose', function( evt ) {
               var file = evt.data.files.first();
               //document.getElementById( 'url' ).value = file.getUrl();
               $('#' + name + ' #'+id_img+'_'+index).attr('src', file.getUrl());
               $('#' + name + ' #'+id_input+'_'+index).val(file.getUrl());
           } );
           finder.on( 'file:choose:resizedImage', function( evt ) {
               document.getElementById( 'url' ).value = evt.data.resizedUrl;
           } );
       }
   });
}
//init single file upload
function sigf_init(title, name, src){
  var html = '';
  var val = '';
  var index = 0;
  if(src == "" || src == undefined){
    src = no_img;
  }else{
    val = src;
  }

  //Add title
  html += '<div id="'+sigf_title+'">';
  html += '<label>'+title+'  </label>';
  html += '</div>';
  html += '<br />';
  //Add wrap div
  html += '<div id="'+sigf_wrap+'">';

  //preview img
  html += '<img class="'+sigf_img+'" id="'+sigf_img+'_'+index+'" src="'+src+'" height="100px" width="100px"/>';
  //input
  html += '<input class="'+sigf_input+'" id="'+sigf_input+'_'+index+'" name="'+name+'" type="hidden" value="'+val+'">';
  html += '<br />';
  //choose
  html += '<a class="'+sigf_choose+'" href="#" onclick="handle_ckfinder(\''+name+'\', \''+sigf_img+'\', \''+sigf_input+'\','+index+')">Choose</a>';
  //remove
  html += '<a class="'+sigf_remove+'" href="#" data-name="'+name+'">Remove</a>';

  //Close wrap
  html += '</div>';

  //Display html
  $('#' + name).html(html);

  //css
  sigf_css(name);
}
//add css
function sigf_css(id){
  var css = '<style>';

  //padding top
  css += '#'+id+'{float:left; width:100%;}';
  css += '.'+sigf_wrap+'{margin-bottom: 10px;padding: 10px;width:121px;float:left;}';
  css += '.'+sigf_choose+'{margin-right: 10px;}';
  css += '.'+sigf_choose+', .'+mulf_remove+'{margin-right: 5px;}';
  css += '#'+sigf_title+' label{margin-right: 10px;}';
  //end
  css += '</style>';

  //append css
  $('#' + id).append(css);
}
//events
function sigf_events(){
  //click remove
  $('body').on('click', '.'+sigf_remove+'', function(){
    //Get current element name
    var name = $(this).attr('data-name');
    //Remove mulf-item
    $('#' + name + ' .'+sigf_img).attr('src', no_img);
    $('#' + name + ' .'+sigf_input).val('');
  });
}
