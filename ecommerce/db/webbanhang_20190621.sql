-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for webbanhang
CREATE DATABASE IF NOT EXISTS `webbanhang` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `webbanhang`;

-- Dumping structure for table webbanhang.zl_banner
CREATE TABLE IF NOT EXISTS `zl_banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT 1,
  `title` varchar(100) NOT NULL,
  `logo` text NOT NULL,
  `controller` varchar(50) NOT NULL,
  `function` varchar(50) NOT NULL,
  `params` varchar(50) NOT NULL,
  `link` text NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 - top, 1 - bottom',
  `sorting` int(11) NOT NULL,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`banner_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_banner: ~10 rows (approximately)
/*!40000 ALTER TABLE `zl_banner` DISABLE KEYS */;
REPLACE INTO `zl_banner` (`banner_id`, `banner_gid`, `lang_id`, `title`, `logo`, `controller`, `function`, `params`, `link`, `enabled`, `type`, `sorting`, `created_date`) VALUES
	(1, 1, 1, 'Ảnh 1', 'assets/upload/users/admin/slide/top_banner.jpg', 'content', 'detail', 'gioi-thieu', '', 1, 1, 1, 1559167200),
	(2, 1, 2, 'Banner 1', 'assets/upload/users/admin/slide/top_banner.jpg', 'content', 'index', 'about-us', '', 1, 1, 2, 1559167200),
	(3, 3, 1, 'Ảnh 2', 'assets/upload/users/admin/slide/bottom_banner.jpg', 'store', 'index', '', '', 1, 1, 3, 1559167200),
	(4, 3, 2, 'Banner 2', 'assets/upload/users/admin/slide/bottom_banner.jpg', 'store', 'index', '', '', 1, 1, 4, 1559167200),
	(5, 5, 1, 'Anh 3', 'assets/upload/users/admin/banner/HP-dresses.jpg', 'product', 'index', 'trang-suc-ban-chay-nhat', 'a', 1, 0, 5, 1559340000),
	(6, 5, 2, 'banner 3', 'assets/upload/users/admin/banner/HP-dresses.jpg', 'content', 'index', 'about-us', 'b', 1, 0, 5, 1559340000),
	(7, 7, 1, 'Anh 4', 'assets/upload/users/admin/banner/80-WALL-ARTWORK_383.jpg', 'product', 'index', 'tron-bo-bong-tai', 'a', 1, 0, 0, 1559426400),
	(8, 7, 2, 'Banner 4', 'assets/upload/users/admin/banner/80-WALL-ARTWORK_383.jpg', 'content', 'index', '', 'b', 1, 0, 0, 1559426400),
	(15, 9, 1, 's', '', 'product', 'index', 'vong-khuyen-tai', '', 1, 0, 0, 1560204000),
	(16, 9, 2, 's', '', 'product', 'index', 'gift', '', 1, 0, 0, 1560204000);
/*!40000 ALTER TABLE `zl_banner` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_cart
CREATE TABLE IF NOT EXISTS `zl_cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_address` text COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `voucher_gid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `payment_method` int(11) NOT NULL COMMENT '0 - transfer, 1 - paypal',
  `payment_info` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_swedish_ci DEFAULT NULL,
  `token` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0 - processing, 1 - payed',
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`cart_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_cart: ~4 rows (approximately)
/*!40000 ALTER TABLE `zl_cart` DISABLE KEYS */;
REPLACE INTO `zl_cart` (`cart_id`, `delivery_address`, `full_name`, `phone`, `code`, `voucher_gid`, `user_id`, `payment_method`, `payment_info`, `token`, `status`, `created_date`) VALUES
	(2, '1', '', '', '107AWW0XXV', 1, 2, 1, NULL, NULL, 0, 1544569200),
	(3, '1', '', '', '107AWW1XXC', 2, 2, 1, NULL, NULL, 0, 1557266400),
	(4, '0', '', '', 'JGH7216WO2', 1, 2, 1, '', 'daa3a2bac9260bfb3877cb96f4c49cdb0665a192c12aaa2a5d9a40cd219f1e01d851b3a610c47e54c5a2f89fbd2fe3842da9645c08c45ff3ff8af1b2bd1a5e61', 1, 1558130400),
	(10, '1', '', '', 'GU41FUYBSP', 0, 2, 2, '{\\"create_time\\":\\"2019-06-10T13:50:54Z\\",\\"update_time\\":\\"2019-06-10T13:50:54Z\\",\\"id\\":\\"0BC266003P0571603\\",\\"intent\\":\\"CAPTURE\\",\\"status\\":\\"COMPLETED\\",\\"payer\\":{\\"email_address\\":\\"windowsphone2012nt@gmail.com\\",\\"payer_id\\":\\"R6C572D97C2HG\\",\\"address\\":{\\"country_code\\":\\"US\\"},\\"name\\":{\\"given_name\\":\\"Trieu\\",\\"surname\\":\\"Mua\\"}},\\"purchase_units\\":[{\\"reference_id\\":\\"default\\",\\"amount\\":{\\"value\\":\\"47.30\\",\\"currency_code\\":\\"USD\\"},\\"payee\\":{\\"email_address\\":\\"trieu.public@gmail.com\\",\\"merchant_id\\":\\"TV59EEPH3MDJL\\"},\\"shipping\\":{\\"name\\":{\\"full_name\\":\\"Trieu Mua\\"},\\"address\\":{\\"address_line_1\\":\\"1 Main St\\",\\"admin_area_2\\":\\"San Jose\\",\\"admin_area_1\\":\\"CA\\",\\"postal_code\\":\\"95131\\",\\"country_code\\":\\"US\\"}},\\"payments\\":{\\"captures\\":[{\\"status\\":\\"COMPLETED\\",\\"id\\":\\"77N29882R83232642\\",\\"final_capture\\":true,\\"create_time\\":\\"2019-06-10T13:50:54Z\\",\\"update_time\\":\\"2019-06-10T13:50:54Z\\",\\"amount\\":{\\"value\\":\\"47.30\\",\\"currency_code\\":\\"USD\\"},\\"seller_protection\\":{\\"status\\":\\"ELIGIBLE\\",\\"dispute_categories\\":[\\"ITEM_NOT_RECEIVED\\",\\"UNAUTHORIZED_TRANSACTION\\"]},\\"links\\":[{\\"href\\":\\"https://api.sandbox.paypal.com/v2/payments/captures/77N29882R83232642\\",\\"rel\\":\\"self\\",\\"method\\":\\"GET\\",\\"title\\":\\"GET\\"},{\\"href\\":\\"https://api.sandbox.paypal.com/v2/payments/captures/77N29882R83232642/refund\\",\\"rel\\":\\"refund\\",\\"method\\":\\"POST\\",\\"title\\":\\"POST\\"},{\\"href\\":\\"https://api.sandbox.paypal.com/v2/checkout/orders/0BC266003P0571603\\",\\"rel\\":\\"up\\",\\"method\\":\\"GET\\",\\"title\\":\\"GET\\"}]}]}}],\\"links\\":[{\\"href\\":\\"https://api.sandbox.paypal.com/v2/checkout/orders/0BC266003P0571603\\",\\"rel\\":\\"self\\",\\"method\\":\\"GET\\",\\"title\\":\\"GET\\"}]}', '50e46bf6a9835c3c6bb511b4be458a405e1059c8657536607796e928c1096a2de7fceef32c2b2ec1684ac64587a7fd370f9ef8f8124a99a59297712278547349', 1, 1560117600);
/*!40000 ALTER TABLE `zl_cart` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_cart_detail
CREATE TABLE IF NOT EXISTS `zl_cart_detail` (
  `cart_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) NOT NULL,
  `product_gid` int(11) NOT NULL,
  `color_gid` int(11) NOT NULL,
  `material_gid` int(11) NOT NULL,
  `size_gid` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`cart_detail_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_cart_detail: ~9 rows (approximately)
/*!40000 ALTER TABLE `zl_cart_detail` DISABLE KEYS */;
REPLACE INTO `zl_cart_detail` (`cart_detail_id`, `cart_id`, `product_gid`, `color_gid`, `material_gid`, `size_gid`, `count`, `created_date`) VALUES
	(1, 2, 1, 1, 4, 6, 1, 1544569200),
	(2, 3, 3, 1, 4, 6, 2, 1544569200),
	(3, 3, 2, 1, 4, 6, 3, 1557266400),
	(4, 4, 4, 1, 4, 6, 1, 1558130400),
	(5, 6, 4, 1, 4, 6, 1, 1560117600),
	(6, 7, 3, 1, 4, 6, 1, 1560117600),
	(7, 8, 3, 1, 4, 6, 1, 1560117600),
	(8, 9, 4, 1, 4, 6, 1, 1560117600),
	(9, 10, 5, 1, 4, 6, 1, 1560117600);
/*!40000 ALTER TABLE `zl_cart_detail` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_content
CREATE TABLE IF NOT EXISTS `zl_content` (
  `content_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_gid` int(11) NOT NULL,
  `content_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `alias` varchar(200) NOT NULL,
  `intro_text` varchar(100) NOT NULL,
  `full_text` longtext NOT NULL,
  `logo` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`content_id`) USING BTREE,
  UNIQUE KEY `alias` (`alias`) USING BTREE,
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_content: ~10 rows (approximately)
/*!40000 ALTER TABLE `zl_content` DISABLE KEYS */;
REPLACE INTO `zl_content` (`content_id`, `content_gid`, `content_category_gid`, `lang_id`, `title`, `alias`, `intro_text`, `full_text`, `logo`, `sorting`, `enabled`, `created_date`) VALUES
	(1, 1, -1, 1, 'Liên hệ với chúng tôi', 'lien-he-voi-chung-toi', '', '<h1><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Liên lạc!</font></font></h1>\r\n<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bạn có thể tìm thấy thông tin bạn đang tìm kiếm trong </font></font><a href="https://oliverbonas.freshdesk.com/support/home"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Trung tâm trợ giúp</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> \r\ncủa chúng tôi Hoặc Nhóm dịch vụ khách hàng của chúng tôi sẽ rất vui khi được nghe từ bạn.</font></font></p>\r\n<h4><span style="color: #72ac96;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gửi email cho chúng tôi: </font></font><a style="text-decoration: underline; color: #72ac96;" href="mailto:AskUs@oliverbonas.com"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AskUs@oliverbonas.com</font></font></a></span></h4>\r\n<h4><span style="color: #72ac96;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gọi cho chúng tôi: 020 8974 0110</font></font></span></h4>\r\n<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đường dây mở cửa Thứ Hai - Thứ Sáu 8 giờ sáng - 7 giờ tối, Thứ Bảy 8 giờ sáng - 4 giờ chiều, Chủ Nhật 8 giờ sáng - 4 giờ chiều </font></font><br>\r\n&nbsp;<br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">\r\nNgày lễ ngân hàng Thứ Hai ngày 6 tháng 5 - 4:30 chiều</font></font></p>', '', 1, 1, 1544482800),
	(2, 2, -1, 1, 'Vận chuyển', 'van-chuyen', '', '<h1><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chuyển</font></font></h1>\r\n<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chữ ký được yêu cầu cho các dịch vụ giao hàng Tiêu chuẩn, Ngày hôm sau và Thứ bảy của chúng tôi, nhưng bạn sẽ nhận được một SMS cho phép bạn theo dõi hoặc sắp xếp lại việc giao hàng của mình để thuận tiện nhất có thể.</font></font></p>\r\n<h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Giao hàng ngày hôm sau</font></font></h3>\r\n<p><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">£ 5,95 (£ 2,00 cho đơn hàng £ 50,00 trở lên)</font></font></b><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> \r\nĐặt hàng trước 3:30 chiều từ thứ Hai đến thứ Năm để giao hàng vào ngày hôm sau. </font><font style="vertical-align: inherit;">Đơn đặt hàng vào thứ Sáu sẽ được giao vào thứ Hai tới lục địa Anh. </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">\r\nĐặt hàng vào cuối tuần cho đến giữa trưa Chủ nhật để giao hàng vào thứ Hai đến lục địa Anh. </font><font style="vertical-align: inherit;">Không bao gồm nội thất. </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">\r\nGiao hàng ngày hôm sau không có sẵn cho Bắc Ireland, Tây Nguyên và Quần đảo Channel</font></font></p>\r\n<h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Giao hàng tiêu chuẩn</font></font></h3>\r\n<p><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">£ 3,95 (Miễn phí cho các đơn hàng £ 50,00 trở lên)</font></font></b><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> \r\nĐặt hàng từ thứ Hai đến Chủ nhật để giao hàng trong 2-4 ngày làm việc (bao gồm Bắc Ireland, Tây Nguyên và Quần đảo Channel). </font><font style="vertical-align: inherit;">Không bao gồm nội thất.</font></font></p>\r\n<h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đề cử giao hàng thứ bảy</font></font></h3>\r\n<p><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">£ 6,95 (£ 3,00 cho đơn hàng £ 50,00 trở lên)</font></font></b><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> \r\nĐặt hàng bất kỳ ngày nào đến 3:30 tối thứ Sáu để giao hàng vào Thứ Bảy tới lục địa Anh. </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">\r\nViệc giao hàng đến Bắc Ireland, Tây Nguyên và Quần đảo Channel cần được đặt trước 3h30 chiều thứ Năm. </font><font style="vertical-align: inherit;">Không bao gồm nội thất.</font></font></p>\r\n<h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Quốc tế</font></font></h3>\r\n<p><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Giá khác nhau (Miễn phí cho đơn hàng £ 50,00 trở lên)</font></font></b><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> \r\nChúng tôi giao hàng trên toàn thế giới, giá cả và thời gian giao hàng khác nhau tùy thuộc vào điểm đến. </font><font style="vertical-align: inherit;">Giao hàng miễn phí có sẵn cho các đơn hàng trên £ 50,00. </font><font style="vertical-align: inherit;">Không bao gồm nội thất. </font></font><a class="underline" href="https://oliverbonas.freshdesk.com/support/solutions/articles/15000011654-international-delivery" target="_blank" rel="noopener"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nhấp vào đây để có thêm thông tin</font></font></a></p>\r\n<h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bộ sưu tập từ cửa hàng</font></font></h3>\r\n<p><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nhấp và Thu thập £ 2,00 (Miễn phí cho đơn hàng từ 20,00 bảng trở lên)</font></font></b><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> \r\nĐặt hàng trực tuyến và thu thập từ bất kỳ cửa hàng nào của chúng tôi trong 1-4 ngày làm việc. </font><font style="vertical-align: inherit;">Xem thanh toán cho Ngày thu thập của cửa hàng bạn đã chọn. </font><font style="vertical-align: inherit;">Không bao gồm nội thất.</font></font></p>\r\n<h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đồ nội thất</font></font></h3>\r\n<p><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nội thất lớn £ 35,00 giao hàng từ thứ Hai đến thứ Sáu. </font><font style="vertical-align: inherit;">£ 45 Giao hàng thứ bảy (Miễn phí cho đơn hàng £ 750,00 trở lên)</font></font></b><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> \r\nĐặt hàng từ thứ Hai đến Chủ nhật để giao hàng trong vòng 12 ngày làm việc </font></font><br>\r\n<b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nội thất nhỏ £ 14,00</font></font></b><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> \r\nĐặt hàng từ thứ Hai đến Chủ nhật để giao hàng trong 3-6 ngày làm việc</font></font></p>', '', 2, 1, 1544482800),
	(3, 3, -1, 1, 'Trung tâm hỗ trợ', 'trung-tam-ho-tro', '', '<h1><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Trung tâm hỗ trợ</font></font></h1>	\r\n<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Giải đáp mọi thắc mắc yêu cầu từ khách hàng</font></font></p>', '', 3, 1, 1544914800),
	(4, 4, 2, 2, 'Father\'s Day Gift Inspiration with the OB Buying Team 3', 'father-s-day-gift-inspiration-with-the-ob-buying-team-3', 'Gather Father\'s Day Gift Inspiration with the OB Buying Team, on the blog today', 'What is Lorem Ipsum?\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nWhy do we use it?\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n\r\nWhere does it come from?\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', 'assets/upload/users/admin/contents/1.jpg', 4, 1, 1544914800),
	(5, 1, -1, 2, 'Contact us', 'contact-us', '', '<h1>Get In Touch!</h1>\r\n<p>You may find the information you’re looking for in our <a href="https://oliverbonas.freshdesk.com/support/home">Help Centre</a><br>\r\nOr our Customer Services Team would be happy to hear from you.</p>\r\n<h4><span style="color: #72ac96;">Email us: <a style="text-decoration: underline; color: #72ac96;" href="mailto:AskUs@oliverbonas.com">AskUs@oliverbonas.com</a></span></h4>\r\n<h4><span style="color: #72ac96;">Call us: 020 8974 0110</span></h4>\r\n<p>Lines are open Monday – Friday 8am – 7pm, Saturday 8am – 4pm, Sunday 8am – 4pm<br>\r\n&nbsp;<br>\r\nBank holiday Monday 6th May 8 – 4:30pm</p>', '', 1, 1, 1544914800),
	(6, 2, -1, 2, 'Delivery', 'delivery', '', '<h1>Delivery</h1>\r\n<p>A signature is required for our Standard, Next Day and Saturday delivery services, but you will receive an SMS which allows you to track or rearrange your delivery to make it as convenient as possible.</p>\r\n<h3>Next Day Delivery</h3>\r\n<p><b>£5.95 (£2.00 for orders £50.00 and over)</b><br>\r\nOrder by 3.30pm Monday to Thursday for Next Day delivery. Orders placed on Friday will be delivered on Monday to the UK mainland.<br>\r\nOrder over the weekend until midday on Sunday for delivery on Monday to the UK mainland. Excludes furniture.<br>\r\nNext Day delivery is not available for Northern Ireland, Highlands and Channel Islands</p>\r\n<h3>Standard Delivery</h3>\r\n<p><b> £3.95 (Free for orders £50.00 and over)</b><br>\r\nOrder Monday to Sunday for delivery in 2-4 working days (including Northern Ireland, Highlands and Channel Islands). Excludes furniture.</p>\r\n<h3>Nominated Saturday Delivery</h3>\r\n<p><b> £6.95 (£3.00 for orders £50.00 and over)</b><br>\r\nOrder any day up to 3.30pm on Friday for delivery on Saturday to the UK mainland.<br>\r\nDeliveries to Northern Ireland, Highlands and Channel Islands need to be placed by 3.30pm on Thursday. Excludes furniture.</p>\r\n<h3>International</h3>\r\n<p><b> Prices vary (Free for orders £50.00 and over)</b><br>\r\nWe ship worldwide, prices and delivery times vary depending on destination. Free shipping is available on orders over £50.00. Excludes furniture.<a class="underline" href="https://oliverbonas.freshdesk.com/support/solutions/articles/15000011654-international-delivery" target="_blank" rel="noopener">Click here for more information</a></p>\r\n<h3>Collection from store</h3>\r\n<p><b>Click &amp; Collect £2.00 (Free for orders £20.00 and over)</b><br>\r\nOrder online and collect from any of our stores in 1-4 working days. See the checkout for Collection Dates of your selected store. Excludes furniture.</p>\r\n<h3>Furniture</h3>\r\n<p><b>Large Furniture £35.00 delivery Monday to Friday. £45 Saturday delivery (Free for orders £750.00 and over)</b><br>\r\nOrder Monday to Sunday for delivery within 12 working days<br>\r\n<b>Small Furniture £14.00 </b><br>\r\nOrder Monday to Sunday for delivery in 3-6 working days</p>', '', 2, 1, 1544914800),
	(7, 3, -1, 2, 'Help center', 'help-center', '', '<h1><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Help center</font></font></h1>	\r\n<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">You have a quenstion? We have answers.</font></font></p>', '', 0, 1, 1544914800),
	(8, 8, -1, 2, 'About us 1', 'about-us', '', '<div class="m-b text text--no-margin 1 left p-a-3 ta- col-" ng-class="::[vm.component.color, vm.component.align, vm.component.background ? \'p-a-3\' : \'\', \'ta-\' + line.text_align,\'col-\' + line.color]" ng-style="::vm.component.background &amp;&amp; {\'background-color\' : vm.component.background}" ng-bind-html="::vm.component.text | trustedHtml" ng-if="::vm.component.text"><p style="text-align: center;">Our story starts with our founder, Olly. His creative upbringing, culturally influenced by the many countries he lived in, instilled a love of design and an exploratory spirit from an early age. At university, whilst studying Anthropology, Olly began to bring back gifts for friends from his travels abroad. He turned this into a small business and in 1993 the first shop opened, repainted by his friends with Olly behind a second-hand till.</p>\r\n<p style="text-align: center;">Over two decades on, Oliver Bonas has evolved from curating others’ designs to creating our own. Our team of designers now take Olly’s exploratory spirit and channel it into our fashion and homeware collections. We are inspired by the alchemy of great design and fresh thinking and the belief that design has the power to positively affect how we feel. New designs land in store and online every week.</p>\r\n<p style="text-align: center;">Today, OB has grown to over 70 stores across the UK and is run by an amazing team who share Olly’s passion for bringing new ideas to life. We have fun and follow our company values: Work Hard, Play Hard &amp; Be Kind.</p>\r\n</div>', '', 0, 1, 1544914800),
	(9, 8, -1, 1, 'Giới thiệu', 'gioi-thieu', '', '<div class="m-b text text--no-margin 1 left p-a-3 ta- col-" ng-class="::[vm.component.color, vm.component.align, vm.component.background ? \'p-a-3\' : \'\', \'ta-\' + line.text_align,\'col-\' + line.color]" ng-style="::vm.component.background &amp;&amp; {\'background-color\' : vm.component.background}" ng-bind-html="::vm.component.text | trustedHtml" ng-if="::vm.component.text"><p style="text-align: center;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Câu chuyện của chúng tôi bắt đầu với người sáng lập của chúng tôi, Olly. </font><font style="vertical-align: inherit;">Sự giáo dục sáng tạo của anh ấy, chịu ảnh hưởng văn hóa của nhiều quốc gia anh ấy sống, thấm nhuần tình yêu thiết kế và tinh thần khám phá từ khi còn nhỏ. </font><font style="vertical-align: inherit;">Tại trường đại học, trong khi học Nhân chủng học, Olly bắt đầu mang về những món quà cho bạn bè từ những chuyến đi nước ngoài. </font><font style="vertical-align: inherit;">Ông đã biến điều này thành một doanh nghiệp nhỏ và vào năm 1993, cửa hàng đầu tiên được mở ra, được bạn bè của ông với Olly đứng sau một cửa hàng cũ.</font></font></p>\r\n<p style="text-align: center;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Trải qua hơn hai thập kỷ, Oliver Bonas đã phát triển từ việc quản lý các thiết kế của người khác để tạo ra sản phẩm của chúng tôi. </font><font style="vertical-align: inherit;">Đội ngũ các nhà thiết kế của chúng tôi hiện lấy tinh thần khám phá của Olly và đưa nó vào bộ sưu tập thời trang và đồ gia dụng của chúng tôi. </font><font style="vertical-align: inherit;">Chúng tôi được truyền cảm hứng từ thuật giả kim của thiết kế tuyệt vời và suy nghĩ mới mẻ và niềm tin rằng thiết kế có sức mạnh ảnh hưởng tích cực đến cảm giác của chúng tôi. </font><font style="vertical-align: inherit;">Thiết kế mới hạ cánh tại cửa hàng và trực tuyến mỗi tuần.</font></font></p>\r\n<p style="text-align: center;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ngày nay, OB đã phát triển tới hơn 70 cửa hàng trên khắp Vương quốc Anh và được điều hành bởi một đội ngũ tuyệt vời, người chia sẻ niềm đam mê của Olly trong việc đưa những ý tưởng mới vào cuộc sống. </font><font style="vertical-align: inherit;">Chúng tôi vui vẻ và làm theo các giá trị của công ty: Làm việc chăm chỉ, Chơi hết mình và tử tế.</font></font></p>\r\n</div>', '', 0, 1, 1544914800),
	(10, 4, 2, 1, 'Cảm hứng quà tặng ngày của cha với nhóm mua OB 3', 'cam-hung-qua-tang-ngay-cua-cha-voi-nhom-mua-ob-3', 'Tập hợp cảm hứng quà tặng ngày của cha với nhóm mua OB, trên blog hôm nay', 'Lorem Ipsum là gì?\r\nLorem Ipsum chỉ đơn giản là văn bản giả của ngành công nghiệp in ấn và sắp chữ. Lorem Ipsum đã trở thành văn bản giả tiêu chuẩn của ngành công nghiệp kể từ những năm 1500, khi một nhà in không rõ đã lấy một loại galley và xáo trộn nó để tạo ra một cuốn sách mẫu vật. Nó đã tồn tại không chỉ năm thế kỷ, mà còn là bước nhảy vọt trong việc sắp chữ điện tử, về cơ bản vẫn không thay đổi. Nó được phổ biến vào những năm 1960 với việc phát hành các tờ Letraset có chứa các đoạn Lorem Ipsum và gần đây với phần mềm xuất bản trên máy tính để bàn như Aldus PageMaker bao gồm các phiên bản của Lorem Ipsum.\r\n\r\ntại sao chúng ta sử dụng nó?\r\nMột thực tế lâu nay là người đọc sẽ bị phân tâm bởi nội dung có thể đọc được của một trang khi nhìn vào bố cục của nó. Điểm quan trọng của việc sử dụng Lorem Ipsum là nó có sự phân phối các chữ cái ít nhiều bình thường, trái ngược với việc sử dụng \'Nội dung ở đây, nội dung ở đây\', làm cho nó giống như tiếng Anh có thể đọc được. Nhiều gói xuất bản trên máy tính để bàn và trình chỉnh sửa trang web hiện sử dụng Lorem Ipsum làm văn bản mô hình mặc định của họ và tìm kiếm \'lorem ipsum\' sẽ phát hiện ra nhiều trang web vẫn còn trong giai đoạn trứng nước. Các phiên bản khác nhau đã phát triển qua nhiều năm, đôi khi là tình cờ, đôi khi có chủ đích (tiêm hài hước và tương tự).\r\n\r\n\r\nNó đến từ đâu?\r\nTrái với niềm tin phổ biến, Lorem Ipsum không chỉ đơn giản là văn bản ngẫu nhiên. Nó có nguồn gốc từ một phần của văn học Latin cổ điển từ năm 45 trước Công nguyên, làm cho nó hơn 2000 năm tuổi. Richard McClintock, một giáo sư Latin tại Hampden-Sydney College ở Virginia, đã tìm kiếm một trong những từ tiếng Latin khó hiểu hơn, từ một đoạn văn Lorem Ipsum, và đi qua các trích dẫn của từ này trong văn học cổ điển, đã phát hiện ra nguồn gốc không thể nghi ngờ. Lorem Ipsum xuất phát từ các phần 1.10.32 và 1.10.33 của "de Finibus Bonorum et Malorum" (The Extreme Extreme of Good and Evil) của Cicero, được viết vào năm 45 trước Công nguyên. Cuốn sách này là một chuyên luận về lý thuyết đạo đức, rất phổ biến trong thời Phục hưng. Dòng đầu tiên của Lorem Ipsum, "Lorem ipsum dolor sit amet ..", xuất phát từ một dòng trong phần 1.10.32.\r\n\r\nĐoạn tiêu chuẩn của Lorem Ipsum được sử dụng từ những năm 1500 được sao chép dưới đây cho những người quan tâm. Các phần 1.10.32 và 1.10.33 từ "de Finibus Bonorum et Malorum" của Cicero cũng được sao chép ở dạng nguyên bản chính xác, kèm theo các bản tiếng Anh từ bản dịch năm 1914 của H. Rackham.', 'assets/upload/users/admin/contents/1.jpg', 4, 1, 1544914800);
/*!40000 ALTER TABLE `zl_content` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_content_category
CREATE TABLE IF NOT EXISTS `zl_content_category` (
  `content_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`content_category_id`) USING BTREE,
  UNIQUE KEY `alias` (`alias`) USING BTREE,
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_content_category: ~10 rows (approximately)
/*!40000 ALTER TABLE `zl_content_category` DISABLE KEYS */;
REPLACE INTO `zl_content_category` (`content_category_id`, `content_category_gid`, `lang_id`, `title`, `alias`, `sorting`, `enabled`, `created_date`) VALUES
	(1, 1, 2, 'Products and Collections', 'products-and-collections', 1, 1, 1544482800),
	(2, 2, 2, 'Behind the scenes', 'Behind-the-scenes', 2, 1, 1544482800),
	(3, 3, 2, 'Out and about', 'out-and-about', 3, 1, 1544482800),
	(4, 4, 2, 'Inspiration', 'inspiration', 4, 1, 1544482800),
	(6, 1, 2, 'About us', 'about-us', 5, 1, 1544482800),
	(7, 1, 1, 'Sản phẩm và Bộ sưu tập', 'San-pham-va-bo-suu-tap', 1, 1, 1544482800),
	(8, 2, 1, 'Đằng sau hậu trường', 'Đang-sau-hau-truong', 2, 1, 1544482800),
	(9, 3, 1, 'Đây đó', 'day-do', 3, 1, 1544482800),
	(10, 4, 1, 'Cảm hứng', 'cam-hung', 4, 1, 1544482800),
	(11, 1, 1, 'Về chúng tôi', 've-chung-toi', 5, 1, 1544482800);
/*!40000 ALTER TABLE `zl_content_category` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_lang
CREATE TABLE IF NOT EXISTS `zl_lang` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `rate` float NOT NULL DEFAULT 0,
  `rate_usd` float NOT NULL DEFAULT 0,
  `logo` text NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT 0,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`lang_id`) USING BTREE,
  UNIQUE KEY `code_title` (`code`,`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_lang: ~4 rows (approximately)
/*!40000 ALTER TABLE `zl_lang` DISABLE KEYS */;
REPLACE INTO `zl_lang` (`lang_id`, `code`, `title`, `currency`, `rate`, `rate_usd`, `logo`, `enabled`, `default`, `created_date`) VALUES
	(1, 'vi', 'Tiếng Việt', 'vnđ', 1, 0.000043, 'assets/img/lang/vi.png', 1, 1, 1544482800),
	(2, 'en', 'English', 'usd', 0.000043, 1, 'assets/img/lang/en.png', 1, 0, 1544482800),
	(3, 'uk', 'UK GBP', '£', 0.000033, 1.29, 'assets/img/lang/uk.png', 1, 0, 1544482800),
	(4, 'ireland', 'IRELAND EUR', '€', 0.000039, 1.12, 'assets/img/lang/ireland.png', 1, 0, 1544482800);
/*!40000 ALTER TABLE `zl_lang` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_menu
CREATE TABLE IF NOT EXISTS `zl_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_gid` int(11) NOT NULL,
  `parent_gid` int(11) NOT NULL DEFAULT 0,
  `lang_id` int(11) NOT NULL DEFAULT 1,
  `title` varchar(100) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `function` varchar(50) NOT NULL,
  `params` varchar(50) NOT NULL,
  `link` text NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 - category, 1 - feature (content), 2 - header, 3 - footer',
  `sorting` int(11) NOT NULL,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_menu: ~110 rows (approximately)
/*!40000 ALTER TABLE `zl_menu` DISABLE KEYS */;
REPLACE INTO `zl_menu` (`menu_id`, `menu_gid`, `parent_gid`, `lang_id`, `title`, `controller`, `function`, `params`, `link`, `enabled`, `type`, `sorting`, `created_date`) VALUES
	(1, 1, 15, 1, 'HÀNG MỚI VỀ', '', '', '', '', 1, 0, 1, 1544482800),
	(2, 2, 4, 1, 'Thời trang', 'product', 'index', 'thoi-trang', '', 1, 0, 2, 1544482800),
	(3, 3, 4, 1, 'Kim cương', 'product', 'index', 'do-bao-kim', '', 1, 0, 3, 1544482800),
	(4, 4, 1, 1, 'Hàng mới', '', '', '', '', 1, 0, 4, 1544482800),
	(5, 5, 15, 1, 'THỜI TRANG', 'product', 'index', 'thoi-trang', '', 1, 0, 5, 1544482800),
	(6, 6, 5, 1, 'Danh mục', '', '', '', '', 1, 0, 6, 1544482800),
	(8, 8, 6, 1, 'Váy', 'product', 'index', 'vay', '', 1, 0, 8, 1544482800),
	(10, 10, 1, 1, 'QUÀ CHO CHA', 'product', 'index', 'qua-cho-cha', '', 1, 1, 10, 1544482800),
	(12, 12, 15, 1, 'ĐỒ KIM HOÀN', 'product', 'index', 'do-kim-hoan', '', 1, 0, 12, 1544482800),
	(13, 13, 15, 1, 'ĐỒ NHÀ', 'product', 'index', 'do-nha', '', 1, 0, 13, 1544482800),
	(14, 14, 15, 1, 'ĐỒ NỘI THẤT', 'product', 'index', 'do-noi-that', '', 1, 0, 14, 1544482800),
	(15, 15, 0, 1, 'HEADER', '', '', '', '', 1, 2, 15, 1544482800),
	(16, 16, 0, 1, 'FOOTER', '', '', '', '', 1, 3, 16, 1544482800),
	(17, 17, 19, 1, 'Liên hệ với chúng tôi', 'content', 'detail', 'lien-he-voi-chung-toi', '', 1, 0, 17, 1544482800),
	(18, 18, 19, 1, 'Vận chuyển', 'content', 'detail', 'van-chuyen', '', 1, 0, 18, 1544482800),
	(19, 19, 16, 1, 'Chăm sóc khách hàng', '', '', '', '', 1, 0, 19, 1544482800),
	(20, 20, 16, 1, 'Tài khoản', '', '', '', '', 1, 0, 20, 1544482800),
	(21, 21, 20, 1, 'Đơn hàng', 'access', 'myorders', '', '', 1, 0, 21, 1544482800),
	(22, 22, 20, 1, 'Địa chỉ giao hàng', 'access', 'addressbook', '', '', 1, 0, 22, 1544482800),
	(23, 23, 16, 1, 'Giới thiệu', '', '', '', '', 1, 0, 23, 1544482800),
	(24, 24, 23, 1, 'Cửa hàng', 'store', 'index', '', '', 1, 0, 24, 1544482800),
	(25, 25, 23, 1, 'Tin tức', 'content', 'index', '', '', 1, 0, 25, 1544482800),
	(26, 1, 15, 2, 'NEW IN', '', '', '', '', 1, 0, 1, 1544482800),
	(27, 4, 1, 2, 'New in', '', '', '', '', 1, 0, 4, 1544482800),
	(28, 3, 4, 2, 'Jewellry', 'product', 'index', 'jewellery', '', 1, 0, 3, 1544482800),
	(29, 2, 4, 2, 'Fashion', 'product', 'index', 'fashion', '', 1, 0, 2, 1544482800),
	(30, 30, 4, 2, 'Homeware', 'product', 'index', 'homeware', '', 1, 0, 26, 1544482800),
	(31, 30, 4, 1, 'Đồ gia dụng', 'product', 'index', 'homeware', '', 1, 0, 26, 1544482800),
	(32, 32, 4, 2, 'Furniture', 'product', 'index', 'furniture', '', 1, 0, 27, 1544482800),
	(33, 32, 4, 1, 'Nôi thất', 'product', 'index', 'furniture', '', 1, 0, 27, 1544482800),
	(34, 34, 4, 2, 'Gift', 'product', 'index', 'gift', '', 1, 0, 28, 1544482800),
	(35, 34, 4, 1, 'Quà tặng', 'product', 'index', 'gift', '', 1, 0, 28, 1544482800),
	(36, 36, 1, 1, 'Ngày của cha', 'product', 'index', 'fathers-day', '', 1, 0, 29, 1544482800),
	(37, 36, 1, 2, 'Father\'s Day', 'product', 'index', 'fathers-day', '', 1, 0, 29, 1544482800),
	(38, 38, 36, 1, 'Quà tặng cha', 'product', 'index', 'qua-cho-cha', '', 1, 0, 30, 1544482800),
	(39, 38, 36, 2, 'Father\'s Day Gift', 'product', 'index', 'fathers-day-gift', '', 1, 0, 30, 1544482800),
	(40, 40, 36, 1, 'Thiệp dành cho cha', 'product', 'index', 'thiep-danh-cho-cha', '', 1, 0, 31, 1544482800),
	(41, 40, 36, 2, 'Father\'s Day Card', 'product', 'index', 'fathers-day-cards', '', 1, 0, 31, 1544482800),
	(42, 15, 0, 2, 'HEADER', '', '', '', '', 1, 2, 15, 1544482800),
	(43, 16, 0, 2, 'FOOTER', '', '', '', '', 1, 3, 16, 1544482800),
	(45, 10, 1, 2, 'FATHER\'S GIFT', 'product', 'index', 'fathers-day-gift', '', 1, 1, 10, 1544482800),
	(46, 5, 15, 2, 'FASHION', 'product', 'index', 'fashion', '', 1, 0, 5, 0),
	(47, 8, 6, 2, 'Dresses', 'product', 'index', 'desses', '', 1, 0, 8, 0),
	(48, 48, 6, 1, 'Áo liền quần', 'product', 'index', 'ao-lien-quan', '', 1, 0, 32, 1544482800),
	(49, 48, 6, 2, 'Jumpsuits', 'product', 'index', 'jumpsuites', '', 1, 0, 32, 1544482800),
	(50, 50, 6, 1, 'Áo thun', 'product', 'index', 'ao-thun', '', 1, 0, 33, 1544482800),
	(51, 50, 6, 2, 'T-Shirt', 'product', 'index', 't-shirt', '', 1, 0, 33, 1544482800),
	(52, 52, 6, 1, 'Tất cả', 'product', 'index', 'fashion/all', '', 1, 0, 9, 1544482800),
	(53, 52, 6, 2, 'All', '', '', '', '', 1, 0, 9, 0),
	(54, 6, 5, 2, 'Category', '', '', '', '', 1, 0, 6, 0),
	(56, 56, 5, 1, 'Váy', '', '', '', '', 1, 0, 34, 1544482800),
	(57, 56, 5, 2, 'Dresses', '', '', '', '', 1, 0, 34, 1544482800),
	(58, 58, 56, 1, 'Váy midi', '', '', '', '', 1, 0, 35, 1544482800),
	(59, 58, 56, 2, 'Midi dresses', '', '', '', '', 1, 0, 35, 1544482800),
	(60, 60, 56, 1, 'Váy nhỏ', '', '', '', '', 1, 0, 36, 1544482800),
	(61, 60, 56, 2, 'Mini desses', '', '', '', '', 1, 0, 36, 1544482800),
	(62, 63, 56, 1, 'Váy đi nắng', '', '', '', '', 1, 0, 37, 1544482800),
	(64, 63, 56, 2, 'Sundesses', '', '', '', '', 1, 0, 37, 1544482800),
	(65, 65, 5, 1, 'Bộ sưu tập', '', '', '', '', 1, 0, 38, 1544482800),
	(66, 65, 5, 2, 'Collection', '', '', '', '', 1, 0, 38, 1544482800),
	(67, 67, 65, 1, 'Lông thú', '', '', '', '', 1, 0, 39, 1544482800),
	(68, 67, 65, 2, 'Animal Print', '', '', '', '', 1, 0, 39, 1544482800),
	(69, 69, 65, 1, 'Đồ đi biển', '', '', '', '', 1, 0, 40, 1544482800),
	(70, 69, 65, 2, 'Beachwear', '', '', '', '', 1, 0, 40, 1544482800),
	(71, 71, 5, 1, 'Cánh hoa', 'product', 'index', 'canh-hoa', '', 1, 1, 41, 1544482800),
	(72, 71, 5, 2, 'Florals', 'product', 'index', 'florals', '', 1, 1, 41, 1544482800),
	(73, 73, 5, 1, 'Đồ bảo hộ lao động', 'product', 'index', 'do-bao-ho-lao-dong', '', 1, 1, 42, 1544482800),
	(74, 73, 5, 2, 'Workwear', 'product', 'index', 'workwear', '', 1, 1, 42, 1544482800),
	(75, 12, 15, 2, 'JEWELLERY', 'product', 'index', 'jewellery', '', 1, 0, 12, 0),
	(76, 76, 12, 1, 'Danh mục', '', '', '', '', 1, 0, 43, 1544482800),
	(77, 76, 12, 2, 'Category', '', '', '', '', 1, 0, 43, 1544482800),
	(78, 78, 76, 1, 'Mới', 'product', 'index', 'do-kim-hoan/new', '', 1, 0, 44, 1544482800),
	(79, 78, 76, 2, 'New', 'product', 'index', 'jewellery/new', '', 1, 0, 44, 1544482800),
	(80, 80, 76, 1, 'Bông tai', 'product', 'index', 'bong-tai', '', 1, 0, 45, 1544482800),
	(81, 80, 76, 2, 'Earrings', 'product', 'index', 'earrings', '', 1, 0, 45, 1544482800),
	(82, 82, 76, 1, 'Nhẫn', 'product', 'index', 'nhan', '', 1, 0, 46, 1544482800),
	(83, 82, 76, 2, 'Rings', 'product', 'index', 'rings', '', 1, 0, 46, 1544482800),
	(84, 84, 76, 1, 'Trâm cài', 'product', 'index', 'tram-cai', '', 1, 0, 47, 1544482800),
	(85, 84, 76, 2, 'Brooches', 'product', 'index', 'brooches', '', 1, 0, 47, 1544482800),
	(86, 86, 76, 1, 'Tất cả', 'product', 'index', 'do-kim-hoan/all', '', 1, 0, 48, 1544482800),
	(87, 86, 76, 2, 'All', 'product', 'index', 'jewellery/all', '', 1, 0, 48, 1544482800),
	(88, 88, 12, 1, 'Kiểu dáng', '', '', '', '', 1, 0, 49, 1544482800),
	(89, 88, 12, 2, 'Style', '', '', '', '', 1, 0, 49, 1544482800),
	(90, 90, 88, 1, 'Đồ hoàn kim bạc', 'product', 'index', 'do-hoan-kim-bac', '', 1, 0, 50, 1544482800),
	(92, 92, 88, 1, 'Đồ hoàn kim ngọc', 'product', 'index', 'do-hoan-kim-ngoc', '', 1, 0, 51, 1544482800),
	(93, 92, 88, 2, 'Pearl Jewellery', 'product', 'index', 'pearl-jewellery', '', 1, 0, 51, 1544482800),
	(94, 94, 88, 1, 'Đồ trang sức nhựa', 'undefined', 'undefined', 'undefined', '', 1, 0, 52, 0),
	(95, 94, 88, 2, 'Resin Jewellery', 'undefined', 'undefined', 'undefined', '', 1, 0, 52, 0),
	(96, 96, 12, 1, 'Nhẫn', 'product', 'index', 'nhan', '', 1, 1, 53, 0),
	(97, 96, 12, 2, 'Rings', 'product', 'index', 'rings', '', 1, 1, 53, 0),
	(98, 13, 15, 2, 'HOMEWEAR', 'product', 'index', 'homewear', '', 1, 0, 13, 0),
	(99, 14, 15, 2, 'FURNITURE', 'product', 'index', 'furniture', '', 1, 0, 14, 0),
	(100, 100, 15, 1, 'QUÀ TẶNG', 'product', 'index', 'qua-tang', '', 1, 0, 54, 0),
	(101, 100, 15, 2, 'GIFT', 'product', 'index', 'gift', '', 1, 0, 54, 0),
	(102, 19, 16, 2, 'Customer services', '', '', '', '', 1, 0, 19, 0),
	(103, 17, 19, 2, 'Contact us', 'content', 'detail', 'contact-us', '', 1, 0, 17, 0),
	(104, 18, 19, 2, 'Delivery', 'content', 'detail', 'delivery', '', 1, 0, 18, 0),
	(105, 105, 19, 1, 'Trung tâm hỗ trợ', 'content', 'detail', 'trung-tam-ho-tro', '', 1, 0, 57, 0),
	(106, 105, 19, 2, 'Help center', 'content', 'detail', 'help-center', '', 1, 0, 57, 0),
	(107, 20, 16, 2, 'My account', '', '', '', '', 1, 0, 0, 0),
	(108, 21, 20, 2, 'Orders', 'access', 'myorders', '', '', 1, 0, 21, 0),
	(109, 22, 20, 2, 'Address', 'access', 'addressbook', '', '', 1, 0, 22, 0),
	(110, 110, 20, 1, 'Thông tin tài khoản', 'access', 'myaccount', '', '', 1, 0, 55, 0),
	(111, 110, 20, 2, 'Account', 'access', 'myaccount', '', '', 1, 0, 55, 0),
	(112, 23, 16, 2, 'About us', '', '', '', '', 1, 0, 23, 0),
	(113, 24, 23, 2, 'Store locator', 'store', 'index', '', '', 1, 0, 24, 0),
	(114, 25, 23, 2, 'Blog', 'content', 'index', '', '', 1, 0, 25, 0),
	(115, 115, 23, 1, 'Giới thiệu', 'content', 'detail', 'gioi-thieu', '', 1, 0, 56, 0),
	(116, 115, 23, 2, 'About us', 'content', 'detail', 'about-us', '', 1, 0, 56, 0),
	(127, 90, 88, 2, 'Silver Jewellery', 'product', 'index', 'silver-jewellery', '', 1, 0, 50, 0);
/*!40000 ALTER TABLE `zl_menu` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_places
CREATE TABLE IF NOT EXISTS `zl_places` (
  `places_id` int(11) NOT NULL AUTO_INCREMENT,
  `places_gid` int(11) NOT NULL,
  `parent_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT 1,
  `title` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 - country, 1 - tỉnh / tp trưc thuộc tw, 2 - quận / huyện, 3 - phường / xã',
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`places_id`) USING BTREE,
  UNIQUE KEY `title` (`title`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_places: ~8 rows (approximately)
/*!40000 ALTER TABLE `zl_places` DISABLE KEYS */;
REPLACE INTO `zl_places` (`places_id`, `places_gid`, `parent_gid`, `lang_id`, `title`, `alias`, `type`, `enabled`, `created_date`) VALUES
	(1, 1, 0, 1, 'Việt Nam', 'viet-nam', 0, 1, 1544482800),
	(2, 2, 1, 1, 'Thành Phố Hà Nội', 'thanh-pho-ha-noi', 1, 1, 1544482800),
	(3, 3, 2, 1, 'Quận Ba Đình', 'quan-ba-dinh', 2, 1, 1544482800),
	(4, 4, 3, 1, 'Phường Phúc Xá', 'phuong-phuc-xa', 3, 1, 1544482800),
	(5, 5, 3, 1, 'Phường Trúc Bạch', 'phuong-truc-bach', 3, 1, 1544482800),
	(6, 6, 2, 1, 'Quận Hoàn Kiếm', 'quan-hoan-kiem', 2, 1, 1544482800),
	(7, 7, 6, 1, 'Phường Phúc Tân', 'phuong-phuc-tan', 3, 1, 1544482800),
	(8, 8, 6, 1, 'Phường Đồng Xuân', 'phuong-dong-xuan', 3, 1, 1544482800);
/*!40000 ALTER TABLE `zl_places` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_product
CREATE TABLE IF NOT EXISTS `zl_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_gid` int(11) NOT NULL,
  `product_category_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT 1,
  `title` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `bg_color` varchar(100) NOT NULL DEFAULT '#ccefcc',
  `intro_text` text NOT NULL,
  `tra_pay` text NOT NULL,
  `ref_mai` text NOT NULL,
  `full_text` text NOT NULL,
  `price_vn` float NOT NULL DEFAULT 0,
  `price_vn_sale` float NOT NULL DEFAULT 0,
  `warehouse` int(11) NOT NULL,
  `logo` text NOT NULL,
  `slide1` text NOT NULL,
  `slide2` text NOT NULL,
  `color_gid` varchar(100) NOT NULL DEFAULT '0',
  `material_gid` varchar(100) NOT NULL DEFAULT '0',
  `size_gid` varchar(100) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL,
  `sell_count` int(11) NOT NULL,
  `is_new` int(11) NOT NULL DEFAULT 0,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`product_id`) USING BTREE,
  UNIQUE KEY `name_alias` (`title`,`alias`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_product: ~26 rows (approximately)
/*!40000 ALTER TABLE `zl_product` DISABLE KEYS */;
REPLACE INTO `zl_product` (`product_id`, `product_gid`, `product_category_gid`, `lang_id`, `title`, `alias`, `code`, `bg_color`, `intro_text`, `tra_pay`, `ref_mai`, `full_text`, `price_vn`, `price_vn_sale`, `warehouse`, `logo`, `slide1`, `slide2`, `color_gid`, `material_gid`, `size_gid`, `sorting`, `sell_count`, `is_new`, `enabled`, `created_date`) VALUES
	(1, 1, 63, 2, 'Marvin Monkey Charm Silver Chain Bracelet', 'marvin-monkey-charm-silver-chain-bracelet', '1240108', '#EBE3DA', 'On a fine adjustable chain with a spring ring clasp, this silver 925 bracelet is embellished with a delicate climbing monkey charm for a playful addition to your wrist. Match with other items from our Marvin collection to complete your look.', '<p>Transfer and Payment</p>', '<p>Refun and Maintance</p>', 'Here is list of features:<br>\r\n1. Beautifull<br>\r\n2. Modern<br>\r\n3. Youghth<br>', 1200000, 0, 10, 'assets/upload/users/admin/products/pro_1.jpg', 'assets/upload/users/admin/products/pro_1.jpg||assets/upload/users/admin/products/pro_2.jpg', 'assets/upload/users/admin/products/pro_1.jpg||assets/upload/users/admin/products/pro_2.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 1, 0, 0, 1, 1544482800),
	(2, 2, 65, 2, 'Monstera Leaf Silver Bracelet', 'monstera-leaf-silver-bracelet', '1259964', '#EBE3DA', 'Wear your plants on your sleeve with this Monstera Leaf Silver Bracelet. With a delicate monstera leaf charm on an adjustable fine chain, it is completed with a spring ring clasp.', '<p>Transfer and Payment</p>', '<p>Refun and Maintance</p>', 'Here is list of features:<br>\r\n1. Beautifull<br>\r\n2. Modern<br>\r\n3. Youghth<br>', 2300000, 0, 5, 'assets/upload/users/admin/products/pro_2.jpg', 'assets/upload/users/admin/products/pro_2.jpg', 'assets/upload/users/admin/products/pro_2.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 2, 0, 0, 1, 1544483800),
	(3, 3, 63, 2, 'Reeve Green Cubic Zirconia Hoop Earrings', 'reeve-green-cubic-zirconia-hoop-earrings', '1241228', '#EBE3DA', 'Add a dazzling touch to your hoops with these small earrings, adorned with cubic zirconia inlays in tonal greens, completed with stud backs.', '<p>Transfer and Payment</p>', '<p>Refun and Maintance</p>', 'Here is list of features:<br>\r\n1. Beautifull<br>\r\n2. Modern<br>\r\n3. Youghth<br>', 890000, 0, 22, 'assets/upload/users/admin/products/pro_3.jpg', 'assets/upload/users/admin/products/pro_3.jpg||assets/upload/users/admin/products/pro_3_1.jpg', 'assets/upload/users/admin/products/pro_3.jpg||assets/upload/users/admin/products/pro_3_1.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 3, 0, 0, 1, 1544484800),
	(4, 4, 65, 2, 'Alex Yellow Tapered Handle Tote Bag', 'alex-yellow-tapered-handle-tote-bag', '1236842', '#EBE3DA', 'Combining a smooth sunny yellow finish with a slouchy profile, this oversized tote bag is a spacious everyday bag, featuring one main compartment with a mustard-toned bonded lining and a magnetic button fastening. This bag is completed with double shoulder straps and a removable inner pouch with gold-toned hardware.', '<p>Transfer and Payment</p>', '<p>Refun and Maintance</p>', 'Here is list of features:<br>\r\n1. Beautifull<br>\r\n2. Modern<br>\r\n3. Youghth<br>', 3250000, 0, 76, 'assets/upload/users/admin/products/pro_4.jpg', 'assets/upload/users/admin/products/pro_4.jpg||assets/upload/users/admin/products/pro_4.jpg', 'assets/upload/users/admin/products/pro_4.jpg||assets/upload/users/admin/products/pro_4.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 4, 0, 0, 1, 1544487800),
	(5, 5, 63, 2, 'Silver Lyca Turquoise Earrings', 'silver-lyca-turquoise-earrings', '982504', '#EBE3DA', 'Silver drop earrings featuring a silver and turquoise circle.', '<p>Transfer and Payment</p>', '<p>Refun and Maintance</p>', 'Danh sách các tính năng:<br>\r\n1. Mẫu mã đẹp<br>\r\n2. Hiện tại<br>\r\n3. Trẻ trung<br>', 1100000, 0, 12, 'assets/upload/users/admin/products/pro_5.jpg', 'assets/upload/users/admin/products/pro_5.jpg', 'assets/upload/users/admin/products/pro_5.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 3, 0, 0, 1, 1544486800),
	(6, 6, 65, 2, 'Naos Trio Star Creeper Earrings', 'naos-trio-star-creeper-earrings', '1141801', '#EBE3DA', 'Make your earrings the star of the show with the Naos Trio Star Creeper Earrings. These gold-plated stud earrings feature a row of stars in ascending size that are designed to sit up the lobe.', '<p>Transfer and Payment</p>', '<p>Refun and Maintance</p>', 'Here is list of features:<br>\r\n1. Beautifull<br>\r\n2. Modern<br>\r\n3. Youghth<br>', 2200000, 0, 32, 'assets/upload/users/admin/products/pro_6.jpg', 'assets/upload/users/admin/products/pro_6.jpg', 'assets/upload/users/admin/products/pro_6.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 3, 0, 0, 1, 1544487800),
	(7, 7, 57, 1, 'Gingham kiểm tra chèn áo thun midi trắng', 'gingham-kiem-tra-chen-ao-thun-midi-trang', '3242343', '#EBE3DA', 'Hãy kiểm tra tủ quần áo ngoài nhiệm vụ của bạn với chiếc váy midi kiểu áo phông trắng này, với các tấm gingham màu vàng ở hai bên. Với tay áo ngắn và đường viền cổ tròn, chiếc váy này có hình chữ A nhẹ nhàng tạo cảm giác bay bổng.', '<p>Vận chuyển và thanh toán</p>', '<p>Đổi trả và vận hành</p>', 'Danh sách các tính năng:<br>\r\n1. Mẫu mã đẹp<br>\r\n2. Hiện tại<br>\r\n3. Trẻ trung<br>', 3200000, 0, 1, 'assets/upload/users/admin/products/midi.jpg', 'assets/upload/users/admin/products/midi.jpg||assets/upload/users/admin/products/midi_2.jpg', 'assets/upload/users/admin/products/midi_3.jpg||assets/upload/users/admin/products/midi_4.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 5, 0, 0, 1, 1544487800),
	(8, 7, 57, 2, 'Gingham Checked Insert White Midi T-Shirt Dress', 'gingham-checked-insert-white-midi-t-shirt-dress', '3242343', '#EBE3DA', 'Get your off-duty wardrobe in check with this white t-shirt style midi dress, with yellow gingham panels at the sides. With short sleeves and a round neckline, this dress has a gentle A-line silhouette for a floaty feel.', '<p>Transfer and Payment</p>', '<p>Refun and Maintance</p>', 'Here is list of features:<br>\r\n1. Beautifull<br>\r\n2. Modern<br>\r\n3. Youghth<br>', 3200000, 0, 1, 'assets/upload/users/admin/products/midi.jpg', 'assets/upload/users/admin/products/midi.jpg||assets/upload/users/admin/products/midi_2.jpg', 'assets/upload/users/admin/products/midi_3.jpg||assets/upload/users/admin/products/midi_4.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 5, 0, 0, 1, 1544487800),
	(9, 9, 59, 1, 'Váy mini màu vàng', 'vay-mini-mau-vang', '5465465', '#78f58e', '<p>Kết hợp màu vàng chanh với một hình bóng có cấu trúc, chiếc váy mini này được thiết kế chi tiết với nếp gấp tinh tế ở phía trước và đường viền cổ vuông. Được buộc chặt bằng khóa kéo ở phía sau, chiếc váy được hoàn thành với túi bên hông và dây đeo vai rộng, và có thể được mang với dép có gót hoặc bằng phẳng để mặc lên hoặc xuống.</p>', '<p>Vận chuyển và thanh toán</p>', '<p>Đổi trả và vận hành</p>', '<p>Danh sách các tính năng:<br>1. Mẫu mã đẹp<br>2. Hiện tại<br>3. Trẻ trung<br>&nbsp;</p>', 5600000, 0, 22, 'assets/upload/users/admin/products/midi_5.jpg', 'assets/upload/users/admin/products/midi_5.jpg||assets/upload/users/admin/products/midi_6.jpg', 'assets/upload/users/admin/products/midi_7.jpg||assets/upload/users/admin/products/midi_8.jpg', '2,11,12', '4,7', '5,9,10', 6, 0, 0, 1, 1544487800),
	(10, 9, 59, 2, 'Structured Yellow Mini Dress', 'structured-yellow-mini-dress', '5465465', '#78f58e', '<p>Pairing lemon yellow with a structured silhouette, this mini dress is detailed with subtle pleating at the front and a square neckline. Fastened with a zip fastening at the back, the dress is completed with side pockets and wide shoulder straps, and can be worn with heeled or flat sandals to dress up or down.</p>', '<p>Transfer and Payment</p>', '<p>Refun and Maintance</p>', '<p>Here is list of features:<br>1. Beautifull<br>2. Modern<br>3. Youghth<br>&nbsp;</p>', 5600000, 0, 22, 'assets/upload/users/admin/products/midi_5.jpg', 'assets/upload/users/admin/products/midi_5.jpg||assets/upload/users/admin/products/midi_6.jpg', 'assets/upload/users/admin/products/midi_7.jpg||assets/upload/users/admin/products/midi_8.jpg', '2,11,12', '4,7', '5,9,10', 6, 0, 0, 1, 1544487800),
	(11, 11, 59, 1, 'Cassie nhiều màu sọc dệt kim nhỏ', 'Cassie-nhieu-mau-soc-det-kim-nho', '4544554', '#EBE3DA', 'Trong một điểm nhấn đan tốt, chiếc váy dài nhỏ này sẽ giữ cho bạn mát mẻ và đầy màu sắc, cắt theo kiểu áo phông với kiểu dáng thẳng và dáng dài, với tay áo ngắn và cổ tròn. Với các sọc màu vàng đậm, hồng, đỏ và xanh lá cây, nó được hoàn thành với đan trắng ở đường viền cổ áo, cổ tay áo và viền dưới.', '<p>Vận chuyển và thanh toán</p>', '<p>Đổi trả và vận hành</p>', 'Danh sách các tính năng:<br>\r\n1. Mẫu mã đẹp<br>\r\n2. Hiện tại<br>\r\n3. Trẻ trung<br>', 1200000, 0, 1, 'assets/upload/users/admin/products/midi_9.jpg', 'assets/upload/users/admin/products/midi_9.jpg||assets/upload/users/admin/products/midi_10.jpg', 'assets/upload/users/admin/products/midi_11.jpg||assets/upload/users/admin/products/midi_12.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 7, 0, 0, 1, 1544487800),
	(12, 11, 59, 2, 'Cassie Multicoloured Stripe Knitted Mini T-Shirt Dress', 'cassie-Multicoloured-Stripe-Knitted-Mini-T-Shirt-Dress', '4544554', '#EBE3DA', 'In a fine pointelle knit, this mini length dress will keep you cool and colourful, cut in a t-shirt style with a straight fit and a sheer look, with short sleeves and a round neck. With stripes of bold yellow, pink, red and green, it is completed with white knit at the neckline, sleeve cuffs and bottom hem.', '<p>Transfer and Payment</p>', '<p>Refun and Maintance</p>', 'Here is list of features:<br>\r\n1. Beautifull<br>\r\n2. Modern<br>\r\n3. Youghth<br>', 1200000, 0, 1, 'assets/upload/users/admin/products/midi_9.jpg', 'assets/upload/users/admin/products/midi_9.jpg||assets/upload/users/admin/products/midi_10.jpg', 'assets/upload/users/admin/products/midi_11.jpg||assets/upload/users/admin/products/midi_12.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 7, 0, 0, 1, 1544487800),
	(13, 13, 42, 1, 'Sundress bãi biển sọc Lilac', 'Sundress-bai-bien-soc-Lilac', '5454354', '#EBE3DA', 'Ngồi phía trên đầu gối, chiếc lundac lilac này được trang trí với các sọc màu hồng tương phản cho một bổ sung đầy màu sắc cho tủ quần áo thời tiết ấm áp của bạn. Với dây đeo vai mỏng, chiếc váy mini này được hoàn thành với một chi tiết thô ráp ở đường viền cổ áo và tách ra ở viền bên hông.', '<p>Vận chuyển và thanh toán</p>', '<p>Đổi trả và vận hành</p>', 'Danh sách các tính năng:<br>\r\n1. Mẫu mã đẹp<br>\r\n2. Hiện tại<br>\r\n3. Trẻ trung<br>', 4500000, 3300000, 1, 'assets/upload/users/admin/products/mini_1.jpg', 'assets/upload/users/admin/products/mini_1.jpg||assets/upload/users/admin/products/mini_2.jpg', 'assets/upload/users/admin/products/mini_3.jpg||assets/upload/users/admin/products/mini_4.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 8, 0, 0, 1, 1544487800),
	(14, 13, 42, 2, 'Stripe Lilac Beach Sundress', 'Stripe-Lilac-Beach-Sundress', '5454354', '#EBE3DA', 'Sitting above the knee, this lilac sundress is decorated with contrasting pink stripes for a colourful addition to your warm-weather wardrobe. Featuring thin shoulder straps, this mini dress is completed with a ruched detailing at the neckline and splits in the side hem.', '<p>Transfer and Payment</p>', '<p>Refun and Maintance</p>', 'Here is list of features:<br>\r\n1. Beautifull<br>\r\n2. Modern<br>\r\n3. Youghth<br>', 3300000, 0, 1, 'assets/upload/users/admin/products/mini_1.jpg', 'assets/upload/users/admin/products/mini_1.jpg||assets/upload/users/admin/products/mini_2.jpg', 'assets/upload/users/admin/products/mini_3.jpg||assets/upload/users/admin/products/mini_4.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 8, 0, 0, 1, 1544487800),
	(15, 15, 42, 1, 'Váy Ditsy in hoa đỏ', 'Vay-Ditsy-in-hoa-do', '6546556', '#EBE3DA', 'Một họa tiết hoa rực rỡ với màu hồng và đen trang trí cho chiếc váy đỏ rực rỡ này, chi tiết với sự tập hợp ở eo và tay áo dài một nửa với ruching. Nó có một cổ tròn và một hình bóng bay lơ lửng, thanh lịch, được bù đắp bằng cách tách chân và nhúng gấu ở phía sau, buộc bằng một khóa kéo ở phía sau.', '<p>Vận chuyển và thanh toán</p>', '<p>Đổi trả và vận hành</p>', 'Danh sách các tính năng:<br>\r\n1. Mẫu mã đẹp<br>\r\n2. Hiện tại<br>\r\n3. Trẻ trung<br>', 15000000, 0, 1, 'assets/upload/users/admin/products/mini_5.jpg', 'assets/upload/users/admin/products/mini_5.jpg|assetns/upload/users/admin/products/mini_6.jpg', 'assets/upload/users/admin/products/mini_7.jpg|assets/upload/users/admin/products/mini_8.jpg', '1', '3', '5', 9, 0, 0, 1, 1544487800),
	(16, 15, 42, 2, 'Ditsy Floral Print Red Midi Dress', 'Ditsy-Floral-Print-Red-Midi-Dress', '6546556', '#EBE3DA', 'A ditsy floral print in bright pink and black decorates this vivid red dress, detailed with gathering at the waist and half length sleeves with ruching. It has a round neck and a floaty, elegant silhouette, offset by a leg split and dip hem at the back, fastening with a zip at the back.', '<p>Transfer and Payment</p>', '<p>Refun and Maintance</p>', 'Here is list of features:<br>\r\n1. Beautifull<br>\r\n2. Modern<br>\r\n3. Youghth<br>', 15000000, 0, 1, 'assets/upload/users/admin/products/mini_5.jpg', 'assets/upload/users/admin/products/mini_5.jpg|assetns/upload/users/admin/products/mini_6.jpg', 'assets/upload/users/admin/products/mini_7.jpg|assets/upload/users/admin/products/mini_8.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 9, 0, 0, 1, 1544487800),
	(17, 17, 42, 1, 'Váy Ditsy in hoa màu đỏ và xanh', 'Vay-Ditsy-in-hoa-mau-do-va-xanh', '1292122', '#EBE3DA', 'Giữ cho cánh hoa của bạn tươi mới với chiếc váy midi hai tông màu này, trong một kiểu dáng thẳng có thiết kế hoa với màu đen, trắng và hồng nổi bật trên nền màu đỏ rực rỡ và màu xanh cam rực rỡ. Chiếc váy này có tay áo dài một nửa với chi tiết ruched và một chân tách ra một bên, có đường viền cổ tròn, và được hoàn thành với một khóa kéo ở phía sau.', '<p>Vận chuyển và thanh toán</p>', '<p>Đổi trả và vận hành</p>', 'Danh sách các tính năng:<br>\r\n1. Mẫu mã đẹp<br>\r\n2. Hiện tại<br>\r\n3. Trẻ trung<br>', 4500000, 0, 2, 'assets/upload/users/admin/products/mini_9.jpg', 'assets/upload/users/admin/products/mini_9.jpg||assets/upload/users/admin/products/mini_10.jpg', 'assets/upload/users/admin/products/mini_11.jpg||assets/upload/users/admin/products/mini_12.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 10, 0, 0, 1, 1544487800),
	(18, 17, 42, 2, 'Ditsy Floral Print Red & Blue Midi Dress', 'Ditsy-Floral-Print-Red-Blue-Midi-Dress', '1292122', '#EBE3DA', 'Keep your florals fresh with this two-tone midi dress, in a straight fit featuring flower designs with black, white, and pink highlights against vibrant red and bright blue orange polka-dotted backgrounds. This dress features half-length sleeves with ruched detailing and a leg split up one side, has a round neckline, and is completed with a zip fastening at the back.', '<p>Transfer and Payment</p>', '<p>Refun and Maintance</p>', 'Here is list of features:<br>\r\n1. Beautifull<br>\r\n2. Modern<br>\r\n3. Youghth<br>', 4500000, 0, 2, 'assets/upload/users/admin/products/mini_9.jpg', 'assets/upload/users/admin/products/mini_9.jpg||assets/upload/users/admin/products/mini_10.jpg', 'assets/upload/users/admin/products/mini_11.jpg||assets/upload/users/admin/products/mini_12.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 10, 0, 0, 1, 1544487800),
	(19, 19, 44, 1, 'Jumpsuit thắt eo màu cam', 'Jumpsuit-that-eo-mau-cam', '9433243', '#EBE3DA', 'Được cắt theo kiểu cắt, bộ jumpsuit tông màu cam cháy này có thiết kế vừa vặn với một chi tiết buộc vải ở thắt lưng cho một kết thúc thanh lịch. Tay áo ngắn phát triển và ruching co giãn ở thắt lưng làm cho bộ jumpsuit này vừa vặn thoải mái, trong khi đường viền cổ tròn được hoàn thành với một chi tiết mở ở phía sau và cài nút.', '<p>Vận chuyển và thanh toán</p>', '<p>Đổi trả và vận hành</p>', 'Danh sách các tính năng:<br>\r\n1. Mẫu mã đẹp<br>\r\n2. Hiện tại<br>\r\n3. Trẻ trung<br>', 7800000, 0, 1, 'assets/upload/users/admin/products/jumpsuits_1.jpg', 'assets/upload/users/admin/products/jumpsuits_1.jpg||assets/upload/users/admin/products/jumpsuits_2.jpg', 'assets/upload/users/admin/products/jumpsuits_3.jpg||assets/upload/users/admin/products/jumpsuits_4.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 11, 0, 0, 1, 1544487800),
	(20, 19, 44, 2, 'Tie Waist Orange Jumpsuit', 'Tie-Waist-Orange -Jumpsuit', '9433243', '#EBE3DA', 'Cut in a cropped style, this burnt orange-toned jumpsuit features a loose fitting design with a fabric tie detailing at the waist for an elegant finish. Short grown-on sleeves and elasticated ruching in the back waist makes this jumpsuit a comfortable fit, whilst the round neckline is completed with an open detailing in the back and a button fastening.', '<p>Transfer and Payment</p>', '<p>Refun and Maintance</p>', 'Here is list of features:<br>\r\n1. Beautifull<br>\r\n2. Modern<br>\r\n3. Youghth<br>', 7800000, 0, 1, 'assets/upload/users/admin/products/jumpsuits_1.jpg', 'assets/upload/users/admin/products/jumpsuits_1.jpg||assets/upload/users/admin/products/jumpsuits_2.jpg', 'assets/upload/users/admin/products/jumpsuits_3.jpg||assets/upload/users/admin/products/jumpsuits_4.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 11, 0, 0, 1, 1544487800),
	(21, 1, 63, 1, 'Marvin Monkey Chuỗi vòng đeo tay bằng bạc', 'Marvin-monkey-chuoi-vong-deo-tay-bang-bac', '1240108', '#EBE3DA', 'Trên một dây chuyền có thể điều chỉnh tốt với một chiếc vòng lò xo, chiếc vòng tay bằng bạc 925 này được tô điểm bằng một chiếc bùa khỉ leo tinh tế để thêm phần tinh nghịch cho cổ tay của bạn. Phù hợp với các mặt hàng khác từ bộ sưu tập Marvin của chúng tôi để hoàn thành giao diện của bạn.', '<p>Vận chuyển và thanh toán</p>', '<p>Đổi trả và vận hành</p>', 'Danh sách các tính năng:<br>\r\n1. Mẫu mã đẹp<br>\r\n2. Hiện tại<br>\r\n3. Trẻ trung<br>', 1200000, 0, 10, 'assets/upload/users/admin/products/pro_1.jpg', 'assets/upload/users/admin/products/pro_1.jpg||assets/upload/users/admin/products/pro_2.jpg', 'assets/upload/users/admin/products/pro_1.jpg||assets/upload/users/admin/products/pro_2.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 1, 0, 0, 1, 1544482800),
	(22, 2, 65, 1, 'Vòng tay bạc lá Monstera', 'Vong-tay-bac-la-Monstera', '1259964', '#EBE3DA', 'Đeo cây của bạn trên tay áo của bạn với Vòng tay bạc lá Monstera này. Với một lá bùa monstera tinh tế trên một chuỗi tốt có thể điều chỉnh, nó được hoàn thành với một vòng kẹp mùa xuân.', '<p>Vận chuyển và thanh toán</p>', '<p>Đổi trả và vận hành</p>', 'Danh sách các tính năng:<br>\r\n1. Mẫu mã đẹp<br>\r\n2. Hiện tại<br>\r\n3. Trẻ trung<br>', 2300000, 0, 5, 'assets/upload/users/admin/products/pro_2.jpg', 'assets/upload/users/admin/products/pro_2.jpg', 'assets/upload/users/admin/products/pro_2.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 2, 0, 0, 1, 1544487800),
	(23, 3, 63, 1, 'Bông tai hoop Zirconia xanh lục', 'Bong-tai-hoop-Zirconia-xanh-luc', '1241228', '#EBE3DA', 'Thêm một điểm nhấn rực rỡ cho các vòng của bạn với những đôi bông tai nhỏ này, được trang trí bằng các khối zirconia khối trong màu xanh lá cây, hoàn thành với mặt sau stud.', '<p>Vận chuyển và thanh toán</p>', '<p>Đổi trả và vận hành</p>', 'Danh sách các tính năng:<br>\r\n1. Mẫu mã đẹp<br>\r\n2. Hiện tại<br>\r\n3. Trẻ trung<br>', 890000, 0, 22, 'assets/upload/users/admin/products/pro_3.jpg', 'assets/upload/users/admin/products/pro_3.jpg||assets/upload/users/admin/products/pro_3_1.jpg', 'assets/upload/users/admin/products/pro_3.jpg||assets/upload/users/admin/products/pro_3_1.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 3, 0, 0, 1, 1544487800),
	(24, 4, 65, 1, 'Alex Tote thon xử lý túi tote', 'Alex-Tote-thon-xu-ly-tui-tote', '1236842', '#EBE3DA', 'Kết hợp một kết thúc màu vàng nắng mịn màng với một hồ sơ trơn, túi tote quá khổ này là một túi hàng ngày rộng rãi, có một ngăn chính với một lớp lót liên kết mù tạt và nút từ tính. Chiếc túi này được hoàn thành với dây đeo vai đôi và túi bên trong có thể tháo rời với phần cứng được mạ vàng.', '<p>Vận chuyển và thanh toán</p>', '<p>Đổi trả và vận hành</p>', 'Danh sách các tính năng:<br>\r\n1. Mẫu mã đẹp<br>\r\n2. Hiện tại<br>\r\n3. Trẻ trung<br>', 3250000, 0, 76, 'assets/upload/users/admin/products/pro_4.jpg', 'assets/upload/users/admin/products/pro_4.jpg||assets/upload/users/admin/products/pro_4.jpg', 'assets/upload/users/admin/products/pro_4.jpg||assets/upload/users/admin/products/pro_4.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 4, 0, 0, 1, 1544487800),
	(25, 5, 63, 1, 'Bông tai ngọc lam Lycra', 'Bong-tai-ngoc-lam-Lycra', '982504', '#EBE3DA', 'Hoa tai thả bạc có hình tròn màu bạc và màu ngọc lam.', '<p>Vận chuyển và thanh toán</p>', '<p>Đổi trả và vận hành</p>', 'Here is list of features:<br>\r\n1. Beautifull<br>\r\n2. Modern<br>\r\n3. Youghth<br>', 1100000, 0, 12, 'assets/upload/users/admin/products/pro_5.jpg', 'assets/upload/users/admin/products/pro_5.jpg', 'assets/upload/users/admin/products/pro_5.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 3, 0, 0, 1, 1544487800),
	(26, 6, 65, 1, 'Bông tai dây leo Naos Trio Star', 'Bong-tai-day-leo-Naos-Trio-Star', '1141801', '#EBE3DA', 'Biến đôi bông tai của bạn thành ngôi sao của chương trình với Bông tai dây leo Naos Trio Star. Những đôi bông tai stud mạ vàng này có một hàng sao với kích thước tăng dần được thiết kế để ngồi lên thùy.', '<p>Vận chuyển và thanh toán</p>', '<p>Đổi trả và vận hành</p>', 'Danh sách các tính năng:<br>\r\n1. Mẫu mã đẹp<br>\r\n2. Hiện tại<br>\r\n3. Trẻ trung<br>', 2200000, 0, 32, 'assets/upload/users/admin/products/pro_6.jpg', 'assets/upload/users/admin/products/pro_6.jpg', 'assets/upload/users/admin/products/pro_6.jpg', '1,2,11,12', '3,4,7', '5,6,8,9,10', 3, 0, 0, 1, 1544487800);
/*!40000 ALTER TABLE `zl_product` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_product_category
CREATE TABLE IF NOT EXISTS `zl_product_category` (
  `product_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category_gid` int(11) NOT NULL,
  `parent_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT 1,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `slogan` varchar(90) NOT NULL,
  `intro_text` text NOT NULL,
  `big_logo` text NOT NULL,
  `medium_logo` text NOT NULL,
  `config` text NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0 COMMENT '0 - category, 1 - product type, 2 - style',
  `display_position` int(1) NOT NULL DEFAULT 0 COMMENT '0 - none, 1 - home top cat, 2 - home bottom cat',
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`product_category_id`) USING BTREE,
  UNIQUE KEY `title` (`title`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_product_category: ~64 rows (approximately)
/*!40000 ALTER TABLE `zl_product_category` DISABLE KEYS */;
REPLACE INTO `zl_product_category` (`product_category_id`, `product_category_gid`, `parent_gid`, `lang_id`, `title`, `alias`, `slogan`, `intro_text`, `big_logo`, `medium_logo`, `config`, `type`, `display_position`, `sorting`, `enabled`, `created_date`) VALUES
	(1, 1, 0, 1, 'Đồ kim hoàn', 'do-kim-hoan', '', '<p>Khám phá đồ trang sức cho mọi dịp với bộ sưu tập dây chuyền &amp; mặt dây chuyền, bông tai và đinh tán, vòng đeo &amp; vòng đeo tay, nhẫn, trâm cài và bộ của chúng tôi.</p>', 'assets/upload/users/admin/product_category/7.jpg', 'assets/upload/users/admin/product_category/7.jpg', '{"title": "Đồ kim hoàn","description": "Khám phá đồ trang sức cho mọi dịp với bộ sưu tập dây chuyền & mặt dây chuyền, bông tai và đinh tán, vòng đeo & vòng đeo tay, nhẫn, trâm cài và bộ của chúng tôi.","list_top_cats_gid": [3,5],"show_bestsellers": true,"list_bestsellers_products_gid": [1,2,3,4,5,6],"show_newin": true,"list_newin_products_gid": [1,2,3,4,5,6],"show_middle_cats": true,"list_middle_cats_gid": [3,4,5,49],"show_bottom_cats": true,"list_bottom_cats_gid": [6,7]}', 1, 1, 1, 1, 1544482800),
	(2, 2, 0, 1, 'Đồ nhà', 'do-nha', '', '<p>Nâng cấp ngôi nhà của bạn từ góc này sang góc khác với đồ gia dụng độc đáo với màu sắc tươi sáng và thiết kế bắt mắt.</p>', 'assets/upload/users/admin/product_category/8.jpg', 'assets/upload/users/admin/product_category/8.jpg', '', 1, 2, 2, 1, 1544482800),
	(3, 3, 7, 1, 'Đồ hoàn kim bạc', 'do-hoan-kim-bac', 'Đặt vẻ ngoài của bạn tỏa sáng với đồ trang sức bạc bắt mắt.', 'Khám phá bộ sưu tập trang sức bạc độc đáo của chúng tôi; từ những mảnh trừu tượng tuyên bố, đến một cái gì đó tinh tế hơn một chút. Trong các hình dạng cổ điển hoặc thiết kế hiện đại, mua sắm từ dây chuyền, vòng đeo tay, vòng đeo, hoa tai và nhẫn khác thường và tìm một mảnh phù hợp với phong cách của bạn. Với ngọc lam, ngọc trai và đá bán quý, bộ trang sức bạc nổi bật của chúng tôi cũng làm quà tặng lý tưởng.', 'assets/upload/users/admin/product_category/silver_jewellery_big.jpg', 'assets/upload/users/admin/product_category/silver_jewellery.jpg', '', 1, 0, 3, 1, 1544482800),
	(4, 4, 1, 1, 'Bông tai', 'bong-tai', '<p>Khám phá những bông tai tuyên bố trong bộ sưu tập trang phục của bạn.</p>', '<p>Khám phá những bông tai tuyên bố trong bộ sưu tập trang phục của bạn.</p>', 'assets/upload/users/admin/product_category/earrings.jpg', 'assets/upload/users/admin/product_category/earrings.jpg', '', 1, 1, 4, 1, 1544482800),
	(5, 5, 1, 1, 'Nhẫn', 'nhan', '<p>Khám phá những nhan tuyên bố trong bộ sưu tập trang phục của bạn.</p>', '<p>Khám phá những nhan tuyên bố trong bộ sưu tập trang phục của bạn.</p>', 'assets/upload/users/admin/product_category/1_2.jpg', 'assets/upload/users/admin/product_category/1_2.jpg', '', 1, 1, 5, 1, 1544482800),
	(6, 6, 1, 2, 'Jewellery Boxes & Storage', 'Jewellery-Boxes-Storage', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', 'Keep your most precious pieces stored in style with our selection of jewellery boxes, stands and storage. With large marble jewellery boxes with compartments and stands for necklaces and bracelets, as well as colourful ring holders and ceramic trinket dishes for smaller pieces, discover your new jewellery storage here.', 'assets/upload/users/admin/product_category/5.jpg', 'assets/upload/users/admin/product_category/5.jpg', '', 2, 0, 6, 1, 1544482800),
	(7, 7, 1, 2, 'Jewellery Gift Boxes & Storage', 'jewellery-gift-boxes-storage', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget do', '<p>Add the finishing touch to your jewellery gifts with our Oliver Bonas gift boxes and bags.</p>', 'assets/upload/users/admin/product_category/6.jpg', 'assets/upload/users/admin/product_category/6.jpg', '', 0, 0, 7, 1, 1544482800),
	(8, 8, 6, 2, 'Custom Jewellery', 'custom-jewellery', '', 'Discover our collection of costume jewellery in seasonal shades and tactile finishes. Including necklaces, bangles, earrings, studs and rings, our covetable costume range combines a range of shapes and materials, designed to make a statement no matter what your style. With exclusive designs mixing resin, wood, metallics and cord, find a fresh look with our unique costume jewellery edit.', '', '', '', 0, 0, 8, 1, 1544482800),
	(9, 9, 7, 2, 'The Most Sale Jewellery', 'the-most-sale-jewellery', '', '', '', '', '', 0, 0, 9, 1, 1544482800),
	(10, 10, 6, 1, 'Trang sức cho giới trẻ', 'trang-suc-cho-gioi-tre', '', '', '', '', '', 0, 0, 10, 1, 1544482800),
	(12, 12, 3, 2, 'Earrings Set', 'earrings-set', '', '', '', '', '', 0, 1, 12, 1, 1544482800),
	(13, 13, 3, 2, 'Bangles & Bracelets', 'bangles-bracelets', '', '', '', '', '', 0, 2, 13, 1, 1544482800),
	(14, 14, 8, 2, 'Earrings & Studs', 'earrings-studs', '', '', '', '', '', 0, 0, 14, 1, 1544482800),
	(15, 15, 8, 2, 'Chain Bracelets', 'chain-bracelets', '', '', '', '', '', 0, 0, 15, 1, 1544482800),
	(16, 16, 9, 2, 'Charm Bracelets', 'charm-bracelets', '', '', '', '', '', 0, 0, 16, 1, 1544482800),
	(17, 4, 1, 2, 'Earrings', 'earrings', '<p>Discover the statement earrings in our costume collection.</p>', '<p>Discover our collection of unique earrings and studs in opulent materials and luxe finishes. From gold hoop earrings to silver studs and costume designs, discover a range of styles with silver and gold-plated options, and find a pair to suit your style. Make your mark with our statement drop earrings, or opt for a smaller pair for a daytime look. Our pearl studs and gold-plated earrings with semi-precious stones make ideal gifts, too.</p>', 'assets/upload/users/admin/product_category/earrings.jpg', 'assets/upload/users/admin/product_category/earrings.jpg', '', 1, 1, 4, 1, 1544482800),
	(18, 5, 1, 2, 'Rings', 'rings', '<p>Discover the statement earrings in our costume collection.</p>', '<p>Discover our collection of unique earrings and studs in opulent materials and luxe finishes. From gold hoop earrings to silver studs and costume designs, discover a range of styles with silver and gold-plated options, and find a pair to suit your style. Make your mark with our statement drop earrings, or opt for a smaller pair for a daytime look. Our pearl studs and gold-plated earrings with semi-precious stones make ideal gifts, too.</p>', 'assets/upload/users/admin/product_category/1_2.jpg', 'assets/upload/users/admin/product_category/1_2.jpg', '', 1, 1, 5, 1, 1544482800),
	(19, 19, 0, 1, 'Quà tặng', 'qua-tang', '', 'Quà tặng Bespoke đang là xu hướng ngay bây giờ và Dòng sản phẩm unisex của OB Alphabet cung cấp cho bạn các tùy chọn chữ lồng được cá nhân hóa để đặt hàng ngay lập tức. Chọn từ các chữ cái bảng chữ cái bằng đồng, người mua vải, rửa và trang điểm, dây chuyền bạc và charm, cốc sứ xương, hộp đựng thẻ du lịch tiện dụng, hộp bút chì và móc treo tường; tất cả trong combo màu OB của xám và trắng với một màu pop.', '', '', '', 0, 0, 17, 1, 1544482800),
	(20, 19, 0, 2, 'Gift', 'gift', '', 'Bespoke gifting is on trend right now, and the OB Alphabet Range of unisex products provides you with personalised monogrammed options for immediate order. Choose from copper alphabet letters, canvas shoppers, wash and make up bags, silver and charm necklaces, bone china mugs, handy travel card holders, pencil cases and wall hooks; all in the OB colour combo of grey and white with a pop of colour.', '', '', '', 0, 0, 17, 1, 1544482800),
	(21, 21, 19, 1, 'Quà cho cha', 'qua-cho-cha', '', '', 'assets/upload/users/admin/product_category/2.jpg', 'assets/upload/users/admin/product_category/3_2.jpg', '', 0, 0, 18, 1, 1544482800),
	(22, 21, 19, 2, 'Father\'s Day Gift', 'fathers-day-gift', '', '', 'assets/upload/users/admin/product_category/2.jpg', 'assets/upload/users/admin/product_category/3_2.jpg', '', 0, 0, 18, 1, 1544482800),
	(23, 23, 0, 1, 'Cánh hoa', 'canh-hoa', '', '', 'assets/upload/users/admin/product_category/florals.jpg', 'assets/upload/users/admin/product_category/florals.jpg', '', 0, 0, 18, 1, 1544482800),
	(27, 23, 0, 2, 'Florals', 'florals', '', '', 'assets/upload/users/admin/product_category/florals.jpg', 'assets/upload/users/admin/product_category/florals.jpg', '', 0, 0, 18, 1, 1544482800),
	(28, 28, 0, 1, 'Đồ bảo hộ lao động', 'do-bao-ho-lao-dong', '', '', 'assets/upload/users/admin/product_category/workwear.jpg', 'assets/upload/users/admin/product_category/workwear.jpg', '', 0, 0, 19, 1, 1544482800),
	(30, 28, 0, 2, 'Workwear', 'workwear', '', '', 'assets/upload/users/admin/product_category/workwear.jpg', 'assets/upload/users/admin/product_category/workwear.jpg', '', 0, 0, 19, 1, 1544482800),
	(33, 2, 0, 2, 'Homeware', 'homewear', '', '<p>Uplift your home from corner to corner with unique homeware in bright colours and eye-catching designs.</p>', 'assets/upload/users/admin/product_category/8.jpg', 'assets/upload/users/admin/product_category/8.jpg', '', 1, 2, 0, 1, 1544482800),
	(34, 3, 7, 2, 'Silver Jewellery', 'silver-jewellery', 'Set your look to shine with light-catching silver jewellery in delicate fine chain designs', 'Discover our collection of unique silver jewellery; from statement abstract pieces, to something a little more delicate. In classic shapes or contemporary designs, shop from unusual necklaces, bracelets, bangles, earrings and rings and find a piece to suit your style. With turquoise, pearl and semi-precious stones, our standout silver jewellery sets make ideal gifts too. ', 'assets/upload/users/admin/product_category/silver_jewellery_big.jpg', 'assets/upload/users/admin/product_category/silver_jewellery.jpg', '', 1, 0, 0, 1, 1544482800),
	(35, 1, 0, 2, 'Jewellery', 'jewellery', '', '<p>Discover jewellery for every occasion with our collection of necklaces &amp; pendants, earrings &amp; studs, bangles &amp; bracelets, rings, brooches and sets.</p>', 'assets/upload/users/admin/product_category/7.jpg', 'assets/upload/users/admin/product_category/7.jpg', '{"title": "Jewellery","description": "Discover jewellery for every occasion with our collection of necklaces & pendants, earrings & studs, bangles & bracelets, rings, brooches and sets.","list_top_cats_gid": [3,5],"show_bestsellers": true,"list_bestsellers_products_gid": [1,2,3,4,5,6],"show_newin": true,"list_newin_products_gid": [1,2,3,4,5,6],"show_middle_cats": true,"list_middle_cats_gid": [3,4,5,49],"show_bottom_cats": true,"list_bottom_cats_gid": [6,7]}', 1, 1, 0, 1, 1544482800),
	(36, 36, 0, 1, 'Thời trang', 'thoi-trang', '', 'Cắt trong một loạt các hình bóng, bộ sưu tập váy của chúng tôi tập trung vào sự phù hợp và hình dạng để bạn có thể mặc một cái gì đó độc đáo cho công việc, các bữa tiệc và tất cả mọi thứ ở giữa. Với những ảnh hưởng hiện đại & được xem xét chi tiết theo chiều dài mini hoặc midi, hãy chọn từ các thiết kế váy thoải mái và dễ dàng, hoặc một cái gì đó có cấu trúc hơn cho các dịp trang trọng.', '', '', '{"title": "Thời trang","description": "Chiêm ngưỡng những mẫu thiết kế thời trang đẹp nhất.","list_top_cats_gid": [38,42],"show_bestsellers": true,"list_bestsellers_products_gid": [7,9,11,13,15,17,19],"show_newin": true,"list_newin_products_gid": [7,9,11,13,15,17,19],"show_middle_cats": true,"list_middle_cats_gid": [38,42,44,46],"show_bottom_cats": true,"list_bottom_cats_gid": [44,46]}', 0, 0, 19, 1, 1544482800),
	(37, 36, 0, 2, 'Fashion', 'fashion', '', 'Cut in a range of silhouettes, our collection of dresses focus on fit and shape so you can wear something unique for work, parties and everything in between. With modern influences & considered detailing in mini or midi lengths, choose from relaxed and effortless dress designs, or something more structured for formal occasions. ', '', '', '{"title": "Fashion","description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ","list_top_cats_gid": [38,42],"show_bestsellers": true,"list_bestsellers_products_gid": [7,9,11,13,15,17,19],"show_newin": true,"list_newin_products_gid": [7,9,11,13,15,17,19],"show_middle_cats": true,"list_middle_cats_gid": [38,42,44,46],"show_bottom_cats": true,"list_bottom_cats_gid": [44,46]}', 0, 0, 19, 1, 1544482800),
	(38, 38, 36, 1, 'Váy midi', 'vay-midi', 'Chiêm ngưỡng những bộ váy midi tuyệt đẹp', 'Váy midi là 1 trong những loại váy phổ biến dành cho các quý cô ngày nay. Với thiết kế hiện dại, trẻ trung và sang trọng', 'assets/upload/users/admin/product_category/midi-desses.jpg', 'assets/upload/users/admin/product_category/midi-desses.jpg', '', 1, 0, 18, 1, 1544482800),
	(41, 38, 36, 2, 'Midi desses', 'midi-desses', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ', 'assets/upload/users/admin/product_category/midi-desses.jpg', 'assets/upload/users/admin/product_category/midi-desses.jpg', '', 1, 0, 18, 1, 1544482800),
	(42, 42, 38, 1, 'Váy mini', 'vay-mini', 'Chiêm ngưỡng những bộ váy mini tuyệt đẹp', 'Váy mini là 1 trong những loại váy phổ biến dành cho các quý cô ngày nay. Với thiết kế hiện dại, trẻ trung và sang trọng', 'assets/upload/users/admin/product_category/mini-desses.jpg', 'assets/upload/users/admin/product_category/mini-desses.jpg', '', 2, 0, 19, 1, 1544482800),
	(43, 42, 38, 2, 'Mini desses', 'mini-desses', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ', 'assets/upload/users/admin/product_category/mini-desses.jpg', 'assets/upload/users/admin/product_category/mini-desses.jpg', '', 2, 0, 19, 1, 1544482800),
	(44, 44, 38, 1, 'Áo liền quần', 'ao-lien-quan', 'Chiêm ngưỡng những bộ áo liền quần tuyệt đẹp', 'Áo liền quần là 1 trong những loại váy phổ biến dành cho các quý cô ngày nay. Với thiết kế hiện dại, trẻ trung và sang trọng', 'assets/upload/users/admin/product_category/jumpsuits.jpg', 'assets/upload/users/admin/product_category/jumpsuits.jpg', '', 2, 0, 19, 1, 1544482800),
	(45, 44, 38, 2, 'Jumpsuits', 'jumpsuits', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ', 'assets/upload/users/admin/product_category/jumpsuits.jpg', 'assets/upload/users/admin/product_category/jumpsuits.jpg', '', 2, 0, 19, 1, 1544482800),
	(46, 46, 36, 1, 'Áo thun', 'ao-thun', 'Chiêm ngưỡng những bộ áo thun tuyệt đẹp', 'Áo thun là 1 trong những loại váy phổ biến dành cho các quý cô ngày nay. Với thiết kế hiện dại, trẻ trung và sang trọng', 'assets/upload/users/admin/product_category/tshirt.jpg', 'assets/upload/users/admin/product_category/tshirt.jpg', '', 1, 0, 20, 1, 1544482800),
	(48, 46, 36, 2, 'T-Shirt', 't-shirt', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. ', 'assets/upload/users/admin/product_category/tshirt.jpg', 'assets/upload/users/admin/product_category/tshirt.jpg', '', 1, 0, 20, 1, 1544482800),
	(49, 49, 1, 1, 'Vòng khuyên tai', 'vong-khuyen-tai', '<p>Không có bộ sưu tập trang sức nào hoàn chỉnh mà không có một đôi bông tai hoop và chúng', '<p>Không có bộ sưu tập trang sức nào hoàn chỉnh mà không có một đôi bông tai hoop và chúng tô</p>', 'assets/upload/users/admin/product_category/3_2.jpg', 'assets/upload/users/admin/product_category/3_2.jpg', '', 2, 1, 20, 1, 1544482800),
	(50, 49, 1, 2, 'Hoop Earrings', 'Hoop-Earrings', '<p>No jewellery collection is complete without a pair of hoop earrings and we\\\'ve got styl', '<p>No jewellery collection is complete without a pair of hoop earrings and we\\\'ve got styles t</p>', 'assets/upload/users/admin/product_category/3_2.jpg', 'assets/upload/users/admin/product_category/3_2.jpg', '', 2, 1, 20, 1, 1544482800),
	(51, 7, 1, 1, 'Hộp quà tặng và lưu trữ đồ trang sức', 'Hop-qua-tang-va-luu-tru-do-trang-suc', '<p>Hộ quà dành riêng cho bạn</p>', '<p>Thêm sự hoàn thiện cho quà tặng trang sức của bạn với hộp và túi quà Oliver Bonas của chúng tôi.</p>', 'assets/upload/users/admin/product_category/6.jpg', 'assets/upload/users/admin/product_category/6.jpg', '', 0, 0, 7, 1, 1544482800),
	(52, 6, 1, 1, 'Hộp đựng đồ trang sức', 'Hop-dung-do-trang-suc', 'Hộ bộ kim hoàn bao gồm nhiều món đồ có giá trị', 'Giữ những món đồ quý giá nhất của bạn được lưu trữ theo phong cách với lựa chọn hộp trang sức, giá đỡ và lưu trữ của chúng tôi. Với các hộp trang sức bằng đá cẩm thạch lớn với các ngăn và viết tắt của dây chuyền và vòng đeo tay, cũng như hộp đựng nhẫn nhiều màu sắc và các món đồ trang trí bằng gốm cho các mảnh nhỏ hơn, hãy khám phá kho lưu trữ trang sức mới của bạn ở đây.', 'assets/upload/users/admin/product_category/5.jpg', 'assets/upload/users/admin/product_category/5.jpg', '', 2, 0, 6, 1, 1544482800),
	(53, 53, 0, 1, 'Đồ nội thất', 'do-noi-that', '', 'Bộ sưu tập ghế bành & ghế của chúng tôi được thiết kế để đưa ra tuyên bố. Từ những chiếc ghế nhung trong sắc thái suy đồi được bọc ở Anh cho đến những chiếc ghế da mới với một điểm khác biệt, hãy chọn từ một loạt các hình dạng hiện đại với khung độc đáo bằng vải nhung sang trọng và vải Designers Guild.', '', '', '', 0, 0, 21, 1, 1544482800),
	(54, 53, 0, 2, 'Furniture', 'furniture', '', 'Our collection of armchairs & chairs are designed to make a statement. From velvet chairs in decadent shades upholstered in the UK to new leather chairs with a point of difference, choose from a range of modern shapes with unique frames in luxe velvet and Designers Guild fabrics.', '', '', '', 0, 0, 21, 1, 1544482800),
	(55, 55, 36, 1, 'Váy', 'vay', '', '', '', '', '', 1, 0, 22, 1, 1544482800),
	(56, 55, 36, 2, 'Desses', 'desses', '', '', '', '', '', 1, 0, 22, 1, 1544482800),
	(57, 57, 55, 1, 'Váy ôm', 'vay-om', '', '', '', '', '', 2, 0, 23, 1, 1544482800),
	(58, 57, 55, 2, 'Wrap desses', 'wrap-desses', '', '', '', '', '', 2, 0, 23, 1, 1544482800),
	(59, 59, 55, 1, 'Váy áo', 'vay-ao', '', '', '', '', '', 0, 0, 24, 1, 1544482800),
	(60, 59, 55, 2, 'Shirt desses', 'shirt-desses', '', '', '', '', '', 0, 0, 24, 1, 1544482800),
	(61, 61, 19, 1, 'Thiệp dành cho cha', 'thiep-danh-cho-cha', '', '', '', '', '', 0, 0, 25, 1, 1544482800),
	(62, 61, 19, 2, 'Father\'s Card Day', 'fathers-card-day', '', '', '', '', '', 0, 0, 0, 1, 1544482800),
	(63, 63, 7, 1, 'Vòng cổ đính cườm', 'vong-co-dinh-cuom', '', '', '', '', '', 2, 0, 26, 1, 1544482800),
	(64, 63, 7, 2, 'Beaded necklace', 'Beaded-necklace', '', '', '', '', '', 2, 0, 26, 1, 1544482800),
	(65, 65, 6, 1, 'Trọn bộ bông tai', 'tron-bo-bong-tai', '', '', '', '', '', 2, 0, 27, 1, 1544482800),
	(66, 65, 6, 2, 'Earrings Sets', 'earring-sets', '', '', '', '', '', 2, 0, 27, 1, 1544482800),
	(67, 10, 6, 2, 'Jewellery For The Young', 'jewellery-for-the-young', '', '', '', '', '', 0, 0, 10, 1, 1544482800),
	(68, 12, 3, 1, 'Bộ bông tai', 'bo-bong-tai', '', '', '', '', '', 0, 1, 12, 1, 1544482800),
	(69, 13, 3, 1, 'Lắc tay & Vòng tay', 'lac-tay-va-vong-tay', '', '', '', '', '', 0, 2, 13, 1, 1544482800),
	(70, 14, 8, 1, 'Hoa tai & đinh tán', 'hoa-tai-dinh-tan', '', '', '', '', '', 0, 0, 14, 1, 1544482800),
	(71, 15, 8, 1, 'Vòng tay chuỗi', 'vong-tay-chuoi', '', '', '', '', '', 0, 0, 15, 1, 1544482800),
	(72, 16, 9, 1, 'Vòng tay quyến rũ', 'vong-tay-quyen-ru', '', '', '', '', '', 0, 0, 16, 1, 1544482800),
	(73, 9, 7, 1, 'Trang sức bán chạy nhất', 'trang-suc-ban-chay-nhat', '', '', '', '', '', 0, 0, 9, 1, 0),
	(74, 8, 6, 1, 'Trang sức tùy chỉnh', 'trang-suc-tuy-chinh', '', 'Khám phá bộ sưu tập trang sức của chúng tôi trong các sắc thái theo mùa và hoàn thiện xúc giác. Bao gồm dây chuyền, vòng đeo, hoa tai, đinh tán và nhẫn, loạt trang phục phù hợp của chúng tôi kết hợp một loạt các hình dạng và vật liệu, được thiết kế để đưa ra tuyên bố cho dù phong cách của bạn là gì. Với các thiết kế độc quyền pha trộn nhựa, gỗ, kim loại và dây, tìm một cái nhìn mới mẻ với chỉnh sửa trang sức độc đáo của chúng tôi.', '', '', '', 0, 0, 8, 1, 0);
/*!40000 ALTER TABLE `zl_product_category` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_properties
CREATE TABLE IF NOT EXISTS `zl_properties` (
  `product_properties_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_properties_gid` int(11) NOT NULL,
  `product_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT 1,
  `title` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `attr` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0 COMMENT '0 - color, 1 - material, 2 - size',
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`product_properties_id`) USING BTREE,
  UNIQUE KEY `name_alias` (`title`,`alias`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_properties: ~24 rows (approximately)
/*!40000 ALTER TABLE `zl_properties` DISABLE KEYS */;
REPLACE INTO `zl_properties` (`product_properties_id`, `product_properties_gid`, `product_gid`, `lang_id`, `title`, `alias`, `attr`, `type`, `sorting`, `enabled`, `created_date`) VALUES
	(1, 1, 15, 1, 'Trắng', 'trang', '#f7f3f3', 0, 1, 1, 1544482800),
	(2, 2, 15, 1, 'Đỏ', 'do', '#ff0101', 0, 2, 1, 1544482800),
	(3, 3, 11, 1, 'Lụa', 'lua', '', 1, 5, 1, 1544482800),
	(4, 4, 15, 1, 'Vải', 'vải', '', 1, 6, 1, 1544482800),
	(5, 5, 15, 1, 'S', 's', '', 2, 8, 1, 1544482800),
	(6, 6, 11, 1, 'M', 'm', '', 2, 9, 1, 1544482800),
	(7, 7, 11, 1, 'Da', 'da', '', 1, 7, 1, 1544482800),
	(8, 8, 0, 1, 'L', 'l', '', 2, 10, 1, 0),
	(9, 9, 0, 1, 'X', 'x', '', 2, 11, 1, 0),
	(10, 10, 0, 1, 'K', 'k', '', 2, 12, 1, 0),
	(11, 11, 0, 1, 'Vàng', 'vang', '#ffff00', 0, 3, 1, 0),
	(12, 12, 0, 1, 'Xanh nước biển', 'xanh-nuoc-bien', '#0000ff', 0, 4, 1, 0),
	(13, 1, 0, 2, 'White', 'white', '#f7f3f3', 0, 1, 1, 0),
	(14, 2, 0, 2, 'Red', 'red', '#ff0101', 0, 2, 1, 0),
	(15, 11, 0, 2, 'Yellow', 'yellow', '#ffff00', 0, 3, 1, 0),
	(16, 3, 0, 2, 'Silk', 'silk', '', 1, 5, 1, 0),
	(17, 12, 0, 2, 'Blue', 'blue', '#0000ff', 0, 4, 1, 0),
	(18, 4, 0, 2, 'Cotton', 'cotton', '', 1, 6, 1, 0),
	(19, 5, 0, 2, 'S', 's-en', '', 2, 8, 1, 0),
	(20, 6, 0, 2, 'M', 'm-en', '', 2, 9, 1, 0),
	(21, 7, 0, 2, 'leather', 'leather', '', 1, 7, 1, 0),
	(22, 8, 0, 2, 'L', 'l-en', '', 2, 10, 1, 0),
	(23, 9, 0, 2, 'X', 'x-en', '', 2, 11, 1, 0),
	(24, 10, 0, 2, 'K', 'k-en', '', 2, 12, 1, 0);
/*!40000 ALTER TABLE `zl_properties` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_setting
CREATE TABLE IF NOT EXISTS `zl_setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL COMMENT 'md5 encrypt',
  `value` text DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`setting_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_setting: ~13 rows (approximately)
/*!40000 ALTER TABLE `zl_setting` DISABLE KEYS */;
REPLACE INTO `zl_setting` (`setting_id`, `alias`, `title`, `value`, `enabled`, `created_date`) VALUES
	(1, 'website_title', 'Tiêu đề website ', 'Web Bán Hàng 2019', 1, 1544482800),
	(3, 'logo', 'Logo', 'assets/upload/users/admin/IMG_5031.JPG', 1, 1544482800),
	(5, 'favicon', 'Favicon', 'assets/upload/users/admin/IMG_5058.JPG', 1, 1544482800),
	(7, 'website_url', 'Website url', 'webbanhang.com', 1, 1544482800),
	(8, 'email_name', 'Email Name', 'trieu.public@gmail.com', 1, 1544482800),
	(9, 'email_password', 'Email Password', 'abcdef@123456', 1, 1544482800),
	(10, 'footer_note', 'Footer Note', 'Web Bán Hàng @ Coppyright 2019', 1, 1544482800),
	(11, 'facebook', 'Facebook', '#', 1, 1544482800),
	(12, 'twitter', 'Twitter', '#', 1, 1544482800),
	(13, 'instagram', 'Instagram', '#', 1, 1544482800),
	(14, 'pinterest', 'Pinterest', '#', 1, 1544482800),
	(15, 'home_config', 'Thiết lập hiển thị trang chủ', '[{"lang_id":1,"config":{"top_banner": {"controller": "product","function" : "index","params" : "fashion/all","img" : "assets/upload/users/admin/slide/top_banner.jpg"},"title" : "Bán chạy nhất","description" : "Làm bừng sáng phong cách cho mùa xuân với thời trang và phụ kiện bán chạy nhất","top_productcats" : [1,2,3,4],"bottom_productcats" : [4,5],"bottom_banner": {"controller": "product","function" : "index","params" : "jewellery/all","img" : "assets/upload/users/admin/slide/bottom_banner.jpg"}}},{"lang_id":2,"config":{"top_banner": {"controller": "product","function" : "index","params" : "fashion/all","img" : "assets/upload/users/admin/slide/top_banner.jpg"},"title" : "Bestsellers","description" : "Brighten up your style for spring with best-selling fashion and accessories","top_productcats" : [1,2,3,4],"bottom_productcats" : [4,5],"bottom_banner": {"controller": "product","function" : "index","params" : "jewellery/all","img" : "assets/upload/users/admin/slide/bottom_banner.jpg"}}}]', 1, 1544482800),
	(16, 'transfer_fee', 'Phí Vận chuyển', '50000', 1, 1544482800);
/*!40000 ALTER TABLE `zl_setting` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_store
CREATE TABLE IF NOT EXISTS `zl_store` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT 1,
  `title` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `map` text NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `sorting` int(11) NOT NULL,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`store_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_store: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_store` DISABLE KEYS */;
REPLACE INTO `zl_store` (`store_id`, `store_gid`, `lang_id`, `title`, `alias`, `map`, `enabled`, `sorting`, `created_date`) VALUES
	(1, 1, 2, 'Ho Chi Minh City', 'ho-chi-minh-city', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d501725.33821518696!2d106.41502420192768!3d10.755341096429683!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529292e8d3dd1%3A0xf15f5aad773c112b!2sHo+Chi+Minh+City%2C+Vietnam!5e0!3m2!1sen!2s!4v1560102569494!5m2!1sen!2s', 1, 1, 1544569200),
	(2, 2, 2, 'London East', 'london-east', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d501725.33821518696!2d106.41502420192768!3d10.755341096429683!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529292e8d3dd1%3A0xf15f5aad773c112b!2sHo+Chi+Minh+City%2C+Vietnam!5e0!3m2!1sen!2s!4v1560102569494!5m2!1sen!2s', 1, 2, 1544569200),
	(3, 3, 2, 'London South', 'london-south', 'https://maps.google.com/maps?q=Movies%20theater%20in%20london&t=&z=13&ie=UTF8&iwloc=&output=embed', 1, 3, 1544569200),
	(4, 1, 1, 'Thành Phố Hồ Chí Minh', 'thanh-pho-ho-chi-minh', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d501725.33821518696!2d106.41502420192768!3d10.755341096429683!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529292e8d3dd1%3A0xf15f5aad773c112b!2sHo+Chi+Minh+City%2C+Vietnam!5e0!3m2!1sen!2s!4v1560102569494!5m2!1sen!2s', 1, 1, 1544569200),
	(5, 2, 1, 'Tây london', 'tay-london', 'https://maps.google.com/maps?q=Movies%20theater%20in%20london&t=&z=13&ie=UTF8&iwloc=&output=embed', 1, 2, 1544569200),
	(6, 3, 1, 'Nam london', 'nam-london', 'https://maps.google.com/maps?q=Movies%20theater%20in%20london&t=&z=13&ie=UTF8&iwloc=&output=embed', 1, 3, 1544569200),
	(9, 7, 1, 'Demo', 'demo', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3834.1724910152248!2d108.17035671485834!3d16.056536388888798!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31421910acc9146d%3A0x1c521f3989857038!2zQuG6v24gWGUgVHJ1bmcgVMOibSDEkMOgIE7hurVuZw!5e0!3m2!1sen!2s!4v1560005615814!5m2!1sen!2s', 1, 0, 1559944800),
	(10, 7, 2, 'Demo en', 'demo-en', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3834.1724910152248!2d108.17035671485834!3d16.056536388888798!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31421910acc9146d%3A0x1c521f3989857038!2zQuG6v24gWGUgVHJ1bmcgVMOibSDEkMOgIE7hurVuZw!5e0!3m2!1sen!2s!4v1560005615814!5m2!1sen!2s', 1, 0, 1559944800);
/*!40000 ALTER TABLE `zl_store` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_store_detail
CREATE TABLE IF NOT EXISTS `zl_store_detail` (
  `store_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_detail_gid` int(11) NOT NULL,
  `store_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `close_time` varchar(100) NOT NULL,
  `map` text NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`store_detail_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_store_detail: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_store_detail` DISABLE KEYS */;
REPLACE INTO `zl_store_detail` (`store_detail_id`, `store_detail_gid`, `store_gid`, `lang_id`, `title`, `alias`, `address`, `close_time`, `map`, `enabled`, `created_date`) VALUES
	(1, 1, 1, 2, 'Base center', 'base-center', '434 Pham Van Chieu, Ward 14, Go Vap, Ho Chi Minh City', '08h00-17h00, M-S', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.480673658405!2d106.6531062148014!3d10.85099809227096!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529bdd227bdff%3A0x1373623af60f4857!2zNDM0IFBo4bqhbSBWxINuIENoacOqdSwgUGjGsOG7nW5nIDksIEfDsiBW4bqlcCwgSOG7kyBDaMOtIE1pbmg!5e0!3m2!1sen!2s!4v1560102926409!5m2!1sen!2s', 1, 1544569200),
	(2, 2, 1, 2, 'Bankside', 'bankside', 'Unit 16 Bankside 2 100 Southwark Street', '18:00', 'https://maps.google.com/maps?q=Unit%2016%20Bankside%202%20100%20Southwark%20Street%20%20London&t=&z=13&ie=UTF8&iwloc=&output=embed', 1, 1544569200),
	(3, 3, 2, 2, 'Canary Wharf', 'canary-wharf', '45 Bank Street Jubilee Place, London E14 5NY', '18:00', 'https://maps.google.com/maps?q=45%20Bank%20Street%20Jubilee%20Place%2C%20London%20E14%205NY&t=&z=13&ie=UTF8&iwloc=&output=embed', 1, 1544569200),
	(4, 4, 2, 2, 'Blackheath', 'blackheath', '22 Tranquil Vale', '17:00', 'https://maps.google.com/maps?q=22%20Tranquil%20Vale%20%20Blackheath&t=&z=13&ie=UTF8&iwloc=&output=embed', 1, 1544569200),
	(5, 1, 1, 1, 'Trụ sơ chính', 'tru-so-chinh', '434 Phạm Văn Chiêu, phường 14, Gò Vấp, Ho Chi Minh City', '08h00-17h00, T2-CN', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.480673658405!2d106.6531062148014!3d10.85099809227096!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529bdd227bdff%3A0x1373623af60f4857!2zNDM0IFBo4bqhbSBWxINuIENoacOqdSwgUGjGsOG7nW5nIDksIEfDsiBW4bqlcCwgSOG7kyBDaMOtIE1pbmg!5e0!3m2!1sen!2s!4v1560102926409!5m2!1sen!2s', 1, 1544569200),
	(6, 3, 2, 1, 'Đường Canary Wharf', 'duong-canary-wharf', '45 Bank Street Jubilee Place, London E14 5NY', '18:00', 'https://maps.google.com/maps?q=45%20Bank%20Street%20Jubilee%20Place%2C%20London%20E14%205NY&t=&z=13&ie=UTF8&iwloc=&output=embed', 1, 1544569200),
	(7, 4, 2, 1, 'Đường blackheath', 'duong-blackheath', '22 Tranquil Vale', '17:00', 'https://maps.google.com/maps?q=22%20Tranquil%20Vale%20%20Blackheath&t=&z=13&ie=UTF8&iwloc=&output=embed', 1, 1544569200),
	(8, 2, 1, 1, 'Đường bankside', 'duong-bankside', 'Đơn vị 16 Bankside 2 100 Southwark Street', '18:00', 'https://maps.google.com/maps?q=Unit%2016%20Bankside%202%20100%20Southwark%20Street%20%20London&t=&z=13&ie=UTF8&iwloc=&output=embed', 1, 1544569200);
/*!40000 ALTER TABLE `zl_store_detail` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_subscribes
CREATE TABLE IF NOT EXISTS `zl_subscribes` (
  `subscribes_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`subscribes_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_subscribes: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_subscribes` DISABLE KEYS */;
REPLACE INTO `zl_subscribes` (`subscribes_id`, `email`, `created_date`) VALUES
	(1, 'trieu.public@gmail.com', 1554588000),
	(2, 'trieu.ngohuy@gmail.com', 1554588000),
	(4, 'dde@fweg.wgewg', 1556575200),
	(5, 'asdas@gwgw.geg', 1560031200);
/*!40000 ALTER TABLE `zl_subscribes` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_user
CREATE TABLE IF NOT EXISTS `zl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1 - admin, 0 - user',
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `prefix` varchar(5) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL COMMENT 'md5 encrypt',
  `address` text NOT NULL,
  `reset_token` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_user: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_user` DISABLE KEYS */;
REPLACE INTO `zl_user` (`user_id`, `type`, `email`, `phone`, `username`, `prefix`, `firstname`, `lastname`, `password`, `address`, `reset_token`, `sorting`, `enabled`, `created_date`) VALUES
	(1, 1, 'admin@gmail.com', '0123456789', 'admin', 'Ms', 'Admin', 'Manager', '$2y$10$B6N9rVoZmUjT.gLHPUW1yuj3CwB2jtR1qWhrTftao/W4nUngTjPji', '[{"address":"2/11 Thừa Thiên Huế","country_gid": 1,"province_gid": 2,"district_gid": 3,"ward_gid": 5},{"address": "1/20 Đặng Thùy Trâm","country_gid": 1,"province_gid": 2,"district_gid": 6,"ward_gid": 4}]', '52a4f78d71e20a1057deb369f3bccaadd2d4282685fe66c39b7c702b6c8af782c9158113c4c71d45f1c7256e62724dcb5c083e0f3611aca5536b7200e377524d', 1, 1, 1544482800),
	(2, 0, 'guest@gmail.com', '0123456789', 'guest', 'Miss', 'Thừa Thiên', 'Huế City', '$2y$10$XU0gBOJSiZwDgzWrcL1oW.DOzo9OrUJZNDn/q0vFZxYdRiEBkO/EW', '[{\\"address\\":\\"3/11 Hihi\\",\\"country_gid\\":\\"1\\",\\"province_gid\\":\\"2\\",\\"district_gid\\":\\"3\\",\\"ward_gid\\":\\"4\\"},{\\"address\\":\\"2 Haha\\",\\"country_gid\\":\\"1\\",\\"province_gid\\":\\"2\\",\\"district_gid\\":\\"6\\",\\"ward_gid\\":\\"7\\"}]', 'c380f43fd767ac581bfb51ade4dd22ac5d33057108f34e1d9cb9ba372d10cff5f9807c1134c85acd03a21561ee691b84c6b667947f88741e239c0a9087c6d239', 2, 1, 1544482800),
	(5, 0, 'guest3@gmail.com', '0123456789', NULL, 'Miss', 'a', 'b', '$2y$10$47wal1MqY8tN7CcFbZTSiuSZX2YWMYwP.F42Y4f5h.cmICzo1p6Gi', '', '', 3, 1, 1558476000);
/*!40000 ALTER TABLE `zl_user` ENABLE KEYS */;

-- Dumping structure for table webbanhang.zl_voucher
CREATE TABLE IF NOT EXISTS `zl_voucher` (
  `voucher_id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_gid` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT 1,
  `title` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `intro_text` text NOT NULL,
  `percent` int(11) NOT NULL DEFAULT 0,
  `money` int(11) NOT NULL DEFAULT 0,
  `min_order` int(11) NOT NULL DEFAULT 0,
  `start_date` varchar(100) NOT NULL,
  `end_date` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (`voucher_id`) USING BTREE,
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `title` (`title`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table webbanhang.zl_voucher: ~0 rows (approximately)
/*!40000 ALTER TABLE `zl_voucher` DISABLE KEYS */;
REPLACE INTO `zl_voucher` (`voucher_id`, `voucher_gid`, `lang_id`, `title`, `alias`, `code`, `intro_text`, `percent`, `money`, `min_order`, `start_date`, `end_date`, `enabled`, `created_date`) VALUES
	(1, 1, 1, 'Coupon giảm 150K cho các sản phẩm trang sức', 'coupon-giam-150k-cho-cac-san-pham-trang-suc', '107MWW0XXC', 'Áp dụng cho các sản phẩm Sabina khi mua qua App', 0, 150000, 200000, '11/05/2019 12:00', '11/06/2019 23:00', 1, 1544482800),
	(2, 2, 1, 'Mã giảm giá giảm 15% cho các sản phẩm áo quần', 'ma-giam-gia-giam-15-cho-cac-san-pham-ao-quan', '4FLX1QW84G', 'Áp dụng cho các sản phẩm Nhà Cửa Đời Sống khi mua qua App', 15, 0, 300000, '11/05/2019 12:00', '11/05/2019 12:00', 1, 1544482800),
	(3, 0, 1, 'Thử thách 100 ngày với nhiều ưu đãi hấp dẫn', 'thu-thach-100-ngay-voi-nhieu-uu-dai-hap-dan', '4S3JU0NM1I', 'abcd e f g', 0, 160000, 100000, '11/5/2019 12:00', '11/6/2019 12:00', 1, 1557525600);
/*!40000 ALTER TABLE `zl_voucher` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
