<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

//Language constant
defined('SITE_LANG')      OR define('SITE_LANG', 'site_lang');
defined('DEFAULT_LANG')      OR define('DEFAULT_LANG', 'vi');
//Date format
defined('DATE_FORMAT')      OR define('DATE_FORMAT', 'd-m-Y');
defined('DATETIME_FORMAT')      OR define('DATETIME_FORMAT', 'd-m-Y H:m:i');
//God mode
defined('IS_GOD_MODE')      OR define('IS_GOD_MODE', true);
//System email
defined('SYSTEM_EMAIL')      OR define('SYSTEM_EMAIL', 'lightnovelsynthetic@gmail.com');
defined('SYSTEM_EMAIL_PASSWORD')      OR define('SYSTEM_EMAIL_PASSWORD', 'khanhtoan1999');
//Website
defined('WEBSITE')      OR define('WEBSITE', 'google.com');
//Upload path for user
defined('USER_UPLOAD_PATH')      OR define('USER_UPLOAD_PATH', 'assets/upload/users/');
//Default background image
defined('IMG_DEFAULT_ITEM')      OR define('IMG_DEFAULT_ITEM', 'assets/ass_front/img/default_story.jpg');
defined('IMG_DEFAULT_USER_LOGO')      OR define('IMG_DEFAULT_USER_LOGO', 'assets/img/user_logo.png');
//Number of items on each page
defined('PAGE_MAX_ITEMS')      OR define('PAGE_MAX_ITEMS', 10);

//Routes constant
defined('URL_ADMIN')      OR define('URL_ADMIN', 'admin.html');
defined('URL_ADMIN_SETTING')      OR define('URL_ADMIN_SETTING', 'admin/setting.html');
defined('URL_ADMIN_LOGIN')      OR define('URL_ADMIN_LOGIN', 'admin/login.html');
defined('URL_ADMIN_LOGOUT')      OR define('URL_ADMIN_LOGOUT', 'admin/logout.html');
defined('URL_ADMIN_CONTENT')      OR define('URL_ADMIN_CONTENT', 'admin/content.html');
defined('URL_ADMIN_SEASON')      OR define('URL_ADMIN_SEASON', 'admin/season.html');
defined('URL_ADMIN_EPISODES')      OR define('URL_ADMIN_EPISODES', 'admin/episodes.html');
defined('URL_ADMIN_PROJECT')      OR define('URL_ADMIN_PROJECT', 'admin/project.html');
defined('URL_ADMIN_CATEGORY')      OR define('URL_ADMIN_CATEGORY', 'admin/category.html');
defined('URL_ADMIN_STATE')      OR define('URL_ADMIN_STATE', 'admin/state.html');
defined('URL_ADMIN_USER')      OR define('URL_ADMIN_USER', 'admin/user.html');
defined('URL_ADMIN_COMMENT')      OR define('URL_ADMIN_COMMENT', 'admin/comment.html');
defined('URL_HOME')      OR define('URL_HOME', '/');
defined('URL_LOGIN')      OR define('URL_LOGIN', 'dang-nhap');
defined('URL_REGISTER')      OR define('URL_REGISTER', 'dang-ky');
defined('URL_LOGOUT')      OR define('URL_LOGOUT', 'dang-xuat');
defined('URL_FORGET_PASSWORD')      OR define('URL_FORGET_PASSWORD', 'quen-mat-khau');
defined('URL_PROJECT')      OR define('URL_PROJECT', 'du-an/');
defined('URL_CONTENT')      OR define('URL_CONTENT', 'truyen/');
defined('URL_CATEGORY')      OR define('URL_CATEGORY', 'the-loai/');
defined('URL_STATE')      OR define('URL_STATE', 'tinh-trang/');
defined('URL_EPISODES')      OR define('URL_EPISODES', 'doc-truyen/');
defined('URL_SEARCH')      OR define('URL_SEARCH', 'tim-kiem');

//Session constant
defined('SS_ADMIN_LOGIN')      OR define('SS_ADMIN_LOGIN', 'ss_admin_login');
defined('SS_LOGIN')      OR define('SS_LOGIN', 'ss_login');
