<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LanguageLoader {

    function initialize() {

        $ci = & get_instance();

        $ci->load->helper('language');

        $siteLang = $ci->session->userdata(SITE_LANG);

        if ($siteLang) {

            $ci->lang->load('db', $siteLang['code']);
        } else {
            $tmp = get_lang_data(array('code' => DEFAULT_LANG));
            $ci->lang->load('db', $tmp['code']);
        }
    }

}
