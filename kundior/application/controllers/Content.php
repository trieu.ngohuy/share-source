<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends ZLFront_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('content_model');
        $this->load->model('season_model');
        $this->load->model('episodes_model');
        $this->load->model('state_model');
        $this->load->model('category_model');
        $this->load->model('comment_model');
        $this->load->model('user_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index($alias) {
        try {

            //Post comment
            if ($this->input->post('comment')) {
                //Get post data
                $arr_post_data = $this->input->post('comment');
                //Insert comment
                $this->comment_model->insert_new_data(
                    array(
                        'parent_id' => $arr_post_data['parent_id'],
                        'content_id' => $arr_post_data['content_id'],
                        'content_episodes_id' => $arr_post_data['content_episodes_id'],
                        'full_text' => $arr_post_data['full_text'],
                        'user_id' => $this->arr_view_data['login_data']['user_id'],
                        'created_date' => strtotime(date(DATE_FORMAT)),
                    )
                );
            }

            //Get detail
            $obj_data = $this->content_model->get_single_data(array(
                'alias' =>$alias
            ));

            //Update view
            $this->content_model->update_existing_data(
                array('alias' => $alias),
                array('view' => $obj_data['view'] + 1)
            );

            //Get season and episodes
            $sum_word_count = 0;
            $arr_seasons = $this->season_model->get_list_data(null, array(
                'content_id' => $obj_data['content_id']
            ));
            
            foreach ($arr_seasons as $key => &$value) {
                $value['detail'] = $this->episodes_model->get_list_data(null, array(
                    'content_season_id' => $value['content_season_id'],
                    'parent_gid' => 0
                ));
                foreach ($value['detail'] as &$value2) {
                    $arr_episodes = $this->episodes_model->get_list_data(null, array(
                        'parent_gid' => $value2['content_episodes_id']
                    ));
                    $value2['detail'] = $arr_episodes;
                    $sum_word_count += $value2['word_count'];
                    foreach ($arr_episodes as $value3) {
                        $sum_word_count += $value3['word_count'];
                    }
                }
                unset($value2);
                
            };
            unset($value);
            //Get state
            $tmp = $this->state_model->get_single_data(array(
                'content_state_id' =>$obj_data['content_state_id']
            ));
            $obj_data['state'] = $tmp;
            //Get category name
            $tmp = explode("||", $obj_data['list_content_category_id']);
            $str_category = '';
            for($i = 0 ; $i < count($tmp) ; $i++){
                $tmp_category = $this->category_model->get_single_data(array(
                    'content_category_id' =>$tmp[$i]
                ));
                if($i < count($tmp)){
                    $str_category .= '<a href="'.$tmp_category['url'].'">' . $tmp_category['title'] . '</a>, ';
                }else{
                    $str_category .= '<a href="'.$tmp_category['url'].'">' . $tmp_category['title'] . '</a>';
                }
            }
            $obj_data['detail'] = $arr_seasons;
            $this->arr_view_data['sum_word_count'] = $sum_word_count;
            $obj_data['category'] = $str_category;

            //Get comment
            $arr_comment = $this->comment_model->get_list_data(null,
                array(
                    'content_id' => $obj_data['content_id']
                )
            );
            $arr_tmp_comment = array();
            foreach ($arr_comment as $key => $value) {

                if($value['parent_id'] !== "0"){
                    continue;
                }
                //get user full name
                $tmp = $this->user_model->get_single_data(array(
                    'user_id' => $value['user_id']
                ));
                $value['full_name'] = $tmp['full_name'];
                //Get detail
                $arr_detail = array();
                foreach ($arr_comment as $key => &$value2) {
                    if($value['comment_id'] === $value2['parent_id']){
                        //get user full name
                        $tmp = $this->user_model->get_single_data(array(
                            'user_id' => $value2['user_id']
                        ));
                        $value2['full_name'] = $tmp['full_name'];
                        $arr_detail[] = $value2;
                    }
                }
                $value['detail'] = $arr_detail;
                $arr_tmp_comment[] = $value;
            }
            //$arr_comment = recursive_data($arr_comment, 0, array());
            $this->arr_view_data['comment'] = $arr_tmp_comment;

            //Set post data back to view
            $this->arr_view_data['data'] = $obj_data;

            //Website title
            $this->arr_view_data['website_title'] = $obj_data['title'];

            //Set layout
            $this->layout->load('content', 'content/frontend/index', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /*
    * Search
    */
    public function search(){
        $key = '';
        if ($this->input->post('search')) {
            $arr_post_data = $this->input->post('search');
            $key = $arr_post_data['key'];
        }
        $arr_data = $this->content_model->get_list_data();
        //Set post data back to view
        $this->arr_view_data['data'] = $arr_data;
        $this->arr_view_data['key'] = $key;

        //Website title
        $this->arr_view_data['website_title'] = 'Tìm kiếm: ' . $key;
        
        //Set layout
        $this->layout->load('project', 'content/frontend/search', $this->arr_view_data);   
    }
}   
