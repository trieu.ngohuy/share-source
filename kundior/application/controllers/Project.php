<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends ZLFront_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('project_model');
        $this->load->model('content_model');
    }
    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            //Get list projects
            $arr_data = $this->project_model->get_list_data();

            //Get list content
            $arr_content = sort_array_2($this->content_model->get_list_data());
            $arr_detail = array();
            foreach($arr_data as $value){
                foreach ($arr_content as $value2) {
                    $arr_tmp = explode('||', $value2['list_project_id']);
                    if (in_array($value['project_id'], $arr_tmp)) {
                        $arr_detail[] = $value2;
                    }
                }
                unset($value2);
            }
            unset($value);

            //Set post data back to view
            $this->arr_view_data['data'] = $arr_data;

            //Website title
            $this->arr_view_data['website_title'] = 'Dự án';

            //Set layout
            $this->layout->load('category', 'project/frontend/index', $this->arr_view_data);   
            
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
    /**
     * Detail
     */
    public function detail($alias) {
        try {
            //Get project detail
            $obj_data = $this->project_model->get_single_data(array(
                'alias' =>$alias
            ));
            //Get list content
            $arr_content = sort_array_2($this->content_model->get_list_data());
            $arr_data = array();
            foreach ($arr_content as $value) {
                $arr_tmp = explode('||', $value['list_project_id']);
                if (in_array($obj_data['project_id'], $arr_tmp)) {
                    $arr_data[] = $value;
                }
            }
            unset($value);

            //Set post data back to view
            $this->arr_view_data['data'] = $arr_data;
            $this->arr_view_data['detail'] = $obj_data;

            //Website title
            $this->arr_view_data['website_title'] = $obj_data['title'];

            //Set layout
            $this->layout->load('project', 'project/frontend/detail', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

}
