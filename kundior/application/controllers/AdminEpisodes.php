<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminEpisodes extends ZLAdmin_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('episodes_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        try {
            //$this->arr_view_data = array();

            //Active menu
            $this->arr_view_data['active_menu'] = 'episodes';
            
            //Get list data
            $arr_data = $this->episodes_model->get_list_data();
            foreach ($arr_data as &$value) {
                //Get season
                $this->load->model('season_model');
                $tmp = $this->season_model->get_single_data(array(
                    'content_season_id' => $value['content_season_id']
                ));
                $value['season'] = $tmp['title'];
                //Get content
                $this->load->model('content_model');
                $tmp = $this->content_model->get_single_data(array(
                    'content_id' => $tmp['content_id']
                ));
                $value['content'] = $tmp['title'];
                //$text =  htmlspecialchars($value['full_text']);
                $value['full_text'] =  htmlspecialchars_decode($value['full_text']);
                $value['footer_note'] =  htmlspecialchars_decode($value['footer_note']);
            }
            unset($value);

            //Set post data back to view
            $this->arr_view_data['data'] = $arr_data;

            //Get list content
            $this->load->model('content_model');
            $this->arr_view_data['content'] = $this->content_model->get_list_data();
            
            //Get list category
            $this->load->model('season_model');
            $this->arr_view_data['season'] = $this->season_model->get_list_data();

            //Set layout
            $this->layout->load('admin', 'episodes/backend/manager', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    //Update/New data
    function execute_query() {
        $this->db->reconnect();
        //Get post data
        $content_episodes_id = $_POST['content_episodes_id'];
        $arr_query_data = array(
            'title' => $_POST['title'],
            'alias' => $_POST['alias'],
            'footer_note' => htmlspecialchars($_POST['footer_note']),
            'content_season_id' => $_POST['content_season_id'],
            'full_text' => htmlspecialchars($_POST['full_text']),
            'parent_gid' => $_POST['parent_gid'],
            'word_count' => $_POST['word_count']
        );
        //echo $_POST['full_text'];
        //Execute query
        if ($content_episodes_id == 0) {
            $arr_query_data['created_date'] = strtotime(date(DATE_FORMAT));
            //New
            $arr_data = $this->episodes_model->insert_new_data($arr_query_data);
        } else {
            //Update
            $arr_data = $this->episodes_model->update_existing_data(array(
                'content_episodes_id' => $content_episodes_id
                    ), $arr_query_data);
        }
    }

    //Get data
    function get_data() {

        //Get list data
        $arr_data = $this->episodes_model->get_list_data();
        foreach ($arr_data as &$value) {
            //Get season
            $this->load->model('season_model');
            $tmp = $this->season_model->get_single_data(array(
                'content_season_id' => $value['content_season_id']
            ));
            $value['season'] = $tmp['title'];
            //Get content
            $this->load->model('content_model');
            $tmp = $this->content_model->get_single_data(array(
                'content_id' => $tmp['content_id']
            ));
            $value['content'] = $tmp['title'];
            $value['full_text'] =  htmlspecialchars_decode($value['full_text']);
            $value['footer_note'] =  htmlspecialchars_decode($value['footer_note']);
        }
        unset($value);
        echo json_encode($arr_data);
    }

    //Delete
    function delete() {
        //Get post data
        $content_episodes_id = $_POST['content_episodes_id'];

        //Get detail
        $arr_data = $this->episodes_model->get_single_data(array(
            'content_episodes_id' => $content_episodes_id
        ));

        //Execute query
        $this->episodes_model->delete_data(array(
            'content_episodes_id' => $content_episodes_id
        ));

        //Delete images
        delete_images($arr_data['logo']);
    }

}
