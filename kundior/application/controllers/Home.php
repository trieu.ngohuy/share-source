<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends ZLFront_Controller {

    /**
     * Index Page for this controller.
     */
    public function index() {

        //Website title
        $this->arr_view_data['website_title'] = 'Trang chủ';

        //get list contents
        $this->load->model('content_model');
        $this->load->model('season_model');
        $this->load->model('episodes_model');
        $arr_data = $this->episodes_model->get_list_data_2();
        //sort_array($arr_data, 'created_date_number');
        usort($arr_data, "sort_array_descending");
        foreach ($arr_data as $key => &$value) {
        	//Get season
        	$tmp = $this->season_model->get_single_data(array(
        		'content_season_id' => $value['content_season_id']
        	));
        	$value['season'] = $tmp['title'];
        	$value['season_url'] = $tmp['url'];
        	//Get content
        	$tmp = $this->content_model->get_single_data(array(
        		'content_id' => $tmp['content_id']
        	));
        	$value['content'] = $tmp['title'];
        	$value['content_logo'] = $tmp['logo'];
        	$value['content_url'] = $tmp['url'];
        }

        //Get list contents
        // $arr_data = $this->content_model->get_list_data();
        // usort($arr_data, "sort_array_descending");
        // foreach ($arr_data as $key => &$value) {
        //     //Get list seasons
        //     $arr_chapter = $this->season_model->get_list_data(null, array(
        //         'content_id' => $value['content_id']
        //     ));
        //     usort($arr_data, "sort_array_descending");
        //     foreach ($arr_chapter as $key2 => &$value2) {
        //         //Get lis episodes
        //         $arr_episodes = $this->episodes_model->get_list_data(null, array(
        //             'content_season_id' => $value2['content_season_id']
        //         ));
        //         usort($arr_episodes, "sort_array_descending");
        //         $value2['episodes'] = $arr_episodes;
        //     }
        //     unset($value2);
        //     $value['seasons'] = $arr_chapter;
        // }

        unset($value);
        $this->arr_view_data['data'] = $arr_data;
        //Category logo
        $this->arr_view_data['url'] = base_url() . URL_CATEGORY;

        $this->layout->load('front', 'home/index', $this->arr_view_data);
    }
    
}
