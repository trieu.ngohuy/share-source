<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminSetting extends ZLAdmin_Controller {

    /**
     * Index
     */
    public function index() {
        try {
            //$this->arr_view_data = array();
            $config_data = array();

            //Active menu
            $this->arr_view_data['active_menu'] = 'setting';

            //Get login user data
            $arr_login_user = $this->session->userdata(SS_ADMIN_LOGIN);

            //Form submitted
            if ($this->input->post('data')) {

                //Get post data
                $arr_post_data = $this->input->post('data');

                //Update config data
                $config_data = array();
                $config_data['website_url'] = $arr_post_data['website_url'];
                $config_data['website_title'] = $arr_post_data['website_title'];
                $config_data['footer_note'] = $arr_post_data['footer_note'];
                $config_data['email_name'] = $arr_post_data['email_name'];
                $config_data['email_password'] = $arr_post_data['email_password'];

                //Upload image
                $upload_image = $this->upload_images($config_data, $arr_login_user);
                if ($upload_image !== false) {
                    //Update config data
                    $config_data = $upload_image;
                    //Convert config array to string
                    //$str_config_data = $this->convert_to_string($config_data);

                    //Update into db
                    foreach ($config_data as $key => $value) {
                        $this->setting_model->update_existing_data(array(
                            'user_id' => $arr_login_user['user_id'],
                            'alias' => $key
                                ), array(
                            'value' => $value
                        ));
                    }
                    
                    // $this->user_model->update_existing_data(array(
                    //     'user_id' => $arr_login_user['user_id']
                    //         ), array(
                    //     'config' => $str_config_data
                    // ));
                    //Update login data
                    //$login_data = $this->session->userdata(SS_ADMIN_LOGIN);
                    //$arr_login_user['config'] = $str_config_data;
                    //$this->session->set_userdata(SS_ADMIN_LOGIN, $arr_login_user);
                    //Set success message
                    $this->arr_view_data['result'] = array(
                        'status' => 1,
                        'message' => 'Lưu thành công.'
                    );
                } else {
                    $this->arr_view_data['result'] = array(
                        'status' => 0,
                        'message' => 'Lưu không thành công.'
                    );
                }
            }
            
            //Set post data back to view
            $config_data = $this->setting_model->get_list_data(null, array(
                'user_id' => $arr_login_user['user_id']
            ));
            $this->arr_view_data['data'] = $config_data;
            if ($this->input->post('data')) {
                $this->arr_view_data['setting'] = $config_data;
            }

            //Set layout
            $this->layout->load('admin', 'setting/manager', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    /*
     * Get config data
     */
    // function get_config_data($arr_login_user) {
    //     //Get config data
    //     $arr_data = $this->user_model->get_single_data(array(
    //         'user_id' => $arr_login_user['user_id'],
    //         'type' => 1
    //     ));
    //     //Set post data back to view
    //     return $this->convert_to_array($arr_data['config']);
    // }
    // function get_config_data($arr_data = null, $name = null) {
    //     //Valid input
    //     if($arr_data === null || $name === null){
    //         return '';
    //     }
    //     //Get config data
    //     foreach ($arr_data as $value) {
    //         if($value['alias'] === $name){
    //             return $value['value'];
    //         }
    //     }
    //     return '';
    // }

    /*
     * Convert config array to string
     */
    function convert_to_string($arr_post_data) {
        $config_data = '';
        $count = 1;
        foreach ($arr_post_data as $key => $value) {
            if ($count == count($arr_post_data)) {
                $config_data .= $key . ':' . $value;
            } else {
                $config_data .= $key . ':' . $value . '||';
            }
            $count++;
        }
        unset($value);
        return $config_data;
    }

    /*
     * Upload images
     */
    function upload_images($config_data, $arr_login_user) {
        $is_upload_success = true;
        if ($_FILES['logo']['name'] != '') {
            $upload_logo = upload_images($this, $arr_login_user['username'], 'logo');
            if ($upload_logo['status'] == false) {
                $is_upload_success = false;
            } else {
                $config_data['logo'] = USER_UPLOAD_PATH . $arr_login_user['username'] . '/' . $upload_logo['data']['upload_data']['file_name'];
            }
        }
        //Upload favicon
        if ($is_upload_success && $_FILES['favicon']['name'] != '') {
            $upload_favicon = upload_images($this, $arr_login_user['username'], 'favicon');
            if ($upload_favicon['status'] == false) {
                $is_upload_success = false;
            } else {
                $config_data['favicon'] = USER_UPLOAD_PATH . $arr_login_user['username'] . '/' . $upload_favicon['data']['upload_data']['file_name'];
            }
        }

        if ($is_upload_success) {
            return $config_data;
        } else {
            return false;
        }
    }

}
