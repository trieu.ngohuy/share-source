<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Episodes extends ZLFront_Controller {
    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Load model
        $this->load->model('content_model');
        $this->load->model('season_model');
        $this->load->model('episodes_model');
        $this->load->model('state_model');
        $this->load->model('category_model');
        $this->load->model('comment_model');
        $this->load->model('user_model');
    }

    /**
     * Index Page for this controller.
     */
    public function detail($alias) {
        try {

            //Post comment
            if ($this->input->post('comment')) {
                //Get post data
                $arr_post_data = $this->input->post('comment');
                //Insert comment
                $this->comment_model->insert_new_data(
                    array(
                        'parent_id' => $arr_post_data['parent_id'],
                        'content_id' => $arr_post_data['content_id'],
                        'content_episodes_id' => $arr_post_data['content_episodes_id'],
                        'full_text' => $arr_post_data['full_text'],
                        'user_id' => $this->arr_view_data['login_data']['user_id'],
                        'created_date' => strtotime(date(DATE_FORMAT)),
                    )
                );
            }

            //Get detail
            $obj_data = $this->episodes_model->get_single_data(array(
                'alias' =>$alias
            ));
            $obj_data['full_text'] = preg_replace("/[\\n\\r]+/", "", htmlspecialchars_decode($obj_data['full_text']));
            $obj_data['footer_note'] = preg_replace("/[\\n\\r]+/", "", htmlspecialchars_decode($obj_data['footer_note']));

            //Get season detail
            $obj_season = $this->season_model->get_single_data(array(
                'content_season_id' =>$obj_data['content_season_id']
            ));
            $this->arr_view_data['season'] = $obj_season;

            //Get previous and next chapter
            $arr_episodes = $this->episodes_model->get_list_data(null, array(
                'content_season_id' =>$obj_season['content_season_id']
            ));
            $active_index = 0;
            $previous_url = '';
            $next_url = '';
            foreach ($arr_episodes as $key => $value) {
                if($value['alias'] === $alias){
                    $active_index = $key;
                }
            }
            if(($active_index - 1) >= 0){
                $previous_url = $arr_episodes[$active_index - 1]['url'];
            }
            if(($active_index + 1) <= (count($arr_episodes) - 1)){
                $next_url = $arr_episodes[$active_index + 1]['url'];
            }
            $this->arr_view_data['previous_url'] = $previous_url;
            $this->arr_view_data['next_url'] = $next_url;

            //Get content detail
            $obj_content = $this->content_model->get_single_data(array(
                'content_id' =>$obj_season['content_id']
            ));
            $this->arr_view_data['content'] = $obj_content;
            //Update view
            $this->content_model->update_existing_data(
                array('content_id' => $obj_content['content_id']),
                array('view' => $obj_content['view'] + 1)
            );

            //Get comment
            $arr_comment = $this->comment_model->get_list_data(null,
                array(
                    'content_episodes_id' => $obj_data['content_episodes_id']
                )
            );
            $arr_tmp_comment = array();
            foreach ($arr_comment as $key => $value) {

                if($value['parent_id'] !== "0"){
                    continue;
                }
                //get user full name
                $tmp = $this->user_model->get_single_data(array(
                    'user_id' => $value['user_id']
                ));
                $value['full_name'] = $tmp['full_name'];
                //Get detail
                $arr_detail = array();
                foreach ($arr_comment as $key => &$value2) {
                    if($value['comment_id'] === $value2['parent_id']){
                        //get user full name
                        $tmp = $this->user_model->get_single_data(array(
                            'user_id' => $value2['user_id']
                        ));
                        $value2['full_name'] = $tmp['full_name'];
                        $arr_detail[] = $value2;
                    }
                }
                $value['detail'] = $arr_detail;
                $arr_tmp_comment[] = $value;
            }
            //$arr_comment = recursive_data($arr_comment, 0, array());
            $this->arr_view_data['comment'] = $arr_tmp_comment;

            //Set post data back to view
            $this->arr_view_data['data'] = $obj_data;
            
            //Website title
            $this->arr_view_data['website_title'] = $obj_data['title'];

            //Set layout
            $this->layout->load('episodes', 'episodes/frontend/detail', $this->arr_view_data);
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
}
