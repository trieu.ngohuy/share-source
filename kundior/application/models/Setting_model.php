<?php

class Setting_model extends Zl_model {

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct('setting', 'Setting_object');
    }

    /*
     * Get single data
     */

    function get_single_data($arrCondition = null, $arrJoin = null) {
        return parent::get_single_data($arrCondition, $arrJoin);
    }

    /*
     * Get list data
     */

    function get_list_data($select = null, $arrCondition = null, $arrJoin = null, $count = null, $paging = null) {
        return parent::get_list_data($select, $arrCondition, $arrJoin, $count, $paging);
    }

    /*
     * Insert new data
     */

    function insert_new_data($arrData = null) {
        return parent::insert_new_data($arrData);
    }

    /*
     * Update existing data
     */

    function update_existing_data($arrCondition = null, $arrData = null) {
        return parent::update_existing_data($arrCondition, $arrData);
    }

    /*
     * Update existing data
     */

    function delete_data($arrCondition = null) {
        return parent::delete_data($arrCondition);
    }
}
