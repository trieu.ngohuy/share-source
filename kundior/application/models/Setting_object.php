<?php

class Setting_object {

    private $setting_id;
    private $setting_gid;
    private $lang_id;
    private $user_id;
    private $alias;
    private $title;
    private $value;
    private $enabled;
    private $created_date;
    private $url;

    /*
     * Get title
     */

    function title() {
        return $this->title;
    }

    /*
     * Get value
     */

    function value() {
        return $this->value;
    }

    /*
     * Get url
     */

    function url() {
        return $this->url;
    }

    /*
     * Set url
     */

    function set_url($url) {
        $this->url = $url;
    }

}
