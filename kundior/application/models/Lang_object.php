<?php

class Lang_object {

    private $lang_id;
    private $code;
    private $title;
    private $logo;
    private $enabled;
    private $default;
    private $created_date;
    public $url;

    /*
     * Get title
     */

    function title() {
        return $this->title;
    }

    /*
     * Get code
     */

    function code() {
        return $this->code;
    }

    /*
     * Get url
     */

    function url() {
        return $this->url;
    }

    /*
     * Set url
     */

    function set_url($url) {
        $this->url = $url;
    }

}
