<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ZLFront_Controller extends CI_Controller {

    protected $arr_view_data = array();

    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();

        //Check if user login
        if (!$this->session->userdata(SS_LOGIN)) {
            $this->arr_view_data['is_login'] = false;
        }else{
            $this->arr_view_data['is_login'] = true;
        }
        //Get login user
        $arr_login_user = $this->session->userdata(SS_LOGIN);
        $this->arr_view_data['login_data'] = $arr_login_user;
        //Load model
        $this->load->model('setting_model');
        $config_data = $this->setting_model->get_list_data();
        $this->arr_view_data['setting'] = $config_data;
        //Load state for header
        $this->load->model('state_model');
        $state = $this->state_model->get_list_data();
        $this->arr_view_data['state'] = $state;

        //Get list content for ranking
        $this->load->model('content_model');
        $content = $this->content_model->get_list_data();
        sort_array($content, 'view');
        $this->arr_view_data['global_content'] = $content;

        try {
            //Get login user
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }
}

class ZLAdmin_Controller extends CI_Controller {

    protected $_arrData = array();

    /*
     * Constuctor function
     */

    function __construct() {
        parent::__construct();
        //Check if user login
        if (!$this->session->userdata(SS_ADMIN_LOGIN)) {
            redirect(URL_ADMIN_LOGIN, 'refresh');
        }
        //Get common data
        try {
            //Get login user
            $arr_login_user = $this->session->userdata(SS_ADMIN_LOGIN);
            $this->arr_view_data['login_data'] = $arr_login_user;
            //Load model
            $this->load->model('setting_model');
            $config_data = $this->setting_model->get_list_data(null, array(
                'user_id' => $arr_login_user['user_id']
            ));
            $this->arr_view_data['setting'] = $config_data;
        } catch (Exception $exc) {
            custom_exception($this, $exc);
        }
    }

    /*
     * Convert config string to array
     */
    function convert_to_array($arr_tmp) {
        if ($arr_tmp != '') {
            $arr_config = array();
            $arr_tmp = explode('||', $arr_tmp);
            foreach ($arr_tmp as $value) {
                $arr_value = explode(':', $value);
                $arr_config[$arr_value[0]] = $arr_value[1];
            }
            unset($value);
        }
        return $arr_config;
    }

}
