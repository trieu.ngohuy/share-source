<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quản lý người dùng
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header al-right">
                    <button type="button" id="btn_new" class="btn btn-primary">Thêm mới</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tên đầy đủ</th>
                                <th>Loại người dùng</th>
                                <th>Tên đăng nhập</th>
                                <th>Email</th>
                                <th>Kích hoạt</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($data as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $value['full_name']; ?></td>
                                    <td><?php
                                        if ($value['type'] == 1) {
                                            echo 'Quản trị viên';
                                        } else {
                                            echo 'Người đọc';
                                        }
                                        ?></td>
                                    <td><?php echo $value['username']; ?></td>
                                    <td><?php echo $value['email']; ?></td>
                                    <td><?php 
                                    if ($value['enabled'] == "0"){
                                        echo 'Chưa kích hoạt';
                                    } else{
                                        echo 'Đã kích hoạt';
                                    }
                                    ?></td>
                                    <td>
                                        <a href="#" class="btn-edit" data-index="<?php echo $count - 1; ?>"><i class="fa fa-edit"></i> Sửa</a>
                                        <a href="#" class="btn-remove" data-index="<?php echo $count - 1; ?>"><i class="fa fa-remove"></i> Xóa</a>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Tên đầy đủ</th>
                                <th>Loại người dùng</th>
                                <th>Tên đăng nhập</th>
                                <th>Email</th>
                                <th>Kích hoạt</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade" id="modal-data">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Tên đầy đủ</label>
                    <input id="txt_fullname" type="text" class="form-control" placeholder="Tên đầy đủ">
                </div>
                <div class="form-group">
                    <label>Tên đăng nhập</label>
                    <input id="txt_username" type="text" class="form-control" placeholder="Tên đằng nhập">
                    <p class="help-block">Tên đăng nhập không được trùng.</p>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input id="txt_email" type="email" class="form-control" placeholder="Email">
                    <p class="help-block">Email không được trùng.</p>
                </div>
                <div class="form-group">
                    <label>Mật khẩu</label>
                    <input id="txt_password" type="text" class="form-control" placeholder="Nhập mật khẩu">
                </div>
                <div class="form-group">
                    <label>Loại người dùng</label>
                    <select id="com_type" class="form-control select2" style="width: 100%;">
                        <option value="0">Người đọc</option>
                        <option value="1">Quản trị viên</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Kích hoạt</label>
                      <div class="radio">
                        <label>
                          <input type="radio" name="enabled" value="1" checked="" id="rad_active">Có
                        </label>
                        <label>
                          <input type="radio" name="enabled" value="0" id="rad_unactive">Không
                        </label>
                      </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="btn-save">Lưu</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Style -->
<style>
    .select2-container .select2-selection--single {
        height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 31px;
    }
</style>
<!-- page script -->
<script>
    //Convert php array to js array
<?php
$js_array = json_encode($data);
echo "var arr_data = " . $js_array . ";\n";
?>
    var mode = 'new';
    var user_id = 0;
    $(function () {
        //Initialize datatable
        $('#example1').DataTable();
    });
    $(document).ready(function () {

        //Click new button
        $(document).on('click', '#btn_new', function (e) {
            //Update mode
            mode = 'new';
            //Set title
            $('.modal-title').html('Thêm mới');
            //Project id
            user_id = 0;
            //Reset modal data
            $("#txt_fullname").val('');
            $("#txt_username").val('');
            $("#txt_email").val('');
            $("#txt_password").val('');
            $('#com_type').val($('#com_type option:eq(0)').val()).trigger('change')
            $('#rad_active').prop("checked", true);
            //Open modal
            $("#modal-data").modal();
        });
        //Click remove button
        $(document).on('click', '.btn-remove', function (e) {
            if (confirm("Are you sure you want to delete this?")) {
                //Get id
                var index = $(this).attr('data-index');
                //Get edit data
                var obj_data = arr_data[index];
                //Project id
                user_id = obj_data['user_id'];
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url() ?>/AdminUser/delete",
                    data: {
                        'user_id': user_id
                    },
                    success: function (objData) {
                        //Refresh data
                        refresh_data();
                    },
                    error: function (jqXHR, exception) {
                        alert('Có lỗi. Thử lại sau.');
                    }
                });
            }
        });
        //Click edit button
        $(document).on('click', '.btn-edit', function (e) {
            //Update mode
            mode = 'edit';
            //Get id
            var index = $(this).attr('data-index');
            //Set title
            $('.modal-title').html('Chỉnh sửa');
            //Get edit data
            var obj_data = arr_data[index];
            //Project id
            user_id = obj_data['user_id'];
            //Fill modal data
            $("#txt_fullname").val(obj_data['full_name']);
            $("#txt_username").val(obj_data['username']);
            $("#txt_email").val(obj_data['email']);
            if (user_id == 0) {
                $('#com_type').val($('#com_type option:eq(0)').val()).trigger('change');
            } else {
                $("#com_type").val(parseInt(obj_data['type'])).trigger('change');
            }
            if(obj_data['enabled'] == "1"){
                $('#rad_active').prop("checked", true);
            }else{
                $('#rad_unactive').prop("checked", true);
            }
            //Open modal
            $("#modal-data").modal();
        });

        //Modal save button click
        $('#btn-save').click(function () {

            //If add new then check if duplicate alias
            if(verify_data() === false){
                return;
            }

            //Hide modal
            $('#modal-data').modal('toggle');
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url() ?>/AdminUser/execute_query",
                data: {
                    'user_id': user_id,
                    'full_name': $('#txt_fullname').val(),
                    'username': $('#txt_username').val(),
                    'email': $('#txt_email').val(),
                    'password': $('#txt_password').val(),
                    'type': $('#com_type').val(),
                    'enabled': $('input[name=enabled]:checked').val(),
                },
                success: function (objData) {
                    //Refresh data
                    refresh_data();
                },
                error: function (jqXHR, exception) {
                    alert('Có lỗi. Thử lại sau.');
                }
            });
        }
        );
    });

    //Verify data
    function verify_data() {
        //Check empty input
        if ($('#txt_fullname').val() == '') {
            alert('Tên đầy đủ không được rỗng!');
            return false;
        }
        if ($('#txt_username').val() == '') {
            alert('Tên đăng nhập không được rỗng!');
            return false;
        }
        if ($('#txt_email').val() == '') {
            alert('Email không được rỗng!');
            return false;
        }
        if (user_id == 0 && $('#txt_fullname').val() == '') {
            alert('Mật khẩu không được rỗng!');
            return false;
        }

        //Check email format
        if(is_email($('#txt_email').val()) == false){
            alert('Định dạng email không đúng!');
            return false;
        }

        //Check duplicate data
        var result = $.grep(arr_data, function (e) {
            return e.username === $('#txt_username').val() && e.user_id !== user_id;
        });
        if (result.length > 0) {
            alert('Tên đăng nhập trùng!');
            return false;
        }
        result = $.grep(arr_data, function (e) {
            return e.email === $('#txt_email').val() && e.user_id !== user_id;
        });
        if (result.length > 0) {
            alert('Email trùng!');
            return false;
        }
    }
    /*
     * Refill datatable data
     */
    function refresh_data() {
        //Refresh data
        $.ajax({
            type: 'GET',
            url: "<?php echo base_url() ?>/AdminUser/get_data",
            success: function (data) {
                data = $.parseJSON(data);
                arr_data = data;
                //Fill datatable
                $('#example1 tbody').html('');
                var html = '';
                for (var i = 0; i < data.length; i++) {
                    html += '<tr>'
                            + '<td>' + (i + 1) + '</td>'
                            + '<td>' + data[i]['full_name'] + '</td>'
                            + '<td>' + (data[i]['type'] === '1' ? 'Quản trị viên' : 'Người đọc') + '</td>'
                            + '<td>' + data[i]['username'] + '</td>'
                            + '<td>' + data[i]['email'] + '</td>'
                            + '<td>' + (data[i]['enabled'] == "0" ? "Chưa kích hoạt" : "Đã kích hoạt") + '</td>'
                            + '<td>'
                            + '<a href="#" class="btn-edit" data-index="' + i + '"><i class="fa fa-edit"></i> Sửa</a>'
                            + ' <a href="#" class="btn-remove" data-index="' + i + '"><i class="fa fa-remove"></i> Xóa</a>'
                            + '</td>'
                            + '</tr>';
                }
                $('#example1 tbody').html(html);
                //Initialize datatable
                $('#example1').DataTable();

            },
            error: function () {
                alert('Có lỗi. Thử lại sau.');
            }
        });
    }
</script>