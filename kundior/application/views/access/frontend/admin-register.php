
<!--Main layout-->
<main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-2 mb-4"></div>
            <!-- Default form login -->
            <div class="col-md-8 mb-4 bk-white border border-light">
                <form id="frmSubmit" method="post" class="text-center p-5 bk-white">

                    <p class="h4 mb-4">Đăng ký</p>

                    <?php
                    if (isset($result)) {
                        if ($result['status'] == 0) {
                            ?>
                            <div class="alert alert-danger al-left" role="alert">
                                <?php echo $result['message'] ?>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-success" role="alert">
                                <?php echo $result['message'] ?>
                            </div>
                            <?php
                        }
                    }
                    ?>

                    <!-- Full name -->
                    <input type="text" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="Tên đầy đủ" name="data[full_name]" required="" minlength="10"
                    <?php
                    if (isset($arr_post_data['full_name'])) {
                        echo 'value="' . $arr_post_data['full_name'] . '"';
                    }
                    ?>
                           >

                    <!-- Email -->
                    <input type="email" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="Email" name="data[email]" required=""
                    <?php
                    if (isset($arr_post_data['email'])) {
                        echo 'value="' . $arr_post_data['email'] . '"';
                    }
                    ?>
                           >

                    <!-- Username -->
                    <input type="text" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="Tên đăng nhập" name="data[username]" required="" minlength="5"
                    <?php
                    if (isset($arr_post_data['username'])) {
                        echo 'value="' . $arr_post_data['username'] . '"';
                    }
                    ?>
                           >

                    <!-- Password -->
                    <input type="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="Password" name="data[password]" required="" minlength="6"
                    <?php
                    if (isset($arr_post_data['password'])) {
                        echo 'value="' . $arr_post_data['password'] . '"';
                    }
                    ?>
                           >

                    <!-- Confirm Password -->
                    <input type="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="Password" name="data[confirm_password]" required="" minlength="6"
                    <?php
                    if (isset($arr_post_data['confirm_password'])) {
                        echo 'value="' . $arr_post_data['confirm_password'] . '"';
                    }
                    ?>
                           >

                    <!-- Sign in button -->
                    <button class="btn btn-info btn-block my-4" type="submit">Đăng ký</button>

                </form>
            </div>
            <!-- Default form login -->
            <div class="col-md-2 mb-4"></div>
        </div>
    </div>
</main>