<div class="btitle">Đăng nhập</div>
<br>
<?php
if (isset($result)) {
    ?>
    <div class="btitle alert-div" style="background: red;">
        <p>Lỗi!</p>
        <?php echo $result['message'] ?>
    </div>
    <?php
}
?>
<div class="bcontent">
    <form method="post">
        <div class="search-box">Tên đăng nhập
            <input type="text" name="data[username]" value="" spellcheck="false">
        </div>
        <div class="search-box">Mật khẩu
            <input type="password" name="data[password]" value="" spellcheck="false">
        </div>
        <div class="search-box">
            <a href="<?php echo base_url() . URL_FORGET_PASSWORD?>" style="color: #fff;">Quên mật khẩu?</a><br>
            <a href="<?php echo base_url() . URL_REGISTER?>" style="color: #fff;">Chưa có tài khoản? Hãy đăng ký ngay.</a>
        </div>
        <button class="seemore" type="submit">Đăng nhập</button>
    </form>
</div>