<style>
.sub-chapter{
    background: #252323;
}
</style>
<h1 id="storytitle"><?php echo $data['title']?></h1>
    <div id="sinfo">
        <div class="box">
            <div class="btitle">THÔNG TIN</div>
            <div class="bcontent">
                <div class="image" style="background-image: url(&quot;<?php echo base_url() . $data['logo']?>&quot;), url(<?php echo asset_front_url() . IMG_DEFAULT_ITEM?>);"></div>
                <div class="field">
                    <div class="key">TỔNG SỐ TỪ</div>
                    <div class="value"> <?php echo $sum_word_count?></div>
                </div>
                <?php if ($data['participator'] !== ''){
                	?>
                		<div class="field">
		                    <div class="key">THAM GIA</div>
		                    <div class="value"> <?php echo $data['participator']?></div>
		                </div>
                	<?php
                }?>
                <?php if($data['author'] !== ''){
                	?>
                		<div class="field">
		                    <div class="key">TÁC GIẢ</div>
		                    <div class="value"><?php echo $data['author']?></div>
		                </div>
                	<?php
                }?>
                
                <?php if($data['article'] !== ''){
                	?>
                		<div class="field">
		                    <div class="key">HỌA SĨ</div>
		                    <div class="value"><?php echo $data['article']?></div>
		                </div>
                	<?php
                }?>
                
                <?php if($data['language'] !== ''){
                	?>
                		<div class="field">
		                    <div class="key">NGÔN NGỮ</div>
		                    <div class="value"><?php echo $data['language']?></div>
		                </div>
                	<?php
                }?>
                
                <div class="field">
                    <div class="key">THỂ LOẠI</div>
                    <div class="value">
                    	<?php echo $data['category']?>
                    </div>
                </div>

                <?php if($data['type'] !== ''){
                	?>
                		<div class="field">
		                    <div class="key">LOẠI TRUYỆN</div>
		                    <div class="value"><?php echo $data['type']?></div>
		                </div>
                	<?php
                }?>
                
                <div class="field">
                    <div class="key">TÌNH TRẠNG</div>
                    <div class="value"><a href="<?php echo $data['state']['url']?>"><?php echo $data['state']['title']?></a></div>
                </div>
                <?php if($data['progress'] !== ''){
                	?>
                	<div class="field">
	                    <div class="key">TIẾN ĐỘ</div>
	                    <div class="value"><?php echo $data['progress']?></div>
	                </div>
                	<?php	
                }?>
                
                <div class="field">
                    <div class="key">LƯỢT XEM</div>
                    <div class="value"><?php echo $data['view']?></div>
                </div>
                
                <?php
                if($data['title_en'] !== ""){
                    ?>
                        <div class="field">
                            <div class="key">TÊN TIẾNG ANH</div>
                            <div class="value"><?php echo $data['title_en']?></div>
                        </div>
                    <?php
                }
                ?>

                <?php
                if($data['title_ja'] !== ""){
                    ?>
                        <div class="field">
                            <div class="key">TÊN TIẾNG NHẬT</div>
                            <div class="value"><?php echo $data['title_ja']?></div>
                        </div>
                    <?php
                }
                ?>
                <?php
                if($data['title_romaji'] !== ""){
                    ?>
                        <div class="field">
                            <div class="key">TÊN ROMAJI</div>
                            <div class="value"><?php echo $data['title_romaji']?></div>
                        </div>
                    <?php
                }
                ?>

                <?php
                if($data['translator'] !== ""){
                	?>
						<div class="field">
		                    <div class="key">NHÓM DỊCH</div>
		                    <div class="value"><?php echo $data['translator']?></div>
		                </div>
                	<?php
                }
                ?>
                <?php if($data['note'] !== ''){
                	?>
                		<div class="field">
		                    <div class="key">GHI CHÚ</div>
		                    <div class="value"><?php echo $data['note']?></div>
		                </div>
                	<?php
                }?>
                
            </div>
        </div>
    </div>
    <div id="sright">
        <div class="box" id="sdetail">
            <div class="btitle">MÔ TẢ</div><input id="opdetail" type="checkbox">
            <div class="bcontent" id="full_text_wrap">
                
            </div>
            <div class="bmore"><label for="opdetail">XEM THÊM</label></div>
        </div>
        <div class="box" id="svol">
            <div class="btitle">DANH SÁCH TẬP</div>
            <div class="bcontent">
            	<?php
            		foreach ($data['detail'] as $key => $value) {
            			?>
            				<input id="vol-<?php echo $key?>" type="radio" name="vol" checked="false">
			                <div class="vol"><label class="vtitle" for="vol-<?php echo $key?>"><?php echo $value['title']?></label>
				                    <div class="vinfo">
				                        <div class="image" style="background-image: url(&quot;<?php echo base_url().$value['logo']?>&quot;), url(<?php echo asset_front_url() . IMG_DEFAULT_ITEM ?>);"></div>
				                    </div>
				                    <div class="clist">
				                    	<?php
				                    		foreach ($value['detail'] as $key => $value2) {
				                    			?>
				                    				<a class="chapter" href="<?php echo $value2['url']?>">
							                            <div class="ctitle"><?php echo $value2['title']?></div>
							                            <div class="cdate"><?php echo $value2['created_date']?>
                                                        </div>
							                        </a>
                                                    <?php
                                                        if(count($value2['detail']) > 0){
                                                            ?>
                                                                <div class="sub-chapter">
                                                                    <?php
                                                                        foreach ($value2['detail'] as $key => $value3) {
                                                                            ?>
                                                                                <a class="chapter" href="<?php echo $value3['url']?>" style="padding-left: 20px;">
                                                                                    <div class="ctitle"><?php echo $value3['title']?></div>
                                                                                    <div class="cdate"><?php echo $value3['created_date']?></div>
                                                                                 </a>
                                                                            <?php
                                                                        }
                                                                    ?>
                                                                </div>
                                                            <?php
                                                        }
                                                    ?>
				                    			<?php
				                    		}
				                    	?>
				                        
				                </div>
				            </div>
            			<?php
            		}
            	?>
            	
        </div>
        <?php $this->load->view('elements/frontend/comment'); ?> 
    </div>
    <input id="op-sadult" type="checkbox" checked="checked">
    <div id="sadult" <?php if($data['is_adult'] === '0'){ echo 'style="display: none;"';}?>>
        <div class="title">CẢNH BÁO</div><label class="close" for="op-sadult"></label>
        <div class="content">
            <p>Truyện có chứa yếu tố <b>người lớn</b>, vui lòng cân nhắc trước khi xem.</p>
            <p><b><i>Con nít?</i></b> Tắt máy đi ngủ ngay!</p>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        <?php
        $js_array = json_encode($data);
        echo "var obj_data = " . $js_array . ";\n";
        ?>
    	$(document).ready(function(){
            
            var full_text = obj_data['full_text'];
            full_text = full_text.replace(/\\n/g, "");
            $('#full_text_wrap').html(full_text);

    		//reply button click
    		$('.btn_reply').click(function(){
    			//get id
    			var id = $(this).attr('data-id');
    			$('#frm-' + id).css('display', 'block');
    		});
    		//cancle button click
    		$('.btn_cancle').click(function(){
    			//get id
    			var id = $(this).attr('data-id');
    			$('#frm-' + id).css('display', 'none');
    		});
    	});
    </script>