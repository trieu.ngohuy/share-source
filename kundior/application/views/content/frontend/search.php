<div class="btitle">TÌM KIẾM: <?php echo $key?></div>
<div class="bcontent">
	<div class="search-box">
		<input type="text" name="search[key]" value="" style="width: 100%" id="txt_key">
		<button style="float: right; margin-top: 10px; display: none;" id="btn_search">TÌM</button>
		
	</div>
	<div class="gridlist">
		
	</div>
	<div class="glmore">
		
	</div>
</div>
<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script type="text/javascript">
	<?php
	$js_array = json_encode($data);
	echo "var arr_data = " . $js_array . ";\n";
	?>
	
	var page_max_item = '<?php echo PAGE_MAX_ITEMS?>';
	page_max_item = parseInt(page_max_item);

	$(document).ready(function(){
		display_data(1, arr_data);
		//on input
		$("#txt_key").on('input', function () {
            var key = $(this).val();
            var search_data = search_in_array(key);
			display_data(1, search_data);
        });
		//Search
		// $('#btn_search').click(function(){
		// 	var key = $('#txt_key').val();
		// 	var search_data = search_in_array(key);
		// 	display_data(1, search_data);
		// });
	});
	//search in array
	function search_in_array(key){
		tmp = [];
		for(var i = 0 ; i < arr_data.length ; i++){
			if(arr_data[i]['title'].toLowerCase().indexOf(key) >= 0){
				tmp.push(arr_data[i]);
			}
		}
		return tmp;
	}
	//Display data
	function display_data(page, arr_data){
		var html = '';
		var max_item = parseInt(page) * page_max_item;
		var min_item = max_item - page_max_item + 1;
		for(var i = 0 ; i <arr_data.length ; i++){
			if(min_item <= (i+1) <= max_item){
				html += '<div class="glitem">'+
					'<a class="image" href="' +arr_data[i]['url']+ '" style="background-image: url(&quot;<?php echo base_url();?>' +arr_data[i]['logo']+ '&quot;), url(<?php echo base_url() . IMG_DEFAULT_ITEM?>);"></a>'+
					'<div class="info">'+
					'	<a class="story" href="' +arr_data[i]['url']+ '">' +arr_data[i]['title']+ '</a>'+
					'</div>'+
				'</div>';
			}			
		}
		$('.gridlist').html(html);
		//Display paging
		display_paging(page, arr_data);
	}
	//Display paging
	function display_paging(page, arr_data){
		var total_count = arr_data.length;
		var count = 0;
		var html = '';
		var index = 0;
		while(index < total_count){
			count++;
			html += '<a class="page button ' +(page === count ? "current" : "")+ '" href="#" onclick="display_data(' + count + ')">' + count + '</a>';
			index += page_max_item;
		}
		$('.glmore').html(html);
	}
</script>