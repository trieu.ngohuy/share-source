<!--div id="fb-root"></div><script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.12&appId=783908218461381&autoLogAppEvents=1';fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script-->
<header id="header" login="false">
    <div class="logo">
        <div class="image" style="background-image: url(<?php echo base_url() . get_setting_value($setting, 'logo'); ?>)"></div>
        <div class="tbox">
            <div class="title"><?php echo get_setting_value($setting, 'website_title'); ?></div>
        </div>
    </div>
</header><input id="opmenu" type="checkbox"><label for="opmenu" login="false"></label>
<div class="menu" login="false">
    <div class="mtitle">DANH MỤC</div>
    <a class="item" href="<?php echo base_url(). URL_HOME?>">TRANG CHỦ</a>
    <input id="mn-pr" type="checkbox" name="mn-pr">
    <a class="item" href="<?php echo base_url(). URL_PROJECT?>">DỰ ÁN</a>
    <a class="item" href="<?php echo base_url(). URL_SEARCH?>">TÌM KIẾM</a>
    <a class="item" href="<?php echo base_url(). URL_CATEGORY?>">THỂ LOẠI</a>
    <input id="mn-st" type="checkbox" name="mn-st">
    <label class="item" for="mn-st">TÌNH TRẠNG</label>
    <div class="submenu" for="mn-st">
        <?php
        foreach ($state as $key => $value) {
            ?>
            <a class="sitem" href="<?php echo $value['url']?>"><?php echo mb_strtoupper($value['title'])?></a>
            <?php
        }
        ?>
    </div>
    <?php
        if($is_login){
            ?>
                <a class="item" href="<?php echo base_url().URL_LOGOUT;?>">ĐĂNG XUẤT</a>
            <?php
        }else{
            ?>
                <a class="item" href="<?php echo base_url().URL_LOGIN?>">ĐĂNG NHẬP</a>
            <?php
        }
    ?>
    <a class="item" href="#">ĐĂNG NHẬP</a>
</div>
<div class="addspace"></div>