<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2018 <a href="https://adminlte.io">Zola Web Group. Email: zolawebgroup@gmail.com. Skype: zolawebgroup</a>.</strong> All rights
    reserved.
</footer>