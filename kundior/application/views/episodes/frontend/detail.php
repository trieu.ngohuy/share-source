<div id="read">
        <div class="info">
            <div class="ttitle"><?php echo $content['title']?></div>
            <h1 class="title"><?php echo $data['title']?></h1>
            <div class="stitle"><?php echo $season['title']?></div>
            <div class="stitle">Tổng số từ: <?php echo $data['word_count']?></div>
        </div>
        <div class="content noselect">
            <hr>
            <div class="content-footer" style="text-align: center;"></div>
        </div>
        <div class="direct"><a class="prev" href="<?php echo $previous_url?>">CHƯƠNG TRƯỚC</a>
            <a class="cur" href="<?php echo $content['url']?>">MỤC LỤC</a>
            <a class="next" href="<?php echo $next_url?>">CHƯƠNG SAU</a></div>
    </div>
    <div class="hidden" id="qdirect">
        <div class="direct"><a class="prev" href="<?php echo $previous_url?>" style="background-image: url(<?php echo asset_front_url() ?>img/left-button.png)"></a><a class="cur" href="<?php echo $content['url']?>" style="background-image: url(<?php echo asset_front_url() ?>img/menu-button.png)"></a>
            <a
                class="next" href="<?php echo $next_url?>" style="background-image: url(<?php echo asset_front_url() ?>img/right-button.png)"></a><label for="op-config" style="background-image: url(<?php echo asset_front_url() ?>img/config-button.png)"></label></div><input id="op-config" type="checkbox">
        <div class="config">
            <div class="field"><label>MÀU SẮC</label><input id="cfcl-default" type="radio" name="cfcl" onchange="updateColor()"><label class="color" for="cfcl-default"></label><input id="cfcl-light" type="radio" name="cfcl" onchange="updateColor()"><label class="color"
                    for="cfcl-light"></label><input id="cfcl-wood" type="radio" name="cfcl" onchange="updateColor()"><label class="color" for="cfcl-wood"></label><input id="cfcl-yellow" type="radio" name="cfcl" onchange="updateColor()"><label class="color"
                    for="cfcl-yellow"></label><input id="cfcl-green" type="radio" name="cfcl" onchange="updateColor()"><label class="color" for="cfcl-green"></label></div>
            <div class="field"><label>CỠ CHỮ</label><input id="cf-fontsize" type="text"><button id="cf-ifontsize" onclick="increaseFontSize()">+</button><button id="cf-dfontsize" onclick="decreaseFontSize()">-</button></div>
            <div class="field"><label>LỀ</label><input id="cf-padding" type="text"><button id="cf-ipadding" onclick="increasePadding()">+</button><button id="cf-dpadding" onclick="decreasePadding()">-</button></div>
        </div>
    </div>
    <div id="other">
        <?php $this->load->view('elements/frontend/comment'); ?> 
    </div>
    <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        <?php
        $js_array = json_encode($data);
        echo "var obj_data = " . $js_array . ";\n";
        ?>
        $(document).ready(function(){
            var full_text = obj_data['full_text'];
            full_text = full_text.replace(/\\n/g, "");
            $('.content').prepend(full_text);

            var footer_note = obj_data['footer_note'];
            footer_note = footer_note.replace(/\\n/g, "");
            $('.content-footer').html(footer_note);
        });
    </script>