<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quản lý chương
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header al-right">
                    <button type="button" id="btn_new" class="btn btn-primary">Thêm mới</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Truyện</th>
                                <th>Tập</th>
                                <th>Chương</th>
                                <th>Ngày tạo</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($data as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $value['content']; ?></td>
                                    <td><?php echo $value['season']; ?></td>
                                    <td><?php echo $value['title']; ?></td>
                                    <td><?php echo $value['created_date']; ?></td>
                                    <td>
                                        <a href="#" class="btn-edit" data-index="<?php echo $count - 1; ?>"><i class="fa fa-edit"></i> Sửa</a>
                                        <a href="#" class="btn-remove" data-index="<?php echo $count - 1; ?>"><i class="fa fa-remove"></i> Xóa</a>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Truyện</th>
                                <th>Tập</th>
                                <th>Chương</th>
                                <th>Ngày tạo</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade" id="modal-data">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Dự án cha</label>
                    <select id="com_parent" class="form-control select2" style="width: 100%;">
                        <option value="0">---</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Tên chương</label>
                    <input id="txt_title" type="text" class="form-control" placeholder="Tên chương">
                </div>
                <div class="form-group">
                    <label>Tên rút gọn</label>
                    <input id="txt_alias" type="text" class="form-control" placeholder="Tên rút gọn">
                    <p class="help-block">Tên rút gọn không được trùng.</p>
                </div>
                <div class="form-group">
                    <label>Danh sách truyện</label>
                    <select id="com_content" class="form-control select2"data-placeholder="Chọn truyện"
                            style="width: 100%;">
                                <?php
                                foreach ($content as $value) {
                                    echo '<option value="' . $value['content_id'] . '">' . $value['title'] . '</option>';
                                }
                                ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Danh sách tập</label>
                    <select id="com_season" class="form-control select2"data-placeholder="Chọn tập"
                            style="width: 100%;">
                                <?php
                                foreach ($season as $value) {
                                    echo '<option value="' . $value['content_season_id'] . '" data-content-id="' . $value['content_id'] . '">' . $value['title'] . '</option>';
                                }
                                ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Nội dung</label>
                    <textarea id="edi_fulltext" name="editor1" rows="20" cols="80">
                    </textarea>
                </div>
                <div class="form-group">
                    <label>Ghi chú</label>
                    <textarea id="edi_footer_note" name="editor2" rows="10" cols="80">
                    </textarea>
                </div>
                <!-- <div class="form-group">
                    <label>Tiêu đề kết luận</label>
                    <input id="txt_footer_title" type="text" class="form-control" placeholder="Tiêu đề kết luận">
                </div>
                <div class="form-group">
                    <label>Ghi chú kết luận</label>
                    <input id="txt_footer_note" type="text" class="form-control" placeholder="Ghi chú kết luận">
                </div>
                <div class="form-group">
                    <label>Biên dịch</label>
                    <input id="txt_footer_translator" type="text" class="form-control" placeholder="Biên dịch">
                </div>
                <div class="form-group">
                    <label>Chỉnh sửa</label>
                    <input id="txt_footer_editor" type="text" class="form-control" placeholder="Chỉnh sửa">
                </div> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="btn-save">Lưu</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Style -->
<style>
    .select2-container .select2-selection--single {
        height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 31px;
    }
</style>
<!-- page script -->
<script>
    //Convert php array to js array
<?php
$js_array = json_encode($data);
$arr_season = json_encode($season);
echo "var arr_data = " . $js_array . ";\n";
echo "var arr_season = " . $arr_season . ";\n";
?>
    var mode = 'new';
    var content_episodes_id = 0;
    $(function () {
        //Initialize datatable
        $('#example1').DataTable();
        //Initialize Select2 Elements
        $('.select2').select2();
    });
    $(document).ready(function () {
        //Count character CKEDITOR.instances['edi_fulltext'].getData().replace(/<[^>]*>|\s/g, '').length
        //=> Check worked
        //Init ckeditor
        var editor = CKEDITOR.replace('editor1', {
            filebrowserBrowseUrl: '<?php echo libs_url() ?>ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '<?php echo libs_url() ?>ckfinder/ckfinder.html?Type=Images',
            filebrowserUploadUrl: '<?php echo libs_url() ?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '<?php echo libs_url() ?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        });
        CKFinder.setupCKEditor( editor );
        // CKEDITOR.replace('editor1', {
        //     toolbarGroups : [
        // 		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        // 		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        // 		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        // 		{ name: 'forms', groups: [ 'forms' ] },
        // 		'/',
        // 		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        // 		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        // 		{ name: 'links', groups: [ 'links' ] },
        // 		{ name: 'insert', groups: [ 'insert' ] },
        // 		'/',
        // 		{ name: 'styles', groups: [ 'styles' ] },
        // 		{ name: 'colors', groups: [ 'colors' ] },
        // 		{ name: 'tools', groups: [ 'tools' ] },
        // 		{ name: 'others', groups: [ 'others' ] },
        // 		{ name: 'about', groups: [ 'about' ] }
        // 	],
        //     removePlugins: 'easyimage',
        //     //extraPlugins = 'filebrowser',
        //     removeButtons : 'Source,Save,NewPage,Preview,Templates,Scayt,Form,Checkbox,Radio,TextField,Textarea,Button,Select,ImageButton,HiddenField,CreateDiv,Language,Flash,Iframe,PageBreak,HorizontalRule,About,Print,Image,SpecialChar,EasyImage'
        // });
        CKEDITOR.replace('editor2', {
            toolbarGroups : [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                { name: 'forms', groups: [ 'forms' ] },
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                { name: 'links', groups: [ 'links' ] },
                { name: 'insert', groups: [ 'insert' ] },
                '/',
                { name: 'styles', groups: [ 'styles' ] },
                { name: 'colors', groups: [ 'colors' ] },
                { name: 'tools', groups: [ 'tools' ] },
                { name: 'others', groups: [ 'others' ] },
                { name: 'about', groups: [ 'about' ] }
            ],
            removePlugins: 'easyimage',
            removeButtons : 'Source,Save,NewPage,Preview,Templates,Scayt,Form,Checkbox,Radio,TextField,Textarea,Button,Select,ImageButton,HiddenField,CreateDiv,Language,Flash,Iframe,PageBreak,HorizontalRule,About,Print,Image,SpecialChar,EasyImage'
        });
        //Title on change
        $("#txt_title").on('input', function () {
            $("#txt_alias").val(convert_vi_to_en($(this).val()));
        });
        //Click new button
        $(document).on('click', '#btn_new', function (e) {
            //Update mode
            mode = 'new';
            //Set title
            $('.modal-title').html('Thêm mới');
            //Project id
            content_episodes_id = 0;
            //Reset modal data
            $("#txt_title").val('');
            $("#txt_alias").val('');
            $("#txt_footer_note").val('');
            $("#txt_footer_title").val('');
            $("#txt_footer_translator").val('');
            $("#txt_footer_editor").val('');
            $("#com_content").val(null).trigger("change");
            $("#com_season").find('option').remove();
            CKEDITOR.instances['edi_fulltext'].setData('');
            CKEDITOR.instances['edi_footer_note'].setData('');
            //Add data into parent select
            $("#com_parent").find('option').not(':first').remove();
            for (var i = 0; i < arr_data.length; i++) {
                $("#com_parent").append(new Option(arr_data[i]['content'] + " - " + arr_data[i]['season'] + " - " + arr_data[i]['title'], arr_data[i]['content_episodes_id']));
            }

            $('#com_parent').val($('#com_parent option:eq(0)').val()).trigger('change');
            //Open modal
            $("#modal-data").modal();
        });
        //Click remove button
        $(document).on('click', '.btn-remove', function (e) {
            if (confirm("Are you sure you want to delete this?")) {
                //Get id
                var index = $(this).attr('data-index');
                //Get edit data
                var obj_data = arr_data[index];
                //Project id
                content_episodes_id = obj_data['content_episodes_id'];
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url() ?>/AdminEpisodes/delete",
                    data: {
                        'content_episodes_id': content_episodes_id
                    },
                    success: function (objData) {
                        //Refresh data
                        refresh_data();
                    },
                    error: function (jqXHR, exception) {
                        alert('Có lỗi. Thử lại sau.');
                    }
                });
            }
        });
        //Click edit button
        $(document).on('click', '.btn-edit', function (e) {
            //Update mode
            mode = 'edit';
            //Get id
            var index = $(this).attr('data-index');
            //Set title
            $('.modal-title').html('Chỉnh sửa');
            //Get edit data
            var obj_data = arr_data[index];
            //id
            content_episodes_id = obj_data['content_episodes_id'];
            var parent_gid = parseInt(obj_data['parent_gid']);
            //Get content id
            var result = $.grep(arr_season, function (e) {
                return e.content_season_id === obj_data['content_season_id'];
            });
            //Fill modal data
            $("#txt_title").val(obj_data['title']);
            $("#txt_alias").val(obj_data['alias']);
            $("#txt_footer_note").val(obj_data['footer_note']);
            $("#txt_footer_title").val(obj_data['footer_title']);
            $("#txt_footer_translator").val(obj_data['footer_translator']);
            $("#txt_footer_editor").val(obj_data['footer_editor']);
            $("#com_content").val(parseInt(result[0]['content_id'])).trigger('change');
            //Get list season
            get_list_season(result[0]['content_id']);
            var full_text = obj_data['full_text'];
            if(full_text !== '' && full_text !== null){
                full_text = full_text.replace(/\\n/g, "");    
            }
            CKEDITOR.instances['edi_fulltext'].setData(full_text);
            var footer_note = obj_data['footer_note'];
            if(footer_note !== '' && footer_note !== null){
                footer_note = footer_note.replace(/\\n/g, "");
            }
            CKEDITOR.instances['edi_footer_note'].setData(footer_note);
            //Add data to parent combobox
            $("#com_parent").find('option').not(':first').remove();
            for (var i = 0; i < arr_data.length; i++) {
                $("#com_parent").append(new Option(arr_data[i]['content'] + " - " + arr_data[i]['season'] + " - " + arr_data[i]['title'], arr_data[i]['content_episodes_id']));
            }
            if (parent_gid == 0) {
                $('#com_parent').val($('#com_parent option:eq(0)').val()).trigger('change');
            } else {
                $("#com_parent").val(parseInt(obj_data['parent_gid'])).trigger('change');
            }
            //Open modal
            $("#modal-data").modal();
        });

        //Modal save button click
        $('#btn-save').click(function () {

            //If add new then check if duplicate alias
            if (verify_data() === false) {
                return;
            }

            //Get submit data
            var fd = new FormData();
            fd.append('content_episodes_id', content_episodes_id);
            fd.append('title', $('#txt_title').val());
            fd.append('alias', $('#txt_alias').val());
            // fd.append('footer_note', $('#txt_footer_note').val());
            // fd.append('footer_title', $('#txt_footer_title').val());
            // fd.append('footer_translator', $('#txt_footer_translator').val());
            // fd.append('footer_editor', $('#txt_footer_editor').val());
            fd.append('content_season_id', $('#com_season').val());
            fd.append('parent_gid', $('#com_parent').val());
            fd.append('full_text', CKEDITOR.instances['edi_fulltext'].getData());
            fd.append('footer_note', CKEDITOR.instances['edi_footer_note'].getData());
            fd.append('word_count', CKEDITOR.instances['edi_fulltext'].getData().replace(/<[^>]*>|\s/g, '').length);


            //Hide modal
            $('#modal-data').modal('toggle');
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url() ?>/AdminEpisodes/execute_query",
                data: fd,
                processData: false,
                contentType: false,
                success: function (objData) {
                    //Refresh data
                    refresh_data();
                },
                error: function (jqXHR, exception) {
                    alert('Có lỗi. Thử lại sau.');
                }
            });
        });
        //Combobo content select event
        $('#com_content').on('select2:select', function (e) {
            var data = e.params.data;
            //Get list season
            get_list_season(data.id);
        });
    });

    function get_list_season(content_id) {
        //Hide season not belong to selected content
        $("#com_season").find('option').remove();
        for (var i = 0; i < arr_season.length; i++) {
            if (arr_season[i]['content_id'] === content_id) {
                $("#com_season").append(new Option(arr_season[i]['title'], arr_season[i]['content_season_id']));
            }
        }
    }
    //Verify data
    function verify_data() {
        //Check empty input
        if ($('#txt_title').val() == '') {
            alert('Tên tập không được rỗng!');
            return false;
        }
        if ($('#txt_alias').val() == '') {
            alert('Tên rút gọn không được rỗng!');
            return false;
        }
        if ($('#com_season').val() === null) {
            alert('Bắt buộc chọn chương!');
            return false;
        }
       //Check duplicate data
       var result = $.grep(arr_data, function (e) {
           return e.alias === $('#txt_alias').val() && e.content_episodes_id !== content_episodes_id;
       });
       if (result.length > 0) {
           alert('Dữ liệu trùng!');
           return false;
       }
    }
    /*
     * Refill datatable data
     */
    function refresh_data() {
        //Refresh data
        $.ajax({
            type: 'GET',
            url: "<?php echo base_url() ?>/AdminEpisodes/get_data",
            success: function (data) {
                data = $.parseJSON(data);
                arr_data = data;
                //Fill datatable
                $('#example1 tbody').html('');
                var html = '';
                for (var i = 0; i < data.length; i++) {
                    html += '<tr>'
                            + '<td>' + (i + 1) + '</td>'
                            + '<td>' + data[i]['content'] + '</td>'
                            + '<td>' + data[i]['season'] + '</td>'
                            + '<td>' + data[i]['title'] + '</td>'
                            + '<td>' + data[i]['created_date'] + '</td>'
                            + '<td>'
                            + '<a href="#" class="btn-edit" data-index="' + i + '"><i class="fa fa-edit"></i> Sửa</a>'
                            + ' <a href="#" class="btn-remove" data-index="' + i + '"><i class="fa fa-remove"></i> Xóa</a>'
                            + '</td>'
                            + '</tr>';
                }
                $('#example1 tbody').html(html);
                //Initialize datatable
                $('#example1').DataTable();

            },
            error: function () {
                alert('Có lỗi. Thử lại sau.');
            }
        });
    }
</script>