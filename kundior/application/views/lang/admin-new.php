
<!--Main layout-->
<main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-2 mb-4"></div>
            <!-- Default form login -->
            <div class="col-md-8 mb-4 bk-white border border-light">
                <div class="row">
                    <div class="col-md-6">
                        <div class="bk-white al-center p-5">
                            <img src="" alt="Zola Web Group V2" height="200" class="mb-4"/>
                            <h4 class="mb-4">WELCOME TO ADMIN MANAGER.</h4>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <form id="frmSubmit" class="text-center p-5 bk-white">

                            <p class="h4 mb-4">Sign in</p>

                            <!-- Email -->
                            <input type="email" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="E-mail" name="data[username]" required="">

                            <!-- Password -->
                            <input type="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="Password" name="data[password]" required="">

                            <div class="d-flex justify-content-around">
                                <div>
                                    <!-- Remember me -->
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                                        <label class="custom-control-label" for="defaultLoginFormRemember">Remember me</label>
                                    </div>
                                </div>
                                <div>
                                    <!-- Forgot password -->
                                    <a href="">Forgot password?</a>
                                </div>
                            </div>

                            <!-- Sign in button -->
                            <button class="btn btn-info btn-block my-4" type="submit">Sign in</button>

                        </form>
                    </div>
                </div>
            </div>
            <!-- Default form login -->
            <div class="col-md-2 mb-4"></div>
        </div>
    </div>
</main>
<!--Main layout-->
<script type="text/javascript">
    $(document).ready(function () {
        //Submit login data
        $("#frmSubmit").submit(function (e) {
            var frm = $(this);
            //Prevent form submit
            e.preventDefault();
            //Submit login
            AjaxHandle({
                'type': 'POST',
                'url': '<?php echo $urlAmdinLoginRequest; ?>',
                'objData': form.serialize()
            });
        });
    });
    
    /*
    * Handle after submit login data
     */
</script>