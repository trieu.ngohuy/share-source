<main id="main">
    <div class="box">
        <div class="btitle">CHƯƠNG TIỂU THUYẾT MỚI CẬP NHẬT</div>
        <div class="bcontent">
            <div class="gridlist">
                <?php
                    foreach ($data as $key => $value) {
                        ?>
                            <div class="glitem">
                                <a class="image" href="<?php echo $value['url']?>" style="background-image: url(&quot;<?php echo $value['content_logo']?>&quot;), url(&quot;<?php echo base_url() . IMG_DEFAULT_ITEM?>&quot;);"></a>
                                <div class="info">
                                    <a class="story" href="<?php echo $value['content_url']?>"><?php echo $value['content']?></a>
                                    <a class="chap" href="<?php echo $value['url']?>"><?php echo $value['title']?></a>
                                    <div class="vol"><?php echo $value['season']?></div>
                                </div>
                            </div>
                        <?php
                    }
                ?>
            </div>
            <a class="seemore" href="<?php echo $url?>">XEM TẤT CẢ</a>
        </div>
    </div>    
</main>