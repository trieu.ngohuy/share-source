<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quản lý tập
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header al-right">
                    <button type="button" id="btn_new" class="btn btn-primary">Thêm mới</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tên tập</th>
                                <th>Logo</th>
                                <th>Thuộc truyện</th>
                                <th>Ngày tạo</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($data as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $value['title']; ?></td>
                                    <td><img src="<?php echo base_url().$value['logo']; ?>" width="50" /></td>
                                    <td><?php echo $value['content']; ?></td>
                                    <td><?php echo $value['created_date']; ?></td>
                                    <td>
                                        <a href="#" class="btn-edit" data-index="<?php echo $count - 1; ?>"><i class="fa fa-edit"></i> Sửa</a>
                                        <a href="#" class="btn-remove" data-index="<?php echo $count - 1; ?>"><i class="fa fa-remove"></i> Xóa</a>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Tên tập</th>
                                <th>Logo</th>
                                <th>Thuộc truyện</th>
                                <th>Ngày tạo</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade" id="modal-data">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Tên tập</label>
                    <input id="txt_title" type="text" class="form-control" placeholder="Tên tập">
                </div>
                <div class="form-group">
                    <label>Tên rút gọn</label>
                    <input id="txt_alias" type="text" class="form-control" placeholder="Tên rút gọn">
                    <p class="help-block">Tên rút gọn không được trùng.</p>
                </div>
                <div class="form-group pos-related">
                    <label for="exampleInputFile">Logo</label>
                    <input name="logo" type="file" id="logo">
                    <img class="upload_img" src="" width="50" id="img_logo"/>
                    <p class="help-block">Tải lên logo cho truyện. Chỉ hổ trợ định dạng png/jpg.</p>
                </div>
                <div class="form-group">
                    <label>Thuộc truyện</label>
                    <select id="com_content" class="form-control select2"data-placeholder="Chọn truyện"
                            style="width: 100%;">
                                <?php
                                foreach ($content as $value) {
                                    echo '<option value="' . $value['content_id'] . '">' . $value['title'] . '</option>';
                                }
                                ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="btn-save">Lưu</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Style -->
<style>
    .select2-container .select2-selection--single {
        height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 31px;
    }
</style>
<!-- page script -->
<script>
    //Convert php array to js array
<?php
$js_array = json_encode($data);
echo "var arr_data = " . $js_array . ";\n";
?>
    var mode = 'new';
    var content_season_id = 0;
    $(function () {
        //Initialize datatable
        $('#example1').DataTable();
        //Initialize Select2 Elements
        $('.select2').select2();
    });
    $(document).ready(function () {

        //Title on change
        $("#txt_title").on('input', function () {
            $("#txt_alias").val(convert_vi_to_en($(this).val()));
        });
        //Click new button
        $(document).on('click', '#btn_new', function (e) {
            //Update mode
            mode = 'new';
            //Set title
            $('.modal-title').html('Thêm mới');
            //Project id
            content_season_id = 0;
            //Reset modal data
            $("#txt_title").val('');
            $("#txt_alias").val('');
            $("#com_content").val(null).trigger("change");
            $('.upload_img').hide();            
            $("#logo").val('');
            //Open modal
            $("#modal-data").modal();
        });
        //Click remove button
        $(document).on('click', '.btn-remove', function (e) {
            if (confirm("Are you sure you want to delete this?")) {
                //Get id
                var index = $(this).attr('data-index');
                //Get edit data
                var obj_data = arr_data[index];
                //Project id
                content_season_id = obj_data['content_season_id'];
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url() ?>/AdminSeason/delete",
                    data: {
                        'content_season_id': content_season_id
                    },
                    success: function (objData) {
                        if(objData === '1'){
                            //Refresh data
                            refresh_data();
                        }else{
                            alert('Có lỗi. Thử lại sau.');
                        }
                    },
                    error: function (jqXHR, exception) {
                        alert('Có lỗi. Thử lại sau.');
                    }
                });
            }
        });
        //Click edit button
        $(document).on('click', '.btn-edit', function (e) {
            //Update mode
            mode = 'edit';
            //Get id
            var index = $(this).attr('data-index');
            //Set title
            $('.modal-title').html('Chỉnh sửa');
            //Get edit data
            var obj_data = arr_data[index];
            //Project id
            content_season_id = obj_data['content_season_id'];
            //Fill modal data
            $("#txt_title").val(obj_data['title']);
            $("#txt_alias").val(obj_data['alias']);
            if (content_season_id === 0) {
                $('#com_content').val($('#com_content option:eq(0)').val()).trigger('change');
            } else {
                $("#com_content").val(parseInt(obj_data['content_id'])).trigger('change');
            }
            if (obj_data['logo'] !== '' && obj_data['logo'] !== null) {
                $('#img_logo').show();
                $('#img_logo').attr('src', '<?php echo base_url() ?>' + obj_data['logo']);
            }else{
                $('#img_logo').hide();
            }                        
            //Open modal
            $("#modal-data").modal();
        });

        //Modal save button click
        $('#btn-save').click(function () {

            //If add new then check if duplicate alias
            if (verify_data() === false) {
                return;
            }

            //Get submit data
            var fd = new FormData();
            var logo_upload = $('#logo')[0].files[0];
            fd.append('logo', logo_upload);
            fd.append('content_season_id', content_season_id);
            fd.append('title', $('#txt_title').val());
            fd.append('alias', $('#txt_alias').val());
            fd.append('content_id', $('#com_content').val());

            //Hide modal
            $('#modal-data').modal('toggle');
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url() ?>/AdminSeason/execute_query",
                data: fd,
                processData: false,
                contentType: false,
                success: function (objData) {
                    //Refresh data
                    refresh_data();
                },
                error: function (jqXHR, exception) {
                    alert('Có lỗi. Thử lại sau.');
                }
            });
        });
    });
    //Verify data
    function verify_data() {
        //Check empty input
        if ($('#txt_title').val() == '') {
            alert('Tên tập không được rỗng!');
            return false;
        }
        if ($('#txt_alias').val() == '') {
            alert('Tên rút gọn không được rỗng!');
            return false;
        }
        //Check duplicate data
//        var result = $.grep(arr_data, function (e) {
//            return e.alias === $('#txt_alias').val() && e.content_season_id !== content_season_id;
//        });
//        if (result.length > 0) {
//            alert('Dữ liệu trùng!');
//            return false;
//        }
    }
    /*
     * Refill datatable data
     */
    function refresh_data() {
        //Refresh data
        $.ajax({
            type: 'GET',
            url: "<?php echo base_url() ?>/AdminSeason/get_data",
            success: function (data) {
                data = $.parseJSON(data);
                arr_data = data;
                //Fill datatable
                $('#example1 tbody').html('');
                var html = '';
                for (var i = 0; i < data.length; i++) {
                    html += '<tr>'
                            + '<td>' + (i + 1) + '</td>'
                            + '<td>' + data[i]['title'] + '</td>'
                            + '<td><img src="<?php echo base_url();?>' + data[i]['logo'] + '" width="50"/></td>'
                            + '<td>' + data[i]['content'] + '</td>'
                            + '<td>' + data[i]['created_date'] + '</td>'
                            + '<td>'
                            + '<a href="#" class="btn-edit" data-index="' + i + '"><i class="fa fa-edit"></i> Sửa</a>'
                            + ' <a href="#" class="btn-remove" data-index="' + i + '"><i class="fa fa-remove"></i> Xóa</a>'
                            + '</td>'
                            + '</tr>';
                }
                $('#example1 tbody').html(html);
                //Initialize datatable
                $('#example1').DataTable();

            },
            error: function () {
                alert('Có lỗi. Thử lại sau.');
            }
        });
    }
</script>