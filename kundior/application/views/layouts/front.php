<!DOCTYPE html>
<html>
<meta http-req="Content-Type" content="charset=utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title><?php echo get_setting_value($setting, 'website_title'); ?> - <?php echo $website_title;?></title>
<meta name="desc" content="<?php echo get_setting_value($setting, 'website_title'); ?> - <?php echo $website_title;?>">
<meta name="image" content="<?php echo base_url(). get_setting_value($setting, 'favicon'); ?>">
<link rel="icon" type="image/png" href="<?php echo base_url(). get_setting_value($setting, 'favicon'); ?>">
<meta property="fb:app_id" content="783908218461381">
<!-- facebook-->
<meta property="og:title" content="<?php echo get_setting_value($setting, 'website_title'); ?> - <?php echo $website_title;?>">
<meta property="og:description" content="<?php echo get_setting_value($setting, 'website_title'); ?> - <?php echo $website_title;?>">
<meta property="og:image" content="<?php echo base_url(). get_setting_value($setting, 'favicon'); ?>">

<body>
    <script>
        window.fbAsyncInit = function() {FB.init({appId      : '783908218461381',cookie     : true,xfbml      : true,version    : 'v2.12'});FB.AppEvents.logPageView();};(function(d, s, id){var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) {return;}js = d.createElement(s); js.id = id;js.src = "https://connect.facebook.net/en_US/sdk.js";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));
    </script>
    <!--div id="fb-root"></div><script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.12&appId=783908218461381&autoLogAppEvents=1';fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script-->
    <?php $this->load->view('elements/frontend/header'); ?> 
    <?php echo $contents; ?>
    <?php $this->load->view('elements/frontend/sidebar'); ?> 
    <?php $this->load->view('elements/frontend/footer'); ?> 
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/index.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/header.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/main.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/rpanel.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/footer.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/box.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/login.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/gridlist.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/ranklist.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/tabpanel.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/search.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/story.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/read.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/member.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/list.css">
    <link rel="stylesheet" href="<?php echo asset_front_url() ?>css/profile.css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i|Varela+Round&amp;amp;subset=vietnamese" rel="stylesheet">
    <script src="<?php echo asset_front_url() ?>js/iscript.js"></script>
</body>

</html>