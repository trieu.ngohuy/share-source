﻿function scrollUpdate(){
    if (window.scrollY <= 10){
        document.getElementById('header').setAttribute('itop', true);
        document.querySelector('.menu').setAttribute('itop', true);
        document.querySelector('label[for="opmenu"]').setAttribute('itop', true);
    } else {
        document.getElementById('header').setAttribute('itop', false);
        document.querySelector('.menu').setAttribute('itop', false);
        document.querySelector('label[for="opmenu"]').setAttribute('itop', false);
    }
}

document.addEventListener('DOMContentLoaded', () => {
    document.body.setAttribute('iscript', 'true');
    scrollUpdate();
})

window.onscroll = function(){
    scrollUpdate();
}

var content = document.querySelector('#read .content');

if (content){

    var color = localStorage.getItem('color') || 'default';
    var fontsize = localStorage.getItem('fontsize') || 12;
    var padding = localStorage.getItem('padding') || 0;
    content.style.fontSize = fontsize + 'pt';
    content.style.padding = '0px ' + padding * 10 + 'px';
    document.getElementById('qdirect').style.display = 'block';
    document.getElementById(`cfcl-${color}`).checked = true;
    document.getElementById(`cf-fontsize`).value = fontsize;
    document.getElementById(`cf-padding`).value = padding;
    document.body.setAttribute('theme', color);

    var lscroll = 0;

    window.addEventListener('scroll', function(){
        var cscroll = window.pageYOffset;

        if (cscroll >= 200){
            if (lscroll < cscroll){
                document.body.setAttribute('hideheader', 'true');
            } else {
                console.log('show')
                document.body.setAttribute('hideheader', 'false');
            }
        }

        lscroll = cscroll;
    });

    var r = /\(Note\: (.*?)\)/gm;
    var rel;
    var index = 0;
    while (rel = r.exec(content.innerHTML)){
        content.innerHTML = content.innerHTML.replace(rel[0], '<input type="checkbox" class="opnote hidden" id="note-' + index + '"><span class="note-content">' + rel[1] + '</span><label class="note" for="note-' + index + '">...</label>')
        index++
    }

    function updateColor(){
        var def = document.getElementById(`cfcl-default`);
        var light = document.getElementById(`cfcl-light`);
        var wood = document.getElementById(`cfcl-wood`);
        var green = document.getElementById(`cfcl-green`);
        var yellow = document.getElementById(`cfcl-yellow`);

        if (def.checked){
            localStorage.setItem('color', 'default');
            document.body.setAttribute('theme', 'default');
        } else if (light.checked) {
            localStorage.setItem('color', 'light');
            document.body.setAttribute('theme', 'light');
        } else if (wood.checked) {
            localStorage.setItem('color', 'wood');
            document.body.setAttribute('theme', 'wood');
        } else if (green.checked) {
            localStorage.setItem('color', 'green');
            document.body.setAttribute('theme', 'green');
        } else if (yellow.checked) {
            localStorage.setItem('color', 'yellow');
            document.body.setAttribute('theme', 'yellow');
        }
    }

    function increaseFontSize(){
        fontsize++;
        document.getElementById(`cf-fontsize`).value = fontsize;
        localStorage.setItem('fontsize', fontsize);
        content.style.fontSize = fontsize + 'pt';
    }

    function increasePadding(){
        padding++;
        localStorage.setItem('padding', padding);
        document.getElementById(`cf-padding`).value = padding;
        content.style.padding = '0px ' + padding * 10 + 'px';
    }

    function decreaseFontSize(){
        fontsize--;
        document.getElementById(`cf-fontsize`).value = fontsize;
        localStorage.setItem('fontsize', fontsize);
        content.style.fontSize = fontsize + 'pt';
    }

    function decreasePadding(){
        padding--;
        localStorage.setItem('padding', padding);
        document.getElementById(`cf-padding`).value = padding;
        content.style.padding = '0px ' + padding * 10 + 'px';
    }
}
